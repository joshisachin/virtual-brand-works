// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  API_URL: "http://44.232.180.17:5055/api/",
  SUCCESS_URL: "http://localhost:4200/paymentRedirectUrl?type=success",
  CANCEL_URL: "http://localhost:4200/paymentRedirectUrl?type=cancel",
  FACEBOOK_LINK: "https://www.facebook.com/virtualbrandworks",
  INSTAGRAM_LINK: "https://www.instagram.com/virtualbrandworks/",
  TWITTER_LINK: "https://twitter.com/VrBrWk",
  LINKEDIN_LINK: "https://www.linkedin.com/company/virtualbrandworks",
  PAGINATE_FETCH: 20,
  FILE_TYPES: ["jpeg", "jpg", "png", "mp4", "mov", "pdf"],
  firebase: {
    apiKey: "AIzaSyDb5bo-kg3UJ5ZSHqRJ8a_byZCsz3I4jfQ",
    authDomain: "fir-notification-b32ce.firebaseapp.com",
    databaseURL: "https://fir-notification-b32ce.firebaseio.com",
    projectId: "fir-notification-b32ce",
    storageBucket: "fir-notification-b32ce.appspot.com",
    messagingSenderId: "9258028866",
    appId: "1:9258028866:web:2be306afc0f9e48fbdc2db"
  },
  SITE_URL: "http://44.232.180.17:5058/",
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
