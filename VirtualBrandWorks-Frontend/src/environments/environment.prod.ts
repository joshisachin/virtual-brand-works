export const environment = {
  production: true,
  API_URL: "http://44.232.180.17:5055/api/",
  SUCCESS_URL: "http://44.232.180.17:6058/paymentRedirectUrl?type=success",
  CANCEL_URL: "http://44.232.180.17:6058/paymentRedirectUrl?type=cancel",
  FACEBOOK_LINK: "https://www.facebook.com/virtualbrandworks",
  INSTAGRAM_LINK: "https://www.instagram.com/virtualbrandworks/",
  TWITTER_LINK: "https://twitter.com/VrBrWk",
  LINKEDIN_LINK: "https://www.linkedin.com/company/virtualbrandworks",
  PAGINATE_FETCH: 20,
  FILE_TYPES: ["jpeg", "jpg", "png", "mp4", "mov", "pdf"],
  SITE_URL: "http://44.232.180.17:6058/",
  firebase: {
    apiKey: "AIzaSyDb5bo-kg3UJ5ZSHqRJ8a_byZCsz3I4jfQ",
    authDomain: "fir-notification-b32ce.firebaseapp.com",
    databaseURL: "https://fir-notification-b32ce.firebaseio.com",
    projectId: "fir-notification-b32ce",
    storageBucket: "fir-notification-b32ce.appspot.com",
    messagingSenderId: "9258028866",
    appId: "1:9258028866:web:2be306afc0f9e48fbdc2db"
  }
};
