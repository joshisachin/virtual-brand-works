import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse,
  HttpClient
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from "../../environments/environment"
import { catchError, tap, switchMap } from 'rxjs/operators';
import { CommonService } from '../shared/services/commonservices/commonservices.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpService } from '../shared/services/httpservice/httpservice.service';
import { cpuUsage } from 'process';
import { API_NAME } from '../shared/constants/apiname/apiname';

@Injectable()
export class ServiceInterceptor implements HttpInterceptor {
  apiUrl: any;
  storeReq: any = [];
  constructor(private commonServices: CommonService, private cookieService: CookieService, private httpServices: HttpService, private http: HttpClient) {
    this.apiUrl = environment.API_URL;
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.cookieService.get("token");
    const httpRequest = new HttpRequest(<any>request.method, this.apiUrl + request.url, request.body);
    request = Object.assign(request, httpRequest);
    if (token) { // addigng token for every request after login
      request = request.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + token,
          'Offset': new Date().getTimezoneOffset().toString()
        }
      });
    }
    let requestURL = request.url.split('/').pop(); // fetch the url string
    if (!request.headers.has('Content-Type') && requestURL === "login") { // different headers required for login
      request = request.clone({
        setHeaders: {
          'content-type': 'application/x-www-form-urlencoded',
        }
      });
    }
    else if (!request.headers.has('Content-Type') && requestURL !== "login") {
      request = request.clone({
        setHeaders: {
          'content-type': 'application/json',
        }
      });
    }
    this.commonServices.loader.next(true);
    let a = this.sendRequest(request, next);
    return a;
  }

  /**
   * send request
   * @param request 
   * @param next 
   */
  sendRequest(request, next) {
    return next.handle(request).pipe(
      tap((res) => {
        if (res instanceof HttpResponse) {
          this.commonServices.loader.next(false);
        }
      }),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `${error.error.message}`;
        } else {
          // server-side error
          errorMessage = `Server error`;
          if (error.status == 401) {
            this.storeReq.push(request);
            console.log("unauthorized");

            if (this.cookieService.get("token")) {
              return this.httpServices.renewToken().pipe(
                switchMap((newToken) => {
                  console.log("newtoken", newToken)
                  this.cookieService.set("token", newToken.Data);
                  let req = request.clone({
                    setHeaders: {
                      'Authorization': 'Bearer ' + newToken.Data,
                      'Offset': new Date().getTimezoneOffset().toString()
                    }
                  });
                  return next.handle(req);
                })
              )
            }
            else {
              this.commonServices.logout();
              errorMessage = "Unauthorized, please contact your company admin";
            }
          }
          else {
            this.commonServices.showError(errorMessage, "");
          }
        }
        this.commonServices.loader.next(false);
        return throwError(errorMessage);
      })
    );
  }
}
