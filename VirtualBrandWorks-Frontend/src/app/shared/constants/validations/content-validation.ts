/**
 * constant for form validation messages
 */
export const VALIDATION_MESSAGES = {
    'FolderId': [
        { type: 'required', message: 'Please select a folder' },
    ],
    'Name': [
        { type: 'required', message: 'Title is required' },
        { type: "pattern", message: "Please enter valid title" },
        { type: "maxlength", message: "Maximum number of characters are 30." }
    ],
    'Description': [
        { type: 'required', message: 'Description is required' },
        { type: "pattern", message: "Please enter valid description" },
        { type: "maxlength", message: "Maximum 500 characters are allowed" },
        { type: "minlength", message: "Minimum 4 characters are allowed" }
    ],
    'file': [
        { type: 'required', message: 'A file is required' },
    ],
    'FolderName': [
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'pattern', message: 'Enter a valid name' },
        { type: 'maxlength', message: 'Maximum number of characters ar 30' },
    ],
    "FolderShortName": [
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'minlength', message: 'The field should be two characters long' },
        { type: 'maxlength', message: 'The field should be two characters long' },
        { type: 'pattern', message: 'This is not a valid short form. Please input only alphabets' }
    ],
    "StartDate": [
        { type: 'required', message: 'The field cannot be left blank' },
    ],
    "EndDate": [
        { type: 'required', message: 'The field cannot be left blank' },
    ],
    "Rules": [
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'maxlength', message: 'Rules cannot be more than 500 characters' },
    ],
    "YoutubeUrl": [
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'pattern', message: 'Enter a valid url' },
    ],
    "FacebookUrl": [
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'pattern', message: 'Enter a valid url' },
        { type: 'maxlength', message: 'Either you can upload a video or provide one of the url(s)' },
    ]
}
