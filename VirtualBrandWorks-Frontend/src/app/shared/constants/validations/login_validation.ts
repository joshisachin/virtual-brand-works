/**
 * constant for form validation messages
 */
export const VALIDATION_MESSAGES = {
    'Email': [
        { type: 'required', message: 'Username cannot be left blank' },
        { type: 'pattern', message: 'Please enter a valid email format' },
    ],
    'Password': [
        { type: 'required', message: 'Password cannot be left blank' },
    ]
}
