/**
 * constant for form validation messages
 */
export const VALIDATION_MESSAGES = {
    'Question': [
        { type: 'required', message: 'Question is required' },
        { type: 'maxlength', message: 'Maximum number of characters are 500' },
    ],
    'Answer1': [
        { type: 'required', message: 'All answer options are required' },
        { type: 'maxlength', message: 'Maximum number of characters are 200' },
    ],
    'Answer2': [
        { type: 'required', message: 'All answer options are required' },
        { type: 'maxlength', message: 'Maximum number of characters are 200' },
    ],
    'Answer3': [
        { type: 'required', message: 'All answer options are required' },
        { type: 'maxlength', message: 'Maximum number of characters are 200' },
    ],
    'Answer4': [
        { type: 'required', message: 'All answer options are required' },
        { type: 'maxlength', message: 'Maximum number of characters are 200' },
    ],
    'IsRightAnswer': [
        { type: 'required', message: 'Right answer option is required' },
    ],
    
}
