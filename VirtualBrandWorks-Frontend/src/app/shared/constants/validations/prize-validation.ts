/**
 * constant for form validation messages
 */
export const VALIDATION_MESSAGES = {
    'PrizeCategoryId': [
        { type: 'required', message: 'Category is required' },
    ],
    'Title': [
        { type: 'required', message: 'Title is required' },
        { type: "pattern", message: "Please enter valid title" },
        { type: "maxlength", message: "Maximum number of characters are 30." }
    ],
    'Description': [
        { type: 'required', message: 'Description is required' },
        { type: "pattern", message: "Please enter valid description" },
        { type: "maxlength", message: "Maximum 500 characters are allowed" },
        { type: "minlength", message: "Minimum 4 characters are allowed" }
    ],
    'PrizeAmount': [
        { type: 'required', message: 'Prize amount is required' },
        { type: 'min', message: 'Prize amount cannot be 0' },
    ],
    'BrandName': [
        { type: 'required', message: 'Brand name is required' },
        { type: "pattern", message: "Please enter valid name" },
    ],
    'Points': [
        { type: 'required', message: 'Points are required' },
        { type: 'min', message: 'Minimum 1 point is required' },
    ],
    'Quantity': [
        { type: 'required', message: 'Quantity is required' },
        { type: 'min', message: 'Minimum quantity is 1' },
        { type: 'max', message: 'Maximum quantity is 10' },
    ],
    "StartDate": [
        { type: 'required', message: 'The field cannot be left blank' },
    ],
    "EndDate": [
        { type: 'required', message: 'The field cannot be left blank' },
    ]
}
