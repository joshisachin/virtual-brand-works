/**
 * constant for form validation messages
 */
export const VALIDATION_MESSAGES = {
    'Email': [
        { type: 'required', message: 'Email cannot be left blank' },
        { type: 'pattern', message: 'Please enter a valid email format' },
        { type: "maxlength", message: "Maximum number of characters 100" }
    ],
    'ResetCode': [
        { type: 'required', message: 'Reset code is required.' }
    ],
    'Password': [
        { type: 'required', message: 'Password cannot be left blank' },
        { type: 'minlength', message: 'Password must be at least 8 characters long.' },
        { type: 'maxlength', message: 'Password cannot be more than 15 characters long.' },
        { type: 'pattern', message: 'Your password must contain uppercase, lowercase characters, numbers and special character' },
        { type: 'Confirmpassword', message: 'The New password and Confirmation password do not match.' },
    ],
    'NewPassword': [
        { type: 'required', message: 'Password cannot be left blank' },
        { type: 'minlength', message: 'Password must be at least 8 characters long.' },
        { type: 'maxlength', message: 'Password cannot be more than 15 characters long.' },
        { type: 'pattern', message: 'Your password must contain least one lowercase letter, one uppercase letter, one numeric digit, and one special character.' },
        { type: 'Confirmpassword', message: 'The New password and Confirmation password do not match.' },
    ],
    'ConfirmPassword': [
        { type: 'required', message: 'Confirm Password is required.' },
        { type: 'Confirmpassword', message: 'Password and confirm password do not match.' },
    ],
    'SelectedCategoryId': [
        { type: 'required', message: 'Industry cannot be left blank' },
    ],
    'CompanyName': [
        { type: 'required', message: 'Company cannot be left blank' },
        { type: 'pattern', message: 'Please enter valid company name' },
        { type: 'maxlength', message: 'Maximum number of characters are 50' },
    ],
    'handleName': [
        { type: 'required', message: 'Handle Name is required' }
    ],
    'FirstName': [
        { type: 'required', message: 'First Name cannot be left blank' },
        { type: 'pattern', message: 'Please input a valid First Name' },
        { type: 'maxlength', message: 'Maximum number of characters are 50' },
    ],
    'LastName': [
        { type: 'required', message: 'Last Name cannot be left blank' },
        { type: 'pattern', message: 'Please input a valid Last Name' },
        { type: 'maxlength', message: 'Maximum number of characters are 50' },
    ],
    'PhoneNo': [
        { type: 'required', message: 'Phone No. cannot be left blank' },
        { type: 'pattern', message: 'Please enter a valid Phone No.' },
        { type: 'maxlength', message: 'Maximum number of characters are 15' },
    ],
    'SelectedCountryId': [
        { type: 'required', message: 'Country cannot be left blank' }
    ],
    'SelectedStateId' : [
        { type: 'required', message: 'State/Province cannot be left blank' }
    ],
    'SelectedCityId': [
        { type: 'required', message: 'City cannot be left blank' },
    ],
    'LocationName': [
        { type: 'required', message: 'Location Name cannot be left blank' }
    ],
    'ZipCode': [
        { type: 'required', message: 'ZipCode cannot be left blank' },
        { type: 'pattern', message: 'Please enter a valid ZipCode.' },
        { type: 'maxlength', message: 'Maximum number of characters are 30' },
    ],
    'IsPolicyAccepted':[
        { type: 'required', message: 'Please accept privacy policy and terms' }
    ],
    'AddressOne':[
        { type: 'maxlength', message: 'Maximum number of characters are 150' }
    ],
    'AddressTwo':[
        { type: 'maxlength', message: 'Maximum number of characters are 200' }
    ],
    'Message':[
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'pattern', message: 'The field cannot be left blank'},
        { type: 'maxlength', message: 'Maximum number of characters are 500'},
    ],
    'Name':[
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'pattern', message: 'Enter a valid name'},
        { type: 'maxlength', message: "Name should be maximum 30 characters long" }
    ],
    'ShortName':[
        { type: 'required', message: 'The field cannot be left blank' },
        { type: 'minlength', message: 'The field should be two characters long'},
        { type: 'maxlength', message: 'The field should be two characters long'},
        { type: 'pattern', message: 'Enter a valid short name'}
    ],
    'Description': [
        { type: 'maxlength', message: 'This field does not allow more than 500 characters'},
    ],
    'AboutCompany': [
        { type: 'maxlength', message: 'This field does not allow more than 500 characters'},
    ]
}

/**
 * constant for change password form validation messages
 */

export const change_password_VALIDATION_MESSAGES = {
    'OldPassword': [
        { type: 'required', message: 'Old Password cannot be left blank' },
        { type: 'minlength', message: 'Password must be at least 8 characters long.' },
        { type: 'maxlength', message: 'Password cannot be more than 15 characters long.' },
        { type: 'pattern', message: 'Your password must contain uppercase, lowercase characters, numbers and special character' },
        { type: 'Confirmpassword', message: 'The New password and Confirmation password do not match.' },
    ],
    'NewPassword': [
        { type: 'required', message: 'New Password cannot be left blank' },
        { type: 'minlength', message: 'Password must be at least 8 characters long.' },
        { type: 'maxlength', message: 'Password cannot be more than 15 characters long.' },
        { type: 'pattern', message: 'Your password must contain least one lowercase letter, one uppercase letter, one numeric digit, and one special character.' },
        { type: 'Confirmpassword', message: 'The New password and Confirmation password do not match.' },
    ],
    'ConfirmPassword': [
        { type: 'required', message: 'Confirm Password cannot be left blank' },
        { type: 'Confirmpassword', message: 'The confirmation password should be same as New Password' },
    ],
}

/**
 * Constant message for add location form
 */

export const ADD_LOCATION = {
    'LocationName': [
        { type: 'required', message: 'Location Name cannot be left blank' },
        { type: 'pattern', message: 'Please input a valid Location Name' },
        { type: 'maxlength', message: 'Maximum number of characters are 50' },
    ],
    'CountryId': [
        { type: 'required', message: 'Country cannot be left blank' }
    ],
    'StateId' : [
        { type: 'required', message: 'State/Province cannot be left blank' }
    ],
    'CityId': [
        { type: 'required', message: 'City cannot be left blank' },
    ],
}
