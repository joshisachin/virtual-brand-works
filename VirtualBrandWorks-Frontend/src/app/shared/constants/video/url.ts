/**
 * constant for API Urls
 */
export const SOCIAL_URL = {
    "Facebook": "https://www.facebook.com/plugins/video.php?href=",
    "Youtube": "https://www.youtube.com/embed/",
    "FacebookImage": "http://44.232.180.17:5055/Images/thumbs/thumb_fb_video.png",
    "YoutubeImage": "https://img.youtube.com/vi/"
}
