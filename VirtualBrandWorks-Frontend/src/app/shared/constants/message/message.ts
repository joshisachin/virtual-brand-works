/**
 * constant for messages when reponse coming from API
 */
export const API_MESSAGES = {
    'ChangePassword': { 
        "successMessage": 'Your password has been changed successfully'
    },
    "CreateFolder": {
        "successMessage": "Successfully created"
    },
    "UpadateFolder": {
        "successMessage": "Updated succesfully",
        "shortNameError": "Short name cannot be empty and should be two characters",
        "nameError": "Name cannot be empty."
    },
    "Logout": {
        "successMessage": "Logged out successfully"
    },
    "DeleteTeam": {
        "successMessage": "Deleted Successfully"
    },
    "ResetPasswordCode": {
        "successMessage": "Reset Code sent to your email"
    },
    "WriteToUs": {
        "successMessage": "Thank you for getting in touch. We will get back to you soon."
    },
    "Payment": {
        "successMessage": "Payment Successful",
        "cancelMessage": "Payment Cancelled"
    },
    "ResetPassword": {
        "successMessage": "Password Reset Successful"
    },
    "Login": {
        "successMessage": "Logged in successfully"
    },
    "Signup": {
        "successMessage": "Thanks for subscribing! Your account has been created successfully."
    },
    "CommonError": {
        "message": "An error occurred"
    },
    "LoginValidationError": {
        "message": "Please provide valid User Name/Handle Name and password."
    },
    "Team": {
        "noTeamError": "No team found",
        "nameRequire": "Team name cannot be blank",
        "memberRequire": "Please select atleast one team member",
        "locationRequired":"Please select the location",
        "nameLength": "Name cannot be more then 30 characters long",
        "teamHandle": "Team Handle cannot be blank"
    },
    "UpdateProfile": {
        "successMessage": "Profile has been updated successfully"
    },
    "UpdateProfileImage": {
        "successMessage": "Profile image has been updated successfully"
    },
    "FileUpload": {
        "errorMessage": "An error in uploading file",
        "fileType": "Please upload valid files only",
        "fileCount": "Select one file only"
    },
    "Content": {
        "successMessage": "Created successfully",
        "publishMessage": "Published successfully",
        "oneAllowed": "Upload a video or any one url",
        "dateError": "Start date cannot be less then end date",
        "videoDuration": "Video cannot be more than 5 minutes long"
    },
    "Quiz": {
        "questionAddSuccess": "Questions added successfully",
        "createQuiz": "Successfully created",
        "checkName": "Please enter valid name",
        "questionUpdateSuccess": "Questions updated successfully",
        "submitSuccess": "Submitted successfully"
    },
    "Playlist": {
        "playlistCreateSuccess": "Playlist created successfully",
        "userTeamAddedSuccess": "User/Team Assigned Successfully",
        "assignUserTeamRequired":"Please select atleast one user or team",
        "startDateError": "Start date should be less than the end date",
        "expiredPlaylist": "This playlist has expired",
        "noContentFound": "No Content Found"
    },
    "Messages":{
        "MessageRequired":"Please type some message to send.",
        "FileUpload":"Please send only image files"
    },
    "AssociatedUsersTeams": {
        "NoFoldersFound": "No folder found."
    },
    "Contest": {
        "IsBroadcast": "Broadcasted successfully"
    },
    "Store": {
        "VoucherError": "Voucher codes are mandatory. Please save all.",
        "VoucherSuccess": "Added Successfully",
        "VoucherCount": "All fields are required",
        "VoucherSend": "Voucher has been sent on your email"
    },
    "Plan": {
        "Cancelled": "Plan Cancelled",
        "NoPlan": "No new plan selected"
    },
    "Status": {
        "Success": "Status updated successfully",
    },
    "ReportStatus": {
        "Report": "Video reported successfully",
        "UnReport": "Video unReported successfully",
        "Delete": "Video deleted successfully"
    }
}
