/**
 * constant for API Urls
 */
export const API_NAME = {
    'Login': [
        { name: 'account/login' }
    ],
    'ExternalLogin': [
        { name: 'account/external_login' }
    ],
    'ForgotPassword': [
        {name: 'account/forgot_password'}
    ],
    'ResetPassword': [
        {name: 'account/reset_password'}
    ],
    'GetCountries': [
        {name: 'userinformation/countries'}
    ],
    'GetStates': [
        {name: 'userinformation/states'}
    ],
    'GetCities': [
        {name: 'userinformation/cities'}
    ],
    'GetIndustries': [
        {name: 'categories/get_all_categories'}
    ],
    'GetSubscriptionPlan': [
        {name: 'subscription/GetSubscriptionListForAdmin'}
    ],
    'SignUpAdmin': [
        {name: 'Account/SaveAdminDetails'}
    ],
    'GetCheckoutSessionId': [
        {name: 'payment/GetCheckoutSession'}
    ],
    'PaymentSuccess': [
        {name: 'payment/PaymentSucess'}
    ],
    'CancelPayment': [
        {name: 'payment/PaymentCancel'}
    ],
    'ValidateEmailAndCompanyDetails': [
        {name: 'Account/ValidateUserEmailAndCompanyName'}
    ],
    'GetUserInfo': [
        {name: 'userinformation/GetUserInvite'}
    ],
    'SignUpUser': [
        {name: 'account/signup_as_user'}
    ],
    'WriteToUs': [
        {name: 'userinformation/WriteToUs'}
    ],
    'GetAllFolderData': [
        {name: 'masterfolder/get_all_folders_with_content'}
    ],
    'GetFolderDetail': [
        {name: 'mastercontent/get_all_contents_for_preveiew_pop_up'}
    ],
    'SaveSelectedFolder': [
        {name: 'MasterContent/SaveSelectedMasterContentFolder'}
    ],
    'GetSelectedFolder': [
        {name: 'MasterContent/GetMasterContentSelectedFolders'}
    ],
    'GetCompanyUsers': [
        {name: 'userinformation/GetCompanyUsers'}
    ],
    'GetTeams': [
        {name: 'team/GetTeams'}
    ],
    'GetContest':[
        {name: 'VBPlay/GetAllVBPlayContest'}
    ],
    'GetTeamDetails': [
        {name: 'team/GetTeamDetail'}
    ],
    'GetTrainers': [
        {name: 'team/GetTrainers'}
    ],
    'GetUsers': [
        {name: 'team/GetUsers'}
    ],
    'CreateTeam': [
        {name: 'team/CreateTeam'}
    ],
    'UpdateUserRole': [
        {name: 'userinformation/UpdateUserRole'}
    ],
    'UpdateUserStatus': [
        {name: 'userinformation/UpdateUserStatus'}
    ],
    'AddUser': [
        {name: 'userinformation/AddUserInvite'}
    ],
    'CheckUserLimitForCompany': [
        {name: 'userinformation/CheckUserLimitForCompany'}
    ],
    'GetLocation': [
        {name: 'userinformation/GetLocation'}
    ],
    'GetTeamUsers': [
        {name: 'team/GetUsers'}
    ],
    'SaveTrialPayment': [
        {name: 'Payment/SaveTrialPayment'}
    ],
    'UpdateTeam': [
        {name: 'team/UpdateTeam'}
    ],
    'DeleteTeam': [
        {name: 'team/DeleteTeam'}
    ],
    'GetUserProifle': [
        {name: 'userinformation/GetUserProfile'}
    ],
    'GetCompanyContentFolders': [
        {name: 'CompanyContent/GetCompanyContentFolders'}
    ],
    'GetCompanyContentSelectedFolders': [
        {name: 'CompanyContent/GetCompanyContentSelectedFolders'}
    ],
    'SaveCompanySelectedFolder': [
        {name: 'CompanyContent/SaveSelectedCompanyContentFolder'}
    ],
    'UpdateCompanyFolder': [
        {name: 'CompanyContent/UpdateFolder'}
    ],
    'UpdateUserProfile': [
        {name: 'userinformation/UpdateUserProfile'}
    ],
    'ChangePassword': [
        {name: 'Account/change_password'}
    ],
    'CreateFolder': [
        {name: 'CompanyContent/CreateFolder'}
    ],
    'CreateContent': [
        {name: 'CompanyContent/CreateContent'}
    ],
    'GetMasterFolderContent': [
        {name: 'mastercontent/GetMasterFolderContents'}
    ],
    'GetCompanyFolderContent': [
        {name: 'CompanyContent/GetCompanyFolderContents'}
    ],
    'GetTagSuggestions': [
        {name: 'Tag/GetTagSuggestion'}
    ],
    'GetCompanyFolders': [
        {name: 'CompanyContent/GetCompanyFolderList'}
    ],
    'AddProfileImage': [
        {name: 'userinformation/AddProfileImage'}
    ],
    "GetAwss3PolicyAndSignature": [
        {name: "mastercontent/GetAwss3PolicyAndSignature"}
    ],
    "PublishContent": [
        {name: "CompanyContent/PublishCompanyContent"}
    ],
    "CompanyContentDetails": [
        {name: "CompanyContent/GetCompanyFolderContentDetail"}
    ],
    "CompanyContentByType": [
        {name: "CompanyContent/GetCompanyContentsByType"}
    ],
    "MasterContentDetails": [
        {name: "mastercontent/GetMasterFolderContentDetail"}
    ],
    "MasterContentByType": [
        {name: "mastercontent/GetMasterContentsByType"}
    ],
    "GetContentConfirmation": [
        {name: "CompanyContent/GetContentConfirmation"}
    ],
    "GetUserCreatedPlaylistList": [
        {name: "userinformation/GetUserCreatedPlaylistList"}
    ],
    "GetUserAssignedPlaylistList": [
        {name: "userinformation/GetUserAssignedPlaylistList"}
    ],
    "GetUserAssociatedTeam": [
        {name: "userinformation/GetUserAssociatedTeam"}
    ],
    "GetUserAssociatedUsers": [
        {name: "userinformation/GetUserAssociatedUsers"}
    ],
    "PlaylistDetailView": [
        {name: "MyPlaylist/GetMyPlaylistDetail"}
    ],
    "GetMyPlaylist": [
        {name: "MyPlaylist/GetMyPlaylist"}
    ],
    "GetPlaylistTopPerofermers": [
        {name: "Playlist/GetPlaylistTopPerofermers"}
    ],
    "AddCompanyLogo": [
        {name: "userinformation/AddCompanyLogo"}
    ],
    "SaveQuiz": [
        {name: "CompanyQuiz/SaveQuiz"}
    ],
    "AddQuestions": [
        {name: "CompanyQuiz/AddQuestion"}
    ],
    "GetQuizDetails": [
        {name: "CompanyQuiz/GetQuizDetails"}
    ],
    "UpdateQuestion": [
        {name: "CompanyQuiz/UpdateQuestion"}
    ],
    "DeleteQuestion": [
        {name: "CompanyQuiz/DeleteQuestion"}
    ],
    "DeleteQuiz": [
        {name: "CompanyQuiz/DeleteQuiz"}
    ],
    "CreatePlaylist": [
        {name: "BuildPlaylist/CreatePlaylist"}
    ],
    "SavePlaylist": [
        {name: "BuildPlaylist/SavePlaylistFolders"}
    ],
    "GetPlaylistFolders": [
        {name: "BuildPlaylist/GetPlaylistFolders"}
    ],
    "GetPlaylistFooterData": [
        {name: "BuildPlaylist/GetPlaylistFooterData"}
    ],
    "UpdatePlaylistSortOrder": [
        {name: "BuildPlaylist/UpdateSortOrderForPlaylistFolder"}
    ],
    "GetPlayListContents": [
        {name: "BuildPlaylist/GetPlayListContents"}
    ],
    "savePlaylistContents": [
        {name: "BuildPlaylist/UpdatePlayListContents"}
    ],
    "GetPlayListContentsForAddQuestion": [
        {name: "BuildPlaylist/GetPlayListContentsForAddQuestion"}
    ],
    "GetMyPlaylistFolderDetail": [
        {name: "MyPlaylist/GetMyPlaylistFolderDetail"}
    ],
    "GetMyPlaylistFolderContentList": [
        {name: "MyPlaylist/GetMyPlaylistFolderContentList"}
    ],
    "ViewResultDetailAndFolders": [
        {name: "MyPlaylist/ViewResultDetailAndFolders"}
    ],
    "ViewResultContentList": [
        {name: "MyPlaylist/ViewResultContentList"}
    ],
    "GetQuizResult": [
        {name: "MyPlaylist/GetQuizResult"}
    ],
    "GetUsersAndTeamsForAssignPlaylistToMore": [
        {name: "BuildPlaylist/GetUsersAndTeamsForAssignPlaylistToMore"}
    ],
    "AssignPlaylistTo": [
        {name: "BuildPlaylist/AssignPlaylistTo"}
    ],
    "TurnOffExistingQuiz": [
        {name: "BuildPlaylist/TurnOffExistingQuiz"}
    ],
    "TurnOffExistingQuizForAll": [
        {name: "BuildPlaylist/TurnOffExistingQuizForAll"}
    ],
    "GetPlaylistDetails": [
        {name: "BuildPlaylist/GetPlaylistDetail"}
    ],
    "GetTeamsAndUsers": [
        {name: "BuildPlaylist/GetPlaylistAssignedUserAndTeam"}
    ],
    "LaunchPlaylist": [
        {name: "BuildPlaylist/LaunchPlaylist"}
    ],
    "GetPlaylistUsers": [
        {name: "MyPlaylist/GetPlaylistUsers"}
    ],
    "ReceiveMessage": [
        {name: "Playlist/ReceiveMessage"}
    ],
    "SendMessage": [
        {name: "Playlist/SendMessage"}
    ],
    "GetQuizToPlay": [
        {name: "CompanyQuiz/GetQuizToPlay"}
    ],
    "SubmitPlayQuiz": [
        {name: "CompanyQuiz/SubmitQuiz"}
    ],
    "GetCompanyInfo": [
        {name: "userinformation/GetCompanyInfo"}
    ],
    "UpdateCompany": [
        {name: "userinformation/UpdateCompanyProfile"}
    ],
    "GetBuildPlaylistSavedState": [
        {name: "BuildPlaylist/GetBuildPlaylistSavedState"}
    ],
    "GetMyCreatedPlaylist": [
        {name: "MyPlaylist/GetMyCreatedPlaylist"}
    ],
    "GetPlaylistAssociatedTeams": [
        {name: "MyPlaylist/GetPlaylistAssociatedTeams"}
    ],
    "AssociatedUsers": [
        {name: "MyPlaylist/AssociatedUsers"}
    ],
    "GetAllDashPlaylists": [
        {name: "MyPlaylist/GetMyDashboardPlaylist"}
    ],
    "UpdatePlaylist": [
        {name: "BuildPlaylist/UpdatePlaylist"}
    ],
    "DeleteContent": [
        {name: "companycontent/DeleteCompanyContent"}
    ],
    "DeletePlaylist": [
        {name: "MyPlaylist/DeletePlaylist"}
    ],
    "ClonePlaylist": [
        {name: "MyPlaylist/ClonePlaylist"}
    ],
    "CreateContest": [
        {name: "VBPlay/CreateContest"}
    ],
    "BroadcastCompanyContent": [
        {name: "CompanyContent/BroadcastCompanyContent"}
    ],
    "BroadcastContest": [
        {name: "VBPlay/BroadcastContest"}
    ],
    "GetAllVBPlayContest": [
        {name: "VBPlay/GetAllVBPlayContest"}
    ],
    "GetAllNotifications": [
        {name: "Account/GetNotifications"}
    ],
    "ReadNotifications": [
        {name: "Account/ReadNotifications"}
    ],
    "JoinContest": [
        {name: "VBPlay/JoinContest"}
    ],
    "LikeContest": [
        {name: "VBPlay/LikeContest"}
    ],
    "ContestContentViews": [
        {name: "VBPlay/ContestContentViews"}
    ],
    "GetBroadcastedContent":[
        {name: "VBPlay/GetBroadcastedContent"}
    ],
    "SubmitContestEntry": [
        {name: "VBPlay/SubmitContestEntry"}
    ],
    "ViewContest": [
        {name: "VBPlay/ViewContest"}
    ],
    "UpdateContestEntry": [
        {name: "VBPlay/UpdateContestEntry"}
    ],
    "GetSharedContent": [
        {name: "VBPlay/GetSharedContent"}
    ],
    "GetEarnedPoints": [
        {name: "VBPlay/GetEarnedPoints"}
    ],
    "DeleteContest": [
        {name: "VBPlay/DeleteContest"}
    ],
    "ExtendContentDate":[
        {name:"VBPlay/ExtendContestDate"}
    ],
    "OptOutNotification":[
        {name:"VBPlay/OptOutNotification"}
    ],
    "RenewToken":[
        {name:"account/RenewToken"}
    ],
    "GetPrizeCategories":[
        {name:"PrizeStore/GetPrizeCategories"}
    ],
    "CreatePrize":[
        {name:"PrizeStore/CreatePrize"}
    ],
    "GetPrizeStore":[
        {name:"PrizeStore/GetPrizeStore"}
    ],
    "GetMyCreatedPrize":[
        {name:"PrizeStore/GetMyCreatedPrize"}
    ],
    "UpdatePrize":[
        {name:"PrizeStore/UpdatePrize"}
    ],
    "UpdatePrizeStatus":[
        {name:"PrizeStore/UpdatePrizeStatus"}
    ],
    "AvailPrize":[
        {name:"PrizeStore/AvailPrize"}
    ],
    "DeletePrize":[
        {name:"PrizeStore/DeletePrize"}
    ],
    "GetAllPlaylist":[
        {name:"Playlist/GetAllPlaylist"}
    ],
    "GetPlaylistLeaderboard":[
        {name:"Playlist/GetPlaylistLeaderboard"}
    ],
    "GetPlaylistLeaderboardByTeam":[
        {name:"Playlist/GetPlaylistLeaderboardByTeam"}
    ],
    "MyPlan":[
        {name:"account/MyPlan"}
    ],
    "UpdateBillingAddress":[
        {name:"account/UpdateBillingAddress"}
    ],
    "ChangeSubscriptionPlan":[
        {name:"account/ChangeSubscriptionPlan"}
    ],
    "GetCreateCardCheckoutSession":[
        {name:"account/GetCreateCardCheckoutSession"}
    ],
    "UpdatePaymentMethodSuccess":[
        {name:"account/UpdatePaymentMethodSuccess"}
    ],
    "UpdatePaymentMethodFailure":[
        {name:"account/UpdatePaymentMethodFailure"}
    ],
    "CancelSubscriptionPlan":[
    {name:"account/CancelSubscriptionPlan"}
    ],
    "UpdateCompanyContentVisibility":[
        {name:"CompanyContent/UpdateCompanyContentVisibility"}
    ],
    "UpdateVbPlayContentVisibility":[
        {name:"VBPlay/UpdateVbPlayContentVisibility"}
    ],
    "ManageLocation": {
        getLocationList: "userinformation/getlocations",
        GetSignUpLocations : "userinformation/GetSignUpLocations",
        deleteLocation: "userinformation/DeleteLocation",
        addEditLocation: "userinformation/locations"
    },
    "ReportedVideo":[
        {
            report:"VBPlay/ReportAbuse",
        }
    ],
    "Mail": [
        {
            mailToKnow: "userinformation/WriteToUs"
        }
    ]
}
