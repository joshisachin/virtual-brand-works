import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderpreComponent } from '../views/elements/headerpre/headerpre.component';
import { PlansComponent } from '../views/elements/plans/plans.component';
import { ChangePlanComponent } from './modals/change-plan/change-plan.component';
import { PaymentComponent } from '../views/elements/payment/payment.component';
import { FolderComponent } from '../views/elements/folder/folder.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FolderFooterComponent } from '../views/elements/folder-footer/folder-footer.component';
import { FolderDetailsComponent } from './modals/folder-details/folder-details.component';
import { HeaderComponent } from '../views/elements/header/header.component';
import { HeaderSecondComponent } from '../views/elements/header-second/header-second.component';
import { SidemenuComponent } from '../views/elements/sidemenu/sidemenu.component';
import { NgDynamicBreadcrumbModule } from "ng-dynamic-breadcrumb";
import { RouterModule } from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TeamCommonComponent } from '../views/elements/team-common/team-common.component';
import { TeamOptionsComponent } from '../views/elements/team-options/team-options.component';
import { ConfirmationAlertComponent } from './modals/confirmation/confirmation-alert.component';
import { AlertComponent } from './modals/alert/alert.component';
import { LocationSelectorComponent } from '../views/elements/location-selector/location-selector.component';
import { CompanyContentService } from '../views/company-content/company-content.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FolderDetailsCommonComponent } from '../views/elements/folder-details-common/folder-details-common.component';
import { TagsComponent } from '../views/elements/tags/tags.component';
import { HandleTagsComponent } from '../views/elements/handle-tags/handle-tags.component';
import { ContentDetailsCommonComponent } from '../views/elements/content-details-common/content-details-common.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContentTypeBasedComponent } from '../views/elements/content-type-based/content-type-based.component';
import { PlaylistComponent } from '../views/elements/playlist/playlist-list/playlist-list.component';
import { CreateQuizComponent } from './modals/quiz/create-quiz/create-quiz.component';
import { AddQuestionsComponent } from './modals/quiz/add-questions/add-questions.component';
import { ViewQuizComponent } from './modals/quiz/view-quiz/view-quiz.component';
import { UpdateQuestionComponent } from './modals/quiz/update-question/update-question.component';
import { CreateComponent } from './modals/playlist/create/create.component';
import { PlaylistFoldersComponent } from '../views/elements/playlist-folders/playlist-folders.component';
import { PlaylistDetailViewComponent } from '../views/elements/playlist/playlist-detail-view/playlist-detail-view.component';
import { TopPerformerComponent } from '../views/elements/playlist/top-performer/top-performer.component';
import { PlayQuizComponent } from './modals/quiz/play-quiz/play-quiz.component';
import { ResultComponent } from './modals/quiz/result/result.component';
import { PlaylistContentDetailComponent } from '../views/elements/playlist/playlist-content-detail/playlist-content-detail.component';
import { PointsLevelComponent } from './modals/points-level/points-level.component';
import { ViewResultComponent } from './modals/playlist/view-result/view-result.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ViewQuizResultComponent } from './modals/quiz/view-quiz-result/view-quiz-result.component';
import { BuildPlaylistService } from '../views/build-playlist/build-playlist.service';
import { SearchPipe } from './pipes/search.pipe';
import { RenameComponent } from './modals/playlist/rename/rename.component';
import { FilterPipe } from './pipes/filter.pipe';
import { MessagingService } from './services/messaging/messaging.service'; 
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { environment } from 'src/environments/environment';
import { UploadDirective } from './directive/upload.directive';
import { SocialShareComponent } from '../views/elements/social-share/social-share.component';
import { ViewContentComponent } from '../views/elements/view-content/view-content.component';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import { UpdateBillingComponent } from './modals/update-billing/update-billing.component';
import { CancelPlanComponent } from '../views/elements/cancel-plan/cancel-plan.component';
import { PlansNewComponent } from '../views/elements/plans-new/plans-new.component';


@NgModule({
  declarations: [
    HeaderpreComponent,
    PlansComponent,
    PlansNewComponent,
    ChangePlanComponent,
    ConfirmationAlertComponent,
    AlertComponent,
    PaymentComponent,
    FolderComponent,
    FolderFooterComponent,
    FolderDetailsComponent,
    HeaderComponent,
    HeaderSecondComponent,
    SidemenuComponent,
    TeamCommonComponent,
    TeamOptionsComponent,
    LocationSelectorComponent,
    FolderDetailsCommonComponent,
    TagsComponent,
    HandleTagsComponent,
    ContentDetailsCommonComponent,
    ContentTypeBasedComponent,
    PlaylistDetailViewComponent,
    PlaylistComponent,
    TopPerformerComponent,
    CreateQuizComponent,
    AddQuestionsComponent,
    ViewQuizComponent,
    UpdateQuestionComponent,
    CreateComponent,
    PlaylistFoldersComponent,
    PlayQuizComponent,
    ResultComponent,
    ViewResultComponent,
    PlaylistContentDetailComponent,
    PointsLevelComponent,
    ViewQuizResultComponent,
    SearchPipe,
    RenameComponent,
    FilterPipe,
    UploadDirective,
    SocialShareComponent,
    ViewContentComponent,
    SanitizeHtmlPipe,
    UpdateBillingComponent,
    CancelPlanComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    NgDynamicBreadcrumbModule,
    RouterModule,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgCircleProgressModule.forRoot({
    }),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  exports: [
    HeaderpreComponent,
    PlansComponent,
    PlansNewComponent,
    ChangePlanComponent,
    ConfirmationAlertComponent,
    AlertComponent,
    PaymentComponent,
    FolderComponent,
    FolderFooterComponent,
    FolderDetailsComponent,
    HeaderComponent,
    HeaderSecondComponent,
    SidemenuComponent,
    TeamCommonComponent,
    TeamOptionsComponent,
    LocationSelectorComponent,
    FolderDetailsCommonComponent,
    TagsComponent,
    HandleTagsComponent,
    ContentDetailsCommonComponent,
    ContentTypeBasedComponent,
    PlaylistDetailViewComponent,
    PlaylistComponent,
    TopPerformerComponent,
    CreateQuizComponent,
    PlaylistFoldersComponent,
    ViewResultComponent,
    PlaylistContentDetailComponent,
    SearchPipe,
    RenameComponent,
    FilterPipe,
    UploadDirective,
    SocialShareComponent,
    ViewContentComponent
  ],
  providers: [CompanyContentService, BuildPlaylistService, MessagingService, NgbActiveModal]
})
export class SharedModule { }
