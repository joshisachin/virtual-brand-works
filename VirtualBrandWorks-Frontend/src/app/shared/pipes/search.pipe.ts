import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPipe'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args: string): any {
    if (args == "") {
      return value;
    }
    if (value) {
      return value.filter((item) => {
        let name = item.FirstName + ' ' + item.LastName;
        return JSON.stringify(name).toLowerCase().includes(args);
      });
    }
  }

}
