import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPipe'
})
export class FilterPipe implements PipeTransform {

  transform(value: any[], args: any, filterParam: string): any {
    if (args === "") {
      return value;
    }
    if (value) {
      return value.filter(item => item[filterParam] == args);
    }
  }

}
