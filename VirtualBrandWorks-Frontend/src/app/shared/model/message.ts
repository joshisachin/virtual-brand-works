export interface message {
    PlayListId?: number,
    SenderId?: number,
    ReceiverId?: number,
    Date?:string,
    SenderName?:string,
    ReceiverName?:string,
    SenderImage?:string,
    ReceiverImage?:string,
    MessageType?:number,
    Message?:string,
    PageIndex?:number,
    PageSize?:number,
    MessageId?:number
}