export interface PlaylistDetail {
    Name?: string,
    StartDate?: string,
    EndDate?: string,
    Location?: string,
    UserId?: number,
    RoleId?: number,
    CreatedDate?: string,
    ModifiedDate?: string
}
