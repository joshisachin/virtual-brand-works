export interface TeamUser {
    UserId?: number,
    ProfilePicture?: string,
    FirstName?: string,
    LastName?: string,
    IsSelected?: boolean
    TagList: any
}
