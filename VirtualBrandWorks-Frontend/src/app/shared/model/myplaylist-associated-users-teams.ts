export interface MyPlaylistAssociatedUsersTeams {
    Badges?: string,
    NumnberOfUsers?:number,
    TeamId?:number,
    TeamName?: string,
    TrainerFirstName?: string,
    TrainerLastName?: string,
    PlaylistUsers?:PlaylistUsers[]
}

export interface PlaylistUsers {
    UserId?: number,
    TotalScore?: number,
    ProfilePicture?: string,
    FirstName?:string,
    LastName?: string,
    CompletedPercentage?:number,
    FolderResult?:FolderResult[]
}

export interface FolderResult {
    Badge?:number,
    MarksObtained?:string,
    Name?:string,
    PlaylistFolderId?:number,
    QuizCompletedCount?:number,
    QuizCount?:number,
    ShortName?:string
}
