export interface MyProfile {
    FirstName?: string,
    LastName?: string,
    Description?: string,
    EmailId?: string,
    AddressOne?: string,
    AddressTwo?: string,
    Country?: string,
    State?: string,
    City?: string,
    Zip?: string,
    PhoneNumber?: number,
    ProfilePicture?: string,
    DateOfJoining?:string,
    Tags: [{
        TagId?: number,
        Name?: string,
        ShortName?: string
    }],
    Status?: boolean,
    CityId?: number,
    StateId?: number,
    CountryId?: number,
    UserId?: number,
    RoleId?: number,
    CreatedBy?: string,
    ModifiedBy?: string,
    CreatedDate?: string,
    ModifiedDate?: string,
    CompanyLogo?: string,
    AboutCompany?: string

}
    