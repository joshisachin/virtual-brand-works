export interface Industry {
    CategoryId?: number,
    CategoryName?: string,
    Description?: string
}
