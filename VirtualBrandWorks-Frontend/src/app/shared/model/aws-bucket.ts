export interface AwsBucket {
    Signature?: string,
    Policy?: string,
    FormAction?: string,
    FileId?: string
}
