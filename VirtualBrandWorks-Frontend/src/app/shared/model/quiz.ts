export interface Quiz {
    QuizId?: number,
    QuizName?: string,
    TotalQuestion?: number,
    CorrectCount?: number,
    IncorrectCount?: number,
    MarksObtained?: number
    QuestionAnswers?: [{
        Question: {
            QuestionId: number,
            Question: string
        },
        Answers: [{
            AnswerId: number,
            Answer: string,
            IsRightAnswer: boolean
        }]
    }]
}
