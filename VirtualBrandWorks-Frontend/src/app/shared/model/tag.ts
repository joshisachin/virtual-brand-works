export interface Tag {
    TagId?: number,
    Name?: String,
    ShortName?: string
}
