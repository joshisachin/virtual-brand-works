export interface PlaylistFolder {
    PlayListFolderId?: number,
    PlayListId?: number,
    PlaylistName?: string,
    FolderId?: number,
    FolderName?: string,
    FolderShortName?: string,
    NumberOfContent?: number,
    IsMasterLibraryFolder?: boolean,
    SortOrder?: number
    UserId?: number,
    PlaylistFolderCompletePercentage?: number
}