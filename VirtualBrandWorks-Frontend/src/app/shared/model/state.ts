export interface State {
    CountryID?: number,
    ID?: number,
    Name?: string,
    StateCode?: string
}
