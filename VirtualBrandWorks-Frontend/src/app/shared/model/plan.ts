export interface Plan {
    CostForAnnualBilling?: string,
    CostForMonthlyBilling?: string,
    MonthlyPlanId?: number,
    NumberOfUsers?: number,
    PlanDescription?: string,
    PlanId?: string,
    PlanName?: string,
    PlanType?: number,
    Price?: number,
    ProductId?: number,
    Status?: boolean,
    SubscriptionId?: number,
    SubscriptionPlanPrice?: number,
    SubscriptionPlanPriceId?: number,
    YearlyPlanId?: number
}
