export interface Notification {
    NotificationId?: number,
    UserId?:number,
    Tiele?:string,
    Body?:string,
    IsRead?:boolean
}