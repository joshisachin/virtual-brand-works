export interface ConfirmContent {
    CompanyLibraryContentId?: number,
    ContestId: number,
    FolderId?: number,
    FolderName?: string,
    Name?: string,
    ShortName?: string,
    ContentUrl?: any,
    Description?: string,
    Tags: [{
        TagId?: number,
        Name?: string,
        ShortName?: string
    }],
    IsPublished?: boolean,
    IsBroadcasted?: boolean,
    ThumbnailUrl?: string,
    IsFacebookContent?: boolean,
    IsYoutubeContent?: boolean,
    IsAwsContent?: boolean,
    ContentURLWithExtension?: string,
    MimeType?: string,
    UserId?: number,
    RoleId?: number,
    CreatedBy?: string,
    ModifiedBy?: string,
    CreatedDate?: string,
    EndDate?: string,
    ModifiedDate?: string,
    IsContestEntry?: boolean
}
    