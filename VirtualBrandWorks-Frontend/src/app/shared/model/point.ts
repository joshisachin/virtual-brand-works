export interface Point {
    NumberOfContestPlayed?: number,
    NumberOfContestCreated?: number,
    ContestCreatedEarnedPoints?: number,
    ContestPlayedEarnedPoints?: number,
    TotalEarnedPoints?: number,
    PointsRedeemed?: number,
    BalancePoints?: number,
    PointsHistory?: {
        PointsTransactionId?: number,
        PlaylistId?: number,
        ContestId?: number,
        UserId?: number,
        Contest?: string,
        Points?: number,
        CreatedDate?: string,
        Balance?: number,
        IsEarned?: boolean,
        Type?: string
    }
}
