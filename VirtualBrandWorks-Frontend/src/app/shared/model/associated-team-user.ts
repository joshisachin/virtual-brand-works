export interface AssociatedTeam {
    TeamId?: number,
    Name?: string,
    NumberOfUsers?: number,
    CreatedDate?: string,
    Status?: boolean
}

export interface AssociatedUser {
    UserId?: number,
    FirstName?: string,
    LastName?: string,
    TeamName?: string,
    AssociatedPlaylist?: [],
    Status?: boolean
}