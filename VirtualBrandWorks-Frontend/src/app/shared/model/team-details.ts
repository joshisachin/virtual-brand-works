export interface TeamDetails {
    TeamId?: number,
    CompanyId?: number,
    TeamName?: string,
    HandleName?: string,
    NumberOfUsers?: number,
    AssociatedPlayList?: string,
    TrainerFirstName?: string,
    TrainerLastName?: string,
    Tags?: [{
        TagId?: number,
        Name?: string
        ShortName?: string
    }],
    Users: [{
        UserId?: number,
        CompanyId?: number,
        FirstName?: string,
        LastName?: string,
        ProfilePicture?: string,
        Email?: string,
        Status?: boolean,
        ContactNo?: string,
        Location?: string,
        Score?: number,
        CourseCompleted?: number,
        HandleName?: string
    }]
}
