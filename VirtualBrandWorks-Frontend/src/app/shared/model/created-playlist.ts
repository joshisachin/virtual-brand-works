export interface PlayListData {
    PlaylistId?: number,
    Name?: string,
    StartDate?: string,
    EndDate?: string,
    NumberOfUsers?: number,
    NumberOfTeams?: number,
    Status?: boolean,
    MessageCount?: boolean,
    PlaylistCompletePercentage?: number,
    Selected?: boolean,
    Description?: string,
    IsExpired?: boolean,
    Folders: [
        {
          PlayListFolderId?: number,
          PlayListId?: number,
          FolderId?: number,
          FolderName?: string,
          FolderShortName?: string,
          NumberOfContent?: number,
          IsMasterLibraryFolder?: boolean,
          SortOrder?: number,
          UserId?: number,
          PlaylistFolderCompletePercentage?: number
        }
    ],
    AssignedTo?: any
}
    