export interface Location {
    ID?: number,
    Name?: string,
    StateID?: number
}
