export interface PlaylistContentResultList {
    Data?: [
      {
        PlaylistContentMasterId?: number,
        IsUserPassed?: any,
        MarksObtained?: any,
        NumberOfQuizAttempts?: number,
        IsTurnedOffExistingQuiz?: boolean,
        Badge?: number,
        Content?: {
            ContentId?: number,
            ContentName?: string,
            IsQuestionAdded?: boolean,
            IsTurnOffExistingQuiz?: boolean,
            CreatedDate?: string,
            ModifiedDate?: string,
            ShortName?: string,
            QuizName?: string,
            QuizId?: number,
            IsAllQuestionAdded?: boolean,
            ContentURL?: string,
            ThumbnailURL?: string,
            FolderName?: string,
            FolderId?: number,
            ContentURLWithExtension?: string,
            IsSelectedFolder?: boolean,
            ContentDescription?: string,
            ViewCount?: number,
            SortOrder?: number,
            IsContentSaved?: boolean,
            IsPublished?: boolean,
            IsBroadcasted?: boolean,
            MimeType?: string
        }
      }
    ]
}