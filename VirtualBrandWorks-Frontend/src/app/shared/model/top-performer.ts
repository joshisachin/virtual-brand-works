export interface TopPerformer {
    UserId?: number,
    ProfilePicture?: string,
    FirstName?: string,
    LastName?: string,
    Points?: number
}