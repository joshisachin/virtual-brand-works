export interface MyPlan {
    SubscriptionName?: string,
    BillingCycle?: string,
    PlanType?: number,
    UsersUpTo?: number,
    ValidTill?: string,
    ActiveFrom?: string,
    BilledAmount?: number,
    StripeSubscriptionId?: string,
    CompanySubscriptionId?: number,
    IsCancelled?: boolean,
    PaymentCard?: {
            CardType?: string,
            ExpMonth?: number,
            ExpYear?: number,
            Last4Digit?: string,
            PaymentMethodId?: string
            BillingDetails?: {
                address?: {
                    city?: string,
                    country?: string,
                    line1?: string,
                    line2?: string
                    postal_code?: string,
                    state?: string
                },
                email?: string,
                name?: string,
                phone?: string
            },
            IsDefault: boolean
        }
}
