export interface Trainer {
    CompanyId?: number,
    DisplayName?: string,
    FirstName?: string,
    LastName?: string,
    TrainerId?: number
}
