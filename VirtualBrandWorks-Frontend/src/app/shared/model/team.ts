export interface Team {
    IsDeleted?: boolean,
    Name?: String,
    TeamId?: number,
    IsSelected?:boolean,
    TeamName?:String
    TagList?: any
}
