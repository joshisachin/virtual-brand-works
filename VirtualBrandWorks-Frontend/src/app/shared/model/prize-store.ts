export interface PrizeStore {
    PrizeId?: number,
    PrizeCategoryId?: number,
    Title?: string,
    Description?: string,
    Points?: number,
    ThumbnailImage?: string,
    EndDate?: any,
    IsActive?: boolean,
    AvailableCount?: number,
    Remarks?: string,
    StartDate?: any,
    CanDelete?: boolean
}
