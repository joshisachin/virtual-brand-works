export interface PlaylistFolderContentResult {
    TotalPercentage?: number,
    TotalScore?: number,
    Badges?: any,
    folders?: [
      {
        PlayListFolderId?: number,
        PlayListId?: number,
        FolderId?: number,
        FolderName?: string,
        FolderShortName? : string,
        NumberOfContent?: number,
        IsMasterLibraryFolder?: boolean,
        SortOrder?: number,
        UserId?: number,
        PlaylistFolderCompletePercentage?:number
      }
    ]
}