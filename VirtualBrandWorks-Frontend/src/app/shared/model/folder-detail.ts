export interface FolderDetail {
    FolderId?: number,
    FolderName?: string,
    NoOfVideo?: number,
    NoOfImages?: number,
    NoOfDocs?: number,
    IsSelectedFolder?: boolean,
    ContentList: [{ContentName: string, ContentURL: string}]
}
