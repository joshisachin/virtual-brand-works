export interface Result {
    QuizId?: number,
    PlaylistId?: number,
    UserId?: number,
    IsMasterQuiz?: boolean,
    IsUserPassed?: boolean,
    NumberOfRightAnswer?: number,
    NumberOfWrongAnswer?: number,
    MarksObtained?: number,
    TotalMarks?: number
}
