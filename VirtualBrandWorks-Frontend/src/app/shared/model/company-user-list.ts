export interface CompanyUserList {
    UserId?: number,
    CompanyId?: number,
    RoleId?: number,
    FirstName?: string,
    LastName?: string,
    ProfilePicture?: string,
    Email?: string,
    Status?: boolean,
    ContactNo?: number,
    Location?: string,
    TotalStudent?: number,
    TotalTeam?: number,
    AssignedPlayList?: number
}
