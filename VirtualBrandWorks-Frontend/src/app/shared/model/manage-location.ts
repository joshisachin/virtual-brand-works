export interface ManageLocation {
    LocationId?: number,
    StateId?: number,
    CityId?: number,
    LocationName?: string,
    TrainerId?: number,
    AdminId?: number,
    CountryId?: number,
    IsAdmin?: boolean
}
    