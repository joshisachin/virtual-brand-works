export interface Country {
    CountryCode?: string,
    ID?: number,
    Name?: string
}
