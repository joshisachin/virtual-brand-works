import { PlaylistFolder } from './playlist-folder';
import { PlaylistContentResultList } from './playlist-content-list';

export interface MyPlaylistFolderDetail {
    Folder?: PlaylistFolder,
    PlaylistContent?: PlaylistContentResultList,
    StartDate?: string,
    EndDate?: string
}