export interface Leaderboard {
    TeamId?: number,
    UserId?: number,
    FirstName?: string,
    LastName?: string,
    Name?: string,
    PlaylistCompletePercentage?: number,
    Color?: string
}
