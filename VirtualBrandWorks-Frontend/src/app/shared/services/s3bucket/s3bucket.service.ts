import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { API_NAME } from '../../constants/apiname/apiname';
import { CommonService } from '../commonservices/commonservices.service';
import { Observable } from 'rxjs';
import { API_MESSAGES } from '../../constants/message/message';
import { AwsBucket } from '../../model/aws-bucket';

@Injectable({
  providedIn: 'root'
})
export class S3bucketService {
  httpNew: HttpClient; // for aws
  constructor(private http: HttpClient, private commonServices: CommonService, private handler: HttpBackend) {
    this.httpNew = new HttpClient(handler);
  }

  /**
   * get aws policy signature
   */
  getAwsPolicySignature(fileType: string): Observable<any> {
    return this.http.get(API_NAME.GetAwss3PolicyAndSignature[0].name + "?fileType=" + fileType);
  }

  /**
   * perform upload
   * @param file, @param validFileTypes
   * @response null
   */
  performUpload(file: any, validFileTypes: any): Promise<any> {
    console.log(file);
    return new Promise((resolve, reject) => {
      let fileType = this.getFileType(file["name"]); // get the file type
      this.validateFileType(fileType, validFileTypes).then(res => { // validate the file type
        this.getAwsPolicySignature(fileType).subscribe(data => {
          let awsRes: AwsBucket = data.Data;
          let fileName: string = awsRes.FileId;
          if (data.Status) {
            this.commonServices.loader.next(true);
            this.uploadFileToAws(file, awsRes).subscribe(data => {
              if (data) {
                reject(true);
              }
              else {
                resolve(fileName);
              }
            });
          }
          else {
            this.commonServices.showError(data.Message, "");
            reject(true);
          }
        });
      })
      .catch(err => {
        this.commonServices.showError(err, "");
        reject(true);
      });
    });
  }

  /**
   *  upload file to aws
   *  @param file
   *  @response null
   */
  uploadFileToAws(file: any, data: any): Observable<any> {
    const formData = new FormData();

    formData.append("key", data.FileId);
    formData.append("AWSAccessKeyId", "AKIAZVGVOTQUCA2RJAEK");
    formData.append("acl", "private");
    formData.append("policy", data.Policy);
    formData.append("signature", data.Signature);
    formData.append("file", file);
    return this.httpNew.post(data.FormAction, formData);
  }

  /**
   * get file type
   * @param fileName
   * @response string
   */
  getFileType(fileName: string) {
    return fileName.substr(fileName.lastIndexOf(".")+1);
  }

  /**
   * validate file types
   * @param fileType
   * @param validFileTypes
   */
  validateFileType(fileType: string, validFileTypes: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let validType = validFileTypes.indexOf(fileType.toLowerCase());
      if (validType == -1) {
        reject(API_MESSAGES.FileUpload.fileType);
      }
      resolve(true);
    });
  }
}
