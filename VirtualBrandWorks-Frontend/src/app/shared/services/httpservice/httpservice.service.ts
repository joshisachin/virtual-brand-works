import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpBackend, HttpClient, HttpParams } from '@angular/common/http';
import { API_NAME } from '../../constants/apiname/apiname';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  apiName = API_NAME;
  httpNew: HttpClient; // for ip
  constructor(private http: HttpClient, private handler: HttpBackend) {
    this.httpNew = new HttpClient(handler);
  }

  /**
   * login
   * @param loginData
   */
  login(loginData, fcmToken): Observable<any> {
    let body = new URLSearchParams();
    body.set('Email', loginData.Email);
    body.set('Password', loginData.Password);
    body.set("FCMToken", fcmToken);
    return this.http.post(this.apiName.Login[0].name, body.toString());
  }

  /**
   * social login
   * @param provider
   * @param accessToken
   */
  socialLogin(provider: string, accessToken: string): Observable<any> {
    return this.http.post(this.apiName.ExternalLogin[0].name, { "Provider": provider, "AccessToken": accessToken });
  }

  /**
   * forgot password
   * @param formData
   */
  forgotPassword(formData: any): Observable<any> {
    return this.http.post(this.apiName.ForgotPassword[0].name, { "EmailId": formData.Email });
  }

  /**
   * reset password
   * @param formData
   */
  resetPassword(formData: any): Observable<any> {
    return this.http.post(this.apiName.ResetPassword[0].name, formData);
  }

  /**
   * get countries
   * @param null
   */
  getCountries(): Observable<any> {
    return this.http.get(this.apiName.GetCountries[0].name);
  }

  /**
   * get states
   * @param countryId
   */
  getStates(countryId: number): Observable<any> {
    return this.http.post(this.apiName.GetStates[0].name , { "CountryID": countryId});
  }

  /**
   * get cities
   * @param stateId
   */
  getCities(stateId: number): Observable<any> {
    return this.http.post(this.apiName.GetCities[0].name, { "StateId": stateId});
  }

  /**
   * get industries
   */
  getIndustries(): Observable<any> {
    return this.http.get(this.apiName.GetIndustries[0].name);
  }

  /**
   * get plans
   */
  getPlans(noTrial: boolean = false): Observable<any> {
    return this.http.get(this.apiName.GetSubscriptionPlan[0].name + "?noTrial=" + noTrial);
  }

  /**
   * signup
   * @param data
   */
  signup(data: any): Observable<any> {
    return this.http.post(this.apiName.SignUpAdmin[0].name, data);
  }

  /**
   * get session id for payment
   * @param paymentData
   */
  getSessionIdForPayment(paymentData: any): Observable<any> {
    return this.http.post(this.apiName.GetCheckoutSessionId[0].name, paymentData);
  }

  /**
   * update status success
   * @param sessionId
   */
  successUpdate(sessionId: string): Observable<any> {
    return this.http.post(this.apiName.PaymentSuccess[0].name, "'" + sessionId + "'");
  }

  /**
   * update status cancel
   * @param sessionId
   */
  cancelUpdate(sessionId: string): Observable<any> {
    return this.http.post(this.apiName.CancelPayment[0].name, "'" + sessionId + "'");
  }

  /**
   * check email and company
   * @param email
   * @param company
   */
  checkEmailAndCompany(email: string, company: string): Observable<any> {
    return this.http.post(this.apiName.ValidateEmailAndCompanyDetails[0].name, { "Email": email, "CompanyName": company })
  }

  /**
   * get user info
   * @param email
   */
  getUserInfo(email: string): Observable<any> {
    return this.http.post(this.apiName.GetUserInfo[0].name, { "EncodedEmail": email })
  }

  /**
   * signup as user
   * @param data
   */
  signupAsUser(data: any): Observable<any> {
    return this.http.post(this.apiName.SignUpUser[0].name, data);
  }

  /**
   * write to us
   * @param data
   */
  writeToUs(data: any): Observable<any> {
    return this.http.post(this.apiName.WriteToUs[0].name, data);
  }

  /**
   * get all folder data
   * @param searchText
   * @param sortyBy
   * @param pageIndex
   * @param pageSize
   */
  getAllFoldersData(searchText: string = "", sortyBy: string = "", pageIndex: number = 1, pageSize: number = 20): Observable<any> {
    let data = { "SortBy": sortyBy, "SearchText": searchText, "PageIndex": pageIndex, "PageSize": pageSize }
    return this.http.post(this.apiName.GetAllFolderData[0].name, data);
  }

  /**
   * get folder details
   * @param folderId
   */
  getFolderDetails(folderId: number): Observable<any> {
    return this.http.get(this.apiName.GetFolderDetail[0].name + "?FolderId=" + folderId);
  }

  /**
   * save selected folder
   * @param folderId
   * @param isAddRequest
   */
  saveSelectedFolder(folderId: number, isAddRequest: boolean): Observable<any> {
    return this.http.post(this.apiName.SaveSelectedFolder[0].name, { "FolderId": folderId, "IsAddRequest": isAddRequest });
  }

  /**
   * get selected folders
   * @param null
   */
  getSelectedFolders(): Observable<any> {
    return this.http.get(this.apiName.GetSelectedFolder[0].name);
  }

  /**
   * get location
   * @param searchString
   */
  getLocation(searchString: string = ""): Observable<any> {
    return this.http.post(this.apiName.GetLocation[0].name, { "Name": searchString });
  }

  /**
   * save trial payment
   */
  saveTrialPayment(email: string, firstName: string, lastName: string): Observable<any> {
    return this.http.post(this.apiName.SaveTrialPayment[0].name, { "Email": email, "FirstName": firstName, "LastName": lastName })
  }

  /**
   * get company content folder
   */
  getCompanyContentFolder(searchText: string = "", sortyBy: string = "", pageIndex: number = 1, pageSize: number = 20): Observable<any> {
    let data = { "SortBy": sortyBy, "SearchText": searchText, "PageIndex": pageIndex, "PageSize": pageSize }
    return this.http.post(this.apiName.GetCompanyContentFolders[0].name, data);
  }

  /**
   * get company content selected folder
   */
  getCompanyContentSelectedFolder(): Observable<any> {
    return this.http.get(this.apiName.GetCompanyContentSelectedFolders[0].name);
  }

  /**
   * save company selected folder
   * @param folderId
   * @param isAddRequest
   */
  saveCompanySelectedFolder(folderId: number, isAddRequest: boolean): Observable<any> {
    return this.http.post(this.apiName.SaveCompanySelectedFolder[0].name, { "FolderId": folderId, "IsAddRequest": isAddRequest });
  }

  /**
   * get company content
   */
  getCompanyFolderContent(folderId: number, searchText: string): Observable<any> {
    return this.http.post(API_NAME.GetCompanyFolderContent[0].name, { "FolderId": folderId, "SearchText": searchText });
  }

  /**
   * get company content
   */
  getMasterFolderContent(folderId: number, searchText: string): Observable<any> {
    return this.http.post(API_NAME.GetMasterFolderContent[0].name, { "FolderId": folderId, "SearchText": searchText });
  }

  /**
   * get tag suggestion
   */
  GetTagSuggestion(tag: string,type): Observable<any> {
    return this.http.get(API_NAME.GetTagSuggestions[0].name + "?Suggestion=" + tag + '&Type=' + type);
  }

  /**
   * get company content details
   */
  getCompanyContentDetails(contentId: number): Observable<any> {
    return this.http.post(API_NAME.CompanyContentDetails[0].name, { "ContentId": contentId});
  }

  /**
   * get company content by type
   */
  getCompanyContentByType(folderId: number, contentId: number, contentType: string, searchText: string = ""): Observable<any> {
    return this.http.post(API_NAME.CompanyContentByType[0].name, { "FolderId": folderId, "ContentId": contentId, "ContentType": contentType, "SearchText": searchText });
  }

  /**
   * get master content details
   */
  getMasterContentDetails(contentId: number): Observable<any> {
    return this.http.post(API_NAME.MasterContentDetails[0].name, { "ContentId": contentId});
  }

  /**
   * get master content by type
   */
  getMasterContentByType(folderId: number, contentId: number, contentType: string, searchText: string = ""): Observable<any> {
    return this.http.post(API_NAME.MasterContentByType[0].name, { "FolderId": folderId, "ContentId": contentId, "ContentType": contentType, "SearchText": searchText });
  }

  /**
   * get playlist folders footer data
   */
  getPlaylistFoldersFooterData(): Observable<any> {
    return this.http.get(API_NAME.GetPlaylistFooterData[0].name);
  }

  /**
   * get all notifications
   */
  getNotifications(userId): Observable<any> {
    return this.http.get(API_NAME.GetAllNotifications[0].name + "?userId=" + userId);
  }

  /**
   * update notifications
   */
  readNotifications(userId): Observable<any> {
    return this.http.get(API_NAME.ReadNotifications[0].name+"?userId="+userId);
  }

  /**
   * update notifications
   */
  getContent(contentId: number, contentType: number): Observable<any> {
    return this.http.get(API_NAME.GetSharedContent[0].name+"?contentId=" + contentId + "&contentType=" + contentType);
  }

  /**
   * Update content extend date
   */
  extendContentDate(contestId: number, notificationId: number,type:string): Observable<any> {
    return this.http.post(API_NAME.ExtendContentDate[0].name+"?contestId="+contestId+"&notificationId="+notificationId+"&type="+type,{});
  }
  /**
   * OptOut Notifications
   */
  OptOutNotifications(notificationId: number): Observable<any> {
    return this.http.post(API_NAME.OptOutNotification[0].name+"?notificationId="+notificationId,{});
  }

  /**
   * renew token
   */
  renewToken(): Observable<any> {
    return this.http.get<any>(API_NAME.RenewToken[0].name);
  }

  /**
   * update address
   */
  updateAddress(formData: any): Observable<any> {
    return this.http.post(API_NAME.UpdateBillingAddress[0].name, formData);
  }

  /**
   * update payment change
   */
  updatePaymentChange(sessionId: string): Observable<any> {
    return this.http.post(API_NAME.UpdatePaymentMethodSuccess[0].name, "'" + sessionId + "'");
  }

  /**
   * update payment change
   */
  updatePaymentChangeFailure(sessionId: string): Observable<any> {
    return this.http.post(API_NAME.UpdatePaymentMethodFailure[0].name, "'" + sessionId + "'");
  }

  /**
   * cancel plan
   */
  cancelPlan(planId: string): Observable<any> {
    return this.http.post(API_NAME.CancelSubscriptionPlan[0].name, {"PlanId": planId});
  }

  mail(email: string, message: string): Observable<any>{
    return this.http.post(API_NAME.Mail[0].mailToKnow, {"email": email, "message": message});
  }
}