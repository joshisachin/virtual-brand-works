import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionGuard implements CanActivate {
  token: string = "";
  constructor( private router: Router,
    private cookieService: CookieService) { }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.token = this.cookieService.get("token");
      if (this.token) {
        this.router.navigate(['/']);
        return true;
      } else {
        return true;
      }
  }
  
}
