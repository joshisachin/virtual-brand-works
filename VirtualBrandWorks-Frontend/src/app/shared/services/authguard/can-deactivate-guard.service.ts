import { Observable } from 'rxjs';
import { CanDeactivate } from '@angular/router';
import { SignupComponent } from 'src/app/views/sessions/signup/signup.component';

export class CanDeactivateGuard implements CanDeactivate<SignupComponent> {
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem("bypass") == "true") { // to bypass the route
      localStorage.removeItem("bypass");
      return true;
    }
    return true;
    let confirmBack = confirm("Are you sure, data may lost"); // popup to show confirm if a user presses browser back button
    if (confirmBack) {
      return true;
    }
    else {
      confirmBack = false; // if he presses more then once
      return false;
    }
  }
}