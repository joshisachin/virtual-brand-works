import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  token: string = "";

  constructor(
    private router: Router,
    private cookieService: CookieService) { }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.token = this.cookieService.get("token");
    if (this.token) {
      // logged in so return true
      return true;
    } else {
      this.router.navigate(['/home']);
      return true;
    }
  }

}
