import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, fromEvent } from 'rxjs';
import { Folder } from '../../model/folder';
import { HttpService } from '../httpservice/httpservice.service';
import { Router, Event, NavigationEnd } from '@angular/router';
import { map, debounceTime, delay } from 'rxjs/operators';
import { API_MESSAGES } from '../../constants/message/message';
import { CookieService } from 'ngx-cookie-service';
import { SOCIAL_URL } from '../../constants/video/url';
import { DomSanitizer } from '@angular/platform-browser';
import { ManageLocation } from '../../model/manage-location';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  selectedFolders: BehaviorSubject<any> = new BehaviorSubject([]); // for selected folders in folder component
  folders: BehaviorSubject<Folder[]> = new BehaviorSubject([]); // folder all the folder in folder component
  dynamicFolder: BehaviorSubject<any> = new BehaviorSubject([]); // for dynamic folder
  currentUrl: BehaviorSubject<string> = new BehaviorSubject(""); // get the current url
  toggleRightSide: BehaviorSubject<boolean> = new BehaviorSubject(false);
  loader: BehaviorSubject<boolean> = new BehaviorSubject(false);
  selectedLocation: BehaviorSubject<ManageLocation> = new BehaviorSubject({}); // get the selected location object
  searchDataForFolder: BehaviorSubject<string> = new BehaviorSubject("");
  updateProfileImage: BehaviorSubject<string> = new BehaviorSubject(""); // update profile image on header
  updateUserName: BehaviorSubject<string> = new BehaviorSubject(""); // update user name if profile update
  updateLogoImage: BehaviorSubject<string> = new BehaviorSubject(""); // logo image
  selectedTags: BehaviorSubject<any> = new BehaviorSubject([]); // selected tags
  selectedTags_1: BehaviorSubject<any> = new BehaviorSubject([]); // selected tags
  notifications:BehaviorSubject<any> = new BehaviorSubject([]); // notifications

  constructor(private toastr: ToastrService, private httpService: HttpService, private router: Router, private cookieService: CookieService, private sanitizer: DomSanitizer) { }
  /**
   * show success message
   * @param message 
   * @param title 
   */
  showSuccess(message, title) {
    this.toastr.success(message, title);
  }

  /**
   * show error message
   * @param message
   * @param title 
   */
  showError(message, title) {
    this.toastr.error(message, title);
  }

  /**
   * show info message
   * @param message 
   * @param title 
   */
  showInfo(message, title) {
    this.toastr.info(message, title);
  }

  /**
   * show warning message
   * @param message 
   * @param title 
   */
  showWarning(message, title) {
    this.toastr.warning(message, title);
  }

  /**
   * get selected folders
   * @param type
   * @response object
   */
  getSelectedFolders(type: string) {
    this.httpService[type]().subscribe(res => {
      if (res.Status) {
        this.selectedFolders.next(res.Data);
      }
      else {
        this.showError(res.Message, "");
      }
    });
  }

  /**
   * get current route
   * @param null
   * @response route
   */
  getCurrentRoute() {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl.next(event.url);
      }
    });
  }

  /**
   * search
   * @param searchFrom
   * @param searchString
   * @param searchItem
   */
  search(searchFrom: any, searchString: string, searchItem: any) {
    searchFrom.filter(user => { // filtering the record type from the search bar
      return (searchFrom[searchItem].toLowerCase()).indexOf(searchString.toLowerCase()) > -1;
    });
  }

  /**
   * trim spaces
   * @param inputArray
   * @response promise
   */
  trimArray(inputArray: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let returnObj: Object = {};
      Object.keys(inputArray).map((data, i) => {
        if (typeof inputArray[data] === "string" && inputArray[data] !== "IsPolicyAccepted" && inputArray[data] !== "Password" && inputArray[data] !== "NewPassword" && inputArray[data] !== "ConfirmPassword") {
          returnObj[data] = inputArray[data].trim();
        }
        else {
          returnObj[data] = inputArray[data];
        }
      });
      resolve(returnObj);
    });
  }

  /**
   * search debounce
   * @param elementRef
   * @response observable
   */
  searchDebounce(elementRef) {
    return fromEvent(elementRef, "keyup").pipe(
      map((event: any) => {
        return event.target.value
      }),
      debounceTime(400),
      // distinctUntilChanged()
    );
  }

  /**
   * update profile image on header
   * @param imageUri
   * @response observable
   */
  updateHeaderProfileImage(imageUri) {
    this.updateProfileImage.next(imageUri);
  }

  /**
   * update user name on header
   * @param imageUri
   * @response observable
   */
  updateHeaderUserName(userName) {
    this.updateUserName.next(userName);
  }

  /**
   * logout
   * @param null
   * @response null
   */
  logout() {
    this.cookieService.delete("token", "/");
    this.cookieService.deleteAll();
    localStorage.clear();
    this.router.navigate(["/home"]);
    this.showSuccess(API_MESSAGES.Logout.successMessage, "");
  }

  /**
   * peform soring
   * @param user
   * @response object 
   */
  performSorting(user: any[], type: string = "") {
    user.sort((a, b) => {
      let match1: string;
      let match2: string
      if (type === "") {
        match1 = a.FirstName.toLocaleLowerCase() + a.LastName.toLocaleLowerCase();
        match2 = b.FirstName.toLocaleLowerCase() + b.LastName.toLocaleLowerCase();
      }
      else {
        match1 = a[type];
        match2 = b[type];
      }
      if (match1 > match2) {
        return 1;
      }
      else {
        return -1;
      }
    });
  }

  /**
   * time difference
   * @param expireDate 
   * @param currentDate 
   */
  timeDifference(expireDate, currentDate) {
    var diff = expireDate.getTime() - currentDate.getTime();
    var days = Math.floor(diff / (60 * 60 * 24 * 1000));
    var hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
    var minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));

    return "( Time Left : "+days+" Days "+ hours +" Hours " + minutes +" Minutes )";

  }

   /**
   * To update the notifications on header.
   * @param notifications updated notification object 
   */
  updateNotifications() {
    this.httpService.getNotifications(localStorage.getItem("userId")).subscribe(data => { //
      if (data.Data) {
        this.notifications = data.Data;
      }
    });
  }

  /**
   * format video url to play video of type
   * @param data
   * @response url
   */
  formatVideoUrl(data: any) {
    if (data.IsFacebookContent) {
      data.ContentUrl = SOCIAL_URL.Facebook + data.ContentUrl;
    }
    else if (data.IsYoutubeContent) {
      data.ContentUrl = SOCIAL_URL.Youtube + data.ContentUrl;
    };
    data.ContentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(data.ContentUrl);
  }
  
  /**
   * format video thumbnail url from type
   * @param data
   * @response url
   */
  formatVideoThumbnailUrl(data: any) {
    if (data.IsFacebookContent) {
      data.ThumbnailUrl = SOCIAL_URL.FacebookImage;
    }
    else if (data.IsYoutubeContent) {
      data.ThumbnailUrl = SOCIAL_URL.YoutubeImage + data.ContentUrl + "/hqdefault.jpg";
    };
    data.ThumbnailUrl = this.sanitizer.bypassSecurityTrustResourceUrl(data.ThumbnailUrl);
  }
}
