import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs'
import { CookieService } from "ngx-cookie-service";

@Injectable()
export class MessagingService {
  currentMessage = new BehaviorSubject(null);
  constructor(private angularFireMessaging: AngularFireMessaging, private cookieService:CookieService) {
    // this.angularFireMessaging.messages.subscribe(
    //   (_messaging) => {
    //     _messaging.onMessage = _messaging.onMessage.bind(_messaging);
    //     _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    //   }
    // )
  }
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        localStorage.setItem("fcmToken", token);
        this.cookieService.set("fcmToken", localStorage.getItem("fcmToken"));
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        this.currentMessage.next(payload);
      })
  }

  refreshToken() { 
    if(!this.cookieService.get("fcmToken")) {
      this.angularFireMessaging.getToken.subscribe(
        (token) => {
          localStorage.setItem("fcmToken", token);
          this.cookieService.set("fcmToken", localStorage.getItem("fcmToken"));
        },
        (err) => {
          console.error('Error on get token', err);
        }
      );
    }

  }
}