import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'confirmation-alert',
	templateUrl: './confirmation-alert.component.html',
	styleUrls: ['./confirmation-alert.component.scss']
})
/**
 * common class for all the confirmation popup
 */
export class ConfirmationAlertComponent implements OnInit {
	@Input() data: any;
	constructor(private activeModal : NgbActiveModal) {
	}

	ngOnInit(): void { } 

	/**
	 * to close the modal
	 */
	close() {
		this.activeModal.dismiss(false);
	}
	
	/**
	 * to submit and close the modal
	 * @param null
	 * @response null
	 */
	submit() {
		this.activeModal.close(true);		
	} 
}
