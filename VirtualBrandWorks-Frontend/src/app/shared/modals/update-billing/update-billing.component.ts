import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../../services/commonservices/commonservices.service';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/validation';
import { Country } from '../../model/country';
import { State } from '../../model/state';
import { City } from '../../model/city';
import { HttpService } from '../../services/httpservice/httpservice.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MyPlan } from '../../model/my-plan';
import { API_MESSAGES } from '../../constants/message/message';

@Component({
  selector: 'app-update-billing',
  templateUrl: './update-billing.component.html',
  styleUrls: ['./update-billing.component.scss']
})
export class UpdateBillingComponent implements OnInit {
  billingForm: FormGroup;
  @Input() data: MyPlan;
  validationMessages: any;
  countries: Array<Country>; // countries array
  states: Array<State>; // states array
  cities: Array<City>; // cities array
  cityName: string;
  stateName: string;
  countryName: string;
  countryId: number;
  stateId: number;
  constructor(private formBuilder: FormBuilder, private commonService: CommonService, private httpService: HttpService, private activeModal: NgbActiveModal) {
    this.billingForm = this.formBuilder.group({
      SelectedCountryId: ['', Validators.compose([
        Validators.required
      ])],
      SelectedStateId: ['', Validators.compose([
        Validators.required
      ])],
      SelectedCityId: ['', Validators.compose([
        Validators.required
      ])],
      ZipCode: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^(?:[A-Za-z0-9]+)(?:[A-Za-z0-9 _]+$)'),
        Validators.maxLength(30)
      ])],
      AddressOne: ['', Validators.compose([
        Validators.maxLength(150),
      ])],
      AddressTwo: ['', Validators.compose([
        Validators.maxLength(150),
      ])],
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void {
    this.billingForm.patchValue({"AddressOne": this.data.PaymentCard.BillingDetails.address.line1, "AddressTwo": this.data.PaymentCard.BillingDetails.address.line2, "ZipCode": this.data.PaymentCard.BillingDetails.address.postal_code})
    this.httpService.getCountries().subscribe(data => { // get countries
      if (data.Data) {
        this.countries = data.Data;
        if(this.data.PaymentCard.BillingDetails.address.country){
        this.countries.map(res => {
          if(res.CountryCode == this.data.PaymentCard.BillingDetails.address.country){
            this.countryId = res.ID;
            this.billingForm.patchValue({
              "SelectedCountryId": this.countryId
            })
            this.getStates(this.countryId);
          }
        })
      }
      }
    });
  }

  /**
   * update
   * @param formData
   * @response null
   */
  update(formData: FormData) {
    if (this.billingForm.valid) {
      this.commonService.trimArray(formData).then(data => {
        let sendData: any = {
          "Name": this.data.PaymentCard.BillingDetails.name,
          "Line1": formData["AddressOne"],
          "Line2": formData["AddressTwo"],
          "City": this.cityName,
          "State": this.stateName,
          "Country": this.countryName,
          "PostalCode": formData["ZipCode"],
        }
        this.httpService.updateAddress(sendData).subscribe(data => {
          if (data.Status) {
            this.commonService.showSuccess(API_MESSAGES.UpadateFolder.successMessage, "");
            this.activeModal.close();
          }
          else {
            this.commonService.showError(data.Message, "");
          }
        })
      });
    }
    else {
      this.billingForm.markAllAsTouched();
    }
  }

  /**
   * get states
   * @param countryId 
   * @response list
   */
  getStates(countryId: number) {
    this.billingForm.patchValue({
      "SelectedStateId": ''
    })
    let index = this.countries.findIndex(data => data.ID == countryId);
    this.countryName = this.countries[index].CountryCode;
    this.httpService.getStates(countryId).subscribe(data => {
      if (data.Data) {
        this.states = data.Data;
        if(this.data.PaymentCard.BillingDetails.address.state){
        this.states.map(res => {
          if(res.Name.toUpperCase() == this.data.PaymentCard.BillingDetails.address.state.toUpperCase() 
            || res?.StateCode?.toUpperCase() == this.data.PaymentCard.BillingDetails.address.state.toUpperCase()){
            this.stateId = res.ID;
            this.billingForm.patchValue({
              "SelectedStateId": this.stateId
            })
            this.getCities(this.stateId)
          }
        })
      }
      }
    });
  }

  /**
   * get cities
   * @param stateId 
   * @response list
   */
  getCities(stateId: number) {
    this.billingForm.patchValue({
      "SelectedCityId": ''
    })
    let index = this.states.findIndex(data => data.ID == stateId);
    this.stateName = this.states[index].Name;
    this.httpService.getCities(stateId).subscribe(data => {
      if (data.Data) {
        this.cities = data.Data;
        if (this.data.PaymentCard.BillingDetails.address.city) {
          this.cities.map(res => {
            if (res.Name.toUpperCase() == this.data.PaymentCard.BillingDetails.address.city.toUpperCase()) {
              this.billingForm.patchValue({
                "SelectedCityId": res.Name
              })
            }
          })
        }
      }
    });
  }

  /**
   * get city name
   * @param cityName 
   */
  getCityName(cityName: string) {
    this.cityName = cityName;
  }

  /**
   * close
   */
  close() {
    this.activeModal.dismiss();
  }

}
