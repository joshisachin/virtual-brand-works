import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-points-level',
  templateUrl: './points-level.component.html',
  styleUrls: ['./points-level.component.scss']
})
export class PointsLevelComponent implements OnInit {

  constructor(
    private activeModal : NgbActiveModal
  ) { }

  ngOnInit(): void {
  }

  /**
	 * to close the modal
	 */
	close() {
		this.activeModal.dismiss();
	}

}
