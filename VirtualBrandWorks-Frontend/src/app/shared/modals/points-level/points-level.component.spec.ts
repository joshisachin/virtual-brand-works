import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsLevelComponent } from './points-level.component';

describe('PointsLevelComponent', () => {
  let component: PointsLevelComponent;
  let fixture: ComponentFixture<PointsLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
