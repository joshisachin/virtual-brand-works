import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { HttpService } from '../../services/httpservice/httpservice.service';
import { CommonService } from '../../services/commonservices/commonservices.service';
import { FolderDetail } from '../../model/folder-detail';

@Component({
  selector: 'app-folder-details',
  templateUrl: './folder-details.component.html',
  styleUrls: ['./folder-details.component.scss']
})
export class FolderDetailsComponent implements OnInit {

  @Input() folderPopUp: any; // get the folder popup data
  @Output() showFolderModal = new EventEmitter<any>(); // send the folder popup data
  folderData: FolderDetail; // folder detail object
  buttonData: string = ""; // button string to display according to the selection of the folder
  @ViewChild('loadButton') loadButton: ElementRef;
  constructor(private httpService: HttpService, private commonService: CommonService) { }

  /**
   * get folder detail
   * @param null
   * @response object
   */
  ngOnInit(): void {
    this.httpService.getFolderDetails(this.folderPopUp.FolderId).subscribe(data => {
      if (data.Status) {
        this.folderData = data.Data;
        this.buttonData = this.folderData.IsSelectedFolder ? "Unselect" : "Select";
      }
      else {
        this.commonService.showError(data.Message, "");
      }
    });
  }

  /**
   * hide button
   * @param null
   * @response null
   */
  ngAfterViewInit() {
    this.loadButton.nativeElement.hidden = true;
  }

  /**
   * toggle folder to select or unselect
   * @param null
   * @response object
   */
  toggleFolder() {
    this.commonService.loader.next(true);
    this.folderData.IsSelectedFolder = !this.folderData.IsSelectedFolder;
    this.folderPopUp.IsSelectedFolder = this.folderData.IsSelectedFolder;
    this.showFolderModal.emit(false); // to destruct the object of this component so that it reinitialize for every popup
    this.httpService.saveSelectedFolder(this.folderData.FolderId, this.folderData.IsSelectedFolder).subscribe(data => {
      if (data.Status) {
        this.commonService.loader.next(false);
        this.commonService.getSelectedFolders("getSelectedFolders"); // as folder details is only for master library
      }
      else {
        this.commonService.loader.next(false);
        this.commonService.showError(data.Message, "");
      }
    });
  }

  /**
   * close modal
   * @param null
   * @response null
   */
  close() {
    this.showFolderModal.emit(false); // to destruct the object of this component so that it reinitialize for every popup
  }

}
