import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../../services/httpservice/httpservice.service';
import { Plan } from '../../model/plan';
import { CommonService } from '../../services/commonservices/commonservices.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-change-plan',
  templateUrl: './change-plan.component.html',
  styleUrls: ['./change-plan.component.scss']
})
export class ChangePlanComponent implements OnInit {
  @Input() planData: any = [];
  updatedPlanData: any = []; // plan data after selection
  plans: Plan; // plans object
  planCopy: any = []; // a copy of plans object to revert back to original after sorting
  oldPlanName: string = ""; // old plan name to display
  newPlanName: string = ""; // new plan name
  togglePlanTypeClass: boolean = false; // plan type changes toggle
  constructor(private httpService: HttpService, private commonService: CommonService, private activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
    if (this.planData.PlanType == 1) {
      this.togglePlanTypeClass = true;
    }
    this.oldPlanName = this.planData.PlanName;
    this.updatedPlanData = this.planData;
    this.httpService.getPlans(this.planData.NoTrial ? this.planData.NoTrial : false).subscribe(data => {
      if (data.Data) {
        this.planCopy = data.Data;
        this.plans = data.Data.filter(res => res.PlanType == this.planData.PlanType // assigning the plans of type selected by the user previously
        );
      }
      else {
        this.commonService.showError(data.Message, "");
      }
    });
  }

  /**
   * select plan
   * @param plan 
   */
  selectPlanData(plan: any) {
    this.updatedPlanData = plan;
    this.newPlanName = this.updatedPlanData.PlanName;
  }

  /**
   * update plan
   * @param null
   * @response event emmiter
   */
  updatePlanData() {
    this.activeModal.close(this.updatedPlanData)
  }

  /**
   * to close the modal
   */
  close() {
    this.activeModal.dismiss(this.planData);
  }

  /**
   * change plan type
   * @param value
   * @response null
   */
  changePlanType(value) {
    this.planData.planType = 2;
    this.togglePlanTypeClass = false;
    this.plans = this.planCopy.filter(res => res.PlanType === 2
    );
    if (value) {
      this.planData.planType = 1;
      this.togglePlanTypeClass = true;
      this.plans = this.planCopy.filter(res => res.PlanType === 1
      );
    }
  }

}
