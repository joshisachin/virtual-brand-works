import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { VALIDATION_MESSAGES } from 'src/app/shared/constants/validations/content-validation';
import { BuildPlaylistService } from 'src/app/views/build-playlist/build-playlist.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  createPlaylistForm: FormGroup;
  validationMessages: any = [];
  constructor(private activeModal: NgbActiveModal, private formBuilder: FormBuilder, private commonServices: CommonService, private buildPlaylistService: BuildPlaylistService) {
    this.createPlaylistForm = this.formBuilder.group({
      Name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
      Description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(500),
        Validators.minLength(4)
      ])]
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void {
  }

  /**
   * submit form
   * @param formData 
   * @response null
   */
  submit(formData: FormData) {
    if (this.createPlaylistForm.valid) {
      this.commonServices.trimArray(formData).then(data => {
        this.buildPlaylistService.buildPlaylist(data).subscribe(res => {
          if (res.Status) {
            this.commonServices.showSuccess(API_MESSAGES.Playlist.playlistCreateSuccess, "")
            this.activeModal.close(res.Data);
            //res.Data.Id
          }
          else {
            this.commonServices.showError(res.Message, "");
            this.activeModal.dismiss();
          }
        })
      });
    }
    else {
      this.createPlaylistForm.markAllAsTouched();
    }
  }

  close() {
    this.activeModal.dismiss();
  }

}
