import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbNavChangeEvent, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaylistContentResultList } from 'src/app/shared/model/playlist-content-list';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { PlaylistFolderContentResult } from 'src/app/shared/model/playlist-content-result';
import { PlaylistService } from 'src/app/views/my-playlist/playlist.service';
import { ViewQuizResultComponent } from '../../quiz/view-quiz-result/view-quiz-result.component';

@Component({
  selector: 'app-view-result',
  templateUrl: './view-result.component.html',
  styleUrls: ['./view-result.component.scss']
})
export class ViewResultComponent implements OnInit {

  @Input() data;
  playlistFolderResult: PlaylistFolderContentResult;
  folderContentData: PlaylistContentResultList;
  activeId: number;
  playlistContentName: string;
  playlistMarkObtained: number = 0;
  playlistId: number;
  userId: number;

  constructor(
    private activeModal : NgbActiveModal,
    public playlistService: PlaylistService,
    private commonService: CommonService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
   this.playlistId = this.data.playlistId;
   this.userId = this.data.userId;
    this.activeId = 1;
    this.viewResult()
    
  }
viewResult() {
  this.playlistService.getResultFolderAndDetail(this.playlistId, this.userId).subscribe(folderData => {
    if(folderData.Status) {
      this.playlistFolderResult = folderData.Data;
      this.getFolderContent();
    }
    else {
      this.commonService.showError(folderData.Message,'');
    }
  })
}

  getFolderContent(index: number = 0) {
    this.commonService.loader.next(true);
    this.playlistService.viewResultContentList(this.playlistFolderResult.folders[index]?.PlayListFolderId, this.userId).subscribe(contentData=> {
      if(contentData.Status) {
        this.commonService.loader.next(false);
        this.folderContentData = contentData;
        this.playlistContentName = this.playlistFolderResult.folders[index].FolderName;
        this.markObtained()
      } else {
        this.commonService.showError(contentData.Message, "");
        this.commonService.loader.next(false);
      }
    })
  }

  markObtained() {
    this.playlistMarkObtained = 0;
    this.folderContentData.Data.map(data => {
      this.playlistMarkObtained+=data.MarksObtained || 0
    })
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {  
    this.activeId = changeEvent.nextId;
  }

  /**
   * To view quiz result
   * @param contentList 
   */

  viewQuizResult(contentList, folder) {
    let data: any = {};
    data.QuizId = contentList.Content.QuizId;
    data.playlistId = folder.PlayListId;
    data.userId = localStorage.getItem("userId");
    data.isMasterQuiz = folder.IsMasterLibraryFolder && !contentList.Content.IsTurnOffExistingQuiz ? true : false;
    this.playlistService.getQuizResult(data).subscribe(quizResult => {
      const modalRef = this.modalService.open(ViewQuizResultComponent, {
        size: 'xl' , 
        centered: true, 
        ariaLabelledBy: 'modal-basic-title'
      });
      modalRef.componentInstance.data = { 
        quizResult: quizResult
      };
      modalRef.result.then((result) => {
        // setTimeout(() => {
        // 	this.addUser();
        // },200)
      }, (reason) => { });
    })
    
  }

  /**
   * check the content type
   * @param mimeType
   * @response null
   */
  checkType(mimeType: string) {
    if (mimeType.match("video")) {
      return true;
    }
    else {
      return false;
    }
  }

  userPassStatus(isUserPassed) {
    return isUserPassed != null ? 'Pass' : 'Fail';
  }

  /**
	 * to close the modal
	 */
	close() {
		this.activeModal.dismiss();
	}

}
