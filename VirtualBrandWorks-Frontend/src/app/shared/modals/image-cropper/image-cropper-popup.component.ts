import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
	selector: 'image-cropper-popup',
	templateUrl: './image-cropper-popup.component.html',
	styleUrls: ['./image-cropper-popup.component.scss']
})
export class ImageCropperPopupComponent implements OnInit {
	@Input() inputData;

    imageChangedEvent: any = '';
    croppedImage: any = '';
    data:any;

    cropperSettings: any;
	constructor(private activeModal : NgbActiveModal) {
	}

	ngOnInit(): void {
    }

	close() {
		this.activeModal.dismiss(false);
	}
	
	submit() {
		this.activeModal.close(this.croppedImage);		
	} 

	fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        console.log("image", this.croppedImage);
    }
    imageLoaded() {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }
}
