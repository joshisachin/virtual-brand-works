import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'common-popup',
	templateUrl: './common-popup.component.html',
	styleUrls: ['./common-popup.component.scss']
})
export class CommonPopupComponent implements OnInit {
	@Input() data;
	constructor(private activeModal : NgbActiveModal) {
	}

	ngOnInit(): void { } 

	/**
	 * to close the modal
	 */
	close() {
		this.activeModal.dismiss(false);
	}
	
	/**
	 * to add more folder or any other
	 * @param null
	 * @response null
	 */
	addMore() {
		this.activeModal.close(true);		
	} 
}
