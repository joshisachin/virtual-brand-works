import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'alert',
	templateUrl: './alert.component.html',
	styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
	@Input() data;
	constructor(private activeModal : NgbActiveModal) {
	}
	
	ngOnInit(): void { } 

	/**
	 * to close the modal
	 */
	close() {
		this.activeModal.dismiss(false);
	}
	
	/**
	 * submit the modal and close
	 * @param null
	 * @response null
	 */
	submit() {
		this.activeModal.close(true);		
	} 
}
