import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QuizService } from '../quiz.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { Quiz } from 'src/app/shared/model/quiz';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { ConfirmationAlertComponent } from '../../confirmation/confirmation-alert.component';
import { ResultComponent } from '../result/result.component';

@Component({
  selector: 'app-play-quiz',
  templateUrl: './play-quiz.component.html',
  styleUrls: ['./play-quiz.component.scss']
})
export class PlayQuizComponent implements OnInit {

  @Input() reqObj: any;
  quizData: Quiz;
  index: number = 0; // question index
  submitQuiz: any = { "QuestionAttempt": [] }
  constructor(private activeModal: NgbActiveModal, private quizService: QuizService, private modalService: NgbModal, private commonServices: CommonService) {
    this.commonServices.loader.next(true);
  }

  ngOnInit(): void {
    this.getQuiz();
  }

  /**
   * get quiz
   * @param null
   * @response null
   */
  getQuiz() {
    this.quizService.getQuizToPlay(this.reqObj).subscribe(data => {
      if (data.Status) {
        this.quizData = data.Data;
        this.quizData.QuestionAnswers.map(question => {
          this.submitQuiz.QuestionAttempt.push({ "QuestionId": question.Question.QuestionId, "AnswerId": 0 });
        });
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }
  /**
   * to close the modal
   */
  close() {
    this.activeModal.dismiss(false);
  }

  /**
   * previous
   * @param null
   * @response null
   */
  previous() {
    this.index--;
  }

  /**
   * next
   * @param null
   * @response null
   */
  next() {
    this.index++;
  }

  /**
   * check answer
   * @param answerId
   * @response null
   */
  checkAnswer(answerId: number) {
    let index = this.submitQuiz["QuestionAttempt"].findIndex(element => element.QuestionId == this.quizData.QuestionAnswers[this.index].Question.QuestionId);
    if (index != -1) {
      this.submitQuiz["QuestionAttempt"][index].AnswerId = answerId;
    }
  }

  /**
   * submit
   * @param null
   * @response null
   */
  submit() {
    this.submitQuiz.QuizId = this.reqObj.quizId;
    this.submitQuiz.IsMasterQuiz = (this.reqObj.isTurnedOffExistingQuiz) ? false : this.reqObj.isMasterContent;
    this.submitQuiz.PlaylistId = this.reqObj.playlistId;
    this.submitQuiz.PlaylistContentMasterId = this.reqObj.playlistContentMasterId;
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {
      windowClass: 'modalwidth'
    });

    modalRef.componentInstance.data = {
      "title": "Are you sure you want to submit your quiz?",
      "okButton": "Yes, Submit",
      "cancelButton": "Recheck"
    };

    modalRef.result.then(() => {
      this.commonServices.loader.next(true);
      this.quizService.submitPlayQuiz(this.submitQuiz).subscribe(data => {
        if (data.Status) {
          this.commonServices.showSuccess(API_MESSAGES.Quiz.submitSuccess, "");
          const resultModal = this.modalService.open(ResultComponent, { windowClass: 'modalWidth' });
          resultModal.componentInstance.result = data.Data;
          resultModal.result.then(() => {
          }, (reason) => {
            // this.contentDetails.QuizId = null;
          });
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
        this.activeModal.close(data.Data);
      });
    });
  }

}
