import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_NAME } from '../../constants/apiname/apiname';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http: HttpClient) { }
  
  /**
   * create quiz
   */
  createQuiz(name: string, contentId: number, isPlaylistContentquiz: boolean = false, isMasterContent: boolean = false): Observable<any> {
    return this.http.post(API_NAME.SaveQuiz[0].name, {"ContentId": contentId, "QuizName": name, "IsPlaylistContentQuiz": isPlaylistContentquiz, "IsMasterContent": isMasterContent});
  }

  /**
   * add questions
   */
  addQuestions(data: any): Observable<any> {
    return this.http.post(API_NAME.AddQuestions[0].name, data);
  }

  /**
   * update question
   */
  updateQuestion(data: any): Observable<any> {
    return this.http.post(API_NAME.UpdateQuestion[0].name, data);
  }

  /**
   * delete question
   */
  deleteQuestion(questionId: number): Observable<any> {
    return this.http.delete(API_NAME.DeleteQuestion[0].name + "?questionId=" + questionId);
  }

  /**
   * delete quiz
   */
  deleteQuiz(quizId: number): Observable<any> {
    return this.http.delete(API_NAME.DeleteQuiz[0].name + "?quizId=" + quizId);
  }

  /**
   * get quiz
   */
  getQuiz(quizId: number, isMasterFolder: boolean = false, isTurnedOffExistingQuiz: boolean = false): Observable<any> {
    return this.http.get(API_NAME.GetQuizDetails[0].name + "?quizId=" + quizId + "&isMasterFolder=" + isMasterFolder + "&isTurnedOffExistingQuiz=" + isTurnedOffExistingQuiz);
  }

  /**
   * get quiz
   */
  getQuizToPlay(data: any): Observable<any> {
    return this.http.get(API_NAME.GetQuizToPlay[0].name + "?quizId=" + data.quizId + "&isMasterContent=" + data.isMasterContent + "&isTurnedOffExistingQuiz=" + data.isTurnedOffExistingQuiz);
  }

  /**
   * submit play quiz
   */
  submitPlayQuiz(data: any): Observable<any> {
    return this.http.post(API_NAME.SubmitPlayQuiz[0].name, data);
  }
}
