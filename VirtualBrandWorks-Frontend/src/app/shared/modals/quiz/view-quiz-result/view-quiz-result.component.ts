import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-quiz-result',
  templateUrl: './view-quiz-result.component.html',
  styleUrls: ['./view-quiz-result.component.scss']
})
export class ViewQuizResultComponent implements OnInit {

  @Input() data;
  quizData: any;
  constructor(
    private activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    this.quizData = this.data.quizResult.Data;
  }

  /**
   * to close the modal
   */
  close() {
    this.activeModal.dismiss();
  }

}
