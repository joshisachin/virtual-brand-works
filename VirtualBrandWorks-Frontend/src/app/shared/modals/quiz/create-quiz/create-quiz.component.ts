import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../../../services/httpservice/httpservice.service';
import { CommonService } from '../../../services/commonservices/commonservices.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QuizService } from '../quiz.service';
import { AddQuestionsComponent } from '../add-questions/add-questions.component';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-create-quiz',
  templateUrl: './create-quiz.component.html',
  styleUrls: ['./create-quiz.component.scss']
})
export class CreateQuizComponent implements OnInit {

  name: string = "";
  @Input() contentId: number;
  @Input() isPlaylistContent: boolean = false;
  @Input() isMasterContent: boolean = false;
  constructor(private commonService: CommonService, private activeModal: NgbActiveModal, private quizService: QuizService, private modalService: NgbModal) { }

  /**
   * get folder detail
   * @param null
   * @response object
   */
  ngOnInit(): void {

  }

  /**
   * submit name
   * @param null
   * @response null
   */
  submit() {
    this.name = this.name.trim();
    if (this.name == "" || this.name.length < 3 || this.name.length > 30) {
      this.commonService.showError(API_MESSAGES.Quiz.checkName, "");
    }
    else {
      this.commonService.loader.next(true);
      this.quizService.createQuiz(this.name, this.contentId, this.isPlaylistContent, this.isMasterContent).subscribe(data => {
        if (data.Status) {
          this.commonService.showSuccess(API_MESSAGES.Quiz.createQuiz, "");
          this.activeModal.close(data.Data);
          this.openAddQuestions(data.Data);
        }
        else {
          this.commonService.showError(data.Message, "");
        }
        this.commonService.loader.next(false);
      });
    }
  }

  /**
   * to close the modal
   */
  close() {
    this.activeModal.dismiss(false);
  }

  /**
   * open add questions
   * @param null
   * @response null
   */
  openAddQuestions(quizId: number) {
    const modalRef = this.modalService.open(AddQuestionsComponent, { windowClass: 'm680w' });
    modalRef.componentInstance.quizId = quizId;
    modalRef.result.then((result) => {
    }, (reason) => { });
  }

}
