import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { QuizService } from '../quiz.service';
import { Quiz } from 'src/app/shared/model/quiz';
import { UpdateQuestionComponent } from '../update-question/update-question.component';
import { ConfirmationAlertComponent } from '../../confirmation/confirmation-alert.component';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { AddQuestionsComponent } from '../add-questions/add-questions.component';


@Component({
  selector: 'app-view-quiz',
  templateUrl: './view-quiz.component.html',
  styleUrls: ['./view-quiz.component.scss']
})
export class ViewQuizComponent implements OnInit {
  @Input() quizId: number;
  @Input() folderType: string;
  @Input() isTurnedOffExistingQuiz: boolean = false;
  quizData: Quiz;
  constructor(private activeModal: NgbActiveModal, private commonServices: CommonService, private quizService: QuizService, private modalService: NgbModal) {
    this.commonServices.loader.next(true);
  }

  ngOnInit(): void {
    this.getQuiz();
  }

  /**
   * close
   */
  close() {
    this.activeModal.close();
  }

  /**
   * get quiz
   * @param null
   * @response null
   */
  getQuiz() {
    let masterFolder: boolean = (this.folderType != 'master') ? false : true;
    this.quizService.getQuiz(this.quizId, masterFolder, this.isTurnedOffExistingQuiz).subscribe(data => {
      if (data.Status) {
        this.quizData = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * update question
   * @param index
   * @response null
   */
  updateQuestion(index: number) {
    const modalRef = this.modalService.open(UpdateQuestionComponent, { centered: true });
    modalRef.componentInstance.questionData = this.quizData.QuestionAnswers[index];
    modalRef.componentInstance.quizId = this.quizData.QuizId;
    modalRef.result.then((result) => {
    }, (reason) => { });
  }

  /**
   * delete question
   * @param index
   * @response null
   */
  deleteQuestion(index) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {
      windowClass: 'modalwidth',
      centered: true
    });

    modalRef.componentInstance.data = {
      "title": "Are you sure you want to delete this question?"
    };

    modalRef.result.then(() => {
      this.commonServices.loader.next(true);
      this.quizService.deleteQuestion(this.quizData.QuestionAnswers[index].Question.QuestionId).subscribe(data => {
        if (data.Status) {
          let indexPop = this.quizData.QuestionAnswers.indexOf(this.quizData.QuestionAnswers.find(elem => elem.Question.QuestionId == this.quizData.QuestionAnswers[index].Question.QuestionId));
          this.quizData.QuestionAnswers.splice(index, 1);
          this.commonServices.showSuccess(API_MESSAGES.DeleteTeam.successMessage, "");
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
      });
    });
  }

  /**
   * open add questions
   * @param null
   * @response null
   */
  openAddQuestions() {
    this.activeModal.close();
    const modalRef = this.modalService.open(AddQuestionsComponent, { centered: true });
    modalRef.componentInstance.quizId = this.quizId;
    modalRef.result.then((result) => {
    }, (reason) => { });
  }

  /**
   * delete quiz
   * @param null
   * @response null
   */
  deleteQuiz() {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {
      windowClass: 'modalwidth',
      centered: true
    });

    modalRef.componentInstance.data = {
      "title": "Are you sure you want to delete this quiz?"
    };

    modalRef.result.then(() => {
      this.commonServices.loader.next(true);
      this.quizService.deleteQuiz(this.quizId).subscribe(data => {
        if (data.Status) {
          this.activeModal.dismiss();
          this.commonServices.showSuccess(API_MESSAGES.DeleteTeam.successMessage, "");
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
      });
    });
  }
}
