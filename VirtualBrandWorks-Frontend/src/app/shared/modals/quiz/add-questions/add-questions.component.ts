import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { VALIDATION_MESSAGES } from "../../../constants/validations/question"
import { QuizService } from '../quiz.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-add-questions',
  templateUrl: './add-questions.component.html',
  styleUrls: ['./add-questions.component.scss']
})
export class AddQuestionsComponent implements OnInit {

  addQuestionForm: FormGroup;
  validationMessages: any;
  sendData: any = [];
  lastQuestion: any;
  @Input() quizId: number;
  constructor(private activeModal: NgbActiveModal, private formBuilder: FormBuilder, private commonServices: CommonService, private quizService: QuizService) {
    this.addQuestionForm = this.formBuilder.group({
      Question: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(500)
      ])],
      Answer1: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(200)
      ])],
      Answer2: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(200)
      ])],
      Answer3: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(200)
      ])],
      Answer4: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(200)
      ])],
      IsRightAnswer: ['', Validators.compose([
        Validators.required,
      ])],
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void {
  }

  /**
	 * to close the modal
	 */
  close() {
    this.activeModal.dismiss(false);
  }

  /**
   * submit form
   * @param formData
   * @response null
   */
  submit(formData: FormData) {
    if (this.addQuestionForm.valid) {
      this.commonServices.trimArray(formData).then(data => {
        this.lastQuestion = {
          "Question": {
            "Question": data["Question"]
          },
          "Answers": [
            {
              "Answer": data["Answer1"],
              "IsRightAnswer": false
            },
            {
              "Answer": data["Answer2"],
              "IsRightAnswer": false
            },
            {
              "Answer": data["Answer3"],
              "IsRightAnswer": false
            },
            {
              "Answer": data["Answer4"],
              "IsRightAnswer": false
            }],
          "QuizId": this.quizId
        };
        let index = parseInt(data["IsRightAnswer"]) - 1;
        this.lastQuestion.Answers[index].IsRightAnswer = true;
        this.addQuestionForm.reset();
      });
    }
    else {
      this.addQuestionForm.markAllAsTouched();
    }
  }

  /**
   * add new question
   * @param null
   * @response null
   */
  addNew() {
    if (this.lastQuestion) {
      this.sendData.push(this.lastQuestion);
      this.lastQuestion = null;
    }
  }

  /**
   * update last question
   * @param null
   * @response null
   */
  update() {
    this.addQuestionForm.patchValue({ "Question": this.lastQuestion.Question.Question });
    this.addQuestionForm.patchValue({ "Answer1": this.lastQuestion.Answers[0].Answer });
    this.addQuestionForm.patchValue({ "Answer2": this.lastQuestion.Answers[1].Answer });
    this.addQuestionForm.patchValue({ "Answer3": this.lastQuestion.Answers[2].Answer });
    this.addQuestionForm.patchValue({ "Answer4": this.lastQuestion.Answers[3].Answer });
  }

  /**
   * upload question
   */
  uploadQuestions() {
    this.commonServices.loader.next(true);
    this.quizService.addQuestions(this.sendData).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Quiz.questionAddSuccess, "");
        this.activeModal.close();
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }
}
