import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { QuizService } from '../quiz.service';
import { VALIDATION_MESSAGES } from 'src/app/shared/constants/validations/question';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-update-question',
  templateUrl: './update-question.component.html',
  styleUrls: ['./update-question.component.scss']
})
export class UpdateQuestionComponent implements OnInit {

  updateQuestionForm: FormGroup;
  validationMessages: any;
  lastQuestion: any;
  @Input() quizId: number;
  @Input() questionData: any;
  questionDataCopy: any; // for local instance so that nothing changes in the parent if the user does not save the changes
  constructor(private activeModal: NgbActiveModal, private formBuilder: FormBuilder, private commonServices: CommonService, private quizService: QuizService) {
    this.updateQuestionForm = this.formBuilder.group({
      Question: ['', Validators.compose([
        Validators.required,
      ])],
      Answer1: ['', Validators.compose([
        Validators.required,
      ])],
      Answer2: ['', Validators.compose([
        Validators.required,
      ])],
      Answer3: ['', Validators.compose([
        Validators.required,
      ])],
      Answer4: ['', Validators.compose([
        Validators.required,
      ])],
      IsRightAnswer: ['', Validators.compose([
        Validators.required,
      ])],
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void {
    this.questionDataCopy = JSON.parse(JSON.stringify(this.questionData));
  }

  /**
   * to close the modal
   */
  close() {
    this.activeModal.dismiss(false);
  }

  /**
   * submit form
   * @param formData
   * @response null
   */
  submit(formData: FormData) {
    if (this.updateQuestionForm.valid) {
      this.commonServices.trimArray(formData).then(data => {
        this.questionData.Question.Question = data["Question"];
        this.questionData.Answers[0].Answer = data["Answer1"];
        this.questionData.Answers[1].Answer = data["Answer2"];
        this.questionData.Answers[2].Answer = data["Answer3"];
        this.questionData.Answers[3].Answer = data["Answer4"];
        let index = parseInt(data["IsRightAnswer"]) - 1;
        this.questionData.Answers.map(answer => {
          answer.IsRightAnswer = false;
        });
        this.questionData.Answers[index].IsRightAnswer = true;
        this.uploadQuestion();
      });
    }
    else {
      this.updateQuestionForm.markAllAsTouched();
    }
  }

  /**
   * upload question
   * @param null
   * @response object
   */
  uploadQuestion() {
    this.commonServices.loader.next(true);
    this.quizService.updateQuestion([this.questionData]).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Quiz.questionUpdateSuccess, "");
        this.activeModal.close();
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

}
