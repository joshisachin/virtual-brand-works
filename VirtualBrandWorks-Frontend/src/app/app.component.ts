import { Component, OnInit } from '@angular/core';
import { LoaderComponent } from "./views/elements/loader/loader.component";
import { MessagingService } from "./shared/services/messaging/messaging.service";
import { HttpService } from './shared/services/httpservice/httpservice.service';
import { CommonService } from './shared/services/commonservices/commonservices.service';
import { Router,NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  message;

  title = 'angapp';
  token: string;
  constructor(private messagingService: MessagingService, private router: Router, private commanServices:CommonService) { 
     
  }
  ngOnInit() {
    this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
    this.messagingService.refreshToken()
    this.message = this.messagingService.currentMessage
    
  }

  

  //     Username : nidhi.evontech@gmail.com
  // Password: Nidhi.evontech@stripe
}
