import { Component, OnInit } from '@angular/core';
import { BuildPlaylistService } from '../build-playlist.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { PlaylistFolder } from 'src/app/shared/model/playlist-folder';
import { NgbNavChangeEvent, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { CreateQuizComponent } from 'src/app/shared/modals/quiz/create-quiz/create-quiz.component';
import { ViewQuizComponent } from 'src/app/shared/modals/quiz/view-quiz/view-quiz.component';
import { PlaylistContent } from 'src/app/shared/model/playlist-content';

@Component({
  selector: 'app-playlistcontent',
  templateUrl: './playlistcontent.component.html',
  styleUrls: ['./playlistcontent.component.scss']
})
export class PlaylistContentComponent implements OnInit {

  constructor(private buildPlaylistService: BuildPlaylistService, private commonServices: CommonService, private route: ActivatedRoute, private modalService: NgbModal) { }

  active = '1';
  nav = '1';
  folders: PlaylistFolder[];
  folderContent: PlaylistContent[];
  playlistId: number;
  selectedFolder: PlaylistFolder;
  allQuizOff: boolean;
  selectedFolderIndex: number;
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.playlistId) {
        this.playlistId = params.playlistId;
        this.getFolders();
      }
    });
  }

  /**
   * get folders
   * @param null
   * @response null
   */
  getFolders() {
    this.commonServices.loader.next(true);
    this.buildPlaylistService.getPlaylistFolders(this.playlistId).subscribe(data => {
      if (data.Status) {
        this.folders = data.Data;
        this.getFolderContent(0);
        this.active = data.Data[0].FolderId.toString();
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * get folder content
   * @param index
   * @response null
   */
  getFolderContent(index: number) {
    this.commonServices.loader.next(true);
    this.selectedFolder = this.folders[index];
    this.buildPlaylistService.getFoldercontent(this.selectedFolder.PlayListFolderId).subscribe(data => {
      if (data.Status) {
        this.folderContent = data.Data;
        let check = this.folderContent.find(element => element.IsTurnedOffExistingQuiz == false);
        this.allQuizOff = true;
        this.selectedFolderIndex = index;
        if (check) {
          this.allQuizOff = false;
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * check the content type
   * @param mimeType
   * @response null
   */
  checkType(mimeType: string) {
    if (mimeType.match("video")) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {
    //this.commonServices.loader.next(true);
  }

  /**
   * open create quiz modal
   * @param index
   * @response null
   */
  openCreateQuiz(index: number) {
    this.folderContent[index].IsTurnedOffExistingQuiz = true;
    this.toggleQuiz(index);
    const modalRef = this.modalService.open(CreateQuizComponent, {});

    modalRef.componentInstance.contentId = this.folderContent[index].PlaylistContentMasterId;
    modalRef.componentInstance.isPlaylistContent = true;
    if (this.selectedFolder.IsMasterLibraryFolder) {
      modalRef.componentInstance.isMasterContent = true;
    }
    modalRef.result.then((result) => {
      this.folderContent[index].Content.QuizId = result;
    }, (reason) => { });
  }

  /**
   * view quiz
   * @param index
   * @response null
   */
  viewQuiz(index: number) {
    const modalRef = this.modalService.open(ViewQuizComponent, {centered: true });

    modalRef.componentInstance.quizId = this.folderContent[index].Content.QuizId;
    modalRef.componentInstance.folderType = "company-content";
    modalRef.componentInstance.isTurnedOffExistingQuiz = this.folderContent[index].IsTurnedOffExistingQuiz;
    if (this.selectedFolder.IsMasterLibraryFolder) {
      modalRef.componentInstance.folderType = "master";
    }
    modalRef.result.then((result) => {
    }, (reason) => {
      this.folderContent[index].Content.QuizId = null;
    });
  }

  /**
   * toggle all quiz
   * @param value
   * @response null
   */
  toggleAllQuiz() {
    this.buildPlaylistService.turnOffExistingQuizForAll(this.selectedFolder.PlayListFolderId, this.allQuizOff).subscribe(data => {
      if (data.Status) {
        this.folderContent.map((arr) => {
          arr.IsTurnedOffExistingQuiz = this.allQuizOff;
        });
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * toggle one quiz
   * @param index
   * @response null
   */
  toggleQuiz(index: number) {
    this.buildPlaylistService.turnOffExistingQuiz(this.folderContent[index].PlaylistContentMasterId, this.folderContent[index].IsTurnedOffExistingQuiz).subscribe(data => {
      if (data.Status) {
        if (!this.folderContent[index].IsTurnedOffExistingQuiz) {
          this.allQuizOff = false;
        }
        //To update the folder content quizes on toggle TrunOffExisting Quiz
        this.getFolderContent(this.selectedFolderIndex);
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

}
