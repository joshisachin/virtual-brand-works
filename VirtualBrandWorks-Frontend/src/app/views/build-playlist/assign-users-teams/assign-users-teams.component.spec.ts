import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignUsersTeamsComponent } from './assign-users-teams.component';

describe('AssignUsersTeamsComponent', () => {
  let component: AssignUsersTeamsComponent;
  let fixture: ComponentFixture<AssignUsersTeamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignUsersTeamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignUsersTeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
