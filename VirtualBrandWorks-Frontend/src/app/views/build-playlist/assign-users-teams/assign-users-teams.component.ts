import { Component, OnInit } from '@angular/core';
import { BuildPlaylistService } from '../build-playlist.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { TeamUser } from 'src/app/shared/model/team-user';
import { Team } from 'src/app/shared/model/team';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';

@Component({
  selector: 'app-assign-users-teams',
  templateUrl: './assign-users-teams.component.html',
  styleUrls: ['./assign-users-teams.component.scss']
})
export class AssignUsersTeamsComponent implements OnInit {
  //Playlistid corresponding to which user and team is assigned
  playlistId: number;
  //users object
  users: TeamUser[];
  storeUsers: TeamUser[];
  //to store selected users
  selectedUsers: TeamUser[] = [];
  //startdate of playlist
  startDate: NgbDateStruct;
  //end date of playlist
  endDate: NgbDateStruct;
  //team object
  teams: Team[];
  //variable used to store the team object
  storeTeams: Team[];
  //team ready to assigned
  selectedTeams: Team[] = [];
  //formgroup object created for validation
  assignUserTeamForm: FormGroup;
  currentDate: Date; // current date
  minStartDate: NgbDateStruct; // minimum date for start date
  minEndDate: NgbDateStruct; // minimum date for end date

  selectedAllTeams: boolean;
  selectedAllUsers: boolean;
  locationStyle: boolean = true;
  isChange: string = "false"; // to only change this part
  userSearch: string = ""; // to search users and the tags
  teamSearch: string = ""; // to search teams and the tags
  isReverse: boolean = false; // for reverse sort
  /**
  * constructor.
  */
  constructor(private buildPlaylistService: BuildPlaylistService, private commonServices: CommonService, public datepipe: DatePipe, private route: ActivatedRoute, private fb: FormBuilder, private router: Router, private httpService: HttpService) {
    this.currentDate = new Date();
    this.minStartDate = this.fromModel(this.currentDate);
    this.minEndDate = this.fromModel(this.currentDate);
    //form group intitialized
    this.assignUserTeamForm = this.fb.group({
      frmStartDate: [this.fromModel(this.currentDate), Validators.required],
      frmEndDate: [this.fromModel(this.currentDate), Validators.required],
    });
  }

  ngOnInit(): void {
    //Get the playlistId from query string 
    this.route.params.subscribe(params => {
      if (params.playlistId) {
        this.playlistId = params.playlistId;
        this.isChange = params.isChange;
        this.getTeamsAndUsers();
      }
    });
  }


  /**
  * get the teams,user object, startdate, enddate from web api. 
  * @param null
  * @response null
  */
  getTeamsAndUsers() {
    this.commonServices.loader.next(true);
    this.commonServices.selectedLocation.subscribe(value => {

      this.buildPlaylistService.getTeamsandUsers(this.playlistId, value.LocationId).subscribe(data => {
        if (data.Status) {
          //For Users
          this.users = data.Data.Users;
          this.storeUsers = data.Data.Users;
          this.selectedUsers = this.storeUsers.filter(value => value.IsSelected === true);

          //Start Date and End Date
          let ngbStartDate = data.Data.StartDate ? new Date(data.Data.StartDate.split('T')[0]) : this.currentDate;
          this.startDate = this.fromModel(ngbStartDate);
          let ngbEndDate = data.Data.EndDate ? new Date(data.Data.EndDate.split('T')[0]) : this.currentDate;
          this.endDate = this.fromModel(ngbEndDate);

          //For Teams
          this.teams = data.Data.Teams;
          this.storeTeams = data.Data.Teams;
          this.selectedTeams = this.storeTeams.filter(value => value.IsSelected === true);

          this.checkAllSelected();

          this.commonServices.loader.next(false);
        }
        else {
          this.commonServices.showError(data.Message, "");
          this.commonServices.loader.next(false);
        }
      });

    })

  }

  /**
  * push selected users
  * @param userId
  * @response object
  */
  pushSelectedUsers(userId: number) {
    let index = this.storeUsers.indexOf(this.storeUsers.find(elem => elem.UserId == userId));
    this.storeUsers[index].IsSelected = true;
    this.selectedUsers = this.storeUsers.filter(value => value.IsSelected === true);
  }

  /**
   * unselect user
   * @param index
   */
  unselectUser(index) {
    this.selectedUsers[index].IsSelected = false;
    this.selectedUsers = this.selectedUsers.filter(value => value.IsSelected === true);
    this.selectedAllUsers = false;
  }

  /**
    * push selected Teams
    * @param teamId
    * @response object
    */
  pushSelectedTeams(teamId: number) {
    let index = this.storeTeams.indexOf(this.storeTeams.find(elem => elem.TeamId == teamId));
    this.storeTeams[index].IsSelected = true;
    this.selectedTeams = this.storeTeams.filter(value => value.IsSelected === true);
  }

  /**
   * unselect team
   * @param index
   */
  unselectTeam(index) {
    this.selectedTeams[index].IsSelected = false;
    this.selectedTeams = this.selectedTeams.filter(value => value.IsSelected === true);
    this.selectedAllTeams = false;
  }

  /**
  * sort by name
  * @param type
  * @param sortOn to sort the array object on field.
  * @response object
  */
  sortName(type: string,sortOn: string='') {
      this.commonServices.performSorting(sortOn=="team"?this.teams:this.users, type);

      if (this.isReverse) {
        sortOn=="team"?this.teams.reverse():this.users.reverse()
      }
      this.isReverse = !this.isReverse;
  }

  /**
   * saving assigned team and user data on server
   * @param null
   * @response null
   */
  save() {
    if (this.assignUserTeamForm.invalid) {
      this.assignUserTeamForm.markAllAsTouched();
      return;
    }

    if (this.selectedUsers.length == 0 && this.selectedTeams.length == 0) {
      this.commonServices.showError(API_MESSAGES.Playlist.assignUserTeamRequired, "");
      return;
    }

    if (new Date(this.format(this.startDate)).getTime() > new Date(this.format(this.endDate)).getTime()) {
      this.commonServices.showError(API_MESSAGES.Playlist.startDateError, "");
      return;
    }
    this.commonServices.loader.next(true);
    var usersData = this.selectedUsers.map(function (a) { return a.UserId; });
    var teamsData = this.selectedTeams.map(function (a) { return a.TeamId; });
    this.buildPlaylistService.UpdateTeamsandUsers(this.playlistId, usersData, teamsData, this.format(this.startDate), this.format(this.endDate)).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Playlist.userTeamAddedSuccess, "");
        if (this.isChange == 'true') {
          this.router.navigate(["/my-playlist"]);
        }
        else {
          this.router.navigate(["/build-playlist/launch/" + this.playlistId]);
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      };
      this.commonServices.loader.next(false);
    });
  }

  /**
   * date conversion from string to ngbdate
   * @param type
   * @response
   * event to select date on calender
   */
  selectDate(type: string = "endDate", value: NgbDate) {
    if (type === "startDate") {
      this.startDate = value;
    }
    else {
      this.endDate = value;
    }
  }

  /**
   * To convert date to ngbDateStruct for calender.
   */
  fromModel(date: Date): NgbDateStruct {
    return date ? {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    } : null;
  }

  /**
   * format the date
   * @param date 
   * Format NGBDateStruct to string to that we can save it on server. 
   */
  format(date: NgbDateStruct): string {
    return date
      ? `${this.isNumber(date.month) ? this.padNumber(date.month) : ""}/${this.isNumber(date.day) ? this.padNumber(date.day) : ""}/${
      date.year
      }`
      : "";
  }

  /**
  * Convert given value to integer 
  */
  toInteger(value: any): number {
    return parseInt(`${value}`, 10);
  }

  /**
   * check integer
   * @param value
   * @response null
   */
  isNumber(value: any): value is number {
    return !isNaN(this.toInteger(value));
  }

  /**
   * padding the date to double digits
   * @param value
   * @response null
   */
  padNumber(value: number) {
    if (this.isNumber(value)) {
      return `0${value}`.slice(-2);
    } else {
      return "";
    }
  }
  /**
     * Select all users
     */

  selectAll(type: string = 'users') {
    if (type == 'users') {
      this.selectedUsers = [];
      this.users.map((user, i) => {
        user.IsSelected = this.selectedAllUsers;
        if (this.selectedAllUsers)
          this.selectedUsers[i] = user;
      });
    }
    else {
      this.selectedTeams = [];
      this.teams.map((team, i) => {
        team.IsSelected = this.selectedAllTeams;
        if (this.selectedAllTeams)
          this.selectedTeams[i] = team;
      });
    }
  }

  /**
   * check all the selected user
   * @param null
   * @response null
   */
  checkAllSelected() {
    let usersData: any;
    usersData = this.users.filter(m => m.IsSelected == false);
    this.selectedAllUsers = true;
    this.selectedAllTeams = true;
    if (usersData && usersData.length > 0) {
      this.selectedAllUsers = false;
    }
    let teamsData: any;
    teamsData = this.teams.filter(m => m.IsSelected == false);

    if (teamsData && teamsData.length > 0) {
      this.selectedAllTeams = false;
    }
  }

  ngOnDestroy() {
    this.commonServices.selectedLocation.next({});
  }


}
