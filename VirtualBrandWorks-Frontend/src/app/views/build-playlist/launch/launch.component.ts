import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { BuildPlaylistService } from '../build-playlist.service';
import { PlaylistFolder } from 'src/app/shared/model/playlist-folder';
import { PlaylistDetail } from 'src/app/shared/model/playlist-detail';
import { User } from 'src/app/shared/model/user';
import { Team } from 'src/app/shared/model/team';

@Component({
  selector: 'app-launch',
  templateUrl: './launch.component.html',
  styleUrls: ['./launch.component.scss']
})
export class LaunchComponent implements OnInit {

  playlistId: number;
  playlistData: PlaylistDetail;
  folders: PlaylistFolder[];
  users: User[];
  teams: Team[];
  showLaunchSuccess: boolean = false;
  constructor(private route: ActivatedRoute, private commonServices: CommonService, private buildPlaylistService: BuildPlaylistService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.playlistId) {
        this.playlistId = params.playlistId;
        this.getPlaylistDetails();
        this.getFolders();
        this.getTeamsAndUsers();
      }
    });
  }

  /**
   * get playlist details
   * @param null
   * @response null
   */
  getPlaylistDetails() {
    this.commonServices.loader.next(true);
    this.buildPlaylistService.getPlaylistDetails(this.playlistId).subscribe(data => {
      if (data.Status) {
        this.playlistData = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * get folders
   * @param null
   * @response null
   */
  getFolders() {
    this.commonServices.loader.next(true);
    this.buildPlaylistService.getPlaylistFolders(this.playlistId).subscribe(data => {
      if (data.Status) {
        this.folders = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * get team and users
   * @param null
   * @response null
   */
  getTeamsAndUsers() {
    this.commonServices.loader.next(true);
    this.buildPlaylistService.getAssignedTeamsAndUsers(this.playlistId).subscribe(data => {
      if (data.Status) {
        this.users = data.Data.Users;
        this.teams = data.Data.Teams;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * launch
   * @param null
   * @response null
   */
  launch() {
    this.commonServices.loader.next(true);
    this.buildPlaylistService.launchPlaylist(this.playlistId, true).subscribe(data => {
      if (data.Status) {
        this.showLaunchSuccess = true;
        localStorage.removeItem("playlistId");
        window.scrollTo(0, 0);
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

}
