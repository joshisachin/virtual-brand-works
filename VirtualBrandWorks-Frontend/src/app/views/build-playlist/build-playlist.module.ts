import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuildPlaylistComponent } from './build-playlist.component';
import { HomeComponent } from './home/home.component';
import { BuildPlaylistRoutingModule } from './build-playlist-routing.module';
import { BuildPlaylistService } from './build-playlist.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListComponent } from './list/list.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ContentComponent } from './content/content.component';
import { PlaylistContentComponent } from './playlistcontent/playlistcontent.component';
import { AssignUsersTeamsComponent } from './assign-users-teams/assign-users-teams.component';
import { DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LaunchComponent } from './launch/launch.component';



@NgModule({
  declarations: [BuildPlaylistComponent, HomeComponent, ListComponent, PlaylistContentComponent, ContentComponent, LaunchComponent, AssignUsersTeamsComponent],
  imports: [
    CommonModule,
    BuildPlaylistRoutingModule,
    SharedModule,
    NgbModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe]
})
export class BuildPlaylistModule { }
