import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-build-playlist',
  templateUrl: './build-playlist.component.html',
  styleUrls: ['./build-playlist.component.scss']
})
export class BuildPlaylistComponent implements OnInit {

  toggleRightSide: boolean; // to expand right side when side menu shrinks
  constructor(private commonServices: CommonService) {
    this.commonServices.toggleRightSide.subscribe(data => {
      this.toggleRightSide = data;
    });
  }

  ngOnInit(): void {
  }

}
