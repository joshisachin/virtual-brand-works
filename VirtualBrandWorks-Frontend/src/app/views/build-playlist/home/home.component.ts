import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { environment } from 'src/environments/environment';
import { FolderComponent } from '../../elements/folder/folder.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateComponent } from 'src/app/shared/modals/playlist/create/create.component';
import { BuildPlaylistService } from '../build-playlist.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  folderType = "master"; // send to folder component
  isPlaylist: boolean = true;
  @ViewChild("searchFolder", { static: true }) searchFolder: ElementRef;
  playlistId: number = parseInt(localStorage.getItem("playlistId"));
  getFolderApi: string;
  constructor(private commonService: CommonService, private httpService: HttpService, private modalService: NgbModal, private buildPlaylistService: BuildPlaylistService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeToStep();
    this.setFolderApi();
    this.commonService.searchDebounce(this.searchFolder.nativeElement).subscribe(searchString => {
      this.getSearchData(searchString);
    });
  }

  /**
   * route to step
   * @param null
   * @response null
   */
  routeToStep() {
    if (this.playlistId) {
      this.buildPlaylistService.getStep(this.playlistId).subscribe(data => {
        if (data.Status) {
          this.route.queryParams.subscribe(params => {
            if (params.routeHome) {
              data.Data.StepNumber = 1;
            }
            let route: string;
            switch (data.Data.StepNumber) {
              case 2:
                route = "build-playlist/list/" + this.playlistId;
                break;
              case 3:
                route = "build-playlist/playlist-content/" + this.playlistId;
                break;
              case 4:
                route = "build-playlist/assign-users-teams/" + this.playlistId + "/false";
                break;
              case 5:
                route = "build-playlist/launch/" + this.playlistId;
                break;
              default:
                route = "build-playlist";
                break;
            }
            this.router.navigate([route]);
          });
        }
        else {
          this.commonService.showError(data.Message, "");
        }
      });
    }
  }

  /**
   * set folder api
   * @param null
   * @response null
   */
  setFolderApi() {
    if (this.folderType == "master") {
      this.getFolderApi = "getAllFoldersData";
    }
    else {
      this.getFolderApi = "getCompanyContentFolder";
    }
  }

  /**
   * get search data
   * @param param
   * @response object
   */
  getSearchData(param) {
    this.commonService.searchDataForFolder.next(param);
    this.httpService[this.getFolderApi](param, "", 1, environment.PAGINATE_FETCH).subscribe(data => {
      if (data.Status && data.Data.length > 0) {
        let folders = data.Data.filter((res) => { // filter to separate dynamic folder
          return res.IsDynamicFolder == false;
        });
        FolderComponent.stopScroll.next(false);
        this.commonService.folders.next(folders); // updating the value
      }
    })
  }

  /**
   * open create playlist
   * @param null
   * @response null
   */
  openCreatePlaylist() {
    const modalRef = this.modalService.open(CreateComponent, {
      ariaLabelledBy: 'modal-basic-title',
      windowClass: 'w925',
      centered: true
    });

    modalRef.result.then((result) => {
      this.playlistId = result;
      localStorage.setItem("playlistId", this.playlistId.toString());
    }, (reason) => {
    });
  }

  /**
   * select folder type
   * @param folderType
   * @response null
   */
  selectFolderType(folderType: string) {
    this.commonService.folders.next([]);
    this.folderType = folderType;
    this.setFolderApi();
  }

  /**
   * save playlist
   * @param null
   * @response null
   */
  savePlaylist() {
    this.buildPlaylistService.savePlaylist(this.playlistId).subscribe(data => {
      if (data.Status) {
        this.router.navigate(["build-playlist/list/" + this.playlistId]);
      }
      else {
        this.commonService.showError(data.Message, "");
      }
    });
  }
}
