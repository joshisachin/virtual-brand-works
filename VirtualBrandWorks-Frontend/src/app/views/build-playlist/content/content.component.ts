import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BuildPlaylistService } from '../build-playlist.service';
import { FolderContent } from 'src/app/shared/model/folder-content';
import { Folder } from 'src/app/shared/model/folder';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CompanyContentService } from '../../company-content/company-content.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  playlistFolderId: number;
  playlistId: number;
  type: boolean;
  getContentApi: string;
  folderDetails: FolderContent[];
  folder: Folder;
  constructor(private route: ActivatedRoute, private commonServices: CommonService, private buildPlaylistService: BuildPlaylistService, private router: Router, private companyServices: CompanyContentService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.playlistFolderId = params.id;
        this.playlistId = params.playlistId;
        this.getContents();
      }
    });
  }

  /**
   * get contents
   * @param null
   * @response null
   */
  getContents() {
    this.buildPlaylistService.getPlaylistFolderContent(this.playlistFolderId).subscribe(data => {
      if (data.Status) {
        this.folderDetails = data.Data.Contents;
        this.folder = data.Data.Folder;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * check mime type
   * @param mimeType
   * @response null 
   */
  checkType(mimeType: string) {
    if (mimeType.match("video")) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * select contents
   * @param index
   * @response null
   */
  selectContents(index: number) {
    this.folderDetails[index].IsContentSaved = !this.folderDetails[index].IsContentSaved;
  }

  /**
   * to drag and drop the folder
   * @param event 
   * @response array
   */
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.folderDetails, event.previousIndex, event.currentIndex);
  }

  /**
   * save content
   * @param null
   * @response null
   */
  saveContent() {
    this.commonServices.loader.next(true);
    let saveData: any = [];
    let count: number = 0;
    if (this.folderDetails.length == 0) {
      this.router.navigate(["/build-playlist/list/" + this.playlistId]);
    }
    this.folderDetails.map((data, i) => {
      if (data.IsContentSaved) {
        saveData.push({"PlaylistFolderId": this.playlistFolderId, "PlaylistContentId": data.ContentId, "SortOrder": ++count});
      }
      if (this.folderDetails.length - 1 == i) {
        this.buildPlaylistService.savePlaylistContents(this.playlistId, saveData).subscribe(data => {
          if (data.Status) {
            this.router.navigate(["/build-playlist/list/" + this.playlistId]);
          }
          else {
            this.commonServices.showError(data.Message, "");
          }
          this.commonServices.loader.next(false);
        });
      }
    });
  }

  /**
   * open create content popup
   * @param null
   * @response null
   */
  createContent() {
    this.companyServices.createContentPopup(this.playlistFolderId, this.folder.FolderId).then(data => {
      if (data) {
        this.getContents();
      }
    })
  }
}
