import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { BuildPlaylistService } from '../build-playlist.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { PlaylistFolder } from 'src/app/shared/model/playlist-folder';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  folders: PlaylistFolder[];
  playlistId: number;
  constructor(private buildPlaylistService: BuildPlaylistService, private commonServices: CommonService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.playlistId = params.id;
        this.getFolders(params.id);
      }
    });
  }

  /**
   * get folders
   * @param id
   * @response null 
   */
  getFolders(id: number) {
    this.commonServices.loader.next(true);
    this.buildPlaylistService.getPlaylistFolders(id, true).subscribe(data => {
      if (data.Status) {
        this.folders = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }
  /**
   * to drag and drop the folder
   * @param event 
   * @response array
   */
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.folders, event.previousIndex, event.currentIndex);
    this.buildPlaylistService.updateOrder(this.playlistId, event.item.data.PlayListFolderId, event.currentIndex).subscribe(data => {
      if (data.Status) {
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

}
