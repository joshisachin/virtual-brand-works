import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BuildPlaylistComponent } from './build-playlist.component';
import { ListComponent } from './list/list.component';
import { AssignUsersTeamsComponent } from './assign-users-teams/assign-users-teams.component';
import { ContentComponent } from './content/content.component';
import { PlaylistContentComponent } from './playlistcontent/playlistcontent.component';
import { LaunchComponent } from './launch/launch.component';

const routes: Routes = [
	{
		path: '',
		component: BuildPlaylistComponent,
		children: [
			{
				path: '',
				component: HomeComponent,
			},
			{
				path: 'list/:id',
				component: ListComponent,
			},
			{
				path: 'content/:id/:playlistId',
				component: ContentComponent,
			},
			{
				path: 'playlist-content/:playlistId',
				component: PlaylistContentComponent,
			},
			{
				path: 'assign-users-teams/:playlistId/:isChange',
				component: AssignUsersTeamsComponent,
			},
			{
				path: 'launch/:playlistId',
				component: LaunchComponent,
			},
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class BuildPlaylistRoutingModule { }
