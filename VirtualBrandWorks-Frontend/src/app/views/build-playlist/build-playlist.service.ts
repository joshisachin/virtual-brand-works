import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';
import { Observable } from 'rxjs';

@Injectable()
export class BuildPlaylistService {

  constructor(private http: HttpClient) { }

  /**
   * build playlist
   */
  buildPlaylist(data: any): Observable<any> {
    return this.http.post(API_NAME.CreatePlaylist[0].name, data);
  }

  /**
   * update playlist
   * @param playlistId 
   */
  updatePlaylist(data: any): Observable<any> {
    return this.http.post(API_NAME.UpdatePlaylist[0].name, data);
  }

  /**
   * save playlist
   */
  savePlaylist(playlistId: number): Observable<any> {
    return this.http.post(API_NAME.SavePlaylist[0].name, {"PlayListId": playlistId});
  }

  /**
   * get playlist folders
   */
  getPlaylistFolders(playlistId: number, isAllContentCount: boolean = false): Observable<any> {
    return this.http.get(API_NAME.GetPlaylistFolders[0].name + "?playlistId=" + playlistId + "&isAllContentCount=" + isAllContentCount);
  }

  /**
   * update sort order
   */
  updateOrder(playlistId: number, playlistFolderId: number, sortOrder: number): Observable<any> {
    return this.http.post(API_NAME.UpdatePlaylistSortOrder[0].name, {"playListId": playlistId, "playlistFolderId": playlistFolderId, "destinationSortOrder": sortOrder});
  }

  /**
   * get playlist folder content
   */
  getPlaylistFolderContent(playlistFolderId: number): Observable<any> {
    return this.http.get(API_NAME.GetPlayListContents[0].name + "?PlayListFolderId="+playlistFolderId);
  }

  /**
   * save playlist contents
   */
  savePlaylistContents(playlistId: number, PlaylistContents: any): Observable<any> {
    return this.http.post(API_NAME.savePlaylistContents[0].name, {"playListId": playlistId, PlaylistContents});
  }

  /*
   * get folder contents
   */
  getFoldercontent(playlistFolderId: number): Observable<any> {
    return this.http.get(API_NAME.GetPlayListContentsForAddQuestion[0].name + "?playListFolderId=" + playlistFolderId);
  }

   /**
   * get both team and users from same API
   */
  getTeamsandUsers(playListId:number, locationId:number=0):Observable<any>{
    return this.http.get(API_NAME.GetUsersAndTeamsForAssignPlaylistToMore[0].name+"?playlistId="+playListId+"&locationId="+locationId);
  }

   /**
   * Saving Assigned teams and users
   */
  UpdateTeamsandUsers(playListId:number,users:number[],teams:number[],startDate:string,endDate:string):Observable<any>{
    return this.http.post(API_NAME.AssignPlaylistTo[0].name,{"PlayListId": playListId,"Users":users,"Teams":teams,"StartDate":startDate,"EndDate":endDate});
  }
  /**
   * turn off existing quiz
   */
  turnOffExistingQuiz(playlistContentMasterId: number, isTurnedOffExistingQuiz: boolean): Observable<any> {
    return this.http.post(API_NAME.TurnOffExistingQuiz[0].name, {"PlaylistContentMasterId": playlistContentMasterId, "IsTurnedOffExistingQuiz": isTurnedOffExistingQuiz});
  }

  /**
   * turn off all existing quiz
   */
  turnOffExistingQuizForAll(playlistFolderId: number, isTurnedOffExistingQuiz: boolean): Observable<any> {
    return this.http.post(API_NAME.TurnOffExistingQuizForAll[0].name, {"PlaylistFolderId": playlistFolderId, "IsTurnedOffExistingQuiz": isTurnedOffExistingQuiz});
  }

  /**
   * get playlist detail
   */
  getPlaylistDetails(playlistId: number): Observable<any> {
    return this.http.get(API_NAME.GetPlaylistDetails[0].name + "?playlistId=" + playlistId);
  }

  /**
   * get teams and users
   */
  getAssignedTeamsAndUsers(playlistId: number): Observable<any> {
    return this.http.get(API_NAME.GetTeamsAndUsers[0].name + "?playlistId=" + playlistId);
  }

  /**
   * launch the playlist
   */
  launchPlaylist(playlistId: number, isLaunched: boolean): Observable<any> {
    return this.http.post(API_NAME.LaunchPlaylist[0].name, {"PlayListId": playlistId, "IsLaunched": isLaunched});
  }

  /**
   * get step
   */
  getStep(playlistId: number): Observable<any> {
    return this.http.get(API_NAME.GetBuildPlaylistSavedState[0].name + "?playlistId=" + playlistId);
  }

  /**
   * delete playlist
   */
  deletePlaylist(playlistId: number): Observable<any> {
    return this.http.post(API_NAME.DeletePlaylist[0].name, {"PlayListId": playlistId});
  }
  
}
