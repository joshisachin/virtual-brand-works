import { Component, OnInit } from '@angular/core';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { PlaylistService } from '../playlist.service';

@Component({
  selector: 'app-my-playlist-detail',
  templateUrl: './my-playlist-detail.component.html',
  styleUrls: ['./my-playlist-detail.component.scss']
})
export class MyPlaylistDetailComponent implements OnInit {

  playlistDetail: string = 'userPlaylistDetail';
  loggedInUserRole: string;
  activeId: number;

  constructor(
    private _location: Location,
    public playlistService: PlaylistService
  ) { }

  ngOnInit(): void {
    this.loggedInUserRole = localStorage.getItem("userRole");
    this.activeId = this.playlistService.playlistDetailTabIndex;
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {   
    this.activeId = changeEvent.nextId;
  }

  /**
   * close the page
   * @param null
   * @response null
   */
  close() {    
    this._location.back();
  }

}
