import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';

@Injectable()
export class PlaylistService {

	apiName = API_NAME;
	profileTabIndex: number = 1;
	playlistTabIndex: number = 1;
	playlistDetailTabIndex: number = 1; // Playlist tab index set for detail, teams and users, message

	constructor(private http: HttpClient) { }

	/**
	 * Get created playlist as per status published or un-published for Trainer only
	 * @param data
	 */
	getUserCreatedPlaylistList(data: any): Observable<any> {
		return this.http.post(this.apiName.GetUserCreatedPlaylistList[0].name, data);
	}

	/**
	 * Get assigned playlist for user and trainer
	 * @param data
	 */
	getUserAssignedPlaylistList(data: any): Observable<any> {
		return this.http.post(this.apiName.GetUserAssignedPlaylistList[0].name, data);
	}

	/**
	* get associated playlist detail
	* @param playlistId 
	*/
	getAssociatedPlaylistDetail(playlistId, isSelfCreated: boolean = false, userId: number = 0): Observable<any> {
		return this.http.get(this.apiName.PlaylistDetailView[0].name + "?playlistId=" + playlistId + "&isSelfCreated=" + isSelfCreated + "&userId=" + userId);
	}

	/**
	* Get user playlist as per isCompleted check
	* @param isCompleted 
	*/
	getMyPlaylist(isCompleted): Observable<any> {
		return this.http.get(this.apiName.GetMyPlaylist[0].name + "?isCompleted=" + isCompleted);
	}

	/**
	* Get top performers list of playlist
	* @param playListId 
	*/
	getPlaylistTopPerformer(playListId: number, isTopTen: boolean): Observable<any> {
		return this.http.get(this.apiName.GetPlaylistTopPerofermers[0].name + "?playListId=" + playListId + "&isTopTen=" + isTopTen);
	}

	/**
	 * get result folder and detail
	 * @param playlistId 
	 */

	getResultFolderAndDetail(playlistId, userId:number): Observable<any> {
		return this.http.get(this.apiName.ViewResultDetailAndFolders[0].name + "?playlistId=" + playlistId + "&userId="+userId);
	}

	/**
	 * Get result content
	 * @param playListFolderId 
	 * @param userId 
	 */

	viewResultContentList(playListFolderId, userId): Observable<any> {
		return this.http.get(this.apiName.ViewResultContentList[0].name + "?playListFolderId=" + playListFolderId + "&userId=" + userId);
	}

	/**
	 * get my playlist folder detail
	 * @param playListFolderId 
	 */
	getMyPlaylistFolderDetail(playListFolderId, playlistContentMasterId): Observable<any> {
		return this.http.get(this.apiName.GetMyPlaylistFolderDetail[0].name + "?playListFolderId=" + playListFolderId + "&playlistContentMasterId=" + playlistContentMasterId);
	}

	/**
	 * get my playlist content list 
	 */
	getMyPlaylistContentList(playListFolderId, playlistContentId): Observable<any> {
		return this.http.get(this.apiName.GetMyPlaylistFolderContentList[0].name + "?playListFolderId=" + playListFolderId + "&playlistContentId=" + playlistContentId);
	}


	/**
	* get quiz result
	* @param data 
	*/
	getQuizResult(data): Observable<any> {
		return this.http.get(this.apiName.GetQuizResult[0].name + "?quizId=" + data.QuizId + "&playlistId=" + data.playlistId + "&userId=" + data.userId + "&isMasterQuiz=" + data.isMasterQuiz);
	}

	/**
	* get teams and users
	*/
	getPlayListUsers(playlistId: number): Observable<any> {
		return this.http.get(API_NAME.GetPlaylistUsers[0].name + "?playListId=" + playlistId);
	}

	/**
	 * receive messages
	 */
	receiveMessages(playlistId: number, senderId: number): Observable<any> {
		return this.http.post(API_NAME.ReceiveMessage[0].name, { "PlayListId": playlistId, "SenderId": senderId });
	}

	/**
	 * send message 
	 */
	sendMessage(playListId: number, message: string, receiverId: number, messageType: number, imageMessage: string): Observable<any> {
		return this.http.post(API_NAME.SendMessage[0].name, { "PlayListId": playListId, "ReceiverId": receiverId, "Message": message, "MessageType": messageType, "ImageMessage": imageMessage });
	}

	/**
	 * get my created playlist
	 */
	getMyCreatedPlaylist(isLaunched: boolean): Observable<any> {
		return this.http.get(this.apiName.GetMyCreatedPlaylist[0].name + "?isLaunched=" + isLaunched)
	}

	getPlayListAssociatedUsers(playListId:number) : Observable<any>{
		return this.http.get(this.apiName.GetPlaylistAssociatedTeams[0].name + "?playListId=" + playListId);
	}

	getMyPlaylistAssociatedTeamsAndUsersResult(playListId:number, teamId:number, isTeam:boolean): Observable<any> {
		return this.http.get(this.apiName.AssociatedUsers[0].name + "?playListId=" + playListId + '&teamId='+teamId + '&isTeam='+isTeam);
	}

	/**
	 * clone playlist
	 */
	clonePlaylist(playlistId: number): Observable<any> {
		return this.http.post(this.apiName.ClonePlaylist[0].name, {"PlayListId": playlistId});
	}


}
