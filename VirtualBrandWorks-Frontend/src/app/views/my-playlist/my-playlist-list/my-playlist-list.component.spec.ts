import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPlaylistListComponent } from './my-playlist-list.component';

describe('MyPlaylistListComponent', () => {
  let component: MyPlaylistListComponent;
  let fixture: ComponentFixture<MyPlaylistListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPlaylistListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPlaylistListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
