import { Component, OnInit } from '@angular/core';
import { NgbNavChangeEvent, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaylistService } from '../playlist.service';
import { CreateComponent } from 'src/app/shared/modals/playlist/create/create.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-playlist-list',
  templateUrl: './my-playlist-list.component.html',
  styleUrls: ['./my-playlist-list.component.scss']
})
export class MyPlaylistListComponent implements OnInit {

  playlistType: string = 'myPlaylist';
  createdPlaylistType: string = 'createdPlaylist';
  loggedInUserRole: string;
  activeId: number = 1;

  constructor(
    public playlistService: PlaylistService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loggedInUserRole = localStorage.getItem("userRole");
    this.activeId = this.playlistService.playlistTabIndex;
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {  
    this.playlistService.playlistTabIndex = changeEvent.nextId;   
    this.activeId = changeEvent.nextId;
  }

  /**
   * open create playlist
   * @param null
   * @response null
   */
  openCreatePlaylist() {
    const modalRef = this.modalService.open(CreateComponent, {
      ariaLabelledBy: 'modal-basic-title',
      windowClass: 'w925',
      centered: true
    });

    modalRef.result.then((result) => {
      localStorage.setItem("playlistId", result.toString());
      this.router.navigate(["/build-playlist"]);
    }, (reason) => {
    });
  }
}
