import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPlaylistMainComponent } from './my-playlist-main.component';

describe('MyPlaylistMainComponent', () => {
  let component: MyPlaylistMainComponent;
  let fixture: ComponentFixture<MyPlaylistMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPlaylistMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPlaylistMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
