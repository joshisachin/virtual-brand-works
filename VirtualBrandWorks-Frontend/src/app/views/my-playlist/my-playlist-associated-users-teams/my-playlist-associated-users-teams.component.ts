import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { TeamUser } from 'src/app/shared/model/team-user';
import { Team } from 'src/app/shared/model/team';
import { PlaylistService } from 'src/app/views/my-playlist/playlist.service';
import { MyPlaylistAssociatedUsersTeams, FolderResult, PlaylistUsers } from 'src/app/shared/model/myplaylist-associated-users-teams';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewResultComponent } from 'src/app/shared/modals/playlist/view-result/view-result.component';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-my-playlist-associated-users-teams',
  templateUrl: './my-playlist-associated-users-teams.component.html',
  styleUrls: ['./my-playlist-associated-users-teams.component.scss']
})
export class MyPlaylistAssociatedUsersTeamsComponent implements OnInit {

  playlistId: number;
  users: TeamUser[];
  teams: Team[];
  isActive: boolean = false;
  myPlaylistAssociatedUsersTeams: MyPlaylistAssociatedUsersTeams;
  playListUsers: PlaylistUsers[];
  folderNames: FolderResult[] = [];
  isTeam: boolean = true;
  selectedIndex: number = 0;

  constructor(private commonServices: CommonService, private route: ActivatedRoute, private playListService: PlaylistService,  private modalService: NgbModal) { }

  ngOnInit(): void {
    //Get the playlistId from query string 
    this.route.params.subscribe(params => {
      if (params.playlistId) {
        this.playlistId = params.playlistId;
        this.getTeamsAndUsers();
      }
    });
  }

  /**
  * get the teams,user object, startdate, enddate from web api. 
  * @param null
  * @response null
  */
  getTeamsAndUsers() {
    this.commonServices.loader.next(true);
    this.commonServices.selectedLocation.subscribe(value => {
      this.playListService.getPlayListAssociatedUsers(this.playlistId).subscribe(data => {
        if (data.Status) {
          this.teams = data.Data;
          this.getAssociatedTeamandUserResult(this.teams[0].TeamId, true);
          this.commonServices.loader.next(false);
        }
        else { 
          this.myPlaylistAssociatedUsersTeams = null;
          this.commonServices.showError(data.Message, "");
          this.commonServices.loader.next(false);
        }
      });
    })
  }

  /**
  * On click of team from let panel we 
  * load all teams users with their top 5 folders results
  * @param index clicked index of team
  * @response null
  */
  getTeamData(index: number) {
    this.selectedIndex = index;
    this.getAssociatedTeamandUserResult(this.teams[index].TeamId, true);
  }

  /**
  * Get the Users and teams related to a playlist id
  * @param teamId Team id to get the users
  * @param isTeam If we select Users in radio filter then we load all users data 
  * corresponding to all teams.
  * @response null
  */
  getAssociatedTeamandUserResult(teamId: number, isTeam: boolean) {
    this.commonServices.loader.next(true);
    this.playListService.getMyPlaylistAssociatedTeamsAndUsersResult(this.playlistId, teamId, isTeam).subscribe(data => {
      if (data.Status) {
        this.myPlaylistAssociatedUsersTeams = data.Data;
        this.folderNames = this.myPlaylistAssociatedUsersTeams.PlaylistUsers[0].FolderResult;
        this.commonServices.loader.next(false);
      }
      else {
        this.commonServices.showError(data.Message, "");
        this.commonServices.loader.next(false);
      }
    });
  }

  /**
  * On Users and Team radio filter event change this method will be called
  * @param value Value of the radio.
  * @response null
  */
  onItemChange(value) {
    if (value == "user") {
      this.isTeam = false;
      this.selectedIndex = 0;
      this.getAssociatedTeamandUserResult(0, this.isTeam);
    }
    else {
      this.isTeam = true;
      this.selectedIndex = 0;
      this.getTeamsAndUsers();
    }
  }

  /**
  * Open the results popup corresponding to any userid
  * @param userId user id of the user which result need to be open.
  * @response null
  */
  viewResult(userId:number) {
    if (this.folderNames.length > 0) {
      const modalRef = this.modalService.open(ViewResultComponent, {
        windowClass: 'resultWindow',
        size: 'xl',
        centered: true
      });
      modalRef.componentInstance.data = {
        playlistId: this.playlistId,
        userId: userId
      };
      modalRef.result.then((result) => { 
      }, (reason) => { });
    }
    else {
      this.commonServices.showError(API_MESSAGES.AssociatedUsersTeams.NoFoldersFound, "");
    } 
  }

  /**
  * sort the result on the basis of totalscore
  * @param null
  * @response null
  */
  sort() {
      this.commonServices.performSorting(this.myPlaylistAssociatedUsersTeams.PlaylistUsers, "TotalScore");
  }
}
