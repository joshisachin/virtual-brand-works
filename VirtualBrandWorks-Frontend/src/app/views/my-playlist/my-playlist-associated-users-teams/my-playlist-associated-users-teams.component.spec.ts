import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPlaylistAssociatedUsersTeamsComponent } from './my-playlist-associated-users-teams.component';

describe('MyPlaylistAssociatedUsersTeamsComponent', () => {
  let component: MyPlaylistAssociatedUsersTeamsComponent;
  let fixture: ComponentFixture<MyPlaylistAssociatedUsersTeamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPlaylistAssociatedUsersTeamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPlaylistAssociatedUsersTeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
