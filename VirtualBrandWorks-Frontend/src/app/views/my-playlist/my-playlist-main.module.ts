import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MyPlaylistMainRoutingModule } from './my-playlist-main-routing.module';
import { MyPlaylistMainComponent } from './my-playlist-main.component';
import { NgDynamicBreadcrumbModule } from 'ng-dynamic-breadcrumb';

import { MyPlaylistListComponent } from './my-playlist-list/my-playlist-list.component';
import { MyPlaylistDetailComponent } from './my-playlist-detail/my-playlist-detail.component';
import { MyPlaylistContentDetailComponent } from './my-playlist-content-detail/my-playlist-content-detail.component';
import { PlaylistService } from './playlist.service';
import { MyPlaylistMessageComponent } from './my-playlist-message/my-playlist-message.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MyPlaylistAssociatedUsersTeamsComponent } from './my-playlist-associated-users-teams/my-playlist-associated-users-teams.component';
import { TopPerformerPageComponent } from './top-performer/top-performer-page.component';

@NgModule({
    declarations: [
        MyPlaylistMainComponent,
        MyPlaylistListComponent,
        MyPlaylistDetailComponent,
        MyPlaylistContentDetailComponent,
        MyPlaylistMessageComponent,
        MyPlaylistAssociatedUsersTeamsComponent,
        TopPerformerPageComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule,
        MyPlaylistMainRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule,
        InfiniteScrollModule,
        NgDynamicBreadcrumbModule,
        MatProgressBarModule
    ],
    providers: [
        PlaylistService,
        DatePipe
    ]
})
export class MyPlaylistMainModule { }
