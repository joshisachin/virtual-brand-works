import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPlaylistMessageComponent } from './my-playlist-message.component';

describe('MyPlaylistMessageComponent', () => {
  let component: MyPlaylistMessageComponent;
  let fixture: ComponentFixture<MyPlaylistMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPlaylistMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPlaylistMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
