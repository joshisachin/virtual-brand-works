import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { User } from 'src/app/shared/model/user';
import { message } from 'src/app/shared/model/message';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { ActivatedRoute } from '@angular/router';
import { PlaylistService } from '../playlist.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-chat-message',
  templateUrl: './my-playlist-message.component.html',
  styleUrls: ['./my-playlist-message.component.scss']
})
export class MyPlaylistMessageComponent implements OnInit {
  @ViewChild('scrollBottom') scrollDiv: ElementRef;

  //Playlist id need to be changed 
  playListId: number;
  senderId: number;
  users: User[];
  messages: message[];
  message: string;
  imageMessage: string;
  loginId: string;
  userName: string;
  userId: string = localStorage.getItem("userId");
  sendBtnDisabled: boolean = true;
  //This variable is to highlight the user when it clicked. 
  //By default it was for 0.
  selectedIndex: number;
  fileToUpload: File = null;
  //Message type by default is 2 for text. If user send image then it will be 1.
  messageType: number = 2;
  currentDate:string;

  constructor(private commonServices: CommonService, private playlistServices: PlaylistService, private route: ActivatedRoute,private datepipe:DatePipe) { 
    var date = new Date();
    this.currentDate = this.datepipe.transform(date,"yyyy-MM-dd");
  }

  //This variable is used to maintain the left user highligted
  //Because after binding left pane selected to first user.
  activeReceiverId: number;
  activeIndex: number = 0;
  //Variable for button color when image upload. And ready to send message.
  //By Default blank. 
  btnColour = '';
  sendBtnColor = '';
  msgGroups:{
    date:string,
    msgs:message[]
  }[];


  ngOnInit(): void {
    //Get the login id from cookie to get the sender
    this.loginId = localStorage.getItem("userId");
    //Get the playlistId from query string 
    this.route.params.subscribe(params => {
      if (params.playlistId) {
        this.playListId = params.playlistId;
        //Get the list of users for left pane.
        this.getPlayListUsers();
      }
    });
  }

  //This ngAfterViewChecked was called because we need to scroll down the bottom of the chat after 
  //messages rendered on view
  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  /**
  * Get the users chat when clicked 
  * @param index : The selected index
  * @response User chat set
  */
  getUserChat(index: number, userName: string) {
    this.users[index].UnreadMessageCount = 0;
    this.userName = userName;
    this.activeReceiverId = this.users[index].UserId;
    this.activeIndex = index;
    this.commonServices.loader.next(true);
    this.playlistServices.receiveMessages(this.playListId, this.activeReceiverId).subscribe(data => {
      if (data.Status) {
        this.messages = data.Data;
        this.selectedIndex = index;

        this.formatMessagesInDateGroups(this.messages);

        //this.commonServices.showSuccess(API_MESSAGES.Playlist.userTeamAddedSuccess, "");
      }
      else {
        this.commonServices.showError(data.Message, "");
      };
      this.commonServices.loader.next(false);
    });
  }

  /**
  * Get the users in the left pane
  * @param null
  * @response Users list set
  */
  getPlayListUsers() {
    this.commonServices.loader.next(true);
    this.playlistServices.getPlayListUsers(this.playListId).subscribe(data => {
      if (data.Status) {
        this.users = data.Data;
        this.getUserChat(this.activeIndex, this.users[0].FirstName + ' ' + this.users[0].LastName)
        this.commonServices.loader.next(false);
      }
      else {
        this.commonServices.showError(data.Message, "");
        this.commonServices.loader.next(false);
      }
    });
  }

  /**
  * Send Messages
  * @param null
  *  
  */
  sendMessage() {
    this.message = this.message?.trim();
    if ((this.message == undefined || this.message == '') && (this.imageMessage == undefined || this.imageMessage == '')) {
      this.commonServices.showError(API_MESSAGES.Messages.MessageRequired, "");
      return;
    }
    this.commonServices.loader.next(true);
    this.playlistServices.sendMessage(this.playListId, this.message, this.activeReceiverId, this.messageType, this.imageMessage).subscribe(data => {
      if (data.Status) {
        this.getUserChat(this.activeIndex, this.userName);
        this.message = '';
        this.btnColour = '';
        this.imageMessage = '';
        this.messageType = 2;
        this.sendBtnDisabled = true;
        this.sendBtnColor = '';

      }
      else {
        this.commonServices.showError(data.Message, "");
      };
      this.commonServices.loader.next(false);
    });
  }

  /**
   * Validation and Set the file to upload in message
   * @param file object
   *  
   */
  handleFileInput(files: any) {
    let imageExt = files.item(0).name.split('.').pop();
    if (!imageExt.match(/(\jpg|\png|\JPG|\PNG|\jpeg|\JPEG)$/)) {
      this.commonServices.showError(API_MESSAGES.Messages.FileUpload, "");
      return;
    }
    this.fileToUpload = files.item(0);
    const reader = new FileReader();
    reader.readAsDataURL(files.item(0));
    reader.onload = () => {
      this.imageMessage = reader.result.toString().split(',')[1];
      this.messageType = 1;
      this.btnColour = 'blue';
      this.sendBtnColor='blue';
      this.sendBtnDisabled = false;
    };
  }

  /**
   * enter to send message
   * @param event
   * @response null
   */
  enterToSend(event: any) {
    if (this.message != '') {
      this.sendBtnDisabled = false;
      this.sendBtnColor = 'blue';
    }
    else {
      this.sendBtnDisabled = true;
      this.sendBtnColor = '';
    }

    if (event.keyCode === 13) {
      this.sendMessage();
    }
  }

  /**
  * Method to scroll bottom of the chat
  * @param null
  *  
  */
  scrollToBottom(): void {
    try {
      this.scrollDiv.nativeElement.scrollTop = this.scrollDiv.nativeElement.scrollHeight;
    } catch (err) { }
  }

  /**
  * Grouping of messages according to date
  * @param messages : Messages list that need to be grouped
  *  
  */
  formatMessagesInDateGroups(messages:any) {
    // this gives an object with dates as keys
    const groups = messages.reduce((groups, game) => {
      const date = game.Date.split('T')[0];
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(game);
      return groups;
    }, {});

    // to add it in the array format instead
    const groupArrays = Object.keys(groups).map((date) => {
      return {
        date,
        msgs: groups[date]
      };
    });
    this.msgGroups = groupArrays;
  }
}
