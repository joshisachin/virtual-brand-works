import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
	selector: 'app-my-playlist-main',
	templateUrl: './my-playlist-main.component.html',
	styleUrls: ['./my-playlist-main.component.scss']
})
export class MyPlaylistMainComponent implements OnInit {

  	toggleRightSide: boolean; // to expand right side when side menu shrinks
  
  	constructor(private commonService: CommonService) { 
    	this.commonService.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  	}

	ngOnInit(): void {
	}

}
