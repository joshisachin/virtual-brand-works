import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopPerformerPageComponent } from './top-performer-page.component';

describe('TopPerformerPageComponent', () => {
  let component: TopPerformerPageComponent;
  let fixture: ComponentFixture<TopPerformerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopPerformerPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopPerformerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
