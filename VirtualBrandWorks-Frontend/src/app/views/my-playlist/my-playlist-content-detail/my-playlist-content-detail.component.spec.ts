import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPlaylistContentDetailComponent } from './my-playlist-content-detail.component';

describe('MyPlaylistContentDetailComponent', () => {
  let component: MyPlaylistContentDetailComponent;
  let fixture: ComponentFixture<MyPlaylistContentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPlaylistContentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPlaylistContentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
