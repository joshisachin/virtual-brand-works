import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyPlaylistMainComponent } from './my-playlist-main.component';
import { MyPlaylistListComponent } from './my-playlist-list/my-playlist-list.component';
import { MyPlaylistDetailComponent } from './my-playlist-detail/my-playlist-detail.component';
import { MyPlaylistContentDetailComponent } from './my-playlist-content-detail/my-playlist-content-detail.component';
import { TopPerformerPageComponent } from './top-performer/top-performer-page.component';

const routes: Routes = [
	{
		path: '',
		component: MyPlaylistMainComponent,
		children: [
			{
				path: '',
				component: MyPlaylistListComponent,
			},
			{
				path: ':name/:playlistId/:isSelfCreated',
				component: MyPlaylistDetailComponent,
				data: {
					breadcrumb: [
						{
							label: 'My Playlist',
							url: '/my-playlist',
						},
						{
							label: '{{ name }}',
							url: '',
						}
					]
				},
			},
			{
				path: ':name/:playlistId/:playlistName/:folderPlaylistId',
				component: MyPlaylistContentDetailComponent,
				data: {
					breadcrumb: [
						{
							label: 'My Playlist',
							url: '/my-playlist',
						},
						{
							label: '{{ name }}',
							url: '/my-playlist/:name/:playlistId/false',
						},
						{
							label: '{{ playlistName }}',
							url: '',
						}
					]
				},
			},
			{
				path: 'top-performer/:playlistId',
				component: TopPerformerPageComponent,
			},
			// {
			// 	path: 'profile/:userId/:role',
			// 	component: ProfileComponent
			// },
			// {
			// 	path: 'profile/:userId',
			// 	component: ProfileComponent
			// },
			// {
			// 	path: 'playlist/:userId/:role/:playlistId',
			// 	component: ProfileComponent,
			// },
			// {
			// 	path: 'playlist/:userId/:playlistId',
			// 	component: ProfileComponent,
			// }
		]
	}
]
@NgModule({
	imports: [
        RouterModule.forChild(routes)
    ],
	exports: [
        RouterModule
    ]
})
export class MyPlaylistMainRoutingModule { }
