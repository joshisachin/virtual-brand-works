import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { API_NAME } from '../../shared/constants/apiname/apiname';

@Injectable({
  	providedIn: 'root'
})
export class ChangePasswordService {
	apiName = API_NAME;
	constructor(private http: HttpClient) { }

	/**
   * Method to change password
   * @param FromData
   * @response null
   */
	  
	changePassword(data: any): Observable<any> {
        return this.http.post(this.apiName.ChangePassword[0].name, data);
    }
}
