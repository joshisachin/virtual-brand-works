import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { change_password_VALIDATION_MESSAGES } from '../../shared/constants/validations/validation';
import { ConfirmpasswordDirective } from 'src/app/shared/directive/confirmpassword.directive';
import { ChangePasswordService } from './change-password.service';
import { API_MESSAGES } from '../../shared/constants/message/message';

@Component({
	selector: 'app-change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

	changePasswordForm: FormGroup; // form array
	validationMessages: any; // validation messages
	isFormValid: boolean = true; 
	toggleRightSide: boolean; // to expand right side when side menu shrinks
	
  	constructor(
		private formBuilder: FormBuilder,
		private commonService: CommonService, 
		private changePasswordService: ChangePasswordService,
  	) {
		this.commonService.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
		this.changePasswordForm = this.formBuilder.group({
			OldPassword: ['', Validators.compose([
				Validators.required,
			])],
			NewPassword: ['', Validators.compose([
				Validators.minLength(8),
				Validators.maxLength(15),
				Validators.required,
				Validators.pattern('^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*+=]).*$'),
			])],
			ConfirmPassword: ['', Validators.compose([
				Validators.required,
			])],
		}, { validators: ConfirmpasswordDirective("NewPassword", "ConfirmPassword") });
		this.validationMessages = change_password_VALIDATION_MESSAGES;
	}

  	ngOnInit() {
	}

	/**
	 * Change password
	 * @param isValid 
	 * @response null
	 */
	changePassword(isValid) {
		this.isFormValid = isValid;
		if(this.isFormValid) {
			this.commonService.loader.next(true);
			let data = {};
			data["UserId"] = localStorage.getItem("userId");
			data["OldPassword"] = this.changePasswordForm.value.OldPassword;
			data["NewPassword"] = this.changePasswordForm.value.NewPassword;
			data["ConfirmPassword"] = this.changePasswordForm.value.ConfirmPassword; 
			this.changePasswordService.changePassword(JSON.stringify(data)).subscribe(data => {
				if(data.Status) {
					this.commonService.showSuccess(API_MESSAGES.ChangePassword.successMessage, "");
					this.changePasswordForm.reset()
					this.commonService.loader.next(false);
				} else {
					this.commonService.showError(data.Message, "");
					this.commonService.loader.next(false);
				}
			})
		}
	}

}

