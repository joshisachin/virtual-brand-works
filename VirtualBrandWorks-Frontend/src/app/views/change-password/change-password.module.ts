import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordService } from './change-password.service';
import { ChangePasswordComponent } from './change-password.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ChangePasswordComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule,
        ChangePasswordRoutingModule,
        NgbModule,
        ReactiveFormsModule,
        FormsModule
    ],
    providers: [
        ChangePasswordService        
    ]
})
export class ChangePasswordModule { }
