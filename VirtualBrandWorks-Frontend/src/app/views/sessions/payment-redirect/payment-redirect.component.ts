import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HeaderpreComponent } from "../../../views/elements/headerpre/headerpre.component"
import { timer } from 'rxjs';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { UserTabSevice } from '../../users/user/user.service';

@Component({
  selector: 'app-payment-redirect',
  templateUrl: './payment-redirect.component.html',
  styleUrls: ['./payment-redirect.component.scss']
})
export class PaymentRedirectComponent implements OnInit {

  showSuccess: boolean = false; // to check if the payment succeeded or not
  sessionId: string; // session id stored in the local storage
  name: string; // name stored in the local storage
  amount: string; // amount stored in the local storage
  isCardChangeApi: boolean = false;
  constructor(private route: ActivatedRoute, private httpService: HttpService, private commonService: CommonService, private router: Router,private userService: UserTabSevice) {
    this.sessionId = localStorage.getItem("sessionId");
    this.name = localStorage.getItem("name");
    this.amount = localStorage.getItem("amount");
    this.route.queryParams.subscribe(params => {
      this.isCardChangeApi = false;
      if (params.cardChange == "true") {
        this.isCardChangeApi = true
      } 
      if (params.type == "success") {
        this.showSuccess = true;
        this.successUpdate();
      }
      else {
        this.cancelUpdate();
      }
    });
  }

  /**
   * sucess update of payment
   * @param null
   * @response null
   */
  successUpdate() {
    let targetApi = this.isCardChangeApi ? "updatePaymentChange" : "successUpdate";
    this.httpService[targetApi](this.sessionId).subscribe(data => {
      if (data) {
        this.commonService.showSuccess(API_MESSAGES.Payment.successMessage, "Success");
        this.removeLocalStorage();
      }
    });
  }

  /**
   * cancel update of payment
   * @param null
   * @response null
   */
  cancelUpdate() {
    let cancelApi = this.isCardChangeApi ? "updatePaymentChangeFailure" : "cancelUpdate";
    this.httpService[cancelApi](this.sessionId).subscribe(data => {
      if (data) {
        this.commonService.showError(API_MESSAGES.Payment.cancelMessage, "");
        this.removeLocalStorage();
      }
    });
  }

  /**
   * remove local storage after payment
   * @param null
   * @response null
   */
  removeLocalStorage() {
    let url = this.isCardChangeApi ? "/my-profile/profile/" + localStorage.getItem("userId") : "/login";
    localStorage.removeItem("sessionId");
    localStorage.removeItem("name");
    localStorage.removeItem("amount");
    let timerSchedule = timer(4000);
    timerSchedule.subscribe(() => {
      this.router.navigate([url]); // redirect to login
    });
  }

  ngOnInit(): void {
  }

}
