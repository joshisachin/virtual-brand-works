import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';

@Component({
  selector: 'app-content-view',
  templateUrl: './content-view.component.html',
  styleUrls: ['./content-view.component.scss']
})
export class ContentViewComponent implements OnInit {
  contentId: number;
  type: number;
  data: any;
  constructor(private route: ActivatedRoute, private httpService: HttpService, private commonServices: CommonService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.contentId = params.id;
        this.type = params.type;
        this.getContent();
      }
    });
  }

  /**
   * get content
   * @param null
   * @response object
   */
  getContent() {
    this.httpService.getContent(this.contentId, this.type).subscribe(data => {
      if (data.Status) {
        this.data = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

}
