import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { Router, NavigationExtras } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/validation';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup; // form array
  validationMessages: any; // validation messages array
  constructor(private formBuilder: FormBuilder, private httpService: HttpService, private commonService: CommonService, private router: Router) {
    this.forgotPasswordForm = this.formBuilder.group({
			Email: ['', Validators.compose([
				Validators.required,
				Validators.pattern('^ *[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5} *$')
			])]
		});
		this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void { }

  /**
   * forgot password
   * @param formData
   * @response null 
   */
  forgotPassword(formData: FormData) {
    if (formData) {
      this.commonService.trimArray(formData).then(data => {
        formData = data;
        this.httpService.forgotPassword(formData).subscribe(data => {
          if (data.Status) {
            let navigationExtras: NavigationExtras = {
              state: {
                email: formData["Email"],
              }
            };
            this.router.navigate(["/resetpassword"], navigationExtras); // sending hidden email param for reset forward
            this.commonService.showSuccess(API_MESSAGES.ResetPasswordCode.successMessage, "")
          }
          else {
            this.commonService.showError(data.Message, "");
          }
        });
      });
    }
  }

}
