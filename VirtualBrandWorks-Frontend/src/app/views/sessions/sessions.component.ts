import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {
  token: string;
  constructor(private cookieServices: CookieService, private router: Router) { }

  ngOnInit(): void {
    this.token = this.cookieServices.get("token");
  }

}
