import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from '../../shared/services/authguard/can-deactivate-guard.service';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { SessionsComponent } from './sessions.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { PaymentRedirectComponent } from './payment-redirect/payment-redirect.component';
import { HomepageComponent } from './home/homepage.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TncComponent } from './tnc/tnc.component';
import { SessionGuard } from 'src/app/shared/services/authguard/session.guard'
import { ContentViewComponent } from './content-view/content-view.component';
import { HomeNewComponent} from './home-new/home-new.component';

const routes: Routes = [
	{
		path: '',
		component: SessionsComponent,
		children: [
			{
				path: 'home',
				component: HomeNewComponent,
				canActivate: [SessionGuard]
			},
			{
				path: 'home-new',
				component: HomeNewComponent,
				canActivate: [SessionGuard]
			},
			{
				path: 'login',
				component: SigninComponent,
				canActivate: [SessionGuard]
			},
			{
				path: 'signup',
				component: SignupComponent,
				canDeactivate: [CanDeactivateGuard],
				canActivate: [SessionGuard]
			},			
			{
				path: 'forgotpassword',
				component: ForgotpasswordComponent,
				canActivate: [SessionGuard]
			},
			{
				path: 'resetpassword',
				component: ResetpasswordComponent,
				canActivate: [SessionGuard]
			},
			{
				path: 'paymentRedirectUrl',
				component: PaymentRedirectComponent,
			},
			{
				path: "privacypolicy",
				component: PrivacyPolicyComponent,
				canActivate: [SessionGuard]
			},
			{
				path: "tnc",
				component: TncComponent,
				canActivate: [SessionGuard]
			},
			{
				path: "contentView/:id/:type",
				component: ContentViewComponent,
			}
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SessionsRoutingModule { }
