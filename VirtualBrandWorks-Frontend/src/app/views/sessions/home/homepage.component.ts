import { Component, OnInit } from '@angular/core';
import { HeaderpreComponent } from "../../../views/elements/headerpre/headerpre.component";
import { PlansComponent } from "../../elements/plans/plans.component";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VALIDATION_MESSAGES } from 'src/app/shared/constants/validations/validation';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { environment } from "../../../../environments/environment";
import { Router } from '@angular/router';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  writeToUsForm: FormGroup; // form array
  validationMessages: any; // validation messages array
  facebookLink: string = environment.FACEBOOK_LINK; // facebook link
  instagramLink: string = environment.INSTAGRAM_LINK; // instagram links
  twitterLink: string = environment.TWITTER_LINK; // twitter link
  linkedinLink: string = environment.LINKEDIN_LINK; // linked in link

  constructor(private formBuilder: FormBuilder, private httpService: HttpService, private commonService: CommonService, private router: Router) {
    this.writeToUsForm = this.formBuilder.group({
      Email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^ *[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5} *$'),
        Validators.maxLength(100)
      ])],
      Message: ['', Validators.compose([
        Validators.required,
        Validators.pattern('(?!^ +$)^.+$'),
        Validators.maxLength(500)
      ])],
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit() { }

  /**
   * write to us
   * @param formData 
   * @response message
   */
  writeToUs(formData: FormData) {
    this.commonService.loader.next(true);
    if (formData) {
      this.commonService.trimArray(formData).then(data => {
        formData = data;
        this.httpService.writeToUs(formData).subscribe(data => {
          if (data.Status) {
            this.writeToUsForm.reset();
            this.commonService.loader.next(false);
            this.commonService.showSuccess(API_MESSAGES.WriteToUs.successMessage, "");
          }
          else {
            this.commonService.loader.next(false);
            this.commonService.showError(data.Message, "");
          }
        });
      })
    }
  }

  /**
   * route to different routes
   * @param route 
   */
  routeTo(route: string) {
    this.router.navigateByUrl(route); // using navigate by url to navigate to the section of page on clicking link by respective id
  }
}
