import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/validation';
import { ConfirmpasswordDirective } from 'src/app/shared/directive/confirmpassword.directive';
import { Country } from 'src/app/shared/model/country';
import { State } from 'src/app/shared/model/state';
import { City } from 'src/app/shared/model/city';
import { Industry } from 'src/app/shared/model/industry';
import { Plan } from 'src/app/shared/model/plan';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HeaderpreComponent } from "../../../views/elements/headerpre/headerpre.component";
import { PlansComponent } from "../../../views/elements/plans/plans.component";
import { PaymentComponent } from "../../../views/elements/payment/payment.component";
import { ChangePlanComponent } from "../../../shared/modals/change-plan/change-plan.component";
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManageLocationService } from '../../manage-location/manage-location.service';
import { ManageLocation } from 'src/app/shared/model/manage-location';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  userRole: string = ""; // user role default to admin, can be user too
  signupForm: FormGroup; // form array
  validationMessages: any; // validation messages array
  planData: Plan; // plan array
  countries: Country; // countries array
  states: State; // states array
  cities: City; // cities array
  industries: Industry; // industries array
  submitForm: boolean = false; // toggle to submit form as the page is single but with multiple condition
  changePlanPopup: boolean = false; // toggle for change plan popup
  showPayment: boolean = false; // toggle for payment options
  personalInfo: any = []; // personal information array
  stripe: any; // stripe payment
  email: string = ""; // email to be displayed and be used in case of userRole is "user"
  company: string = ""; // company to be displayed and be used in case of userRole is "User"
  emailCompanyCheck: boolean = false; // to check if the company or email already exists
  roleId: number = 2; // roleId default to 2 that is of Admin
  submitButton: string = "Continue"; // if the plan is free then change the text
  companyId: number = 0;
  locationListData: ManageLocation[];  

  constructor(
    private router: Router,
    private httpService: HttpService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private modalService: NgbModal,
    private manageLocationService: ManageLocationService
  ) {
    this.initForm();
    this.validationMessages = VALIDATION_MESSAGES;
    this.checkUserRole();
  }

  @HostListener('document:click', ['$event'])
  captureSignUp(event: any) {
    if (event.target.innerHTML == 'Signup') {
      this.signupForm.reset()
      this.initForm();
    }
  }

  initForm(){
    this.signupForm = this.formBuilder.group({
      Email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^ *[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5} *$'),
        Validators.maxLength(100)
      ])],
      CompanyName: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]+$)'),
        Validators.maxLength(50)
      ])],
      handleName: ['', Validators.required],
      SelectedCategoryId: [''],
      FirstName: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z _]+$)'),
        Validators.maxLength(50)
      ])],
      LastName: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z _]+$)'),
        Validators.maxLength(50)
      ])],
      PhoneNo: ['', Validators.compose([
        Validators.maxLength(15),
        Validators.required,
        Validators.pattern('^[0-9]{9,15}$'),
        Validators.maxLength(15)
      ])],
      Password: ['', Validators.compose([
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.required,
        Validators.pattern('^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*+=]).*$')
      ])],
      ConfirmPassword: ['', Validators.compose([
        Validators.required,
      ])],
      SelectedCountryId: ['', Validators.compose([
        Validators.required
      ])],
      SelectedStateId: ['', Validators.compose([
        Validators.required
      ])],
      SelectedCityId: ['', Validators.compose([
        Validators.required
      ])],
      LocationId: ['', Validators.compose([
        Validators.required
      ])],
      ZipCode: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^(?:[A-Za-z0-9]+)(?:[A-Za-z0-9 _]+$)'),
        Validators.maxLength(30)
      ])],
      IsPolicyAccepted: ['', Validators.compose([
        Validators.requiredTrue,
      ])],
      AddressOne: ['', Validators.compose([
        Validators.maxLength(150),
      ])],
      AddressTwo: ['', Validators.compose([
        Validators.maxLength(150),
      ])],
    }, { validators: ConfirmpasswordDirective("Password", "ConfirmPassword") });

  }

  ngOnInit() {

    this.httpService.getIndustries().subscribe(data => { // get industries
      if (data.Data) {
        this.industries = data.Data;
      }
    });
  }


  /**
   * get states
   * @param countryId 
   * @response list
   */
  getStates(countryId: number) {
    this.httpService.getStates(countryId).subscribe(data => {
      if (data.Data) {
        this.states = data.Data;
      }
    });
  }

  /**
   * get cities
   * @param stateId 
   * @response list
   */
  getCities(stateId: number) {
    this.httpService.getCities(stateId).subscribe(data => {
      if (data.Data) {
        this.cities = data.Data;
      }
    });
  }

  navigateToHome(){
      let data = this.getConfirmation("Are you sure, your Sign Up information will be lost");
      data.then(res => {
        this.router.navigate(['/home'])
      },
        err => { }
      )
  }

  getConfirmation(title: string) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, { centered: true });
    let data: any = {"title": title}
    modalRef.componentInstance.data = data;
    return modalRef.result;
  }
  
  /**
   * get locations
   */

  getLocations() {
    let formData = {};
    formData["CountryId"] = this.signupForm.value.SelectedCountryId;
    formData["StateId"] = this.signupForm.value.SelectedStateId;
    formData["CityId"] = this.signupForm.value.SelectedCityId;
    formData["CompanyID"] = this.companyId != 0 ? this.companyId : '';
    formData["LocationName"] = "";    
    this.manageLocationService.getSignUpLocation(formData).subscribe(result => {
      if(result.Status) {
        this.locationListData = result.Data;
      } else {
        this.commonService.showError(result.Message, "");        
      }
      this.commonService.loader.next(false);
    })
  }

  /**
   * signup as user and as admin
   * @param formData 
   * @response null
   */
  signup(formData: FormData) {
    console.log("SignupData", formData);
    this.commonService.loader.next(true);
    this.commonService.trimArray(formData).then(data => {
      formData = data;
      if (formData && this.userRole == "Admin") {
        this.personalInfo.Email = formData["Email"]; // personal info to be send for payment
        this.personalInfo.FirstName = formData["FirstName"]; // personal info to be send for payment
        this.personalInfo.LastName = formData["LastName"]; // personal info to be send for payment
        formData["RoleId"] = this.roleId;
        formData["SubscriptionPlanPriceId"] = this.planData.SubscriptionPlanPriceId;
        formData["SelectedPlanType"] = this.planData.PlanType;
        this.httpService.signup(formData).subscribe(data => {
          if (data.Status) {
            this.showPayment = true;
            this.commonService.loader.next(false);
          }
          else {
            this.commonService.showError(data.Message, "");
            this.commonService.loader.next(false);
          }
        });
      }
      else { // sign up as user as there is no plan select and payment
        formData["RoleId"] = this.roleId;
        formData["CompanyId"] = this.companyId;
        localStorage.setItem("bypass", "true");
        this.httpService.signupAsUser(formData).subscribe(data => {
          if (data.Status) {
            this.router.navigate(["/login"]);
            this.commonService.loader.next(false);
            this.commonService.showSuccess(API_MESSAGES.Signup.successMessage, "");
          }
          else {
            this.commonService.loader.next(false);
            this.commonService.showError(data.Message, "");
          }
        });
      }
    });
  }

  /**
   * get plan data
   * @param event 
   * @response list
   */
  getPlanData(event) {
    this.changePlanPopup = false;
    this.planData = event;
    this.submitButton = "Continue";
    if (this.planData.Price <= 0) {
      this.submitButton = "Sign up";
    }
  }

  /**
   * show select plan selector
   * @param null
   * @response null
   */
  showSelectedPlan() {
    this.submitForm = true;
  }

  /**
   * toggle change plan popup
   * @param null
   * @response null
   */
  toggleChangePlan() {
    const modalRef = this.modalService.open(ChangePlanComponent, {
      windowClass: 'modalwidth custom-modal-cls',
      centered: true
    });
    modalRef.componentInstance.planData = this.planData;
    modalRef.result.then((result) => {
      this.changePlanPopup = false;
      this.planData = result;
      this.submitButton = "Continue";
      if (this.planData.Price <= 0) {
        this.submitButton = "Sign up";
      }
    }, (reason) => {
      this.changePlanPopup = false;
      this.planData = reason;
      this.submitButton = "Continue";
      if (this.planData.Price <= 0) {
        this.submitButton = "Sign up";
      }
    });
  }

  ngAfterViewChecked(){
    if(this.userRole == 'Admin'){
      this.signupForm.get('LocationId').clearValidators();
        this.signupForm.get('LocationId').updateValueAndValidity();
    }
  }

  /**
   * check for email or company already exist
   * @param null
   * @response message
   */
  checkEmailAndCompany() {
    if (this.email || this.company) {
      this.httpService.checkEmailAndCompany(this.email, this.company).subscribe(data => {
        if (data["Data"].length > 0) {
          this.emailCompanyCheck = false;
          data["Data"].map(res => {
            this.commonService.showError(res.ColumnValue, "");
          })
        }
        else {
          this.emailCompanyCheck = true;
        }
      });
    }
  }

  /**
   * check the type of user for signup
   * @param null
   * @response null
   */
  checkUserRole() {
    this.route.queryParams.subscribe(params => {
      if (params.EmailId) {
        this.httpService.getUserInfo(params.EmailId).subscribe(data => {
          if (data.Status) {
            this.email = data.Data.EmailId;
            this.company = data.Data.CompanyName;
            this.roleId = data.Data.RoleId;
            this.companyId = data.Data.CompanyId;
            this.getInitCountries();
            if (this.roleId == 4) {
              this.userRole = "User";
              this.getLocations();
            }
            else {
              this.userRole = "Trainer";
              this.getLocations();
            }
          }
          else {
            this.getInitCountries();
            this.commonService.showError(data.Message, "");
            this.userRole = "";
          }
        });
      }
      else {
        this.getInitCountries();
        this.userRole = "Admin";
        this.signupForm.get('LocationId').clearValidators();
        this.signupForm.get('LocationId').updateValueAndValidity();
        this.signupForm.controls["SelectedCategoryId"].setValidators([Validators.required]); // set the validation for industries in case of userRole is Admin
        this.signupForm.controls['SelectedCategoryId'].updateValueAndValidity();
      }
    });
  }

  getInitCountries(){
    this.httpService.getCountries()
      .subscribe(data => { // get countries
        if (data.Data) {
          this.countries = data.Data;
        }
      });
  }

  keyPressAlphanumeric(event) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
