import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/login_validation';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { AuthService, SocialUser } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { Router } from '@angular/router';
import { CookieService } from "ngx-cookie-service";
import { User } from 'src/app/shared/model/user';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
	selector: 'app-signin',
	templateUrl: './signin.component.html',
	styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
	loginForm: FormGroup; // form array 
	validationMessages: any; // validation messages array
	private user: SocialUser; // google and facebook login
	private loggedIn: boolean; // check if the user has logged in
	showPass: boolean = false;
	inputType: string = 'password';
	constructor(private formBuilder: FormBuilder, private httpService: HttpService, private authService: AuthService, private commonService: CommonService, private router: Router, private cookieService: CookieService) {
		this.loginForm = this.formBuilder.group({
			Email: ['', Validators.compose([
				Validators.required,
				// Validators.pattern('^ *[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5} *$')
			])],
			Password: ['', Validators.compose([
				Validators.required,
			])],
			RememberMe: [false]
		});
		this.validationMessages = VALIDATION_MESSAGES;
	}

	ngOnInit() {
		this.authService.authState.subscribe((user) => {
			this.user = user;
			this.loggedIn = (user != null);
		});
	}

	/**
	 * sign in with google
	 * @param null
	 * @response email
	 */
	signInWithGoogle(): void {
		this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(data => {
			if (data && data.authToken) {
				this.httpService.socialLogin(data.provider, data.authToken).subscribe(res => {
					if (res.Status) {
						this.setLoginResponse(res);
						this.commonService.showSuccess(API_MESSAGES.Login.successMessage, "");
						this.router.navigate(["/"]);
					}
					else {
						this.commonService.showError(res.Message, "");
					}
				});
			}
			else {
				this.commonService.showError(API_MESSAGES.CommonError.message, "");
			}
		});
	}

	/**
	 * sign in with facebook
	 * @param null
	 * @response email
	 */
	signInWithFB(): void {
		this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(data => {
			if (data && data.idToken) {
				this.httpService.socialLogin(data.provider, data.idToken).subscribe(res => {
					if (res.Status) {
						this.commonService.showSuccess(API_MESSAGES.Login.successMessage, "");
						this.router.navigate(["/"]);
					}
					else {
						this.commonService.showError(res.Message, "");
					}
				});
			}
			else {
				this.commonService.showError(API_MESSAGES.CommonError.message, "");
			}
		});
	}

	signOut(): void {
		this.authService.signOut();
	}

	/**
	   * function to get the api check for user credentials 
	   * @access public
	   * @param formData
	   * @response object
	   */
	public login(formData: FormData) {
		this.commonService.loader.next(true);
		if (formData["Email"] != "" && formData["Password"] != "") {
			this.commonService.trimArray(formData).then(data => {
				if(data.Email[0] == '@'){
					data.Email = data.Email.slice(1);
				}
				formData = data;
				this.httpService.login(formData, this.cookieService.get("fcmToken")).subscribe(data => {
					if (data.Status) {
						let userData: User = data.Data;
						this.setLoginResponse(data);
						if (formData["RememberMe"]) {
							this.cookieService.set("token", userData.JWTToken, 3600);
						}
						this.commonService.showSuccess(API_MESSAGES.Login.successMessage, "");
						this.commonService.loader.next(false);
						if (userData.UserRole == 'User') {
							this.router.navigate(["/my-profile/profile/" + userData.UserId])
						} else {
							this.router.navigate(["/"]);
						}
					}
					else {
						this.commonService.showError(data.Message, "");
						this.commonService.loader.next(false);
					}
				});
			});
		} else {
			this.commonService.showError(API_MESSAGES.LoginValidationError.message, "");
			this.commonService.loader.next(false);
		}
	}

	/**
	 * to route to signup
	 * @param null
	 * @response null
	 */
	signUp() {
		this.router.navigate(["signup"]);
	}

	setLoginResponse(response: any) {
		let userData: User = response.Data;
		localStorage.setItem("userName", userData.UserName); // set the username in local storage
		localStorage.setItem("userRole", userData.UserRole); // set the user role in local storage
		localStorage.setItem("roleId", userData.RoleId.toString()); // set the user role in local storage
		localStorage.setItem("userId", userData.UserId.toString()); // set the user id in local storage
		localStorage.setItem("userIndustry", userData.Industry.toString()); // set the user industry in local storage
		localStorage.setItem("profileImage", userData.ProfilePicture); // set the user industry in local storage
		localStorage.setItem("logoImage", userData.CompanyLogo); // set the user industry in local storage
		this.cookieService.set("token", userData.JWTToken); // set the token in cookie
	}

	showHidePass(){
		this.showPass = !this.showPass;
		this.inputType = this.inputType == 'password' ? 'text' : 'password';
	}

}
