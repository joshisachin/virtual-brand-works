import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ContactusComponent } from 'src/app/contactus/contactus.component';

@Component({
  selector: 'app-home-new',
  templateUrl: './home-new.component.html',
  styleUrls: ['./home-new.component.scss']
})
export class HomeNewComponent implements OnInit {
  playClass:string = "";
  buildClass:string = "";
  makeClass:string = "";
  hideBuild:string = "";
  hideMake:string = "";
  hidePlay:string = "";
  constructor(private modalService: NgbModal ) { 

  }

  ngOnInit(): void {
    
  }
  setplayClass(){
    this.playClass = 'active';
    this.hidePlay = 'd-none'
  }
  setmakeClass(){
    this.makeClass = 'active';
    this.hideMake = 'd-none'
  }
  setbuildClass(){
    this.buildClass = 'active';
    this.hideBuild = 'd-none'
  }

  focuslost(){
    this.buildClass = "";
    this.hideBuild = "";
  }

  focuslost1(){
    this.makeClass = "";
    this.hideMake = "";
  }

  focuslost2(){
    this.playClass = "";
    this.hidePlay = "";
  }

  contactus() { 
    const modalRef = this.modalService.open(ContactusComponent, { windowClass: 'w925', centered: true });   
  }
}
