import { NgModule } from '@angular/core';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SignupComponent } from './signup/signup.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SigninComponent } from './signin/signin.component';
import { SessionsComponent } from './sessions.component';
import { SessionsRoutingModule } from './sessions-routing.module';
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { CanDeactivateGuard } from 'src/app/shared/services/authguard/can-deactivate-guard.service';
import { HomepageComponent } from './home/homepage.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TncComponent } from './tnc/tnc.component';
import { ContentViewComponent } from './content-view/content-view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeNewComponent } from './home-new/home-new.component';
import { ManageLocationService } from '../manage-location/manage-location.service';
import { UserTabSevice } from '../users/user/user.service';
let config = new AuthServiceConfig([
	{
	  id: GoogleLoginProvider.PROVIDER_ID,
	  provider: new GoogleLoginProvider("506952347456-i8bkclhus8f7mr42m7auainfrlkvnp57.apps.googleusercontent.com")
	},
	{
	  id: FacebookLoginProvider.PROVIDER_ID,
	  provider: new FacebookLoginProvider("1605498219614210")
	}
  ]);

  export function provideConfig() {
	return config;
  }
@NgModule({
  declarations: [
    SignupComponent,
    SigninComponent,
    SessionsComponent,
    ResetpasswordComponent,
    ForgotpasswordComponent,
    HomepageComponent,
    PrivacyPolicyComponent,
    TncComponent,
    ContentViewComponent,
    HomeNewComponent
  ],
  imports: [
    SharedModule,
    NgbModule,
    CommonModule,
    SessionsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    CanDeactivateGuard,
    ManageLocationService,
    UserTabSevice
  ]
		
})
export class SessionsModule { }
