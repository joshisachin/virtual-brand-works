import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/validation';
import { ConfirmpasswordDirective } from 'src/app/shared/directive/confirmpassword.directive';
import { User } from 'src/app/shared/model/user';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  resetPasswordForm: any = []; // form array
  validationMessages: any; // validation messages
  email: string; // email fetched from forgot password
  constructor(private FormBuilder: FormBuilder, private httpService: HttpService, private commonService: CommonService, private router: Router, private route: ActivatedRoute) {

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.email = this.router.getCurrentNavigation().extras.state.email;
      }
    });

    this.resetPasswordForm = this.FormBuilder.group({
      Email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^ *[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5} *$')
      ])],
      ResetCode: ['', Validators.compose([
        Validators.required,
      ])],
      NewPassword: ['', Validators.compose([
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.required,
        Validators.pattern('^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*+=]).*$'),
      ])],
      ConfirmPassword: ['', Validators.compose([
        Validators.required,
      ])],
    }, { validators: ConfirmpasswordDirective("NewPassword", "ConfirmPassword") });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit() { }

  /**
   * reset password
   * @param formData 
   * @response null
   */
  resetPassword(formData: any) {
    let sendData: User;
    if (formData) {
    sendData = formData;
    formData = this.commonService.trimArray(formData).then(data => {
      formData = data;
      this.httpService.resetPassword(sendData).subscribe(data => {
        if (data.Status) {
          this.router.navigate(["/login"]);
          this.commonService.showSuccess(API_MESSAGES.ResetPassword.successMessage, "");
        }
        else {
          this.commonService.showError(data.Message, "");
        }
      });
    })
    }
  }

  /**
   * resend the otp code
   * @param event
   * @response message
   */
  resendCode(event) {
    event.preventDefault();
    this.commonService.loader.next(true);
    let sendData: any = { "Email": this.email };
    this.httpService.forgotPassword(sendData).subscribe(data => {
      if (data.Status) {
        this.commonService.loader.next(false);
        this.commonService.showSuccess(API_MESSAGES.ResetPasswordCode.successMessage, "");
      }
      else {
        this.commonService.loader.next(false);
        this.commonService.showError(data.Message, "");
      }
    });
  }

}
