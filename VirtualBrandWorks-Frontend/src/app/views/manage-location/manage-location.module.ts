import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageLocationComponent } from './manage-location.component';
import { LocationListComponent } from './location-list/location-list.component';
import { ManageLocationRoutingModule } from './manage-location.routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ManageLocationService } from './manage-location.service';
import { AddLocationComponent } from './add-location/add-location.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ManageLocationComponent, 
    LocationListComponent, 
    AddLocationComponent
  ],
  imports: [
    CommonModule,
    ManageLocationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ManageLocationService
  ]
})
export class ManageLocationModule { }
