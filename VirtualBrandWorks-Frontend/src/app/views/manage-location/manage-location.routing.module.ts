import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddLocationComponent } from './add-location/add-location.component';
import { LocationListComponent } from './location-list/location-list.component';
import { ManageLocationComponent } from './manage-location.component';


const routes: Routes = [
	{
		path: '',
		component: ManageLocationComponent,
		children: [
			{
				path: '',
				component: LocationListComponent
			},
			{
				path: 'add-location/:locationId',
				component: AddLocationComponent
			}
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ManageLocationRoutingModule { }
