import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/shared/model/country';
import { State } from 'src/app/shared/model/state';
import { City } from 'src/app/shared/model/city';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ADD_LOCATION } from '../../../shared/constants/validations/validation';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { ManageLocationService } from '../manage-location.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.scss']
})
export class AddLocationComponent implements OnInit {
  addLocationForm: FormGroup; // form array
  validationMessages: any; // validation messages array
  countries: Country; // countries array
  states: State; // states array
  cities: City; // cities array
  userRole: string;
  isFormValid: boolean = true;
  locationId: number;
  constructor(
    private formBuilder: FormBuilder,
    private httpService: HttpService,
    private commonService: CommonService,
    private manageLocationService: ManageLocationService,
    private router: Router, 
    private route: ActivatedRoute
  ) { 
    this.addLocationForm = this.formBuilder.group({
      CountryId: ['', Validators.compose([
        Validators.required
      ])],
      StateId: ['', Validators.compose([
        Validators.required
      ])],
      CityId: ['', Validators.compose([
        Validators.required
      ])],
      LocationName: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^(?:[A-Za-z0-9]+)(?:[A-Za-z0-9 _]+$)'),
        Validators.maxLength(50)
      ])]
    });
    this.validationMessages = ADD_LOCATION;    
  }

  ngOnInit(): void {
    this.userRole = localStorage.getItem("userRole");
    this.route.params.subscribe(params => {
      this.locationId = params.locationId;
      if(this.locationId != 0) {
        this.route.queryParams.subscribe(params => {
          this.setFormData(params);
        });
        
      }
    })
    this.httpService.getCountries().subscribe(data => { // get countries
      if (data.Data) {
        this.countries = data.Data;
      }
    });
  }

  /**
   * get states
   * @param countryId 
   * @response list
   */
   getStates(countryId: number) {
    this.httpService.getStates(countryId).subscribe(data => {
      if (data.Data) {
        this.states = data.Data;
      }
    });
  }

  /**
   * get cities
   * @param stateId 
   * @response list
   */
  getCities(stateId: number) {
    this.httpService.getCities(stateId).subscribe(data => {
      if (data.Data) {
        this.cities = data.Data;
      }
    });
  }

  /**
   * Set form data when edit location
   */

  setFormData(queryParams) {
    this.getStates(queryParams.countryId);
    this.getCities(queryParams.stateId);
    this.addLocationForm.patchValue({
      CountryId: queryParams.countryId,
      StateId: queryParams.stateId,
      CityId: queryParams.cityId,
      LocationName: queryParams.locationName
    })
  }

  /**
   * Add or Edit a location
   * @param formData 
   * @response null
   */

  submitAddEditLocation(addEditForm) {
    this.isFormValid = addEditForm.valid;
    let formData = {};
    if(this.isFormValid) {
      this.commonService.loader.next(true);
      this.commonService.trimArray(this.addLocationForm.value).then(data => {
        formData = data;
        formData["LocationId"] = this.locationId;
        formData["IsAdmin"] = this.userRole == 'Admin' ? true : false;
        this.manageLocationService.addEditLocation(formData).subscribe(result => {          
          if(result.Status) {
            this.commonService.loader.next(false);
            this.router.navigate(['/manage-location']);
          } else {
            this.commonService.showError(result.Message, "");
            this.commonService.loader.next(false);
          }
        })
      })
    }
    
  }

}
