import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { ManageLocation } from 'src/app/shared/model/manage-location';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { ManageLocationService } from '../manage-location.service';

@Component({
  selector: 'app-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.scss']
})
export class LocationListComponent implements OnInit {

  locationListData: ManageLocation[];

  constructor(
    private httpService: HttpService,
    private commonService: CommonService,
    private manageLocationService: ManageLocationService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.commonService.loader.next(true);
    let formData = {};
    formData["CountryId"] = "";
    formData["StateId"] = "";
    formData["CityId"] = "";
    formData["LocationName"] = ""; 
    this.manageLocationService.getLocation(formData).subscribe(result => {
      if(result.Status) {
        this.locationListData = result.Data;
      } else {
        this.commonService.showError(result.Message, "");        
      }
      this.commonService.loader.next(false);
    })
  }


  /**
   * Edit location
   * @param location
   */

  editLocation(location) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        countryId: location.CountryId,
        stateId: location.StateId,
        cityId: location.CityId,
        locationName:location.LocationName
      }
    };
    this.router.navigate(["/manage-location/add-location/"+location.LocationId], navigationExtras);
  }

  /**
   * Delete location
   * @params locationId
   */

   deleteLocation(locationId) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {
      windowClass: 'modalwidth'
    });

    modalRef.componentInstance.data = {
      "title": "Are you sure you want to delete this location?"
    };

    modalRef.result.then(() => {
      this.manageLocationService.deleteLocation(locationId).subscribe(result => {
        if(result.Status) {
          // this.locationListData = result.Data;
          let index = this.locationListData.indexOf(this.locationListData.find(elem => elem.LocationId == locationId));
          this.locationListData.splice(index, 1);
        } else {
          this.commonService.showError(result.Message, "");        
        }
        this.commonService.loader.next(false);
      })
    });
    
   }

}
