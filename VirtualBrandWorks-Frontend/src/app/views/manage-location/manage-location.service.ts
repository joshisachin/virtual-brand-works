import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { API_NAME } from '../../../app/shared/constants/apiname/apiname';

@Injectable()
export class ManageLocationService {
  	constructor(private http: HttpClient) { }

	/**
	 * get location list
	 */
	getLocation(payload): Observable<any> {
		return this.http.post(API_NAME.ManageLocation.getLocationList, payload);
	}

	getSignUpLocation(payload): Observable<any> {
		return this.http.post(API_NAME.ManageLocation.GetSignUpLocations, payload);
	}

	/**
	 * Add/Edit location
	 */

	addEditLocation(payload: any): Observable<any> {
		return this.http.post(API_NAME.ManageLocation.addEditLocation , payload);
	}
	/**
	 * Delete location
	 */

	deleteLocation(locationId: number): Observable<any> {
		return this.http.get(API_NAME.ManageLocation.deleteLocation + "?locationId=" + locationId);
	}
}
