import { Component, OnInit } from '@angular/core';
import { FolderDetailsCommonComponent } from "../../elements/folder-details-common/folder-details-common.component";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  folderType: string = "master";
  constructor() { }

  ngOnInit(): void {  }
}
