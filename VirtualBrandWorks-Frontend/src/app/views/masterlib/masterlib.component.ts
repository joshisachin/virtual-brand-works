import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';


@Component({
  selector: 'app-masterlib',
  templateUrl: './masterlib.component.html',
  styleUrls: ['./masterlib.component.scss']
})
export class MasterlibComponent implements OnInit {
  
  toggleRightSide: boolean; // to expand right side when side menu shrinks
  constructor(private commonServices: CommonService) {
    this.commonServices.toggleRightSide.subscribe(data => {
      this.toggleRightSide = data;
    });
  }

  ngOnInit() {
  }
}
// Compute: 34.215.148.226 new Server ==> 34.215.73.124 ==> 44.232.180.17
// UserName : Administrator
// Password : D(BUn4HLIqnIG9yWBgK.DS!sV5F2.RsD