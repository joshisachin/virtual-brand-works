import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MasterlibComponent } from './masterlib.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './home/home.component';
import { MasterlibRoutingModule } from './masterlib-routing.module';
import { DetailsComponent } from './details/details.component';
import { NgDynamicBreadcrumbModule } from 'ng-dynamic-breadcrumb';
import { ContentDetailsComponent } from './content-details/content-details.component';


@NgModule({
	declarations: [
		MasterlibComponent, 
		HomeComponent, 
		DetailsComponent, ContentDetailsComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		MasterlibRoutingModule,
		NgDynamicBreadcrumbModule,
		RouterModule
	]
})
export class MasterlibModule { }
