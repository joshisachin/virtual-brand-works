import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FolderComponent } from "../../elements/folder/folder.component";
import { FolderFooterComponent } from "../../elements/folder-footer/folder-footer.component";
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { FolderDetailsComponent } from "../../../shared/modals/folder-details/folder-details.component";
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  countSelect: number = 0; // count of selected folders
  showFooterFolderVar: boolean = false; // toggle for selected folder footer
  folderType="master"; // send to folder component
  @ViewChild("searchFolder", {static: true}) searchFolder: ElementRef;

  constructor(private commonService: CommonService, private httpService: HttpService) { }

  ngOnInit(): void {
    this.commonService.searchDebounce(this.searchFolder.nativeElement).subscribe(searchString => {
        this.getSearchData(searchString);
    });
  }

  ngAfterViewInit() {
    this.initFolderComponent();
  }

  /**
   * initialize the folder variable
   */
  initFolderComponent() {
    this.commonService.selectedFolders.subscribe(data => {
      if (data) {
        this.countSelect = data.length; // get the count
      }
    });
  }

  /**
   * get search data
   * @param param
   * @response object
   */
  getSearchData(param) {
    this.commonService.searchDataForFolder.next(param);
    this.httpService.getAllFoldersData(param, "", 1, environment.PAGINATE_FETCH).subscribe(data => {
      if (data.Status && data.Data.length > 0) {
        let folders = data.Data.filter((res) => { // filter to separate dynamic folder
          return res.IsDynamicFolder == false;
        });
        FolderComponent.stopScroll.next(false);
        this.commonService.folders.next(folders); // updating the value
      }
    })
  }

  /**
   * toggle folder footer
   * @param null
   * @eesponse null
   */
  showFooterFolder() {
    this.showFooterFolderVar = !this.showFooterFolderVar;
  }
}
