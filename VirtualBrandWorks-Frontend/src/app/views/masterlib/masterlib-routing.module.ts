import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterlibComponent } from './masterlib.component';
import { HomeComponent } from './home/home.component';
import { DetailsComponent } from './details/details.component';
import { ContentDetailsComponent } from './content-details/content-details.component';


const routes: Routes = [
	{
		path: '',
		component: MasterlibComponent,
		children: [
			{
				path: '',
				component: HomeComponent,
				data: {
					breadcrumb: [
						{
							label: 'Master Library',
							url: ''
						}
					]
				},
			},
			{
				path: ':name/:id',
				component: DetailsComponent,
				data: {
					breadcrumb: [
						{
							label: 'Master Library',
							url: '/masterlibrary',
						},
						{
							label: '{{ name }}',
							url: '',
						}
					]
				},
			},
			{
				path: ':name/:id/:contentName/:idNew',
				component: ContentDetailsComponent,
				data: {
					breadcrumb: [
						{
							label: 'Master Library',
							url: '/masterlibrary'
						},
						{
							label: '{{ name }}',
							url: '/masterlibrary/:name/:id',
						},
						{
							label: '{{ contentName }}',
							url: '',
						}
					]
				},
			}
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MasterlibRoutingModule { }
