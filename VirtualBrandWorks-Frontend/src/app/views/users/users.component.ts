import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
@Component({
	selector: 'app-user',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

	toggleRightSide: boolean; // to expand right side when side menu shrinks

	constructor(private router: Router,
		private commonService: CommonService
	) { 
		this.commonService.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
	}

	ngOnInit() {
	}
}
