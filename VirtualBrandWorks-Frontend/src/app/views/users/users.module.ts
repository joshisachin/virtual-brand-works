import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { UserComponent } from './user/user.component';
import { UserTabSevice } from './user/user.service';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { AddUserComponent } from './add-user/add-user.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CommonPopupComponent } from 'src/app/shared/modals/common-popup/common-popup.component';
import { UserProfileSevice } from './user-profile/user-profile.service';
import { AssociatedPlaylistComponent } from './associated-playlist/associated-playlist.component';
import { AssociatedUsersComponent } from './associated-users/associated-users.component';
import { PrivacySettingsComponent } from './privacy-settings/privacy-settings.component'; 
import { AssociatedUsersService } from './associated-users/associated-users.service';
import { PlaylistService } from '../my-playlist/playlist.service';
import { AboutCompanyComponent } from './about-company/about-company.component';
import { MyPlanComponent } from './my-plan/my-plan.component';

@NgModule({
    declarations: [
        UsersComponent,
        UserComponent,
        AddUserComponent,
        CommonPopupComponent,
        ProfileComponent,
        UserProfileComponent,
        EditProfileComponent,
        AssociatedPlaylistComponent,
        AssociatedUsersComponent,
        PrivacySettingsComponent,
        AboutCompanyComponent,
        MyPlanComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule,
        UsersRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule,
        InfiniteScrollModule
    ],
    providers: [
        UserTabSevice,
        UserProfileSevice,
        AssociatedUsersService,
        PlaylistService
    ]
})
export class UsersModule { }
