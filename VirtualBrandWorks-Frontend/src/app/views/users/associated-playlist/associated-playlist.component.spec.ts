import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedPlaylistComponent } from './associated-playlist.component';

describe('AssociatedPlaylistComponent', () => {
  let component: AssociatedPlaylistComponent;
  let fixture: ComponentFixture<AssociatedPlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatedPlaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedPlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
