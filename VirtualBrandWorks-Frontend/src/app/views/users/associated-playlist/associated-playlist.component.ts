import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-associated-playlist',
  templateUrl: './associated-playlist.component.html',
  styleUrls: ['./associated-playlist.component.scss']
})
export class AssociatedPlaylistComponent implements OnInit {

  playlistType: string = "associatedPlaylist";
  constructor() {}

  ngOnInit(): void {}

}