import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import { UserTabSevice } from '../user/user.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { AlertComponent } from 'src/app/shared/modals/alert/alert.component';
import { AddUserComponent } from '../add-user/add-user.component';
import { timer, BehaviorSubject } from 'rxjs';
import { CommonPopupComponent } from 'src/app/shared/modals/common-popup/common-popup.component';
import { CompanyUserList } from 'src/app/shared/model/company-user-list';

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

	tabType: any;
	getUserData: CompanyUserList[];
	selectedIndex: any;
	userRole: any = {};
	sortByKey: any = {};
	sortBy: any;
	searchUser: any;
	page: any;
	sortDefaultValue: any;
	loggedInUserRole: any;
	currentUserRoleId: string = localStorage.getItem("roleId");
	locations: any = [];

	static stopScroll: BehaviorSubject<boolean> = new BehaviorSubject(false); // to stop the api on scroll when all the results got fetched

	@ViewChild("searchLoc", {static: true}) searchLoc: ElementRef;
	
	constructor(
		private router: Router,
		private userTabService: UserTabSevice,
		private modalService: NgbModal,
		private commonService: CommonService,
		private activatedRoute: ActivatedRoute
	) {
	}

	ngOnInit() {
		let formData = {};
		formData["CountryId"] = "";
		formData["StateId"] = "";
		formData["CityId"] = "";
		formData["LocationName"] = ""; 
		this.userTabService.getLocation(formData).subscribe(result => {
			this.locations = result.Data;
		})
		this.loggedInUserRole = localStorage.getItem("userRole");
		this.tabType = this.loggedInUserRole =='Admin' ? 'trainer' : 'user';
		this.clearData();
		this.getUserList();	

		this.userRole = [
			{'title': 'Trainer', 'roleId': '3'}, 
			{'title': 'User', 'roleId': '4'}
		]

		this.sortByKey = [
			{'name': 'Sort By', 'value': ''}, 
			{'name': 'By Name', 'value': 'FirstName'}, 
			{'name': 'By Location', 'value': 'Location'}
		]

		this.commonService.searchDebounce(this.searchLoc.nativeElement).subscribe(searchString => {
			if (searchString === "") {
				this.searchUser = '';
				this.page = 1;
				this.getUserData = [];
				this.getUserList();
			} else {
				this.searchUser = searchString;
				this.page = 1;
				this.getUserData = [];
				this.getUserList();
			}
		});			
	}

	selectedTab(tabNum) {		
		this.tabType = tabNum;
		UserComponent.stopScroll.next(false);
		this.clearData();		
		this.getUserList();
	}

	/**
   * Changes user role as per selected value
   * @param  roleId
   * @param userData
   * @param statesEl
   * @response success
   */

	changeUserRole(roleId, userData, statesEl) {
		const modalRef = this.modalService.open(ConfirmationAlertComponent, { 
			windowClass: 'modalwidth',
			centered: true
		});

		modalRef.componentInstance.data = { 
			"title": 'Are you sure you want to change the role of this user?' 
		};

    	modalRef.result.then((result) => {
			this.clearData();
			let data = {};
			data["RoleId"] = roleId;
			data["UserId"] = userData.UserId;
			this.commonService.loader.next(true);
			this.userTabService.updateUserRole(JSON.stringify(data)).subscribe(data => {
				if(data.Status) {
					this.commonService.showSuccess(data.Message, "");					
					this.commonService.loader.next(false);
					this.getUserList();			
				} else {
					this.commonService.showError(data.Message, "");
					this.commonService.loader.next(false);
				}
			})
		}, (reason) => {
			statesEl.selectedIndex = roleId == 4 ? 0 : 1
		});	
	}

	/**
   * Changes user status active or inactive
   * @param  isChecked
   * @param UserData
   * @response null
   */

	changeUserStatus(isChecked, userData) {
		let data = {};
		data["IsActive"] = isChecked;
		data["UserId"] = userData.UserId;
		this.commonService.loader.next(true);
		this.userTabService.updateUserStatus(JSON.stringify(data)).subscribe(data => {
			if(data.Status) {
				this.commonService.showSuccess(data.Message, "");
				this.commonService.loader.next(false);				
			} else {
				this.commonService.showError(data.Message, "");
				this.commonService.loader.next(false);
			}
		})
	}

	/**
   * Get user list as per salected tab ( Trainer, user, all)
   * @param  null
   * @response Object
   */

	getUserList() {		
		let data = {} ;
		let role = this.tabType == 'trainer' ? "3" : this.tabType == 'user' ? "4" : "3,4";
		data["UserRole"] = role;
		data["Search"] = this.searchUser;
		data["PageIndex"] = this.page;
		data["PageSize"] = 7;
		data["SortBy"] = this.sortBy;
		this.commonService.loader.next(true);
		this.userTabService.getUsersList(JSON.stringify(data)).subscribe(result => {
			if(result.Status) {
				this.commonService.loader.next(false);
				result.Data.forEach(listArr => {
					this.getUserData.push(listArr);	
				});								
			} else {
				this.commonService.showError(result.Message, "");
				this.commonService.loader.next(false);
			}

			if (result.Data.length === 1 || result.Data.length === 0)  {
				UserComponent.stopScroll.next(true);
				this.page = 1;
			}
		})
	}

	/**
   * Check user subscription limit, opens modal to add new user, trainer
   * @param  null
   * @response null
   */

	addUser() {
	this.locations.length > 0 ?
		this.userTabService.checkUserLimit().subscribe(data => {
			if(data.Status) {
				const modalRef = this.modalService.open(AddUserComponent, { 
					windowClass: 'modalwidth',
					centered: true 
				});

				modalRef.componentInstance.data = { 
					"roleId": this.tabType == 'trainer' ? "3" : "4"
				};
				modalRef.result.then((result) => {
					setTimeout(() => {
						this.openCommonPopup(result)
					}, 200)
				}, (reason) => { });
			} else {
				const modalRef = this.modalService.open(AlertComponent, { 
					windowClass: 'modalwidth',
					centered:true
				});

				modalRef.componentInstance.data = { 
					"title": 'You have reached your maximum limit of the number of users you can add as per your subscription plan.' 
				};
				modalRef.result.then((result) => {
				}, (reason) => { });
			}
		})				
		:
		this.commonService.showError('Locations should be added first','');
	}

	/**
   * Modal opens after add new user.
   * @param  formData
   * @response true, false
   */

	openCommonPopup(formData?) {
		let name = formData.FirstName+" "+formData.LastName;
		const modalRef = this.modalService.open(CommonPopupComponent, { 
			windowClass: 'modalwidth',
			centered: true
		});
		modalRef.componentInstance.data = { 
			"title": "Congratulations",
			"subTitle": 'An email invite has been sent successfully to '+name+' to sign up.',
			"message": "Would you like to add more?",
			"buttonOne": "+ Add More",
			"buttonTwo": "View User"
		};
		modalRef.result.then((result) => {
			setTimeout(() => {
				this.addUser();
			},200)
		}, (reason) => { });
	}

	sortUserList(sortKey) {
		UserComponent.stopScroll.next(false);
		this.sortBy = sortKey;
		this.page = 1;
		this.getUserData = [];
		this.getUserList();
	}

	searchUsers(searchKey) {
		this.searchUser = searchKey;
		this.page = 1;
		this.getUserData = [];
		this.getUserList();
	}

	onScroll() {
		if (UserComponent.stopScroll.getValue() == false) {
			this.page = this.page + 1;
			this.getUserList();
		}		
	}

	ngOnDestroy() {
		UserComponent.stopScroll.next(false);
	  }

	clearData() {
		this.sortBy = '';
		this.searchUser  = '';
		this.page = 1;
		this.selectedIndex = '';
		this.getUserData = [];
		this.sortDefaultValue = 'default';
	}

	userProfile(userData) {
		let userRole = userData.RoleId == 3 ? 'Trainer' : 'User'
		this.router.navigate(["/users/profile/"+userData.UserId+"/"+userRole]);
		// this.router.navigate(["/users/profile"], { queryParams: { userId: userData.UserId, role:  userData.RoleId == 3 ? 'Trainer' : 'User'}});
	}
}
