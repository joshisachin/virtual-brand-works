import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { API_NAME } from '../../../shared/constants/apiname/apiname';
import { environment } from 'src/environments/environment';

@Injectable()
export class UserTabSevice {
    apiName = API_NAME;
    tabIndex: number = 1;
    constructor(private http: HttpClient) { }

    /**
     * Get user list
     * @param data
     */

    getUsersList(data: any): Observable<any> {
        return this.http.post(this.apiName.GetCompanyUsers[0].name, data);
    }

    /**
   * Update user role
   * @param data
   */

    updateUserRole(data: any): Observable<any> {
        return this.http.post(this.apiName.UpdateUserRole[0].name, data);
    }

    /**
   * Update user status
   * @param data
   */

    updateUserStatus(data: any): Observable<any> {
        return this.http.post(this.apiName.UpdateUserStatus[0].name, data);
    }

    /**
   * add Check user limit
   * @param null
   */

    checkUserLimit(): Observable<any> {
        return this.http.get(this.apiName.CheckUserLimitForCompany[0].name)
    }

    /**
   * add user
   * @param data
   */

    addUser(data: any): Observable<any> {
        return this.http.post(this.apiName.AddUser[0].name, data);
    }

    /**
     * get plan details
     */
    getPlanDetails(): Observable<any> {
        return this.http.get(this.apiName.MyPlan[0].name);
    }

    /**
     * change plan
     */
    changePlan(data: any): Observable<any> {
        return this.http.post(this.apiName.ChangeSubscriptionPlan[0].name + "?planId=" + data.planId + "&stripeSubscriptionPlanId=" + data.stripeSubscriptionPlanId + "&oldPlanName=" + data.oldPlanName, {});
    }

    /**
     * create session for change card
     */
    createSession(): Observable<any> {
        return this.http.post(this.apiName.GetCreateCardCheckoutSession[0].name, {"SuccessUrl": environment.SUCCESS_URL + "&cardChange=true", "CancelUrl": environment.CANCEL_URL + "&cardChange=true"});
    }

    getLocation(payload): Observable<any> {
		return this.http.post(API_NAME.ManageLocation.getLocationList, payload);
	}
}
