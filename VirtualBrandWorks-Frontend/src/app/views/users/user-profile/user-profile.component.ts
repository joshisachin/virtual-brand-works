import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';
import { ImageCropperPopupComponent } from 'src/app/shared/modals/image-cropper/image-cropper-popup.component';
import { MyProfile } from 'src/app/shared/model/my-profile';
import { UserProfileSevice } from './user-profile.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
	selector: 'user-profile',
	templateUrl: './user-profile.component.html',
	styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
	userId: any;
	userProfileData: MyProfile;
	loggedInUserId: any;
	userRole: any;
	show: boolean = false;
	previewUrl: any;

	constructor(
		private route: ActivatedRoute,
		private userProfileService: UserProfileSevice,
		private commonServices: CommonService,
		private modalService: NgbModal,
	) {
		this.route.params.subscribe(params => {
			this.userId = params.userId;
			this.getUserProfile()
		})
	}

	/**
	 * Get user profile info
	 * @param  null
	 * @response Object
	 */
	getUserProfile() {
		let data = {};
		data["UserId"] = this.userId;
		this.commonServices.loader.next(true);
		this.userProfileService.getUserProfile(JSON.stringify(data)).subscribe(data => {
			if (data.Status) {
				this.commonServices.loader.next(false);
				this.userProfileData = data.Data;
				if (localStorage.getItem("userId") == this.userId) {
					this.commonServices.updateHeaderProfileImage(this.userProfileData.ProfilePicture);
				}
			}
		})
	}

	getAddress(data) {
		return (data.AddressOne == null ? '' : data.AddressOne) + ' ' + (data.AddressTwo == null ? '' : data.AddressTwo) + ' ' + (data.City == null ? '' : data.City) + ', ' + (data.State == null ? '' : data.State) + ', ' + (data.Country == null ? '' : data.Country) + ', ' + (data.Zip == null ? '' : data.Zip)
	}

	/**
   * this function will open edit profile modal
   * @param  userData
   * @response success
   */

	editUserProfile(userData) {
		const modalRef = this.modalService.open(EditProfileComponent, {
			windowClass: 'w925',
			centered: true
		});

		modalRef.componentInstance.data = {
			"userData": userData
		};
		modalRef.result.then((result) => {
			this.userProfileData = result;
			localStorage.setItem("userName", this.userProfileData.FirstName);
			this.commonServices.updateHeaderUserName(this.userProfileData.FirstName);
		}, (reason) => { });
	}

	ngOnInit() {
		this.loggedInUserId = localStorage.getItem("userId");
		this.userRole = localStorage.getItem("userRole");
	}

	/**
   * This function will open image cropper modal
   * @param  null
   * @response image url
   */

	openCropImage() {
		const modalRef = this.modalService.open(ImageCropperPopupComponent, {
		});
		modalRef.componentInstance.inputData = {
			"profileImage": true
		};
		modalRef.result.then((result) => {
			this.commonServices.loader.next(true);
			let data = {};
			let image = result.split(",");
			data["ProfilePictue"] = image[1]
			this.userProfileService.saveProfilePicture(JSON.stringify(data)).subscribe(data => {
				if (data.Status) {
					this.commonServices.showSuccess(API_MESSAGES.UpdateProfile.successMessage, "");
					this.commonServices.loader.next(false);
					this.userProfileData.ProfilePicture = result;
					localStorage.setItem("profileImage", this.userProfileData.ProfilePicture);
					this.commonServices.updateHeaderProfileImage(result);
				} else {
					this.commonServices.showError(data.Message, "");
					this.commonServices.loader.next(false);
				}
			})
		}, (reason) => { });
	}

	getDate(date) {
		return date.replace(/[AP]/, " $&")
	}

}
