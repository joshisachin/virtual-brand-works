import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { API_NAME } from '../../../shared/constants/apiname/apiname';

@Injectable()
export class UserProfileSevice {
    apiName = API_NAME;
    constructor(private http: HttpClient) { }

    /**
   * Get User profile
   * @param fromData
   * @response Object
   */

    getUserProfile(data: any): Observable<any> {
        return this.http.post(this.apiName.GetUserProifle[0].name, data);
    }

    /**
     * get company info
     * @param data
     */
    getCompanyInfo(): Observable<any> {
        return this.http.get(this.apiName.GetCompanyInfo[0].name);
    }
    /**
   * Save profile picture
   * @param fromData
   * @response Object
   */

    saveProfilePicture(data: any): Observable<any> {
        return this.http.post(this.apiName.AddProfileImage[0].name, data);
    }

    /**
   * Update user profile
   * @param fromData
   * @response Object
   */

    updateProfile(data: any): Observable<any> {
        return this.http.post(this.apiName.UpdateUserProfile[0].name, data);
    }
    /**
     * update company
     */
    updateCompany(aboutCompany: string): Observable<any> {
        return this.http.post(this.apiName.UpdateCompany[0].name, {"AboutCompany": aboutCompany})
    }
    /**
     * change company logo
     */
    changeCompanyLogo(data: any): Observable<any> {
        return this.http.post(this.apiName.AddCompanyLogo[0].name, data);
    }

}
