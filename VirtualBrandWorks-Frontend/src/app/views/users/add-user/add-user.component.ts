import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/validation';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserTabSevice } from '../user/user.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
	selector: 'add-user',
	templateUrl: './add-user.component.html',
	styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
	@Input() data;
	addNewUserForm: FormGroup; // form array
	validationMessages = VALIDATION_MESSAGES;
	userRole: any = {};
	isAddUserFormValid: boolean = true;

	constructor(
		private formBuilder: FormBuilder,
		private activeModal : NgbActiveModal,
		private userTabService: UserTabSevice,
		private commonService: CommonService
	) {
		this.addNewUserForm = this.formBuilder.group({
			FirstName: ['', Validators.compose([
			  Validators.required,
			  Validators.pattern('^ *(?:[A-Za-z]+)(?:[A-Za-z _]+ *$)'),
			  Validators.maxLength(50)
			])],
			LastName: ['', Validators.compose([
			  Validators.required,
			  Validators.pattern('^ *(?:[A-Za-z]+)(?:[A-Za-z _]+ *$)'),
			  Validators.maxLength(50)
			])],
			RoleId: ['', Validators.compose([
				Validators.required
			])],
			EmailId: ['', Validators.compose([
				Validators.required,
				Validators.pattern('^ *[a-zA-Z0-9][-a-zA-Z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5} *$'),
				Validators.maxLength(100)
			])]
		});
	}

	/**
   * Add new user
   * @param  isValid
   * @response success
   */
	addUser(isValid) {
		this.isAddUserFormValid = isValid;
		if(isValid) {
			this.commonService.loader.next(true);
			let data = this.addNewUserForm.value;
			this.commonService.trimArray(data).then(result => {
				this.userTabService.addUser(JSON.stringify(result)).subscribe(data => {
					if(data.Status) {
						this.activeModal.close(this.addNewUserForm.value);
					}
					else {
						this.commonService.showError(data.Message, "");
					}
					this.commonService.loader.next(false);
				})
			})			
		}
	}

	close() {
		this.activeModal.dismiss(false);
	}

	ngOnInit(): void {
		this.userRole = [
			{'title': 'Trainer', 'roleId': '3'}, 
			{'title': 'User', 'roleId': '4'}
		]
		this.addNewUserForm.patchValue({
			RoleId: this.data.roleId
		})
		
	} 
}
