import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { UserProfileSevice } from '../user-profile/user-profile.service';
import { ImageCropperPopupComponent } from 'src/app/shared/modals/image-cropper/image-cropper-popup.component';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-about-company',
  templateUrl: './about-company.component.html',
  styleUrls: ['./about-company.component.scss']
})
export class AboutCompanyComponent implements OnInit {
  companyLogo: string = "";
  aboutCompany: string = "";
  update: boolean = false;
  status: string;
  name: string;
  constructor(private modalService: NgbModal, private commonServices: CommonService, private userProfileService: UserProfileSevice) { }

  ngOnInit(): void {
    this.getCompanyInfo();
  }

  /**
	 * change logoe
	 * @param null
	 * @response null
	 */
	changeLogo() {
		const modalRef = this.modalService.open(ImageCropperPopupComponent, {});

		modalRef.componentInstance.inputData = {
			"profileImage": false
		};
		modalRef.result.then((result) => {
			this.commonServices.loader.next(true);
			let data = {};
			let image = result.split(",");
			data["CompanyLogo"] = image[1];
			this.userProfileService.changeCompanyLogo(JSON.stringify(data)).subscribe(data => {
				if (data.Status) {
					this.commonServices.showSuccess(data.Message, "");
					this.commonServices.loader.next(false);
					this.companyLogo = result;
					localStorage.setItem("logoImage", this.companyLogo);
					this.commonServices.updateLogoImage.next(result);
				} else {
					this.commonServices.showError(data.Message, "");
					this.commonServices.loader.next(false);
				}
			})
		}, (reason) => { });
	}

  /**
	 * get company info
	 * @param  null
	 * @response Object
	 */
	getCompanyInfo() {
		this.commonServices.loader.next(true);
		this.userProfileService.getCompanyInfo().subscribe(data => {
			if (data.Status) {
				this.commonServices.loader.next(false);
        this.aboutCompany = data.Data.AboutCompany;
		this.companyLogo = data.Data.CompanyLogo;
		this.status = data.Data.Status;
		this.name = data.Data.CompanyName;
			}
		});
	}
	
	/**
	 * update company about
	 * @param null
	 * @response null
	 */
	updateAbout() {
		this.commonServices.loader.next(true);
		this.userProfileService.updateCompany(this.aboutCompany.trim()).subscribe(data => {
			if (data.Status) {
				this.update = false;
				this.commonServices.showSuccess(API_MESSAGES.UpadateFolder.successMessage, "");
			}
			else {
				this.commonServices.showError(data.Message, "");
			}
			this.commonServices.loader.next(false);
		});
	}
}
