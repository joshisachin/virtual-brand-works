import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { UserComponent } from './user/user.component';
import { ProfileComponent } from './profile/profile.component';
import { MyPlanComponent } from './my-plan/my-plan.component';

const routes: Routes = [
	{
		path: '',
		component: UsersComponent,
		children: [
			{
				path: '',
				component: UserComponent
			},
			{
				path: 'profile/:userId/:role',
				component: ProfileComponent
			},
			{
				path: 'profile/:userId',
				component: ProfileComponent
			},
			{
				path: 'playlist/:userId/:role/:playlistId/:isSelfCreated',
				component: ProfileComponent,
			},
			{
				path: 'playlist/:userId/:playlistId',
				component: ProfileComponent,
			}
		]
	}
]
@NgModule({
	imports: [
        RouterModule.forChild(routes)
    ],
	exports: [
        RouterModule
    ]
})
export class UsersRoutingModule { }
