import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/validation';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Country } from 'src/app/shared/model/country';
import { State } from 'src/app/shared/model/state';
import { City } from 'src/app/shared/model/city';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { TagsComponent } from "../../elements/tags/tags.component";
import { UserProfileSevice } from '../user-profile/user-profile.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
	selector: 'edit-profile',
	templateUrl: './edit-profile.component.html',
	styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
	@Input() data;
	countries: Country; // countries array
	states: State; // states array
	cities: City; // cities array
	editProfileFrom: FormGroup;
	selectedTags: any = []; // selected tags array	
	validationMessages: any; // validation messages array
	isFormValid: boolean = true;
	userRole: string = localStorage.getItem("userRole");

	constructor(
		private formBuilder: FormBuilder,
		private activeModal : NgbActiveModal,
		private httpService: HttpService,
		private userProfileService: UserProfileSevice,
		private commonService: CommonService
	) {
		this.editProfileFrom = this.formBuilder.group({
			FirstName: ['', Validators.compose([
				Validators.required,
				Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z _]+$)'),
				Validators.maxLength(50)
			])],
			LastName: ['', Validators.compose([
				Validators.required,
				Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z _]+$)'),
				Validators.maxLength(50)
			])],
			Email: [{value:'', disabled: true}],
			Description: ['', Validators.compose([
				Validators.maxLength(500)
			])],
			PhoneNumber: ['', Validators.compose([
			  Validators.maxLength(15),
			  Validators.required,
			  Validators.pattern('^[0-9]{9,15}$')
			])],
			CountryId: ['', Validators.compose([
			  Validators.required
			])],
			StateId: ['', Validators.compose([
			  Validators.required
			])],
			CityId: ['', Validators.compose([
			  Validators.required
			])],
			Zip: ['', Validators.compose([
			  Validators.required,
			  Validators.pattern('^(?:[A-Za-z0-9]+)(?:[A-Za-z0-9 _]+$)'),
			  Validators.maxLength(30)
			])],
			AddressOne: ['', Validators.maxLength(150)],
			AddressTwo: ['', Validators.maxLength(200)],
			Tags: this.formBuilder.array([]),
			HandleName: [{value:'', disabled: true}]
		  });
		this.validationMessages = VALIDATION_MESSAGES;
	}

	ngOnInit(): void {		
		this.editProfileFrom.patchValue({
			FirstName: this.data.userData.FirstName,
			LastName: this.data.userData.LastName,
			Email: this.data.userData.EmailId,
			Description: this.data.userData.Description,	
			PhoneNumber: this.data.userData.PhoneNumber,
			CountryId: this.data.userData.CountryId,
			StateId: this.data.userData.StateId,
			CityId: this.data.userData.CityId,
			Zip: this.data.userData.Zip,
			AddressOne: this.data.userData.AddressOne,
			AddressTwo: this.data.userData.AddressTwo,
			HandleName: this.data.userData.HandleName
		})

		this.selectedTags = this.data.userData.Tags;
		this.commonService.selectedTags.next(this.data.userData.Tags);

		this.httpService.getCountries().subscribe(data => { // get countries
			if (data.Data) {
			  this.countries = data.Data;
			  this.getStates(this.editProfileFrom.value.CountryId)
			}
		});
	}
	
	/**
   * get states
   * @param countryId 
   * @response list
   */
	getStates(countryId: number) {
		this.httpService.getStates(countryId).subscribe(data => {
			if (data.Data) {
				this.states = data.Data;
				this.getCities(this.editProfileFrom.value.StateId)
			}
		});
	}

	/**
	 * get cities
	 * @param stateId 
	 * @response list
	 */
	getCities(stateId: number) {
		this.httpService.getCities(stateId).subscribe(data => {
			if (data.Data) {
				this.cities = data.Data;
			}
		});
	}

	/**
   * get selected tags
   * @param tags
   * @response object
   */
	getSelectedTags(tags: any) {
		this.selectedTags = [];
		if (tags.length > 0) {
			tags.map(data => {
				this.selectedTags.push(data);
			});
		}
	}

	/**
   * submit user profile form
   * @param isValid
   * @response object
   */

	submitForm(isValid) {
		this.isFormValid = isValid;	
		if(isValid) {
			this.commonService.loader.next(true);
			this.selectedTags.map(object => {
				this.editProfileFrom.value.Tags.push({"TagId": object.TagId});
			})
			this.userProfileService.updateProfile(JSON.stringify(this.editProfileFrom.value)).subscribe(data => {
				if(data.Status) {
					this.commonService.showSuccess(API_MESSAGES.UpdateProfile.successMessage, "");			
					this.commonService.loader.next(false);
					this.activeModal.close(data.Data);
				} else {
					this.commonService.showError(data.Message, "");
					this.commonService.loader.next(false);
				}
			})
		}		
	}

	close() {
		this.activeModal.dismiss(false);
	}
}
