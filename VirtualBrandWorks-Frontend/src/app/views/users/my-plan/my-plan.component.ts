import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { ChangePlanComponent } from 'src/app/shared/modals/change-plan/change-plan.component';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { UpdateBillingComponent } from 'src/app/shared/modals/update-billing/update-billing.component';
import { MyPlan } from 'src/app/shared/model/my-plan';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { CancelPlanComponent } from '../../elements/cancel-plan/cancel-plan.component';
import { UserTabSevice } from '../user/user.service';
declare var Stripe: any;
@Component({
  selector: 'app-my-plan',
  templateUrl: './my-plan.component.html',
  styleUrls: ['./my-plan.component.scss']
})
export class MyPlanComponent implements OnInit {
  planDetails: MyPlan;
  constructor(private userServices: UserTabSevice, private commonServices: CommonService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getPlanDetails();
  }

  /**
   * get plan details
   * @param null
   * @response null
   */
  getPlanDetails() {
    this.userServices.getPlanDetails().subscribe(data => {
      if (data.Status) {
        this.planDetails = data.Data;
        let address = this.planDetails.PaymentCard.BillingDetails?.address;
        if(address){
        for (var key in address){
          if(address[key] == null){
            address[key] = '';
          }
        }
      }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * change plan
   * @param null
   * @response null
   */
  changePlanConfirm() {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {centered: true});
    modalRef.componentInstance.data = {
      "title": "Please read and click ok to accept the terms for change of plan to proceed ahead:\r\n\r\n"+
      "1. The changed plan will be applicable from immediate effect. However, the arrears for the same will be adjusted in the upcoming billing cycles if the cycle does not change i.e from monthly to yearly or vice versa.\r\n"+
      "2. In case the plan is being downgraded the remaining amount will be adjusted in further billing cycles.\r\n"+
      "3. If the billing plan is changed to annual billing then the remaining amount after adjustments shall be charged at the time of changing the plan."
		};
		modalRef.result.then((result) => {
      this.changePlan();
		}, (reason) => { });
  }

  /**
   * change plan
   * @param null
   * @response null
   */
  changePlan() {
    const modalRef = this.modalService.open(ChangePlanComponent, {size: 'lg', centered: true});
		modalRef.componentInstance.planData = {
      PlanType: this.planDetails.PlanType,
      PlanName: this.planDetails.SubscriptionName,
      NoTrial: true
		};
		modalRef.result.then((result) => {
      if (!result.SubscriptionPlanPriceId) {
        this.commonServices.showError(API_MESSAGES.Plan.NoPlan, "");
        this.changePlan();
        return;
      }
      this.userServices.changePlan({"planId": result.SubscriptionPlanPriceId, "stripeSubscriptionPlanId": result.PlanId, "oldPlanName": this.planDetails.SubscriptionName}).subscribe(data => {
        if (data.Status) {
          let previousPlan = this.planDetails.PlanType;
          this.getPlanDetails();
          if(previousPlan == 2 && result.PlanType == 1){
            let result = this.paymentUpdate() 
          }
          else
          this.commonServices.showSuccess(API_MESSAGES.UpadateFolder.successMessage, "");
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
      });
		}, (reason) => { });
  }

  /**
   * change address
   * @param null
   * @response null
   */
  updateAddress() {
    const modalRef = this.modalService.open(UpdateBillingComponent);
		modalRef.componentInstance.data = this.planDetails; 
		modalRef.result.then((result) => {
      this.getPlanDetails();
		}, (reason) => { });
  }

  /**
   * update payment
   * @param null
   * @response null
   */
  paymentUpdate() {
    this.userServices.createSession().subscribe(data => {
      if (data.Status) {
        localStorage.setItem("sessionId", data.Data);
        this.makePayment(data.Data);
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * make payment
   * @param sessionId 
   * @response message
   */
  makePayment(sessionId: string) {
    let stripe = Stripe('pk_test_4FSrCf5onLCLEqcayRaD2Aki00lhVxVytM');
    stripe.redirectToCheckout({
      sessionId: sessionId
    }).then(() => {
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer
      // using `result.error.message`.
      this.commonServices.showError(API_MESSAGES.CommonError.message, "");
    });
  }

  /**
   * cancel plan
   * @param null
   * @response null
   */
  cancelPlan() {
    const modalRef = this.modalService.open(CancelPlanComponent);
		modalRef.componentInstance.data = this.planDetails;
		modalRef.result.then((result) => {
      this.getPlanDetails();
		}, (reason) => { });
  }

}
