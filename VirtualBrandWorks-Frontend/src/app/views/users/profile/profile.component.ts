import { Component, OnInit } from '@angular/core';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PlaylistService } from '../../my-playlist/playlist.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userRole: any;
  loggedInUserId: any;
  userId: any;
  loggedInUserRole: any;
  playListDetail: boolean = false;
  routePlaylistId: any = 0;
  activeId: number;
  playlistDetail: string = 'associatedPlaylistDetail'

  constructor(private router: Router, 
    private route: ActivatedRoute,
    private playlistService: PlaylistService,
    private _location: Location
    ) { 
      this.route.params.subscribe(params => {
        this.userId = params.userId;  
        this.userRole = params.role;
        this.routePlaylistId = params.playlistId
      })
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {    
    this.playlistService.profileTabIndex = changeEvent.nextId;
    if(this.routePlaylistId != 0) {
      this._location.back();
    }
  }
  ngOnInit() {
    this.routePlaylistId = this.routePlaylistId == undefined ? 0 : this.routePlaylistId;
    this.activeId = this.playlistService.profileTabIndex;
    this.loggedInUserRole = localStorage.getItem("userRole");
    this.loggedInUserId = localStorage.getItem("userId");
  }

}
