import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedUsersComponent } from './associated-users.component';

describe('AssociatedUsersComponent', () => {
  let component: AssociatedUsersComponent;
  let fixture: ComponentFixture<AssociatedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatedUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
