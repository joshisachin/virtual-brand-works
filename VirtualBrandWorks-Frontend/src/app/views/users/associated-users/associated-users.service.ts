import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { API_NAME } from '../../../shared/constants/apiname/apiname';

@Injectable()
export class AssociatedUsersService {

	apiName = API_NAME;
	constructor(private http: HttpClient) { }

	/**
	 * Get user associated teams
	 * @param data
	 */
	getUserUserAssociatedTeam(data: any): Observable<any> {
		return this.http.get(this.apiName.GetUserAssociatedTeam[0].name+"?userId="+data.userId+"&search="+data.search);
	}
	
	/**
	 * Get user associated users
	 * @param data
	 */
	getUserUserAssociatedUser(data: any): Observable<any> {
		return this.http.get(this.apiName.GetUserAssociatedUsers[0].name+"?userId="+data.userId+"&search="+data.search);
	} 
}
