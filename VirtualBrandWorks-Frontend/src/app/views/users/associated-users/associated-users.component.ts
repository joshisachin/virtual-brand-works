import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AssociatedUsersService } from './associated-users.service';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-associated-users',
  templateUrl: './associated-users.component.html',
  styleUrls: ['./associated-users.component.scss']
})
export class AssociatedUsersComponent implements OnInit {

  routeUserId: any;
  routeRole: string;
  userTeamOption: any;
  searchKey: string = "";
  isResult: boolean = true;
  checkedValue: any;
  userListData: any;
  isReverse: boolean = false; // for reverse sort

  @ViewChild("searchTeamUser", { static: true }) searchTeamUser: ElementRef;
  constructor(
    public associatedUserService: AssociatedUsersService,
    private route: ActivatedRoute,
    private commonService: CommonService
  ) {
      this.route.params.subscribe(params => {
      this.routeUserId = params.userId;
      this.routeRole = params.role;
      this.getAssociatedTeams();
    })

  }

  ngOnInit(): void {
    this.clearData();
    this.userTeamOption = [
      { "title": "Team", "value": "team", "id": "teamCheck" },
      { "title": "User", "value": "user", "id": "userCheck" }
    ]

    this.checkedValue = "team";

    this.commonService.searchDebounce(this.searchTeamUser.nativeElement).subscribe(searchString => {
      if (searchString !== "") {
        this.searchKey = searchString;
        this.userListData = null;
      }
      (this.checkedValue == 'team') ? this.getAssociatedTeams() : this.getAssociatedUsers();
    });
  }

  /**
   * Get checkbox selected value
   * @param event 
   */

  getSelectedCheckboxValue(event) {
    this.clearData();
    (this.checkedValue == 'team') ? this.getAssociatedTeams() : this.getAssociatedUsers();
  }

  /**
   * Get associated team list
   * @param null
   */

  getAssociatedTeams() {
    let data = {};
    data["userId"] = this.routeUserId;
    data["search"] = this.searchKey;
    this.commonService.loader.next(true);
    this.associatedUserService.getUserUserAssociatedTeam(data).subscribe(result => {
      if (result.Status) {
        this.commonService.loader.next(false);
        this.isResult = result.Data.length == 0 ? false : true;
        this.userListData = result.Data;
      } else {
        this.commonService.showError(result.Message, "");
        this.commonService.loader.next(false);
      }
    })
  }

  /**
   * Get associated users list
   * @param null
   */

  getAssociatedUsers() {
    let data = {};
    data["userId"] = this.routeUserId;
    data["search"] = this.searchKey;
    this.commonService.loader.next(true);
    this.associatedUserService.getUserUserAssociatedUser(data).subscribe(result => {
      if (result.Status) {
        this.commonService.loader.next(false);
        this.isResult = result.Data.length == 0 ? false : true;
        this.userListData = result.Data;
      } else {
        this.commonService.showError(result.Message, "");
        this.commonService.loader.next(false);
      }
    })
  }

  /**
   * sort
   * @param type
   * @param order
   * @response object
   */
  sort(type: string = "", order: string = "") {
    this.commonService.performSorting(this.userListData, type);
    if (this.isReverse) {
      this.userListData.reverse();
    }
    this.isReverse = !this.isReverse;
  }

  clearData() {
    this.userListData = null;
    this.searchKey = "";
    this.isResult = true;
  }

}
