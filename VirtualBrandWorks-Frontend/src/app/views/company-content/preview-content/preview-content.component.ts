import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { CompanyContentService } from '../company-content.service';
import { ConfirmContent } from 'src/app/shared/model/confirm-content';

@Component({
  selector: 'app-preview-content',
  templateUrl: './preview-content.component.html',
  styleUrls: ['./preview-content.component.scss']
})
export class PreviewContentComponent implements OnInit {
  @Input() data;
  fileType: string = "";
  confirmContentData: ConfirmContent;
  constructor(
    private activeModal: NgbActiveModal,
    private domSanitizer: DomSanitizer,
    private companyServices: CompanyContentService
  ) {
  }

  close() {
    this.activeModal.close(true);
  }

  ngOnInit(): void {
    this.confirmContentData = this.data;
    this.setFileType()
    if (this.confirmContentData.CompanyLibraryContentId) {
      this.companyServices.contentConfirmation(this.confirmContentData.CompanyLibraryContentId).subscribe(result => {
        if (result.Status) {
          this.confirmContentData = result.Data;
          this.setType();
        }
      })
    } else {
      this.setType();
    }
  }

  /**
   * set file type
   * @param null
   * @response null
   */
  setFileType() {
    if (this.confirmContentData.MimeType.match("image")) {
      this.fileType = "image"
    }
    else if (this.confirmContentData.MimeType.match("pdf")) {
      this.fileType = "pdf"
    }
    else if (this.confirmContentData.MimeType.match("txt")) {
      this.fileType = "text"
    }
    else if (this.confirmContentData.MimeType.match("video")) {
      this.fileType = "video"
    }
    else {
      this.fileType = "doc"
    }
  }

  /**
   * set the type of content
   * @param null
   * @response null
   */
  setType() {
    if (this.fileType == 'doc' || this.fileType == 'text' || this.fileType == 'pdf') {
      let url;
      if (this.confirmContentData.CompanyLibraryContentId) {
        url = "https://docs.google.com/gview?url=" + encodeURIComponent(this.confirmContentData.ContentUrl) + "&embedded=true";
      } else {
        url = URL.createObjectURL(this.confirmContentData.ContentUrl)
      }
      this.confirmContentData.ContentUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }

  /**
   * create content modal
   */
  createContent() {
    this.activeModal.close(true); // close the active modal
    this.companyServices.createContentPopup();
  }

}
