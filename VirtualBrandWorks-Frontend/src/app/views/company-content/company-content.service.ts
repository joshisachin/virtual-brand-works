import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_NAME } from '../../shared/constants/apiname/apiname';
import { Observable, BehaviorSubject } from 'rxjs';
import { CreateContentComponent } from './create-content/create-content.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class CompanyContentService {

  changeVideo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private modalService: NgbModal) { }

  /**
   * update folder
   */
  updateTeam(name: string, id: number, shortName: string): Observable<any> {
    return this.http.post(API_NAME.UpdateCompanyFolder[0].name, { "Name": name, "CompanyLibraryFolderId": id, "ShortName": shortName });
  }

  /**
   * create folder
   */
  createFolder(name: string, shortName: string): Observable<any> {
    return this.http.post(API_NAME.CreateFolder[0].name, { "Name": name, "ShortName": shortName });
  }

  /**
   * get company folders
   */
  getCompanyFolders(): Observable<any> {
    return this.http.get(API_NAME.GetCompanyFolders[0].name);
  }

  /**
   * create content
   */
  createContent(formData: any): Observable<any> {
    return this.http.post(API_NAME.CreateContent[0].name, formData)
  }

  /**
   * publish content
   */
  publishContent(contentId: number, isPublished: boolean = true): Observable<any> {
    return this.http.post(API_NAME.PublishContent[0].name, { "ContentId": contentId, "IsPublished": isPublished });
  }

  /**
   * create content
   */
  createContentPopup(playlistFolderId: number = 0, folderId: number = 0): Promise<any> {
    return new Promise((resolve, reject) => {
      const modalRef = this.modalService.open(CreateContentComponent, {
        windowClass: 'w925',
        centered: true
      });
  
      modalRef.componentInstance.playlistFolderId = playlistFolderId;
      modalRef.componentInstance.folderId = folderId;
      modalRef.result.then((result) => {
        // later use
        resolve(true);
      }, (reason) => {
        // for later use
        resolve(true);
      });
    });
  }

  /**
   * Content confirmation after publish
   * @param contentId
   */
  contentConfirmation(contentId: number): Observable<any> {
    return this.http.post(API_NAME.GetContentConfirmation[0].name, { "ContentId": contentId });
  }

  /**
   * delete content
   */
  deleteContent(contentId: number): Observable<any> {
    return this.http.post(API_NAME.DeleteContent[0].name, {"ContentId": contentId});
  }

  /**
   * broadcast
   */
  broadcast(contentId: number, isBroadcast: boolean): Observable<any> {
    return this.http.post(API_NAME.BroadcastCompanyContent[0].name, {"ContentId": contentId, "IsBroadcasted": isBroadcast});
  }

  /**
   * change status
   */
  changeStatus(contentId: number, value: string): Observable<any> {
    return this.http.post(API_NAME.UpdateCompanyContentVisibility[0].name, {"ContentId": contentId, "Visibility": value});
  }
}
