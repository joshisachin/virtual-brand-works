import { TestBed } from '@angular/core/testing';

import { CompanyContentService } from './company-content.service';

describe('CompanyContentService', () => {
  let service: CompanyContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompanyContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
