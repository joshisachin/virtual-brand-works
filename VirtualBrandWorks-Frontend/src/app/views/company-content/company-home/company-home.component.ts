import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FolderComponent } from "../../elements/folder/folder.component";
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateFolderComponent } from 'src/app/views/company-content/create-folder/create-folder.component';
import { CompanyContentService } from '../company-content.service';

@Component({
  selector: 'app-company-home',
  templateUrl: './company-home.component.html',
  styleUrls: ['./company-home.component.scss']
})
export class CompanyHomeComponent implements OnInit {

  folderType: string = "company-content"; // type of folder send
  showFooterFolderVar: boolean = false; // toggle for selected folder footer
  countSelect: number = 0; // count of selected folders
  @ViewChild("searchFolder", { static: true }) searchFolder: ElementRef; // for search

  constructor(private commonServices: CommonService, private httpServices: HttpService, private modalService: NgbModal, private companyServices: CompanyContentService) { }

  ngOnInit(): void {
    this.commonServices.searchDebounce(this.searchFolder.nativeElement).subscribe(searchString => {
      this.getSearchData(searchString); // call for search
    });
  }

  ngAfterViewInit() {
    this.initFolderComponent();
  }

  /**
   * initialize the folder variable
   */
  initFolderComponent() {
    this.commonServices.selectedFolders.subscribe(data => {
      if (data) {
        this.countSelect = data.length; // get the count
      }
    });
  }

  /**
   * toggle folder footer
   * @param null
   * @eesponse null
   */
  showFooterFolder() {
    this.showFooterFolderVar = !this.showFooterFolderVar;
  }

  /**
   * get search data
   * @param param
   * @response object
   */
  getSearchData(param) {
    this.commonServices.searchDataForFolder.next(param);
    this.httpServices.getCompanyContentFolder(param, "", 1, environment.PAGINATE_FETCH).subscribe(data => {
      if (data.Status && data.Data.length > 0) {
        let folders = data.Data.filter((res) => { // filter to separate dynamic folder
          return res.IsDynamicFolder == false;
        });
        FolderComponent.stopScroll.next(false);
        this.commonServices.folders.next(folders); // updating the value
      }
    });
  }

  /**
   * create folder popup
   * @param null
   * @response null
   */
  createFolderPopup() {
    const modalRef = this.modalService.open(CreateFolderComponent, {
      windowClass: 'modalwidth',
      centered: true
    });

    modalRef.componentInstance.data = {
    };
    modalRef.result.then((result) => {
      this.getSearchData("");
    }, (reason) => {
      // for later use
    });
  }

  /**
   * create content
   * @param null
   * @response null
   */
  createContent() {
    this.companyServices.createContentPopup();
  }

}
