import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyContentComponent } from './company-content.component';

describe('CompanyContentComponent', () => {
  let component: CompanyContentComponent;
  let fixture: ComponentFixture<CompanyContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
