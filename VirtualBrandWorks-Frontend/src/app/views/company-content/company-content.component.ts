import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-company-content',
  templateUrl: './company-content.component.html',
  styleUrls: ['./company-content.component.scss']
})
export class CompanyContentComponent implements OnInit {

  toggleRightSide: boolean; // to expand right side when side menu shrinks

  constructor(private commonServices: CommonService) {
    this.commonServices.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  }

  ngOnInit(): void {
  }

}
