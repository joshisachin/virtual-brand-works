import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/content-validation';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { FileHandle } from 'src/app/shared/directive/upload.directive';
import { DomSanitizer } from '@angular/platform-browser';
import { Folder } from 'src/app/shared/model/folder';
import { CompanyContentService } from '../company-content.service';
import { S3bucketService } from 'src/app/shared/services/s3bucket/s3bucket.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { environment } from 'src/environments/environment';
import { PreviewContentComponent } from '../preview-content/preview-content.component';

@Component({
  selector: 'app-create-content',
  templateUrl: './create-content.component.html',
  styleUrls: ['./create-content.component.scss']
})
export class CreateContentComponent implements OnInit {

  createContentForm: FormGroup; // create content form
  validationMessages: any; // validation messages
  selectedTags: any = []; // selected tags array
  files: FileHandle[] = []; // file array
  fileType: string = "";
  folders: Folder[];
  bucketName = 'virtualbrandworks';
  enableOtherFolderName: boolean = false;
  uploader: any;
  folderId: number = 0;
  contentId: number;
  tagsArr: [];
  @Input() playlistFolderId: number = 0;
  @Input() selectedFolderId: number = 0;
  duration: number = 0;
  selectedFolder: Folder; // in case of no folder list to be shown

  constructor(private activeModal: NgbActiveModal, private formBuilder: FormBuilder, private companyServices: CompanyContentService, private commonServices: CommonService, private sanitizer: DomSanitizer, private awsService: S3bucketService, private modalService: NgbModal) {
    this.createContentForm = this.formBuilder.group({
      FolderId: [null, Validators.compose([
        Validators.required,
      ])],
      Name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30),
        Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]+$)')
      ])],
      Description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(500),
        Validators.minLength(4)
      ])],
      file: [this.formBuilder.array([]), Validators.compose([
        Validators.required,
      ])],
      FolderName: [""],
      FolderShortName: [""],
      IsBroadcasted: [false],
    });
    this.validationMessages = VALIDATION_MESSAGES;
    this.commonServices.selectedTags.next([]);
  }

  ngOnInit(): void {
    this.getFolders();
    if (this.folderId) {
      this.createContentForm.patchValue({"FolderId": this.folderId});
    }
  }

  /**
   * get folders
   * @param null
   * @response object
   */
  getFolders() {
    this.companyServices.getCompanyFolders().subscribe(data => {
      if (data.Status) {
        this.folders = data.Data;
        this.selectedFolder = this.folders.find(element => element.FolderId == this.folderId);
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    })
  }

  /**
   * get video duration
   * @param e 
   */
  getDuration(e: any) {
    this.duration = e.target.duration;
  }

  /**
   * get selected tags
   * @param tags
   * @response object
   */
  getSelectedTags(tags: any) {
    this.selectedTags = [];
    this.tagsArr = [];
    if (tags.length > 0) {
      this.tagsArr = tags;
      this.commonServices.selectedTags.next(this.tagsArr);
      tags.map(data => {
        this.selectedTags.push(data.TagId);
      });
    }
  }

  /**
   * close the modal
   */
  close() {
    this.activeModal.dismiss(false);
  }

  /**
   * create folder
   * @param formData 
   * @response null
   */
  create(formData: FormData) {
    if (this.createContentForm.valid) {
      if (this.duration > 300) {
        this.commonServices.showError(API_MESSAGES.Content.videoDuration, "");
        return false;
      }
      this.commonServices.loader.next(true);
      this.commonServices.trimArray(formData).then(data => {
        formData = data;
        if (formData["FolderId"] == 0 && this.folderId == 0) {
          this.companyServices.createFolder(formData["FolderName"], formData["FolderShortName"]).subscribe(data => {
            if (data.Status) {
              this.folderId = data.Data.FolderId;
              formData["FolderId"] = data.Data.FolderId;
              this.performUpload(formData);
            }
            else {
              this.commonServices.showError(data.Message, "");
              this.commonServices.loader.next(false);
            }
          })
        }
        else {
          this.performUpload(formData);
        }
      });
    }
    else {
      this.createContentForm.markAllAsTouched();
    }
  }

  /**
   * perform upload
   * @param formData
   */
  performUpload(formData: any) {
    this.awsService.performUpload(formData["file"], environment.FILE_TYPES).then(data => {
      formData["Tags"] = this.selectedTags; // add tags
      formData["ContentUrl"] = data;
      formData["MimeType"] = (!formData["file"].type || formData["file"].type.match('video')) ?this.fileType + '/mp4' : formData["file"].type,
      formData["PlaylistFolderId"] = this.playlistFolderId;
      if (this.playlistFolderId) {
        formData["IsPublished"] = true;
      }
      delete formData["FolderName"]; // delete not required object
      delete formData["FolderShortName"]; // delete not required object
      delete formData["file"]; // delete not required object
      this.companyServices.createContent(formData).subscribe(data => {
        if (data.Status) {
          this.contentId = data.Data.ContentId;
          this.commonServices.showSuccess(API_MESSAGES.Content.successMessage, "");
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
      });
    })
      .catch(err => {
        this.commonServices.showError(API_MESSAGES.FileUpload.errorMessage, "");
        this.commonServices.loader.next(false);
      });
  }

  /**
   * publish content
   * @param null
   * @response null
   */
  publishContent() {
    if (this.contentId) {
      this.commonServices.loader.next(true);
      this.companyServices.publishContent(this.contentId).subscribe(data => {
        if (data.Status) {
          this.commonServices.showSuccess(API_MESSAGES.Content.publishMessage, "");
          this.activeModal.close(true);
          this.previewContent(this.contentId)
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
      });
    }
  }

  /**
   * reset form
   * @param null
   * @response null
   */
  reset() {
    this.createContentForm.reset();
    this.createContentForm.patchValue({ "IsBroadcasted": false });
    this.selectedTags = [];
    this.files = []
    this.fileType = "";
    this.enableOtherFolderName = false;
    this.folderId = 0
    this.contentId = undefined;
    this.commonServices.selectedTags.next([]);
  }

  /**
   * manage drag drop file
   * @param files
   * @response object
   */
  fileBrowserHandler(files: FileHandle[]): void {
    this.files = [];
    if (files.length !== 1) {
      this.commonServices.showError(API_MESSAGES.FileUpload.fileCount, "");
    }
    else {
      this.files = files;
      this.createContentForm.patchValue({ "file": this.files[0].file });
      this.createContentForm.controls["file"].clearValidators();
      this.createContentForm.controls["file"].updateValueAndValidity();
      this.setFileType();
    }
  }

  /**
   * manage manually uploaded file
   * @param event
   * @response object
   */
  upload(event: any = []): void {
    this.files = [];
    this.duration = 0;
    if (event && event.length == 1) {
      for (let i = 0; i < event.length; i++) {
        const file = event[i];
        const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(file));
        this.files.push({ file, url });
        this.createContentForm.patchValue({ "file": this.files[0].file });
        this.setFileType();
      }
    }
    else {
      this.commonServices.showError(API_MESSAGES.FileUpload.fileCount, "");
    }
    //get image upload file obj;
  }

  /**
   * set file type
   * @param null
   * @response null
   */
  setFileType() {

    if (this.files[0].file.type.match("image")) {
      this.fileType = "image"
    }
    else if (this.files[0].file.type.match("pdf")) {
      this.fileType = "pdf"
    }
    else if (this.files[0].file.type.match("video") || (this.files[0].file.name.substring(this.files[0].file.name.lastIndexOf('.') + 1)).toLowerCase() == 'mov') {
      this.fileType = "video"
    }
  }

  /**
   * check other folder
   * @param value
   * @reponse null
   */
  checkOther(value) {
    if (value == "0") {
      this.enableOtherFolderName = true;
      this.createContentForm.controls["FolderName"].setValidators([Validators.required, Validators.pattern("^(?:[A-Za-z]+)(?:[A-Za-z _]+$)"), Validators.maxLength(30)]); // set the validation for name
      this.createContentForm.controls["FolderShortName"].setValidators([Validators.required, Validators.pattern("^(?:[A-Za-z]+)(?:[A-Za-z _]+$)"), Validators.maxLength(2), Validators.minLength(2)]); // set the validators for the short name
    }
    else {
      this.enableOtherFolderName = false;
      this.createContentForm.controls["FolderName"].clearValidators(); // clear fname validator
      this.createContentForm.controls["FolderShortName"].clearValidators() // clear short namevalidator
    }
    this.createContentForm.controls['FolderName'].updateValueAndValidity();
    this.createContentForm.controls["FolderShortName"].updateValueAndValidity();
  }

  /**
   * content preview and confirmation
   * @param null
   * @reponse null
   */
  previewContent(contentId?) {
    const modalRef = this.modalService.open(PreviewContentComponent, {
      ariaLabelledBy: 'modal-basic-title',
      windowClass: 'w925',
      centered: true
    });

    modalRef.componentInstance.data = {
      "Description": this.createContentForm.value.Description,
      "MimeType": (!this.files[0].file.type || this.files[0].file.type.match('video')) ?this.fileType + '/mp4' : this.files[0].file.type,
      "Tags": this.tagsArr,
      "CompanyLibraryContentId": contentId ? contentId : null,
      "ContentUrl": this.fileType == 'doc' || this.fileType == 'text' || this.fileType == 'pdf' ? this.createContentForm.value.file : this.files[0].url
    };

    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }
}
