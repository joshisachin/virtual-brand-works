import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyHomeComponent } from './company-home/company-home.component';
import { CompanyContentComponent } from './company-content.component';
import { DetailsComponent } from './details/details.component';
import { ContentDetailsComponent } from './content-details/content-details.component';


const routes: Routes = [
	{
		path: '',
		component: CompanyContentComponent,
		children: [
			{
				path: '',
				component: CompanyHomeComponent,
				data: {
					breadcrumb: [
						{
							label: 'Company Content',
							url: ''
						}
					]
				},
			},
			{
				path: ':name/:id',
				component: DetailsComponent,
				data: {
					breadcrumb: [
						{
							label: 'Company Content',
							url: '/company-content'
						},
						{
							label: '{{ name }}',
							url: '',
						}
					]
				},
			},
			{
				path: ':name/:id/:contentName/:idNew',
				component: ContentDetailsComponent,
				data: {
					breadcrumb: [
						{
							label: 'Company Content',
							url: '/company-content'
						},
						{
							label: '{{ name }}',
							url: '/company-content/:name/:id',
						},
						{
							label: '{{ contentName }}',
							url: '',
						}
					]
				},
			}
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CompanyContentRoutingModule { }
