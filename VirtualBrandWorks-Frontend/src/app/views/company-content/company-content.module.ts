import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CompanyContentRoutingModule } from './company-content-routing.module';
import { CompanyContentComponent } from './company-content.component';
import { CompanyHomeComponent } from './company-home/company-home.component';
import { CreateFolderComponent } from 'src/app/views/company-content/create-folder/create-folder.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateContentComponent } from './create-content/create-content.component';
import { DetailsComponent } from './details/details.component';
import { NgDynamicBreadcrumbModule } from 'ng-dynamic-breadcrumb';
import { UploadDirective } from 'src/app/shared/directive/upload.directive';
import { ContentDetailsComponent } from './content-details/content-details.component';
import { PreviewContentComponent } from './preview-content/preview-content.component';

@NgModule({
  declarations: [
    CompanyContentComponent,
    CompanyHomeComponent,
    CreateFolderComponent,
    CreateContentComponent,
    DetailsComponent,
    ContentDetailsComponent,
    PreviewContentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CompanyContentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgDynamicBreadcrumbModule
  ]
})
export class CompanyContentModule { }
