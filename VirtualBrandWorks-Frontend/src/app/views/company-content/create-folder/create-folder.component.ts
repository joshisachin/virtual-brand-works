import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyContentService } from 'src/app/views/company-content/company-content.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/validation';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
	selector: 'app-create-folder',
	templateUrl: './create-folder.component.html',
	styleUrls: ['./create-folder.component.scss']
})
export class CreateFolderComponent implements OnInit {

	createFolder: FormGroup; // create folder form group
	validationMessages: any; // validation messages
	name: string = ""; // to bind name into folder view
	shortName: string = ""; // to bind shortname into folder view
	
	constructor(private activeModal: NgbActiveModal, private companyServices: CompanyContentService,
		private formBuilder: FormBuilder, private commonServices: CommonService) {
		this.createFolder = this.formBuilder.group({
			Name: ['', Validators.compose([
				Validators.required,
				Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z _]+$)'),
				Validators.maxLength(30)
			])],
			ShortName: ['', Validators.compose([
				Validators.required,
				Validators.minLength(2),
				Validators.maxLength(2),
				Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z _]+$)')
			])],
		});
		this.validationMessages = VALIDATION_MESSAGES;
	}

	ngOnInit(): void { }

	/**
	 * close the modal
	 */
	close() {
		this.activeModal.dismiss(false);
	}

	/**
	 * create folder
	 * @param formData 
	 * @response null
	 */
	create(formData: FormData) {
		if (this.createFolder.valid) {
			this.commonServices.loader.next(true);
			this.commonServices.trimArray(formData).then(data => {
				formData = data;
				this.companyServices.createFolder(formData["Name"], formData["ShortName"]).subscribe(data => {
					if (data.Status) {
						this.commonServices.showSuccess(API_MESSAGES.CreateFolder.successMessage, "");
						this.activeModal.close(true);
					}
					else {
						this.commonServices.showError(data.Message, "");
					}
					this.commonServices.loader.next(false);
				});
			});
		}
		else {
			this.createFolder.markAllAsTouched();
		}
	}

}
