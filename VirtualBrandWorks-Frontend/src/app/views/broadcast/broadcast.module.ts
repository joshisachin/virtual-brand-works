import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrandworksPlayService } from '../brandworks-play/brandworks-play.service';
import { BroadcastRoutingModule } from './broadcast-routing.module';
import { BroadcastComponent } from './broadcast.component';
import { BroadcastService } from './broadcast.service';

@NgModule({
    declarations: [BroadcastComponent],
    imports: [
        BroadcastRoutingModule, NgbNavModule, SharedModule,NgbModule,
        ReactiveFormsModule, CommonModule, FormsModule
    ],
    providers: [BroadcastService, BrandworksPlayService]
  })
  export class BroadcastModule { }