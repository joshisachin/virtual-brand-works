import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BroadcastService {

  constructor(private http: HttpClient) { }

  getBroadCastContent(name:string) : Observable<any> {
    return this.http.get(API_NAME.GetBroadcastedContent[0].name + "?search=" + name);
  }
}
