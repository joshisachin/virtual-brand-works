import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BroadcastContent } from 'src/app/shared/model/broadcastcontent';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { BrandworksPlayService } from '../brandworks-play/brandworks-play.service';
import { ViewContentComponent } from '../elements/view-content/view-content.component';
import { BroadcastService } from './broadcast.service';

@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss']
})
export class BroadcastComponent implements OnInit {
  
  toggleRightSide: boolean; // to expand right side when side menu shrinks
  broadcastContent: BroadcastContent[];
  searchText:string;

  constructor(private commonServices: CommonService, private broadcastService: BroadcastService, private router: Router, private modalService: NgbModal, private vbPlayServices: BrandworksPlayService) {
    this.commonServices.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  }
 
  ngOnInit(): void {
    this.getBroadcastContent(''); //Get all broadcast content from API
  }

  /**
   * get content from backend API
   * @param name This parameter is used for search
   * @response array
   */
  getBroadcastContent(name:string) {
    this.broadcastService.getBroadCastContent(name).subscribe(data => {
      if (data.Status) {
        this.formatUrl(data.Data);
        this.commonServices.loader.next(false);
      }
      else {
        this.broadcastContent = [];
        this.commonServices.showError(data.Message, "");
        this.commonServices.loader.next(false);
      }
    });
  }

   /**
   * format data by the url
   * @param broadcastContent
   * @response array
   */
  formatUrl(broadcastData: any) {
      broadcastData.map((data, i) => {
        this.commonServices.formatVideoThumbnailUrl(data);
        if (broadcastData.length -1 == i) {
          this.broadcastContent = broadcastData;
        }
      });
  }

  /**
   * content search
   * @param null
   * @response searched object
   */
  searchBroadcastContent() {
    this.getBroadcastContent(this.searchText);
  }

  /**
   * route to
   * @param route
   * @response null
   */
  routeTo(route: string) {
    this.router.navigate([route]);
  }

    /**
   * open video
   * @param index
   * @response null
   */
  openVideo(index: number) {
    const modalRef = this.modalService.open(ViewContentComponent, { centered: true });
    modalRef.componentInstance.data = this.broadcastContent[index];
    modalRef.componentInstance.isVideo = true;
    console.log(this.broadcastContent[index]);
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  /**
   * like
   * @param index
   * @response null
   */
  like(index: number) {
    this.vbPlayServices.like(this.broadcastContent[index].ContentId, this.broadcastContent[index].IsContestEntry, !this.broadcastContent[index].IsLikedByUser).subscribe(data => {
      if (data.Status) {
        this.broadcastContent[index].IsLikedByUser = !this.broadcastContent[index].IsLikedByUser;
        if (this.broadcastContent[index].IsLikedByUser) {
          this.broadcastContent[index].Likes++;
        }
        else {
          this.broadcastContent[index].Likes--;
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

}
