import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { NgbActiveModal, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { VALIDATION_MESSAGES } from 'src/app/shared/constants/validations/prize-validation';
import { PrizeStore } from 'src/app/shared/model/prize-store';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { PreviewComponent } from '../preview/preview.component';
import { PrizeStoreService } from '../prize-store.service';

@Component({
  selector: 'app-prize-update',
  templateUrl: './prize-update.component.html',
  styleUrls: ['./prize-update.component.scss']
})
export class PrizeUpdateComponent implements OnInit {
  @Input() data: PrizeStore;
  updatePrizeForm: FormGroup;
  minStartDate: NgbDateStruct; // minimum date for start date
  minEndDate: NgbDateStruct; // minimum date for end date
  validationMessages: any;
  currentDate: Date; // current date
  imageUrl: SafeUrl;
  imageName: string;
  constructor(private activeModal: NgbActiveModal, private prizeService: PrizeStoreService, private formBuilder: FormBuilder, private commonServices: CommonService, private sanitizer: DomSanitizer, private modalService: NgbModal) {
    this.updatePrizeForm = this.formBuilder.group({
      "Title": ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30),
        Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]+$)')
      ])],
      "Description": ['', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(500),
      ])],
      "Points": [0, Validators.compose([
        Validators.required,
        Validators.min(1),
      ])],
      "Remarks": [''],
      "ThumbnailImage": [""],
      "StartDate": ["", Validators.compose([
        Validators.required
      ])],
      "EndDate": ["", Validators.compose([
        Validators.required
      ])]
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void {
    this.imageUrl = this.data.ThumbnailImage;
    this.checkDateValidation();
    this.updatePrizeForm.patchValue({"Title": this.data.Title, "Description": this.data.Description, "Points": this.data.Points, "Remarks": this.data.Remarks, "StartDate": this.convertDate(new Date(this.data.StartDate)), "EndDate": this.convertDate(new Date(this.data.EndDate))});
    this.updatePrizeForm.controls["StartDate"].markAsUntouched();
    this.updatePrizeForm.controls["EndDate"].markAsUntouched();
  }

  /*
  * To convert date to ngbDateStruct for calender.
  */
  convertDate(date: Date): NgbDateStruct {
    return date ? {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    } : null;
  }

  /**
   * check date validation
   * @param null
   * @reponse date
   */
  checkDateValidation() {
    this.currentDate = new Date();
    this.minStartDate = this.convertDate(this.currentDate);
    this.minEndDate = this.convertDate(this.currentDate);
    if (this.currentDate.getTime() > new Date(this.data.StartDate).getTime()) {
      this.minStartDate = this.convertDate(new Date(this.data.StartDate));
    }
    if (this.currentDate.getTime() > new Date(this.data.EndDate).getTime()) {
      this.minEndDate = this.convertDate(new Date(this.data.EndDate));
    }
  }

  /**
   * close
   */
  close() {
    this.activeModal.dismiss();
  }

  /**
   * update
   * @param formData
   * @response null
   */
  update(formData: FormData) {
    if (this.updatePrizeForm.valid) {
        this.commonServices.trimArray(formData).then(data => {
          formData = data;
          formData["PrizeId"] = this.data.PrizeId;
          formData["StartDate"] = this.formatDate(formData["StartDate"]);
          formData["EndDate"] = this.formatDate(formData["EndDate"]);
          this.prizeService.updatePrize(formData).subscribe(data => {
            if (data.Status) {
              this.activeModal.close();
              this.commonServices.showSuccess(API_MESSAGES.Content.successMessage, "");
            }
            else {
              this.commonServices.showError(data.Message, "");
            }
          })
        });
    }
    else {
      this.updatePrizeForm.markAllAsTouched();
    }
  }

  /**
   * format date to regular
   * @param date
   * @return format date
   */
  formatDate(date: NgbDateStruct) {
    return date.year + "-" + date.month + "-" + date.day;
  }

  
  /**
   * upload
   * @param value
   * @params null
   */
  upload(value: any[]) {
    if (value && value.length == 1) {
      for (let i = 0; i < value.length; i++) {
        if (!value[i]["type"].match("image")) {
          this.commonServices.showError("Please insert image only", "");
          return;
        }
        this.imageName = value[i].name;
        this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(value[i]));
        let reader = new FileReader();
        reader.readAsDataURL(value[i]);
        reader.onload = () => {
          //me.modelvalue = reader.result;
          this.updatePrizeForm.patchValue({ "ThumbnailImage": reader.result });
          console.log(reader.result);
        };
      }
    }
    else {
      this.commonServices.showError(API_MESSAGES.FileUpload.fileCount, "");
    }
  }

  /**
   * preview
   * @param null
   * @response null
   */
  preview() {
    const modalRef = this.modalService.open(PreviewComponent, {
      windowClass: 'modalwidth'
    });
    modalRef.componentInstance.data = {"title": this.updatePrizeForm.controls["Title"].value, "points": this.updatePrizeForm.controls["Points"].value, "endDate": this.formatDate(this.updatePrizeForm.controls["EndDate"].value), "image": this.imageUrl};
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

}
