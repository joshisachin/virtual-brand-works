import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizeUpdateComponent } from './prize-update.component';

describe('PrizeUpdateComponent', () => {
  let component: PrizeUpdateComponent;
  let fixture: ComponentFixture<PrizeUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrizeUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
