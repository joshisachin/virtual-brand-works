import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrizeHomeComponent } from './prize-home/prize-home.component';
import { PrizeStoreComponent } from './prize-store.component';


const routes: Routes = [
	{
		path: '',
		component: PrizeStoreComponent,
		children: [
			{
				path: '',
				component: PrizeHomeComponent,
			},
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PrizeStoreRoutingModule { }
