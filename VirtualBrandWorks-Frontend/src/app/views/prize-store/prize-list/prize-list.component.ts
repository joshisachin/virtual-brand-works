import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { CommonPopupComponent } from 'src/app/shared/modals/common-popup/common-popup.component';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { PrizeStore } from 'src/app/shared/model/prize-store';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { CreatePrizeComponent } from '../create-prize/create-prize.component';
import { PrizeStoreService } from '../prize-store.service';
import { PrizeUpdateComponent } from '../prize-update/prize-update.component';

@Component({
  selector: 'app-prize-list',
  templateUrl: './prize-list.component.html',
  styleUrls: ['./prize-list.component.scss']
})
export class PrizeListComponent implements OnInit {
  listApi: string; // name of the api to hit according to the tab
  storeData: PrizeStore[];
  constructor(public prizeService: PrizeStoreService, private commonServices: CommonService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.listApi = (this.prizeService.currentNav == 1) ? "getPrizeStore" : "getMyCreatedPrize";
    this.getPrizeStore();
  }

  ngOnChanges() {
    this.listApi = (this.prizeService.currentNav == 1) ? "getPrizeStore" : "getMyCreatedPrize";
    this.getPrizeStore();
  }

  /**
   * get prize store
   * @param null
   * @response array
   */
  getPrizeStore(value: any = 1) {
    this.prizeService[this.listApi](value).subscribe(data => {
      if (data.Status) {
        this.storeData = data.Data
        if (this.prizeService.currentNav == 2) {
          this.storeData.map(res => {
            res.CanDelete = false;
            console.log(new Date().getTime(), " ", new Date(res.StartDate).getTime(), " ", new Date(res.EndDate).getTime(), res.IsActive, new Date(), res.StartDate);
            if (new Date().getTime() < new Date(res.StartDate).getTime() || new Date().getTime() > new Date(res.EndDate).getTime() || !res.IsActive) {
              res.CanDelete = true;
            }
          });
        }
        this.storeData.map(res => {
          if(!res?.ThumbnailImage.includes('amazonaws')){
            res.ThumbnailImage = '';
          }
        })
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * open create prize
   * @param null
   * @response null
   */
  openCreatePrize() {
    const modalRef = this.modalService.open(CreatePrizeComponent, {
      windowClass: 'modalwidth'
    });
    // modalRef.componentInstance.data = "";
    modalRef.result.then((result) => {
      this.getPrizeStore();
    }, (reason) => {
    });
  }

  /**
   * open update modal
   * @param index
   * @response null
   */
  openUpdateModal(index: number) {
    const modalRef = this.modalService.open(PrizeUpdateComponent, {
      windowClass: 'modalwidth'
    });
    console.log("store data", this.storeData[index]);
    modalRef.componentInstance.data = this.storeData[index];
    modalRef.result.then((result) => {
      this.getPrizeStore();
    }, (reason) => {
    });
  }

  /**
   * changeStatus
   * @param index
   * @response boolean
   */
  changeStatus(index) {
    this.prizeService.changeStatus(this.storeData[index].PrizeId, !this.storeData[index].IsActive).subscribe(data => {
      if (data.Status) {
        this.storeData[index].IsActive = !this.storeData[index].IsActive;
      }
      else {
        this.storeData[index].IsActive = this.storeData[index].IsActive;
        this.commonServices.showError(data.Message, "");
      }
    })
  }

  /**
   * sort
   * @param value
   * @response nukk
   */
  sort(value: any) {
    this.getPrizeStore(value);
  }

  /**
   * get coupon
   * @param index
   * @response null
   */
  getCoupon(index: number) {
    this.prizeService.getCoupon(this.storeData[index].PrizeId).subscribe(data => {
      if (data.Status) {
        this.storeData[index].AvailableCount--;
        this.commonServices.showSuccess(API_MESSAGES.Store.VoucherSend, "");
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * delete
   * @param index
   * @response null
   */
  delete(index: number) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, { centered: true });
    let data: any = { "title": "Are you sure you want to delete" }
    modalRef.componentInstance.data = data;
    modalRef.result.then((result) => {
      this.prizeService.delete(this.storeData[index].PrizeId).subscribe(data => {
        if (data.Status) {
          this.storeData.splice(index, 1);
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
      });
    });
  }

  /**
   * open description
   * @param index
   * @response null
   */
  openDescription(index: number) {
    const modalRef = this.modalService.open(CommonPopupComponent, { centered: true });
    let data: any = { "title": "Are you sure you want to delete" }
    modalRef.componentInstance.data = {
      title: "Description",
      subTitle: this.storeData[index].Title,
      message: this.storeData[index].Description,
      buttonTwo: "Close"
    };
    modalRef.result.then((result) => {
    });
  }

}
