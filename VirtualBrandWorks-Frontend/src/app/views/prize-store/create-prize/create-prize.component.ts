import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { NgbActiveModal, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { VALIDATION_MESSAGES } from 'src/app/shared/constants/validations/prize-validation';
import { PrizeCategory } from 'src/app/shared/model/prize-category';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { PreviewComponent } from '../preview/preview.component';
import { PrizeStoreService } from '../prize-store.service';

@Component({
  selector: 'app-create-prize',
  templateUrl: './create-prize.component.html',
  styleUrls: ['./create-prize.component.scss']
})
export class CreatePrizeComponent implements OnInit {
  prizeCategories: PrizeCategory[];
  timeOff: any;
  categoryType: number = 1;
  createPrizeForm: FormGroup;
  minStartDate: NgbDateStruct; // minimum date for start date
  minEndDate: NgbDateStruct; // minimum date for end date
  validationMessages: any;
  voucherArray: any = [];
  voucherCodes: any = [];
  currentDate: Date; // current date
  imageUrl: SafeUrl;
  imageName: string;
  constructor(private activeModal: NgbActiveModal, private prizeService: PrizeStoreService, private formBuilder: FormBuilder, private commonServices: CommonService, private sanitizer: DomSanitizer, private modalService: NgbModal) {
    this.currentDate = new Date();
    this.minStartDate = this.convertDate(this.currentDate);
    this.minEndDate = this.convertDate(this.currentDate);


    this.createPrizeForm = this.formBuilder.group({
      "PrizeCategoryId": [1, Validators.compose([
        Validators.required
      ])],
      "Title": ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30),
        Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]+$)')
      ])],
      "Description": ['', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(500),
      ])],
      "Points": [0, Validators.compose([
        Validators.required,
        Validators.min(1),
      ])],
      "Quantity": [0, Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.max(10)
      ])],
      "TimeOffDuration": [''],
      "PrizeAmount": [0],
      "BrandName": [''],
      "Remarks": [''],
      "ThumbnailImage": [""],
      "StartDate": ["", Validators.compose([
        Validators.required
      ])],
      "EndDate": ["", Validators.compose([
        Validators.required
      ])]
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void {
    this.prizeService.getPrizeCategories().subscribe(data => {      
      this.prizeCategories = data.Data.PrizeCategories;
      this.timeOff = data.Data.TimeOffOptions;
      this.createPrizeForm.patchValue({ "TimeOffDuration": this.timeOff[0] });
    });
  }

  /*
  * To convert date to ngbDateStruct for calender.
  */
  convertDate(date: Date): NgbDateStruct {
    return date ? {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    } : null;
  }

  /**
   * close
   */
  close() {
    this.activeModal.dismiss();
  }

  /**
   * change type
   * @param value
   * @response null
   */
  changeType(value: number) {
    this.categoryType = value;
  }

  /**
   * create
   * @param formData
   * @response null
   */
  create(formData: FormData) {
    if (this.createPrizeForm.valid) {
      formData["BrandName"].trim();
      if ((this.categoryType == 2 || this.categoryType == 3) && (this.voucherCodes.length == 0 || !formData["BrandName"])) {
        if (this.voucherCodes.length == 0) {
          this.commonServices.showError(API_MESSAGES.Store.VoucherError, "");
        }
        this.createPrizeForm.controls["BrandName"].setValidators([Validators.required, Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]+$)')]);
        this.createPrizeForm.controls["BrandName"].updateValueAndValidity();
        this.createPrizeForm.controls["BrandName"].markAsTouched();
        return;
      }
      else if (this.categoryType == 5 && !formData["PrizeAmount"]) {
        this.createPrizeForm.controls["PrizeAmount"].setValidators([Validators.required, Validators.min(1)]);
        this.createPrizeForm.controls["PrizeAmount"].updateValueAndValidity();
        this.createPrizeForm.controls["PrizeAmount"].markAsTouched();
        return;
      }
      else {
        this.commonServices.trimArray(formData).then(data => {
          formData = data;
          formData["StartDate"] = this.formatDate(formData["StartDate"]);
          formData["EndDate"] = this.formatDate(formData["EndDate"]);
          formData["VoucherCodes"] = (this.categoryType == 2 || this.categoryType == 3) ? this.voucherCodes : "";
          formData["BrandName"] = (this.categoryType == 2 || this.categoryType == 3) ? formData["BrandName"] : "";
          formData["TimeOffDuration"] = (this.categoryType == 1) ? formData["TimeOffDuration"] : "";
          formData["PrizeAmount"] = (this.categoryType == 5) ? formData["PrizeAmount"] : 0;

          this.prizeService.createPrize(formData).subscribe(data => {
            if (data.Status) {
              this.activeModal.close();
              this.commonServices.showSuccess(API_MESSAGES.Content.successMessage, "");
            }
            else {
              this.commonServices.showError(data.Message, "");
            }
          })
        });
      }
    }
    else {
      this.createPrizeForm.markAllAsTouched();
    }
  }

  /**
   * format date to regular
   * @param date
   * @return format date
   */
  formatDate(date: NgbDateStruct) {
    return date.year + "-" + date.month + "-" + date.day;
  }

  /**
   * create voucher code box
   * @param value
   * @response array
   */
  createVoucher(value: number) {
    this.voucherArray = [];
    let count = 0;
    if (value <= 10) {
      for (let i = 0; i < value; i++) {
        count = i + 1;
        this.voucherArray.push({ "id": count, "name": "voucher_" + count, "placeholder": "Enter voucher " + count, "value": "" });
      }
    }
  }

  /**
   * save vouchers
   * @param null
   * @response null
   */
  save() {
    this.voucherArray.map(data => {
      data.value.trim();
      if (data.value != "") {
        if (!this.voucherCodes.includes(data.value)) {
          this.voucherCodes.push(data.value);
          this.commonServices.showSuccess(API_MESSAGES.Store.VoucherSuccess, "");
        }
      }
      else {
        this.commonServices.showError(API_MESSAGES.Store.VoucherCount, "");
      }
    });
  }

  /**
   * insert value voucher
   * @param index
   * @response null
   */
  insertValue(index: number, value: string) {
    this.voucherArray[index].value = value;
  }

  /**
   * upload
   * @param value
   * @params null
   */
  upload(value: any[]) {
    if (value && value.length == 1) {
      for (let i = 0; i < value.length; i++) {
        if (!value[i]["type"].match("image")) {
          this.commonServices.showError("Please insert image only", "");
          return;
        }
        this.imageName = value[i].name;
        this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(value[i]));
        let reader = new FileReader();
        reader.readAsDataURL(value[i]);
        reader.onload = () => {
          //me.modelvalue = reader.result;
          this.createPrizeForm.patchValue({ "ThumbnailImage": reader.result });
        };
      }
    }
    else {
      this.commonServices.showError(API_MESSAGES.FileUpload.fileCount, "");
    }
  }

  /**
   * preview
   * @param null
   * @response null
   */
  preview() {
    const modalRef = this.modalService.open(PreviewComponent, {
      windowClass: 'modalwidth'
    });
    modalRef.componentInstance.data = { "title": this.createPrizeForm.controls["Title"].value, "points": this.createPrizeForm.controls["Points"].value, "endDate": this.formatDate(this.createPrizeForm.controls["EndDate"].value), "image": this.imageUrl };
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

}
