import { Component, OnInit } from '@angular/core';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { PrizeStoreService } from '../prize-store.service';

@Component({
  selector: 'app-prize-home',
  templateUrl: './prize-home.component.html',
  styleUrls: ['./prize-home.component.scss']
})
export class PrizeHomeComponent implements OnInit {

  constructor(private prizeService: PrizeStoreService) { }
  userRole: string = localStorage.getItem("userRole");
  ngOnInit(): void {
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {
    console.log("nav", changeEvent.nextId);
    this.prizeService.currentNav = changeEvent.nextId;
  }

}
