import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizeHomeComponent } from './prize-home.component';

describe('PrizeHomeComponent', () => {
  let component: PrizeHomeComponent;
  let fixture: ComponentFixture<PrizeHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrizeHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
