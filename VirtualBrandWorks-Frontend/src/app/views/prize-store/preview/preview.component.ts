import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {
  @Input() data: any;
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  /**
   * close
   */
  close() {
    this.activeModal.dismiss();
  }

}
