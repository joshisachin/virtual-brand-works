import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizeStoreComponent } from './prize-store.component';

describe('PrizeStoreComponent', () => {
  let component: PrizeStoreComponent;
  let fixture: ComponentFixture<PrizeStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrizeStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
