import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrizeHomeComponent } from './prize-home/prize-home.component';
import { PrizeStoreComponent } from './prize-store.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrizeStoreRoutingModule } from './prize-store-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreatePrizeComponent } from './create-prize/create-prize.component';
import { PrizeStoreService } from './prize-store.service';
import { ReactiveFormsModule } from '@angular/forms';
import { PreviewComponent } from './preview/preview.component';
import { PrizeListComponent } from './prize-list/prize-list.component';
import { PrizeUpdateComponent } from './prize-update/prize-update.component';



@NgModule({
  declarations: [PrizeHomeComponent, PrizeStoreComponent, CreatePrizeComponent, PreviewComponent, PrizeListComponent, PrizeUpdateComponent],
  imports: [
    CommonModule,
    SharedModule,
    PrizeStoreRoutingModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [PrizeStoreService]
})
export class PrizeStoreModule { }
