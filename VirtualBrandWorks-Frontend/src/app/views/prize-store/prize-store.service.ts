import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';

@Injectable()
export class PrizeStoreService {
  currentNav: number = 1;
  constructor(private http: HttpClient) { }

  /**
   * get prize categories
   */
  getPrizeCategories(): Observable<any> {
    return this.http.get(API_NAME.GetPrizeCategories[0].name);
  }

  /**
   * create prize
   */
  createPrize(formData: any): Observable<any> {
    return this.http.post(API_NAME.CreatePrize[0].name, formData);
  }

  /**
   * update prize
   */
  updatePrize(formData: any): Observable<any> {
    return this.http.post(API_NAME.UpdatePrize[0].name, formData);
  }

  /**
   * get prize store
  */
  getPrizeStore(value: any): Observable<any> {
    return this.http.get(API_NAME.GetPrizeStore[0].name + "?sortBy=" + value);
  }

  /**
   * changeStatus
  */
  changeStatus(prizeId: number, status: boolean): Observable<any> {
    return this.http.post(API_NAME.UpdatePrizeStatus[0].name, { "PrizeId": prizeId, "IsActive": status });
  }

  /**
   * get my create prize store
  */
  getMyCreatedPrize(value: any): Observable<any> {
    console.log("dataa");
    return this.http.get(API_NAME.GetMyCreatedPrize[0].name + "?sortBy=" + value);
  }

  /**
   * get coupon
  */
  getCoupon(prizeId: number): Observable<any> {
    return this.http.post(API_NAME.AvailPrize[0].name, { "PrizeId": prizeId });
  }

  /**
   * delete
  */
  delete(prizeId: number): Observable<any> {
    return this.http.delete(API_NAME.DeletePrize[0].name + "?prizeId=" + prizeId);
  }
}
