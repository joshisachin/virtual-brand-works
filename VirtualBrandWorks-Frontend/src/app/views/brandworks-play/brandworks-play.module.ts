import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayComponent } from './play/play.component';
import { BrandworksPlayRoutingModule } from './brandworks-play-routing.module';
import { BrandworksPlayComponent } from './brandworks-play.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { StartGameComponent } from './start-game/start-game.component';
import { BrandworksPlayService } from './brandworks-play.service';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { PlayListComponent } from './play-list/play-list.component';
import { GameDetailsComponent } from './game-details/game-details.component';
import { VideojsRecordComponent } from 'src/app/shared/modals/videojs-record/videojs-record.component';


@NgModule({
  declarations: [
    BrandworksPlayComponent, 
    PlayComponent, 
    StartGameComponent, 
    ConfirmationComponent, 
    PlayListComponent, 
    PlayListComponent, 
    GameDetailsComponent, 
    GameDetailsComponent,
    VideojsRecordComponent
  ],
  imports: [
    CommonModule,
    BrandworksPlayRoutingModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [BrandworksPlayService]
})
export class BrandworksPlayModule { }
