import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { Game } from 'src/app/shared/model/Game';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { environment } from 'src/environments/environment';
import { BrandworksPlayService } from '../brandworks-play.service';
import { StartGameComponent } from '../start-game/start-game.component';
import { ViewContentComponent } from '../../elements/view-content/view-content.component';
import { CommonPopupComponent } from 'src/app/shared/modals/common-popup/common-popup.component';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';

@Component({
  selector: 'app-play-list',
  templateUrl: './play-list.component.html',
  styleUrls: ['./play-list.component.scss']
})
export class PlayListComponent implements OnInit {
  gameData: Game[];
  environment: any = environment;
  constructor(private modalService: NgbModal, private vbPlayServices: BrandworksPlayService, private commonServices: CommonService, private router: Router) { }
  
  ngOnInit(): void {
    this.getGamesList();
    window.scroll(0,0);
  }

  /**
   * get games list
   * @param null
   * @response object
   */
  getGamesList() {
    this.vbPlayServices.getAllGames(this.vbPlayServices.currentNav).subscribe(data => {
      if (data.Status) { 
        this.formatUrl(data.Data);
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    })
  }

  /**
   * start game popup
   * @param null
   * @response null
   */
  startGame(type: string="create", contestId: number = 0, index:number=0) { 
    const modalRef = this.modalService.open(StartGameComponent, { windowClass: 'w925', centered: true });
    modalRef.componentInstance.type = type;
    modalRef.componentInstance.contestId = contestId;
    if (contestId) {
      modalRef.componentInstance.contestName = this.gameData[index].Name;
    }
    if(type=="update"){
      modalRef.componentInstance.contestEntry = this.gameData[index];
    }
    modalRef.result.then((result) => {
      this.getGamesList();
    }, (reason) => {
      this.getGamesList();
    });
  }

  /**
   * route to
   * @param route
   * @response null
   */
  routeTo(route: string) {
    this.router.navigate([route]);
  }

  /**
   * format data by the url
   * @param gameData
   * @response array
   */
  formatUrl(gameData: any) {
    gameData.map((data, i) => {
      data.CanDelete = true;
      if (new Date(data.EndDate).getTime() < new Date().getTime()) {
        data.CanDelete = false;
      }
      this.commonServices.formatVideoThumbnailUrl(data);
      if(data.ChallengeEntry){
      this.commonServices.formatVideoThumbnailUrl(data.ChallengeEntry);
      }
      if (gameData.length -1 == i) {
        this.gameData = gameData;
      }

      data.CanJoin = false;
      if (new Date() >= new Date(data.StartDate) && new Date() <= new Date(data.EndDate)) {
        data.CanJoin = true;
      }
    });
  }

  /**
   * like
   * @param index
   * @response null
   */
  like(index: number) {
    this.vbPlayServices.like(this.gameData[index].ContestId, false, !this.gameData[index].IsLikedByUser).subscribe(data => {
      if (data.Status) {
        this.gameData[index].IsLikedByUser = !this.gameData[index].IsLikedByUser;
        if (this.gameData[index].IsLikedByUser) {
          this.gameData[index].Likes++;
        }
        else {
          this.gameData[index].Likes--;
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * delete
   * @param index
   */
  delete(index: number) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, { centered: true });
    let data: any = {"title": "Are you sure you want to delete this topic completely"}
    modalRef.componentInstance.data = data;
    modalRef.result.then((result) => {
      this.vbPlayServices.deleteContest(this.gameData[index].ContestId).subscribe(data => {
        if (data.Status) {
          this.gameData.splice(index, 1);
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
      });
    }, (reason) => {
    });
  }

  /**
   * open video
   * @param index
   * @response null
   */
  openVideo(index: number,game) {
    let isVideo = false;
    if(game.MimeType?.split('/')[0] == 'video' || game.IsYoutubeContent || game.IsFacebookContent){
      isVideo = true;
    }
    this.gameData[index].ContentId = this.gameData[index].ContestId;
    const modalRef = this.modalService.open(ViewContentComponent, { centered: true });
    modalRef.componentInstance.data = this.gameData[index];
    modalRef.componentInstance.isVideo = isVideo;
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  /**
   * broadcast
   * @param index
   * @response null
   */
  broadcast(index: number) {
    this.vbPlayServices.broadcastGame(this.gameData[index].ContestId, true).subscribe(data => {
      if (data.Status) {
        this.gameData[index].IsBroadCasted = true;
        this.commonServices.showSuccess(API_MESSAGES.Contest.IsBroadcast, "");
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    })
  }

  /**
   * submit entry
   * @param contestEntryId 
   * @param IsSubmitted 
   * @response submit contest entry
   */
  submitContest(contestEntryId, IsSubmitted ) {
    this.vbPlayServices.submitContestEntry(contestEntryId, IsSubmitted).subscribe(data => {
      if (data.Status) {
        this.getGamesList();
      }
    })
  }

  /**
   * open rules
   * @param index
   * @response null
   */
  openRules(index: number) {
    const modalRef = this.modalService.open(CommonPopupComponent, {
      windowClass: 'modalwidth'
    });
    modalRef.componentInstance.data = {
      "title": "Rules",
      "subTitle": "",
      "message": this.gameData[index].Rules,
      "buttonOne": "",
      "buttonTwo": ""
    };
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  /**
   * change status
   * @param index
   * @param value
   * @response null
   */
  changeStatus(index: number, value: string) {
    console.log(this.gameData);
    this.vbPlayServices.changeStatus(this.gameData[index].ContestId, value).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Status.Success, "");
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  reportVideo(index){
    const modalRef = this.modalService.open(ConfirmationAlertComponent, { centered: true });
    let data: any = {"title": "Are you sure you want to report this topic"}
    modalRef.componentInstance.data = data;
    modalRef.result.then((result) => {
      this.vbPlayServices.reportVideo(this.gameData[index].ContestId).subscribe(data => {
        this.commonServices.showSuccess(API_MESSAGES.ReportStatus.Report,'');
      });
    }, (reason) => {
    });
  }
}
