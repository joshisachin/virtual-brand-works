import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { CommonPopupComponent } from 'src/app/shared/modals/common-popup/common-popup.component';
import { Game } from 'src/app/shared/model/Game';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { ViewContentComponent } from '../../elements/view-content/view-content.component';
import { BrandworksPlayService } from '../brandworks-play.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.scss']
})
export class GameDetailsComponent implements OnInit {

  gameId: number = 0;
  gameData: Game;
  gameEntries: Game[];
  roleID: number;
  @ViewChild("searchText", { static: true }) searchText: ElementRef;
  constructor(private route: ActivatedRoute, private vbPlayServices: BrandworksPlayService, private commonServices: CommonService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.roleID = parseInt(localStorage.getItem("roleId"));
    this.route.params.subscribe(params => {
      if (params.id) {
        this.gameId = params.id;
        this.commonServices.searchDebounce(this.searchText.nativeElement).subscribe(searchString => {
          this.getGameDetails(searchString);
        });
        this.getGameDetails();
      }
    });
  }

  /**
   * get game details
   * @param null
   * @response array
   */
  getGameDetails(searchString: string = "") {
    this.vbPlayServices.getGameDetails(this.gameId, searchString).subscribe(data => {
      if (data.Status) {
        this.gameData = data.Data.ContestChallenge;
        this.gameEntries = data.Data.ContestEntries;
        this.commonServices.formatVideoThumbnailUrl(this.gameData);
        if (this.gameEntries.length > 0) {
          this.gameEntries.map((data) => {
            this.commonServices.formatVideoThumbnailUrl(data);
          });
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    })
  }

  /**
   * like
   * @param index
   * @response null
   */
  like(index: number = 0, isEntry: boolean = false) {
    let contestId: number = this.gameData.ContestId;
    let isLikedByUser: boolean = this.gameData.IsLikedByUser;
    if (isEntry) {
      contestId = this.gameEntries[index].ContestEntryId;
      isLikedByUser = this.gameEntries[index].IsLikedByUser;
    }
    this.vbPlayServices.like(contestId, isEntry, !isLikedByUser).subscribe(data => {
      if (data.Status) {
        if (isEntry) {
          this.gameEntries[index].IsLikedByUser = !this.gameEntries[index].IsLikedByUser;
          if (this.gameEntries[index].IsLikedByUser) {
            this.gameEntries[index].Likes++;
          }
          else {
            this.gameEntries[index].Likes--;
          }
        }
        else {
          this.gameData.IsLikedByUser = !this.gameData.IsLikedByUser;
          if (this.gameData.IsLikedByUser) {
            this.gameData.Likes++;
          }
          else {
            this.gameData.Likes--;
          }
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * open video
   * @param index
   * @response null
   */
  openVideo(game,index: number = 0) {
    let isVideo = false;
    if(game.MimeType?.split('/')[0] == 'video' || game.IsYoutubeContent || game.IsFacebookContent){
      isVideo = true;
    }
    let data: any = index == 0 ? this.gameData : this.gameEntries[index - 1];
    data.ContentId = index == 0 ? data.ContestId : data.ContestEntryId;
    const modalRef = this.modalService.open(ViewContentComponent, { centered: true });
    modalRef.componentInstance.data = data;
    modalRef.componentInstance.isVideo = isVideo;
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  /**
   * broadcast
   * @param index
   * @response null
   */
  broadcast(index: number) {
    let gameData: any = index ? this.gameEntries[index - 1] : this.gameData;
    this.vbPlayServices.broadcastGame(index ? gameData.ContestEntryId : gameData.ContestId, true, index ? true : false).subscribe(data => {
      if (data.Status) {
        if (index) {
          this.gameEntries[index - 1].IsBroadCasted = true;
        }
        else {
          this.gameData.IsBroadCasted = true;
        }
        this.commonServices.showSuccess(API_MESSAGES.Contest.IsBroadcast, "");
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    })
  }

  /**
   * open rules
   * @param null
   * @response null
   */
  openRules() {
    const modalRef = this.modalService.open(CommonPopupComponent, {
      windowClass: 'modalwidth'
    });
    modalRef.componentInstance.data = {
      "title": "Rules",
      "subTitle": "",
      "message": this.gameData.Rules,
      "buttonOne": "",
      "buttonTwo": ""
    };
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  /**
   * change status
   * @param index
   * @param value
   * @response null
   */
  changeStatus(index: number, value: string) {
    this.vbPlayServices.changeStatus(this.gameEntries[index].ContestEntryId, value, true).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Status.Success, "");
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

}
