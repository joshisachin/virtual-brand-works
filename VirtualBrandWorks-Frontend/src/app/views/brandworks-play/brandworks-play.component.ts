import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-brandworks-play',
  templateUrl: './brandworks-play.component.html',
  styleUrls: ['./brandworks-play.component.scss']
})
export class BrandworksPlayComponent implements OnInit {

  toggleRightSide: boolean; // to expand right side when side menu shrinks

  constructor(private commonServices: CommonService) {
    this.commonServices.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  }

  ngOnInit(): void {
  }

}
