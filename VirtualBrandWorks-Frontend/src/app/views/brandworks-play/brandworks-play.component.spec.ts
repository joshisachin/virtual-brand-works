import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandworksPlayComponent } from './brandworks-play.component';

describe('BrandworksPlayComponent', () => {
  let component: BrandworksPlayComponent;
  let fixture: ComponentFixture<BrandworksPlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandworksPlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandworksPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
