import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { NgbActiveModal, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { VALIDATION_MESSAGES } from '../../../shared/constants/validations/content-validation';
import { FileHandle } from 'src/app/shared/directive/upload.directive';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { S3bucketService } from 'src/app/shared/services/s3bucket/s3bucket.service';
import { BrandworksPlayService } from '../brandworks-play.service';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { Game } from 'src/app/shared/model/Game';
import { SOCIAL_URL } from 'src/app/shared/constants/video/url';
import { VideojsRecordComponent } from 'src/app/shared/modals/videojs-record/videojs-record.component';


@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.scss']
})
export class StartGameComponent implements OnInit {

  startGameForm: FormGroup;
  validationMessages: any = [];
  files: FileHandle[] = []; // file array
  selectedTags: any = []; // selected tags array
  selectedTags_1: any = []; // selected tags array
  tagsArr: [];
  tagsArr_1: [];
  minStartDate: NgbDateStruct; // minimum date for start date
  minEndDate: NgbDateStruct; // minimum date for end date
  currentDate: Date; // current date
  @Input() type: string = "create";
  @Input() contestId: number = 0;
  @Input() contestName: string = "";
  @Input() contestEntry:Game;
  contentId: number = 0;
  apiName: string;
  duration: number = 0;
  socialUrl: SafeUrl = "";
  videoResolution: any = {Width: 0, Height: 0};
  isCamVideo: boolean = false;
  constructor(private activeModal: NgbActiveModal, private formBuilder: FormBuilder, private commonServices: CommonService, private sanitizer: DomSanitizer, private awsService: S3bucketService, private vbPlayService: BrandworksPlayService, private modalService: NgbModal) {
    this.currentDate = new Date();
    this.minStartDate = this.convertDate(this.currentDate);
    this.minEndDate = this.convertDate(this.currentDate);

    this.startGameForm = this.formBuilder.group({
      Name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30),
        Validators.pattern('^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]+$)')
      ])],
      Description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(500),
        Validators.minLength(4)
      ])],
      ContentUrl: [""],
      StartDate: [""],
      EndDate: [""],
      Rules: [""],
      YoutubeUrl: ["", Validators.compose([
        Validators.pattern("((http|https)://)(www.)?(youtube|yt|youtu)[a-zA-Z0-9@:%._\\+~#?&//=]{0,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)")
      ])],
      FacebookUrl: ["", Validators.compose([
        Validators.pattern("((http|https)://)(www.)?(facebook)[a-zA-Z0-9@:%._\\+~#?&//=]{0,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)")
      ])],
      IsAwsContent: [false],
      IsYoutubeContent: [false],
      IsFacebookContent: [false],
    });
    this.validationMessages = VALIDATION_MESSAGES;
  }

  ngOnInit(): void {
    this.commonServices.selectedTags.next([]);
    this.apiName = "joinGame";
    if (this.type == "create") {
      this.apiName = "createGame";
      this.startGameForm.controls["StartDate"].setValidators([Validators.required]);
      this.startGameForm.controls["EndDate"].setValidators([Validators.required]);
      this.startGameForm.controls["Rules"].setValidators([Validators.required, Validators.maxLength(500)]);
      this.startGameForm.controls["StartDate"].updateValueAndValidity();
      this.startGameForm.controls["EndDate"].updateValueAndValidity();
      this.startGameForm.controls["Rules"].updateValueAndValidity();
    }
    else if(this.type == "update") {
      this.apiName = "updateContestEntry";
      
      this.formatUrl(this.contestEntry); 

      this.startGameForm.controls["Name"].setValue(this.contestEntry.ChallengeEntry.Name);

      this.startGameForm.controls["Description"].setValue(this.contestEntry.ChallengeEntry.Description);
 
    }
  }

  ngOnDestroy() {
    this.contestName = "";
  }

  formatUrl(gameData:any) {
    this.commonServices.formatVideoUrl(gameData?.ChallengeEntry);
  }

  /**
   * close
   * @param null
   * @response null
   */
  close() {
    this.activeModal.close();
  }

  /**
   * get video duration
   * @param e 
   */
  getDuration(e: any) {
    if(e.target.duration != Infinity){
      this.duration = e.target.duration;
      console.log(this.duration)
    }
    this.videoResolution.Width = e.target.videoWidth;
    this.videoResolution.Height = e.target.videoHeight;
  }
  /**
   * To convert date to ngbDateStruct for calender.
   */
  convertDate(date: Date): NgbDateStruct {
    return date ? {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    } : null;
  }

  /**
   * upload
   * @param value
   * @params null
   */
  upload(value: any[]) { 
    this.files = [];
    this.socialUrl = "";
    this.duration = 0;
    if (value && value.length == 1) {
      for (let i = 0; i < value.length; i++) {
        const file = value[i];
        const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(file));
        this.files.push({ file, url });
      }
    }
    else {
      this.commonServices.showError(API_MESSAGES.FileUpload.fileCount, "");
    }
  }

  openVideoModal() {
    const modalRef = this.modalService.open(VideojsRecordComponent, {});
		modalRef.result.then((result) => {
      console.log(result);
			const file = result;
      const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(new Blob([file], {type: 'video/mp4'})));
      this.files.push({ file, url });
		}, (reason) => { });
  }

  /**
   * get selected tags
   * @param tags
   * @response object
   */
  getSelectedTags(tags: any) {
    this.selectedTags = [];
    this.tagsArr = [];
    if (tags.length > 0) {
      this.tagsArr = tags;
      this.commonServices.selectedTags.next(this.tagsArr);
      tags.map(data => {
        this.selectedTags.push(data.TagId);
      });
    }
  }

  getSelectedTags_1(tags_1: any) {
    this.selectedTags_1 = [];
    this.tagsArr_1 = [];
    if (tags_1.length > 0) {
      this.tagsArr_1 = tags_1;
      this.commonServices.selectedTags_1.next(this.tagsArr_1);
      tags_1.map(data => {
        this.selectedTags_1.push(data.TagId);
      });
    }
  }

  /**
   * manage drag drop file
   * @param files
   * @response object
   */
  fileBrowserHandler(files: FileHandle[]): void {
    this.files = [];
    if (files.length !== 1) {
      this.commonServices.showError(API_MESSAGES.FileUpload.fileCount, "");
    }
    else {
      this.files = files;
    }
  }

  /**
   * create
   * @param formData
   * @response null
   */
  create(formData: FormData) {
    if (this.startGameForm.invalid) {
      this.startGameForm.markAllAsTouched();
    }
    else if (this.files.length == 0 && formData["YoutubeUrl"] == "" && formData["FacebookUrl"] == "" && this.type !="update") {
      this.commonServices.showError(API_MESSAGES.Content.oneAllowed, "");
    }
    else if ((this.files.length > 0 && this.files[0].file.size && formData["YoutubeUrl"] != "") || (formData["YoutubeUrl"] != "" && formData["FacebookUrl"] != "") || (this.files.length > 0 && this.files[0].file.size && formData["FacebookUrl"] != "")) {
      this.commonServices.showError(API_MESSAGES.Content.oneAllowed, "");
    }
    else if (new Date(this.formatDate(formData["StartDate"])).getTime() > new Date(this.formatDate(formData["EndDate"])).getTime() && this.type == "create") {
      this.commonServices.showError(API_MESSAGES.Content.dateError, "")
    }
    else if (this.duration > 300) {
      this.commonServices.showError(API_MESSAGES.Content.videoDuration, "");
    }
    else {
      this.commonServices.loader.next(true);
      this.commonServices.trimArray(formData).then(data => {
        formData = data;
        if (this.files.length > 0 && this.files[0].file.size) {
          this.performUpload(formData);
        }
        else {
          let youtubeUrl: string;
          if (formData["YoutubeUrl"]) {
            youtubeUrl = formData["YoutubeUrl"].substr(formData["YoutubeUrl"].indexOf("v=") + 2, 11);
            formData["IsYoutubeContent"] = true;
          }
          else {
            formData["IsFacebookContent"] = true;
          }
          formData["ContentUrl"] = formData["YoutubeUrl"] != "" ? youtubeUrl : formData["FacebookUrl"];
          this.createContent(formData);
        }
      });
    }
  }

  /**
   * perform upload
   * @param formData
   */
  performUpload(formData: any) {
    let fileType = this.files[0].file["name"];
    fileType =  fileType.substr(fileType.lastIndexOf(".")+1); 
    this.awsService.performUpload(this.files[0].file, ["mp4", "mov","webm","mkv"]).then(data => {
      formData["ContentUrl"] = data;
      formData["MimeType"] = "video/" + fileType,
      formData["IsAwsContent"] = true,
      formData["Resolution"] = this.videoResolution;

        this.createContent(formData);
    })
      .catch(err => {
        this.commonServices.showError(API_MESSAGES.FileUpload.errorMessage, "");
        this.commonServices.loader.next(false);
      });
  }

  /**
   * create content
   * @param formData
   * @response null
   */
  createContent(formData: any) { 
    let tags = this.selectedTags.concat(this.selectedTags_1)
    formData["TagIds"] = tags; // add tags
    formData["StartDate"] = this.formatDate(formData["StartDate"]);
    formData["EndDate"] = this.formatDate(formData["EndDate"]);
    delete formData["YoutubeUrl"]; // delete the object that are not required
    delete formData["FacebookUrl"];
    if (this.type != "create") { // if the game is not created then not send those params
      delete formData["Rules"];
      delete formData["StartDate"];
      delete formData["EndDate"];
      delete formData["TagIds"];
      formData["ContestId"] = this.contestId;
    }
    this.commonServices.loader.next(true);
    this.vbPlayService[this.apiName](formData).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Content.successMessage, "");
        if (this.type == "create") {
          this.activeModal.close();
          this.openConfirmationModal(data.Data);
        }
        else {
          this.contentId = data.Data;
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * format date to regular
   * @param date
   * @return format date
   */
  formatDate(date: NgbDateStruct) {
    return date.year + "-" + date.month + "-" + date.day + " " + new Date().getHours() + ":" + new Date().getMinutes();
  }

  /**
   * reset file and url
   * @param null
   * @response null
   */
  reset() {
    this.startGameForm.patchValue({ "YoutubeUrl": "", "FacebookUrl": "" });
    this.files = [];
    this.socialUrl = "";
    this.duration = 0;
  }

  /**
   * open confirmation modal
   * @param data
   * @response null
   */
  openConfirmationModal(data: any, type: string = "create") {
    const modalRef = this.modalService.open(ConfirmationComponent, { centered: true });
    modalRef.componentInstance.data = data;
    modalRef.componentInstance.type = type;
    modalRef.result.then((result) => {
    }, (reason) => { });
  }

  /**
   * submit the join game
   * @param null
   * @response null
   */
  submitJoinGame() {
    this.vbPlayService.submitContestEntry(this.contentId, true).subscribe(data => {
      if (data.Status) {
        this.openConfirmationModal(data.Data, "not-create");
        this.activeModal.close();
      }
    })
  }

  /**
   * preview
   * @param value
   * @param type
   * @response null
   */
  preview(value: string, type: string) {
    if (type == 'youtube' && this.startGameForm.controls["YoutubeUrl"].valid) {
      value = value.substr(value.indexOf("v=") + 2, value.indexOf("v=") + 11)
      this.socialUrl = this.sanitizer.bypassSecurityTrustResourceUrl(SOCIAL_URL.Youtube + value); 
    }
    else if (type == 'facebook' && this.startGameForm.controls["FacebookUrl"].valid) {
      this.socialUrl = this.sanitizer.bypassSecurityTrustResourceUrl(SOCIAL_URL.Facebook + value);
    }
  }

}
