import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandworksPlayComponent } from './brandworks-play.component';
import { GameDetailsComponent } from './game-details/game-details.component';
import { PlayComponent } from './play/play.component';


const routes: Routes = [
	{
		path: '',
		component: BrandworksPlayComponent,
		children: [
			{
				path: '',
				component: PlayComponent,
			},
			{
				path: 'game-details/:id',
				component: GameDetailsComponent,
			},
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class BrandworksPlayRoutingModule { }
