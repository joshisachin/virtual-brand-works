import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';

@Injectable({
  providedIn: 'root'
})
export class BrandworksPlayService {
  currentNav: number = 1;
  constructor(private http: HttpClient) {
    this.currentNav = 1;
  }

  /**
   * create game
   */
  createGame(data: any): Observable<any> {
    return this.http.post(API_NAME.CreateContest[0].name, data);
  }

  /**
   * broadcast game
   */
  broadcastGame(contentId: number, isBroadcasted: boolean, isContestEntry:boolean=false): Observable<any> {
    return this.http.post(API_NAME.BroadcastContest[0].name, {"ContestId": contentId, "IsBroadcasted": isBroadcasted, "IsContestEntry": isContestEntry});
  }

  /**
   * get all games
   */
  getAllGames(contestType: number): Observable<any> {
    return this.http.get(API_NAME.GetAllVBPlayContest[0].name + "?contestType=" + contestType);
  }

  /**
   * join game
   */
  joinGame(data: any): Observable<any> {
    return this.http.post(API_NAME.JoinContest[0].name, data);
  }

  /**
   * like
   */
  like(id: number, isContestEntry: boolean, isLikedByUser: boolean): Observable<any> {
    return this.http.post(API_NAME.LikeContest[0].name, {"ContestContentId": id, "IsContestEntry": isContestEntry, "IsLikedByUser": isLikedByUser});
  }

  /**
   * view count
   */
  viewCount(contentId: number, contentType: number, isVideo: boolean): Observable<any> {
    return this.http.post(API_NAME.ContestContentViews[0].name, {"ContentId": contentId, "ContentType": contentType, "UserId": localStorage.getItem("userId") ? localStorage.getItem("userId") : 0, "IsVideo": isVideo});
  }

  /**
   * submit contest entry
   */
  submitContestEntry(contestEntryId: number, isSubmitted: boolean): Observable<any> {
    return this.http.post(API_NAME.SubmitContestEntry[0].name, {"ContestEntryId": contestEntryId, "IsSubmitted": isSubmitted});
  }

  /**
   * get game details
   */
  getGameDetails(contestId: number, searchText:string=""): Observable<any> {
    return this.http.get(API_NAME.ViewContest[0].name + "?contestId=" + contestId + "&isCompleted=false&search=" + searchText);
  }

  /**
   * update contest entry
   */
  updateContestEntry(data:any): Observable<any> {
    return this.http.post(API_NAME.UpdateContestEntry[0].name,data);
  }

  /**
   * delete contest
   */
  deleteContest(contestId: number): Observable<any> {
    return this.http.post(API_NAME.DeleteContest[0].name,{"ContestId": contestId});
  }

  /**
   * change status
   */
  changeStatus(contestId: number, value: string, isContestEntry: boolean = false): Observable<any> {
    return this.http.post(API_NAME.UpdateVbPlayContentVisibility[0].name, {"ContestId": contestId, "Visibility": value, "IsContestEntry": isContestEntry});
  }

  reportVideo(contestId: number){
    return this.http.post(API_NAME.ReportedVideo[0].report + '?contestId=' + contestId,'');
  }

}
