import { Component, OnInit } from '@angular/core';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { BrandworksPlayService } from '../brandworks-play.service';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  constructor(private vbPlayService: BrandworksPlayService) { }

  ngOnInit(): void {
    this.vbPlayService.currentNav = 1;
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {  
    this.vbPlayService.currentNav = changeEvent.nextId;
  }
}
