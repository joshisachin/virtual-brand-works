import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { ConfirmContent } from 'src/app/shared/model/confirm-content';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { BrandworksPlayService } from '../brandworks-play.service';
import { environment } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  @Input() data: ConfirmContent;
  @Input() type: string = "create";
  environment: any = environment;
  constructor(private activeModal: NgbActiveModal, private vbPlayServices: BrandworksPlayService, private commonService: CommonService) { }

  ngOnInit(): void {
    this.commonService.formatVideoUrl(this.data);
  }

  /**
   * close
   * @param null
   * @response null
   */
  close() {
    this.activeModal.close();
  }

  /**
   * broadcast
   * @param null
   * @response null
   */
  broadcast() {
    this.vbPlayServices.broadcastGame(this.data.ContestId, true, this.data.IsContestEntry).subscribe(data => {
      if (data.Status) {
        this.commonService.showSuccess(API_MESSAGES.Contest.IsBroadcast, "");
        this.activeModal.close();
      }
      else {
        this.commonService.showSuccess(data.Message, "");
      }
    })
  }
}
