import { Component, NgModuleRef, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { Game } from 'src/app/shared/model/Game';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { environment } from 'src/environments/environment';
import { BrandworksPlayService } from '../../brandworks-play/brandworks-play.service';
import { ViewContentComponent } from '../../elements/view-content/view-content.component';
import { ReportedVideosService } from '../reported-videos.service';

@Component({
  selector: 'app-reported-video',
  templateUrl: './reported-video.component.html',
  styleUrls: ['./reported-video.component.scss']
})
export class ReportedVideoComponent implements OnInit {

  gameData: any = [];
  environment: any = environment;
  constructor(private reportedVideos: ReportedVideosService,private vbPlayServices: BrandworksPlayService, private commonServices: CommonService, private modalService: NgbModal) { }
  
  ngOnInit(): void {
    this.getReportedVideos();
  }

  /**
   * format data by the url
   * @param gameData
   * @response array
   */
  formatUrl(gameData: any) {
    gameData.map((data, i) => {
      data.CanDelete = true;
      if (new Date(data.EndDate).getTime() < new Date().getTime()) {
        data.CanDelete = false;
      }
      this.commonServices.formatVideoThumbnailUrl(data);
      if(data.ChallengeEntry){
      this.commonServices.formatVideoThumbnailUrl(data.ChallengeEntry);
      }
      if (gameData.length -1 == i) {
        this.gameData = gameData;
      }

      data.CanJoin = false;
      if (new Date() >= new Date(data.StartDate) && new Date() <= new Date(data.EndDate)) {
        data.CanJoin = true;
      }
    });
  }

  openVideo(index: number,game) {
    this.gameData[index].ContentId = this.gameData[index].ContestId;
    const modalRef = this.modalService.open(ViewContentComponent, { centered: true });
    modalRef.componentInstance.data = this.gameData[index];
    modalRef.componentInstance.isVideo = false;
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }


  getReportedVideos(){
    this.reportedVideos.getReportedVideos().subscribe(data => {
      if(data.Status){
        this.formatUrl(data.Data);
      }
    })
  }

  delete(index: number) {
    let confirmation = this.getConfirmation("Are you sure you want to delete this video completely");
    confirmation.then((result) => {
      this.reportedVideos.deleteContest(this.gameData[index].ContestId).subscribe(data => {
        if (data.Status) {
          this.gameData.splice(index, 1);
          this.commonServices.showSuccess(API_MESSAGES.ReportStatus.Delete,'');
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
      });
    }, (reason) => {
    });
  }

  unReport(index: number) {
    let confirmation = this.getConfirmation("Are you sure you want to unReport this video")
    confirmation.then((result) => {
      this.reportedVideos.unReportVideo(this.gameData[index].ContestId).subscribe(data => {
        if (data.Status) {
          this.gameData.splice(index, 1);
          this.commonServices.showSuccess(API_MESSAGES.ReportStatus.UnReport,'');
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
      });
    }, (reason) => {
    });
  }

  getConfirmation(title: string) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, { centered: true });
    let data: any = {"title": title}
    modalRef.componentInstance.data = data;
    return modalRef.result;
  }
}
