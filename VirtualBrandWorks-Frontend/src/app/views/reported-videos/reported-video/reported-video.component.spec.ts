import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedVideoComponent } from './reported-video.component';

describe('ReportedVideoComponent', () => {
  let component: ReportedVideoComponent;
  let fixture: ComponentFixture<ReportedVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
