import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';

@Injectable({
  providedIn: 'root'
})
export class ReportedVideosService {
  constructor(private http: HttpClient) {
  }
  
  getReportedVideos(): Observable<any> {
    return this.http.get(API_NAME.ReportedVideo[0].report);
  }

  deleteContest(contestId: number): Observable<any> {
    return this.http.post(API_NAME.DeleteContest[0].name,{"ContestId": contestId});
  }

  unReportVideo(contestId: number): Observable<any> {
    return this.http.delete(API_NAME.ReportedVideo[0].report +'?ContestId='+ contestId);
  }

}
