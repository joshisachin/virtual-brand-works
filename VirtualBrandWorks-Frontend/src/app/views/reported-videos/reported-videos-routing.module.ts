import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportedVideoComponent } from './reported-video/reported-video.component';

const routes: Routes = [
	{
		path: '',
		component: ReportedVideoComponent
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ReportedVideosRoutingModule {}
