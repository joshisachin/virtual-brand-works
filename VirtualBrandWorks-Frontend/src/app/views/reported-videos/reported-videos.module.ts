import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportedVideoComponent } from './reported-video/reported-video.component';
import { ReportedVideosRoutingModule } from './reported-videos-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportedVideosService } from './reported-videos.service';



@NgModule({
  declarations: [ReportedVideoComponent],
  imports: [
    ReportedVideosRoutingModule,SharedModule,CommonModule
],
providers: [ReportedVideosService]
})
export class ReportedVideosModule { }
