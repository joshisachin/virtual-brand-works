import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaderListComponent } from './leader-list/leader-list.component';
import { LeaderboardComponent } from './leaderboard.component';
import { UserDetailsComponent } from './user-details/user-details.component';


const routes: Routes = [
	{
		path: '',
		component: LeaderboardComponent,
		children: [
			{
				path: '',
				component: LeaderListComponent,
			},
			{
				path: 'playlist/:userId/:role/:playlistId/:isSelfCreated/:name',
				component: UserDetailsComponent,
			},
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class LeaderboardRoutingModule { }
