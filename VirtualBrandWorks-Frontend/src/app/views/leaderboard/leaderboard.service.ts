import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';

@Injectable()
export class LeaderboardService {
  
  constructor(private http: HttpClient) { }

  /**
   * get all playlist
   */
  getAllPlaylist(): Observable<any> {
    return this.http.get(API_NAME.GetAllPlaylist[0].name);
  }

  /**
   * get playlist leader
   */
  getPlaylistLeader(playlistId: number, sortBy: string): Observable<any> {
    return this.http.get(API_NAME.GetPlaylistLeaderboard[0].name + "?playlistId=" + playlistId + "&sortBy=" + sortBy);
  }

  /**
   * get playlist leader team
   */
  getPlaylistLeaderTeams(playlistId: number, sortBy: string): Observable<any> {
    return this.http.get(API_NAME.GetPlaylistLeaderboardByTeam[0].name + "?playlistId=" + playlistId + "&sortBy=" + sortBy);
  }
}
