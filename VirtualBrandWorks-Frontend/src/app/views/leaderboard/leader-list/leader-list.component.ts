import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { PlayListData } from 'src/app/shared/model/created-playlist';
import { Leaderboard } from 'src/app/shared/model/Leaderboard';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { LeaderboardService } from '../leaderboard.service';

@Component({
  selector: 'app-leader-list',
  templateUrl: './leader-list.component.html',
  styleUrls: ['./leader-list.component.scss']
})
export class LeaderListComponent implements OnInit {
  playlistId: number;
  playlists: PlayListData[];
  leaders: Array<Leaderboard>;
  sortBy: string = "1";
  teams: Array<Leaderboard[]>;
  currentNav: number = 1;
  constructor(public leaderService: LeaderboardService, private commonServices: CommonService, private router: Router) { }

  ngOnInit(): void {
    this.getAllPlaylist();
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {
    this.currentNav = changeEvent.nextId;
    this.sortBy = "1";
    if (this.playlistId) {
      this.getApi();
    }
  }

  /**
   * fetch data according to the nav
   * @param null
   * @response null
   */
  getApi() {
    if (this.currentNav == 1) {
      this.getPlaylistLeader();
    }
    else {
      this.getPlaylistLeaderTeams();
    }
  }

  /**
   * get all playlist
   * @param null
   * @response array
   */
  getAllPlaylist() {
    this.leaderService.getAllPlaylist().subscribe(data => {
      if (data.Status) {
        this.playlists = data.Data;
        this.playlistId = this.playlists[0].PlaylistId;
        this.getApi();
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    })
  }

/**
   * get playlist leader team
   * @param sortBy
   */
  getPlaylistLeaderTeams() {
    this.leaderService.getPlaylistLeaderTeams(this.playlistId, this.sortBy).subscribe(data => {
      if (data.Status) {
        this.teams = data.Data;
      }
      else {
        this.teams = [];
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * get playlist leader
   * @param sortBy
   */
  getPlaylistLeader() {
    this.leaderService.getPlaylistLeader(this.playlistId, this.sortBy).subscribe(data => {
      if (data.Status) {
        this.leaders = data.Data;
      }
      else {
        this.leaders = [];
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * select playlist
   * @param value
   * @response null
   */
  selectPlaylist(value: any) {
    this.playlistId = value;
    this.getApi();
  }

  /**
   * sort
   * @param value
   * @response null
   */
  sort(value: any) {
    this.sortBy = value;
    this.getApi();
  }

  /**
   * route to playlist details
   * @param index
   * @response null
   */
  routeToPlaylistDetailsOfUser(index: number) {
    let playlistNameIndex: number = this.playlists.findIndex(data => data.PlaylistId == this.playlistId);
    this.router.navigate(["leaderboard/playlist/"+this.leaders[index].UserId+"/User/"+this.playlistId+"/false/"+this.playlists[playlistNameIndex].Name]);
  }

  /**
   * route to teams
   * @param null
   * @response null
   */
  routeToTeams() {
    this.router.navigate(["/teams"]);
  }
}
