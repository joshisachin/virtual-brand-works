import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  playlist: string;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.playlist = params.name;
    });
  }

  ngOnInit(): void {
  }

}
