import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaderboardComponent } from './leaderboard.component';
import { LeaderListComponent } from './leader-list/leader-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LeaderboardRoutingModule } from './leaderboard-routing.module';
import { PlaylistService } from '../my-playlist/playlist.service';
import { LeaderboardService } from './leaderboard.service';
import { NgCircleProgressModule } from 'ng-circle-progress';



@NgModule({
  declarations: [LeaderboardComponent, LeaderListComponent, UserDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    LeaderboardRoutingModule,
    NgCircleProgressModule.forRoot({
    }),
  ],
  providers: [PlaylistService, LeaderboardService]
})
export class LeaderboardModule { }
