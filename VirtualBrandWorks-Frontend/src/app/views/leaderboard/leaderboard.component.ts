import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {
  toggleRightSide: boolean; // to expand right side when side menu shrinks
  constructor(private commonServices: CommonService) {
    this.commonServices.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  }

  ngOnInit(): void {
  }

}
