import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TeamService } from '../team.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { TeamUser } from 'src/app/shared/model/team-user';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonPopupComponent } from 'src/app/shared/modals/common-popup/common-popup.component';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { ManageLocationService } from '../../manage-location/manage-location.service';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss']
})
export class CreateTeamComponent implements OnInit {

  teamName: string = "";
  teamId: number;
  teamUsers: TeamUser[] = [];
  storeTeamUsers: TeamUser[] = [];
  selectedUsers: TeamUser[] = [];
  selectedTags: any = [];
  searchValue: string = "";
  teamStatus: string = "true";
  locations: Location[];
  @ViewChild("searchLoc", { static: true }) searchLoc: ElementRef;
  selectedLocation: string = "";
  locationId: number = 0;
  filteredLocation: string = "";
  teamHandle:string = "";

  constructor(private modalService: NgbModal, private teamServices: TeamService, private commonServices: CommonService, private route: ActivatedRoute, private router: Router, private httpService: HttpService, private manageLocationService: ManageLocationService) {
    this.route.queryParams.subscribe(params => {
      if (params.id) {
        this.teamId = params.id;
        this.teamName = params.name;
        this.teamHandle = params.handleName
      }
      else {
        this.commonServices.selectedTags.next([]);
      }
    });
    this.getTeamDetail();
  }

  ngOnInit(): void {
    this.getTeamUsers();
    // this.commonServices.searchDebounce(this.searchLoc.nativeElement).subscribe(searchString => {
    //   if (searchString === "") {
    //     this.selectedLocation = "";
    //     this.locationId = 0;
    //     this.locations = [];
    //     //this.commonServices.selectedLocation.next({});
    //   }
    //   else {
    //     this.getLocation(searchString);
    //   }
    // });
    this.getLocations();
  }

  getLocations(){
    let formData = {};
    formData["CountryId"] = "";
    formData["StateId"] = "";
    formData["CityId"] = "";
    formData["LocationName"] = ""; 
    this.manageLocationService.getLocation(formData).subscribe(data => {
      this.locations = data.Data;
    })
  }

  getUsersOnInit(){
    let data = {}
    data["UserRole"] = "4,3";
    data["Search"] = '';
    data["SortBy"] = '';
    this.teamServices.getUsersList(JSON.stringify(data)).subscribe((res: any) => {
      if (res.Data.length > 0) {
        this.teamUsers = res.Data;
        this.storeTeamUsers = res.Data;
        this.teamUsers.map(res => {
          this.selectedUsers.map(data => {
            if (data.UserId == res.UserId) {
              res.IsSelected = true;
              data.IsSelected = true;
            }
          })
        })
      }
      else {
        this.commonServices.showError('No users found', "");
      }
    })
  }

  /**
  * get location
  * @param value
  * @response object
  */
  getLocation(value: string) {
    let formData = {};
    formData["CountryId"] = "";
    formData["StateId"] = "";
    formData["CityId"] = "";
    formData["LocationName"] = value; 
    this.manageLocationService.getLocation(formData).subscribe(data => {
      this.locations = data.Data;
    })
  }

  /**
   * get team detail
   * @param index
   * @response object
   */
  getTeamDetail() {
    this.commonServices.loader.next(true);
    if (this.teamId) {
      this.teamServices.getTeamDetails(this.teamId).subscribe(data => {
        if (data.Status) {
          this.commonServices.selectedTags.next(data.Data.Tags);
          this.selectedTags = data.Data.Tags;
          this.teamStatus = data.Data.Status.toString();
          this.locationId = data.Data.LocationId;
          this.selectedLocation = data.Data.LocationName;
          this.selectedUsers = data.Data.Users
        }
        this.commonServices.loader.next(false);
      });
    }
  }

  ngOnDestroy() {
    this.commonServices.selectedLocation.next({});
  }
  /**
   * get team users
   * @param searchString
   * @response object
   */
  getTeamUsers(searchString: string = "") {
    this.commonServices.selectedLocation.subscribe(value => {
      if (Object.keys(value).length > 0 || searchString) {
        this.filteredLocation = value.LocationName;
        this.commonServices.loader.next(true);
        this.teamServices.getTeamUsers(searchString, value.LocationId, this.teamId).subscribe(data => {
          if (data.Status) {
            this.teamUsers = data.Data;
            let index = this.storeTeamUsers.indexOf(this.storeTeamUsers.find(elem => elem.UserId == data.Data.userId));
            this.teamUsers.map(res => {
              this.selectedUsers.map(data => {
                if(data.UserId == res.UserId){
                  res.IsSelected = true;
                }
              })
            })
            // this.storeTeamUsers[index].IsSelected = true;
            // this.selectedUsers = this.storeTeamUsers.filter(value => value.IsSelected === true);
            // this.storeTeamUsers = data.Data;
            // this.selectedUsers = this.storeTeamUsers.filter(value => value.IsSelected === true);
            this.commonServices.loader.next(false);
          }
          else {
            this.commonServices.showError(data.Message, "");
            this.commonServices.loader.next(false);
          }
        });
      }
      else{
        this.getUsersOnInit();
      }
    });
  }

  /**
   * push selected users
   * @param userId
   * @response object
   */
  pushSelectedUsers(userId: number) {
    let index = this.storeTeamUsers.indexOf(this.storeTeamUsers.find(elem => elem.UserId == userId));
    this.storeTeamUsers[index].IsSelected = true;
    this.selectedUsers = this.storeTeamUsers.filter(value => value.IsSelected === true);
  }

  /**
   * unselect user
   * @param index
   */
  unselectUser(index) {
    this.teamUsers.map(res => {
      if(this.selectedUsers[index].UserId == res.UserId){
        res.IsSelected = false;
      }
    })
    this.selectedUsers[index].IsSelected = false;
    this.selectedUsers = this.selectedUsers.filter(value => value.IsSelected === true);
  }

  /**
   * sort by name
   * @param type
   * @response object
   */
  sortName(type: string) {
    if (type === "user") {
      this.commonServices.performSorting(this.teamUsers);
    }
    else {
      this.commonServices.performSorting(this.selectedUsers);
    }
  }

  /**
   * search
   * @param value 
   */
  search(value: string) {
    if (value) {
      this.teamUsers = this.storeTeamUsers.filter(user => { // filtering the record type from the search bar
        return (user.FirstName.toLowerCase() + user.LastName.toLocaleLowerCase()).indexOf(value.toLowerCase()) > -1;
      });
    }
    else {
      this.teamUsers = this.storeTeamUsers;
    }
  }

  /**
   * create team
   * @param content
   */
  createTeam() {
    this.teamName = this.teamName.trim();
    this.teamHandle = this.teamHandle.trim();
    let tags: any = [];
    this.selectedTags.map(data => {
      tags.push(data.TagId);
    });
    if (!this.teamName) {
      this.commonServices.showError(API_MESSAGES.Team.nameRequire, "");
    }
    else if (this.teamName.length > 30) {
      this.commonServices.showError(API_MESSAGES.Team.nameLength, "");
    }
    else if(!this.teamHandle){
      this.commonServices.showError(API_MESSAGES.Team.teamHandle, "");
    }
    
    else if (this.selectedUsers.length === 0) {
      this.commonServices.showError(API_MESSAGES.Team.memberRequire, "");
    }
    else if (this.locationId == 0) {
      this.commonServices.showError(API_MESSAGES.Team.locationRequired, "");
    }
    else if (this.teamName && this.teamHandle && this.selectedUsers.length > 0 && !this.teamId) {
      if ((this.filteredLocation == "" || this.filteredLocation == undefined) || (this.selectedLocation == this.filteredLocation)) {
        this.createUpdateTeam(tags, true);
      }
      else {
        this.showConfirmation.result.then(() => {
          this.createUpdateTeam(tags, true);
        });
      }
    }
    else if (this.teamName && this.teamHandle && this.selectedUsers.length > 0 && this.teamId) {
      if ((this.filteredLocation == "" || this.filteredLocation == undefined) || (this.selectedLocation == this.filteredLocation)) {
        this.createUpdateTeam(tags, false);
      }
      else {
        this.showConfirmation.result.then(() => {
          this.createUpdateTeam(tags, false);
        });
      }
    }
  }

  createUpdateTeam(tags: any[], isCreate: boolean) {
    if (isCreate) {
      this.commonServices.loader.next(true);
      this.teamServices.createTeam(this.teamName, this.selectedUsers, tags, this.teamStatus, this.locationId, this.teamHandle).subscribe(data => {
        this.openPopup(data.Status, data.Message);
      });
    }
    else {
      this.teamServices.updateTeam(this.teamName, this.selectedUsers, tags, this.teamId, this.teamStatus, this.locationId, this.teamHandle).subscribe(data => {
        this.openPopup(data.Status, data.Message);
      });
    }

  }

  get showConfirmation() {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {
      windowClass: 'modalwidth'
    });

    modalRef.componentInstance.data = {
      "title": "You have selected different location users in the team. Are you sure want to proceed ?",
      "okButton": "Yes",
      "cancelButton": "No"
    };
    return modalRef;
  }

  /**
   * open popup
   * @param status
   * @response null
   */
  openPopup(status: boolean = false, message: string = "") {
    if (status) {
      const modalRef = this.modalService.open(CommonPopupComponent, {
        windowClass: 'modalwidth'
      });
      let title = this.teamId ? "Your team " + this.teamName + " has been updated" : "Your team " + this.teamName + " has been created";
      modalRef.componentInstance.data = {
        "title": "Congratulations",
        "subTitle": title,
        "message": "Would you like to create another team?",
        "buttonOne": "+ Create",
        "buttonTwo": "Go To My Team"
      };
      modalRef.result.then((result) => {
        this.router.navigateByUrl("/teams/create-team");
        this.teamName = "";
        this.teamId = 0;
        this.selectedLocation = "";
        this.locationId = 0;
        this.commonServices.selectedLocation.next({});
        this.commonServices.selectedTags.next([]);
        this.ngOnInit();
      }, (reason) => {
        this.router.navigate(["/teams"]);
      });
    }
    else {
      this.commonServices.showError(message, "");
    }
    this.commonServices.loader.next(false);
  }

  /**
   * get selected tags
   * @param tags
   * @response object
   */
  getSelectedTags(tags: any) {
    this.selectedTags = [];
    if (tags.length > 0) {
      tags.map(data => {
        this.selectedTags.push(data);
      });
    }
  }

  getSelectedLocation(selectedLocation) {
    this.locations.map((res: any)=>{
      if(res.LocationName == selectedLocation){
        selectedLocation = res;
      }
    })
    this.selectedLocation = selectedLocation.LocationName;
    this.locationId = selectedLocation.LocationId;
    // this.locations = [];
  }
}
