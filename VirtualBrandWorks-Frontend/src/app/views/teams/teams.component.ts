import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  toggleRightSide: boolean; // to expand right side when side menu shrinks
  constructor(private commonService: CommonService) {
    this.commonService.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  }

  ngOnInit(): void {
  }

}
