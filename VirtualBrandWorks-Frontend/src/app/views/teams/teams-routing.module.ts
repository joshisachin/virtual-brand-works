import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamsComponent } from './teams.component';
import { TeamComponent } from './team/team.component';
import { CreateTeamComponent } from './create-team/create-team.component';


const routes: Routes = [
	{
		path: '',
		component: TeamsComponent,
		children: [
			{
				path: '',
				component: TeamComponent,
				data: {
					breadcrumb: [
						{
							label: 'Team',
							url: ''
						}
					]
				},
			},
			{
				path: 'create-team',
				component: CreateTeamComponent,
				data: {
					breadcrumb: [
						{
							label: 'Create Team',
							url: ''
						}
					]
				},
			}
		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class TeamsRoutingModule { }
