import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_NAME } from '../../shared/constants/apiname/apiname';
import { Injectable } from '@angular/core';

@Injectable()
export class TeamService {
  tabIndex: number = 1;
  constructor(private http: HttpClient) { }

  /**
   * get teams
   */
  getTeams(isCompanyTeam: boolean = false, trainerId: number = 0, IsTaggedContent: boolean = false): Observable<any> {
    return this.http.post(API_NAME.GetTeams[0].name, { "IsCompanyTeam": isCompanyTeam, "TrainerId": trainerId, "IsTaggedContent" : IsTaggedContent });
  }

  getContents(){
    return this.http.get(API_NAME.GetContest[0].name + '?contestType=4');
  }

  /**
   * getTeamDetails
   */
  getTeamDetails(teamId: number): Observable<any> {
    return this.http.post(API_NAME.GetTeamDetails[0].name, { "TeamId": teamId });
  }

  /**
   * get trainers
   */
  getTrainers(): Observable<any> {
    return this.http.get(API_NAME.GetTrainers[0].name);
  }

  /**
   * get team users
   */
  getTeamUsers(search: string = "", locationId: number = 0, teamId: number = 0): Observable<any> {
    return this.http.post(API_NAME.GetTeamUsers[0].name, { "Search": search, "LocationId": locationId, "TeamId": teamId });
  }

  /**
   * create team
   */
  createTeam(teamName: string, users: any, tags: any, teamStatus: string, locationId: number, teamHandle: string): Observable<any> {
    return this.http.post(API_NAME.CreateTeam[0].name, { "TeamName": teamName, "Users": users, "TagIds": tags, "Status": teamStatus, "LocationId": locationId, "handleName": teamHandle });
  }

  /**
   * update team
   */
  updateTeam(teamName: string, users: any, tags: any, teamId: number, teamStatus: string, locationId: number, teamHandle: string): Observable<any> {
    return this.http.post(API_NAME.UpdateTeam[0].name, { "TeamName": teamName, "Users": users, "TagIds": tags, "TeamId": teamId, "Status": teamStatus, "LocationId": locationId, "handleName": teamHandle });
  }

  /**
   * delete team
   */
  deleteTeam(teamId: number): Observable<any> {
    return this.http.delete(API_NAME.DeleteTeam[0].name + "?TeamId=" + teamId);
  }

  getUsersList(data) {
    return this.http.post(API_NAME.GetCompanyUsers[0].name, data);
  }
}
