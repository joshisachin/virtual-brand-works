import { Component, OnInit } from '@angular/core';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { TeamService } from '../team.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  
  isAdmin: boolean = true; // check admin
  constructor(private teamService: TeamService, private commonServices: CommonService) {}

  ngOnInit() {
    let userRole = localStorage.getItem("roleId");
    if (userRole == "4" || userRole == "3") {
      this.isAdmin = false;
    }
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {
    this.commonServices.loader.next(true);
    this.teamService.tabIndex = changeEvent.nextId;
  }

}
