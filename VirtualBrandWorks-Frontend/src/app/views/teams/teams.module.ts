import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { TeamsRoutingModule } from './teams-routing.module';
import { TeamComponent } from './team/team.component';
import { TeamsComponent } from './teams.component';
import { TeamService } from './team.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreateTeamComponent } from './create-team/create-team.component';
import { FormsModule } from '@angular/forms';
import { ManageLocationService } from '../manage-location/manage-location.service';



@NgModule({
  declarations: [
    TeamsComponent,
    TeamComponent,
    CreateTeamComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    FormsModule,
    TeamsRoutingModule
  ],
  providers: [
    TeamService,
    ManageLocationService
  ]
})
export class TeamsModule { }
