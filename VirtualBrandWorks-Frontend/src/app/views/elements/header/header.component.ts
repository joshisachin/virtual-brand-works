import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  name: string;// name string
  profilePicture: string;
  logoImage: string;
  notifications: any;
  showNotification: boolean = false;
  unReadCount: number = 0;

  constructor(private commonSerivces: CommonService, private httpService: HttpService, private renderer: Renderer2, private router: Router) {
    this.commonSerivces.updateProfileImage.subscribe(imageUrl => {
      this.profilePicture = imageUrl;
    });

    this.commonSerivces.updateUserName.subscribe(userName => {
      this.name = userName;
    });

    this.commonSerivces.updateLogoImage.subscribe(image => {
      this.logoImage = image;
    });

    this.updateNotifications();

  }

  ngOnInit() {
    this.updateNotifications();
    this.name = localStorage.getItem("userName");
    this.profilePicture = localStorage.getItem("profileImage");
    this.logoImage = localStorage.getItem("logoImage");

    this.renderer.listen('document', 'click', (evt) => {
      this.redirectToNotifications(evt.srcElement);
    });
  }

  /**
   * logout
   * @param null
   * @response null
   */
  logout() {
    this.commonSerivces.logout();
  }

  showPanel() {
    if(!this.showNotification) {
      this.updateReadStatus();
    }
    this.showNotification = !this.showNotification;
    
  }


  /**
   * To update the notifications on header.
   * @param notifications updated notification object 
   */
  updateNotifications() {
    this.httpService.getNotifications(localStorage.getItem("userId")).subscribe(data => { //
      if (data.Data) {
        this.notifications = data.Data;
        this.unReadCount = 0;
        this.notifications.map(m => {
          if (!m.IsRead) {
            this.unReadCount = this.unReadCount + 1;
          }
        })
      }
    });
  }

   /**
   * update the read status of notifications
   */
  updateReadStatus() {
    this.httpService.readNotifications(localStorage.getItem("userId")).subscribe(data => { //
      if (data.Status) {
        this.unReadCount = 0;
      }
    });
  }

   /**
   * Redirect bubble notifications to detail on click
   */
  redirectToNotifications(srcElement: any) {
    if(srcElement.getAttribute('class')?.indexOf('notifycheck') >= 0) {
      this.showNotification = true;
      if (srcElement.getAttribute('id') == 'notificationsLink') {
        this.router.navigate([srcElement.getAttribute('name')]);
      }
      else if (srcElement.getAttribute('id') == 'extenddate') {
        if (srcElement.getAttribute('value') == "Extend") {
          if(srcElement.getAttribute('valuetype') == "contest") {
            this.httpService.extendContentDate(Number(srcElement.getAttribute('contestId')), Number(srcElement.parentElement.getAttribute('notificationid')),"contest").subscribe(data => { //
              if (data.Status) {
                this.updateNotifications();
              }
            });
          }
          else if(srcElement.getAttribute('valuetype') == "contestentry") {
            this.httpService.extendContentDate(Number(srcElement.getAttribute('contestId')), Number(srcElement.parentElement.getAttribute('notificationid')),"contestentry").subscribe(data => { //
              if (data.Status) {
                this.updateNotifications();
              }
            });
          }
          else if(srcElement.getAttribute('valuetype') == "companylibrarycontent") {
            this.httpService.extendContentDate(Number(srcElement.getAttribute('contestId')), Number(srcElement.parentElement.getAttribute('notificationid')),"companylibrarycontent").subscribe(data => { //
              if (data.Status) {
                this.updateNotifications();
              }
            });
          }
        }
        else if (srcElement.getAttribute('value') == "Opt Out") {
          this.httpService.OptOutNotifications(Number(srcElement.parentElement.getAttribute('notificationid'))).subscribe(data => { //
            if (data.Status) {
              this.updateNotifications();
            }
          });
        }
      }
    }
    else {
      this.showNotification = false;
    } 
  }
}
