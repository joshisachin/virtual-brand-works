import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FolderContent } from 'src/app/shared/model/folder-content';
import { Folder } from 'src/app/shared/model/folder';
import { CompanyContentService } from '../../company-content/company-content.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';

@Component({
  selector: 'app-folder-details-common',
  templateUrl: './folder-details-common.component.html',
  styleUrls: ['./folder-details-common.component.scss']
})
export class FolderDetailsCommonComponent implements OnInit {

  @Input() folderType: string;
  detailApi: string;
  folderDetails: FolderContent[];
  folder: Folder;
  folderId: number;
  @ViewChild("searchContent", { static: true }) searchContent: ElementRef;
  constructor(private httpServices: HttpService, private route: ActivatedRoute, private commonServices: CommonService, private companyServices: CompanyContentService, private router: Router, private modalService: NgbModal) {
    this.commonServices.loader.next(true);
  }

  ngOnInit(): void {
    if (this.folderType == "master") {
      this.detailApi = "getMasterFolderContent"; // set api for master folder
    }
    else {
      this.detailApi = "getCompanyFolderContent"; // set api for company content folder
    }
    this.route.params.subscribe(params => {
      if (params.id) {
        this.folderId = params.id;
        this.getDetails();
        this.commonServices.searchDebounce(this.searchContent.nativeElement).subscribe(searchString => {
          this.getDetails(searchString);
        });
      }
      else {
        this.commonServices.showError(API_MESSAGES.CommonError.message, "");
        this.commonServices.loader.next(false);
      }
    });
  }

  /**
   * get company content details
   * @param id
   * @response object
   */
  getDetails(searchString: string = "") {
    this.httpServices[this.detailApi](this.folderId, searchString).subscribe(data => {
      if (data.Status) {
        this.folderDetails = data.Data.Contents;
        this.folder = data.Data.Folder;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * create content
   * @param null
   * @response null
   */
  createContent() {
    this.companyServices.createContentPopup().then(data => {
      if (data) {
        this.getDetails();
      }
    });
  }

  /**
   * open content details
   * @param index
   * @response null
   */
  openContentDetails(index: number) {
    let mainRoute: string = "/company-content/"
    if (this.folderType == "master") {
      mainRoute = "/masterlibrary/";
    }
    this.router.navigate([mainRoute + this.folderDetails[index].FolderName + "/" + this.folderDetails[index].FolderId + "/" + this.folderDetails[index].ContentName + "/" + this.folderDetails[index].ContentId]);
  }

  /**
   * check the mime type
   * @param details
   * @response null
   */
  checkType(details: any) {
    if ((details.MimeType && details.MimeType.match("video"))) {
      return true;
    }
    else if (details.IsYoutubeContent || details.IsFacebookContent) {
      details.ThumbnailUrl = details.ThumbnailURL;
      details.ContentUrl = details.ContentURL;
      this.commonServices.formatVideoThumbnailUrl(details);
      details.ThumbnailURL = details.ThumbnailUrl;
      return false;
    }
    else {
     return false; 
    }
  }

  /**
   * delete content
   * @index
   * @response null
   */
  delete(index: number) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {
      windowClass: 'modalwidth'
    });

    modalRef.componentInstance.data = {
      "title": "Are you sure you want to delete this content?",
      "okButton": "Yes",
      "cancelButton": "Cancel"
    };

    modalRef.result.then(() => {
      this.commonServices.loader.next(true);
      this.companyServices.deleteContent(this.folderDetails[index].ContentId).subscribe(data => {
        if (data.Status) {
          this.folderDetails.splice(index, 1);
          this.folder.NumberOfContent--;
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
      });
    });
  }
}
