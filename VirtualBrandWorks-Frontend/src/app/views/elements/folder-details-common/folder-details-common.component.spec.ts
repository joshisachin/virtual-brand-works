import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderDetailsCommonComponent } from './folder-details-common.component';

describe('FolderDetailsCommonComponent', () => {
  let component: FolderDetailsCommonComponent;
  let fixture: ComponentFixture<FolderDetailsCommonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderDetailsCommonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderDetailsCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
