import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderFooterComponent } from './folder-footer.component';

describe('FolderFooterComponent', () => {
  let component: FolderFooterComponent;
  let fixture: ComponentFixture<FolderFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
