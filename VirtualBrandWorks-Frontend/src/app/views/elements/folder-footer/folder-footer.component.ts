import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';

@Component({
  selector: 'app-folder-footer',
  templateUrl: './folder-footer.component.html',
  styleUrls: ['./folder-footer.component.scss']
})
export class FolderFooterComponent implements OnInit {
  selectedFolders: any = []; // selected folders object
  @Input() folderType: string;
  getSelectedFolderApi: string;
  saveSelectedFolderApi: string;
  @Output() showFooterFolderVar = new EventEmitter<boolean>(); // toggle for selected folder footer
  constructor(private commonService: CommonService, private httpService: HttpService) { }

  ngOnInit(): void {
    this.commonService.selectedFolders.subscribe(data => { // object of selected folders
      if (data) {
        this.selectedFolders = data;
      }
    });

    if (this.folderType == "master") {
      this.getSelectedFolderApi = "getSelectedFolders";
      this.saveSelectedFolderApi = "saveSelectedFolder";
    }
    else {
      this.getSelectedFolderApi = "getCompanyContentSelectedFolder";
      this.saveSelectedFolderApi = "saveCompanySelectedFolder";
    }
  }

  /**
   * unselect a folder
   * @param index 
   */
  unselectFolder(index: number) {
    this.commonService.loader.next(true);
    this.httpService[this.saveSelectedFolderApi](this.selectedFolders[index].FolderId, false).subscribe(data => {
      if (data.Status) {
        this.commonService.getSelectedFolders(this.getSelectedFolderApi); // temporary set for master library
        // remove from the tile
        if (this.selectedFolders[index].IsDynamicFolder) {
          let dynamicFolder = this.commonService.dynamicFolder.getValue();
          dynamicFolder.IsSelectedFolder = false;
          this.commonService.dynamicFolder.next(dynamicFolder);
        }
        else {
          let arr = this.commonService.folders.getValue(); // get the array where changes need to be done
          let num = arr.findIndex((arr) => { // get the index where the changes need to be done
            return arr["FolderId"] == this.selectedFolders[index].FolderId; // do the changes
          });
          if (num >= 0) { // to check if the unselected folders exists in our current object of folders displaying
            arr[num].IsSelectedFolder = false;
            this.commonService.folders.next(arr);
          }
        }
        this.commonService.loader.next(false);
      }
    });
  }

  /**
   * hide the footer list of folder
   * @param null
   * @response event
   */
  hideFooterFolder() {
    this.showFooterFolderVar.emit(false); // to destruct the object of the current component
  }
}
