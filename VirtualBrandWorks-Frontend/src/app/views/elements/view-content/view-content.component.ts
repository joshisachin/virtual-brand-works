import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Game } from 'src/app/shared/model/Game';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { BrandworksPlayService } from '../../brandworks-play/brandworks-play.service';
import { StartGameComponent } from '../../brandworks-play/start-game/start-game.component';

@Component({
  selector: 'app-view-content',
  templateUrl: './view-content.component.html',
  styleUrls: ['./view-content.component.scss']
})
export class ViewContentComponent implements OnInit {
  @Input() data: Game;
  storeData: Game;
  type: string;
  contentUrl: SafeUrl;
  loggedInUser: string = localStorage.getItem("userId");
  currRoute: any;
  @Input() isVideo: boolean;
  constructor(private vbPlayServices: BrandworksPlayService, private commonServices: CommonService, private domSanitizer: DomSanitizer, private activeModal: NgbActiveModal, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.storeData = JSON.parse(JSON.stringify(this.data));
    this.currRoute = this.router.url;
    if (this.data.IsCompanyLibraryContent) {
      this.setType();
    }
    this.commonServices.formatVideoUrl(this.storeData);
    this.viewCount();
  }

  ngOnDestroy() {
    this.data = JSON.parse(JSON.stringify(this.storeData));
  }

  /**
   * close
   * @param null
   * @response null
   */
  close() {
    this.activeModal.dismiss();
  }

  /**
   * view count
   * @param index
   * @response null
   */
  viewCount() {
    let contentyType: number = 1;
    if (this.data.ContestId || !this.data.IsCompanyLibraryContent) {
      contentyType = 2;
    }
    if (this.data.ContestEntryId || this.data.IsContestEntry) {
      contentyType = 3;
    }
    this.vbPlayServices.viewCount(this.data.ContentId, contentyType, this.isVideo).subscribe(data => { });
  }

  /**
   * like
   * @param index
   * @response null
   */
  like() {
    this.vbPlayServices.like(this.data.ContentId, this.data.IsContestEntry ? this.data.IsContestEntry : false, !this.data.IsLikedByUser).subscribe(data => {
      if (data.Status) {
        this.data.IsLikedByUser = !this.data.IsLikedByUser;
        this.storeData.IsLikedByUser = !this.storeData.IsLikedByUser;
        if (this.data.IsLikedByUser) {
          this.data.Likes++;
          this.storeData.Likes++;
        }
        else {
          this.data.Likes--;
          this.storeData.Likes--;
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * set the type of content
   * @param null
   * @response null
   */
  setType() {
    if (this.data.MimeType) {
      if (this.data.MimeType.match("image")) {
        this.type = "image";
      }
      else if (this.data.MimeType.match("video")) {
        this.type = "video";
      }
      else {
        this.type = "docs";
        this.data.ContentUrl = "https://docs.google.com/gview?url=" + encodeURIComponent(this.data.ContentUrl) + "&embedded=true";
        this.contentUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.data.ContentUrl)
      }
    }
  }
  
  /**
   * start game popup
   * @param null
   * @response null
   */
  startGame(type: string="create", contestId: number = 0, index:number=0) { 
    const modalRef = this.modalService.open(StartGameComponent, { windowClass: 'w925', centered: true });
    modalRef.componentInstance.type = type;
    modalRef.componentInstance.contestId = contestId;
    if(type=="update"){
      modalRef.componentInstance.contestEntry = this.data;
    }
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

}
