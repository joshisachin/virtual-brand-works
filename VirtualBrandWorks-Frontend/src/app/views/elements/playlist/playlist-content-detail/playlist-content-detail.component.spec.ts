import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistContentDetailComponent } from './playlist-content-detail.component';

describe('PlaylistContentDetailComponent', () => {
  let component: PlaylistContentDetailComponent;
  let fixture: ComponentFixture<PlaylistContentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistContentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistContentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
