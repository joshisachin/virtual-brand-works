import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MyPlaylistFolderDetail } from 'src/app/shared/model/my-playlist-folder-detail';
import { PlaylistContentResultList } from 'src/app/shared/model/playlist-content-list';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { PlaylistService } from 'src/app/views/my-playlist/playlist.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlayQuizComponent } from 'src/app/shared/modals/quiz/play-quiz/play-quiz.component';

@Component({
  selector: 'app-playlist-content-detail',
  templateUrl: './playlist-content-detail.component.html',
  styleUrls: ['./playlist-content-detail.component.scss']
})
export class PlaylistContentDetailComponent implements OnInit {

  folderPlaylistId: number;
  folderDetailData: MyPlaylistFolderDetail;
  folderContentDetail: any = [];
  playlistContentList: PlaylistContentResultList;
  daysLeft: number;
  timeLeft: any;
  type: string;
  contentUrl: SafeUrl;
  video: ElementRef;
  videoC: any;
  contentType: string;
  playlistId: number;
  contentIndex: number;
  @ViewChild("videoContainer", { static: false }) set content(video: ElementRef) {
    if (video) {
      this.videoC = video;
    }
  }

  constructor(
    public playlistService: PlaylistService,
    private route: ActivatedRoute,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal
  ) {
    this.route.params.subscribe(params => {
      this.playlistId = params.playlistId;
      this.folderPlaylistId = params.folderPlaylistId;
      this.getPlaylistDetail(0);
    })
  }

  ngOnInit(): void {
  }

  /**
   * Get playlist folder detail
   */

  getPlaylistDetail(getMyPlaylistFolderDetail) {
    this.contentIndex = getMyPlaylistFolderDetail;
    this.playlistService.getMyPlaylistFolderDetail(this.folderPlaylistId, getMyPlaylistFolderDetail).subscribe(folderDetail => {
      this.folderDetailData = folderDetail.Data;
      this.folderContentDetail = folderDetail.Data.PlaylistContent;
      this.setType();
      if (this.type == "video" && this.videoC) {
        this.videoC.nativeElement.load();
      }
      this.getFolderContent(this.folderPlaylistId, this.folderContentDetail.PlaylistContentMasterId)
      this.getDataDiff(new Date(this.folderDetailData.StartDate), new Date(this.folderDetailData.EndDate))
    })
  }

  /**
   * Get playlist folder left time
   * @param startDate
   * @param endDate 
   */

  getDataDiff(startDate, endDate) {
    let diff = endDate.getTime() - startDate.getTime();
    let days = Math.floor(diff / (60 * 60 * 24 * 1000));
    let hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
    let minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
    let seconds = Math.floor(diff / 1000) - ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60));
    this.daysLeft = days;
    this.timeLeft = hours > 0 ? (hours > 1 ? hours + ' hrs' : hours + ' hr') :
      minutes > 0 ? (minutes > 1 ? minutes + ' mins' : minutes + ' min') :
        seconds > 0 ? (seconds > 1 ? seconds + ' secs' : seconds + ' sec') : '';
  }

  /**
   * Get playist folder content list and detail
   * @param folderPlaylistId 
   * @param playlistContentMasterId 
   */

  getFolderContent(folderPlaylistId: number, playlistContentMasterId: number) {
    this.playlistService.getMyPlaylistContentList(folderPlaylistId, playlistContentMasterId).subscribe(result => {
      this.playlistContentList = result.Data;
    })
  }

  /**
   * set the type of content
   * @param null
   * @response null
   */
  setType() {
    if (this.folderContentDetail.Content.MimeType) {
      if (this.folderContentDetail.Content.MimeType.match("image")) {
        this.type = "image";
      }
      else if (this.folderContentDetail.Content.MimeType.match("video")) {
        this.type = "video";
      }
      else {
        this.type = "docs";
        this.folderContentDetail.Content.ContentURL = "https://docs.google.com/gview?url=" + encodeURIComponent(this.folderContentDetail.Content.ContentURL) + "&embedded=true";
        this.contentUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.folderContentDetail.Content.ContentURL)
      }
    }
  }

  /**
   * play quiz
   * @param null
   * @response null
   */
  playQuiz() {
    const modalRef = this.modalService.open(PlayQuizComponent, {});
    let reqObj = {
      "quizId": this.folderContentDetail.Content.QuizId,
      "playlistId": this.playlistId,
      "playlistContentMasterId": this.folderContentDetail.PlaylistContentMasterId,
      "isMasterContent": this.folderDetailData.Folder.IsMasterLibraryFolder,
      "isTurnedOffExistingQuiz": this.folderContentDetail.IsTurnedOffExistingQuiz
    }
    modalRef.componentInstance.reqObj = reqObj;
    modalRef.result.then((result) => {
      this.folderContentDetail.Content.QuizId = result.QuizId;
      this.folderContentDetail.NumberOfQuizAttempts = result.NumberOfQuizAttempts;
      this.folderContentDetail.IsUserPassed = result.IsUserPassed;
      this.getPlaylistDetail(this.contentIndex);
    }, (reason) => {
    });
  }
}
