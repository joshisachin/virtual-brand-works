import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PointsLevelComponent } from 'src/app/shared/modals/points-level/points-level.component';
import { TopPerformer } from 'src/app/shared/model/top-performer';
import { PlaylistService } from 'src/app/views/my-playlist/playlist.service';

@Component({
  selector: 'app-top-performer',
  templateUrl: './top-performer.component.html',
  styleUrls: ['./top-performer.component.scss']
})
export class TopPerformerComponent implements OnInit {

  @Input() playlistId: number = 0;
  @Input() isTopTen: boolean = true;
  topPerformerData: Array<TopPerformer>;
  isTopPerformer: boolean = true;

  constructor(
    private route: ActivatedRoute,
    public playlistService: PlaylistService,
    private commonService: CommonService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.playlistId) {
        this.playlistId = params.playlistId;
      }
      this.getTopPerformer();
    })
  }

  ngOnChanges() {
    if (this.playlistId) {
      this.getTopPerformer();
    }
  }

  getTopPerformer() {
    this.commonService.loader.next(true);
    this.playlistService.getPlaylistTopPerformer(this.playlistId, this.isTopTen).subscribe(result => {
      if (result.Status) {
        this.commonService.loader.next(false);
        this.topPerformerData = result.Data;
        this.isTopPerformer = result.Data.length == 0 ? false : true;
      } else {
        this.commonService.showError(result.Message, "");
        this.commonService.loader.next(false);
      }
    })
  }

  openPointsLevel() {
    this.playlistService.getResultFolderAndDetail(this.playlistId, parseInt(localStorage.getItem("userId"))).subscribe(folderData => {
      const modalRef = this.modalService.open(PointsLevelComponent, {
        windowClass: 'w925',
        centered: true
      });
      modalRef.componentInstance.data = {

      };
      modalRef.result.then((result) => {
      }, (reason) => { });
    })
  }

}
