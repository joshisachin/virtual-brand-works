import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { ViewResultComponent } from 'src/app/shared/modals/playlist/view-result/view-result.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaylistService } from 'src/app/views/my-playlist/playlist.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-playlist-detail-view',
  templateUrl: './playlist-detail-view.component.html',
  styleUrls: ['./playlist-detail-view.component.scss']
})
export class PlaylistDetailViewComponent implements OnInit {
  
  @Input() playlistDetail: string; // playlist detail
  playlistId: number;
  getPlaylistDetailApi: string;
  playlistDetailData: any;
  isSelfCreated: boolean = false;
  userId: number = 0;
    
  constructor(
    public playlistService: PlaylistService,
    private route: ActivatedRoute,
    private _location: Location,
    private commonService: CommonService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit(): void {
    if(this.playlistDetail == 'associatedPlaylistDetail' || this.playlistDetail == 'userPlaylistDetail') {
      this.getPlaylistDetailApi = 'getAssociatedPlaylistDetail'
    }

    this.route.params.subscribe(params => {
      this.playlistId = params.playlistId;
      this.userId = params.userId;
      this.isSelfCreated = params.isSelfCreated == "true" ? true : false;
      this.getPlaylistDetail();
		})
  }

  /**
   * get playlist detail
   * @param null
   * @response null
   */
  getPlaylistDetail() {
    this.commonService.loader.next(true);
    this.playlistService[this.getPlaylistDetailApi](this.playlistId, this.isSelfCreated, this.userId).subscribe(result => {
      if(result.Status) {
        this.commonService.loader.next(false);
        this.playlistDetailData = result.Data;
      } else {
        this.commonService.showError(result.Message, "");
        this.commonService.loader.next(false);
      }
    })
  }

  /**
   * view result
   * @param null
   * @response null
   */
  viewResult() {
    const modalRef = this.modalService.open(ViewResultComponent, {
      windowClass: 'resultWindow',
      size: 'xl',
      centered: true
    });
    modalRef.componentInstance.data = {
      playlistId: this.playlistId,
      userId: (this.userId== undefined)? localStorage.getItem("userId"):this.userId
    };
    modalRef.result.then((result) => {
    }, (reason) => { });
  }

  close() {
    this._location.back();
  }

  /**
   * folder content detail
   * @param playList 
   * @param folder 
   * @response null
   */
  folderContentDetail(playList: any, folder: any) {
    if(folder.NumberOfContent == 0) {
      this.commonService.showError(API_MESSAGES.Playlist.noContentFound, "");
      return false;
    }
    let path = this.router.routerState.snapshot.root.firstChild.url[0].path;
    path = path == "leaderboard" ? "my-playlist" : path;
    let routeUrl = path+"/" + playList.Name + "/" + playList.PlaylistId + "/" + folder.FolderName + "/" + folder.PlayListFolderId;
    this.router.navigate([routeUrl]);
  }

  /**
   Route to assign to more
   @param null
   @response null
   */
  assignToMore() {
    this.router.navigate(["/build-playlist/assign-users-teams/" + this.playlistId + "/true"])
  }

  /**
   * clone playlist
   * @param null
   * @response null
   */
  clone() {
    this.playlistService.clonePlaylist(this.playlistId).subscribe(data => {
      if (data.Status) {
        localStorage.setItem("playlistId", data.Data);
        this.router.navigate(["/build-playlist"]);
      }
      else {
        this.commonService.showError(data.Message, "");
      }
    })
  }

}
