import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { PlayListData } from 'src/app/shared/model/created-playlist';
import { PlaylistService } from 'src/app/views/my-playlist/playlist.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RenameComponent } from 'src/app/shared/modals/playlist/rename/rename.component';
import { BuildPlaylistService } from 'src/app/views/build-playlist/build-playlist.service';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';

@Component({
  selector: 'playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss']
})
export class PlaylistComponent implements OnInit {

  isLaunched: boolean = true;
  isCompleted: boolean = false;
  searchKey: string = "";
  routeUserId: any;
  routeRole: string;
  playlistOption: any;
  checkedValue: any;
  userPlaylistData: Array<PlayListData>;
  isResult: boolean = true;
  getPlaylistApi: string;
  isSearchView: any;
  loggedInUserRole: string;
  selectedStatus: any = "";
  @Input() playlistType: string;
  searchPlaylistRef: ElementRef;
  @ViewChild("searchPlaylist", { static: false }) set content(searchPlaylistRef: ElementRef) {
    if (searchPlaylistRef) {
      this.isSearchView = searchPlaylistRef;
    }
  }

  constructor(
    public playlistService: PlaylistService,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private router: Router,
    private modalService: NgbModal,
    private buildPlaylistService: BuildPlaylistService
  ) {
    this.route.params.subscribe(params => {
      this.routeUserId = params.userId;
      this.routeRole = params.role;
    })
  }

  ngOnInit(): void {
    this.clearData();
    this.loggedInUserRole = localStorage.getItem("userRole");
    if (this.playlistType == "associatedPlaylist") {
      this.associatedPlaylist();
    } else if (this.playlistType == "myPlaylist") {
      this.usersPlaylist();
    }
    else {
      this.adminTrainerPlaylist();
    }
  }

  /**
   * Associated playlist
   * @param null
   * @response null
   */
  associatedPlaylist() {
    this.playlistOption = [ // tab options
      { "title": "Launched", "value": "published", "id": "publishedCheck" },
      { "title": "Not Launched", "value": "unpublished", "id": "unPublishedCheck" },
      { "title": "Assigned", "value": "assigned", "id": "assignedCheck" }
    ]
    this.checkedValue = this.routeRole == 'User' ? "assigned" : 'published';

    setTimeout(() => {
      this.commonService.searchDebounce(this.isSearchView.nativeElement).subscribe(searchString => {
        if (searchString === "") {
          this.userPlaylistData = null;
          this.searchKey = "";
          this.getPlaylistApi = (this.checkedValue == 'published' || this.checkedValue == 'unpublished') ? 'getUserCreatedPlaylistList' : 'getUserAssignedPlaylistList';
          this.getPlaylist();
        } else {
          this.searchKey = searchString;
          this.userPlaylistData = null;
          this.getPlaylistApi = (this.checkedValue == 'published' || this.checkedValue == 'unpublished') ? 'getUserCreatedPlaylistList' : 'getUserAssignedPlaylistList';
          this.getPlaylist();
        }
      });
    }, 1);

    this.getPlaylistApi = this.routeRole == 'User' ? 'getUserAssignedPlaylistList' : 'getUserCreatedPlaylistList'; //set api for Associated playlist
    this.getPlaylist()
  }

  /**
   * Users my playlist
   */
  usersPlaylist() {
    // tab options
    this.playlistOption = [
      { "title": "Assigned", "value": "assigned", "id": "assignedCheck" },
      { "title": "Completed", "value": "completed", "id": "completedCheck" }
    ]
    this.checkedValue = 'assigned';
    this.getPlaylistApi = 'getMyPlaylist'; //set api for my playlist
    this.getPlaylist()
  }

  /**
   * Trainer and admin my playlist
   */
  adminTrainerPlaylist() {
    this.playlistOption = [
      { "title": "Launched", "value": "assigned", "id": "assignedCheck" },
      { "title": "Not Launched", "value": "notassigned", "id": "notAssigned" },
    ]
    this.checkedValue = 'assigned';
    this.getPlaylistApi = 'getMyCreatedPlaylist'; //set api for my playlist
    this.getPlaylist();
  }

  /**
   * Get checkbox selected value
   * @param event
   * @param null
   */
  getSelectedCheckboxValue() {
    this.clearData();
    if (this.playlistType == "associatedPlaylist") {
      if (this.checkedValue == 'published' || this.checkedValue == 'unpublished') {
        this.isLaunched = this.checkedValue == 'published' ? true : false;
        this.getPlaylistApi = 'getUserCreatedPlaylistList';
        this.getPlaylist()
      } else {
        this.getPlaylistApi = 'getUserAssignedPlaylistList';
        this.getPlaylist()
      }
    } else if (this.playlistType == "myPlaylist") {
      this.getPlaylistApi = 'getMyPlaylist'; //set api for my playlist
      this.isCompleted = this.checkedValue == 'completed' ? true : false;
      this.getPlaylist();
    } else {
      this.getPlaylistApi = 'getMyCreatedPlaylist'; //set api for my playlist
      this.isLaunched = this.checkedValue == 'assigned' ? true : false;
      this.getPlaylist();
    }

  }

  /**
   * Get playlist 
   * @param null
   * @response PlayListData
   */
  getPlaylist() {
    let data = {};
    if (this.getPlaylistApi == 'getUserCreatedPlaylistList' || this.getPlaylistApi == 'getUserAssignedPlaylistList') {
      data["UserId"] = this.routeUserId;
      data["SearchText"] = this.searchKey;
      data["IsLaunched"] = this.isLaunched;
    } else if (this.getPlaylistApi == 'getMyPlaylist') {
      data = this.isCompleted;
    }
    else {
      data = this.isLaunched;
    }
    this.commonService.loader.next(true);
    this.playlistService[this.getPlaylistApi](JSON.stringify(data)).subscribe(result => {
      if (result.Status) {
        this.commonService.loader.next(false);
        this.isResult = result.Data.length == 0 ? false : true;
        this.userPlaylistData = result.Data;
      } else {
        this.isResult = false;
        this.commonService.showError(result.Message, "");
        this.commonService.loader.next(false);
      }
    })
  }

  /**
   * clear data
   * @param null
   * @response null
   */
  clearData() {
    this.userPlaylistData = null;
    this.searchKey = "";
    this.isResult = true;
  }

  /**
   * playlist detail
   * @param data 
   * @param index 
   */
  playlistDetail(data: any, index: number) {
    if (!data.Status) {
      return false;
    }

    this.playlistService.playlistDetailTabIndex = index;
    let isSelfCreated: boolean = false;
    if (this.playlistType == "createdPlaylist" || (this.playlistType == "associatedPlaylist" && this.checkedValue == "published")) {
      isSelfCreated = true;
    }
    let path = this.router.routerState.snapshot.root.firstChild.url[0].path;
    let routeUrl = path == 'my-profile' ? "/" + path + "/playlist/" + this.routeUserId + "/" + data.PlaylistId : path == 'users' ? "/" + path + "/playlist/" + this.routeUserId + "/" + this.routeRole + "/" + data.PlaylistId + "/" + isSelfCreated : path + "/" + data.Name + "/" + data.PlaylistId + "/" + isSelfCreated;
    this.router.navigate([routeUrl]);
  }

  /**
   * route to edit playlist
   * @param playlist
   * @response null
   */
  routeTo(playlist: any) {
    let url: string = "";
    if (playlist.IsExpired && this.checkedValue == "notassigned") {
      this.commonService.showError(API_MESSAGES.Playlist.expiredPlaylist, "");
      return false;
    }
    else if (this.checkedValue == "assigned") {
      url = "/build-playlist/assign-users-teams/" + playlist.PlaylistId + "/true";
    }
    else {
      url = "/build-playlist";
    }
    localStorage.setItem("playlistId", playlist.PlaylistId);
    this.router.navigate([url]);
  }

  /**
   * get status
   * @param playlist 
   */
  getStatus(playlist: any) {
    if (this.checkedValue == 'completed' && playlist.IsExpired) {
      return playlist.IsUserPass ? 'Inactive/Pass' : 'Inactive/Fail';
    } else if (this.checkedValue == 'completed' && !playlist.IsExpired) {
      return playlist.IsUserPass ? 'Active/Pass' : 'Active/Fail';
    } else {
      return playlist.IsExpired ? 'Inactive' : 'Active';
    }
  }

  showRemainingTime(playlist: any) {
    return this.commonService.timeDifference(new Date(playlist.EndDate), new Date());
  }

  /**
   * rename playlist
   * @param index
   * @response null
   */
  rename(index: number) {
    const modalRef = this.modalService.open(RenameComponent, {
      ariaLabelledBy: 'modal-basic-title',
      windowClass: 'w925',
      centered: true
    });
    modalRef.componentInstance.data = {
      name: this.userPlaylistData[index].Name,
      description: this.userPlaylistData[index].Description,
      playlistId: this.userPlaylistData[index].PlaylistId
    };
    modalRef.result.then((result) => {
      this.userPlaylistData[index].Name = result;
    }, (reason) => {
    });
  }

  /**
   * deactivate playlist
   * @param index
   * @response null
   */
  deactivate(index: number) {
    this.commonService.loader.next(true);
    this.buildPlaylistService.launchPlaylist(this.userPlaylistData[index].PlaylistId, false).subscribe(data => {
      if (data.Status) {
        this.userPlaylistData.splice(index, 1);
      }
      else {
        this.commonService.showError(data.Message, "");
      }
      this.commonService.loader.next(false);
    });
  }

  /**
   * delete playlist
   * @param index
   * @response null
   */
  delete(index: number) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, {
      windowClass: 'modalwidth',
      centered: true
    });

    modalRef.componentInstance.data = {
      "title": 'Are you sure you want to delete?'
    };

    modalRef.result.then((result) => {
      this.commonService.loader.next(true);
      this.buildPlaylistService.deletePlaylist(this.userPlaylistData[index].PlaylistId).subscribe(data => {
        if (data.Status) {
          this.userPlaylistData.splice(index, 1);
        }
        else {
          this.commonService.showError(data.Message, "");
        }
        this.commonService.loader.next(false);
      });
    }, (reason) => {
    });
  }

  /**
   * select status
   * @param status
   * @response null
   */
  selectStatus(status: string = "") {
    this.selectedStatus = status;
    if (status == "1") {
      this.selectedStatus = false;
    }
    else if (status == "2") {
      this.selectedStatus = true;
    }
  }
}
