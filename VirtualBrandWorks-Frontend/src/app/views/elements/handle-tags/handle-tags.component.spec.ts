import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleTagsComponent } from './handle-tags.component';

describe('HandleTagsComponent', () => {
  let component: HandleTagsComponent;
  let fixture: ComponentFixture<HandleTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
