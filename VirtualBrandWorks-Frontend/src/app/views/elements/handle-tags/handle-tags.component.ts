import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { Tag } from 'src/app/shared/model/tag';

@Component({
  selector: 'app-handle-tags',
  templateUrl: './handle-tags.component.html',
  styleUrls: ['./handle-tags.component.scss']
})
export class HandleTagsComponent implements OnInit {

  tags_1: Tag[]; // tag list
  selectedTags_1: any = []; // selected tags
  tagList_1: any = [];
  @Output() sendTagsHandle = new EventEmitter<any>();
  @ViewChild("searchTagHandle", { static: true }) searchTagHandle: ElementRef; // search element reference

  constructor(private httpServices: HttpService, private commonServices: CommonService) { }

  ngOnInit(): void {
    this.commonServices.selectedTags_1.subscribe(data => {
      this.tagList_1 = data;
      this.selectedTags_1 = this.tagList_1 ? this.tagList_1 : this.selectedTags_1;
    })
    this.commonServices.searchDebounce(this.searchTagHandle.nativeElement).subscribe(searchString => {
      this.getTags(searchString);
    });
  }

  /**
   * get tags
   * @param null
   * @response object
   */
  getTags(searchString: string) {
    this.httpServices.GetTagSuggestion(searchString,'user').subscribe(data => {
      if (data.Status) {
        this.tags_1 = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * push tags
   * @param index
   * @response object
   */
  pushTags(index: number) {
    let checkTag = this.selectedTags_1.find(elem => elem.TagId == this.tags_1[index].TagId); // to check if it already exists
    if (!checkTag) {
      this.selectedTags_1.push(this.tags_1[index]);
    }
    this.sendTagsHandle.emit(this.selectedTags_1);
    this.tags_1 = [];
    this.searchTagHandle.nativeElement.value = "";
  }

  /**
   * remove tags
   * @param id
   * @response object
   */
  removeTag(id: number) {
    let index = this.selectedTags_1.indexOf(this.selectedTags_1.find(elem => elem.TagId == id));
    this.selectedTags_1.splice(index, 1);
    this.sendTagsHandle.emit(this.selectedTags_1);
  }

  @HostListener('document:click', ['$event'])
  closeList() {
    this.tags_1 = [];
  }

}
