import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';

@Component({
  selector: 'app-cancel-plan',
  templateUrl: './cancel-plan.component.html',
  styleUrls: ['./cancel-plan.component.scss']
})
export class CancelPlanComponent implements OnInit {
  @Input() data: any;
  constructor(private activeModal: NgbActiveModal, private commonServices: CommonService, private httpServices: HttpService) { }

  ngOnInit(): void {
  }

  /**
   * cancel
   * @param null
   * @response null
   */
  cancel() {
    this.httpServices.cancelPlan(this.data.PlanId).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Plan.Cancelled, "");
        this.activeModal.close();
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    })
  }

  /**
   * close
   */
  close() {
    this.activeModal.dismiss();
  }

}
