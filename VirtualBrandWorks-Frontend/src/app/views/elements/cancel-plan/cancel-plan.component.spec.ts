import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelPlanComponent } from './cancel-plan.component';

describe('CancelPlanComponent', () => {
  let component: CancelPlanComponent;
  let fixture: ComponentFixture<CancelPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
