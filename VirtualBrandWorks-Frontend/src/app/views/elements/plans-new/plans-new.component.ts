import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { Plan } from 'src/app/shared/model/plan';
import { Title } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ContactusComponent } from 'src/app/contactus/contactus.component';

@Component({
  selector: 'app-plans-new',
  templateUrl: './plans-new.component.html',
  styleUrls: ['./plans-new.component.scss']
})
export class PlansNewComponent implements OnInit {

  plans: Plan; // all the plans
  planType: number = 2; // type of plan, default 2 is monthly
  planCopy: any = []; // a copy of plan to get the older values of plan after sort
  selectedPlan: any = []; // selected plan object
  planTab:string = 'month-tab';
  planID: string = 'month';
  @Output() planData = new EventEmitter<any>();
  constructor(private httpService: HttpService, private titleService: Title, private modalService: NgbModal) {
    this.httpService.getPlans().subscribe(data => { // get all the plans
      if (data.Data) {
        this.planCopy = data.Data; 
        this.titleService.setTitle(this.planCopy[0].PlanName);
        this.plans = data.Data.filter(res => res.PlanType == 2
        );
      }
    });
    }

  ngOnInit(): void {
  }

  /**
   * change plan type
   * @param value 
   * @response json
   */
  changePlanType(value) {
    this.planType = value;
    this.planTab = value == 1 ? 'annual-tab' : 'month-tab';
    this.planID = value == 1 ? 'annual' : 'month';
    this.plans = this.planCopy.filter(res => res.PlanType === this.planType // filter according the toggle selected
    );
  }

  /**
   * select plan
   * @param plan
   * @response json 
   */
  selectPlan(plan: any) {
    this.selectedPlan = plan;
    this.planData.emit(plan); // emit the selected plan
  }

  contactus() { 
    const modalRef = this.modalService.open(ContactusComponent, { windowClass: 'w925', centered: true });   
  }

}
