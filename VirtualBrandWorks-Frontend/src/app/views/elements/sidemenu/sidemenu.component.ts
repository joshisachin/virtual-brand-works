import { Component, OnInit, NgZone } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

  // all the part is used for css and display purpose

  public innerWidth: any = window.innerWidth;
  private toggle: boolean = false;
  public showSideMenu: boolean = true;
  userId: string;
  userRole: string;

  constructor(private commonServices: CommonService, private ngZone: NgZone) { }

  ngOnInit() {
    window.onresize = (e) => {
      //ngZone.run will help to run change detection
      this.ngZone.run(() => {
        this.innerWidth = window.innerWidth;

      });
    };
    this.userId = localStorage.getItem("userId");
    this.userRole = localStorage.getItem("userRole");
  }

  /**
   * click event
   * @param null
   * @response null
   */
  clickEvent() {
    this.toggle = !this.toggle;
  }

  /**
   * toggle nav button
   * @param null
   * @response null
   */
  toggleNavBtn() {
    this.showSideMenu = !this.showSideMenu;
    let rightSide = this.commonServices.toggleRightSide.getValue();
    this.commonServices.toggleRightSide.next(!rightSide);
  }

  /**
   * resize the nav menu
   * @param null
   * @response null
   */
  resizeNav() {
    if (this.innerWidth < 1024) {
      return true
    }
    return false
  }
}
