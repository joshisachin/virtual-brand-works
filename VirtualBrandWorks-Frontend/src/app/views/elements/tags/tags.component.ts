import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { Tag } from 'src/app/shared/model/tag';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {

  tags: Tag[]; // tag list
  selectedTags: any = []; // selected tags
  tagList: any = [];
  @Output() sendTags = new EventEmitter<any>();
  @ViewChild("searchTag", { static: true }) searchTag: ElementRef; // search element reference
  routes: boolean = false;

  constructor(private httpServices: HttpService, private commonServices: CommonService, private route:Router) { }

  ngOnInit(): void {
    this.commonServices.selectedTags.subscribe(data => {
      this.tagList = data;
      this.selectedTags = this.tagList ? this.tagList : this.selectedTags;
    })
    this.commonServices.searchDebounce(this.searchTag.nativeElement).subscribe(searchString => {
      this.getTags(searchString);
    });
    this.routes = this.route.url.includes('/teams/create-team') ? true : false;
  }

  /**
   * get tags
   * @param null
   * @response object
   */
  getTags(searchString: string) {
    this.httpServices.GetTagSuggestion(searchString, 'tags').subscribe(data => {
      if (data.Status) {
        this.tags = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * push tags
   * @param index
   * @response object
   */
  pushTags(index: number) {
    let checkTag = this.selectedTags.find(elem => elem.TagId == this.tags[index].TagId); // to check if it already exists
    if (!checkTag) {
      this.selectedTags.push(this.tags[index]);
    }
    this.sendTags.emit(this.selectedTags);
    this.tags = [];
    this.searchTag.nativeElement.value = "";
  }

  /**
   * remove tags
   * @param id
   * @response object
   */
  removeTag(id: number) {
    let index = this.selectedTags.indexOf(this.selectedTags.find(elem => elem.TagId == id));
    this.selectedTags.splice(index, 1);
    this.sendTags.emit(this.selectedTags);
  }

  @HostListener('document:click', ['$event'])
  closeList() {
    this.tags = [];
  }

}
