import { Component, OnInit, Input } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { timer, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from "../../../../environments/environment"
import { CompanyContentService } from '../../company-content/company-content.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.scss']
})
export class FolderComponent implements OnInit {

  @Input() folderType: string;
  @Input() isPlaylist: boolean = false;
  getFolderApi: string;
  getSelectedFolderApi: string;
  saveSelectedFolderApi: string;
  folders: any = []; // all the folders data
  dynamicFolder: any = []; // dynamic folder data
  folderPopUp: any; // data to send to for folder popup
  showFolderModal: boolean = false; // toggle for folder modal
  singleClick: boolean = true; // to check the single click to prevent it during double click
  page: number = 1; // pagination starts from
  folderCount: number = environment.PAGINATE_FETCH; // number of items in request
  searchData: string = "";
  static stopScroll: BehaviorSubject<boolean> = new BehaviorSubject(false); // to stop the api on scroll when all the results got fetched

  constructor(private httpService: HttpService, private commonServices: CommonService, private router: Router, private companyContentService: CompanyContentService) {
    this.commonServices.loader.next(true);
  }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.folderType == "master") {
      this.getFolderApi = "getAllFoldersData";
      this.getSelectedFolderApi = "getSelectedFolders";
      this.saveSelectedFolderApi = "saveSelectedFolder";
    }
    else {
      this.getFolderApi = "getCompanyContentFolder";
      this.getSelectedFolderApi = "getCompanyContentSelectedFolder";
      this.saveSelectedFolderApi = "saveCompanySelectedFolder";
    }
    if (this.isPlaylist) {
      this.getSelectedFolderApi = "getPlaylistFoldersFooterData";
    }
    this.commonServices.getSelectedFolders(this.getSelectedFolderApi);
    this.getSearchDataForFolder();
    this.getAllFolders();
  }

  /**
   * get search data
   * @param null
   * @response null
   */
  getSearchDataForFolder() {
    this.commonServices.searchDataForFolder.subscribe(data => {
      this.searchData = data;
    })
  }

  /**
   * to get the data of all the folders
   * @param null
   */
  getAllFolders() {
    this.httpService[this.getFolderApi](this.searchData, "", this.page, this.folderCount).subscribe(data => {
      if (data.Status) {
        let dynamicFolder = data.Data.filter((res) => { // separating the data of dynamic folder from rest of the folders
          return res.IsDynamicFolder == true;
        });
        let restFolders = data.Data.filter((res) => {
          return res.IsDynamicFolder == false;
        });
        let tempValue: any = this.commonServices.folders.getValue();
        this.commonServices.folders.next(tempValue.concat(restFolders)); // assining to behaviour subject so that state is maintained all over the app
        this.commonServices.folders.subscribe(data => {
          this.folders = data;
        });
        if (dynamicFolder.length > 0) {
          this.commonServices.dynamicFolder.next(dynamicFolder[0])// assigning dynamic folder object
          this.commonServices.dynamicFolder.subscribe(data => {
            this.dynamicFolder = data;
          });
        }
        this.commonServices.loader.next(false);
      }
      else {
        this.commonServices.showError(data.Message, "");
        this.commonServices.loader.next(false);
      }
      if (data.Data.length === 1) {
        FolderComponent.stopScroll.next(true);
        this.page = 1;
      }
    });
  }

  ngOnDestroy() {
    this.commonServices.folders.next([]);
    FolderComponent.stopScroll.next(false);
  }

  /**
   * to drag and drop the folder
   * @param event 
   * @response array
   */
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.folders, event.previousIndex, event.currentIndex);
  }

  /**
   * to select and unselect the folder and send the value
   * @param folder
   */
  selectFolder(index: number) {
    let schedule = timer(250);
    this.singleClick = true; // enable the single click
    schedule.subscribe(() => { // delaying the click event to check if the user has double click
      if (this.singleClick) {
        if (index >= 0) {
          this.commonServices.loader.next(true);
          this.folders[index].IsSelectedFolder = !this.folders[index].IsSelectedFolder;
          this.commonServices.folders.next(this.folders);

          this.updateSelectedFolder(this.folders[index].FolderId, this.folders[index].IsSelectedFolder);
        }
        else {
          this.commonServices.loader.next(true);
          this.dynamicFolder.IsSelectedFolder = !this.dynamicFolder.IsSelectedFolder;
          this.updateSelectedFolder(this.dynamicFolder.FolderId, this.dynamicFolder.IsSelectedFolder);
          this.commonServices.dynamicFolder.next(this.dynamicFolder);
        }
      }
    });
  }

  /**
   * common function to update the selected folder for folder and dynamic folder
   * @param id
   * @param selected 
   */
  updateSelectedFolder(id: number, selected: boolean) {
    this.httpService[this.saveSelectedFolderApi](id, selected).subscribe(data => {
      if (data.Status) {
        this.commonServices.getSelectedFolders(this.getSelectedFolderApi);
        this.commonServices.loader.next(false);
      }
      else {
        this.commonServices.showError(data.Message, "");
        this.commonServices.loader.next(false);
      }
    });
  }

  /**
   * send folder id and toggle modal
   * @param index
   * @response null
   */
  toggleFolderModal(index) {
    if (index >= 0) { // for all folders except the dynamic
      this.folderPopUp = this.folders[index];
    }
    else {
      this.folderPopUp = this.dynamicFolder; // for dynamic folder
    }
    this.showFolderModal = true;
  }

  /**
   * details open of all folders and dynmaic folder
   * @param index
   * @response null
   */
  details(index: number) {
    let route: string = "/company-content/";
    if (this.folderType == "master") {
      route = "/masterlibrary/";
    }
    let name = "";
    let id: number = 0;
    if (index >= 0) {
      name = this.folders[index].Name;
      id = this.folders[index].FolderId;
    }
    else {
      name = this.dynamicFolder.Name;
      id = this.dynamicFolder.FolderId;
    }
    this.singleClick = false; // stopping the single click
    if (!this.isPlaylist) {
      this.router.navigate([route + name + "/" + id]);
    }
  }

  /**
   * load folders on scroll
   * @param null
   * @respobse object
   */
  onScroll() {
    if (FolderComponent.stopScroll.getValue() == false) {
      this.page = this.page + 1;
      this.getAllFolders();
    }
  }

  /**
   * edit folder
   * @param index
   * @response null
   */
  editFolder(index: number) {
    if (index >= 0) {
      this.folders[index].IsEditable = true;
    }
    else {
      this.commonServices.showError(API_MESSAGES.CommonError.message, "");
    }
  }

  /**
   * update folder
   * @param index
   * @response null
   */
  updateFolder(index: number) {
    if (index >= 0) {
      if (this.folders[index].ShortName == "" || this.folders[index].ShortName.length != 2) {
        this.commonServices.showError(API_MESSAGES.UpadateFolder.shortNameError, "");
        return;
      }
      if (this.folders[index].Name === "") {
        this.commonServices.showError(API_MESSAGES.UpadateFolder.nameError, "");
        return;
      }
      this.commonServices.loader.next(true);
      this.companyContentService.updateTeam(this.folders[index].Name.trim(), this.folders[index].FolderId, this.folders[index].ShortName.trim()).subscribe(data => {
        if (data.Status) {
          this.commonServices.showSuccess(API_MESSAGES.UpadateFolder.successMessage, "");
          this.folders[index].IsEditable = false;
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
      });
    }
    else {
      this.commonServices.showError(API_MESSAGES.CommonError.message, "")
    }
  }
}
