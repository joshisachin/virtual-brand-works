import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { FolderContent } from 'src/app/shared/model/folder-content';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { CompanyContentService } from '../../company-content/company-content.service';

@Component({
  selector: 'app-content-type-based',
  templateUrl: './content-type-based.component.html',
  styleUrls: ['./content-type-based.component.scss']
})
export class ContentTypeBasedComponent implements OnInit {

  @Input() folderType: string;
  @Input() folderId: number;
  @Input() contentId: number;

  type: number = 2;
  getFolderDetailsApi: string;
  folderDetails: FolderContent[];
  activeId: number;

  constructor(private httpServices: HttpService, private commonServices: CommonService, private router: Router, private companyService: CompanyContentService) { }

  ngOnInit(): void {
    if (this.folderType == "master") {
      this.getFolderDetailsApi = "getMasterContentByType"; // set api type based
    }
    else {
      this.getFolderDetailsApi = "getCompanyContentByType"; // set api type based
    }
    this.getDetails();
  }

  /**
   * get folder details type based
   * @param null
   * @response null
   */
  getDetails() {
    this.httpServices[this.getFolderDetailsApi](this.folderId, this.contentId, this.type).subscribe(data => {
      if (data.Status) {
        this.folderDetails = data.Data;
        if (this.type == 2) {
          this.folderDetails.map(data => {
            data.ContentUrl = data.ContentURL;
            data.ThumbnailUrl = data.ThumbnailURL;
            this.commonServices.formatVideoThumbnailUrl(data);
          });
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * tab change
   * @param changeEvent
   */
  onNavChange(changeEvent: NgbNavChangeEvent) {
    this.type = changeEvent.nextId;
    this.getDetails();
  }

  /**
   * open content details
   * @param index
   * @response null
   */
  openContentDetails(index: number) {
    let mainRoute: string = "/company-content/"
    if (this.folderType == "master") {
      mainRoute = "/masterlibrary/";
    }
    this.folderId = this.folderDetails[index].FolderId;
    this.contentId = this.folderDetails[index].ContentId;
    this.type = 2;
    this.activeId = 2;
    this.ngOnInit();
    if (this.folderDetails[index].MimeType && this.folderDetails[index].MimeType.match("video")) {
      this.companyService.changeVideo.next(true);
    }
    this.router.navigate([mainRoute + this.folderDetails[index].FolderName + "/" + this.folderDetails[index].FolderId + "/" + this.folderDetails[index].ContentName + "/" + this.folderDetails[index].ContentId]);
  }

}
