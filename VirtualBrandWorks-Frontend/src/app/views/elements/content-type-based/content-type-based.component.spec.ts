import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentTypeBasedComponent } from './content-type-based.component';

describe('ContentTypeBasedComponent', () => {
  let component: ContentTypeBasedComponent;
  let fixture: ComponentFixture<ContentTypeBasedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentTypeBasedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentTypeBasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
