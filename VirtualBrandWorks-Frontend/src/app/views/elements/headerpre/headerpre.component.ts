import { Component, OnInit, HostListener, ViewChild, ElementRef, Input } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';

@Component({
  selector: 'app-headerpre',
  templateUrl: './headerpre.component.html',
  styleUrls: ['./headerpre.component.scss']
})
export class HeaderpreComponent implements OnInit {
  showSignup: boolean = true; // to toggle the links of login and signup in header
  isLoginPage: boolean = false; // to check if login page
  isHomePage: boolean = true; // to check if home page
  selectedHeader: string; // to assign value of selected item menu
  isBlackBgClass: boolean = false;
  page: string = "";
  deviceInfo = null;
  isDesktop: boolean = false;
  showHideClass: string = 'hide';


  @HostListener('window:scroll', [])
  onWindowScroll() {
    console.log("iyfuie");
    if (this.router.url.match('/home')){
      if (window.pageYOffset > 100) {
        console.log("scroll");
        // document.getElementsByClassName('top-header')[0].classList.add('black-bg');
        this.isBlackBgClass = true;
      }
      else{
        // document.getElementsByClassName('top-header')[0].classList.remove('black-bg');
        this.isBlackBgClass = false;
      }
    }
    
  }
  
  constructor(
    private router: Router,
    private commonServices: CommonService,
    private deviceService: DeviceDetectorService,
    private modalService: NgbModal
  ) {
    this.epicFunction();
    this.commonServices.getCurrentRoute(); // enable the current route function
    this.commonServices.currentUrl.subscribe(data => { // get the current url value
      this.showHideClass = 'hide';
      this.isLoginPage = false;
      this.isHomePage = true;
      this.showSignup = true;
      this.page = data.toString();
      console.log("DATA", data)

      if (data === "/signup") {
        this.showSignup = false;
        this.isBlackBgClass = false;
      }
      if (data === "/login") {
        this.isLoginPage = true;
        this.isBlackBgClass = false;
      }
      if (!data.match("/home")) {
        this.isHomePage = false;
      }
      // if (data.match("subscription")) {
      //   this.selectedHeader = "subscription";
      //   window.scroll(0, window.scrollY - 109);
      // }
      // if (data.match("writetous")) {
      //   this.selectedHeader = "writetous";
      //   window.scroll(0, document.body.scrollHeight);
      // }
      // if (data.match("overview")) {
      //   this.selectedHeader = "overview";
      //   window.scroll(0, window.scrollY - 109);
      // }
    });
  }

  ngOnInit() { }

  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    const isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();
    // console.log(isMobile);  // returns if the device is a mobile device (android / iPhone / windows-phone etc)
    // console.log(isTablet);  // returns if the device us a tablet (iPad etc)
    // console.log(isDesktopDevice); // returns if the app is running on a Desktop browser.

    this.isDesktop = isDesktopDevice;
  }

  toggleNav(){
    if(this.showHideClass == 'show'){
      this.showHideClass = 'hide';
    } else if (this.showHideClass == 'hide'){
      this.showHideClass = 'show';
    }
  }

  scrollToTop(){
    window.scroll(0,0);
    if(this.router.url == '/signup')
    this.routeTo('/home',true);
    else{
      this.router.navigateByUrl('/home')
    }
  }

  /**
   * route to different routes
   * @param route 
   */
  routeTo(route: string,checkForm ?: boolean) {
    if (checkForm) {
      let data = this.getConfirmation("Are you sure, your Sign Up information will be lost");
      data.then(res => {
        this.router.navigateByUrl(route); // using navigate by url to navigate to the section of page on clicking link by respective id
      },
        err => { }
      )
    }
    else {
      this.router.navigateByUrl(route);
    }
  }

  getConfirmation(title: string) {
    const modalRef = this.modalService.open(ConfirmationAlertComponent, { centered: true });
    let data: any = {"title": title}
    modalRef.componentInstance.data = data;
    return modalRef.result;
  }
  

  /**
   * change the menu item on scroll
   */
  // @HostListener('window:scroll', ['$event'])
  // checkOffsetTop() {
  //   if (window.scrollY >= 448 && window.scrollY <= 1200) {
  //     this.selectedHeader = "overview";
  //   }
  //   else if (window.scrollY >= 1200 && window.scrollY < 1900) {
  //     this.selectedHeader = "subscription";
  //   }
  //   else if (window.scrollY >= 1650) {
  //     this.selectedHeader = "writetous";
  //   }
  //   // if (this.router.url.match("home")) {
  //   //   this.router.navigate(["/home"]);
  //   // }
  // }

}
