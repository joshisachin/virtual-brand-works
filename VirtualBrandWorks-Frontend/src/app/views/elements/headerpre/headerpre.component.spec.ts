import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderpreComponent } from './headerpre.component';

describe('HeaderpreComponent', () => {
  let component: HeaderpreComponent;
  let fixture: ComponentFixture<HeaderpreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderpreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderpreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
