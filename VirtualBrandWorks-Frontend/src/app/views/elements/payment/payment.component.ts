import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { environment } from "../../../../environments/environment";
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
declare var Stripe: any;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  @Input() planData: any = [];
  @Input() personalInfo: any = [];

  constructor(private httpService: HttpService, private commonService: CommonService) {
  }

  ngOnInit() { }

  /**
   * create checkout session for payment
   * @param null
   * @response null
   */
  createCheckoutSession() {
    let sendData = {
      "PlanId": this.planData.PlanId,
      "Email": this.personalInfo.Email,
      "Subscription": this.planData.PlanName,
      "Amount": this.planData.Price,
      "BillingCycle": this.planData.PlanType,
      "FirstName": this.personalInfo.FirstName,
      "LastName": this.personalInfo.LastName,
      "SuccessUrl": environment.SUCCESS_URL,
      "CancelUrl": environment.CANCEL_URL,
    }
    this.httpService.getSessionIdForPayment(sendData).subscribe(data => {
      this.makePayment(data["SessionId"]);
      localStorage.setItem("sessionId", data["SessionId"]);
      localStorage.setItem("name", this.personalInfo.FirstName + " " + this.personalInfo.LastName);
      localStorage.setItem("amount", this.planData.Price);
    })
  }

  /**
   * make payment
   * @param sessionId 
   * @response message
   */
  makePayment(sessionId: string) {
    let stripe = Stripe('pk_test_4FSrCf5onLCLEqcayRaD2Aki00lhVxVytM');
    stripe.redirectToCheckout({
      sessionId: sessionId
    }).then(() => {
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer
      // using `result.error.message`.
      this.commonService.showError(API_MESSAGES.CommonError.message, "");
    });
  }
}
