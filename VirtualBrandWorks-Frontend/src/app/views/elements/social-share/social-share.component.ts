import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-social-share',
  templateUrl: './social-share.component.html',
  styleUrls: ['./social-share.component.scss']
})
export class SocialShareComponent implements OnInit {
  environment: any = environment;
  @Input() contentId: number = 0;
  @Input() type: number = 0;
  constructor() { }

  ngOnInit(): void {
  }

}
