import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  loading: boolean;
  constructor(private commonServices: CommonService) {
    this.commonServices.loader.subscribe((value) => {
      this.loading = value;
    });
  }

  ngOnInit(): void {
  }



}
