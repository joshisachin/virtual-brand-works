import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ManageLocation } from 'src/app/shared/model/manage-location';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { ManageLocationService } from '../../manage-location/manage-location.service';

@Component({
  selector: 'app-location-selector',
  templateUrl: './location-selector.component.html',
  styleUrls: ['./location-selector.component.scss']
})
export class LocationSelectorComponent implements OnInit {

  locations: ManageLocation[];
  selectedLocation: string = "";
  @ViewChild("searchLoc", { static: true }) searchLoc: ElementRef;
  @Input() customStyle: boolean = false; 
  
  constructor(private commonServices: CommonService, private httpService: HttpService, private manageLocationService: ManageLocationService) { }

  ngOnInit(): void {
    // this.commonServices.searchDebounce(this.searchLoc.nativeElement).subscribe(searchString => {
    //   if (searchString === "") {
    //     this.locations = [];
    //     this.commonServices.selectedLocation.next({});
    //   }
    //   else {
    //     this.getLocation(searchString);
    //   }
    // });
    this.getLocation('');
  }

  /**
   * get location
   * @param value
   * @response object
   */
  getLocation(value: string) {
    let formData = {};
    formData["CountryId"] = "";
    formData["StateId"] = "";
    formData["CityId"] = "";
    formData["LocationName"] = value; 
    this.manageLocationService.getLocation(formData).subscribe(data => {
      this.locations = data.Data;
    })
  }

  /**
   * get selected location
   * @param index
   * @response object
   */
  getSelectedLocation(index) {
    if(index!= -1){
    this.commonServices.selectedLocation.next(this.locations[index]);
    this.commonServices.selectedLocation.subscribe(data => {
      this.selectedLocation = data.LocationName;
    });
  }
  else{
    this.commonServices.selectedLocation.next({});
  }
    // this.locations = [];
  }

}
