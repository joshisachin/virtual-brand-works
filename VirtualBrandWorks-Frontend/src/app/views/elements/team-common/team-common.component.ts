import { Component, Input, OnInit } from '@angular/core';
import { TeamService } from '../../teams/team.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { Team } from 'src/app/shared/model/team';
import { TeamDetails } from 'src/app/shared/model/team-details';
import { Trainer } from 'src/app/shared/model/trainer';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationAlertComponent } from 'src/app/shared/modals/confirmation/confirmation-alert.component';
import { Router, NavigationExtras } from '@angular/router';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { ViewContentComponent } from '../../elements/view-content/view-content.component';
import { CommonPopupComponent } from 'src/app/shared/modals/common-popup/common-popup.component';

@Component({
  selector: 'app-team-common',
  templateUrl: './team-common.component.html',
  styleUrls: ['./team-common.component.scss']
})
export class TeamCommonComponent implements OnInit {

  teams: Team[]; // teams object
  teamId: number; // selected team id
  teamDetails: TeamDetails; // team detais object
  trainers: Trainer[]; // trainers
  trainerId: number; // selected trainer
  isReverse: boolean = false; // for reverse sort
  @Input() IsTaggedContent: boolean;
  @Input() teamTypeSelection: any = { isCreated: true, IsAssigned: true, IsContent: false };
  contentData:any;
  roleID: number;
  constructor(
    public teamService: TeamService,
    private commonServices: CommonService,
    private modalService: NgbModal,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.roleID = parseInt(localStorage.getItem("roleId"));
    if (this.teamTypeSelection.isCreated ||
      this.teamTypeSelection.IsAssigned) {
      this.getTeams();
    }

    if (this.teamTypeSelection.IsContent){
      this.getContest();
    }

    console.log("teamtype", this.teamTypeSelection);
  }

  getContest(){
    this.teamService.getContents().subscribe(response => {
      console.log("DATA", response);
      if (response.hasOwnProperty('Data')){
        this.contentData = response['Data'];
        this.formatUrl(response['Data']);
        console.log("contentData", this.contentData);
      }
      
      // if (data.Status) {
      //   this.teams = data.Data;
      //   this.getTeamDetail();
      //   if (this.teamService.tabIndex === 2) {
      //     this.getTrainers();
      //   }
      //   this.commonServices.loader.next(false);
      // }
      // else {
      //   this.teams = [];
      //   this.commonServices.showError(data.Message, "");
      //   this.commonServices.loader.next(false);
      // }
    });
  }

  /**
   * format data by the url
   * @param gameData
   * @response array
   */
   formatUrl(gameData: any) {
    gameData.map((data, i) => {
      data.CanDelete = true;
      if (new Date(data.EndDate).getTime() < new Date().getTime()) {
        data.CanDelete = false;
      }
      this.commonServices.formatVideoThumbnailUrl(data);
      if(data.ChallengeEntry){
      this.commonServices.formatVideoThumbnailUrl(data.ChallengeEntry);
      }
      if (gameData.length -1 == i) {
        this.contentData = gameData;
      }

      data.CanJoin = false;
      if (new Date() >= new Date(data.StartDate) && new Date() <= new Date(data.EndDate)) {
        data.CanJoin = true;
      }
    });
  }

  /**
   * get team detail
   * @param index
   * @response object
   */
  getTeamDetail(index: number = 0) {
    this.commonServices.loader.next(true);
    if (this.teams.length > 0) {
      this.teamId = this.teams[index].TeamId;
      this.teamService.getTeamDetails(this.teamId).subscribe(data => {
        if (data.Status) {
          this.teamDetails = data.Data;
          this.commonServices.loader.next(false);
        }
        else {
          this.commonServices.showError(data.Message, "");
          this.commonServices.loader.next(false);
        }
      });
    }
    else {
      this.teamDetails = null
      this.commonServices.showError(API_MESSAGES.Team.noTeamError, "");
    }
  }

  /**
   * get teams
   * @param null
   * @response object
   */
  getTeams(isCompanyAdmin: boolean = true, trainerId: number = 0) {
    this.commonServices.loader.next(true);
    if ((this.teamService.tabIndex === 1 && localStorage.getItem("roleId") == "3") || this.teamService.tabIndex === 2) {
      isCompanyAdmin = false;
    }
    this.teamService.getTeams(isCompanyAdmin, trainerId, this.IsTaggedContent).subscribe(data => {
      if (this.teamService.tabIndex === 2) {
        this.getTrainers();
      }
      if (data.Status) {
        this.teams = data.Data;
        this.getTeamDetail();
        this.commonServices.loader.next(false);
      }
      else {
        this.teams = [];
        this.commonServices.showError(data.Message, "");
        this.commonServices.loader.next(false);
      }
    });
  }

  /**
   * get trainers
   * @param null
   * @response object
   */
  getTrainers() {
    this.teamService.getTrainers().subscribe(data => {
      if (data.Status) {
        this.trainers = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

  /**
   * get trainer teams
   * @param trainerId
   * @response null
   */
  getTrainerTeams(trainerId: number) {
    this.trainerId = trainerId;
    this.getTeams(false, trainerId);
  }

  /**
   * sort
   * @param type
   * @response object
   */
  sort(type: string = "") {
    this.commonServices.performSorting(this.teamDetails.Users, type);
    if (this.isReverse) {
      this.teamDetails.Users.reverse();
    }
    this.isReverse = !this.isReverse;
  }

  /**
   * delete team
   * @param null
   * @response null
   */
  deleteTeam() {
    if (this.teamDetails.TeamId) {
      const modalRef = this.modalService.open(ConfirmationAlertComponent, {
        windowClass: 'modalwidth'
      });

      modalRef.componentInstance.data = {
        "title": "Are you sure you want to delete this team?"
      };

      modalRef.result.then(() => {
        this.teamService.deleteTeam(this.teamDetails.TeamId).subscribe(data => {
          if (data.Status) {
            let index = this.teams.indexOf(this.teams.find(elem => elem.TeamId == this.teamDetails.TeamId));
            this.teams.splice(index, 1);
            this.teamDetails = undefined;
            this.getTeamDetail(0);
            this.commonServices.showSuccess(API_MESSAGES.DeleteTeam.successMessage, "");
          }
          else {
            this.commonServices.showError(data.Message, "");
          }
        });
      });
    }
  }

  /**
   * edit team
   */
  editTeam() {
    if (this.teamDetails) {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          id: this.teamDetails.TeamId,
          name: this.teamDetails.TeamName,
          handleName: this.teamDetails.HandleName
        }
      };
      this.router.navigate(["/teams/create-team"], navigationExtras);
    }
  }

  /**
   * open video
   * @param index
   * @response null
   */
  openVideo(index: number,content) {
    let isVideo = false;
    if(content.MimeType?.split('/')[0] == 'video' || content.IsYoutubeContent || content.IsFacebookContent){
      isVideo = true;
    }
    console.log("DATA", this.contentData[index]);
    this.contentData[index].ContentId = this.contentData[index].ContestId;
    const modalRef = this.modalService.open(ViewContentComponent, { centered: true });
    modalRef.componentInstance.data = this.contentData[index];
    modalRef.componentInstance.isVideo = isVideo;
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

   /**
   * open rules
   * @param index
   * @response null
   */
    openRules(index: number) {
      const modalRef = this.modalService.open(CommonPopupComponent, {
        windowClass: 'modalwidth'
      });
      modalRef.componentInstance.data = {
        "title": "Rules",
        "subTitle": "",
        "message": this.contentData[index].Rules,
        "buttonOne": "",
        "buttonTwo": ""
      };
      modalRef.result.then((result) => {
      }, (reason) => {
      });
    }

}
