import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamCommonComponent } from './team-common.component';

describe('TeamCommonComponent', () => {
  let component: TeamCommonComponent;
  let fixture: ComponentFixture<TeamCommonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamCommonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
