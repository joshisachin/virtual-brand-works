import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistFoldersComponent } from './playlist-folders.component';

describe('PlaylistFoldersComponent', () => {
  let component: PlaylistFoldersComponent;
  let fixture: ComponentFixture<PlaylistFoldersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistFoldersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistFoldersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
