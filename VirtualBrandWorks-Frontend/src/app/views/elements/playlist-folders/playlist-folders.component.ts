import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';

@Component({
  selector: 'app-playlist-folders',
  templateUrl: './playlist-folders.component.html',
  styleUrls: ['./playlist-folders.component.scss']
})
export class PlaylistFoldersComponent implements OnInit {

  selectedMasterFolders: any = [];
  selectedCompanyFolders: any = [];
  @Input() folderType: string;
  saveSelectedFolderApi: string;
  constructor(private commonServices: CommonService, private httpService: HttpService) { }

  ngOnInit(): void {
    this.getSelectedFolders();
  }

  ngOnChanges() {
    this.getSelectedFolders();
  }

  /**
   * get selected folders
   * @param null
   * @response null
   */
  getSelectedFolders() {
    if (this.folderType == "master") {
      this.saveSelectedFolderApi = "saveSelectedFolder";
    }
    else {
      this.saveSelectedFolderApi = "saveCompanySelectedFolder";
    }
    this.commonServices.selectedFolders.subscribe(data => { // object of selected folders
      if (data) {
        this.selectedMasterFolders = data.filter(element => element.IsMasterFolder == true);
        this.selectedCompanyFolders = data.filter(element => element.IsMasterFolder == false);
      }
    });
  }

  /**
   * unselect a folder
   * @param index 
   */
  unselectFolder(index: number, isMaster: boolean = false) {
    this.commonServices.loader.next(true);
    let selectedFolders: any = isMaster ? this.selectedMasterFolders : this.selectedCompanyFolders;
    let saveSelectedFolderApi: string = isMaster ? "saveSelectedFolder" : "saveCompanySelectedFolder";
    this.httpService[saveSelectedFolderApi](selectedFolders[index].FolderId, false).subscribe(data => {
      if (data.Status) {
        this.getPlaylistFooterFolders();
        if (selectedFolders[index].IsDynamicFolder) {
          let dynamicFolder = this.commonServices.dynamicFolder.getValue();
          if (dynamicFolder.FolderId === selectedFolders[index].FolderId) {
            dynamicFolder.IsSelectedFolder = false;
            this.commonServices.dynamicFolder.next(dynamicFolder);
          }
        }
        else {
          let arr = this.commonServices.folders.getValue(); // get the array where changes need to be done
          let num = arr.findIndex((arr) => { // get the index where the changes need to be done
            return arr["FolderId"] == selectedFolders[index].FolderId; // do the changes
          });
          if (num >= 0) { // to check if the unselected folders exists in our current object of folders displaying
            arr[num].IsSelectedFolder = false;
            this.commonServices.folders.next(arr);
          }
        }
        this.commonServices.loader.next(false);
      }
    });
  }

  /**
   * get playlist footer folders
   * @param null
   * @response null
   */
  getPlaylistFooterFolders() {
    this.httpService.getPlaylistFoldersFooterData().subscribe(res => {
      if (res.Status) {
      }
      else {
        this.commonServices.showError(res.Message, "")
      }
      this.commonServices.selectedFolders.next(res.Data);
    });
  }
}
