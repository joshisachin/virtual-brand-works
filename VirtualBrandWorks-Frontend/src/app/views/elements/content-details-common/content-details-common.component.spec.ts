import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentDetailsCommonComponent } from './content-details-common.component';

describe('ContentDetailsCommonComponent', () => {
  let component: ContentDetailsCommonComponent;
  let fixture: ComponentFixture<ContentDetailsCommonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentDetailsCommonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentDetailsCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
