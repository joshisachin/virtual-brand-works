import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/shared/services/httpservice/httpservice.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { Folder } from 'src/app/shared/model/folder';
import { FolderContent } from 'src/app/shared/model/folder-content';
import { CompanyContentService } from '../../company-content/company-content.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateQuizComponent } from 'src/app/shared/modals/quiz/create-quiz/create-quiz.component';
import { ViewQuizComponent } from 'src/app/shared/modals/quiz/view-quiz/view-quiz.component';

@Component({
  selector: 'app-content-details-common',
  templateUrl: './content-details-common.component.html',
  styleUrls: ['./content-details-common.component.scss']
})
export class ContentDetailsCommonComponent implements OnInit {
  @Input() folderType: string;
  getContentApi: string;
  contentDetails: FolderContent;
  folder: Folder;
  type: string;
  contentUrl: SafeUrl;
  video: ElementRef;
  videoC: any;
  @ViewChild("videoContainer", { static: false }) set content(video: ElementRef) {
    if (video) {
      this.videoC = video;
    }
  }
  constructor(private route: ActivatedRoute, private httpServices: HttpService, private commonServices: CommonService, private companyServices: CompanyContentService, private domSanitizer: DomSanitizer, private modalService: NgbModal) {
    this.commonServices.loader.next(true);
  }

  ngOnInit(): void {
    if (this.folderType == "master") {
      this.getContentApi = "getMasterContentDetails"; // set api for master folder
    }
    else {
      this.getContentApi = "getCompanyContentDetails"; // set api for company content folder
    }
    this.route.params.subscribe(params => {
      if (params.idNew) {
        this.getDetails(params.idNew);
      }
    });
  }

  /**
   * get company content details
   * @param id
   * @response object
   */
  getDetails(id: number) {
    this.httpServices[this.getContentApi](id).subscribe(data => {
      if (data.Status) {
        this.contentDetails = data.Data.Content;
        this.folder = data.Data.Folder;
        this.setType();
        if (this.type == "video") {
          this.companyServices.changeVideo.subscribe(data => {
            if (data && this.videoC) {
              this.videoC.nativeElement.load();
            }
          })
        }
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * create content
   * @param null
   * @response null
   */
  createContent() {
    this.companyServices.createContentPopup();
  }

  /**
   * set the type of content
   * @param null
   * @response null
   */
  setType() {
    this.contentDetails.ContentUrl = this.contentDetails.ContentURL;
    this.commonServices.formatVideoUrl(this.contentDetails);
    if (this.contentDetails.MimeType) {
      if (this.contentDetails.MimeType.match("image")) {
        this.type = "image";
      }
      else if (this.contentDetails.MimeType.match("video")) {
        this.type = "video";
      }
      else {
        this.type = "docs";
        this.contentDetails.ContentURL = "https://docs.google.com/gview?url=" + encodeURIComponent(this.contentDetails.ContentURL) + "&embedded=true";
        this.contentUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.contentDetails.ContentURL)
      }
    }
  }

  /**
   * publish content
   * @param null
   * @response null
   */
  publishContent() {
    if (this.contentDetails.ContentId) {
      this.commonServices.loader.next(true);
      this.companyServices.publishContent(this.contentDetails.ContentId).subscribe(data => {
        if (data.Status) {
          this.commonServices.showSuccess(API_MESSAGES.Content.publishMessage, "");
          this.ngOnInit();
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
        this.commonServices.loader.next(false);
      });
    }
  }

  /**
   * open create quiz modal
   * @param null
   * @response null
   */
  openCreateQuiz() {
    const modalRef = this.modalService.open(CreateQuizComponent, {});

    modalRef.componentInstance.contentId = this.contentDetails.ContentId;
    modalRef.result.then((result) => {
      this.contentDetails.QuizId = result;
    }, (reason) => { });
  }

  /**
   * view quiz
   * @param null
   * @response null
   */
  viewQuiz() {
    const modalRef = this.modalService.open(ViewQuizComponent, { centered: true });

    modalRef.componentInstance.quizId = this.contentDetails.QuizId;
    modalRef.componentInstance.folderType = this.folderType;
    modalRef.result.then((result) => {
    }, (reason) => {
      this.contentDetails.QuizId = null;
    });
  }

  /**
   * broadcast
   * @param null
   * @response null
   */
  broadcast() {
    this.commonServices.loader.next(true);
    this.companyServices.broadcast(this.contentDetails.ContentId, !this.contentDetails.IsBroadcasted).subscribe(data => {
      if (data.Status) {
        this.contentDetails.IsBroadcasted = !this.contentDetails.IsBroadcasted;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
      this.commonServices.loader.next(false);
    });
  }

  /**
   * change status
   * @param value
   * @response none
   */
  changeStatus(value: string) {
    this.companyServices.changeStatus(this.contentDetails.ContentId, value).subscribe(data => {
      if (data.Status) {
        this.commonServices.showSuccess(API_MESSAGES.Status.Success, "");
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }
}
