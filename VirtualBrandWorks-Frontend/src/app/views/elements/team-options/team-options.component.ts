import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { TeamService } from '../../teams/team.service';
import { Team } from 'src/app/shared/model/team';
import { TeamDetails } from 'src/app/shared/model/team-details';
import { Trainer } from 'src/app/shared/model/trainer';
import { API_MESSAGES } from 'src/app/shared/constants/message/message';

@Component({
  selector: 'app-team-options',
  templateUrl: './team-options.component.html',
  styleUrls: ['./team-options.component.scss']
})
export class TeamOptionsComponent implements OnInit {

  teams: Team[]; // teams object
  teamId: number; // selected team id
  teamDetails: TeamDetails; // team detais object
  trainers: Trainer[]; // trainers
  trainerId: number; // selected trainer
  isReverse: boolean = false; // for reverse sort
  teamType:any = {isCreated:true, IsAssigned: false, IsContent: false};
  roleID: number;

  constructor(
    private commonServices: CommonService,
    public teamService: TeamService
  ) { 
    
  }
  readioSelected = 'radio1';
  ngOnInit(): void {
    this.roleID = parseInt(localStorage.getItem("roleId"));
    if(this.roleID == 4){
      this.readioSelected = 'radio2';
    }
  }


  /**
   * get teams
   * @param null
   * @response object
   */
  getTeams(isCompanyAdmin: boolean = true, trainerId: number = 0, IsTaggedContent: boolean = true) {
    this.commonServices.loader.next(true);
    if ((this.teamService.tabIndex === 1 && localStorage.getItem("roleId") == "3") || this.teamService.tabIndex === 2) {
      isCompanyAdmin = false;
    }
    this.teamService.getTeams(isCompanyAdmin, trainerId, IsTaggedContent).subscribe(data => {
      if (data.Status) {
        this.teams = data.Data;
        this.getTeamDetail();
        if (this.teamService.tabIndex === 2) {
          this.getTrainers();
        }
        this.commonServices.loader.next(false);
      }
      else {
        this.teams = [];
        this.commonServices.showError(data.Message, "");
        this.commonServices.loader.next(false);
      }
    });
  }

  setTeamType(type){
    if (type == 'created'){
      this.teamType.isCreated = true;
      this.teamType.IsAssigned = false;
      this.teamType.IsContent = false;
    } else if (type == 'assigned'){
      this.teamType.isCreated = false;
      this.teamType.IsAssigned = true;
      this.teamType.IsContent = false;
    } if (type == 'content'){
      this.teamType.isCreated = false;
      this.teamType.IsAssigned = false;
      this.teamType.IsContent = true;
    }
  }

  /**
   * get team detail
   * @param index
   * @response object
   */
  getTeamDetail(index: number = 0) {
    this.commonServices.loader.next(true);
    if (this.teams.length > 0) {
      this.teamId = this.teams[index].TeamId;
      this.teamService.getTeamDetails(this.teamId).subscribe(data => {
        if (data.Status) {
          this.teamDetails = data.Data;
          this.commonServices.loader.next(false);
        }
        else {
          this.commonServices.showError(data.Message, "");
          this.commonServices.loader.next(false);
        }
      });
    }
    else {
      this.teamDetails = null
      this.commonServices.showError(API_MESSAGES.Team.noTeamError, "");
    }
  }

  /**
   * get trainers
   * @param null
   * @response object
   */
  getTrainers() {
    this.teamService.getTrainers().subscribe(data => {
      if (data.Status) {
        this.trainers = data.Data;
      }
      else {
        this.commonServices.showError(data.Message, "");
      }
    });
  }

}
