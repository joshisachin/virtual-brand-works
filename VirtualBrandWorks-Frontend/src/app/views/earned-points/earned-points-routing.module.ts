import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EarnedPointsComponent } from './earned-points.component';


const routes: Routes = [
	{
		path: '',
		component: EarnedPointsComponent,
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class EarnedPointsRoutingModule { }
