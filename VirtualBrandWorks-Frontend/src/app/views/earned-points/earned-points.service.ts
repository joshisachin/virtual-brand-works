import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';

@Injectable({
  providedIn: 'root'
})
export class EarnedPointsService {

  constructor(private http: HttpClient) { }

  /**
   * get earned points
   */
  getEarnedPoints(): Observable<any> {
    return this.http.get(API_NAME.GetEarnedPoints[0].name);
  }
}
