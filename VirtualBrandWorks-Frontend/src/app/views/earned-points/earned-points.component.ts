import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PointsLevelComponent } from 'src/app/shared/modals/points-level/points-level.component';
import { Point } from 'src/app/shared/model/point';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { EarnedPointsService } from './earned-points.service';

@Component({
  selector: 'app-earned-points',
  templateUrl: './earned-points.component.html',
  styleUrls: ['./earned-points.component.scss']
})
export class EarnedPointsComponent implements OnInit {

  toggleRightSide: boolean; // to expand right side when side menu shrinks
  points: Point;
  pointHistory: any = [];
  pointsHistory: any = [];
  
  constructor(private commonServices: CommonService, private earnedPointsService: EarnedPointsService, private modalService: NgbModal) {
    this.commonServices.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  }

  ngOnInit(): void {
    this.getEarnedPoints();
  }

  /**
   * get earned points
   * @param null
   * @response array
   */
  getEarnedPoints() {
    this.earnedPointsService.getEarnedPoints().subscribe(data => {
        if (data.Status) {
          this.points = data.Data;
          if(this.points.PointsHistory){
          this.pointsHistory = this.points.PointsHistory
          this.pointsHistory.sort((a,b) => (a.CreatedDate < b.CreatedDate) ? 1 : ((b.CreatedDate < a.CreatedDate) ? -1 : 0))
          }
        }
        else {
          this.commonServices.showError(data.Message, "");
        }
      });
  }

  /**
   * open points popup
   * @param null
   * @response null
   */
  openPointsPopup() {
    const modalRef = this.modalService.open(PointsLevelComponent, {
      windowClass: 'w925',
      centered: true
    });
    modalRef.componentInstance.data = {
    };
    modalRef.result.then((result) => {
    }, (reason) => { });
  }

}
