import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { EarnedPointsRoutingModule } from './earned-points-routing.module';
import { EarnedPointsService } from './earned-points.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    EarnedPointsRoutingModule
  ],
  providers: [
    EarnedPointsService
  ]
})
export class EarnedPointsModule { }
