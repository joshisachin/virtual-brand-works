import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  toggleRightSide: boolean; // to expand right side when side menu shrinks

  constructor(private commonServices: CommonService) {
    this.commonServices.toggleRightSide.subscribe(data => {
			this.toggleRightSide = data;
		});
  }

  ngOnInit() {
  }

}
