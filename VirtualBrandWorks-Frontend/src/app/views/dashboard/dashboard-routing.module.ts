import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { MyDashboardComponent } from './my-dashboard/my-dashboard.component';

const routes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		children: [
			{
				path: '',
				component: MyDashboardComponent,
			},
		]
	}
]
@NgModule({
	imports: [
        RouterModule.forChild(routes)
    ],
	exports: [
        RouterModule
    ]
})
export class DashboardRoutingModule { }
