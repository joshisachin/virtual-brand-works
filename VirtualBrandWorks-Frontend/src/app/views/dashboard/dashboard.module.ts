import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDashboardComponent } from './my-dashboard/my-dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { PlaylistService } from '../my-playlist/playlist.service';
import { NgCircleProgressModule } from 'ng-circle-progress';



@NgModule({
  declarations: [MyDashboardComponent, DashboardComponent],
  imports: [
    CommonModule,
    NgbModule,
    SharedModule,
    DashboardRoutingModule,
    NgCircleProgressModule.forRoot({})
  ],
  providers: [PlaylistService]
})
export class DashboardModule { }
