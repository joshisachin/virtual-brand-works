import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { CommonService } from 'src/app/shared/services/commonservices/commonservices.service';
import { PlayListData } from 'src/app/shared/model/created-playlist';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateComponent } from 'src/app/shared/modals/playlist/create/create.component';

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.scss']
})
export class MyDashboardComponent implements OnInit {

  playlistId: number;
  playlists: PlayListData[];
  constructor(private dashboardService: DashboardService, private commonService: CommonService, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getPlaylists();
  }

  /**
   * get playlists
   * @param null
   * @response null
   */
  getPlaylists() {
    this.commonService.loader.next(true);
    this.dashboardService.getAllPlaylists().subscribe(data => {
      if (data.Status) {
        this.playlists = data.Data;
        this.selectPlaylist();
      }
      else {
        this.commonService.showError(data.Message, "");
      }
      this.commonService.loader.next(false);
    });
  }

  /**
   * select playlist
   * @param index
   * @response null
   */
  selectPlaylist(index: number = 0) {
    this.playlists.map(playlist => {
      playlist.Selected = false;  
    });
    this.playlists[index].Selected = true;
    this.playlistId = this.playlists[index].PlaylistId;
  }

  /**
   * open create playlist
   * @param null
   * @response null
   */
  openCreatePlaylist() {
    const modalRef = this.modalService.open(CreateComponent, {
      ariaLabelledBy: 'modal-basic-title',
      windowClass: 'w925',
      centered: true
    });

    modalRef.result.then((result) => {
      localStorage.setItem("playlistId", result.toString());
      this.router.navigate(["/build-playlist"]);
    }, (reason) => {
    });
  }

}
