import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_NAME } from 'src/app/shared/constants/apiname/apiname';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  /**
   * get all the playlists
   */
  getAllPlaylists(): Observable<any> {
    return this.http.get(API_NAME.GetAllDashPlaylists[0].name);
  }
}
