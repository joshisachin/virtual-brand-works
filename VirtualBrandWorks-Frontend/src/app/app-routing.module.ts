import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { AuthGuard } from './shared/services/authguard/auth.guard';

const routerOption: ExtraOptions = {
	enableTracing: false,
	anchorScrolling: 'enabled',
};

const routes: Routes = [
	{
		path: '', 
		redirectTo: 'my-playlist', 
		pathMatch: 'full'
	},
	{
		path: "masterlibrary",
		loadChildren: () => import('./views/masterlib/masterlib.module').then(m => m.MasterlibModule),
		canActivate: [AuthGuard]
	},
	{
		path: "users",
		loadChildren: () => import('./views/users/users.module').then(m => m.UsersModule),
		canActivate: [AuthGuard]
	},
	{
		path: "my-profile",
		loadChildren: () => import('./views/users/users.module').then(m => m.UsersModule),
		canActivate: [AuthGuard]
	},
	{
		path: "teams",
		loadChildren: () => import('./views/teams/teams.module').then(m => m.TeamsModule),
		canActivate: [AuthGuard]
	},
	{
		path: "manage-location",
		loadChildren: () => import('./views/manage-location/manage-location.module').then(m => m.ManageLocationModule),
		canActivate: [AuthGuard]
	},
	{
		path: "company-content",
		loadChildren: () => import('./views/company-content/company-content.module').then(m => m.CompanyContentModule),
		canActivate: [AuthGuard]
	},
	{
		path: '',
		loadChildren: () => import('./views/sessions/sessions.module').then(m => m.SessionsModule)
	},
	{
		path: "change-password",
		loadChildren: () => import('./views/change-password/change-password.module').then(m => m.ChangePasswordModule),
		canActivate: [AuthGuard]
	},
	{
		path: "my-playlist",
		loadChildren: () => import('./views/my-playlist/my-playlist-main.module').then(m => m.MyPlaylistMainModule),
		canActivate: [AuthGuard]
	},
	{
		path: "build-playlist",
		loadChildren: () => import('./views/build-playlist/build-playlist.module').then(m => m.BuildPlaylistModule),
		canActivate: [AuthGuard]
	},
	{
		path: "dashboard",
		loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule),
		canActivate: [AuthGuard]
	},
	{
		path: "brandworks-play",
		loadChildren: () => import('./views/brandworks-play/brandworks-play.module').then(m => m.BrandworksPlayModule),
		canActivate: [AuthGuard]
	}, 
	{
		path: "broadcast",
		loadChildren: () => import('./views/broadcast/broadcast.module').then(m => m.BroadcastModule),
		canActivate: [AuthGuard]
	}, 
	{
		path: "earned-points",
		loadChildren: () => import('./views/earned-points/earned-points.module').then(m => m.EarnedPointsModule),
		canActivate: [AuthGuard]
	},
	{
		path: "prize-store",
		loadChildren: () => import('./views/prize-store/prize-store.module').then(m => m.PrizeStoreModule),
		canActivate: [AuthGuard]
	},
	{
		path: "leaderboard",
		loadChildren: () => import('./views/leaderboard/leaderboard.module').then(m => m.LeaderboardModule),
		canActivate: [AuthGuard]
	},
	{
		path: "reportedVideo",
		loadChildren: () => import('./views/reported-videos/reported-videos.module').then(m => m.ReportedVideosModule),
		canActivate: [AuthGuard]
	},
]
@NgModule({
  	imports: [
		RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }
