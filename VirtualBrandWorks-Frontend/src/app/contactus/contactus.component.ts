import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControlName, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../shared/services/commonservices/commonservices.service';
import { HttpService } from '../shared/services/httpservice/httpservice.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private httpService: HttpService,private commonService: CommonService, private activeModal: NgbActiveModal) { }

  mailForm: FormGroup;
  formValid: boolean = true;

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.mailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required]]
    })
  }
  
  mailData(){
    this.formValid = this.mailForm.valid;
    if(this.mailForm.invalid){
      return
    }
    this.httpService.mail(this.mailForm.get('email').value, this.mailForm.get('message').value).subscribe(res => {
      if(res.Status){
        this.activeModal.dismiss(false);
        this.commonService.showSuccess('Your query received Successfully','')
           }
    })
  }

}
