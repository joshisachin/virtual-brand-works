import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ImageCropperModule } from 'ngx-image-cropper';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './views/elements/footer/footer.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ServiceInterceptor } from './interceptor/service.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { PaymentRedirectComponent } from './views/sessions/payment-redirect/payment-redirect.component';
import { SharedModule } from './shared/shared.module';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { CookieService } from 'ngx-cookie-service';
import { provideConfig } from './views/sessions/sessions.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { LoaderComponent } from './views/elements/loader/loader.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ImageCropperPopupComponent } from './shared/modals/image-cropper/image-cropper-popup.component';
import { EarnedPointsComponent } from './views/earned-points/earned-points.component';
import { ContactusComponent } from './contactus/contactus.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    PaymentRedirectComponent,
    LoaderComponent,
    ImageCropperPopupComponent,
    EarnedPointsComponent,
    ContactusComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    SharedModule,
    SocialLoginModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({"preventDuplicates": true}),
    InfiniteScrollModule,
    ReactiveFormsModule,
    FormsModule,
    ImageCropperModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS,useClass: ServiceInterceptor,multi: true },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
