﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class TeamListViewModel 
    {
        public long UserId { get; set; }
        [Required]
        public long RoleId { get; set; }
        public long TeamId { get; set; }
        public string Name { get; set; }
        public long TrainerId { get; set; }
        [Required]
        public long CompanyId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsCompanyTeam { get; set; }
        public bool IsTaggedContent { get; set; }
    }

    public class AssociatedTeamListViewModel
    {
        public long TeamId { get; set; }
        public string Name { get; set; }
        public int NumberOfUsers { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Status { get; set; }
    }

}
