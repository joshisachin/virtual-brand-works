﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class ManageUserViewModel
    {
        public long CompanyId { get; set; }
        public string CompanyName { get; set; }
        public bool CompanyStatus { get; set; }
        public string Industry { get; set; }
        public List<CompanyViewModel> Companies { get; set; }
        public List<UserViewModel> Users { get; set; }
        public string SubscriptionName { get; set; }
        public string BillingCycle { get; set; }
        public long? UsersUpTo { get; set; }
        public string ValidTill { get; set; }
        public string ActiveFrom { get; set; }
        public double? BilledAmount { get; set; }
    }
    public class CompanySubscriptionViewModel
    {
        public long PlanId { get; set; }
        public string SubscriptionName { get; set; }
        public string BillingCycle { get; set; }
        public int PlanType { get; set; }
        public long? UsersUpTo { get; set; }
        public DateTime ValidTill { get; set; }
        public DateTime ActiveFrom { get; set; }
        public double? BilledAmount { get; set; }
        public string StripeSubscriptionId { get; set; }
        public long CompanySubscriptionId { get; set; }
        public PaymentMethodViewModel PaymentCard { get; set; }
        public bool IsCancelled { get; set; }
    }
}
