﻿using System.Collections.Generic;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class ResultsItem
    {
        public string error { get; set; }
        public string message_id { get; set; }
    }

    public class FcmResponse
    {
        public long multicast_id { get; set; }

        public int success { get; set; }

        public int failure { get; set; }

        public int canonical_ids { get; set; }

        public List<ResultsItem> results { get; set; }

        public long? UserId { get; set; }
    }
}
