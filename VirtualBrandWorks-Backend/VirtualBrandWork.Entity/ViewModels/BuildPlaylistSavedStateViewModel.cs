﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class BuildPlaylistSavedStateViewModel
    {
        public long BuildPlaylistSavedStateId { get; set; }
        public long UserId { get; set; }
        public long PlaylistId { get; set; }
        [Required]
        public int StepNumber { get; set; }
    }
}
