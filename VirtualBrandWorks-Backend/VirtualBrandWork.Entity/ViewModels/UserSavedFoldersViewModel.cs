﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class UserSavedFoldersViewModel : BaseViewModel
    {
        public long FolderId { get; set; }
        public bool IsAddRequest { get; set; }
    }
}
