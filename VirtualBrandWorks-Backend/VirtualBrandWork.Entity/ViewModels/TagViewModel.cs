﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class TagViewModel
    {
        public long TagId { get; set; }
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(10)]
        public string ShortName { get; set; }
        public bool IsHandle { get; set; }

        public string Industry { get; set; }
        public long? CategoryId { get; set; }
        public List<SelectListItem> Categories { get; set; }

    }
}
