﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class CompanyFolderViewModel : BaseViewModel
    {
        public long CompanyLibraryFolderId { get; set; }

        public long CompanyId { get; set; }

        [Required(ErrorMessage = "Folder Name cannot be blank", AllowEmptyStrings = false)]
        [Display(Name = "Folder Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Folder Short Name cannot be blank", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "This is not a valid short form. Please input only alphabets")]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Must be 2 characters long")]
        [Display(Name = "Folder Short Name")]
        public string ShortName { get; set; }

        [Display(Name = "Category")]
        public string CategoryName { get; set; }

        [Display(Name = "No. Of Content")]
        public int NumberOfContent { get; set; }

        public bool isDynamicFolder { get; set; }

    }
}
