﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class SubscriptionPlanPriceViewModel
    {
        public long SubscriptionPlanPriceId { get; set; }
        public int SubscriptionId { get; set; }
        public string ProductId { get; set; }
        public string PlanId { get; set; }
        public int PlanType { get; set; }
        public double? Price { get; set; }
        public string StripeSubscriptionId { get; set; }
    }

    public class SubscriptionPriceDetailViewModel
    {
        public long SubscriptionPlanPriceId { get; set; }
        public int SubscriptionId { get; set; }
        public int PlanType { get; set; }
        public double? Price { get; set; }
        public string PlanName { get; set; }
    }
}
