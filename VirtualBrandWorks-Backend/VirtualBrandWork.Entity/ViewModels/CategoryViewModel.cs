﻿using System.ComponentModel.DataAnnotations;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class CategoryViewModel
    {
        public long CategoryId { get; set; }

        [Required(ErrorMessage = "Industry name cannot be blank.", AllowEmptyStrings = false)]
        [Display(Name = "Industry")]
        [StringLength(30)]
        public string CategoryName { get; set; }
        
        [Display(Name = "Description")]
        [StringLength(200)]
        public string Description { get; set; }
    }
}
