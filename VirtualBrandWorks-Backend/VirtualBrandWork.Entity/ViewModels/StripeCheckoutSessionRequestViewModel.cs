﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class StripeCheckoutSessionRequestViewModel
    {
        [Required(ErrorMessage ="PlanId is required")]
        public string PlanId { get; set; }
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Subscription is required")]
        public string Subscription { get; set; }
        [Required(ErrorMessage = "Amount is required")]
        public string Amount { get; set; }
        [Required(ErrorMessage = "BillingCycle is required")]
        public string BillingCycle { get; set; }
        [Required(ErrorMessage = "FirstName is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "LastName is required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "SuccessUrl is required")]
        public string SuccessUrl { get; set; }
        [Required(ErrorMessage = "CancelUrl is required")]
        public string CancelUrl { get; set; }
    }

    public class CreateCardCheckoutRequestViewModel
    {
        [Required(ErrorMessage = "SuccessUrl is required")]
        public string SuccessUrl { get; set; }
        [Required(ErrorMessage = "CancelUrl is required")]
        public string CancelUrl { get; set; }
    }
}
