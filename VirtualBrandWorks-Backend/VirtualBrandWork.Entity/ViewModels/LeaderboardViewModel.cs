﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class LeaderboardViewModel
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PlaylistCompletePercentage { get; set; }
        public string Color { get; set; }
    }

    public class LeaderboardByTeamViewModel
    {
        public long TeamId { get; set; }
        public string Name { get; set; }
        public int PlaylistCompletePercentage { get; set; }
        public string Color { get; set; }
    }

    public class PlaylistLeaderboardViewModel
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PlaylistCompletePercentage { get; set; }
        public string Color { get; set; } = string.Empty;
        public long TeamId { get; set; }
        public string TeamName { get; set; }
        public List<PlaylistFolderViewModel> Folders { get; set; }
    }
}
