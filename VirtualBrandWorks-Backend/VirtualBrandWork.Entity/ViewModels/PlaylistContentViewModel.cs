﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class PlaylistContentViewModel
    {
        public List<PlaylistContent> PlaylistContents { get; set; }
        public long PlaylistId { get; set; }
    }

    public class PlaylistContent
    {
        public long PlaylistFolderId { get; set; }
        public long PlaylistContentId { get; set; }
        public int SortOrder { get; set; }
        public bool IsContenSaved { get; set; }
    }

    public class PlaylistContentResponseViewModel
    {
        public long PlaylistContentMasterId { get; set; }
        public bool? IsUserPassed { get; set; }
        public int? MarksObtained { get; set; }
        public int? NumberOfQuizAttempts { get; set; }
        public bool IsTurnedOffExistingQuiz { get; set; }
        public int Badge { get; set; }
        public ContentViewModel Content { get; set; }
    }
}
