﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class CompanyContentViewModel : BaseViewModel
    {
        public long CompanyLibraryContentId { get; set; }

        [Required(ErrorMessage = "Folder Name cannot be blank")]
        public long FolderId { get; set; }

        public string FolderName { get; set; }

        [Required(ErrorMessage = "The Content title cannot be blank")]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(20)]
        public string ShortName { get; set; }

        public string ContentUrl { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public List<long> Tags { get; set; }

        public bool IsPublished { get; set; }
        public bool IsBroadcasted { get; set; }
        public string MimeType { get; set; }
        //for mapping content created while build playlist
        public long PlayListFolderId { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
    }

    public class CompanyContentResponse : BaseViewModel
    {
        public long CompanyLibraryContentId { get; set; }

        [Required(ErrorMessage = "Folder Name cannot be blank")]
        public long FolderId { get; set; }

        public string FolderName { get; set; }

        [Required(ErrorMessage = "The Content title cannot be blank")]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(20)]
        public string ShortName { get; set; }

        public string ContentUrl { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public List<TagViewModel> Tags { get; set; }

        public bool IsPublished { get; set; }
        public bool IsBroadcasted { get; set; }
        public string ThumbnailUrl { get; set; }
        public string ContentURLWithExtension { get; set; }
        public string MimeType { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
    }
}
