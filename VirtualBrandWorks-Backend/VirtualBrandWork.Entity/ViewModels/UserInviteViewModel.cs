﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class UserInviteViewModel : BaseViewModel
    {
        public long UserInviteId { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        //public int RoleId { get; set; }

        [Required]
        [StringLength(100)]
        public string EmailId { get; set; }
        public long? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string EncodedEmail { get; set; }

    }
}
