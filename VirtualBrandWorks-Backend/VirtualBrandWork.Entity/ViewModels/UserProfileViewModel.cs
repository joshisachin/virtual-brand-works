﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class UserProfileViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(100)]
        public string EmailId { get; set; }

        [StringLength(150)]
        public string AddressOne { get; set; }

        [StringLength(200)]
        public string AddressTwo { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        [StringLength(15)]
        public string PhoneNumber { get; set; }

        public string ProfilePicture { get; set; }
        public DateTime DateOfJoining { get; set; }

        public List<TagViewModel> Tags { get; set; }
        public bool Status { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string HandleName { get; set; }
    }
}
