﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class NotificationViewModel
    {
        public string  Title { get; set; }
        public string Message { get; set; }
        public long UserId { get; set; }
        public DateTime SentOn { get; set; }
        public bool IsSuccess { get; set; }
    }
}
