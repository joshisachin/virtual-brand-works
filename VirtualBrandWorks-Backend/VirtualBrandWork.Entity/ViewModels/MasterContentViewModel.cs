﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class MasterContentViewModel
    {
        public long MasterContentId { get; set; }

        [Required(ErrorMessage = "Folder Name cannot be blank")]
        public long? MasterFolderId { get; set; }

        [Required(ErrorMessage = "The category cannot be blank")]
        public long? CategoryId { get; set; }

        [Display(Name = "Industry")]
        public string CategoryName { get; set; }

        [Display(Name = "Folder")]
        public string FolderName { get; set; }

        [Required(ErrorMessage = "The Content title cannot be blank")]
        [Display(Name = "Content Title")]
        [StringLength(30)]
        public string Name { get; set; }
        
        [Display(Name = "Short Name")]
        [StringLength(20)]
        public string ShortName { get; set; }
        
        public string ContentUrl { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [Display(Name = "Industry")]
        public List<SelectListItem> Categories { get; set; }

        [Display(Name = "Folder")]
        public List<SelectListItem> Folders { get; set; }

        public List<SelectListItem> Tags { get; set; }
        public int[] TagIds { get; set; }

        public string[] TagIdStrings { get; set; }
        public bool IsPublished { get; set; }

       public FileUploadViewModel FileUploadViewModel { get;set;}

        [Display(Name = "Add Content To Dynamic Folder")]
        public bool IsAddDynamicFolder { get; set; }

        public bool flag { get; set; }
        public List<string> SelectedCategoryNames { get; set; }
        public string MimeType { get; set; }
        public bool IsFileChanged { get; set; }
    }

    public class FolderModel
    {
        public long MasterFolderId { get; set; }
        public string Name { get; set; }
    }
    public class TagModel
    {
        public long TagId { get; set; }
        public string Name { get; set; }
        public bool IsHandle { get; set; }
    }
}
