﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class UserListViewModel
    {
        public long UserId { get; set; }
        public string ProfilePicture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsSelected { get; set; }

    }

    public class UserListRequestViewModel 
    {
        public long UserId { get; set; }
        public long CompanyId { get; set; }
        public string Search { get; set; }
        public int LocationId { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 100;
        public string SortBy { get; set; }
        public bool IsSortByReverse { get; set; }
        public long TeamId { get; set; }
    }

    public class AssociatedUsersListViewModel
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TeamName { get; set; }
        public List<string> AssociatedPlaylist { get; set; }
        public bool Status { get; set; }
    }

    public class PlaylistUsersViewModel
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public bool IsPlaylistTrainer { get; set; }
        public int UnreadMessageCount { get; set; }
        public DateTime? LastMessageDate { get; set; }
    }
    public class MessageDate
    {
        public DateTime? TimeOfLastMessage { get; set; }
    }
}
