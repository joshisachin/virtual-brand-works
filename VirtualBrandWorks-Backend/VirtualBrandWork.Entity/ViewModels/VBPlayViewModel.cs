﻿using System;
using System.Collections.Generic;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class VBPlayViewModel
    {
        public long ContestId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ContentUrl { get; set; }
        public long Likes { get; set; }
        public long Views { get; set; }
        public int Users { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Rules { get; set; }
        public List<TagModel> Tags { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public string SortBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ThumbnailUrl { get; set; }
        public bool IsSubmitted { get; set; }
        public string MimeType { get; set; }
        public bool IsBroadCasted { get; set; }
        public bool IsLikedByUser { get; set; }
        public string IPAddress { get; set; }
        public bool IsJoinedByUser { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsWinner { get; set; }
        public long EarnedPoints { get; set; }
        public bool IsCreator { get; set; }
        public bool IsContestEntry { get; set; }
        public int Points { get; set; }
        public int Visibility { get; set; }
        public bool IsWinnerAnnounced { get; set; }
        public List<long> UserIdsToNotify { get; set; }
        public ChallengeEntryViewModel ChallengeEntry { get; set; }
    }

    public class VBPlayCreateViewModel
    {
        public long ContestId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ContentUrl { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Rules { get; set; }
        public List<long> TagIds { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MimeType { get; set; }
        public Resolution Resolution { get; set; }
        public bool? IsSubmitted { get; set; }
    }

    public class VBPlayUpdateViewModel
    {
        public long ContestId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ContentUrl { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Rules { get; set; }
        public List<long> TagIds { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MimeType { get; set; }
        public Resolution Resolution { get; set; }
        public bool? IsSubmitted { get; set; }
    }

    public class ChallengeEntryViewModel
    {
        public long ContestEntryId { get; set; }
        public long JoinedByUserId { get; set; }
        public DateTime JoinedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ContentUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public long Likes { get; set; }
        public long Views { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public string MimeType { get; set; }
        public bool IsLikedByUser { get; set; }
        public bool IsWinner { get; set; }
        public bool IsJoinedByUser { get; set; }
        public bool IsCreator { get; set; }
        public bool IsBroadCasted { get; set; }
        public bool IsContestEntry { get; set; }
        public int Points { get; set; }
        public int Visibility { get; set; }
    }

    public class ContestChallengeAndEntriesViewModel
    {
        public VBPlayViewModel ContestChallenge { get; set; }
        public List<ChallengeEntryViewModel> ContestEntries { get; set; }
    }

    public class VBPlayContentCommonViewModel
    {
        public long ContestContentId { get; set; }
        public bool IsContestEntry { get; set; }
    }

    public class SharedContentViewModel
    {
        public long ContentId { get; set; }
        public Constants.SharedContentType ContentType { get; set; }
        public string IPAddress { get; set; }
        public long UserId { get; set; }
        public bool? IsVideo { get; set; }
    }

    public class ContestEntrySubmitViewModel
    {
        public long ContestEntryId { get; set; }
        public bool IsSubmitted { get; set; }
    }

    public class ContestBroadcastRequestViewModel
    {
        public long ContestId { get; set; }
        public bool IsBroadCasted { get; set; }
        public bool IsContestEntry { get; set; }
    }

    public class ContestVisibilityViewModel : BaseViewModel
    {
        public long ContestId { get; set; }
        public bool IsContestEntry { get; set; }
        public int Visibility { get; set; }
    }
    public class Resolution
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
