﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class QuestionViewModel
    {
        public long QuestionId { get; set; }
        public string Question { get; set; }
    }
}
