﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class StripeSetupModel
    {
        public string PublishableKey { get; set; }
        public string PlanId { get; set; }
    }
}