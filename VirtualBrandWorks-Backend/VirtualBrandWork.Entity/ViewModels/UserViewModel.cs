﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class UserViewModel
    {
        public long UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(100)]
        public string EmailId { get; set; }

        [StringLength(150)]
        public string Address { get; set; }

        [StringLength(15)]
        public string PhoneNumber { get; set; }

        public long? CompanyId { get; set; }
        public string Company { get; set; }
        public string Industry { get; set; }
        public bool CompanyStatus { get; set; }
        public string Role { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
    }

    public class UserContext
    {
        public long UserId { get; set; }
        public string JWTToken { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
        public long Industry { get; set; }
        public string Password { get; set; }
        public long RoleId { get; set; }
        public string ProfilePicture { get; set; }
        public string CompanyLogo { get; set; }
        public bool IsActive { get; set; }
        public bool IsSubscriptionActive { get; set; }
    }

    public class UserMyPlanNotiFicationViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string OldPlanName { get; set; }
        public string PlanName { get; set; }
        public DateTime CancelAt { get; set; }
        public string OldLast4Digit { get; set; }
        public string NewLast4Digit { get; set; }
        public long NumberOfAllowedUsers { get; set; }

        public string PlanType { get; set; }
        public double Price { get; set; }
        public bool IsPlanNameChanged { get; set; }
        public string OldPlanDescription { get; set; }
        public string PlanDescription { get; set; }
        public bool IsPlanDescriptionChanged { get; set; }

        public long OldNumberOfAllowedUsers { get; set; }
        public bool IsNumberOfUsersChanged { get; set; }
        public double OldMonthlyCost { get; set; }
        public double NewMonthlyCost { get; set; }
        public bool IsMonthlyCostChanged { get; set; }
        public double OldAnnualCost { get; set; }
        public double NewAnnualCost { get; set; }
        public bool IsAnnualCostChanged { get; set; }
        public List<SubscribedUserViewModel> UsersForMonthlySubscription { get; set; }
        public List<SubscribedUserViewModel> UsersForYearlySubscription { get; set; }
    }

    public class SubscribedUserViewModel
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
    }
}
