﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
   public class FcmMessage
    {
        public string[] Tokens { get; set; }
        public FcmNotification Notification { get; set; }
        public object Data { get; set; }
    }
}
