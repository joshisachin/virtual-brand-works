﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
  public  class PaymentMethodViewModel
    {
        public string CardType { get; set; }
        public int ExpMonth { get; set; }
        public int ExpYear { get; set; }
        public string Last4Digit { get; set; }
        public string PaymentMethodId { get; set; }
        public BillingDetails BillingDetails { get; set; }
        public bool IsDefault { get; set; }
    }
}
