﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class CompanyViewModel : BaseViewModel
    {
        public long CompanyId { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string AdminEmail { get; set; }
    }

    public class CompanyLogoRequestViewModel: BaseViewModel
    {
        [Required]
        public string CompanyLogo { get; set; }
    }
    public class CompanyInfoViewModel
    {
        public long CompanyId { get; set; }
        public long UserId { get; set; }
        public string CompanyName { get; set; }
        public bool Status { get; set; }
        public string CompanyLogo { get; set; }
        public string AboutCompany { get; set; }
    }

    public class PrifileDeleteRequestModel : BaseViewModel
    {
        public bool IsCompanyLogo { get; set; }
        public string ImageName { get; set; }
    }
}
