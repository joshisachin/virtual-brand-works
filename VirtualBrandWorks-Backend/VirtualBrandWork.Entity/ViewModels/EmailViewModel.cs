﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
   public class EmailViewModel
    {
        public string Email { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public string SubscriptionPlan { get; set; }
        public long Amount { get; set; }
        public int BillingCycle { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool flag { get; set; }
    }
}
