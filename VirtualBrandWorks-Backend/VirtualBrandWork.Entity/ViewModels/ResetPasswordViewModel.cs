﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Email cannot be left blank")]
        [EmailAddress(ErrorMessage = "Please enter a valid email format")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password cannot be left blank")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[_\\W]).{8,15}$", ErrorMessage = "Password must be 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Confirm password cannot be left blank")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]

        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Reset Code cannot be left blank")]
        [Display(Name = "Reset Code")]
        public string ResetCode { get; set; }

        public string Salt { get; set; }
        public string PasswordHash { get; set; }
    }
}
