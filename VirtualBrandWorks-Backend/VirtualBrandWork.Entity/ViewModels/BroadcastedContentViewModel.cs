﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class BroadcastedContentViewModel
    {
        public long ContentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ContentUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string MimeType { get; set; }
        public long Likes { get; set; }
        public long Views { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public bool IsCompanyLibraryContent { get; set; }
        public bool IsContestEntry { get; set; }
        public bool IsLikedByUser { get; set; }
        public long CreatedBy { get; set; }
        public bool IsOptOutForNotification { get; set; }
        public bool IsExtendDateNotified { get; set; }
        public string ContentType { get; set; }
        public DateTime? BroadcastingDate { get; set; }
    }


    public class VisibilityControlContentViewModel
    {
        public long ContentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ContentUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string MimeType { get; set; }
        public long Likes { get; set; }
        public long Views { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public bool IsCompanyLibraryContent { get; set; }
        public bool IsContestEntry { get; set; }
        public bool IsLikedByUser { get; set; }
        public long CreatedBy { get; set; }
        public int Visibility { get; set; }
    }
}
