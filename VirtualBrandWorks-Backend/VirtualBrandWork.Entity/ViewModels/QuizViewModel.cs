﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class QuizViewModel
    {
        public QuizViewModel()
        {
            QuestionAsnwers = new List<QuestionAnswerViewModel>();
        }
        public long QuizId { get; set; }
        public string QuizName { get; set; }
        public int CategoryId { get; set; }
        public int FolderId { get; set; }
        public List<ContentViewModel> Contents { get; set; }
        public List<CategoryViewModel> Categories { get; set; }
        public List<QuestionAnswerViewModel> QuestionAsnwers { get; set; }
        public QuestionAnswerViewModel QuestionAnswer { get; set; }
        public string ShortName { get; set; }
        public long ContentId { get; set; }
        public string QuizShortName { get; set; }
        public DateTime? QuizCreatedDate { get; set; }
        public DateTime? QuizModifiedDate { get; set; }
        public bool IsEditRequest { get; set; }
        public long QuestionId { get; set; }
    }

    public class QuestionAnswerViewModel
    {
        public QuestionViewModel Question { get; set; }
        public List<AnswerViewModel> Answers { get; set; }
        public long QuizId { get; set; }
        public long RightAnsId { get; set; }
        public long UserAnswerId { get; set; }
    }

    public class QuestionAnswerAttemptViewModel
    {
        public List<QuizUserResponseViewModel> QuestionAttempt { get; set; }
        [Required]
        public long QuizId { get; set; }
        public bool IsMasterQuiz { get; set; }
        public long UserId { get; set; }
        [Required]
        public long PlaylistId { get; set; }
        [Required]
        public long PlaylistContentMasterId { get; set; }
    }

    public class QuizUserResponseViewModel
    {
        public long QuestionId { get; set; }
        public long AnswerId { get; set; }
    }

    public class PassFailViewModel
    {
        public long QuizId { get; set; }
        public long PlaylistId { get; set; }
        public long UserId { get; set; }
        public bool IsMasterQuiz { get; set; }
        public bool IsUserPassed { get; set; }
        public int? NumberOfQuizAttempts { get; set; }
        public int NumberOfRightAnswer { get; set; }
        public int NumberOfWrongAnswer { get; set; }
        public int MarksObtained { get; set; }
        public int TotalMarks { get; set; }
    }

}
