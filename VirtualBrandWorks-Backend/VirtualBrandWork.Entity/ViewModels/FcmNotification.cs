﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class FcmNotification
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
