﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class MessageViewModel
    {
        public long PlayListId { get; set; }
        public long SenderId { get; set; }
        public long ReceiverId { get; set; }
        public DateTime Date { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public string SenderImage { get; set; }
        public string ReceiverImage { get; set; }
        public int MessageType { get; set; }
        public string Message { get; set; }
        public string ImageMessage { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 100;
        public long MessageId { get; set; }
    }
}
