﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class PlaylistViewModel : BaseViewModel
    {
        public long PlayListId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public bool IsPublished { get; set; }
        public bool IsLaunched { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class PlaylistAssignToViewModel : BaseViewModel
    {
        [Required]
        public long PlayListId { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public int LocationId { get; set; }
        public List<long> Users { get; set; }
        public List<long> Teams { get; set; }
    }

    public class PlaylistMasterViewModel
    {
        public long PlayListMasterId { get; set; }
        public long PlayListId { get; set; } 
        public int FolderId { get; set; }       
        public string FolderName { get; set; }
        public string FolderShortName { get; set; }
        public int SortOrder { get; set; }
        public bool IsFromMasterLibrary { get; set; }
    }

    public class PlaylistDetailViewModel : BaseViewModel
    {
        public string Name { get; set; }
        //public List<FolderViewModel> PlaylistFolders { get; set; }
        //public List<UserDetailForTeamViewModel> AssignedUsers { get; set; }
        //public List<TeamDetailViewModel> AssignedTeams { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Location { get; set; }
    }

    public class PlaylistAssignedUserOrTeam
    {
        public List<UserDetailForTeamViewModel> Users { get; set; }
        public List<TeamDetailViewModel> Teams { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class PlaylistUpdateViewModel : BaseViewModel
    {
        [Required]
        public long PlayListId { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public bool IsPublished { get; set; }
        public bool IsLaunched { get; set; }
        public bool IsDeleted { get; set; }
        public string SearchText { get; set; }
    }

    public class PlaylistListViewModel
    {
        public long PlaylistId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int NumberOfUsers { get; set; }
        public int NumberOfTeams { get; set; }
        public bool Status { get; set; }
        public int MessageCount { get; set; }
        public bool? IsExpired { get; set; }
        public bool IsUserPass { get; set; }
        public int PlaylistCompletePercentage { get; set; }
        public List<PlaylistFolderViewModel> Folders { get; set; }
        public List<string> AssignedTo { get; set; }
        public int NumberOfUserCompletedPlaylist { get; set; }
        public int NumberOfStudents { get; set; }
    }

    public class PlaylistFolderViewModel
    {
        public long PlayListFolderId { get; set; }
        public long PlayListId { get; set; }
        public string PlaylistName { get; set; }
        public long FolderId { get; set; }
        public string FolderName { get; set; }
        public string FolderShortName { get; set; }
        public int NumberOfContent { get; set; }
        public bool IsMasterLibraryFolder { get; set; }
        public int SortOrder { get; set; }
        public long UserId { get; set; }
        public int PlaylistFolderCompletePercentage { get; set; }
        public int QuizCount { get; set; }
        public int QuizCompletedCount { get; set; }
        public int? MarksObtained { get; set; }
        public int? TotalMarks { get; set; }
        public int NumberOfUserCompletedFolder { get; set; }
    }

    public class UserAssociatedPlaylistRequestViewModel
    {
        [Required]
        public long UserId { get; set; }
        public string SearchText { get; set; }
        public bool IsLaunched { get; set; }
    }

    public class MyPlaylistDetailViewModel
    {
        public long PlaylistId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NumberOfUsers { get; set; }
        public int NumberOfTeams { get; set; }
        public bool Status { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsUserPass { get; set; }
        public int TotalPercentage { get; set; }
        public int? TotalMarksObtained { get; set; }
        public int? TotalMarks { get; set; }
        public int PlaylistCompletePercentage { get; set; }
        public bool IsAssigned { get; set; }
        public bool? IsExpired { get; set; }
        public List<BadgeInfoViewModel> Badges { get; set; }
        public List<PlaylistFolderViewModel> Folders { get; set; }
    }

    public class MyPlaylistFolderDetailViewModel
    {
        public PlaylistFolderViewModel Folder { get; set; }
        public PlaylistContentResponseViewModel PlaylistContent { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

    }

    public class PlaylistFolderSortViewModel
    {
        [Required]
        public long playlistId { get; set; }
        [Required]
        public long playlistFolderId { get; set; }
        [Required]
        public int destinationSortOrder { get; set; }
    }

    public class TurnOffExistingQuizViewModel
    {
       public long PlaylistContentMasterId { get; set; }        
       public long PlaylistFolderId { get; set; }
       public bool IsTurnedOffExistingQuiz { get; set; }
    }

    public class ViewResultDetailAndFoldersViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int TotalPercentage { get; set; }
        public int TotalScore { get; set; }
        public int[] Badges { get; set; }
        public List<PlaylistFolderViewModel> folders { get; set; }
    }


    public class PlaylistAssociatedUserViewModel
    {
        public long TeamId { get; set; }
        public string TeamName { get; set; }
        public string TrainerFirstName { get; set; }
        public string TrainerLastName { get; set; }
        public int NumnberOfUsers { get; set; }
        public int[] Badges { get; set; }
        public List<AssociatedUserDetailViewModel> PlaylistUsers { get; set; }
    }
    public class AssociatedUserDetailViewModel
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public int CompletedPercentage { get; set; }
        public List<PlaylistFolderResultViewModel> FolderResult { get; set; }
        public int TotalScore { get; set; }
    }
    public class PlaylistFolderResultViewModel
    {
        public long PlaylistFolderId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int? MarksObtained { get; set; }
        public int Badge { get; set; }
        public int QuizCount { get; set; }
        public int QuizCompletedCount { get; set; }
        public int NumberOfContent { get; set; }
        public int PlaylistFolderCompletePercentage { get; set; }

    }


    public class PlaylistNameAndDescriptionUpdateViewModel : BaseViewModel
    {
        [Required]
        public long PlayListId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }
    }

    public class BadgeInfoViewModel
    {
        public int BadgeId { get; set; }
        public int NumberOfAwardees { get; set; }
    }

    public class PlaylistDropdownViewModel
    {
        public long PlaylistId { get; set; }
        public string Name { get; set; }
    }
}
