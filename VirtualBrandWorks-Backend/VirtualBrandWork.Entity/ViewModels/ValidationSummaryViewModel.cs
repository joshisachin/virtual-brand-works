﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
   public class ValidationSummaryViewModel
    {
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
    }
}
