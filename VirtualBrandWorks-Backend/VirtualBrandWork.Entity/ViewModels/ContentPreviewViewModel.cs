﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class ContentPreviewViewModel
    {
        public long FolderId { get; set; }
        public string FolderName { get; set; }
        public int NoOfVideo { get; set; }
        public int NoOfImages { get; set; }
        public int NoOfDocs { get; set; }
        public List<ContentViewModel> ContentList { get; set; }
        public bool IsSelectedFolder { get; set; }
    }
}
