﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class FolderContentViewModel
    {
        public FolderContentViewModel()
        {
            Contents = new List<ContentViewModel>();
            Folder = new FolderViewModel();
        }
        public List<ContentViewModel> Contents { get; set; }
        public FolderViewModel Folder { get; set; }
    }

    public class FolderContentDetailViewModel
    {
        public ContentViewModel Content { get; set; }
        public FolderViewModel Folder { get; set; }
    }
}
