﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class PrizeStoreViewModel
    {
        public long PrizeId { get; set; }

        [Required]
        public int PrizeCategoryId { get; set; }
        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? Points { get; set; }

        public string ThumbnailImage { get; set; }

        public int? Quantity { get; set; }

        public string TimeOffDuration { get; set; }

        public string BrandName { get; set; }
        public int PrizeAmount { get; set; }
        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }
        public string Remarks { get; set; }

        public List<string> VoucherCodes { get; set; }
    }

    public class PrizeCategoryViewModel
    {
        public int PrizeCategoryId { get; set; }
        public string Name { get; set; }
    }

    public class PrizeCreateHelperViewModel
    {
        public List<PrizeCategoryViewModel> PrizeCategories { get; set; }
        public List<string> TimeOffOptions = new List<string>() { "Half Day", "Full Day", "Two Days", "Three Days" };
    }

    public class PrizeListViewModel
    {
        public long PrizeId { get; set; }
        public long PrizeCategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Points { get; set; }
        public string ThumbnailImage { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }
        public int AvailableCount { get; set; }
        public string Remarks { get; set; }
    }

    public class AvailPrizeResponseViewModel
    {
        public int StatusType { get; set; }
        public int PrizeCategoryId { get; set; }
        public string TimeOffDuration { get; set; }
        public int CashPrizeAmount { get; set; }
        public string BrandName { get; set; }
        public string AvailedRedeemCode { get; set; }
        public string Remarks { get; set; }
        public UserViewModel User { get; set; }
    }

    public class PrizeStatusViewModel
    {
        public long PrizeId { get; set; }
        public bool IsActive { get; set; }
    }

}
