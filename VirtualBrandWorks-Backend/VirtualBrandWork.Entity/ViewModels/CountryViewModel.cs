﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class CountryViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
    }
}
