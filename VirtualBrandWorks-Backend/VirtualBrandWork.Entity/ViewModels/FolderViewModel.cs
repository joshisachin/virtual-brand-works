﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
   public class FolderViewModel
    {
        [Required]
        public long FolderId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int NumberOfContent { get; set; }
        public string Description { get; set; }
        public long? CategoryId { get; set; }
        public bool IsDynamicFolder { get; set; }
        public string SearchText { get; set; }
        public bool IsMasterFolder { get; set; }
        public bool IsSelectedFolder { get; set; }
        public int ContentType { get; set; }
        public bool IsPublished { get; set; }
    }

    public class FolderDetailViewModel
    {
        public long FolderId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }

    public class FolderContentRequestViewModel
    {
        [Required]
        public long FolderId { get; set; }
        public long ContentId { get; set; }       
        public string SearchText { get; set; }
        public int ContentType { get; set; }
    }
}
