﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class TeamDetailViewModel : BaseViewModel
    {
        [Required]
        public long TeamId { get; set; }
        public long CompanyId { get; set; }
        public string TeamName { get; set; }
        public int NumberOfUsers { get; set; }
        public List<string> AssociatedPlayList { get; set; }
        public string TrainerFirstName { get; set; }
        public string TrainerLastName { get; set; }
        public bool IsSelected { get; set; }
        public bool Status { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public List<long> TagIds { get; set; }
        public List<TagViewModel> Tags { get; set; }
        public List<string> TagList { get; set; }
        public List<UserDetailForTeamViewModel> Users { get; set; }

        [StringLength(30, MinimumLength = 2, ErrorMessage = "Handlename must be at least 2 characters long and max 30 characters long.")]
        public string HandleName { get; set; } 
    }

    public class UserDetailForTeamViewModel
    {
        public long UserId { get; set; }
        public long CompanyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public string ContactNo { get; set; }
        public string Location { get; set; }
        public int Score { get; set; }
        public int CourseCompleted { get; set; }
        public bool IsSelected { get; set; }
        public List<string> TagList { get; set; }
        public string HandleName { get; set; }
    }

    public class CreateTeamViewModel : BaseViewModel
    {
        public long TeamId { get; set; }
        public long CompanyId { get; set; }
        public string TeamName { get; set; }
        public string TrainerFirstName { get; set; }
        public string TrainerLastName { get; set; }
        public bool Status { get; set; }
        public int LocationId { get; set; }
        public List<long> TagIds { get; set; }
        public List<UserDetailForTeamViewModel> Users { get; set; }

        [StringLength(30, MinimumLength = 2, ErrorMessage = "Handlename must be at least 2 characters long and max 30 characters long.")]
        public string HandleName { get; set; }
    }

    public class TeamUpdateViewModel
    {
        public long TeamId { get; set; }
        public string TeamName { get; set; }
        public List<UserListViewModel> Users { get; set; }
    }
}
