﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class FileUploadViewModel
    {
        private readonly string _privateKey;

        public FileUploadViewModel(string publicKey, string privateKey, string bucketName, string redirectUrl)
        {
            _privateKey = privateKey;

            // Document that describes the POST form upload metadata for Amazon S3
            // http://doc.s3.amazonaws.com/proposals/post.html#The_form_declaration

            FormAction = string.Format("https://{0}.s3.amazonaws.com/", bucketName);
            FormMethod = "post";
            FormEnclosureType = "multipart/form-data";

            Bucket = bucketName;
            FileId = Guid.NewGuid().ToString(); // Every file is uploaded with a new Guid as its ID
            AWSAccessKey = publicKey;
            Acl = "private"; // one of private, public-read, public-read-write, or authenticated-read
            RedirectUrl = redirectUrl;
        }

        public string FormAction { get;  set; }
        public string FormMethod { get;  set; } // "post"
        public string FormEnclosureType { get;  set; } // "multipart/form-data"
        public string Bucket { get; private set; }
        public string FileId { get; set; }
        public string AWSAccessKey { get; set; }
        public string Acl { get; set; }
        public string Policy { get; private set; }
        public string RedirectUrl { get; private set; }

        public string Base64EncodedPolicy
        {
            //get
            //{
            //    var encoding = new ASCIIEncoding();
            //    var base64Policy = Convert.ToBase64String(encoding.GetBytes(Policy));
            //    return base64Policy;
            //}
            get;
            set;
        }

        public string Signature
        {
            //get
            //{
            //    var encoding = new ASCIIEncoding();
            //    var hmacsha1 = new HMACSHA1(encoding.GetBytes(_privateKey));
            //    byte[] cipher = hmacsha1.ComputeHash(encoding.GetBytes(Base64EncodedPolicy));
            //    return Convert.ToBase64String(cipher);
            //}
            get;
            set;
        }

        public void SetPolicy(string policy)
        {
            Policy = policy;
            var encoding = new ASCIIEncoding();
            Base64EncodedPolicy = Convert.ToBase64String(encoding.GetBytes(Policy));
            var hmacsha1 = new HMACSHA1(encoding.GetBytes(_privateKey));
            byte[] cipher = hmacsha1.ComputeHash(encoding.GetBytes(Base64EncodedPolicy));
            Signature = Convert.ToBase64String(cipher);
        }
    }

    public class AwsPolicySignatureViewModel
    {
        public string Signature { get; set; }
        public string Policy { get; set; }
        public string FormAction { get; set; }
        public string FileId { get; set; }
    }
}
