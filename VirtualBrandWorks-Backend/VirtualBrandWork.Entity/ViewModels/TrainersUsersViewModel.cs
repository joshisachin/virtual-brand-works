﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class UserReponseViewModel
    {
        public long UserId { get; set; }
        public long CompanyId { get; set; }
        public long? RoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public string ContactNo { get; set; }
        public string Location { get; set; }
        public int TotalStudent { get; set; }
        public int TotalTeam { get; set; }
        public int AssignedPlayList { get; set; }
        public string HandleName { get; set; }

    }
}
