﻿using Newtonsoft.Json;
using System;

namespace VirtualBrandWork.Entity.ViewModels
{
    public partial class StripeSessionResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("object")]
        public string Object { get; set; }

        [JsonProperty("billing_address_collection")]
        public object BillingAddressCollection { get; set; }

        [JsonProperty("cancel_url")]
        public Uri CancelUrl { get; set; }

        [JsonProperty("client_reference_id")]
        public object ClientReferenceId { get; set; }

        [JsonProperty("customer")]
        public object Customer { get; set; }

        [JsonProperty("customer_email")]
        public object CustomerEmail { get; set; }

        [JsonProperty("livemode")]
        public bool Livemode { get; set; }

        [JsonProperty("locale")]
        public object Locale { get; set; }

        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("mode")]
        public string Mode { get; set; }

        [JsonProperty("payment_intent")]
        public string PaymentIntent { get; set; }

        [JsonProperty("payment_method_types")]
        public string[] PaymentMethodTypes { get; set; }

        [JsonProperty("setup_intent")]
        public object SetupIntent { get; set; }

        [JsonProperty("shipping")]
        public object Shipping { get; set; }

        [JsonProperty("shipping_address_collection")]
        public object ShippingAddressCollection { get; set; }

        [JsonProperty("submit_type")]
        public object SubmitType { get; set; }

        [JsonProperty("subscription")]
        public object Subscription { get; set; }

        [JsonProperty("success_url")]
        public Uri SuccessUrl { get; set; }
    }

    public partial class Metadata
    {
    }
}