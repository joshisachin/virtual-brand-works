﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class UserRequestViewModel : BaseViewModel
    {
        public long RoleId { get; set; }
        public string UserRole { get; set; }
        public long CompanyId { get; set; }
        public string Search { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 7;
        public string ProfilePictue { get; set; }
        public bool IsActive { get; set; }
        public bool IsNotificationEnabled { get; set; }
        public string SortBy { get; set; }
        public bool IsSortByReverse { get; set; }
    }
}
