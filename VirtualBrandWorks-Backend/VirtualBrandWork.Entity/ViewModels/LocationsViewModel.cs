﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class LocationsViewModel
    {
        public int LocationId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public string LocationName { get; set; }
        public int? TrainerId { get; set; }
        public int? AdminId { get; set; }
        public int? CountryId { get; set; }
        public bool IsAdmin { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
    }

    public class LocationsSearch
    {
        public string LocationName { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public long? CompanyId { get; set; }
    }
}
