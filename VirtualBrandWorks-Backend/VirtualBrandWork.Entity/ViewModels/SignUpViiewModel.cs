﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
  public  class SignUpViiewModel
    {
        public SignUpViiewModel() {
            Subscriptions = new List<SubscriptionViewModel>();
        }

        [Required(ErrorMessage = "First Name cannot be left blank")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please input a valid first name, Only alphabets and space are allowed")]
        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name cannot be left blank")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please input a valid last name, Only alphabets and space are allowed")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email cannot be left blank")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter a valid Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone No. cannot be left blank")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a valid Phone No.")]
        public string PhoneNo { get; set; }

        [Required(ErrorMessage = "Password cannot be left blank")]
        [StringLength(15, ErrorMessage = "Must be between 8 and 15 characters", MinimumLength = 8)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$", ErrorMessage = "Password must be 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password cannot be left blank")]
        [StringLength(15, ErrorMessage = "Must be between 8 and 15 characters", MinimumLength = 8)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$", ErrorMessage = "Password must be 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }


        public string AddressOne { get; set; }
        public string AddressTwo { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Zip code must be numeric")]
        public string ZipCode { get; set; }

        public string Country { get; set; }
        [Required(ErrorMessage ="Please select a plan")]
       // [Range(1,100, ErrorMessage = "Please select a plan")]
        public int SelectedPlan { get; set; }
        public List<SubscriptionViewModel> Subscriptions { get; set; } 
        public List<RoleViewModel> UserRoles { get; set; }

        [Required(ErrorMessage = "Please select user type")]
        public int SelectedRoleId { get; set; }
        public List<CountryViewModel> Countries { get; set; }

        public int? SelectedCountryId { get; set; }
        public List<StateViewModel> States { get; set; }

        public int? SelectedStateId { get; set; }
        public List<CityViewModel> Cities { get; set; }

        public int? SelectedCityId { get; set; }

        [Required(ErrorMessage = "Please accept privacy policy and terms")]
        public bool IsPolicyAccepted { get; set; } = false;
    }
}
