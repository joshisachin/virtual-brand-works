﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class CompanyContentFolderRequestViewModel : BaseViewModel
    {
        public long CompanyId { get; set; }
        public string SearchText { get; set; }
        public string SortBy { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 100;
    }
}
