﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class PlaylistFolderUserSavedViewModel
    {
        public long PlaylistFolderUserSavedId { get; set; }
        public long UserId { get; set; }
        public long FolderId { get; set; }
        public string FolderName { get; set; }
        public string FolderShortName { get; set; }
        public int NumberOfContent { get; set; }
        public bool IsMasterLibraryFolder { get; set; }
        public int SortOrder { get; set; }
    }

    public class PlaylistFolderUserSavedListViewModel
    {
        public List<PlaylistFolderUserSavedViewModel> folders { get; set; }
    } 
}
