﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class SubscriptionViewModel
    {
        public int SubscriptionId { get; set; }
        [Display(Name = "Plan Name")]
        [Required(ErrorMessage = "Plan Name cannot be left blank")]
        public string PlanName { get; set; }
        [Display(Name = "Plan Description")]
        [StringLength(300, ErrorMessage = "The Plan Description cannot be more than 300 characters")]
        public string PlanDescription { get; set; }
        [Display(Name = "No. of Users")]
        [Required(ErrorMessage = "Permitted Users cannot be left blank")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "This field only allows positive numbers")]
        public Nullable<long> NumberOfUsers { get; set; }
        [Display(Name = "Cost for Monthly Billing")]
        [Required(ErrorMessage = "Monthly Cost cannot be left blank")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "This field only allows numbers with upto two decimal points")]
        public Nullable<double> CostForMonthlyBilling { get; set; }
        [Display(Name = "Cost for Annual Billing")]
        [Required(ErrorMessage = "Annual Cost cannot be left blank")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "This field only allows numbers with upto two decimal points")]
        public Nullable<double> CostForAnnualBilling { get; set; }

        [Required(ErrorMessage = "Status cannot be left blank")]
        public bool Status { get; set; }
        public int PlanType { get; set; }

        public string ProductId { get; set; }
        public string MonthlyPlanId { get; set; }
        public string YearlyPlanId { get; set; }
        public List<SubscriptionPlanPriceViewModel> SubscriptionPlanPrice { get; set; }
        public double? Price { get; set; }
        public string PlanId { get; set; }
        public long SubscriptionPlanPriceId { get; set; }
    }

    public class CancelSubscriptionRequestViewModel
    {
        public long PlanId { get; set; }
    }
}
