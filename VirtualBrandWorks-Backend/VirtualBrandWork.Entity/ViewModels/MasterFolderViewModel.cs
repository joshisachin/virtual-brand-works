﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class MasterFolderViewModel
    {
        public long MasterFolderId { get; set; }

        [Required(ErrorMessage = "The folder must be associated to a category", AllowEmptyStrings = false)]
        public long? CategoryId { get; set; }

        [Required(ErrorMessage = "Folder Name cannot be blank", AllowEmptyStrings = false)]
        [Display(Name = "Folder Name")]
        [StringLength(30)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Folder Short Name cannot be blank", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "This is not a valid short form. Please input only alphabets")]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Must be 2 characters long")]        
        [Display(Name = "Short Name")]
        public string ShortName { get; set; }

        [Display(Name = "Industry")]
        public string CategoryName { get; set; }

        [Display(Name = "No. Of Content")]
        public int NumberOfContent { get; set; }

        [Display(Name = "Industry")]
        public List<SelectListItem> Categories { get; set; }

        public bool isDynamicFolder { get; set; }
        public string SelectedCategoryIds { get; set; }
        public List<string> SelectedCategoryNames { get; set; }
        public bool IsSelectedFolder { get; set; }
        public string SearchText { get; set; }
        public string Description { get; set; }
        public int ContentType { get; set; }
    }
    public class CategoryModel
    {
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}
