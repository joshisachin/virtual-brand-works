﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
   public class BaseViewModel
    {
        public long UserId { get; set; }
        public long? RoleId { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime  CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
