﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class AnswerViewModel
    {
        public long AnswerId { get; set; }
        public string Answer { get; set; }
        public bool IsRightAnswer { get; set; }
    }
}
