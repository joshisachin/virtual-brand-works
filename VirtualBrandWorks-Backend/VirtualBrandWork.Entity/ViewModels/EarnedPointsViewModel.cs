﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class EarnedPointsViewModel
    {
        public int NumberOfContestCreated { get; set; }
        public long ContestCreatedEarnedPoints { get; set; }
        public int NumberOfContestPlayed { get; set; }
        public long ContestPlayedEarnedPoints { get; set; }
        public long TotalEarnedPoints { get; set; }
        public long PointsRedeemed { get; set; }
        public long BalancePoints { get; set; }
        public List<PointsTransactionViewModel> PointsHistory { get; set; }
    }
    public class PointsTransactionViewModel
    {
        public int PointsTransactionId { get; set; }
        public long? PlaylistId { get; set; }
        public long? ContestId { get; set; }
        public long UserId { get; set; }
        public string Contest { get; set; }
        public long? Points { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? Balance { get; set; }
        public bool IsEarned { get; set; }
        public string Type { get; set; }
    }
}
