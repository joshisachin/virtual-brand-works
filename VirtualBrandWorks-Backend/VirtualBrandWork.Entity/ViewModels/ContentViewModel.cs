﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class ContentViewModel
    {
        [Required]
        public long ContentId { get; set; }
        public string ContentName { get; set; }
        public bool IsQuestionAdded { get; set; }
        public bool IsTurnOffExistingQuiz { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ShortName { get; set; }
        public string QuizName { get; set; }
        public long? QuizId { get; set; }
        public bool IsAllQuestionAdded { get; set; }
        public string ContentURL { get; set; }
        public string ThumbnailURL { get; set; }
        public string FolderName { get; set; }
        public long FolderId { get; set; }
        public string ContentURLWithExtension { get; set; }
        public bool IsSelectedFolder { get; set; }
        public string ContentDescription { get; set; }
        public long ViewCount { get; set; }
        public int? SortOrder { get; set; }
        public bool IsContentSaved { get; set; }
        public bool IsPublished { get; set; }
        public bool IsBroadcasted { get; set; }
        public string MimeType { get; set; }
        public string IPAddress { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public int Visibility { get; set; }
    }

    public class ContentResponseViewModel
    {
        public long ContentId { get; set; }
    }

    public class CompanyContentVisibilityViewModel : BaseViewModel
    {
        public long ContentId { get; set; }
        public int Visibility { get; set; }
    }

}
