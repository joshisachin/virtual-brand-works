﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class ReportAbuseViewModel : VBPlayViewModel
    {
        public long ReportAbuseId { get; set; }
        public long? UserId { get; set; }
        public List<string> ReportedByUsers { get; set; }
    }
}