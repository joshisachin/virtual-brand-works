﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.ViewModels
{
    public class CompanyQuizViewModel
    {
        public long QuizId { get; set; }
        public string QuizName { get; set; }
        public int TotalQuestion { get; set; }
        public int CorrectCount { get; set; }
        public int IncorrectCount { get; set; }
        public int MarksObtained { get; set; }
        public List<QuestionAnswerViewModel> QuestionAnswers { get; set; }
    }

    public class CompanyQuizRequestViewModel
    {
        public long ContentId { get; set; }
        public long PlaylistContentId { get; set; }
        public string QuizName { get; set; }
        public long UserId { get; set; }
        public bool IsPlaylistContentQuiz { get; set; }
        public bool IsMasterContent { get; set; }
    }

    public class CompanyQuizCreateViewModel
    {
        [Required]
        public long ContentId { get; set; }
        [Required]
        public string QuizName { get; set; }
        public long UserId { get; set; }
        public bool IsPlaylistContentQuiz { get; set; }
        public bool IsMasterContent { get; set; }
    }
}
