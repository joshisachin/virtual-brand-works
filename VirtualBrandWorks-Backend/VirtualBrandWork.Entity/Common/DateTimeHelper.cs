﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.Common
{
    public class DateTimeHelper
    {
        public static DateTime ConvertDateTimeToUtc(DateTime dt, double offSet)
        {
            return (dt.AddMinutes(offSet));
        }

        public static DateTime ConvertUtcToLocalDateTime(DateTime dt, double offSet)
        {
            return (dt.AddMinutes(-offSet));
        }
    }
}
