﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;

namespace VirtualBrandWork.Entity.Common
{
    public class ImageHelper
    {

        public static string GetBase64Stripped(string image)
        {
            image = Regex.Replace(image, @"^.+?(;base64),", string.Empty);
            return image;
        }
        public static void GenerateImageThumbnail(string path, string outputPath)
        {
            // Load image.
            Image image = Image.FromFile(path);
            // Compute thumbnail size.
            Size thumbnailSize = GetThumbnailSize(image);

            // Get thumbnail.
            Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                thumbnailSize.Height, null, IntPtr.Zero);
            thumbnail.Save(outputPath);
        }

        private static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 40;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }
    }
}
