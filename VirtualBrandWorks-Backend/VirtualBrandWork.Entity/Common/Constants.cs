﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace VirtualBrandWork.Entity
{
    public class Constants
    {
        public static Uri BASE_URI = new Uri(ConfigurationManager.AppSettings["ApiBaseUrl"]);


        #region account
        public const string ACCOUNT_LOGIN = "api/account/login";
        public const string FORGOT_PASSWORD = "api/account/forgot_password";
        public const string RESET_PASSWORD = "api/account/reset_password";
        public const string GET_STATES_BY_COUNTRY_API = "api/userinformation/states";
        public const string GET_CITIES_BY_STATE_API = "api/userinformation/cities";
        public const string CHANGE_PASSWORD = "api/account/change_password";
        public const string EXTERNAL_LOGIN = "api/account/external_login";
        public const string VALIDATE_COMPANY = "api/account/validate_company";
        public const string INVITE_USER = "api/account/invite_user";
        //public const string GET_USER_INVITE = "api/account/get_user_invite";
        public const string SIGNUP_AS_USER = "api/account/signup_as_user";
        public const string VALIDATE_USER_EMAIL_AND_COMPANY_NAME = "api/account/ValidateUserEmailAndCompanyName";
        public const string PAYMENT_CANCELLED_MESSAGE = "The current transaction initiated by you for purchasing the Virtual Brandworks subscription has been cancelled as it did not go through final processing.";
        public const string PAYMENT_CANCELLED = "This transaction has been cancelled. Please sign up again.";
        public const string PAYMENT_CANCELLED_SUBJECT = "Cancelled Sign Up";
        public const string PAYMENT_SUCCESSFULL = "Thanks for subscribing! Your account has been created successfully.";// Please check your email for details related to your account.";
        public const string PAYMENT_SUCCESSFULL_SUBJECT = "Welcome to Virtual Brandworks";
        public const string PAYMENT_CANNOT_PROCESSED = "Payment cannot processes at the moment, Please try again later";
        public const string SOME_ERROR_OCCUREED = "Some error occurred, Please try again later";
        public const string SUBSCRIPTION_COMPLETED_SUCCESSFULLY = "subscription has been completed successfully";
        public const string FILE_CORRUPTED = "The file you have uploaded is corrupted and can not be opened. Please upload another file";
        public const string RESET_PASSWORD_CODE_INVALID = "Entered reset code is invalid ! Please try again.";
        public const string RESET_PASSWORD_CODE_EXPIRED = "Email is invalid or reset code expired ! Please try again";
        #endregion account 
        #region AppUtil
        public static string AppSettings = "appSettings";
        #endregion

        #region FolderCategory
        public const string GET_ALL_CATEGORIES_API = "api/categories/get_all_categories";// must be reuse
        public const string CATEGORIES_DETAILS_API = "api/categories/details_category";
        public const string CATEGORIES_CREATE_API = "api/categories/create_category";
        public const string CATEGORIES_DELETE_API = "api/categories/delete_category";
        public const string COMPANY_ASSOCIATED_TO_INDUSTRY = "api/categories/IsAssociatedCompany";
        public const string CATEGORIES_UPDATE_API = "api/categories/update_category";
        public const string CATEGORIES_CREATE_FROM_FOLDER_API = "api/categories/create_category";
        #endregion FolderCategory

        #region MasterFolder
        public const string GET_ALL_FOLDER_API = "api/masterfolder/get_all_folders";// must be reuse
        public const string GET_ALL_FOLDER_CONTENT_API = "api/masterfolder/get_all_folders_with_content";// must be reuse
        public const string FOLDER_DETAILS_API = "api/masterfolder/details_folder";
        public const string FOLDER_CREATE_API = "api/masterfolder/create_folder";
        public const string FOLDER_UPDATE_API = "api/masterfolder/update_folder";
        public const string FOLDER_DELETE_API = "api/masterfolder/delete_folder";
        public const string GET_FOLDERS_BY_CATEGORY_API = "api/masterfolder/get_folders_by_category";
        public const string VALIDATE_FOLDER_NAME_API = "api/masterfolder/ValidateFolderName";
        #endregion MasterFolder

        #region MasterContent
        public const string GET_ALL_CONTENT_API = "api/mastercontent/get_all_contents";// must be reuse
        public const string MASTER_CONTENT_DETAILS_API = "api/mastercontent/details_content";
        public const string MASTER_CONTENT_CREATE_API = "api/mastercontent/create_content";
        public const string MASTER_CONTENT_UPDATE_API = "api/mastercontent/update_content";
        public const string MASTER_CONTENT_DELETE_API = "api/mastercontent/delete_content";
        public const string GET_MASTER_CONTENT_BY_CATEGORY_API = "api/mastercontent/get_contents_by_category";
        public const string PUBLISH_UNPUBLISH_CONTENT = "api/mastercontent/publishcontent";
        public const string GET_ALL_CONTENT_PREVIEW_POP_UP_API = "api/mastercontent/get_all_contents_for_preveiew_pop_up";
        public const string GET_AWSS3_POLICY = "api/mastercontent/GetAwss3Policy";
        public const string GET_MASTER_FOLDER_CONTENTS = "api/masterfolder/GetMasterFolderContents";

        public const string SAVE_SELECTED_MASTER_CONTENT_FOLDER = "api/MasterContent/SaveSelectedMasterContentFolder";
        public const string UPDATE_MASTER_CONTENT_FOLDER_ORDERS = "api/MasterContent/UpdateMasterContentFolderOrders";
        public const string GET_MASTER_CONTENT_SELECTED_FOLDERS = "api/MasterContent/GetMasterContentSelectedFolders";
        public const string GET_MASTER_CONTENT_BY_TYPE = "api/MasterContent/GetMasterContentsByType";
        #endregion MasterContent

        #region Subscription
        public const string GET_ALL_SUBSCRIPTION_API = "api/subscription/index";// must be reuse
        public const string SUBSCRIPTION_DETAILS_API = "api/subscription/get_subscription";
        public const string SUBSCRIPTION_CREATE_API = "api/subscription/create";
        public const string SUBSCRIPTION_UPDATE_API = "api/subscription/update";
        public const string SUBSCRIPTION_DELETE_API = "api/subscription/delete";
        public const string GET_ALL_SUBSCRIPTION_FOR_ADMIN_API = "api/subscription/GetSubscriptionListForAdmin";
        public const string GET_SUBSCRIPTION_DETAILS_FOR_COMPANY_API = "api/subscription/get_subscription_details_for_company";
        #endregion Subscription

        #region Company
        public const string GET_All_COMPANY = "api/companies/get_all_company";
        public const string GET_USER_INFORMATION = "api/companies/get_users_by_company_id";
        public const string ACTIVATE_DEACTIVATE_COMPANY = "api/companies/activate_deactivate_company";
        public const string SAVE_ADMIN_DETAILS = "api/Account/SaveAdminDetails";
        public const string DELETE_SIGNUP_DETAILS = "api/Account/DeleteSignUpDetails";
        public const string UPDATE_SIGNUP_DETAILS_PAYMENT_SUCCESS = "api/Account/UpdateSignUpDetailsPaymentSuccess";
        public const string SEND_PAYMENT_EMAIL = "api/Account/SendPaymentEmail";
        #endregion Company

        #region Contents
        public const string GET_CONTENTS_BY_FOLDER_API = "api/mastercontent/get_contents_by_folder";
        #endregion

        #region Quiz
        public const string ADD_QUESTION_API = "api/quiz/AddQuestion";
        public const string SAVE_QUIZ = "api/quiz/SaveQuiz";
        public const string GET_QUIZ_DETAILS = "api/quiz/GetQuizDetails";
        public const string Update_QUESTION_API = "api/quiz/UpdateQuestion";
        public const string Get_QUESTION_DETAILS_API = "api/quiz/GetQuestionDetails";
        public const string DELET_QUIZ = "api/quiz/DeleteQuiz";
        public const string DELETE_QUESTION = "api/quiz/DeleteQuestion";
        #endregion

        #region Messages
        public static string INTERNAL_SERVER_ERROR = "Some error occured, Please try later.";
        public static string INVALID_ARGUMENT = "Invalid Argument";
        public const string MODEL_CANNOT_BE_NULL = "Model cannot be null";
        public const string QUIZ_NAME_CANNOT_BE_EMPTY = "Quiz name cannot be empty";
        public const string QUIZ_ID_CANNOT_BE_EMPTY = "Quiz Id cannot be empty";
        public const string QUESTION_ID_CANNOT_BE_EMPTY = "Question Id cannot be empty";
        public const string QUESTION_ADDED = "Question added successfully";
        public const string QUESTION_NOT_ADDED = "Question not added successfully";
        public const string QUIZ_NAME_ALREADY_EXIST = "Quiz name already exist";
        public const string QUIZ_ADDED = "Quiz added successfully";
        public const string QUESTION_UPDATED = "Question updated successfully";
        public const string NO_QUESTION_FOUND = "No question found";
        public const string QUIZ_DELETED = "Quiz deleted successfully";
        public const string NO_QUIZ_FOUND = "No Quiz found";
        public const string QUESTION_DELETED = "Question deleted successfully";
        public const string QUIZ_ALREADY_ASSOCIATED = "There is already a quiz associated with this content";

        public const string COMPANY_ACCOUNT_DEACTIVATED = "Your company account has been deactivated";
        public const string EMAIL_ALREADY_REGISTERED = "This Email is already registered with us";
        public const string SIGNUP_SUCCESSFUL = "Thank you for signing up. Your account has been created successfully";
        public const string PRODUCT_NOT_CREATED_ON_STRIPE = "Product not created on the stripe,Please try again";
        public const string Company_ALREADY_REGISTERED = "This company is already registered with us";
        public const string COMPANY_ACTIVATED = "Your company account is now active. The registered users for the company can login and access the content now.";
        public const string COMPANY_DEACTIVATED = "Your company account is now Inactive. The registered users for the company can no longer login or access the content.";
        public const string IMAGE_ADDED_SUCCESSFULLY = "Image added successfully";
        public const string USER_ROLE_UPDATED_SUCCESSFULLY = "User role updated successfully";
        public const string USER_STATUS_UPDATED_SUCCESSFULLY = "User status updated successfully";
        public const string NOTIFICATION_SETTING_UPDATED_SUCCESSFULLY = "Notification setting updated successfully";
        public const string NOTIFICATION_SETTING_UPDATED_UNSUCCESSFULLY = "Unable to update notification setting at the moment";
        public const string USER_UPDATED_SUCCESSFULLY = "User updated successfully";
        public const string COMPANY_DESCRIPTION_UPDATED_SUCCESSFULLY = "About Company updated successfully";
        public const string USER_ACTIVATED = "This user is active now";
        public const string USER_DEACTIVATED = "This user is inactive now";
        public const string USER_LOGIN_DEACTIVATED = "Your account has been deactivated. Please contact your company admin.";

        public const string EXTERNAL_LOGIN_USER_NOT_REGISTERED = "You are not a registered user";

        public const string USER_LIMIT_EXCEEDED_FOR_COMPANY = "You have reached your maximum limit of the number of users you can add as per your subscription plan";
        public const string SESSION_ID_REQUIRED = "Session Id is required";

        public const string FOLDER_NAME_ALREADY_EXIST = "Folder already exists with same name";
        public const string FOLDER_SHORTNAME_ALREADY_EXIST = "Folder already exist with same short name";

        public const string CONTENT_PUBLISHED_SUCCESSFULLY = "Content Published Successfully";
        public const string CONTENT_UNPUBLISHED_SUCCESSFULLY = "Content Un-published Successfully";
        public const string ATLEAST_ONE_CONTENT_REQUIRED = "Please select atleast one content";
        public const string CONTENT_DELETED_SUCCESSFULLY = "Content successfully deleted";
        public const string CONTENT_ASSOCIATED_TO_PLAYLIST = "This content is in use in one or more playlist";
        public const string CONTENT_BROADCASTED_SUCCESSFULLY = "Content successfully broadcasted";
        public const string CONTENT_UNBROADCASTED_SUCCESSFULLY = "Content successfully unbroadcasted";
        public const string PLAYLIST_DELETED_SUCCESSFULLY = "Playlist successfully deleted";
        public const string EMAIL_REQUIRED = "Email is required";

        public const string INDUSTRY_CANNOT_BE_DELETED = "Industry cannot be deleted,due to have associated folder(s)";
        public const string FOLDER_CANNOT_BE_DELETED = "Folder cannot be deleted,due to have associated folder(s)";
        public const string FOLDER_ID_REQUIRED = "Folder Id is required";

        public const string TEAMS_EMPTY = "No teams found";
        public const string USERS_EMPTY = "No users found";
        public const string TRAINERS_EMPTY = "No trainers found";
        public const string NO_RESULT_FOUND = "No result found";

        public const string SUBSCRIPTION_PLAN_HAS_EXPIRED = "Subscription plan has expired";
        public const string SUBSCRIPTION_PLAN_REACHED_LIMIT = "Subscription plan reached the limit of total number of users";

        public const string NO_TOPIC_FOUND = "No topic found";
        public const string CONTEST_DELETED_SUCCESSFULLY = "Topic deleted succssfully";
        public const string CONTEST_CANNOT_BE_DELETED = "Topic is in use and cannot be deleted";

        public const string NO_FOLDER_SELECTED = "Please select atleast one folder";

        public const string ContentDateExtended = "Broadcasted content date extended";

        public const string PrizeActivatedSuccessfully = "Prize activated successfully";
        public const string PrizeDeactivatedSuccessfully = "Prize deactivated successfully";
        public const string PrizeSuccessfullyAvailed = "Prize Successfully availed. Please find it on your registered email";
        public const string InsufficientBalance = "Sorry you do not have sufficient balance to avail this item";
        public const string PrizeOutOfStock = "Prize went out of stock";
        public const string PRIZE_DELETED_SUCCESSFULLY = "Prize deleted succssfully";
        public const string PRIZE_CANNOT_BE_DELETED = "Prize is in use and cannot be deleted";

        public const string PRIVATE_CONTENT = "This is a private content.";
        public const string HANDLENAME_ALREADY_REGISTERED = "This user handle is already registered with us. Please enter a unique one.";
        public const string TEAMHANDLENAME_ALREADY_REGISTERED = "This Team handle is already registered with us. Please enter a unique one.";

        public const string LOCATION_EXISTS = "Location already exists. Please change the location name.";
        public const string LOCATION_DELETED = "Location deleted successfully.";
        public const string LOCATION_ADDED = "Location added successfully.";
        public const string LOCATION_UPDATED = "Location updated successfully.";
        #endregion

        #region Contents
        public const string GET_ALL_ROLES = "api/role/roles";
        #endregion

        #region Tag
        public const string GET_TAGS_LIST = "api/Tag/get_tags";
        public const string CREATE_TAG = "api/Tag/create_tag";
        public const string GET_TAGS_WITH_INDUSTIRES = "api/Tag/GetTagsWithIndustries";
        public const string EDIT_TAG = "api/Tag/edit_tag";
        public const string GET_TAG = "api/Tag/GetTag";
        public const string DELETE_TAG = "api/Tag/delete_tag";
        #endregion

        #region Company Content
        public const string GET_COMPANY_CONTENT_FOLDERS = "api/CompanyContent/GetCompanyContentFolders";
        #endregion

        #region UserInformation
        public const string GET_ALL_COUNTRIES = "api/userinformation/countries";
        public const string GET_ALL_STATES = "api/userinformation/states";
        public const string GET_ALL_CITIES = "api/userinformation/cities";
        public const string GET_USER_PROFILE = "api/userinformation/GetUserProfile";
        public const string GET_COMPANY_USERS = "api/userinformation/GetCompanyUsers";
        public const string ADD_PROFILE_IMAGE = "api/userinformation/AddProfilemage";
        public const string UPDATE_USER_ROLE = "api/userinformation/UpdateUserRole";
        public const string UPDATE_USER_STATUS = "api/userinformation/UpdateUserStatus";
        public const string CHECK_USER_LIMIT_FOR_COMPANY = "api/userinformation/CheckUserLimitForCompany";
        public const string GET_USER_INVITE = "api/userinformation/GetUserInvite";
        public const string UPDATE_NOTIFICATION_SETTING = "api/userinformation/UpdateNotificationSetting";
        public const string UPDATE_USER_PROFILE = "api/userinformation/UpdateUserProfile";
        public const string GET_USER_PROFILE_STATUS = "api/userinformation/GetUserProfileStatus";
        #endregion

        #region Team
        public const string GET_TEAMS = "api/team/GetTeams";
        #endregion

        #region Payment
        public const string GET_CHECKOUT_SESSION = "api/Payment/GetCheckoutSession";
        public const string GET_CHECKOUT_SESSION_DETAILS = "api/Payment/GetCheckoutSessionDetails";
        public const string PAYMENT_CANCEL = "api/Payment/PaymentCancel";
        public const string PAYMENT_SUCCESS = "api/Payment/PaymentSucess";
        #endregion

        #region Company Quiz
        public const string ADD_COMPANY_QUIZ_QUESTION_API = "api/CompanyQuiz/AddQuestion";
        public const string SAVE_COMPANY_QUIZ = "api/CompanyQuiz/SaveQuiz";
        public const string GET_COMPANY_QUIZ_DETAILS = "api/CompanyQuiz/GetQuizDetails";
        public const string Update_COMPANY_QUIZ_QUESTION_API = "api/CompanyQuiz/UpdateQuestion";
        public const string DELETE_COMPANY_QUIZ = "api/CompanyQuiz/DeleteQuiz";
        public const string DELETE_COMPANY_QUIZ_QUESTION = "api/CompanyQuiz/DeleteQuestion";

        #endregion

        #region NotificationMessages
        public static string TeamUserTitle = "User Assigned";
        public static string TeamUserAdded = "You are assigned to team {0}";
        public static string ProfileDeleted = "Image deleted successfully";

        public static string PlayListAssignedTitle = "New PlayList Assigned";
        public static string PlayListAssigned = "You are assigned to a new playlist '{0}'. <a class='notifycheck blueLink' name='/my-playlist/{0}/{1}/false' id='notificationsLink'>Click Here</a>";

        public static string MessageTitle = "New Message Received";
        public static string MessageBody = "You have received a new message from {0}";

        public static string TagMessage = "A new game '{1}' is created matching your interest. <a class='notifycheck blueLink' name='/brandworks-play/game-details/{0}' id='notificationsLink'>Click Here</a>";
        public static string TagTitle = "A new game created";

        public const string LikeMessage = "User {0} likes your {1} game.";
        public const string LikeTitle = "Like";

        public const string SubscriptionInfoChanged = "Your subscription info has been updated, please check your registered email to know more.";
        public const string SubscriptionChangedTitle = "Subscription info changed";

        public const string WinnerTitle = "Winner";
        public const string WinnerMessage = "You won the topic '{1}'. <a class='notifycheck blueLink' name='/brandworks-play/game-details/{0}' id='notificationsLink'>Click Here</a>";

        public const string BroadcastTitle = "Broadcast Extenstion";
        public const string BroadcastMessage = "Your broadcasted topic '{2}' is expired. Please click here to extend your expiry by 120 days.  <a class='notifycheck blueLink' id='extenddate' value='Extend' valueType='{0}' contestId='{1}'>Click Here</a>";

        public const string ReportAbuseTitle = "Topic Reported";
        public const string ReportAbuseMessage = "A topic has been reported. Please take required action.";
        #endregion

        //Default Profile Picture
        public const string DEFAULT_PROFILE_PICTURE = "no-profile.png";
        public const string DEFAULT_PRIZE_THUMBNAIL = "default-prize-thumbnail.png";

        public const string DEFAULT_SORTING_COMPANY_USERS = "FirstName";

        //Location Filter dropdown options list limit
        public static int LimitDropDownSize = 25;

        //S3 url request expires in
        public static int UrlRequest_ExpiresIn = 10;

        //file extentions
        public static string[] EXTENSIONS = new string[] { ".mp4", ".mov", ".3gp", ".mkv", ".MKV", ".webm", ".WEBM" , ".MOV", ".pdf", ".doc", ".txt" };

        //Quiz Evaluation scheme
        public static int CorrectAnswerMarks = 5;
        public static int WrongAnswerMarks = 1;
        public static int PassingPercentage = 75;

        //Return value for already existing quiz
        public static long QUIZ_ALREADY_EXISTS = -1;

        //allowed view Count For Same User With Different IP
        public static int ViewCountForSameUserWithDifferentIP = 5;

        //Points allocation for contest
        public static int ParticipantWinner = 100;
        public static int CreateContest = 25;
        public static int RemovePointsForReportedVideo = 25;
        public static int HundredViewsContest = 50;
        public static int ViewContest = 5;

        //number of views required for getting award points
        public static int NumberOfViewsForGettingPoints = 10; // for testing = 10 , original value = 100

        //top ten topPerformer
        public static int TopTen = 10;

        public static string Contest = "contest";
        public static string ContestEntry = "contestentry";
        public static string CompanyLibraryContent = "companylibrarycontent";
        public static string VbPlayMimeType = "video/mp4";


        public const string GREEN_COLOR_CODE = "#78C000";
        public const string YELLOW_COLOR_CODE = "#da9928";
        public const string RED_COLOR_CODE = "#d65656";
        public enum UserRoles
        {
            SuperAdmin = 1,
            Admin = 2,
            Trainer = 3,
            User = 4
        }

        public const string XsrfKey = "XsrfKeyId";

        public enum PlanType
        {
            [Display(Name = "Annually")]
            Yearly = 1,
            Monthly = 2
        }
        public const string TRIAL_PLAN = "trial";
        public const string TRIAL_PLAN_BILLING_CYCLE = "None";

        public const string FirstQuiz = "First Quiz Point";

        public const string SUGGESTION_TYPE_USER = "user";
        public const string SUGGESTION_TYPE_TAGS = "tags";


        public enum RoleType
        {
            Trainer = 1,
            User = 2,
            All = 3
        }
        public enum ResetPasswordResponse
        {
            Invalid = 1,
            Success = 2,
            Expired = 3
        }

        public enum PublishedOrUnpublish
        {
            Published = 1,
            Unpublished = 2,
            ContentNotFound = 3
        }

        public enum ContentType
        {
            Image = 1,
            Video = 2,
            Document = 3
        }

        public enum BuildPlaylistStage
        {
            One = 1,
            Two = 2,
            Three = 3,
            Four = 4,
            Five = 5,
        }

        public enum MessageType
        {
            Image = 1,
            Text = 2
        }

        public enum NotificationType
        {
            AdddedInTeam = 1,
            NewContentAdded = 2
        }

        public enum ContestType
        {
            AllGames = 1,
            JoinedGames = 2,
            YourGames = 3,
            TaggedGames = 4
        }

        public enum SharedContentType
        {
            CompanyContent = 1,
            Contest = 2,
            ContestEntry = 3
        }

        public enum S3Folders
        {
            MessageThumbnail = 1,
            CompanyLogo = 2,
            ProfilePicture = 3,
            VBPlay = 4,
            PrizeThumbnail = 5,
            thumbnails = 6,
            Root = 7
        }

        public enum SavePlaylistFolderStatus
        {
            PlaylistDoNotExist = 1,
            FolderCountZero = 2,
            OK = 3
        }

        public enum PointsEarnedType
        {
            Participant = 1,
            Create = 2,
            Views = 3,
            Redeemed = 4
        }

        //Prize categoryies
        public enum PrizeCategory
        {
            TimeOffPrize = 1,
            Voucher = 2,
            GiftCard = 3,
            InHouseFoodCertification = 4,
            CashPrize = 5
        }

        //AvailPrize status
        public enum AvailPrizeStatus
        {
            InsufficientBalance = 1,
            NoCodeAvailable = 2,
            SuccessfullyAvailed = 3
        }

        public enum PrizeFilter
        {
            Name = 1,
            MaxPoints = 2,
            MinPoints = 3,
            EndDate = 4
        }

        public enum LeaderboardFilter
        {
            Name = 1,
            MaxCompletion = 2,
            MinCompletion = 3,
        }

        public enum LeaderboardCompletionSegregation
        {
            SeventyFive = 75,
            Fifty = 50,
            TwentyFive = 25
        }

        //enum for content visibility
        public enum ContentVisibility
        {
            Private = 1,
            Public = 2
        }


        public const string SA_LOGIN_REQUEST_KEY = "ADAD23MNB4QAWD44234^%$^";

        // public static string PROFILE_IMAGE_PATH = ConfigurationManager.AppSettings["ProfileImagePath"].ToString();

        public const string UnitedStatesFilter = "United States";
    }
}