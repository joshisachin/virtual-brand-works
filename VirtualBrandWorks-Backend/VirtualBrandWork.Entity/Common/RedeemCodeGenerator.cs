﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBrandWork.Entity.Common
{
    public class RedeemCodeGenerator
    {
        public static string GenerateRedeemCode()
        {
            string guid = Guid.NewGuid().ToString();
            string unixTimestamp = Convert.ToString((int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
            guid = guid + "_" + unixTimestamp;
            return guid;
        }
    }
}
