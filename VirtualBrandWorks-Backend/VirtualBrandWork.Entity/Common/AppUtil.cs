﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using VirtualBrandWork.Entity;

namespace VirtualBrandWork.Common
{
    public static class AppUtil
    {
        //Holds the collection of all app setting values
        private static readonly NameValueCollection applicationSettings = null;

        static AppUtil()
        {
            applicationSettings = ConfigurationManager.GetSection(Constants.AppSettings) as NameValueCollection;
        }

        public static string EmailId => applicationSettings["EmailId"];
        public static string CompanyName => applicationSettings["CompanyName"];
        public static string EmailPassword => applicationSettings["EmailPassword"];
        public static string BaseUrl => applicationSettings["BaseUrl"];
        public static string SignUpUrl => applicationSettings["SignUpUrl"];
        public static string ProfileImagesUrl => applicationSettings["ProfileImagesUrl"];
        public static string ProfileImagesFolder => applicationSettings["ProfileImagesFolder"];
        public static string PdfThumbnail => applicationSettings["PdfThumbnail"];
        public static string DocThumbnail => applicationSettings["DocThumbnail"];
        public static string TxtThumbnail => applicationSettings["TxtThumbnail"];
        public static string FcmPushNotificationsURL => applicationSettings["FcmPushNotificationsURL"];
        public static string FcmServerKey => applicationSettings["FcmServerKey"];
        public static string FcmSenderId => applicationSettings["FcmSenderId"];
        //public static bool IsHausseBaisseRuleActive => string.IsNullOrEmpty(applicationSettings["IsHausseBaisseRuleActive"]) ? true : Convert.ToBoolean(applicationSettings["IsHausseBaisseRuleActive"]);
        //public static bool Is50PercentRuleActive => string.IsNullOrEmpty(applicationSettings["Is50PercentRuleActive"]) ? true : Convert.ToBoolean(applicationSettings["Is50PercentRuleActive"]);

        ///// <summary>
        ///// This is used to define the minimum number of rows in a column that are requirred to draw 50% rule
        ///// </summary>
        //public static int MinimumRowCountFor50Percent => string.IsNullOrEmpty(applicationSettings["MinimumRowCountFor50Percent"]) ? 5 : Convert.ToInt32(applicationSettings["MinimumRowCountFor50Percent"]);

        ///// <summary>
        ///// This is used to define the maximum number of rows in a column that are requirred to draw 50% rule
        ///// If number of rows exceeds this variable value then rule will be deactivated
        ///// </summary>
        //public static int MaximumRowCountFor50Percent => string.IsNullOrEmpty(applicationSettings["MaximumRowCountFor50Percent"]) ? 12 : Convert.ToInt32(applicationSettings["MaximumRowCountFor50Percent"]);

        ///// <summary>
        ///// This is used to define the minimum or more number of consecutive signals i.e. Hauss/Hausse against the trend
        ///// </summary>
        //public static int MinConsecutiveSignalsInARow => string.IsNullOrEmpty(applicationSettings["MinConsecutiveSignalsInARow"]) ? 2 : Convert.ToInt32(applicationSettings["MinConsecutiveSignalsInARow"]);

        ///// <summary>
        ///// This is used to check weather notification needs to send or not
        ///// </summary>
        //public static bool IsNotificationEnabled => string.IsNullOrEmpty(applicationSettings["IsNotificationEnabled"]) ? true : Convert.ToBoolean(applicationSettings["IsNotificationEnabled"]);

        ///// <summary>
        ///// This is used to send sender application of notification
        ///// </summary>
        //public static string ApplicationName => applicationSettings["ApplicationName"];

        ///// <summary>
        ///// This is used to get Binance API key
        ///// </summary>       
        //public static string BinanceAPIKey => applicationSettings["BinanceAPIKey"];

        ///// <summary>
        ///// This is used to get Binance API secret key
        ///// </summary>
        //public static string BinanceSecretKey => applicationSettings["BinanceSecretKey"];

        ///// <summary>
        ///// This is used to get Binance API key
        ///// </summary>       
        //public static string BinanceOldAPIKey => applicationSettings["BinanceOldAPIKey"];

        ///// <summary>
        ///// This is used to get Binance API secret key
        ///// </summary>
        //public static string BinanceOldSecretKey => applicationSettings["BinanceOldSecretKey"];

        ///// <summary>
        ///// This is used to get BitMEX API key
        ///// </summary>
        //public static string BitMEXAPIKey => applicationSettings["BitMEXAPIKey"];

        ///// <summary>
        ///// This is used to get BitMEX API secret key
        ///// </summary>
        //public static string BitMEXSecretKey => applicationSettings["BitMEXSecretKey"];

        ///// <summary>
        ///// This is used to pass same price for the list of symbols
        ///// </summary>
        //public static string UseSamePriceForSymbols => Convert.ToString(applicationSettings["UseSamePriceForSymbols"]);


        //public static int HourlyErrorEmailThresholdCount => string.IsNullOrEmpty(applicationSettings["HourlyErrorEmailThresholdCount"]) ? 50 : Convert.ToInt32(applicationSettings["HourlyErrorEmailThresholdCount"]);

        //public static int MaximumErrorEmailsInAnHour => string.IsNullOrEmpty(applicationSettings["MaximumErrorEmailsInAnHour"]) ? 12 : Convert.ToInt32(applicationSettings["MaximumErrorEmailsInAnHour"]);

        //public static int EmailThresholdDurationInHour => string.IsNullOrEmpty(applicationSettings["EmailThresholdDurationInHour"]) ? 1 : Convert.ToInt32(applicationSettings["EmailThresholdDurationInHour"]);

        ///// <summary>
        ///// This is used to allow fetch data from BitMEX exchange
        ///// </summary>
        //public static bool AllowBitMEXDataFetch => string.IsNullOrEmpty(applicationSettings["AllowBitMEXDataFetch"]) ? false : Convert.ToBoolean(applicationSettings["AllowBitMEXDataFetch"]);

        ///// <summary>
        ///// This is used to allow fetch data from Binance exchange
        ///// </summary>
        //public static bool AllowBinanceDataFetch => string.IsNullOrEmpty(applicationSettings["AllowBinanceDataFetch"]) ? false : Convert.ToBoolean(applicationSettings["AllowBinanceDataFetch"]);

        ///// <summary>
        ///// This is used to add buffer rows below and above the start and end point
        ///// </summary>
        //public static int BufferRowsCount => string.IsNullOrEmpty(applicationSettings["BufferRowsCount"]) ? 100 : Convert.ToInt32(applicationSettings["BufferRowsCount"]);

        ///// <summary>
        ///// This is used to validate if our db is reaching the maximum db size
        ///// </summary>
        //public static int MaxDBSize => string.IsNullOrEmpty(applicationSettings["MaxDBSize"]) ? 100 : Convert.ToInt32(applicationSettings["MaxDBSize"]);

        ///// <summary>
        ///// This is used to validate if our unallocated space is reaching the beyond targeted size
        ///// </summary>
        //public static int MinUnAllocatedSpace => string.IsNullOrEmpty(applicationSettings["MinUnAllocatedSpace"]) ? 600 : Convert.ToInt32(applicationSettings["MinUnAllocatedSpace"]);

        ///// <summary>
        ///// This will be used to check if we can write draw down details in excel or not.
        ///// </summary>
        //public static int DrawDownRecordThresholdCount => string.IsNullOrEmpty(applicationSettings["DrawDownRecordThresholdCount"]) ? 1048355 : Convert.ToInt32(applicationSettings["DrawDownRecordThresholdCount"]);

        ///// <summary>
        ///// This will be used to check TickGapDetectionTimeInSeconds.
        ///// </summary>
        //public static int TickGapDetectionTimeInSeconds => string.IsNullOrEmpty(applicationSettings["TickGapDetectionTimeInSeconds"]) ? 300 : Convert.ToInt32(applicationSettings["TickGapDetectionTimeInSeconds"]);

        ///// <summary>
        ///// Trading system details page url
        ///// </summary>
        //public static string TradingSystemDetailUrl => string.IsNullOrEmpty(applicationSettings["TradingSystemDetailUrl"]) ? "http://dev.bxbmoney.com/Dashboard/TradingSystemDetails/" : applicationSettings["TradingSystemDetailUrl"];

        //public static string BinanceFutureAPIPrefix => string.IsNullOrEmpty(applicationSettings["BinanceFutureAPIPrefix"]) ? "https://testnet.binancefuture.com/fapi" : applicationSettings["BinanceFutureAPIPrefix"];

        //public static string BitMaxAPIPrefix => string.IsNullOrEmpty(applicationSettings["BitMaxAPIPrefix"]) ? "http://bitmex.com/api" : applicationSettings["BitMaxAPIPrefix"];

        //public static string BinanceAPIPrefix => string.IsNullOrEmpty(applicationSettings["BinanceAPIPrefix"]) ? "https://api.binance.com/api" : applicationSettings["BinanceAPIPrefix"];

        //public static string ServerName => string.IsNullOrEmpty(applicationSettings["ServerName"]) ? "Dev1" : applicationSettings["ServerName"];

        ///// <summary>
        ///// Binance error codes to avoid retry on create order exceptions
        ///// </summary>
        //public static string BinanceErrorCodesToAvoidRetry => string.IsNullOrEmpty(applicationSettings["BinanceErrorCodesToAvoidRetry"]) ? "1000,1006,1007,1010" : applicationSettings["BinanceErrorCodesToAvoidRetry"];

        //public static string Secret => string.IsNullOrEmpty(applicationSettings["Secret"]) ? "b14ca5898a4e4133bbce2ea2315a1916" : applicationSettings["Secret"];

        ///// <summary>
        ///// This is used to get the DB query timeout for DB migration queries. Default to 300 seconds
        ///// </summary>
        //public static int DBMigrationQueryTimeout => string.IsNullOrEmpty(applicationSettings["DBMigrationQueryTimeout"]) ? 300 : Convert.ToInt32(applicationSettings["DBMigrationQueryTimeout"]);


        ///// <summary>
        ///// This is used to get the table name used in DB migration
        ///// </summary>
        //public static string DBMigrationTickTable => string.IsNullOrEmpty(applicationSettings["DBMigrationTickTable"]) ? "tblTradingData" : Convert.ToString(applicationSettings["DBMigrationTickTable"]);


    }
}