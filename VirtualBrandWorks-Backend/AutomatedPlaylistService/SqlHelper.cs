﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace VirtualBrandWork.AutomatedService
{
    public class SqlHelper
    {
        // Internal members
        protected string _connString = null;
        protected SqlConnection _conn = null;
        protected SqlTransaction _trans = null;
        protected bool _disposed = false;

        /// <summary>
        /// Sets or returns the connection string use by all instances of this class.
        /// </summary>
        public static string ConnectionString { get; set; }

        /// <summary>
        /// Returns the current SqlTransaction object or null if no transaction
        /// is in effect.
        /// </summary>
        public SqlTransaction Transaction { get { return _trans; } }

        /// <summary>
        /// Constructor using global connection string.
        /// </summary>
        public SqlHelper()
        {
            _connString = ConfigurationManager.ConnectionStrings["VirtualBrandWorks"].ConnectionString;
            Connect();
        }

        /// <summary>
        /// Constructure using connection string override
        /// </summary>
        /// <param name="connString">Connection string for this instance</param>
        public SqlHelper(string connString)
        {
            _connString = connString;
            Connect();
        }

        // Creates a SqlConnection using the current connection string
        protected void Connect()
        {
            _conn = new SqlConnection(_connString);

            if (_conn.State == ConnectionState.Closed)
            {
                _conn.Open();
            }
        }

        /// <summary>
        /// Constructs a SqlCommand with the given parameters. This method is normally called
        /// from the other methods and not called directly. But here it is if you need access
        /// to it.
        /// </summary>
        /// <param name="qry">SQL query or stored procedure name</param>
        /// <param name="type">Type of SQL command</param>
        /// <param name="args">Query arguments. Arguments should be in pairs where one is the
        /// name of the parameter and the second is the value. The very last argument can
        /// optionally be a SqlParameter object for specifying a custom argument type</param>
        /// <returns></returns>
        public SqlCommand CreateCommand(string qry, CommandType type, params object[] args)
        {
            SqlCommand cmd = new SqlCommand(qry, _conn);

            cmd.CommandTimeout = 2500;

            // Associate with current transaction, if any
            if (_trans != null)
            {
                cmd.Transaction = _trans;
            }

            // Set command type
            cmd.CommandType = type;

            // Construct SQL parameters
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] is string && i < (args.Length - 1))
                {
                    SqlParameter parm = new SqlParameter();
                    parm.ParameterName = (string)args[i];
                    parm.Value = args[++i];
                    cmd.Parameters.Add(parm);
                }
                else if (args[i] is SqlParameter)
                {
                    cmd.Parameters.Add((SqlParameter)args[i]);
                }
                else
                {
                    throw new ArgumentException("Invalid number or type of arguments supplied");
                }
            }
            return cmd;
        }

        #region Exec Members

        /// <summary>
        /// Executes a query that returns no results
        /// </summary>
        /// <param name="qry">Query text</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>The number of rows affected</returns>
        public int ExecNonQuery(string qry, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
                {
                    cmd.CommandTimeout = 2500;

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecNonQuery:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }
                return -1;
            }
            finally
            {
                _conn.Close();
            }
        }

        /// <summary>
        /// Executes a stored procedure that returns no results
        /// </summary>
        /// <param name="proc">Name of stored proceduret</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>The number of rows affected</returns>
        public int ExecNonQueryProc(string proc, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(proc, CommandType.StoredProcedure, args))
                {
                    cmd.CommandTimeout = 2500;

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecNonQueryProc:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }

                return -1;
            }
            finally
            {
                _conn.Close();
            }
        }

        /// <summary>
        /// Executes a query that returns a single value
        /// </summary>
        /// <param name="qry">Query text</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Value of first column and first row of the results</returns>
        public object ExecScalar(string qry, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
                {
                    cmd.CommandTimeout = 2500;

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }

                    return cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecScalar:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }
                return -1;
            }
            finally
            {
                _conn.Close();
            }
        }

        /// <summary>
        /// Executes a query that returns a single value
        /// </summary>
        /// <param name="proc">Name of stored proceduret</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Value of first column and first row of the results</returns>
        public object ExecScalarProc(string qry, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(qry, CommandType.StoredProcedure, args))
                {
                    cmd.CommandTimeout = 2500;

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }

                    return cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecScalarProc:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }
                return -1;
            }
            finally
            {
                _conn.Close();
            }
        }

        /// <summary>
        /// Executes a query and returns the results as a SqlDataReader
        /// </summary>
        /// <param name="qry">Query text</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a SqlDataReader</returns>
        public SqlDataReader ExecDataReader(string qry, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
                {
                    cmd.CommandTimeout = 2500;

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }

                    return cmd.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecDataReader:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }

        /// <summary>
        /// Executes a stored procedure and returns the results as a SqlDataReader
        /// </summary>
        /// <param name="proc">Name of stored proceduret</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a SqlDataReader</returns>
        public SqlDataReader ExecDataReaderProc(string qry, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(qry, CommandType.StoredProcedure, args))
                {
                    cmd.CommandTimeout = 2500;

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }

                    return cmd.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecDataReaderProc:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }

        /// <summary>
        /// Executes a query and returns the results as a DataSet
        /// </summary>
        /// <param name="qry">Query text</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a DataSet</returns>
        public DataSet ExecDataSet(string qry, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(qry, CommandType.Text, args))
                {
                    cmd.CommandTimeout = 2500;
                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }

                    adapt.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecDataSet:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }

        /// <summary>
        /// Executes a stored procedure and returns the results as a Data Set
        /// </summary>
        /// <param name="proc">Name of stored proceduret</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a DataSet</returns>
        public DataSet ExecDataSetProc(string qry, params object[] args)
        {
            try
            {
                using (SqlCommand cmd = CreateCommand(qry, CommandType.StoredProcedure, args))
                {
                    cmd.CommandTimeout = 2500;

                    if (_conn.State == ConnectionState.Closed)
                    {
                        _conn.Open();
                    }

                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapt.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToFile("Exception in ExecDataSetProc:-" + ex.Message);

                if (_conn.State == ConnectionState.Open)
                {
                    _conn.Close();
                }
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }

        #endregion

        #region Transaction Members

        /// <summary>
        /// Begins a transaction
        /// </summary>
        /// <returns>The new SqlTransaction object</returns>
        public SqlTransaction BeginTransaction()
        {
            Rollback();
            _trans = _conn.BeginTransaction();
            return Transaction;
        }

        /// <summary>
        /// Commits any transaction in effect.
        /// </summary>
        public void Commit()
        {
            if (_trans != null)
            {
                _trans.Commit();
                _trans = null;
            }
        }

        /// <summary>
        /// Rolls back any transaction in effect.
        /// </summary>
        public void Rollback()
        {
            if (_trans != null)
            {
                _trans.Rollback();
                _trans = null;
            }
        }

        #endregion
    }
}
