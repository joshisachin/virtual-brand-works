﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using VirtualBrandWork.AutomatedService;

namespace AutomatedPlaylistService
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer();

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Logger.WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 300000; //number in milisecinds  
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            Logger.WriteToFile("Service is stopped at " + DateTime.Now);
        }

        public void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                Logger.WriteToFile("Service is recall at " + DateTime.Now);
                Logger.WriteToFile("Updating the playlist points for First, Second, Third and Runner UP");
                UpdatePlayListPointsPosition();
                UpdatePlayListPoints();
                UpdateContestResult();
                ContentDateExtendNotification();
            }
            catch(Exception ex)
            {
                Logger.WriteToFile(ex.Message);
            }
        }

        private void UpdatePlayListPointsPosition()
        {
            SqlHelper objSQLHelper = new SqlHelper();

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@calculatePosition";
            param.Value = 1;

            objSQLHelper.ExecNonQueryProc("PlaylistPointsUpdate", param);
            Logger.WriteToFile("PlaylistPointsUpdate called");

        }

        private void UpdatePlayListPoints()
        {
            SqlHelper objSQLHelper = new SqlHelper();

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@calculatePosition";
            param.Value = 0;

            objSQLHelper.ExecNonQueryProc("PlaylistPointsUpdate", param);
            Logger.WriteToFile("PlaylistPointsUpdate called");
        }

        private void UpdateContestResult()
        {
            SqlHelper objSQLHelper = new SqlHelper();
            objSQLHelper.ExecNonQueryProc("UpdateContestResult");
            Logger.WriteToFile("UpdateContestResult called");
        }

        private void ContentDateExtendNotification()
        {
            SqlHelper objSQLHelper = new SqlHelper();
            objSQLHelper.ExecNonQueryProc("ContentDateExtendNotification");
            Logger.WriteToFile("ContentDateExtendNotification called");
        }
    }
}
