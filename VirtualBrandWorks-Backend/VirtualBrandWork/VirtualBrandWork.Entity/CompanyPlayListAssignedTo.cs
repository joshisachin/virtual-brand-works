//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VirtualBrandWork.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompanyPlayListAssignedTo
    {
        public long CompanyPlayListAssignedToId { get; set; }
        public long CompanyPlayListId { get; set; }
        public bool IsAssignedToTeam { get; set; }
        public Nullable<long> TeamId { get; set; }
        public Nullable<long> UserId { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual CompanyPlayList CompanyPlayList { get; set; }
        public virtual Team Team { get; set; }
        public virtual User User { get; set; }
    }
}
