﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Repository.Interfaces;
using Repository.Repository;
using Service;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork.Filters
{
    public class AuthenticationModule
    {
        private const string communicationKey = "GQDstc21ewfffffffffffFiwDffVvVBrk";
        SecurityKey signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(communicationKey));

        public static IServiceProvider Container { get; private set; }

        private ServiceCollection serviceCollection = null;

        public AuthenticationModule()
        {
            serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IUserInformationRepository, UserInformationRepository>();
            serviceCollection.AddSingleton<IAWSS3FileStorage, AWSS3FileStorage>();
            serviceCollection.AddSingleton<IUserRepository, UserRepository>();
            serviceCollection.AddSingleton<ISubscriptionRepository, SubscriptionRepository>();
            serviceCollection.AddSingleton<IStripePaymentUtility, StripePaymentUtility>();
        }

        // The Method is used to generate token for user
        public string GenerateTokenForUser(UserContext user, string statusCode)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(communicationKey));
            var now = DateTime.UtcNow;
            var signingCredentials = new SigningCredentials(signingKey,
               SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);

            var claimsIdentity = new ClaimsIdentity(new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.Role, user.RoleId.ToString()),
                new Claim("StatusCode",statusCode)
            }, "local");

            var securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                Audience = "virtual brand works",
                Issuer = "self",
                Subject = claimsIdentity,
                SigningCredentials = signingCredentials,
                Expires = now.AddHours(12),
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var plainToken = tokenHandler.CreateToken(securityTokenDescriptor);
            var signedAndEncodedToken = tokenHandler.WriteToken(plainToken);
            return signedAndEncodedToken;
        }

        /// Using the same key used for signing token, user payload is generated back
        public JwtSecurityToken GenerateUserClaimFromJWT(string authToken)
        {

            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidAudiences = new string[]
                      {
                    "virtual brand works",
                      },

                ValidIssuers = new string[]
                  {
                      "self",
                  },
                // set ClockSkew is zero
                ClockSkew = TimeSpan.Zero,
                IssuerSigningKey = signingKey
            };
            var tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken validatedToken;

            try
            {

                tokenHandler.ValidateToken(authToken, tokenValidationParameters, out validatedToken);
            }
            catch (Exception ex)
            {
                return null;

            }

            return validatedToken as JwtSecurityToken;

        }

        public JWTAuthenticationIdentity PopulateUserIdentity(JwtSecurityToken userPayloadToken)
        {
            string name = ((userPayloadToken)).Claims.FirstOrDefault(m => m.Type == "unique_name").Value;
            string userId = ((userPayloadToken)).Claims.FirstOrDefault(m => m.Type == "nameid").Value;
            string roleId = ((userPayloadToken)).Claims.FirstOrDefault(m => m.Type == "role").Value;
            string statusCode = ((userPayloadToken)).Claims.FirstOrDefault(m => m.Type == "StatusCode").Value;
            return new JWTAuthenticationIdentity(name) { UserId = Convert.ToInt32(userId), UserName = name, RoleId = Convert.ToInt64(roleId), StatusCode = statusCode };
        }

        public UserProfileViewModel GetUserProfileStatus(long userId)
        {
            serviceCollection.AddTransient<UserInformationService>();
            Container = serviceCollection.BuildServiceProvider();
            var userInfoService = Container.GetService<UserInformationService>();
            return userInfoService.GetUserProfileStatus(new UserProfileViewModel { UserId = userId });
        }

        public bool UserLoginStatus(long userId, string statusCode)
        {
            serviceCollection.AddTransient<UserService>();
            Container = serviceCollection.BuildServiceProvider();
            var userService = Container.GetService<UserService>();
            return userService.UserLoginStatus(userId, statusCode);
        }

        public JwtSecurityToken GetUserClaimFromExpiredToken(string authToken)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, // You might need to validate this one depending on your case
                ValidateIssuer = false,
                ValidateActor = false,
                ValidateLifetime = false, // Do not validate lifetime here
                IssuerSigningKey = signingKey
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken validatedToken;

            try
            {

                tokenHandler.ValidateToken(authToken, tokenValidationParameters, out validatedToken);
            }
            catch (Exception ex)
            {
                return null;

            }

            return validatedToken as JwtSecurityToken;
        }
    }
}