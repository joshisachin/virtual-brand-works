﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork.Filters
{
    public class SessionExpire : ActionFilterAttribute, IAuthenticationFilter
    {
        private readonly APIClient apiClient = new APIClient();
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if (!filterContext.HttpContext.Request.Cookies.AllKeys.Contains("JWTToken"))
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        //Runs after the OnAuthentication method  
        //------------//
        //OnAuthenticationChallenge:- if Method gets called when Authentication or Authorization is 
        //failed and this method is called after
        //Execution of Action Method but before rendering of View
        //------------//
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            //We are checking Result is null or Result is HttpUnauthorizedResult 
            // if yes then we are Redirect to Error View
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                                                {
                                                    {"controller", "Account"},
                                                    {"action", "Logout"}
                                                });
            }
            //First get user claims    
            var claims = ClaimsPrincipal.Current.Identities.FirstOrDefault().Claims.ToList();
            if(claims != null)
            {
                //Filter specific claim    
                var userId = claims?.FirstOrDefault(x => x.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", StringComparison.OrdinalIgnoreCase))?.Value;
                var role = claims?.FirstOrDefault(x => x.Type.Equals("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", StringComparison.OrdinalIgnoreCase))?.Value;
                if(!String.IsNullOrEmpty(userId) && !String.IsNullOrEmpty(role))
                {
                    if (!role.Equals(Constants.UserRoles.SuperAdmin.ToString()))
                    {
                        Uri uri = new Uri(Entity.Constants.BASE_URI, Entity.Constants.VALIDATE_COMPANY);
                        Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(new UserContext { UserId = Convert.ToInt64(userId), UserRole = role }));
                        if (response.Data == null)
                        {
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                                                {
                                                    {"controller", "Account"},
                                                    {"action", "Logout"}
                                                });
                        }
                    }
                }

            }
        }
            
        }
    }