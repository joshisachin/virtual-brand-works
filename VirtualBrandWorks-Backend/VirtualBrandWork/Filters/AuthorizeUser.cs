﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;

namespace VirtualBrandWork.Filters
{
    public class AuthorizeUser : AuthorizeAttribute
    {
        // Custom property
        public string Role { get; set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            bool isInRole = false;
            string authHeader = FetchFromHeader(actionContext);
            if(authHeader != null)
            {
                var auth = new AuthenticationModule();
                JwtSecurityToken userPayloadToken = auth.GenerateUserClaimFromJWT(authHeader);

                if(userPayloadToken != null)
                {
                    var identity = auth.PopulateUserIdentity(userPayloadToken);
                    if (identity != null)
                    {
                        Constants.UserRoles role = (Constants.UserRoles)identity.RoleId;
                        isInRole = Role.ToLower().Contains(role.ToString().ToLower());

                    }
                }
            }
            return isInRole;
        }

        private string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            var authRequest = actionContext.Request.Headers.Authorization;
            if (authRequest != null)
            {
                requestToken = authRequest.Parameter;
            }

            return requestToken;
        }
    }
}