﻿using Newtonsoft.Json;
using Service;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using VirtualBrandWork.Common;
using VirtualBrandWork.Controllers;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork.Filters
{
    public class JWTAuthenticationFilter : AuthorizationFilterAttribute
    {
        private readonly APIClient apiClient = new APIClient();
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(JWTAuthenticationFilter));
        public override void OnAuthorization(HttpActionContext filterContext)
        {

            if (!IsUserAuthorized(filterContext))
            {
                ShowAuthenticationError(filterContext);
                return;
            }
            base.OnAuthorization(filterContext);
        }
        public bool IsUserAuthorized(HttpActionContext actionContext)
        {
            try
            {
                var authHeader = FetchFromHeader(actionContext); //fetch authorization token from header

                if (authHeader != null)
                {
                    var auth = new AuthenticationModule();
                    JwtSecurityToken userPayloadToken = auth.GenerateUserClaimFromJWT(authHeader);

                    if (userPayloadToken != null)
                    {
                        var identity = auth.PopulateUserIdentity(userPayloadToken);
                        string[] roles = { "All" };
                        var genericPrincipal = new GenericPrincipal(identity, roles);
                        Thread.CurrentPrincipal = genericPrincipal;
                        var authenticationIdentity = Thread.CurrentPrincipal.Identity as JWTAuthenticationIdentity;
                        if (authenticationIdentity != null && !String.IsNullOrEmpty(authenticationIdentity.UserName))
                        {
                            authenticationIdentity.UserId = identity.UserId;
                            authenticationIdentity.UserName = identity.UserName;
                        }
                        return ValidateUserStatus(identity.UserId, identity.RoleId, identity.StatusCode);
                        //return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return false;
            }
        }
        private static void ShowAuthenticationError(HttpActionContext filterContext)
        {
            var response = new Response() { Status = false, Message = "Unauthorized, please contact your company admin" };
            filterContext.Response =
            filterContext.Request.CreateResponse(HttpStatusCode.Unauthorized, response);
        }
        private string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            var authRequest = actionContext.Request.Headers.Authorization;
            if (authRequest != null)
            {
                requestToken = authRequest.Parameter;
            }

            return requestToken;
        }
        private bool ValidateUserStatus(long userId, long roleId, string statusCode)
        {
            var auth = new AuthenticationModule();

            UserProfileViewModel response = auth.GetUserProfileStatus(userId);

            bool userLoginStatus = auth.UserLoginStatus(userId, statusCode);

            if (response == null || !userLoginStatus)
                return false;
            else if ((response.RoleId != roleId || !response.Status) || !userLoginStatus)
                return false;
            else return true;
        }
    }
}