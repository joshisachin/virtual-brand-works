$(document).ready(function(){


    $('.toggleBtn').click(function(){
        if($('.sideNav').hasClass("toggleNav")){
            $('.sideNav').removeClass("toggleNav");
            $('.rightContent').removeClass('mainMaxWidth');
        } else {
            $('.sideNav').addClass("toggleNav");
            
            $('.rightContent').addClass('mainMaxWidth');
        }
    });
	
    $( function() {
      $( "#sortable" ).sortable();
      $( "#sortable" ).disableSelection();
    } );
    
    $(".otherFoldr .libCol:nth-child(4n+1)").addClass("blueGrn")
    $(".otherFoldr .libCol:nth-child(4n+2)").addClass("violetBlu")
    $(".otherFoldr .libCol:nth-child(4n+3)").addClass("orngYelw")
    $(".otherFoldr .libCol:nth-child(4n+4)").addClass("redPink")




    $(window).bind("resize", function () {    
        if ($(this).width() < 1100) {
            $('.sideNav').addClass('toggleNav')
        } else {
            $('.sideNav').removeClass('toggleNav')
        }
    }).trigger('resize');


	// stickey header

	$(window).scroll(function() {
		var sticky = $('.headerWrap'),
			scroll = $(window).scrollTop();
		if (scroll > 101 ) sticky.addClass('fixedHeader');
		else sticky.removeClass('fixedHeader');
	});




    $(document).on("scroll", onScroll);
    
    //smoothscroll
    $('.navBeforLogin a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('select');
        })
        $(this).addClass('select');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 500, 'swing', function () {
            window.location.hash = target;
            // $(document).on("scroll", onScroll);
            if (target == '#banner'){
                // $(window).scrollTop() = 0;
                $("html, body").animate({scrollTop: 0}, 100);
            }
            else{
                $(document).on("scroll", onScroll);
            }
        });
    });

    $(window).scroll(function(){ 
       if($(window).scrollTop() == ($(document).height() - $(window).height())){
         $('.navBeforLogin a[href^="#"]').each(
            function(e) {
                $(this).removeClass('select');
            })
        $('.navBeforLogin a[href^="#writetous"]').addClass('select');
       }
    })

	$('.planNewWrap').find('.yearly').css("display","none")
    $('.planNewWrap').find('.monthly').css("display","block")
	
    var ckbox = $('.planToggle input');

    $(ckbox).on('click', function () {
        if (ckbox.is(':checked')) {
           $('.planToggle').find('h4').addClass('opc1')
           $('.planToggle').find('h5').removeClass('opc1')
           // .price.showF{display: flex;}
// .planWrap.planNewWrap .plns .price.showS{display: none;}
            $('.planNewWrap').find('.monthly').css("display","none")
            $('.planNewWrap').find('.yearly').css("display","block")


        } else {
           $('.planToggle h4').removeClass('opc1')
           $('.planToggle h5').addClass('opc1')
           $('.planNewWrap').find('.yearly').css("display","none")
           $('.planNewWrap').find('.monthly').css("display","block")

        }
    });

});


function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('.navBeforLogin a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('.navBeforLogin a').removeClass("select");
            currLink.addClass("select");
        }
        else{
            currLink.removeClass("select");
        }
    });
}