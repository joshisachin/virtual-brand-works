﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using System.Linq;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.UIControllers
{
    [Authorize]
    [AuthorizeUser(Role = "SuperAdmin")]
    [SessionExpire]
    public class QuizController : Controller
    {
        private readonly APIClient apiClient = new APIClient();
        /// <summary>
        /// Contents for creating/editing/viewing the Quiz
        /// Author: Viresh
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                QuizViewModel quizViewModel = new QuizViewModel();
                Session["Categories"] = GetCategories();
                Session["Folders"] = new List<MasterFolderViewModel>();
                quizViewModel.Contents = new List<ContentViewModel>();
                return View(quizViewModel);
            }
            catch (Exception ex)
            {
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            return View();
        }

        /// <summary>
        /// Get categories
        /// Author: Viresh
        /// </summary>
        /// <returns></returns>
        public List<CategoryViewModel> GetCategories()
        {
            Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CATEGORIES_API);
            Response response = apiClient.Get<Response>(uri,
                  new Dictionary<string, string>()
                     {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                     }
                 );
            return JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString());
        }

        /// <summary>
        /// Get Folders
        /// Author: Viresh
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetFoldersByCategoyId(long categoryId)
        {
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_FOLDERS_BY_CATEGORY_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(new MasterFolderViewModel { CategoryId = categoryId })
                    );

                Session["Folders"] = JsonConvert.DeserializeObject<List<MasterFolderViewModel>>(response.Data.ToString());
                return Json(JsonConvert.DeserializeObject<List<MasterFolderViewModel>>(response.Data.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            return Json(new List<MasterFolderViewModel>(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get contents based on category and folder
        /// Author : Viresh Bhatt
        /// </summary>
        /// <returns></returns>
        [ActionName("GetContents")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetContentsByFolderId(QuizViewModel quizViewModel)
        {
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_CONTENTS_BY_FOLDER_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(new MasterFolderViewModel { MasterFolderId = quizViewModel.FolderId })
                    );
                quizViewModel.Contents = response.Data == null ? new List<ContentViewModel>() : JsonConvert.DeserializeObject<List<ContentViewModel>>(response.Data.ToString());
            }
            catch (Exception ex)
            {
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            return View("Index", quizViewModel);
        }

        /// <summary>
        /// Get add question view
        /// </summary>
        /// <param name="quizViewModel"></param>
        /// <returns></returns>
        public ActionResult GetAddQuestionView(QuizViewModel quizViewModel)
        {
            quizViewModel.QuestionAnswer = new QuestionAnswerViewModel();
            quizViewModel.QuestionAnswer.Answers = new List<AnswerViewModel>();
            quizViewModel.QuestionAnswer.QuizId = quizViewModel.QuizId;
            ///Adding default question and answer
            quizViewModel.QuestionAnswer.Answers.AddRange(new List<AnswerViewModel> { new AnswerViewModel() { AnswerId = 1 }, new AnswerViewModel() { AnswerId = 2 }, new AnswerViewModel() { AnswerId = 3 }, new AnswerViewModel() { AnswerId = 4 } });

            return PartialView("AddQuestion", quizViewModel.QuestionAnswer);
        }

        /// <summary>
        /// Get edit question view
        /// </summary>
        /// <param name="quizViewModel"></param>
        /// <returns></returns>
        public ActionResult GetEditQuestionView(QuizViewModel quiz)
        {
            try
            {
                QuestionViewModel questionViewModel = new QuestionViewModel() { QuestionId = quiz.QuestionId };
                Uri uri = new Uri(Constants.BASE_URI, Constants.Get_QUESTION_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(questionViewModel)
                    );
                if (response.Status)
                {
                    quiz.QuestionAnswer = JsonConvert.DeserializeObject<QuestionAnswerViewModel>(response.Data.ToString());
                    quiz.QuestionAnswer.RightAnsId = quiz.QuestionAnswer.Answers.Where(obj => obj.IsRightAnswer).Select(ans => ans.AnswerId).FirstOrDefault();
                }
                else ViewBag.Result = response.Message;
            }
            catch (Exception)
            {
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            return PartialView("EditQuestion", quiz);
        }

        /// <summary>
        /// Add question
        /// Author: Viresh Bhatt
        /// </summary>
        /// <param name="QuestionAnswerViewModel"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddQuestion(QuestionAnswerViewModel questionAnswerViewModel)
        {
            Response response = null;
            try
            {
                questionAnswerViewModel.Answers.Where(obj => obj.AnswerId == questionAnswerViewModel.RightAnsId).ToList().ForEach(x => { x.IsRightAnswer = true; });

                Uri uri = new Uri(Constants.BASE_URI, Constants.ADD_QUESTION_API);
                response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(questionAnswerViewModel)
                    );
            }
            catch (Exception ex)
            {
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
                ///log exception here
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update question
        /// Author: Viresh Bhatt
        /// </summary>
        /// <param name="QuestionAnswerViewModel"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateQuestion(QuizViewModel quizViewModel)
        {
            Response response = null;
            try
            {
                quizViewModel.QuestionAnswer.Answers.Where(obj => obj.AnswerId == quizViewModel.QuestionAnswer.RightAnsId).ToList().ForEach(x => { x.IsRightAnswer = true; });

                Uri uri = new Uri(Constants.BASE_URI, Constants.Update_QUESTION_API);
                response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(quizViewModel.QuestionAnswer)
                    );
            }
            catch (Exception ex)
            {
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
                ///log exception here
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Quiz
        /// Author: Viresh Bhatt
        /// </summary>
        /// <param name="quizViewModel"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SaveQuiz(QuizViewModel quizViewModel)
        {
            Response response = null;
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.SAVE_QUIZ);
                response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(quizViewModel)
                  );
            }
            catch (Exception ex)
            {
                response = new Response() { Message = Constants.INTERNAL_SERVER_ERROR, Status = false };
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get view quiz
        /// </summary>
        /// <param name="quizViewModel"></param>
        /// <returns></returns>
        public ActionResult GetViewQuiz(QuizViewModel quizViewModel)
        {
            try
            {
                bool isEditRequest = quizViewModel.IsEditRequest;

                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_QUIZ_DETAILS);
                Response response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(new QuizViewModel { QuizId = quizViewModel.QuizId })
                  );
                quizViewModel = response.Data == null ? new QuizViewModel() : JsonConvert.DeserializeObject<QuizViewModel>(response.Data.ToString());
                quizViewModel.IsEditRequest = isEditRequest;
            }
            catch (Exception ex)
            {
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            if (!quizViewModel.IsEditRequest)
                return PartialView("ViewQuiz", quizViewModel);
            else return PartialView("editQuestions", quizViewModel);
        }

        public JsonResult DeleteQuiz(QuizViewModel quizViewModel)
        {
            Response response = null;
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.DELET_QUIZ);
                response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(quizViewModel)
                  );
            }
            catch (Exception ex)
            {
                response = new Response() { Message = Constants.INTERNAL_SERVER_ERROR, Status = false };
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQuestion(QuestionViewModel questionViewModel)
        {
            Response response = null;
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.DELETE_QUESTION);
                response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(questionViewModel)
                  );
            }
            catch (Exception ex)
            {
                response = new Response() { Message = Constants.INTERNAL_SERVER_ERROR, Status = false };
                ViewBag.Result = Constants.INTERNAL_SERVER_ERROR;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
