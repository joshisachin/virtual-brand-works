﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using Newtonsoft.Json;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Entity;
using System;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.UIControllers
{
    [Authorize]
    [AuthorizeUser(Role = "SuperAdmin")]
    [SessionExpire]
    public class MasterFolderController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MasterFolderController));
        private readonly APIClient apiClient = new APIClient();

        /// <summary>
        /// show list of all folder
        /// </summary>
        /// <returns></returns>
        // GET: MasterFolder
        public ActionResult Index()
        {
            logger.Info("Index is called");
            try
            {
                //throw new Exception();
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_FOLDER_API);
                Response response = apiClient.Get<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }
                   );
                List<CategoryViewModel> categoryList = GetCategoriesForFolderContent();
                logger.Info("Index is completed");
                return View(JsonConvert.DeserializeObject<List<MasterFolderViewModel>>(response.Data.ToString()));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }
        /// <summary>
        /// get all categories and bind to dropdown while creating :ModifiedDetails :shubhangi derkar : 27/04/2020 
        /// </summary>
        /// <returns></returns>
        // GET: MasterFolder/Create
        public ActionResult Create()
        {
            MasterFolderViewModel folderViewModel = new MasterFolderViewModel();
            List<CategoryViewModel> categories = GetCategoriesForFolderContent();
            folderViewModel.Categories = CategoryToSelectList(categories);
            return View(folderViewModel);
        }

        /// <summary>
        /// returns selectlist for categories
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        private List<SelectListItem> CategoryToSelectList(List<CategoryViewModel> categories, long? categoryId = 0)
        {
            List<SelectListItem> categoryList = new List<SelectListItem>();

            foreach (CategoryViewModel category in categories)
            {
                if (categoryId != null && category.CategoryId == categoryId)
                {
                    categoryList.Add(new SelectListItem()
                    {
                        Text = category.CategoryName,
                        Value = Convert.ToString(category.CategoryId),
                        Selected = true
                    });
                }
                else
                {
                    categoryList.Add(new SelectListItem()
                    {
                        Text = category.CategoryName,
                        Value = Convert.ToString(category.CategoryId)
                    });
                }
            }
            categoryList.Add(new SelectListItem()
            {
                Text = "Other",
                Value = "999"
            });
            return categoryList;
        }
        /// <summary>
        /// post call for create folder  
        /// </summary>
        /// <param name="masterFolder"></param>
        /// <returns></returns>
        // POST: MasterFolder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckModelForNull]
        public ActionResult Create([Bind(Include = "MasterFolderId,Name,ShortName,CategoryId,SelectedCategoryIds")] MasterFolderViewModel masterFolder)
        {
            logger.Info("Create post is called");

            if (!ModelState.IsValid)
                return View(masterFolder);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.FOLDER_CREATE_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(masterFolder)
                    );

                if (response.Status)
                    TempData["Success"] = "Folder created successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while creating folder";
                }
                logger.Info("Create post is completed");
                return Json(new { status = "Success", message = "Success" });
                // return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(masterFolder, Formatting.Indented)}", ex);
                return View("Error");
            }
        }
        /// <summary>
        /// edit folder get call :ModifiedDetails :shubhangi derkar : 27/04/2020 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: MasterFolder/Edit/5
        public ActionResult Edit(long id)
        {
            logger.Info("Edit get is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                //get folder details
                Uri uri = new Uri(Constants.BASE_URI, Constants.FOLDER_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(new MasterFolderViewModel { MasterFolderId = id })
                    );
                MasterFolderViewModel masterFolder = JsonConvert.DeserializeObject<MasterFolderViewModel>(response.Data.ToString());
                List<CategoryViewModel> categories = GetCategoriesForFolderContent();
                masterFolder.Categories = CategoryToSelectList(categories);

                logger.Info("Edit get is completed");
                return View(masterFolder);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}", ex);
                return View("Error");
            }
        }
        /// <summary>
        /// edit folder post call :ModifiedDetails :shubhangi derkar : 27/04/2020 
        /// </summary>
        /// <param name="masterFolder"></param>
        /// <returns></returns>
        // POST: MasterFolder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MasterFolderId,Name,ShortName,CategoryId,SelectedCategoryIds")] MasterFolderViewModel masterFolder)
        {
            logger.Info("Edit post is called");

            if (!ModelState.IsValid)
                return View(masterFolder);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.FOLDER_UPDATE_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(masterFolder)
                   );

                if (response.Status)
                    TempData["Success"] = "Folder is updated successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while updating folder";
                }
                logger.Info("Edit post is completed");
                return Json(new { status = "Success", message = "Success" });
                //  return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(masterFolder, Formatting.Indented)}", ex);
                return View("Error");
            }
        }

        /// <summary>
        /// delete folder post calls :ModifiedDetails :shubhangi derkar : 27/04/2020 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // POST: MasterFolder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            logger.Info("Delete post is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.FOLDER_DELETE_API);
                Response response = apiClient.Delete<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(new MasterFolderViewModel { MasterFolderId = id })
                   );

                if (response.Status)
                    TempData["Success"] = "Folder deleted successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while deleting folder";
                }
                logger.Info("Delete post is completed");
                return Json(new { status = "Success", message = "Success" });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}", ex);
                return View("Error");
            }
        }
        public List<CategoryViewModel> GetCategoriesForFolderContent()
        {
            logger.Info("GetCategories get is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CATEGORIES_API);
                Response response = apiClient.Get<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }
                  );
                logger.Info("GetCategories get is completed");
                List<CategoryViewModel> categoryList = JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString());
                return categoryList;
                //JsonConvert.DeserializeObject<List<CategoryModel>>(response.Data.ToString());
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return null;
            }
        }

        public JsonResult ValidateFolderName(MasterFolderViewModel masterFolder)
        {
            logger.Info("ValidateFolderName is called");

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.VALIDATE_FOLDER_NAME_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(masterFolder)
                    );

                logger.Info("ValidateFolderName is completed");
                return Json(response, JsonRequestBehavior.AllowGet);
                // return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(masterFolder, Formatting.Indented)}", ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
