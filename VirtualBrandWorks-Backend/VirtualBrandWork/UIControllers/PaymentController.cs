﻿using Newtonsoft.Json;
using Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Models;
using static VirtualBrandWork.Entity.Constants;

namespace VirtualBrandWork.UIControllers
{
    public class PaymentController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountController));
        private readonly APIClient apiClient = new APIClient();
        private readonly StripePaymentUtility _spu = new StripePaymentUtility();
        private readonly string _stripeSuccessUrl = ConfigurationManager.AppSettings["StripeSuccessUrl"].ToString();
        private readonly string _stripeCancelUrl = ConfigurationManager.AppSettings["StripeCancelUrl"].ToString();
        private readonly string _taxType = ConfigurationManager.AppSettings["TaxType"].ToString();
        private readonly double _taxValue = Convert.ToDouble(ConfigurationManager.AppSettings["TaxValue"]);
        private readonly string _taxId = ConfigurationManager.AppSettings["TaxId"].ToString();
        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PaymentConfirmation(SignUpViewModel signUpViewModel)
        {
            logger.Info("PaymentConfirmation is called");
            try
            {
                Uri uri = new Uri(BASE_URI, SUBSCRIPTION_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                       }, JsonConvert.SerializeObject(new SubscriptionViewModel { SubscriptionId = signUpViewModel.SelectedPlan, PlanId = signUpViewModel.SelectedPlanPrice })
                   );
                signUpViewModel.SelectedSubscription = JsonConvert.DeserializeObject<SubscriptionViewModel>(response.Data.ToString());

                // signUpViewModel.Tax = _taxValue;
                // signUpViewModel.TaxType = _taxType;
                signUpViewModel.FinalPayableAmount = signUpViewModel.SelectedSubscription.Price;// + (signUpViewModel.SelectedSubscription.Price * signUpViewModel.Tax / 100);

                logger.Info("PaymentConfirmation is complete");

                ///Save details with IsPaymentCompleted = false, and make active after successfull payment. For cancel delete from DB
                signUpViewModel.IsPaymentCompleted = false;
                Response resp = SaveUserDetails(signUpViewModel);
                if (resp.Status)
                    return View(signUpViewModel);
                else
                {
                    ViewBag.Error = Constants.PAYMENT_CANNOT_PROCESSED;
                    return View("Index");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        private Response SaveUserDetails(SignUpViewModel signUpViewModel)
        {
            logger.Info("SaveUserDetails is called");
            try
            {
                Uri uri = new Uri(BASE_URI, SAVE_ADMIN_DETAILS);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                       }, JsonConvert.SerializeObject(signUpViewModel)
                   );
                logger.Info("SaveUserDetails is complete");
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return null;
        }

        public JsonResult setup(string planId)
        {
            try
            {
                StripeSetupModel setupModel = _spu.Setup(planId);
                return Json(setupModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {


            }
            return Json(new StripeSetupModel(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateCheckoutSession(string planId, string email, string subscription, string amount, string billingCycle, string firstName, string lastName)
        {
            StripeCheckoutSessionModel sm = null;
            try
            {
                sm = _spu.GetCreateCheckoutSession(planId, email, subscription, amount, billingCycle, firstName, lastName, _stripeSuccessUrl, _stripeCancelUrl);//, _taxId);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return Json(sm, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PaymentCancel(string sid)
        {
            try
            {
                if (string.IsNullOrEmpty(sid))
                    return RedirectToAction("Index", "Home");

                Stripe.Checkout.Session session = _spu.GetCheckoutSession(sid);
                StripeSessionResponse stripeResponse = JsonConvert.DeserializeObject<StripeSessionResponse>(session.RawJObject.ToString());

                EmailViewModel email = new EmailViewModel();
                if (session.Metadata != null)
                {
                    foreach (KeyValuePair<string, string> metadata in session.Metadata)
                    {
                        if (metadata.Key.Equals("FirstName", StringComparison.OrdinalIgnoreCase))
                        {
                            email.FirstName = metadata.Value.ToString();
                        }
                        else if (metadata.Key.Equals("LastName", StringComparison.OrdinalIgnoreCase))
                        {
                            email.LastName = metadata.Value.ToString();
                        }
                    }
                }

                logger.Info("PaymentCancel is called");
                Uri uri = new Uri(BASE_URI, DELETE_SIGNUP_DETAILS);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }, JsonConvert.SerializeObject(new SignUpViewModel() { Email = stripeResponse.CustomerEmail.ToString() })
                   );

                email.Email = stripeResponse.CustomerEmail.ToString();
                email.Message = Constants.PAYMENT_CANCELLED_MESSAGE;
                email.Subject = Constants.PAYMENT_CANCELLED_SUBJECT;
                ///Send cancel email
                uri = new Uri(BASE_URI, SEND_PAYMENT_EMAIL);
                response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                      }, JsonConvert.SerializeObject(email)
                   );

                logger.Info("PaymentCancel is completed");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                ViewBag.Error = Constants.SOME_ERROR_OCCUREED;
            }
            return View();
        }

        public ActionResult PaymentSuccess(string sid)
        {
            try
            {
                if (string.IsNullOrEmpty(sid))
                    return RedirectToAction("Index", "Home");


                Stripe.Checkout.Session session = _spu.GetCheckoutSession(sid);
                StripeSessionResponse stripeResponse = JsonConvert.DeserializeObject<StripeSessionResponse>(session.RawJObject.ToString());
                EmailViewModel email = new EmailViewModel();
                logger.Info("PaymentSuccess is called");
                if (session.Metadata != null)
                {
                    foreach (KeyValuePair<string, string> metadata in session.Metadata)
                    {
                        if (metadata.Key.Equals("subscription", StringComparison.OrdinalIgnoreCase))
                        {
                            email.SubscriptionPlan = metadata.Value.ToString();
                        }
                        else if (metadata.Key.Equals("amount", StringComparison.OrdinalIgnoreCase))
                        {
                            email.Amount = Convert.ToInt64(metadata.Value.ToString());
                        }
                        else if (metadata.Key.Equals("billingCycle", StringComparison.OrdinalIgnoreCase))
                        {
                            email.BillingCycle = Convert.ToInt32(metadata.Value.ToString());
                        }
                        else if (metadata.Key.Equals("FirstName", StringComparison.OrdinalIgnoreCase))
                        {
                            email.FirstName = metadata.Value.ToString();
                        }
                        else if (metadata.Key.Equals("LastName", StringComparison.OrdinalIgnoreCase))
                        {
                            email.LastName = metadata.Value.ToString();
                        }
                    }
                }
                Uri uri = new Uri(BASE_URI, UPDATE_SIGNUP_DETAILS_PAYMENT_SUCCESS);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                       }, JsonConvert.SerializeObject(new SignUpViewModel() { Email = stripeResponse.CustomerEmail.ToString() })
                   );


                email.Email = stripeResponse.CustomerEmail.ToString();
                email.Message = Constants.PAYMENT_SUCCESSFULL;
                email.Subject = Constants.PAYMENT_SUCCESSFULL_SUBJECT;
                email.flag = true;
                //////Send success email
                uri = new Uri(BASE_URI, SEND_PAYMENT_EMAIL);
                response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                         }, JsonConvert.SerializeObject(email)
                   );
                logger.Info("PaymentSuccess is completed");

                TempData["Success"] = Constants.PAYMENT_SUCCESSFULL;
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                ViewBag.Error = Constants.SOME_ERROR_OCCUREED;
            }
            return View();
        }

        public JsonResult Save(string email, string firstName, string lastName)
        {
            try
            {
                logger.Info("PaymentSuccess is called");
                Uri uri = new Uri(BASE_URI, UPDATE_SIGNUP_DETAILS_PAYMENT_SUCCESS);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                       }, JsonConvert.SerializeObject(new SignUpViewModel() { Email = email })
                   );

                EmailViewModel emailInfo = new EmailViewModel();
                emailInfo.SubscriptionPlan = "Trial";
                emailInfo.Amount = 0;
                emailInfo.BillingCycle = 0;
                emailInfo.Email = email;
                emailInfo.Message = Constants.PAYMENT_SUCCESSFULL;
                emailInfo.Subject = Constants.SUBSCRIPTION_COMPLETED_SUCCESSFULLY;
                emailInfo.FirstName = firstName;
                emailInfo.LastName = lastName;
                emailInfo.flag = true;
                //////Send success email
                uri = new Uri(BASE_URI, SEND_PAYMENT_EMAIL);
                response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                         }, JsonConvert.SerializeObject(emailInfo)
                   );

                TempData["Success"] = Constants.PAYMENT_SUCCESSFULL;
                logger.Info("PaymentSuccess is completed");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                ViewBag.Error = Constants.SOME_ERROR_OCCUREED;
                return Json(new Response() { Status = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}