﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using Newtonsoft.Json;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Filters;
using System.Linq;

namespace VirtualBrandWork.UIControllers
{
    [Authorize]
    [AuthorizeUser(Role = "SuperAdmin")]
    [SessionExpire]
    public class MasterContentController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MasterContentController));
        private readonly APIClient apiClient = new APIClient();

        /// <summary>
        /// Get master content list
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            logger.Info("Index is called");
            try
            {
                Response response = new Response();
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CONTENT_API);
                response = apiClient.Get<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }
                   );
                logger.Info("Index is completed");
                return View(JsonConvert.DeserializeObject<List<MasterContentViewModel>>(response.Data.ToString()));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        /// <summary>
        /// // GET: MasterContent/Create
        /// </summary>
        /// <returns></returns>

        public ActionResult Create()
        {
            logger.Info("Create get is called");
            try
            {
                MasterContentViewModel contentViewModel = new MasterContentViewModel();
                List<CategoryViewModel> categories = GetCategories();
                contentViewModel.Categories = CategoryToSelectList(categories);
                List<TagViewModel> tags = GetTags();
                contentViewModel.Tags = TagToSelectList(tags);
                contentViewModel.Folders = new List<SelectListItem>();
                logger.Info("Create get is completed");

                /****Get Upload details******/
                Response response = new Response();
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_AWSS3_POLICY + "?fileId=" + string.Empty);
                response = apiClient.Get<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }
                   );

                FileUploadViewModel fileUploadViewModel = JsonConvert.DeserializeObject<FileUploadViewModel>(response.Data.ToString());
                contentViewModel.FileUploadViewModel = fileUploadViewModel;
                return View(contentViewModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        // POST: MasterContent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CheckModelForNull]
        public ActionResult Create(MasterContentViewModel masterContent)
        {
            logger.Info("Create post is called");

            if (!ModelState.IsValid)
            {
                List<CategoryViewModel> categories = GetCategories();
                masterContent.Categories = CategoryToSelectList(categories);
                List<FolderModel> folders = GetFolders();
                masterContent.Folders = FolderToSelectList(folders, masterContent.MasterFolderId);
                List<TagViewModel> tags = GetTags();
                masterContent.Tags = TagToSelectList(tags);
                return View(masterContent);
            }

            try
            {
                int index = masterContent.ContentUrl.LastIndexOf('.');
                var key = index == -1 ? masterContent.ContentUrl : masterContent.ContentUrl.Substring(0, index);
                masterContent.ContentUrl = key;
                Uri uri = new Uri(Constants.BASE_URI, Constants.MASTER_CONTENT_CREATE_API);
                Response response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(masterContent)
                  );

                if (response.Status)
                {
                    TempData["Success"] = "Content created successfully.";
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while creating content";
                }
                logger.Info("Create post is completed");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(masterContent, Formatting.Indented)}", ex);
                return View("Error");
            }
        }

        /// <summary>
        /// // GET:MasterContent/Edit/5
        /// </summary>
        /// <returns></returns>
        // GET: MasterContent/Edit/5
        public ActionResult Edit(long id)
        {
            logger.Info("Edit get is called");

            if (id == 0)
                return View();

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.MASTER_CONTENT_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(new MasterContentViewModel { MasterContentId = id })
                  );
                logger.Info("Edit get is completed");
                MasterContentViewModel masterContent = JsonConvert.DeserializeObject<MasterContentViewModel>(response.Data.ToString());

                List<CategoryViewModel> categories = GetCategories();
                masterContent.Categories = CategoryToSelectList(categories, masterContent.CategoryId);

                List<TagViewModel> tags = GetTags();
                masterContent.Tags = TagToSelectList(tags, masterContent.TagIds);

                List<FolderModel> folders = GetFolders(Convert.ToInt64(masterContent.CategoryId));
                masterContent.Folders = FolderToSelectList(folders, masterContent.MasterFolderId);

                logger.Info("Edit get completed");
                // for bindind values
                response = new Response();
                uri = new Uri(Constants.BASE_URI, Constants.GET_AWSS3_POLICY + "?fileId=" + string.Empty);
                response = apiClient.Get<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }
                   );

                FileUploadViewModel fileUploadViewModel = JsonConvert.DeserializeObject<FileUploadViewModel>(response.Data.ToString());
                masterContent.FileUploadViewModel = fileUploadViewModel;

                ViewBag.FileName = masterContent.ContentUrl;
                return View(masterContent);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}", ex);
                return View("Error");
            }
        }

        // POST: MasterContent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterContentViewModel masterContent)
        {
            logger.Info("Edit post is called");

            if (!ModelState.IsValid)
            {
                List<CategoryViewModel> categories = GetCategories();
                masterContent.Categories = CategoryToSelectList(categories);
                List<TagViewModel> tags = GetTags();
                masterContent.Tags = TagToSelectList(tags);
                masterContent.Folders = new List<SelectListItem>();
                return View(masterContent);
            }

            try
            {
                int index = masterContent.ContentUrl.LastIndexOf('.');
                var key = index == -1 ? masterContent.ContentUrl : masterContent.ContentUrl.Substring(0, index);
                masterContent.ContentUrl = index == 40 ? key : masterContent.ContentUrl;

                Uri uri = new Uri(Constants.BASE_URI, Constants.MASTER_CONTENT_UPDATE_API);
                Response response = apiClient.Post<Response>(uri,
                new Dictionary<string, string>()
                   {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                   }, JsonConvert.SerializeObject(masterContent)
               );

                if (response.Status)
                {
                    TempData["Success"] = "Content updated successfully.";
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while updating content";
                }
                logger.Info("Edit post is completed");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(masterContent, Formatting.Indented)}", ex);
                return View("Error");
            }
        }


        // POST: MasterContent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {

            logger.Info("Delete post is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.MASTER_CONTENT_DELETE_API);
                Response response = apiClient.Delete<Response>(uri,
                   new Dictionary<string, string>()
                      {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(new MasterContentViewModel { MasterContentId = id })
                  );

                if (response.Status)
                {
                    TempData["Success"] = "Content deleted successfully";
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while deleting content";
                }
                logger.Info("Delete post is completed");
                return Json(new { status = "Success", message = "Success" });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}", ex);
                return View("Error");
            }
        }

        /// <summary>
        /// // GET: Folders by catergory Id
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetFoldersByCategoyId(long categoryId)
        {
            logger.Info("GetFoldersByCategoryId is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_FOLDERS_BY_CATEGORY_API);
                Response response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(new CategoryViewModel { CategoryId = categoryId })
                  );
                Session["Folders"] = JsonConvert.DeserializeObject<List<MasterFolderViewModel>>(response.Data.ToString());
                logger.Info("GetFoldersByCategoryId is complete");
                return Json(JsonConvert.DeserializeObject<List<MasterFolderViewModel>>(response.Data.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return Json(new List<MasterFolderViewModel>(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// used to publish a content
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // [HttpPost]
        // [ValidateAntiForgeryToken]
        public JsonResult PublishUnpublishContent(long id, bool flag)
        {
            logger.Info("PublishUnpublishContent is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI + "api/mastercontent/publish");
                Response response = apiClient.Post<Response>(uri,
                  new Dictionary<string, string>(){
                      { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(new MasterContentViewModel { MasterContentId = id, flag = flag })
                 );
                if (!response.Status)
                {
                    TempData["Error"] = Constants.FILE_CORRUPTED;
                    logger.Info("PublishUnpublishContent is complete");
                    return Json(new { failed = "failed" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["Success"] = response.Message;
                    logger.Info("PublishUnpublishContent is complete");
                    return Json(new { success = "success" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}", ex);
                return Json(new { failed = "failed" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Create folder for other in dropdown
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateFolder(MasterFolderViewModel folder)
        {
            logger.Info("CreateFolder post called");
            try
            {
                if (!ModelState.IsValidField("CategoryId"))
                    return Json(new { ErrorMessge = "The folder must be associated to a category" }, JsonRequestBehavior.AllowGet);
                if (!ModelState.IsValidField("Name"))
                    return Json(new { ErrorMessge = "Folder Name cannot be blank" }, JsonRequestBehavior.AllowGet);
                if (folder.MasterFolderId == 999)
                    return Json(new { ErrorMessge = "Select valid folder" }, JsonRequestBehavior.AllowGet);
                if (!ModelState.IsValidField("ShortName"))
                    return Json(new { ErrorMessge = "This is not a valid short form. Please input only alphabets" }, JsonRequestBehavior.AllowGet);


                Uri uri = new Uri(Constants.BASE_URI, Constants.FOLDER_CREATE_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(folder)
                   );

                if (response.Status == true)
                {
                    var folders = GetFolders(Convert.ToInt64(folder.CategoryId));
                    folders.Add(new FolderModel()
                    {
                        Name = "Other",
                        MasterFolderId = 999
                    });
                    response.Data = folders;
                    response.Message = "Folder created successfully";
                    logger.Info("CreateFolder post is completed");
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        response.Message = response.Message;
                    else
                        response.Message = "An error occured while creating folder";
                }
               

                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(string.Concat(ex.Message, JsonConvert.SerializeObject(folder, Formatting.Indented, new JsonSerializerSettings { })), ex);
                return View("Error");
            }
        }

        private List<CategoryViewModel> GetCategories()
        {
            Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CATEGORIES_API);
            Response response = apiClient.Get<Response>(uri,
                 new Dictionary<string, string>()
                    {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                    }
                );
            return JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString());
        }

        /// <summary>
        /// get the list of tags
        /// </summary>
        /// <returns></returns>
        private List<TagViewModel> GetTags()
        {
            Uri uri = new Uri(Constants.BASE_URI, Constants.GET_TAGS_LIST);
            Response response = apiClient.Get<Response>(uri,
                new Dictionary<string, string>()
                   {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                    }
               );
            return JsonConvert.DeserializeObject<List<TagViewModel>>(response.Data.ToString());
        }

        /// <summary>
        /// get tags in select list item
        /// </summary>
        /// <returns></returns>
        private List<TagModel> GetTagModelList()
        {
            Uri uri = new Uri(Constants.BASE_URI, Constants.GET_TAGS_LIST);
            Response response = apiClient.Get<Response>(uri,
                new Dictionary<string, string>()
                   {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                   }
               );
            return JsonConvert.DeserializeObject<List<TagModel>>(response.Data.ToString());
        }

        /// <summary>
        /// get list of folders
        /// </summary>
        /// <returns></returns>
        private List<FolderModel> GetFolders(long categoryId = 0)
        {
            Response response = new Common.Response();
            if (categoryId == 0)
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_FOLDER_API);
                response = apiClient.Get<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }
                  );

            }
            else
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_FOLDERS_BY_CATEGORY_API);
                response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }, JsonConvert.SerializeObject(new CategoryViewModel { CategoryId = categoryId })
                  );
                //Session["Folders"] = JsonConvert.DeserializeObject<List<MasterFolderViewModel>>(response.Data.ToString());
                logger.Info("GetFoldersByCategoryId is complete");
            }
            var folderList = JsonConvert.DeserializeObject<List<FolderModel>>(response.Data.ToString());
            return folderList;
        }

        /// <summary>
        /// returns selectlist for categories
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        private List<SelectListItem> CategoryToSelectList(List<CategoryViewModel> categories, long? categoryId = 0)
        {
            List<SelectListItem> categoryList = new List<SelectListItem>();

            foreach (CategoryViewModel category in categories)
            {
                if (categoryId != null && category.CategoryId == categoryId)
                {
                    categoryList.Add(new SelectListItem()
                    {
                        Text = category.CategoryName,
                        Value = Convert.ToString(category.CategoryId),
                        Selected = true
                    });
                }
                else
                {
                    categoryList.Add(new SelectListItem()
                    {
                        Text = category.CategoryName,
                        Value = Convert.ToString(category.CategoryId)
                    });
                }
            }
            categoryList.Add(new SelectListItem()
            {
                Text = "Other",
                Value = "999"
            });
            return categoryList;
        }

        /// <summary>
        /// returns selectlist for folders
        /// </summary>
        /// <param name="folders"></param>
        /// <param name="folderId"></param>
        /// <returns></returns>
        private List<SelectListItem> FolderToSelectList(List<FolderModel> folders, long? folderId = 0)
        {
            List<SelectListItem> folderList = new List<SelectListItem>();

            foreach (FolderModel folder in folders)
            {
                if (folderId != null && folder.MasterFolderId == folderId)
                {
                    folderList.Add(new SelectListItem()
                    {
                        Text = folder.Name,
                        Value = Convert.ToString(folder.MasterFolderId),
                        Selected = true
                    });
                }
                else
                {
                    folderList.Add(new SelectListItem()
                    {
                        Text = folder.Name,
                        Value = Convert.ToString(folder.MasterFolderId)
                    });
                }
            }
            folderList.Add(new SelectListItem()
            {
                Text = "Other",
                Value = "999"
            });
            return folderList;
        }

        /// <summary>
        /// returns selectlist for tags
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        private List<SelectListItem> TagToSelectList(List<TagViewModel> tags, int[] tagIds = null)
        {
            List<SelectListItem> tagList = new List<SelectListItem>();
            List<int> tagIdsList = new List<int>();
            if (tagIds != null)
            {
                foreach (int id in tagIds)
                {
                    tagIdsList.Add(id);
                }
            }
            //filter the element with id from the list 
            //remove the element with id from the tag
            // append the element to the list again at the top and return
            IEnumerable<TagViewModel> tagListFinal = tags;
            if (tagIds != null)
            {
                var selectedTagList = tags.Where(t => tagIds.Any(tid => tid == t.TagId)).ToList();
                var otherThanSelectedTagList = tags.Where(t => !tagIds.Any(tid => tid == t.TagId)).ToList();
                tagListFinal = selectedTagList.Concat(otherThanSelectedTagList);
            }
            foreach (TagViewModel tag in tagListFinal)
            {
                if (tagIdsList.Contains(Convert.ToInt32(tag.TagId)))
                {
                    tagList.Add(new SelectListItem()
                    {
                        Text = tag.Name,
                        Value = tag.Name,
                        Selected = true
                    });
                }
                else
                {
                    tagList.Add(new SelectListItem()
                    {
                        Text = tag.Name,
                        Value = tag.Name
                    });
                }
            }
            return tagList;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateCategory(CategoryViewModel category)
        {

            if (string.IsNullOrEmpty(category.CategoryName))
                return Json(new { ErrorMessge = "Category name cannot be blank" }, JsonRequestBehavior.AllowGet);
            if (category.CategoryId == 999)
                return Json(new { ErrorMessge = "Select valid category" }, JsonRequestBehavior.AllowGet);

            logger.Info("CreateCategory post is called");

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.CATEGORIES_CREATE_FROM_FOLDER_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(category)
                   );

                List<CategoryModel> categoryList = GetCategoriesForContent();
                if (response.Status == false)
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        response.Message = response.Message;
                    else
                        response.Message = "An error occured while creating category.";
                }
                else
                {
                    response.Message = "Category created successfully";
                    logger.Info("CreateCategory post is completed");
                }
                response.Data = categoryList;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}", ex);
                return View("Error");
            }
        }
        public List<CategoryModel> GetCategoriesForContent()
        {
            logger.Info("GetCategories get is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CATEGORIES_API);
                Response response = apiClient.Get<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                      }
                  );
                logger.Info("GetCategories get is completed");
                List<CategoryModel> categoryList = JsonConvert.DeserializeObject<List<CategoryModel>>(response.Data.ToString());
                categoryList.Add(new CategoryModel { CategoryId = 999, CategoryName = "Other" });
                return categoryList;
                //JsonConvert.DeserializeObject<List<CategoryModel>>(response.Data.ToString());
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return null;
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult getBase64policy(string fileId)
        {
            logger.Info("getBase64policy is called");
            try
            {
                Response response = new Response();
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_AWSS3_POLICY + "?fileId=" + fileId);
                response = apiClient.Get<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }
                   );
                FileUploadViewModel fileUploadViewModel = JsonConvert.DeserializeObject<FileUploadViewModel>(response.Data.ToString());

                logger.Info("getBase64policy is complete");
                return Json(new { policy = fileUploadViewModel.Base64EncodedPolicy, signature = fileUploadViewModel.Signature, fileId = fileUploadViewModel.FileId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return Json(new List<MasterFolderViewModel>(), JsonRequestBehavior.AllowGet);
        }
    }
}
