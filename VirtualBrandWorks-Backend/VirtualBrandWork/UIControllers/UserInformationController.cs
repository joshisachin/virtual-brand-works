﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.UIControllers
{
    [Authorize]
    [AuthorizeUser(Role = "SuperAdmin")]
    [SessionExpire]
    public class UserInformationController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UserInformationController));
        private readonly APIClient apiClient = new APIClient();
        // GET: UserInformation
        public ActionResult Index()
        {
            try
            {
                ManageUserViewModel manageUser = new ManageUserViewModel();
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_All_COMPANY);
                Response response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }
                    );
                manageUser.Companies = JsonConvert.DeserializeObject<List<CompanyViewModel>>(response.Data.ToString());
                return View(manageUser);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        /// <summary>
        /// use to get the company details and users list of a specific company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public PartialViewResult GetUsersByCompanyId(long companyId)
        {
            ManageUserViewModel manageUser = new ManageUserViewModel();
            manageUser.CompanyId = companyId;
            Uri uri = new Uri(Constants.BASE_URI, Constants.GET_USER_INFORMATION);
            Response response = apiClient.Post<Response>(uri,
               new Dictionary<string, string>()
                  {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                  }, JsonConvert.SerializeObject(new CompanyViewModel { CompanyId = companyId })
              );
            manageUser.Users = JsonConvert.DeserializeObject<List<UserViewModel>>(response.Data.ToString());
            if (manageUser.Users.Count() > 0)
            {
                manageUser.CompanyName = manageUser.Users.FirstOrDefault().Company;
                manageUser.CompanyStatus = manageUser.Users.FirstOrDefault().CompanyStatus;
                manageUser.Industry = manageUser.Users.FirstOrDefault().Industry;
            }
            uri = new Uri(Constants.BASE_URI, Constants.GET_SUBSCRIPTION_DETAILS_FOR_COMPANY_API);
            response = apiClient.Post<Response>(uri,
               new Dictionary<string, string>()
                  {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                  }, JsonConvert.SerializeObject(new CompanyViewModel { CompanyId = companyId })
              );
            CompanySubscriptionViewModel companySubscription = !string.IsNullOrEmpty(Convert.ToString(response.Data)) ? JsonConvert.DeserializeObject<CompanySubscriptionViewModel>(response.Data.ToString()) : null;
            if (companySubscription != null)
            {
                manageUser.SubscriptionName = companySubscription.SubscriptionName;
                manageUser.BillingCycle = companySubscription.BillingCycle;
                manageUser.UsersUpTo = companySubscription.UsersUpTo;
                manageUser.ValidTill = companySubscription.ValidTill.ToString("dd-MMM-yyyy");
                manageUser.BilledAmount = companySubscription.BilledAmount;
                manageUser.ActiveFrom = companySubscription.ActiveFrom.ToString("dd-MMM-yyyy");
            }

            return PartialView("_CompanyUsers", manageUser);
        }

        /// <summary>
        /// activates or deactivates a company
        /// </summary>
        /// <param name="companies"></param>
        /// <returns></returns>
        public ActionResult ActivateOrDeactivaeCompany(long companyId)
        {

            if (companyId == 0)
                return View();

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.ACTIVATE_DEACTIVATE_COMPANY);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(new CompanyViewModel { CompanyId = companyId })
                   );

                if (response.Status)
                    return Json(new { status = "Success", message = "Success" });
                //  return RedirectToAction("Index");
                return Json(new { status = "Success", message = "Success" });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        private List<SelectListItem> CompanyToSelectList(List<CompanyViewModel> companies)
        {
            return (from c in companies
                    select new SelectListItem
                    {
                        Text = c.Name,
                        Value = Convert.ToString(c.CompanyId)
                    }).ToList();
        }
    }
}