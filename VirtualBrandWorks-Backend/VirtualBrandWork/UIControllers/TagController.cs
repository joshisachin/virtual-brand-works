﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork.UIControllers
{
    public class TagController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MasterFolderController));
        private readonly APIClient apiClient = new APIClient();

        [HttpGet]
        public ActionResult Index()
        {
            logger.Info("Tag/Index is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_TAGS_WITH_INDUSTIRES);
                Response response = apiClient.Get<Response>(uri,
                        new Dictionary<string, string>()
                           {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                           }
                       );

                return View(JsonConvert.DeserializeObject<List<TagViewModel>>(response.Data.ToString()));
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            logger.Info("Tag/Index is called");
            try
            {
                TagViewModel model = new TagViewModel();
                List<CategoryViewModel> categories = GetCategories();
                model.Categories = CategoryToSelectList(categories);
                return View(model);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TagViewModel model)
        {
            logger.Info("Tag/Index is called");
            if (!ModelState.IsValid || model.CategoryId == 0 || string.IsNullOrEmpty(model.Name))
            {
                List<CategoryViewModel> categories = GetCategories();
                model.Categories = CategoryToSelectList(categories);
                TempData["Error"] = "Industry or Tag Name is empty";
                return View(model);
            }
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.CREATE_TAG);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(model)
                   );
                if (response.Status)
                {
                    TempData["Success"] = "Tag created successfully.";
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while creating tag";
                }
                logger.Info("Create tag is completed");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            logger.Info("Tag/Edit get is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_TAG);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(new TagViewModel { TagId = id })
                    );
                var model =  JsonConvert.DeserializeObject<TagViewModel>(response.Data.ToString());
                var categories = GetCategories();
                model.Categories = CategoryToSelectList(categories);
                return View(model);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TagViewModel model)
        {
            if (!ModelState.IsValid || model.CategoryId == 0 || string.IsNullOrEmpty(model.Name))
            {
                List<CategoryViewModel> categories = GetCategories();
                model.Categories = CategoryToSelectList(categories);
                TempData["Error"] = "Industry or Tag Name is empty";
                return View(model);
            }
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.EDIT_TAG);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                                { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(model)
                   );
                if (response.Status)
                {
                    TempData["Success"] = "Tag edited successfully.";
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while updating tag";
                }
                logger.Info("Edit tag is completed");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            logger.Info("Tag/Delete get is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.DELETE_TAG);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(new TagViewModel { TagId = id })
                    );
                if (response.Status)
                {
                    TempData["Success"] = "Tag deleted successfully.";
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while deleting tag";
                }
                logger.Info("Delete tag is completed");
                return Json(new { status = "Success", message = "Success" });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

            #region Private Methods
            private List<CategoryViewModel> GetCategories()
        {
            Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CATEGORIES_API);
            Response response = apiClient.Get<Response>(uri,
                 new Dictionary<string, string>()
                    {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                    }
                );
            return JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString());
        }
        private List<SelectListItem> CategoryToSelectList(List<CategoryViewModel> categories)
        {
            List<SelectListItem> categoryList = new List<SelectListItem>();

            foreach (CategoryViewModel category in categories)
            {
                    categoryList.Add(new SelectListItem()
                    {
                        Text = category.CategoryName,
                        Value = Convert.ToString(category.CategoryId)
                    });
            }
            return categoryList;
        }
        #endregion
    }
}