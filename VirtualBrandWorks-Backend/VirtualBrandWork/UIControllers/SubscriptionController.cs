﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using Newtonsoft.Json;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.UIControllers
{
    [Authorize]
    [AuthorizeUser(Role = "SuperAdmin")]
    [SessionExpire]
    public class SubscriptionController : Controller
    {
       private readonly  log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SubscriptionController));
        private readonly APIClient apiClient = new APIClient();
            
        // GET: Subscriptions
        [HttpGet]
        public ActionResult Index()
        {
            logger.Info("Index is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_SUBSCRIPTION_API);
                Response response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }
                    );
                logger.Info("Index is complete");
                return View(JsonConvert.DeserializeObject<List<SubscriptionViewModel>>(response.Data.ToString()));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return View("Error");
            }
        }

        // GET: Subscriptions/Details/5
        public ActionResult Details(int id)
        {
            logger.Info("Details is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.SUBSCRIPTION_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(new SubscriptionViewModel { SubscriptionId = id })
                   );

                logger.Info("Details get is complete");
                return View(JsonConvert.DeserializeObject<SubscriptionViewModel>(response.Data.ToString()));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return View("Error");
            }
            return View();
        }

        // GET: Subscriptions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subscriptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubscriptionViewModel subscription)
        {
            logger.Info("Create post is called");

            if (!ModelState.IsValid)
                return View(subscription);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.SUBSCRIPTION_CREATE_API);
                Response response = apiClient.Post<Response>(uri,
                  new Dictionary<string, string>()
                     {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                     }, JsonConvert.SerializeObject(subscription)
                 );

                if (response.Status)
                    TempData["Success"] = "The subscription plan has been added successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while creating subscription plan";
                }
                logger.Info("Create post is complete");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(string.Concat(ex.Message, JsonConvert.SerializeObject(subscription, Formatting.Indented, new JsonSerializerSettings { })), ex);
                return View("Error");
            }

        }

        // GET: Subscriptions/Edit/5
        public ActionResult Edit(int id, string pId)
        {
            logger.Info("Edit get is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.SUBSCRIPTION_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(new SubscriptionViewModel { SubscriptionId = id, PlanId = pId })
                   );

                logger.Info("Edit get is complete");
                return View(JsonConvert.DeserializeObject<SubscriptionViewModel>(response.Data.ToString()));
            }
            catch (Exception ex)
            {
                logger.Error(string.Concat(ex.Message, id), ex);
                return View("Error");
            }

        }

        // POST: Subscriptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SubscriptionViewModel subscription)
        {
            logger.Info("Edit post is called");

            if (!ModelState.IsValid)
                return View(subscription);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.SUBSCRIPTION_UPDATE_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(subscription)
                   );

                if (response.Status)
                    TempData["Success"] = "Subscription plan updated successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while updating subscription plan";
                }
                logger.Info("Edit post is complete");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(string.Concat(ex.Message, JsonConvert.SerializeObject(subscription, Formatting.Indented, new JsonSerializerSettings { })), ex);
                return View("Error");
            }

        }

        /// <summary>
        ///POST: Subscriptions/Delete/5 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string ProductId)
        {
            logger.Info("Delete post is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.SUBSCRIPTION_DELETE_API);
                Response response = apiClient.Delete<Response>(uri,
                 new Dictionary<string, string>()
                    {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                    }, JsonConvert.SerializeObject(new SubscriptionViewModel { SubscriptionId = id, ProductId = ProductId })
                );

                if (response.Status)
                    TempData["Success"] = "Subscription plan deleted successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while deleting subscription plan";
                }
                logger.Info("Delete post is complete");

                return Json(new { status = "Success", message = "Success" });
    
            }
            catch (Exception ex)
            {
                logger.Error(string.Concat(ex.Message, id), ex);
                return View("Error");
            }
        }



    }
}
