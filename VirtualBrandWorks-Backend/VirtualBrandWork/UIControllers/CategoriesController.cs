﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using Newtonsoft.Json;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Entity;
using System;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.UIControllers
{
    [Authorize]
    [AuthorizeUser(Role = "SuperAdmin")]
    [SessionExpire]
    public class CategoriesController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CategoriesController));
        private readonly APIClient apiClient = new APIClient();
      
        /// <summary>
        /// category list index page with authorization
        /// </summary>
        /// <returns></returns>
       
        public ActionResult Index(string returnUrl)
        {
            logger.Info("Index is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CATEGORIES_API);
                Response response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }
                    );
                logger.Info("Index is completed");
                return View(JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString()));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return View("Error");
            }
        }

        public JsonResult GetCategories()
        {
            Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CATEGORIES_API);
            Response response = apiClient.Get<Response>(uri,
                  new Dictionary<string, string>()
                     {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                     }
                 );
            return Json(JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString()),JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// create category get 
        /// </summary>
        /// <returns></returns>
        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }
        /// <summary>
        /// create category post with authorization header
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryId,CategoryName,Description")] CategoryViewModel category)
        {
            logger.Info("Create post is called");

            if (!ModelState.IsValid)
                return View(category);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.CATEGORIES_CREATE_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(category)
                    );

                if (response.Status)
                    TempData["Success"] = "Industry created successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while creating industry";
                }
                logger.Info("Create post is completed");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}",ex);
                return View("Error");
            }
        }

        /// <summary>
        /// edit category get method wih authorization header
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Categories/Edit/5
        public ActionResult Edit(long id)
        {
            logger.Info("Edit get is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                //get folder details
                Uri uri = new Uri(Constants.BASE_URI, Constants.CATEGORIES_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(new CategoryViewModel { CategoryId = id })
                    );

                logger.Info("Edit get is completed");
                return View(JsonConvert.DeserializeObject<CategoryViewModel>(response.Data.ToString()));
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}",ex);
                return View("Error");
            }
        }
        /// <summary>
        /// edit category post method :ModifiedDetails :shubhangi derkar : 27/04/2020
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryId,CategoryName,Description")] CategoryViewModel category)
        {
            logger.Info("Edit post is called");

            if (!ModelState.IsValid)
                return View(category);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.CATEGORIES_UPDATE_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(category)
                   );

                if (response.Status)
                    TempData["Success"] = "Industry updated successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while updating industry";
                }
                logger.Info("Edit post is completed");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}",ex);
                return View("Error");
            }
        }
        
        /// <summary>
        /// delete category post method :ModifiedDetails :shubhangi derkar : 27/04/2020 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // POST: Categories/Delete/5 
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            logger.Info("Delete post is called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.CATEGORIES_DELETE_API);
                Response response = apiClient.Delete<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(new CategoryViewModel { CategoryId = id })
                   );

                if (response.Status)
                    TempData["Success"] = "Industry deleted successfully";
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        TempData["Error"] = "An error occured while deleting industry.";
                }
                logger.Info("Delete post is completed");
                return Json(new { status = "Success", message = "Success" });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}",ex);
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IsCompanyAssociated(long id)
        {
            logger.Info("IsCompanyAssociated called");

            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.COMPANY_ASSOCIATED_TO_INDUSTRY);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                       }, JsonConvert.SerializeObject(new CategoryViewModel { CategoryId = id })
                   );
                logger.Info("IsCompanyAssociated completed");
                if (response.Status)
                {
                    return Json(new { status = "True", message = "CompanyAssociated" });
                }
                else
                {
                    return Json(new { status = "False", message = "CompanyNotAssociated" });
                }

            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {id}", ex);
                return View("Error");
            }
        }
    }
}
