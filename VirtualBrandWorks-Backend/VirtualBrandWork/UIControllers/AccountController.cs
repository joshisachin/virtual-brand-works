﻿using Facebook;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWork.Models;
using static VirtualBrandWork.Entity.Constants;

namespace VirtualBrandWork.UIControllers
{
    public class AccountController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountController));
        private readonly APIClient apiClient = new APIClient();

        //[AllowAnonymous]
        public ActionResult Login()
        {
            var model = new LoginViewModel();
            if (User.Identity.IsAuthenticated == false)
                return View(model);
            else
            {
                var roles = ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value);
                if (roles.Where(o => o.Equals(UserRoles.SuperAdmin.ToString(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault() != null)
                    return RedirectToAction("AdminDashboard", "Home");
                else return View(model); //return RedirectToAction("Index", "Home");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                logger.Info("Login is called");

                if (!ModelState.IsValid)
                    return View(model);
                model.SaLoginRequestKey = SA_LOGIN_REQUEST_KEY;
                Uri uri = new Uri(BASE_URI, ACCOUNT_LOGIN);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(model));
                if (response.Data == null)
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        ViewBag.Error = response.Message;
                    else
                        ViewBag.Error = "An error occured while login please try after some time.";
                    return View(model);
                }
                else
                {
                    UserContext userViewModel = JsonConvert.DeserializeObject<UserContext>(response.Data.ToString());

                    if (!userViewModel.UserRole.Equals(UserRoles.SuperAdmin.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        ViewBag.Error = "Invalid User Role Access";
                        return View();
                    }

                        if (userViewModel != null)
                        {
                            IdentitySignin(userViewModel, null, model.RememberMe);
                            Session["JWTToken"] = userViewModel.JWTToken;
                            logger.Info("Login is completed");
                        }
                        return RedirectToAction("Index", "Home");
                        //if (userViewModel.UserRole.Equals(UserRoles.User.ToString(), StringComparison.OrdinalIgnoreCase))
                        //{
                        //    return RedirectToAction("UserDummyView", "Home");
                        //}
                        //else
                        //{
                        //    return RedirectToAction("MasterLibrary", "Home");
                        //}

                    }
                }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return RedirectToAction("Login");
            }
        }
        public ActionResult LogOut()
        {
            IdentitySignout();
            HttpContext.Session.Clear();
            HttpContext.Session.Abandon();

            //Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetNoStore();
            return RedirectToAction("Login");
        }

        public ActionResult SignUp()
        {
            return RedirectToAction("Login");

            logger.Info("SignUp is called");
            if (User.Identity.IsAuthenticated == true)
                return RedirectToAction("Index", "Home");
            try
            {
                SignUpViewModel signUpViiewModel = new SignUpViewModel();

                Uri uri = new Uri(BASE_URI, GET_ALL_SUBSCRIPTION_FOR_ADMIN_API);
                Response response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );
                signUpViiewModel.Subscriptions = JsonConvert.DeserializeObject<List<SubscriptionViewModel>>(response.Data.ToString());

                uri = new Uri(BASE_URI, GET_ALL_ROLES);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );

                signUpViiewModel.UserRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(response.Data.ToString());
                signUpViiewModel.SelectedRole = signUpViiewModel.UserRoles.Where(obj => obj.RoleId == (int)UserRoles.Admin).FirstOrDefault();

                uri = new Uri(BASE_URI, GET_ALL_COUNTRIES);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );

                signUpViiewModel.Countries = JsonConvert.DeserializeObject<List<CountryViewModel>>(response.Data.ToString());

                uri = new Uri(BASE_URI, GET_ALL_CATEGORIES_API);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );
                signUpViiewModel.Categories = JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString());

                logger.Info("SignUp is complete");
                return View(signUpViiewModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        public ActionResult SignUpConfirmation(SignUpViewModel signUpViiewModel)
        {
            return RedirectToAction("Login");
            logger.Info("SignUp is called");
            try
            {
                Uri uri = new Uri(BASE_URI, SUBSCRIPTION_DETAILS_API);
                Response response = apiClient.Post<Response>(uri,
                    new Dictionary<string, string>()
                       {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                       }, JsonConvert.SerializeObject(new SubscriptionViewModel { SubscriptionId = signUpViiewModel.SelectedPlan, PlanId = signUpViiewModel.SelectedPlanPrice })
                   );
                signUpViiewModel.SelectedSubscription = JsonConvert.DeserializeObject<SubscriptionViewModel>(response.Data.ToString());

                uri = new Uri(BASE_URI, GET_ALL_SUBSCRIPTION_FOR_ADMIN_API);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );
                signUpViiewModel.Subscriptions = JsonConvert.DeserializeObject<List<SubscriptionViewModel>>(response.Data.ToString());

                uri = new Uri(BASE_URI, GET_ALL_COUNTRIES);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );

                signUpViiewModel.Countries = JsonConvert.DeserializeObject<List<CountryViewModel>>(response.Data.ToString());

                if (signUpViiewModel.SelectedCountryId != 0)
                {
                    uri = new Uri(BASE_URI, GET_STATES_BY_COUNTRY_API);
                    response = apiClient.Post<Response>(uri,
                       new Dictionary<string, string>()
                          {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                          }, JsonConvert.SerializeObject(new StateViewModel { CountryID = signUpViiewModel.SelectedCountryId })
                      );

                    signUpViiewModel.States = JsonConvert.DeserializeObject<List<StateViewModel>>(response.Data.ToString());
                }

                if (signUpViiewModel.SelectedStateId != 0)
                {
                    uri = new Uri(BASE_URI, GET_CITIES_BY_STATE_API);
                    response = apiClient.Post<Response>(uri,
                       new Dictionary<string, string>()
                          {
                           { "Authorization", $"{"Bearer "}{string.Empty}" }
                          }, JsonConvert.SerializeObject(new CityViewModel { StateID = signUpViiewModel.SelectedStateId })
                      );

                    signUpViiewModel.Cities = JsonConvert.DeserializeObject<List<CityViewModel>>(response.Data.ToString());
                }

                if (signUpViiewModel.SelectedCategoryId > 0)
                {
                    uri = new Uri(BASE_URI, GET_ALL_CATEGORIES_API);
                    response = apiClient.Get<Response>(uri,
                         new Dictionary<string, string>()
                            {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                            }
                        );
                    signUpViiewModel.Categories = JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString());
                }

                logger.Info("SignUp is complete");
                return View(signUpViiewModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        public ActionResult SignUpAsUser()
        {
            return RedirectToAction("Login");
            logger.Info("SignUpAsUser called");
            if (User.Identity.IsAuthenticated == true)
                return RedirectToAction("Index", "Home");
            try
            {
                if (String.IsNullOrEmpty(Request.QueryString["EmailId"]))
                {
                    return RedirectToAction("Index", "Home");
                }
                UserInviteViewModel userInvite = new UserInviteViewModel();
                userInvite.EmailId = EncodeDecodeString.Decrypt(Request.QueryString["EmailId"]);

                Uri uri = new Uri(BASE_URI, GET_USER_INVITE);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(userInvite));
                UserInviteViewModel user = JsonConvert.DeserializeObject<UserInviteViewModel>(response.Data.ToString());

                SignUpViewModel signUpViewModel = new SignUpViewModel();

                uri = new Uri(BASE_URI, GET_ALL_ROLES);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );

                signUpViewModel.UserRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(response.Data.ToString());
                signUpViewModel.SelectedRole = signUpViewModel.UserRoles.Where(obj => obj.RoleId == user.RoleId).FirstOrDefault();

                signUpViewModel.CompanyName = user.CompanyName;
                signUpViewModel.CompanyId = user.CompanyId;
                signUpViewModel.Email = user.EmailId;

                uri = new Uri(BASE_URI, GET_ALL_COUNTRIES);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );

                signUpViewModel.Countries = JsonConvert.DeserializeObject<List<CountryViewModel>>(response.Data.ToString());

                uri = new Uri(BASE_URI, GET_ALL_CATEGORIES_API);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );
                signUpViewModel.Categories = JsonConvert.DeserializeObject<List<CategoryViewModel>>(response.Data.ToString());

                logger.Info("SignUpAsUser is complete");
                return View(signUpViewModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        /// <summary>
        /// post method for signupasuser 
        /// </summary>
        /// <param name="signUpViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SignUpAsUser(SignUpViewModel signUpViewModel)
        {
            return RedirectToAction("Login");
            logger.Info("SignUpAsUser called");
            if (signUpViewModel == null)
            {
                return View();
            }
            try
            {
                Uri uri = new Uri(BASE_URI, GET_USER_INVITE);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(new UserInviteViewModel { EmailId = signUpViewModel.Email }));
                UserInviteViewModel user = JsonConvert.DeserializeObject<UserInviteViewModel>(response.Data.ToString());

                signUpViewModel.CompanyName = user.CompanyName;

                uri = new Uri(BASE_URI, GET_ALL_ROLES);
                response = apiClient.Get<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                        }
                    );

                signUpViewModel.UserRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(response.Data.ToString());

                if (signUpViewModel.SelectedCountryId != null)
                {
                    uri = new Uri(BASE_URI, GET_ALL_COUNTRIES);
                    response = apiClient.Get<Response>(uri,
                         new Dictionary<string, string>()
                            {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                            }
                        );

                    signUpViewModel.Countries = JsonConvert.DeserializeObject<List<CountryViewModel>>(response.Data.ToString());

                    uri = new Uri(BASE_URI, GET_ALL_STATES);
                    response = apiClient.Post<Response>(uri,
                          new Dictionary<string, string>()
                            {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                            }, JsonConvert.SerializeObject(new StateViewModel { CountryID = signUpViewModel.SelectedCountryId })
                        );

                    signUpViewModel.States = JsonConvert.DeserializeObject<List<StateViewModel>>(response.Data.ToString());

                    uri = new Uri(BASE_URI, GET_ALL_CITIES);
                    response = apiClient.Post<Response>(uri,
                          new Dictionary<string, string>()
                            {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                            }, JsonConvert.SerializeObject(new CityViewModel { StateID = signUpViewModel.SelectedStateId })
                        );

                    signUpViewModel.Cities = JsonConvert.DeserializeObject<List<CityViewModel>>(response.Data.ToString());

                    signUpViewModel.Country = signUpViewModel.Countries.Where(c => c.ID == signUpViewModel.SelectedCountryId).FirstOrDefault().Name;
                    signUpViewModel.State = signUpViewModel.States.Where(c => c.ID == signUpViewModel.SelectedStateId).FirstOrDefault().Name;
                    signUpViewModel.City = signUpViewModel.Cities.Where(c => c.ID == signUpViewModel.SelectedCityId).FirstOrDefault().Name;
                }

                uri = new Uri(BASE_URI, SIGNUP_AS_USER);
                response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(signUpViewModel));
                if (response.Status == true)
                {
                    logger.Info("SignUpAsUser completed");
                    TempData["Success"] = response.Message;
                    return RedirectToAction("Login");
                }
                ViewBag.Message = response.Message;
                logger.Info("SignUpAsUser completed");
                return View(signUpViewModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        //Post :/ForgotPassword
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            logger.Info("ForgotPassword is called");

            try
            {
                Uri uri = new Uri(Entity.Constants.BASE_URI, Entity.Constants.FORGOT_PASSWORD);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(model));

                if (response.Status)
                {
                    TempData["Success"] = "Reset Code sent to your email";
                    logger.Info("ForgotPassword is completed");
                    // return RedirectToAction("ResetPassword");
                    return View("ResetPassword", new ResetPasswordViewModel() { Email = model.EmailId });
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        ViewBag.Error = response.Message;
                    else
                        ViewBag.Error = "An error occured while creating content.";
                }
                return View();
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return RedirectToAction("Login");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ResendCode(ResetPasswordViewModel model)
        {
            try
            {
                Uri uri = new Uri(Entity.Constants.BASE_URI, Entity.Constants.FORGOT_PASSWORD);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(new ForgotPasswordViewModel() { EmailId = model.Email }));
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return Json(new Response() { Status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            logger.Info("ResetPassword is called");

            if (!ModelState.IsValid)
                return View(model);
            try
            {
                Uri uri = new Uri(Entity.Constants.BASE_URI, Entity.Constants.RESET_PASSWORD);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(model));
                if (response.Status)
                {
                    TempData["Success"] = "Password Reset Successful";
                    logger.Info("ResetPassword is completed");
                    return RedirectToAction("Login");
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        ViewBag.Error = response.Message;
                    else
                        ViewBag.Error = "An error occured while password reset.";
                }
                return View(model);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return View(model);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResendResetCode(ResetPasswordViewModel model)
        {
            logger.Info("ResendResetCode is called");

            if (!ModelState.IsValidField("Email"))
                return View("ForgotPassword"); // redirect to forgot password if resent code
            try
            {
                ForgotPasswordViewModel email = new ForgotPasswordViewModel();
                email.EmailId = model.Email;
                Uri uri = new Uri(Entity.Constants.BASE_URI, Entity.Constants.FORGOT_PASSWORD);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(model));
                if (response.Status)
                {
                    ViewBag.Success = "Reset Password Code has been sent to your email id. (Valid for next 10 Minutes!)";
                    logger.Info("ResendResetCode is completed");
                    return RedirectToAction("ResetPassword");
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        ViewBag.Error = response.Message;
                    else
                        ViewBag.Error = "An error occured while creating content.";
                }
                return RedirectToAction("ResetPassword");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return View(model);
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [AllowAnonymous]
        public JsonResult GetStatesByCountryId(int countryID)
        {
            logger.Info("GetStatesByCountryId is called");
            try
            {
                Uri uri = new Uri(VirtualBrandWork.Entity.Constants.BASE_URI, VirtualBrandWork.Entity.Constants.GET_STATES_BY_COUNTRY_API);
                Response response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                      }, JsonConvert.SerializeObject(new StateViewModel { CountryID = countryID })
                  );
                logger.Info("GetStatesByCountryId is complete");
                return Json(JsonConvert.DeserializeObject<List<StateViewModel>>(response.Data.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return Json(new List<StateViewModel>(), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [AllowAnonymous]
        public JsonResult GetCitiesByStateId(int stateID)
        {
            logger.Info("GetCitiesByStateId is called");
            try
            {
                Uri uri = new Uri(VirtualBrandWork.Entity.Constants.BASE_URI, VirtualBrandWork.Entity.Constants.GET_CITIES_BY_STATE_API);
                Response response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                           { "Authorization", $"{"Bearer "}{string.Empty}" }
                      }, JsonConvert.SerializeObject(new CityViewModel { StateID = stateID })
                  );
                logger.Info("GetStatesByCountryId is complete");
                return Json(JsonConvert.DeserializeObject<List<CityViewModel>>(response.Data.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return Json(new List<CityViewModel>(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get method for Change Password when the user is logged in
        /// </summary>
        /// <param name="changePassword"></param>
        /// <param></param>
        /// <param></param>
        [AuthorizeUser(Role = "SuperAdmin")]
        [SessionExpire]
        [HttpGet]
        public ActionResult ChangePassword()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return View("Login");
            }
            return View();
        }

        /// <summary>
        /// Post method for Change Password when the user is logged in
        /// </summary>
        /// <param name="changePassword"></param>
        /// <param></param>
        /// <param></param>
        [AuthorizeUser(Role = "SuperAdmin")]
        [HttpPost]
        [Authorize]
        [SessionExpire]
        public ActionResult ChangePassword(ChangePasswordViewModel changePassword)
        {
            logger.Info("ChangePassword is called");

            if (!ModelState.IsValid)
                return View(changePassword);

            if (User.Identity.IsAuthenticated == false)
                return View("Login");
            try
            {
                var userId = User.Identity.GetUserId();
                changePassword.UserId = Convert.ToInt32(userId);
                Uri uri = new Uri(VirtualBrandWork.Entity.Constants.BASE_URI, VirtualBrandWork.Entity.Constants.CHANGE_PASSWORD);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(changePassword)
                    );
                if (response.Status)
                {
                    TempData["PasswordChanged"] = "Your password has been changed successfully";
                    logger.Info("ChangePassword is completed");
                    return Redirect("ChangePassword");
                }
                else
                    TempData["Failed"] = "You have entered an incorrect password";
                return Redirect("ChangePassword");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(changePassword, Formatting.Indented)}", ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return View(changePassword);
            }
        }

        [HttpPost]
        public JsonResult ValidateUserEmailAndCompanyName(string companyName, string email)
        {
            logger.Info("ValidateUserEmailAndCompanyName is called");
            try
            {
                Uri uri = new Uri(BASE_URI, VALIDATE_USER_EMAIL_AND_COMPANY_NAME);
                Response response = apiClient.Post<Response>(uri,
                   new Dictionary<string, string>()
                      {
                           { "Authorization", $"{"Bearer "}{string.Empty}" }
                      }, JsonConvert.SerializeObject(new SignUpViewModel { CompanyName = companyName, Email = email })
                  );
                logger.Info("ValidateUserEmailAndCompanyName is complete");
                return Json(JsonConvert.DeserializeObject<List<ValidationSummaryViewModel>>(response.Data.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            return Json(new List<ValidationSummaryViewModel>(), JsonRequestBehavior.AllowGet);
        }

        #region identityHelper
        public void IdentitySignin(UserContext userContext, string providerKey = null, bool isPersistent = false)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.NameIdentifier, userContext.UserId.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, userContext.UserName));
            claims.Add(new Claim(ClaimTypes.Role, userContext.UserRole));
            claims.Add(new Claim(ClaimTypes.UserData, Convert.ToString(userContext.Industry)));

            CreateJWTTokenCookie(userContext.JWTToken, isPersistent);

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            // add to user here!
            AuthenticationManager.SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = isPersistent,
                ExpiresUtc = DateTime.UtcNow.AddHours(12)
            }, identity);

        }

        private void CreateJWTTokenCookie(string JWTToken, bool isPersistent)
        {
            HttpCookie myCookie = new HttpCookie("JWTToken");
            //Add key-values in the cookie
            myCookie.Value = JWTToken;

            //set cookie expiry date-time. Made it to last for next 12 hours.
            if (isPersistent)
                myCookie.Expires = DateTime.Now.AddHours(12);

            //Most important, write the cookie to client.
            Response.Cookies.Add(myCookie);
        }

        public void IdentitySignout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                                          DefaultAuthenticationTypes.ExternalCookie);
        }

        public ActionResult UserInvite()
        {
            UserInviteViewModel userInvite = new UserInviteViewModel();
            //userInvite.EmailId = "divya.evontech@gmail.com";
            userInvite.EmailId = "william@yopmail.com";
            userInvite.FirstName = "William";
            userInvite.LastName = "Hemsworth";
            try
            {
                Uri uri = new Uri(VirtualBrandWork.Entity.Constants.BASE_URI, VirtualBrandWork.Entity.Constants.INVITE_USER);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(userInvite));
                if (response.Status)
                {
                    return RedirectToAction("Index", "Home");
                }

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(userInvite, Formatting.Indented)}", ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return RedirectToAction("Index", "Home");
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return Request.GetOwinContext().Authentication;
            }
        }
        #endregion identityHelper

        #region External Login

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            ControllerContext.HttpContext.Session.RemoveAll();
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            try
            {
                var externalIdentity = AuthenticationManager.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);

                var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (loginInfo == null)
                {
                    return RedirectToAction("Login");
                }
                ExternalLoginViewModel externalLogin = new ExternalLoginViewModel();
                var identity = AuthenticationManager.GetExternalIdentity
                                                (DefaultAuthenticationTypes.ExternalCookie);

                if (loginInfo.Login.LoginProvider.Equals("Facebook"))
                {
                    externalLogin.AccessToken = identity.FindFirstValue("FacebookAccessToken");
                    externalLogin.Provider = loginInfo.Login.LoginProvider;
                }
                else if (loginInfo.Login.LoginProvider.Equals("Google"))
                {
                    externalLogin.AccessToken = identity.FindFirstValue("Google_AccessToken");
                    externalLogin.Provider = loginInfo.Login.LoginProvider;
                }

                Uri uri = new Uri(Entity.Constants.BASE_URI, Entity.Constants.EXTERNAL_LOGIN);
                Response response = apiClient.Post<Response>(uri, null, JsonConvert.SerializeObject(externalLogin));
                if (response.Data == null)
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        TempData["Error"] = response.Message;
                    else
                        ViewBag.Error = "An error occured while login please try after some time.";
                    return View("Login");
                }
                else
                {
                    UserContext userViewModel = JsonConvert.DeserializeObject<UserContext>(response.Data.ToString());
                    if (userViewModel != null)
                    {
                        IdentitySignin(userViewModel, null);
                        Session["JWTToken"] = userViewModel.JWTToken;
                        logger.Info("ExternalLogin completed");
                    }
                    if (userViewModel.UserRole.Equals(UserRoles.SuperAdmin.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("MasterLibrary", "Home");
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                ViewBag.Result = Entity.Constants.INTERNAL_SERVER_ERROR;
                return RedirectToAction("Login");
            }

        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
