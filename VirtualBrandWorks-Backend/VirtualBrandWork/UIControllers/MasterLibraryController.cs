﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.UIControllers
{
    [AuthorizeUser(Role = "Admin")]
    public class MasterLibraryController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MasterContentController));
        private readonly APIClient apiClient = new APIClient();
        // GET: MasterLibrary
        public ActionResult FolderDetails(int folderId)
        {
            ViewBag.FolderId = folderId;
            return View();
        }
        [HttpGet]
        public JsonResult GetFolderAndContentForPreviewPopUp(MasterFolderViewModel folder)
        {
            logger.Info("GetFolderAndContentForPreviewPopUp is called");
            try
            {
                Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_CONTENT_PREVIEW_POP_UP_API);
                Response response = apiClient.Post<Response>(uri,
                     new Dictionary<string, string>()
                        {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                        }, JsonConvert.SerializeObject(new MasterFolderViewModel { MasterFolderId = folder.MasterFolderId })
                    );
                logger.Info("GetFolderAndContentForPreviewPopUp is completed");

                ContentPreviewViewModel listContent = JsonConvert.DeserializeObject<ContentPreviewViewModel>(response.Data.ToString());
                return Json(listContent, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Json(new List<MasterFolderViewModel>(), JsonRequestBehavior.AllowGet);
            }
        }

    }
}