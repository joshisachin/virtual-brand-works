﻿using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Unity;
using Service.Interfaces;
using Service;
using VirtualBrandWork.App_Start;
using Repository.Interfaces;
using Repository.Repository;
using System.Configuration;
using System.Web.Http.Cors;
using VirtualBrandWork.Notification.Interfaces;
using VirtualBrandWork.Notification;

namespace VirtualBrandWork
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var corsOrigins = ConfigurationManager.AppSettings["Cors_Origins"];
            var corsConfig = new EnableCorsAttribute(corsOrigins, "*", "*");
            config.EnableCors(corsConfig);
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var container = new UnityContainer();
            container.RegisterType<IUserTagService, UserTagService>();
            container.RegisterType<IUserTagRepository, UserTagRepository>();

            //categoryService
            container.RegisterType<ICategoriesService, CategoriesService>();
            container.RegisterType<ICategoriesRepository, CategoriesRepository>();
            //masterfolder
            container.RegisterType<IMasterFolderService, MasterFolderService>();
            container.RegisterType<IMasterFolderRepository, MasterFolderRepository>();
            //masterContent
            container.RegisterType<IMasterContentService, MasterContentService>();
            container.RegisterType<IMasterContentRepository, MasterContentRepository>();
            //subscription
            container.RegisterType<ISubscriptionService, SubscriptionService>();
            container.RegisterType<ISubscriptionRepository, SubscriptionRepository>();
            //tag
            container.RegisterType<ITagService, TagService>();
            container.RegisterType<ITagRepository, TagRepository>();
            //user
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IUserRepository, UserRepository>();
            //Quiz
            container.RegisterType<IQuizService, QuizService>();
            container.RegisterType<IQuizRepository, QuizRepository>();
            //role
            container.RegisterType<IRoleService, RoleService>();
            container.RegisterType<IRoleRepository, RoleRepository>();
            //userinformation
            container.RegisterType<IUserInformationService, UserInformationService>();
            container.RegisterType<IUserInformationRepository, UserInformationRepository>();
            //Company
            container.RegisterType<ICompanyService, CompanyService>();
            container.RegisterType<ICompanyRepository, CompanyRepository>();
            //Team
            container.RegisterType<ITeamService, TeamService>();
            container.RegisterType<ITeamRepository, TeamRepository>();
            //BuildPlaylist
            container.RegisterType<IBuildPlaylistService, BuildPlaylistService>();
            container.RegisterType<IBuildPlaylistRepository, BuildPlaylistRepository>();
            //MyPlaylist
            container.RegisterType<IMyPlaylistService, MyPlaylistService>();
            container.RegisterType<IMyPlaylistRepository, MyPlaylistRepository>();

            container.RegisterType<IPlaylistService, PlaylistService>();
            container.RegisterType<IPlaylistRepository, PlaylistRepository>();

            container.RegisterType<IAWSS3FileStorage, AWSS3FileStorage>();

            container.RegisterType<ICompanyContentService, CompanyContentService>();
            container.RegisterType<ICompanyContentRepository, CompanyContentRepository>();

            container.RegisterType<IStripePaymentUtility, StripePaymentUtility>();

            container.RegisterType<ICompanyQuizService, CompanyQuizService>();
            container.RegisterType<ICompanyQuizRepository, CompanyQuizRepository>();

            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<IFcmNotificationService, FcmNotificationService>();

            container.RegisterType<INotificationService, NotificationService>();
            container.RegisterType<INotificationRepository, NotificationRepository>();

            //VBPlay
            container.RegisterType<IVBPlayService, VBPlayService>();
            container.RegisterType<IVBPlayRepository, VBPlayRepository>();

            //PrizeStore
            container.RegisterType<IPrizeStoreService, PrizeStoreService>();
            container.RegisterType<IPrizeStoreRepository, PrizeStoreRepository>();

            config.DependencyResolver = new UnityResolver(container);
        }
    }
}
