﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;

namespace Service
{
    public class UserTagService : IUserTagService
    {
        private readonly IUserTagRepository _userTagRepository;

        public UserTagService(IUserTagRepository userTagRepository)
        {
            _userTagRepository = userTagRepository;
        }
        public void Add(UserTag tag)
        {
            _userTagRepository.Add(tag);
        }
    }
}
