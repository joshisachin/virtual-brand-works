﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;

namespace Service.Interfaces
{
    public interface IUserTagService
    {
        void Add(UserTag tag);
    }
}
