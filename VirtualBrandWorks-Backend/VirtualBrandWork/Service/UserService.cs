﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;

namespace Service
{
    public class UserService : IUserService, IDisposable
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userTagRepository)
        {
            _userRepository = userTagRepository;
        }

        public User GetUser(string name, string password)
        {
            return _userRepository.GetUser(name, password);
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
