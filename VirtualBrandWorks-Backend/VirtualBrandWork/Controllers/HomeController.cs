using Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Notification;
using static VirtualBrandWork.Entity.Constants;
using VirtualBrandWork.Common;
using Newtonsoft.Json;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Filters;
using VirtualBrandWork.Notification.Interfaces;

namespace VirtualBrandWork.Controllers
{
    public class HomeController : Controller
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(HomeController));
        private readonly APIClient apiClient = new APIClient();
        private readonly IEmailService _emailService;

        public HomeController()
        { }
        public HomeController(IEmailService emailService) {
            _emailService = emailService;
        }

        public ActionResult Index()
        {
            try
            {
                logger.Info("Home Index is called");
                if (User.Identity.IsAuthenticated == false)
                    //  return View(GetSubscriptions()); 
                    return RedirectToAction("Login","Account");
                else
                {
                    var roles = ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value);
                    if (roles.Where(o => o.Equals(UserRoles.SuperAdmin.ToString(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault() != null)
                        return View("AdminDashboard");
                    else return RedirectToAction("Login", "Account");
                    //else if(roles.Where(o => o.Equals(UserRoles.User.ToString(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault() != null)
                    //    return Redirect("Home/UserDummyView");
                    //else
                    //    return Redirect("Home/MasterLibrary");
                }

            }
            catch (Exception ex)
            {
                logger.Info(ex.Message, ex);
                return View("Error");
            }
        }

        [AuthorizeUser(Role = "SuperAdmin")]
        [SessionExpire]
        [JWTAuthenticationFilter]
        public ActionResult AdminDashboard()
        {
            return View();
        }

        [AuthorizeUser(Role ="Admin,Trainer")]
        [SessionExpire]
        public ActionResult MasterLibrary()
        {
            logger.Info("Index is called");
            try
            {
                logger.Info("MasterLibrary  is called");
                if (User.Identity.IsAuthenticated == false)
                    return Redirect("Index");
                else
                {
                    var industry = ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == ClaimTypes.UserData).Select(c => c.Value);
                    TempData["UserIndustry"] = industry.ToList()[0];
                    Uri uri = new Uri(Constants.BASE_URI, Constants.GET_ALL_FOLDER_CONTENT_API);
                    Response response = apiClient.Post<Response>(uri,
                         new Dictionary<string, string>()
                            {
                            { "Authorization", $"{"Bearer "}{Request.Cookies["JWTToken"].Value}" }
                            }, JsonConvert.SerializeObject(new SearchModel { Industry = TempData["UserIndustry"].ToString(), SearchText = null, SearchFlag = true })
                        );

                    var folders = JsonConvert.DeserializeObject<List<MasterFolderViewModel>>(response.Data.ToString());

                    logger.Info("MasterLibrary is completed");
                    if (folders != null)
                        return View(folders);
                    else
                        return View("Index");
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message, ex);
                return View("Error");
            }
        }

        public List<SubscriptionViewModel> GetSubscriptions()
        {
            SignUpViewModel signUpViiewModel = new SignUpViewModel();
            Uri uri = new Uri(BASE_URI, Constants.GET_ALL_SUBSCRIPTION_FOR_ADMIN_API);
            Response response = apiClient.Get<Response>(uri,
                 new Dictionary<string, string>()
                    {
                            { "Authorization", $"{"Bearer "}{string.Empty}" }
                    }
                );
            return JsonConvert.DeserializeObject<List<SubscriptionViewModel>>(response.Data.ToString());
        }

        /// <summary>
        /// write to us email
        /// </summary>
        /// <param name="writeToUs"></param>
        /// <returns></returns>
        public ActionResult WriteToUs(WriteToUsViewModel writeToUs)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = "Failed", message = "Failed" });
            }
            try
            {
               _emailService.SendWriteToUs(writeToUs.Email, writeToUs.Message);
                // TempData["Message"] = "Thank you for getting in touch. We will get back to you soon.";
                return Json(new { status = "Success", message = "Success" });
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message, ex);
                return Json(new { status = "Failed", message = "Failed" });
            }
        }
        public ActionResult PrivacyPolicy()
        {
            return View();
        }
        public ActionResult TermsAndConditions()
        {
            return View();
        }

        [AuthorizeUser(Role = "User")]
        [SessionExpire]
        public ActionResult UserDummyView()
        {
            return View();
        }

        [AuthorizeUser(Role = "Admin,Trainer")]
        public ActionResult SendNotificattions()
        {
            FcmNotificationService objNotificationService = new FcmNotificationService();

            //objNotificationService.SendPushNotification()

            return null;
        }
    }
    public class SearchModel
    {
        public string Industry { get; set; }
        public string SearchText { get; set; }

        public bool SearchFlag { get; set; }
    }

}
