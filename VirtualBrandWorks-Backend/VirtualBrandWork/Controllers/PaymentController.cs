﻿using Newtonsoft.Json;
using Service;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Models;
using VirtualBrandWork.Notification;
using VirtualBrandWork.Notification.Interfaces;

namespace VirtualBrandWork.Controllers
{
    [RoutePrefix("api/Payment")]
    public class PaymentController : ApiController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PaymentController));
        private readonly IStripePaymentUtility _stripePaymentUtility;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;

        public PaymentController(IUserService userService, IStripePaymentUtility stripePaymentUtility, IEmailService emailService)
        {
            _userService = userService;
            _stripePaymentUtility = stripePaymentUtility;
            _emailService = emailService;
        }

        /// <summary>
        /// Get stripe checkout session Id
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCheckoutSession")]
        [CheckModelForNull]
        public HttpResponseMessage GetCheckoutSession(StripeCheckoutSessionRequestViewModel requestViewModel)
        {
            logger.Info($"{Constants.GET_CHECKOUT_SESSION} is called");
            try
            {
                if (ModelState.IsValid)
                {
                    StripeCheckoutSessionModel stripeCheckoutSessionModel = _stripePaymentUtility.GetCreateCheckoutSession(requestViewModel.PlanId, requestViewModel.Email, requestViewModel.Subscription, requestViewModel.Amount, requestViewModel.BillingCycle, requestViewModel.FirstName, requestViewModel.LastName, requestViewModel.SuccessUrl, requestViewModel.CancelUrl);//, _taxId);
                    logger.Info($"{Constants.GET_CHECKOUT_SESSION} is completed");
                    return Request.CreateResponse(HttpStatusCode.OK, stripeCheckoutSessionModel);
                }
                else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(requestViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Get stripe checkout session details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCheckoutSessionDetails")]
        public HttpResponseMessage GetCheckoutSession([FromBody] string SessionId)
        {
            logger.Info($"{Constants.GET_CHECKOUT_SESSION_DETAILS} is called");
            try
            {
                if (string.IsNullOrEmpty(SessionId))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Constants.SESSION_ID_REQUIRED);

                Stripe.Checkout.Session session = _stripePaymentUtility.GetCheckoutSession(SessionId);
                logger.Info($"{Constants.GET_CHECKOUT_SESSION_DETAILS} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, session);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(SessionId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Set payment has successfull
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("PaymentSucess")]
        public HttpResponseMessage PaymentSucess([FromBody] string SessionId)
        {
            logger.Info($"{Constants.PAYMENT_SUCCESS} is called");
            try
            {
                if (string.IsNullOrEmpty(SessionId))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Constants.SESSION_ID_REQUIRED);

                Stripe.Checkout.Session session = _stripePaymentUtility.GetCheckoutSession(SessionId);
                StripeSessionResponse stripeResponse = JsonConvert.DeserializeObject<StripeSessionResponse>(session.RawJObject.ToString());
                Response response = _userService.UpdateSignUpDetailsPaymentSuccess(session.CustomerEmail, session.CustomerId, session.SubscriptionId, false );

                if (response != null && response.Status)
                {
                    try
                    {
                        if (session.Metadata != null)
                        {
                            logger.Info("SendEmail called");
                            _emailService.SendPayment(session.Metadata, stripeResponse.CustomerEmail.ToString(), Constants.PAYMENT_SUCCESSFULL, Constants.PAYMENT_SUCCESSFULL_SUBJECT, true);
                            logger.Info("SendEmail completed");
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"{ex.Message} {JsonConvert.SerializeObject(SessionId, Formatting.Indented)}", ex);
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
                    }
                }

                logger.Info($"{Constants.PAYMENT_SUCCESS} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(SessionId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Set payment has cancelled
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("PaymentCancel")]
        public HttpResponseMessage PaymentCancel([FromBody] string SessionId)
        {
            logger.Info($"{Constants.PAYMENT_CANCEL} is called");
            try
            {
                if (string.IsNullOrEmpty(SessionId))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Constants.SESSION_ID_REQUIRED);

                Stripe.Checkout.Session session = _stripePaymentUtility.GetCheckoutSession(SessionId);
                StripeSessionResponse stripeResponse = JsonConvert.DeserializeObject<StripeSessionResponse>(session.RawJObject.ToString());

                ///Delete signup details
                Response response = _userService.DeleteSignUpDetails(stripeResponse.CustomerEmail.ToString());

                if (response != null && response.Status)
                {
                    try
                    {
                        if (session.Metadata != null)
                        {
                            logger.Info("SendEmail called");
                            _emailService.SendPayment(session.Metadata, stripeResponse.CustomerEmail.ToString(), Constants.PAYMENT_CANCELLED_MESSAGE, Constants.PAYMENT_CANCELLED_SUBJECT, false);
                            logger.Info("SendEmail completed");
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"{ex.Message} {JsonConvert.SerializeObject(SessionId, Formatting.Indented)}", ex);
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
                    }
                }

                logger.Info($"{Constants.PAYMENT_CANCEL} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(SessionId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Save trial payment
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveTrialPayment")]
        public HttpResponseMessage SaveTrialPayment(EmailViewModel email)
        {
            try
            {
                logger.Info("SaveTrialPayment is called");
                if (string.IsNullOrEmpty(email.Email))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Constants.EMAIL_REQUIRED);
                Response response = _userService.UpdateSignUpDetailsPaymentSuccess(email.Email.ToString(), string.Empty, string.Empty, true);
                if (response.Status)
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data.Add("subscription", "Trial"); data.Add("FirstName", email.FirstName); data.Add("LastName", email.LastName);
                    logger.Info("SendEmail called");
                    _emailService.SendPayment(data, email.Email, Constants.PAYMENT_SUCCESSFULL, Constants.SUBSCRIPTION_COMPLETED_SUCCESSFULLY, true);
                    logger.Info("SendEmail completed");
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(email, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }
    }
}
