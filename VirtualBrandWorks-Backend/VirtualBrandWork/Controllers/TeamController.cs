﻿using Newtonsoft.Json;
using Repository.Model;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWork.Notification;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [RoutePrefix("api/team")]
    public class TeamController : ApiController
    {
        private readonly ITeamService _teamService;
        private readonly INotificationService _notificationService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TeamController));
        public TeamController(ITeamService teamService, INotificationService notificationService)
        {
            _teamService = teamService;
            _notificationService = notificationService;
        }

        /// <summary>
        /// api to get list of teams
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetTeams")]
        [JWTAuthenticationFilter]
        [CheckModelForNull]
        public HttpResponseMessage GetTeams(TeamListViewModel team)
        {
            logger.Info($"{Constants.GET_TEAMS} is called");
            try
            {
                team.UserId = IdentityHelper.GetUserId();
                List<TeamListViewModel> teams = _teamService.GetTeamList(team);
                logger.Info($"{Constants.GET_TEAMS} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response
                {
                    Data = teams,
                    Status = (teams == null || teams.Count == 0) ? false : true,
                    Message = (teams == null || teams.Count == 0) ? Constants.TEAMS_EMPTY : null
                });

            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(team, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [HttpPost]
        [Route("GetTeamDetail")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage GetTeamDetail(TeamDetailViewModel team)
        {
            logger.Info("GetTeamDetail is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() {  Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                TeamDetailViewModel teamDetail = _teamService.GetTeamDetail(team);
                logger.Info("GetTeamDetail is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response
                {
                    Data = teamDetail,
                    Status = teamDetail == null ? false : true
                });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(team, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [HttpGet]
        [Route("GetTrainers")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage GetTrainers()
        {
            logger.Info("GetTrainers is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                List<TrainerListViewModel> trainers = _teamService.GetTrainers(IdentityHelper.GetUserId());
                logger.Info("GetTrainers is complete");
                string message = "";
                bool isEmpty = (trainers == null || trainers.Count == 0);
                if(isEmpty)
                {
                    message = Constants.TRAINERS_EMPTY;
                }
                return Request.CreateResponse(HttpStatusCode.OK, new Response
                {
                    Data = trainers,
                    Status = !isEmpty,
                    Message = message
                });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(IdentityHelper.GetUserId(), Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [HttpPost]
        [Route("GetUsers")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage GetUsers(UserListRequestViewModel request)
        {
            logger.Info("GetUsers is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                request.UserId = IdentityHelper.GetUserId();
                List<UserListViewModel> users = _teamService.GetUsers(request);
                logger.Info("GetUsers is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response
                {
                    Data = users,
                    Status = (users == null || users.Count == 0) ? false : true,
                    Message = (users == null || users.Count == 0) ? Constants.USERS_EMPTY : null
                });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(request, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [HttpPost]
        [Route("CreateTeam")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage CreateTeam(CreateTeamViewModel team)
        {
            logger.Info("CreateTeam is called");
            if (!ModelState.IsValid || team.Users == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                team.UserId = IdentityHelper.GetUserId();
                if (_teamService.ValidateTeamHandleName(team.HandleName))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response() { Message = Entity.Constants.TEAMHANDLENAME_ALREADY_REGISTERED });
                }
                _teamService.CreateTeam(team);
                logger.Info("CreateTeam is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(team, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [HttpPost]
        [Route("UpdateTeam")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage UpdateTeam(TeamDetailViewModel team)
        {
            logger.Info("UpdateTeam is called");
            if (!ModelState.IsValid || team.Users == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                team.UserId = IdentityHelper.GetUserId();

                List<long> usersToBeNotified = _teamService.UpdateTeam(team);

                _notificationService.SendNotifications(usersToBeNotified, string.Format(Constants.TeamUserAdded, team.TeamName), Constants.TeamUserTitle);
                 
                logger.Info("UpdateTeam is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(team, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }
         

        [HttpDelete]
        [Route("DeleteTeam")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage DeleteTeam(long teamId)
        {
            logger.Info("UpdateTeam is called");
            if (teamId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                _teamService.DeleteTeam(teamId);
                logger.Info("DeleteTeam is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(teamId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }
    }
}
