﻿using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.Controllers
{
    //[JWTAuthenticationFilter]
    [RoutePrefix("api/role")]
    public class RoleController : ApiController
    {
        private readonly IRoleService _roleService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RoleController));
        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet]
        [Route("roles")]
        public HttpResponseMessage GetRoles()
        {
            logger.Info($"{Constants.GET_ALL_ROLES} is called");
            try
            {
                List<RoleViewModel> roles = _roleService.GetRoles();
                Response response = new Response { Data = roles, Status = true };
                logger.Info(string.Concat(Constants.GET_ALL_ROLES, " is complete"));
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
