﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/CompanyQuiz")]
    [AuthorizeUser(Role = "Admin,Trainer")]
    public class CompanyQuizController : ApiController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CompanyQuizController));
        private readonly ICompanyQuizService _companyQuizService;

        public CompanyQuizController(ICompanyQuizService companyQuizService)
        {
            _companyQuizService = companyQuizService;
        }

        /// <summary>
        /// Add question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("AddQuestion")]
        [CheckModelForNull]
        public HttpResponseMessage AddQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel)
        {
            logger.Info($"{Constants.ADD_COMPANY_QUIZ_QUESTION_API} is called");
            try
            {
                _companyQuizService.AddQuestion(questionAnswerViewModel, IdentityHelper.GetUserId());
                logger.Info($"{Constants.ADD_COMPANY_QUIZ_QUESTION_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(questionAnswerViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Add question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveQuiz")]
        public HttpResponseMessage SaveQuiz(CompanyQuizCreateViewModel companyQuiz)
        {
            logger.Info($"{Constants.SAVE_COMPANY_QUIZ} is called");
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
                }

                companyQuiz.UserId = IdentityHelper.GetUserId();
                long newQuizId = _companyQuizService.SaveQuiz(companyQuiz);
                logger.Info($"{Constants.SAVE_COMPANY_QUIZ} is completed");
                Response response = new Response();
                if(newQuizId == Constants.QUIZ_ALREADY_EXISTS)
                {
                    response.Status = false;
                    response.Message = Constants.QUIZ_ALREADY_ASSOCIATED;
                }
                else
                {
                    response.Status = true;
                    response.Data = newQuizId;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(companyQuiz, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Add question in quiz
        /// isMasterFolder is true only when getting details of masterContent's masterQuiz
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizDetails")]
        public HttpResponseMessage GetQuizDetails(long quizId, bool isMasterFolder, bool isTurnedOffExistingQuiz)
        {
            logger.Info("GetQuizDetails is called");
            try
            {
                if (quizId == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Constants.INVALID_ARGUMENT });
                }

                CompanyQuizViewModel companyQuizViewModel = _companyQuizService.GetQuizDetails(quizId, isMasterFolder, isTurnedOffExistingQuiz);
                logger.Info("GetQuizDetails is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response()
                {
                    Data = companyQuizViewModel,
                    Status = companyQuizViewModel == null ? false : true,
                    Message = companyQuizViewModel == null ? Constants.NO_RESULT_FOUND : null
                });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Update question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateQuestion")]
        [CheckModelForNull]
        public HttpResponseMessage UpdateQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel)
        {
            logger.Info($"{Constants.Update_COMPANY_QUIZ_QUESTION_API} is called");
            try
            {
                _companyQuizService.UpdateQuestion(questionAnswerViewModel, IdentityHelper.GetUserId());
                logger.Info($"{Constants.Update_COMPANY_QUIZ_QUESTION_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(questionAnswerViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Delete quiz
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteQuiz")]
        public HttpResponseMessage DeleteQuiz(long quizId)
        {
            logger.Info($"{Constants.DELETE_COMPANY_QUIZ} is called");
            try
            {
                if (quizId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Constants.QUIZ_ID_CANNOT_BE_EMPTY });

                _companyQuizService.DeleteQuiz(quizId);
                logger.Info($"{Constants.DELETE_COMPANY_QUIZ} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Delete question
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteQuestion")]
        public HttpResponseMessage DeleteQuestion(long questionId)
        {
            logger.Info($"{Constants.DELETE_COMPANY_QUIZ_QUESTION} is called");
            try
            {
                if (questionId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Constants.QUESTION_ID_CANNOT_BE_EMPTY });

                _companyQuizService.DeleteQuestion(questionId);
                logger.Info($"{Constants.DELETE_COMPANY_QUIZ_QUESTION} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(questionId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to get quiz to play
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="isMasterFolder"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizToPlay")]
        public HttpResponseMessage GetQuizToPlay(long quizId, bool isMasterContent, bool isTurnedOffExistingQuiz)
        {
            logger.Info("GetQuizToPlay is called");
            try
            {
                if (quizId == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Constants.INVALID_ARGUMENT });
                }

                CompanyQuizViewModel companyQuizViewModel = _companyQuizService.GetQuizToPlay(quizId, IdentityHelper.GetUserId(), isMasterContent, isTurnedOffExistingQuiz);
                logger.Info("GetQuizToPlay is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response()
                {
                    Data = companyQuizViewModel,
                    Status = companyQuizViewModel == null ? false : true,
                    Message = companyQuizViewModel == null ? Constants.NO_RESULT_FOUND : null
                });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [HttpPost]
        [Route("SubmitQuiz")]
        public HttpResponseMessage SubmitQuiz(QuestionAnswerAttemptViewModel quizResponse)
        {
            logger.Info("SubmitQuiz is called");
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
                }

                quizResponse.UserId = IdentityHelper.GetUserId();
                PassFailViewModel passFailViewModel = _companyQuizService.SubmitQuiz(quizResponse);
                logger.Info("SubmitQuiz is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response()
                {
                    Data = passFailViewModel,
                    Status = passFailViewModel == null ? false : true,
                    Message = passFailViewModel == null ? Constants.NO_RESULT_FOUND : null
                    //Status = true
                });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizResponse, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }
    }
}
