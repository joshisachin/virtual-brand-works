﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/MyPlaylist")]
    public class MyPlaylistController : ApiController
    {
        private readonly IMyPlaylistService _myPlaylistService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MyPlaylistController));
        public MyPlaylistController(IMyPlaylistService myPlaylistService)
        {
            _myPlaylistService = myPlaylistService;
        }

        [HttpGet]
        [Route("GetMyCreatedPlaylist")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetMyCreatedPlaylist(bool isLaunched)
        {
            logger.Info("GetMyCreatedPlaylist called");
            try
            {
                List<PlaylistListViewModel> playlists = _myPlaylistService.GetMyCreatedPlaylist(IdentityHelper.GetUserId(), isLaunched, Helper.GetClientOffset());
                logger.Info("GetMyCreatedPlaylist completed");
                Response response = new Response
                {
                    Data = playlists,
                    Status = playlists.Count() == 0 ? false : true,
                    Message = playlists.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Get My Playlists
        /// </summary>
        /// <param name="isLaunched"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyPlaylist")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetMyPlaylist(bool isCompleted)
        {
            logger.Info("GetMyPlaylist called");
            try
            {
                List<PlaylistListViewModel> playlists = _myPlaylistService.GetMyPlaylist(IdentityHelper.GetUserId(), isCompleted, Helper.GetClientOffset());
                logger.Info("GetMyPlaylist completed");
                Response response = new Response
                {
                    Data = playlists,
                    Status = playlists.Count() == 0 ? false : true,
                    Message = playlists.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetMyPlaylistDetail")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetMyPlaylistDetail(long playlistId, long userId, bool isSelfCreated)
        {
            logger.Info("GetMyPlaylistDetail called");
            if (playlistId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                if(userId == 0)
                {
                    userId = IdentityHelper.GetUserId();
                }
                MyPlaylistDetailViewModel playlistDetail = _myPlaylistService.GetMyPlaylistDetail(playlistId, userId, isSelfCreated, Helper.GetClientOffset());
                logger.Info("GetMyPlaylistDetail completed");
                Response response = new Response
                {
                    Data = playlistDetail,
                    Status = playlistDetail == null ? false : true,
                    Message = playlistDetail == null ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("ViewResultDetailAndFolders")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage ViewResultDetailAndFolders(long playlistId, long userId)
        {
            logger.Info("ViewResultDetailAndFolders is called");
            if (playlistId == 0 || userId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                ViewResultDetailAndFoldersViewModel resultDetailAndFolders = _myPlaylistService.ViewResultDetailAndFolders(playlistId, userId);
                Response response = new Response
                {
                    Data = resultDetailAndFolders,
                    Status = resultDetailAndFolders == null ? false : true,
                    Message = resultDetailAndFolders == null ? Constants.NO_RESULT_FOUND : null
                };
                logger.Info("ViewResultDetailAndFolders is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(IdentityHelper.GetUserId(), Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("ViewResultContentList")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage ViewResultContentList(long playListFolderId, long userId)
        {
            logger.Info("ViewResultContentList called");
            if (playListFolderId == 0 || userId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<PlaylistContentResponseViewModel> playlistContentsResult = _myPlaylistService.ViewResultContentList(playListFolderId, userId);
                logger.Info("ViewResultContentList completed");
                Response response = new Response
                {
                    Data = playlistContentsResult,
                    Status = playlistContentsResult == null ? false : true,
                    Message = playlistContentsResult == null ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        ///Note -  isMasterQuiz is true when the Folder is a master folder and isTurnedOffExistingQuiz is also false 
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="playlistId"></param>
        /// <param name="userId"></param>
        /// <param name="isMasterQuiz"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizResult")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetQuizResult(long quizId, long playlistId, long userId, bool isMasterQuiz)
        {
            logger.Info("GetQuizResult is called");
            try
            {
                if (quizId == 0 || playlistId == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Constants.INVALID_ARGUMENT });
                }

                if(userId == 0)
                {
                    userId = IdentityHelper.GetUserId();
                }
                CompanyQuizViewModel companyQuizViewModel = _myPlaylistService.GetQuizResult(quizId, playlistId, userId, isMasterQuiz);
                logger.Info("GetQuizResult is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response()
                {
                    Data = companyQuizViewModel,
                    Status = companyQuizViewModel != null,
                    Message = companyQuizViewModel == null ? Constants.NO_RESULT_FOUND : null
                });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [HttpGet]
        [Route("GetMyPlaylistFolderDetail")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetMyPlaylistFolderDetail(long playListFolderId, long playlistContentMasterId)
        {
            logger.Info("GetMyPlaylistFolderDetail called");
            if (playListFolderId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                MyPlaylistFolderDetailViewModel playlistFolderDetail = _myPlaylistService.GetMyPlaylistFolderDetail(playListFolderId, playlistContentMasterId, IdentityHelper.GetUserId(), Helper.GetClientIp(), Helper.GetClientOffset());
                logger.Info("GetMyPlaylistFolderDetail completed");
                Response response = new Response
                {
                    Data = playlistFolderDetail,
                    Status = playlistFolderDetail != null,
                    Message = playlistFolderDetail == null ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetMyPlaylistFolderContentList")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetMyPlaylistFolderContentList(long playListFolderId, long playlistContentId)
        {
            logger.Info("GetMyPlaylistFolderContentList called");
            if (playListFolderId == 0 || playlistContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<PlaylistContentResponseViewModel> playlistFolderDetail = _myPlaylistService.GetMyPlaylistFolderContentList(playListFolderId, playlistContentId);
                logger.Info("GetMyPlaylistFolderContentList completed");
                Response response = new Response
                {
                    Data = playlistFolderDetail,
                    Status = playlistFolderDetail == null ? false : true,
                    Message = playlistFolderDetail == null ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetPlaylistUsers")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPlaylistUsers(long playListId)
        {
            logger.Info("GetPlaylistUsers called");
            if (playListId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<PlaylistUsersViewModel> playlistUsers = _myPlaylistService.GetPlaylistUsers(playListId, IdentityHelper.GetUserId());
                logger.Info("GetPlaylistUsers completed");
                Response response = new Response
                {
                    Data = playlistUsers,
                    Status = playlistUsers.Count == 0 ? false : true,
                    Message = playlistUsers.Count == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("AssociatedUsers")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage AssociatedUsers(long playListId, long teamId, bool isTeam)
        {
            logger.Info("GetPlaylistUsers called");
            if (playListId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                PlaylistAssociatedUserViewModel playlistUsers = _myPlaylistService.AssociatedUsers(playListId, teamId, isTeam);
                logger.Info("AssociatedUsers completed");
                Response response = new Response
                {
                    Data = playlistUsers,
                    Status = playlistUsers != null,
                    Message = playlistUsers == null ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetPlaylistAssociatedTeams")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPlaylistAssociatedTeams(long playListId)
        {
            logger.Info("GetPlaylistAssociatedTeams called");
            if (playListId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<AssociatedTeamListViewModel> playlistTeams = _myPlaylistService.GetPlaylistAssociatedTeams(playListId);
                logger.Info("GetPlaylistAssociatedTeams completed");
                Response response = new Response
                {
                    Data = playlistTeams,
                    Status = playlistTeams.Count > 0,
                    Message = playlistTeams.Count == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetMyDashboardPlaylist")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetMyDashboardPlaylist()
        {
            logger.Info("GetMyDashboardPlaylist called");

            try
            {
                List<PlaylistListViewModel> dashboardPlaylist = _myPlaylistService.GetMyDashboardPlaylist(IdentityHelper.GetUserId(), Helper.GetClientOffset());
                logger.Info("GetMyDashboardPlaylist completed");
                Response response = new Response
                {
                    Data = dashboardPlaylist,
                    Status = dashboardPlaylist.Count > 0,
                    Message = dashboardPlaylist.Count == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("DeletePlaylist")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage DeletePlaylist(PlaylistViewModel playlist)
        {
            logger.Info("DeletePlaylist is called");
            if (playlist.PlayListId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                playlist.UserId = IdentityHelper.GetUserId();
                _myPlaylistService.DeletePlaylist(playlist);
                logger.Info("DeletePlaylist is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Message = Constants.PLAYLIST_DELETED_SUCCESSFULLY});
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// api to clone an existing playlist, so that user may be able to create a new playlist by using the same contents or with some changes
        /// </summary>
        /// <param name="playlist"></param>
        /// <returns></returns>
        [Route("ClonePlaylist")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage ClonePlaylist(PlaylistViewModel playlist)
        {
            logger.Info("ClonePlaylist is called");
            if (playlist.PlayListId == 0) 
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                playlist.UserId = IdentityHelper.GetUserId();
                long newPlaylistId = _myPlaylistService.CloneMyCreatedPlaylist(playlist);
                logger.Info("ClonePlaylist is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Data = newPlaylistId });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}