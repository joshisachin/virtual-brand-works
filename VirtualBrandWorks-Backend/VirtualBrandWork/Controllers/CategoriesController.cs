﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;
using Service.Interfaces;
using System.Collections.Generic;
using VirtualBrandWork.Entity;
using Newtonsoft.Json;
using VirtualBrandWork.Filters;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [RoutePrefix("api/categories")]
    public class CategoriesController : ApiController
    {
        private readonly ICategoriesService _categoriesService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CategoriesController));

        public CategoriesController(ICategoriesService categoriesService)
        {
            _categoriesService = categoriesService;
        }

        // GET: api/Categories
        /// <summary>
        /// get all active categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("get_all_categories")]
        public HttpResponseMessage GetAllCategories()
        {
            logger.Info($"{Constants.GET_ALL_CATEGORIES_API}  is called");

            try
            {
                List<CategoryViewModel> categoryViewModel = _categoriesService.GetAllCategories();
                Response response = new Response { Data = categoryViewModel, Status = true };
                logger.Info($"{Constants.GET_ALL_CATEGORIES_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        /// <summary>
        /// create category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [Route("create_category")]
        public HttpResponseMessage Create(CategoryViewModel category)
        {
            logger.Info($"{Constants.CATEGORIES_CREATE_API} is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.OK, new Response() {Status = false, Message = Helper.GetErrorMessages(ModelState) });

            Response response = new Response();
            try
            {
                bool isExists = _categoriesService.IsCategoryExists(category);
                if (!isExists)
                {
                    _categoriesService.Create(category);
                    response.Status = true;
                }
                else
                    response.Message = "Industry already exists with same name";

                logger.Info($"{Constants.CATEGORIES_CREATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        /// <summary>
        /// update category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [Route("update_category")]
        public HttpResponseMessage Update(CategoryViewModel category)
        {
            logger.Info($"{Constants.CATEGORIES_UPDATE_API} is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                _categoriesService.Update(category);
                Response response = new Response { Status = true };
                logger.Info($"{Constants.CATEGORIES_UPDATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ModelState);
            }
        }
        /// <summary>
        /// get details of the one cateogry
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [Route("details_category")]
        public HttpResponseMessage Details(CategoryViewModel category)
        {
            logger.Info($"{Constants.FOLDER_DETAILS_API} is called");

            if (category.CategoryId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                CategoryViewModel categoryViewModel = _categoriesService.GetCategory(category.CategoryId);
                Response response = new Response { Data = categoryViewModel, Status = true };
                logger.Info($"{Constants.FOLDER_DETAILS_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        /// <summary>
        /// delete category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpDelete]
        [Route("delete_category")]
        public HttpResponseMessage Delete(CategoryViewModel category)
        {
            logger.Info($"{Constants.CATEGORIES_DELETE_API} is called");

            if (category.CategoryId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                Response response = _categoriesService.Delete(category.CategoryId);
                logger.Info($"{Constants.CATEGORIES_DELETE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("IsAssociatedCompany")]
        public HttpResponseMessage IsAssociatedCompany(CategoryViewModel category)
        {
            logger.Info($"{Constants.COMPANY_ASSOCIATED_TO_INDUSTRY} is called");

            if (category.CategoryId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                bool isAssociatedCompany = _categoriesService.IsAssociatedCompany(category.CategoryId);
                logger.Info($"{Constants.CATEGORIES_DELETE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = isAssociatedCompany});
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(category, Formatting.Indented)}");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}