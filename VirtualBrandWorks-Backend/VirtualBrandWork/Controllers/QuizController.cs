﻿using System.Web.Http;
using Repository;
using Service;
using System.Net.Http;
using System.Net;
using System;
using VirtualBrandWork.Common;
using System.Collections.Generic;
using Repository.Repository;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using Service.Interfaces;
using VirtualBrandWork.Filters;
using Newtonsoft.Json;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/quiz")]
    public class QuizController : ApiController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(QuizController));
        private readonly IQuizService _quizService;

        public QuizController(IQuizService quizService)
        {
            _quizService = quizService;
        }

        /// <summary>
        /// Add question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("AddQuestion")]
        public HttpResponseMessage AddQuestion(QuestionAnswerViewModel questionAnswerViewModel)
        {
            logger.Info($"{Constants.ADD_QUESTION_API} is called");
            try
            {
                Response response = _quizService.AddQuestion(questionAnswerViewModel);
                logger.Info($"{Constants.ADD_QUESTION_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(questionAnswerViewModel, Formatting.Indented)}",ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Add question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveQuiz")]
        public HttpResponseMessage SaveQuiz(QuizViewModel quizViewModel)
        {
            logger.Info($"{Constants.SAVE_QUIZ} is called");
            try
            {
                if (string.IsNullOrEmpty(quizViewModel.QuizName))
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new HttpError(Constants.QUIZ_NAME_CANNOT_BE_EMPTY));

                Response response = _quizService.SaveQuiz(quizViewModel);
                logger.Info($"{Constants.SAVE_QUIZ} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizViewModel, Formatting.Indented)}",ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Add question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetQuizDetails")]
        public HttpResponseMessage GetQuizDetails(QuizViewModel quizViewModel)
        {
            logger.Info($"{Constants.GET_QUIZ_DETAILS} is called");
            try
            {
                if (quizViewModel == null || quizViewModel.QuizId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new HttpError(Constants.QUIZ_ID_CANNOT_BE_EMPTY));

                QuizViewModel response = _quizService.GetQuizDetails(quizViewModel.QuizId);
                logger.Info($"{Constants.GET_QUIZ_DETAILS} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = response, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizViewModel, Formatting.Indented)}",ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Add question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetQuestionDetails")]
        public HttpResponseMessage GetQuestionDetails(QuestionViewModel questionViewModel)
        {
            logger.Info($"{Constants.Get_QUESTION_DETAILS_API} is called");
            try
            {
                if (questionViewModel == null || questionViewModel.QuestionId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new HttpError(Constants.QUESTION_ID_CANNOT_BE_EMPTY));

                QuestionAnswerViewModel response = _quizService.GetQuestionDetails(questionViewModel.QuestionId);
                logger.Info($"{Constants.Get_QUESTION_DETAILS_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = response, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(questionViewModel, Formatting.Indented)}",ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Update question in quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateQuestion")]
        public HttpResponseMessage UpdateQuestion(QuestionAnswerViewModel questionAnswerViewModel)
        {
            logger.Info($"{Constants.Update_QUESTION_API} is called");
            try
            {
                if (questionAnswerViewModel == null || questionAnswerViewModel.Question == null || questionAnswerViewModel.Question.QuestionId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new HttpError(Constants.QUESTION_ID_CANNOT_BE_EMPTY));

                Response response = _quizService.UpdateQuestion(questionAnswerViewModel);
                logger.Info($"{Constants.Update_QUESTION_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(questionAnswerViewModel, Formatting.Indented)}",ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Delete quiz
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteQuiz")]
        public HttpResponseMessage DeleteQuiz(QuizViewModel quizViewModel)
        {
            logger.Info($"{Constants.DELET_QUIZ} is called");
            try
            {
                if (quizViewModel == null || quizViewModel.QuizId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new HttpError(Constants.QUIZ_ID_CANNOT_BE_EMPTY));

                Response response = _quizService.DeleteQuiz(quizViewModel.QuizId);
                logger.Info($"{Constants.DELET_QUIZ} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(quizViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }

        /// <summary>
        /// Delete question
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteQuestion")]
        public HttpResponseMessage DeleteQuestion(QuestionViewModel questionViewModel)
        {
            logger.Info($"{Constants.DELETE_QUESTION} is called");
            try
            {
                if (questionViewModel == null || questionViewModel.QuestionId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new HttpError(Constants.QUESTION_ID_CANNOT_BE_EMPTY));

                Response response = _quizService.DeleteQuestion(questionViewModel.QuestionId);
                logger.Info($"{Constants.DELETE_QUESTION} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(questionViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(Constants.INTERNAL_SERVER_ERROR));
            }
        }
    }
}
