﻿using Newtonsoft.Json;
using Service.Interfaces;
using Stripe;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using System.Linq;
using VirtualBrandWork.Notification.Interfaces;

namespace VirtualBrandWork.Controllers
{
    // [JWTAuthenticationFilter]
    [RoutePrefix("api/subscription")]
    public class SubscriptionController : ApiController
    {
        private readonly ISubscriptionService _subscriptionService;
        private readonly IStripePaymentUtility _stripePaymentUtility;
        private readonly IEmailService _emailService;
        private readonly INotificationService _notificationService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SubscriptionController));
        public SubscriptionController(ISubscriptionService subscriptionService, IStripePaymentUtility stripePaymentUtility, IEmailService emailService, INotificationService notificationService)
        {
            _subscriptionService = subscriptionService;
            _stripePaymentUtility = stripePaymentUtility;
            _emailService = emailService;
            _notificationService = notificationService;
        }

        // GET api/Subscription/Index
        [HttpGet]
        [Route("index")]
        [AllowAnonymous]
        public HttpResponseMessage Index()
        {
            logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_API} is called");
            try
            {
                IEnumerable<SubscriptionViewModel> subscriptions = _subscriptionService.GetAllSubscription();
                Response response = new Response { Data = subscriptions, Status = true };
                logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_API} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // POST api/Subscription/Create
        [HttpPost]
        [Route("create")]
        public HttpResponseMessage Create(SubscriptionViewModel subscription)
        {
            logger.Info($"{Constants.SUBSCRIPTION_CREATE_API} is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                _subscriptionService.Create(subscription);
                Response response = new Response() { Status = true };
                logger.Info($"{Constants.SUBSCRIPTION_CREATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(subscription, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // POST api/Subscription/GetSubscription
        [HttpPost]
        [Route("get_subscription")]
        [AllowAnonymous]
        public HttpResponseMessage GetSubscription(SubscriptionViewModel subscription)
        {
            logger.Info($"{Constants.SUBSCRIPTION_DETAILS_API} is called");

            if (subscription.SubscriptionId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                SubscriptionViewModel subscriptionViewModel = _subscriptionService.GetSubscriptionById(subscription);
                Response response = new Response() { Data = subscriptionViewModel, Status = true };
                logger.Info($"{Constants.SUBSCRIPTION_DETAILS_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(subscription, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // POST api/Subscription/Update
        [HttpPost]
        [Route("update")]
        public HttpResponseMessage Update(SubscriptionViewModel model)
        {
            logger.Info($"{Constants.SUBSCRIPTION_UPDATE_API} is called");

            if (!ModelState.IsValid)
                Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                UserMyPlanNotiFicationViewModel updatedInfo = _subscriptionService.Update(model);
                if(updatedInfo.IsPlanNameChanged || updatedInfo.IsPlanDescriptionChanged || updatedInfo.IsNumberOfUsersChanged || updatedInfo.IsMonthlyCostChanged)
                {
                    if(updatedInfo.UsersForMonthlySubscription.Count > 0)
                    {
                        // mail to monthly users
                        _emailService.SendSubscriptionDetailsUpdatedNotificationEmail(updatedInfo, true);
                        var usersToBeNotified = updatedInfo.UsersForMonthlySubscription.Select(u => u.UserId).ToList();
                        _notificationService.SendNotifications(usersToBeNotified, string.Format(Constants.SubscriptionInfoChanged), Constants.SubscriptionChangedTitle);
                    }
                }
                if (updatedInfo.IsPlanNameChanged || updatedInfo.IsPlanDescriptionChanged || updatedInfo.IsNumberOfUsersChanged || updatedInfo.IsAnnualCostChanged)
                {
                    if(updatedInfo.UsersForYearlySubscription.Count > 0)
                    {
                        // mail to yearly users
                        _emailService.SendSubscriptionDetailsUpdatedNotificationEmail(updatedInfo, false);
                        var usersToBeNotified = updatedInfo.UsersForYearlySubscription.Select(u => u.UserId).ToList();
                        _notificationService.SendNotifications(usersToBeNotified, string.Format(Constants.SubscriptionInfoChanged), Constants.SubscriptionChangedTitle);
                    }
                }
                Response response = new Response() { Status = true };
                logger.Info($"{Constants.SUBSCRIPTION_UPDATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // POST api/Subscription/Delete
        [HttpDelete]
        [Route("delete")]
        public HttpResponseMessage Delete(SubscriptionViewModel subscription)
        {
            logger.Info($"{Constants.SUBSCRIPTION_DELETE_API} is called");

            if (!ModelState.IsValid)
                Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                _subscriptionService.Delete(subscription);
                Response response = new Response() { Status = true };
                logger.Info($"{Constants.SUBSCRIPTION_DELETE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(subscription, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("GetSubscriptionListForAdmin")]
        [AllowAnonymous]
        public HttpResponseMessage GetSubscriptionListForAdmin(bool noTrial)
        {
            logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_FOR_ADMIN_API} is called");
            try
            {
                IEnumerable<SubscriptionViewModel> subscriptions = _subscriptionService.GetSubscriptionListForAdmin(noTrial);
                Response response = new Response { Data = subscriptions, Status = true };
                logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_FOR_ADMIN_API} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [JWTAuthenticationFilter]
        [Route("get_subscription_details_for_company")]
        [HttpPost]
        [AuthorizeUser(Role = "SuperAdmin")]
        public HttpResponseMessage GetSubscriptionDetailsForCompany(CompanyViewModel company)
        {
            logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_FOR_ADMIN_API} is called");

            if (company.CompanyId != 0)
                Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                CompanySubscriptionViewModel companySubscriptions = _subscriptionService.GetSubscriptionDetailsForCompany(company.CompanyId);
                Response response = new Response { Data = companySubscriptions, Status = true };
                logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_FOR_ADMIN_API} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
