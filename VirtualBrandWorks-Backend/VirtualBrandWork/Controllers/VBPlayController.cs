﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [RoutePrefix("api/VBPlay")]
    public class VBPlayController : ApiController
    {
        private readonly IVBPlayService _vbPlayService;
        private readonly INotificationService _notificationService;
        private readonly IUserTagService _userTagService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(VBPlayController));
        public VBPlayController(IVBPlayService vbPlayService, INotificationService notificationService, IUserTagService userTagService)
        {
            _vbPlayService = vbPlayService;
            _notificationService = notificationService;
            _userTagService = userTagService;
        }

        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("GetAllVBPlayContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetAllVBPlayContest(int contestType)
        {
            logger.Info("GetAllVBPlayContest is called");

            try
            {
                List<VBPlayViewModel> contests = _vbPlayService.GetVBPlayContest(IdentityHelper.GetUserId(), (Entity.Constants.ContestType)contestType, Helper.GetClientOffset());
                foreach(var contest in contests)
                {
                    if(contest.UserIdsToNotify != null && contest.UserIdsToNotify.Count != 0)
                    {
                        _notificationService.SendNotifications(contest.UserIdsToNotify, string.Format(Constants.WinnerMessage, contest.ContestId ,contest.Name), Constants.WinnerTitle);
                    }
                    contest.UserIdsToNotify = null;
                }
                Response response = new Response
                {
                    Status = (contests != null && contests.Count() > 0) ? true : false,
                    Data = contests,
                    Message = (contests != null && contests.Count() > 0) ? string.Empty : Constants.NO_TOPIC_FOUND
                };
                logger.Info("GetAllVBPlayContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestType, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("CreateContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        [CheckModelForNull]
        public HttpResponseMessage CreateContest(VBPlayCreateViewModel vbPlayContest)
        {
            logger.Info("CreateContest is called");

            try
            {
                VBPlayViewModel contest = _vbPlayService.CreateContest(vbPlayContest, IdentityHelper.GetUserId(), Helper.GetClientOffset());

                List<long?> userIds = _userTagService.GetUsersFromTags(vbPlayContest.TagIds, IdentityHelper.GetUserId());

                _notificationService.SendNotifications(userIds.Where(x => x != null).Cast<long>().ToList(), string.Format(Constants.TagMessage, contest.ContestId, contest.Name), Constants.TagTitle);

                Response response = new Response
                {
                    Status = (contest != null) ? true : false,
                    Data = contest,
                    Message = (contest != null) ? string.Empty : Constants.NO_TOPIC_FOUND
                };
                logger.Info("CreateContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(vbPlayContest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("JoinContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        [CheckModelForNull]
        public HttpResponseMessage JoinContest(VBPlayViewModel vbPlayViewModel)
        {
            logger.Info("JoinContest is called");

            try
            {
                long contestEntryId = _vbPlayService.JoinContest(vbPlayViewModel, IdentityHelper.GetUserId());
                logger.Info("JoinContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Data = contestEntryId });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(vbPlayViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("GetContest")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage GetContest(long contestId)
        {
            logger.Info("GetContest is called");

            try
            {
                VBPlayViewModel vbPlayViewModel = _vbPlayService.GetContest(contestId);
                logger.Info("GetContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true, Data = vbPlayViewModel });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("ViewContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage ViewContest(long contestId, string search)
        {
            logger.Info("ViewContest is called");

            try
            {
                ContestChallengeAndEntriesViewModel contest = _vbPlayService.ViewContest(IdentityHelper.GetUserId(), contestId, search, Helper.GetClientOffset());
                Response response = new Response
                {
                    Status = (contest != null) ? true : false,
                    Data = contest,
                    Message = (contest != null) ? string.Empty : Constants.NO_TOPIC_FOUND
                };
                logger.Info("ViewContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }


        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("GetContestConfirmation")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetContestConfirmation(long contestId)
        {
            logger.Info("GetContestConfirmation is called");

            try
            {
                VBPlayViewModel contest = _vbPlayService.GetContestConfirmation(contestId);
                Response response = new Response
                {
                    Status = (contest != null) ? true : false,
                    Data = contest,
                    Message = (contest != null) ? string.Empty : Constants.NO_TOPIC_FOUND
                };
                logger.Info("ViewContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }


        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("GetContestEntryConfirmation")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetContestEntryConfirmation(long contestEntryId)
        {
            logger.Info("GetChallengeConfirmation is called");

            try
            {
                ChallengeEntryViewModel contestEntry = _vbPlayService.GetContestEntryConfirmation(contestEntryId);
                Response response = new Response
                {
                    Status = (contestEntry != null) ? true : false,
                    Data = contestEntry,
                    Message = (contestEntry != null) ? string.Empty : Constants.NO_RESULT_FOUND
                };
                logger.Info("GetChallengeConfirmation is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestEntryId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to broadcast a contest
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>'
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("BroadcastContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage BroadcastContest(ContestBroadcastRequestViewModel contest)
        {
            logger.Info("BroadcastContest is called");
            if (contest.ContestId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                _vbPlayService.BroadcastContest(contest);
                logger.Info("BroadcastContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to get list of broadcasted contents
        /// </summary>
        /// <param name="searchName"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("GetBroadcastedContent")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetBroadcastedContent(string search)
        {
            logger.Info("GetBroadcastedContent is called");

            try
            {
                List<BroadcastedContentViewModel> broadcastedContents = _vbPlayService.GetBroadcastedContent(search, IdentityHelper.GetUserId());
                var filteredBroadcastedContents = FilterExpiredContent(broadcastedContents);
                //_notificationService.SendNotifications(new List<long> { 158 }, "<button type=button class='btn btn - primary'  (click)='clickMethod('name')'>Delete me</button>", "Test");
                Response response = new Response
                {
                    Status = (filteredBroadcastedContents.Count() > 0) ? true : false,
                    Data = filteredBroadcastedContents,
                    Message = (filteredBroadcastedContents.Count > 0) ? string.Empty : Constants.NO_RESULT_FOUND
                };
                logger.Info("GetBroadcastedContent is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(search, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }


        /// <summary>
        /// api to like or unlike a contest
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("LikeContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage LikeContest(VBPlayContentCommonViewModel contest)
        {
            logger.Info("LikeContest is called");
            if (contest.ContestContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                long ownerUserId = _vbPlayService.LikeContest(contest, IdentityHelper.GetUserId());
                if (!contest.IsContestEntry)
                {
                    VBPlayViewModel objVBPlayviewmodel = _vbPlayService.GetContest(contest.ContestContentId);
                    if (objVBPlayviewmodel != null && ownerUserId != 0)
                    {
                        _notificationService.SendNotifications(new List<long>() { ownerUserId }, string.Format(Constants.LikeMessage, IdentityHelper.GetUserName(), objVBPlayviewmodel.Name), Constants.LikeTitle);
                    }
                }
                else
                {
                    ChallengeEntryViewModel objVBPlayviewmodel = _vbPlayService.GetContestEntry(contest.ContestContentId);
                    if (objVBPlayviewmodel != null && ownerUserId != 0)
                    {
                        _notificationService.SendNotifications(new List<long>() { ownerUserId }, string.Format(Constants.LikeMessage, IdentityHelper.GetUserName(), objVBPlayviewmodel.Name), Constants.LikeTitle);
                    }
                }

                logger.Info("LikeContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }


        /// <summary>
        /// api to increase views
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ContestContentViews")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage ContestContentViews(SharedContentViewModel content)
        {
            logger.Info("ContestContentViews is called");
            if (content.ContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                content.IPAddress = Helper.GetClientIp();
                _vbPlayService.ContestContentViews(content);
                logger.Info("ContestContentViews is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(content, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to submit a contest entry
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("SubmitContestEntry")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage SubmitContestEntry(ContestEntrySubmitViewModel contestEntry)
        {
            logger.Info("SubmitContestEntry is called");
            if (contestEntry.ContestEntryId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                VBPlayViewModel submittedEntry = _vbPlayService.SubmitContestEntry(contestEntry);
                logger.Info("SubmitContestEntry is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Data = submittedEntry });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestEntry, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to get a contest entry
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("GetContestEntry")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetContestEntry(long contestEntryId)
        {
            logger.Info("GetContestEntry is called");
            if (contestEntryId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                ChallengeEntryViewModel contestEntry = _vbPlayService.GetContestEntry(contestEntryId);
                logger.Info("GetContestEntry is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Data = contestEntry });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestEntryId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to update a contest entry
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("UpdateContestEntry")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage UpdateContestEntry(VBPlayCreateViewModel contestEntry)
        {
            logger.Info("UpdateContestEntry is called");
            if (contestEntry.ContestId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                ChallengeEntryViewModel updatedEntry = _vbPlayService.UpdateContestEntry(contestEntry);
                logger.Info("UpdateContestEntry is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Data = updatedEntry.ContestEntryId });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestEntry, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to get public shared content
        /// </summary>
        /// <param name="searchName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSharedContent")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetSharedContent(long contentId, Constants.SharedContentType contentType)
        {
            logger.Info("GetSharedContent is called");

            try
            {
                BroadcastedContentViewModel sharedContent;
                string authToken = string.Empty;
                var authRequest = Request.Headers.Authorization;
                if(authRequest != null)
                {
                    authToken = authRequest.Parameter;
                }
                var auth = new AuthenticationModule();
                System.IdentityModel.Tokens.Jwt.JwtSecurityToken userPayloadToken = auth.GenerateUserClaimFromJWT(authToken);
                if (userPayloadToken != null)
                {
                    var identity = auth.PopulateUserIdentity(userPayloadToken);
                    sharedContent = _vbPlayService.GetSharedContent(contentId, contentType, identity.UserId);
                }
                else
                {
                    sharedContent = _vbPlayService.GetSharedContent(contentId, contentType, 0);
                }
                Response response = new Response
                {
                    Status = (sharedContent != null) ? true : false,
                    Data = sharedContent,
                    Message = (sharedContent != null) ? string.Empty : Constants.PRIVATE_CONTENT
                };
                logger.Info("GetSharedContent is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contentId, Formatting.Indented)} {JsonConvert.SerializeObject(contentType, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }


        /// <summary>
        /// api to get info about earned points
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("GetEarnedPoints")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetEarnedPoints()
        {
            logger.Info("GetSharedContent is called");

            try
            {
                EarnedPointsViewModel earnedPoints = _vbPlayService.GetEarnedPoints(IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Status = (earnedPoints != null) ? true : false,
                    Data = earnedPoints,
                    Message = (earnedPoints != null) ? string.Empty : Constants.NO_RESULT_FOUND
                };
                logger.Info("GetEarnedPoints is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to delete contest
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("DeleteContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage DeleteContest(VBPlayViewModel contest)
        {
            logger.Info("DeleteContest is called");
            if (contest.ContestId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                bool isDeleted = _vbPlayService.DeleteContest(contest);
                Response response = new Response
                {
                    Status = isDeleted ? true : false,
                    Message = isDeleted ? Constants.CONTEST_DELETED_SUCCESSFULLY : Constants.CONTEST_CANNOT_BE_DELETED
                };
                logger.Info("DeleteContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("ExtendContestDate")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage ExtendContestDate(long contestId, long notificationId,string type)
        {
            logger.Info("ExtendContestDate is called");
            try
            {
                _vbPlayService.ExtendContentDate(contestId, notificationId,type);

                Response response = new Response
                {
                    Status = true,
                    Message = Constants.ContentDateExtended
                };
                logger.Info("ExtendContestDate is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }

        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("OptOutNotification")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage OptOutNotification(long notificationId)
        {
            logger.Info("OptOutNotification is called");
            try
            {
                _vbPlayService.OptOutNotification(notificationId);
                Response response = new Response
                {
                    Status = true,
                    Message = null
                };
                logger.Info("OptOutNotification is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }

        }

        /// <summary>
        /// update visibility level for a contest or entry
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>'
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("UpdateVbPlayContentVisibility")]
        public HttpResponseMessage UpdateVbPlayContentVisibility(ContestVisibilityViewModel contest)
        {
            logger.Info("UpdateVbPlayContentVisibility is called");
            if (contest.ContestId == 0 && contest.Visibility == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                contest.UserId = IdentityHelper.GetUserId();
                _vbPlayService.UpdateVbPlayContentVisibility(contest);
                logger.Info("UpdateVbPlayContentVisibility is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to submit a contest
        /// </summary>
        /// <param name="contest"></param>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("SubmitContest")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage SubmitContest(ContestEntrySubmitViewModel contest)
        {
            logger.Info("SubmitContest is called");
            if (contest.ContestEntryId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                VBPlayViewModel submittedEntry = _vbPlayService.SubmitContest(contest);
                logger.Info("SubmitContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Data = submittedEntry });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("UpdateContest")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage UpdateContest(VBPlayUpdateViewModel vbPlayContest)
        {
            logger.Info("UpdateContest is called");

            try
            {
                VBPlayViewModel contest = _vbPlayService.UpdateContest(vbPlayContest);

                List<long?> userIds = _userTagService.GetUsersFromTags(vbPlayContest.TagIds, IdentityHelper.GetUserId());

                _notificationService.SendNotifications(userIds.Where(x => x != null).Cast<long>().ToList(), string.Format(Constants.TagMessage, contest.ContestId, contest.Name), Constants.TagTitle);

                Response response = new Response
                {
                    Status = (contest != null) ? true : false,
                    Data = contest,
                    Message = (contest != null) ? string.Empty : Constants.NO_TOPIC_FOUND
                };
                logger.Info("UpdateContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(vbPlayContest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        [JWTAuthenticationFilter]
        [Route("ReportAbuse")]
        [HttpGet]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetAllReportAbuse()
        {
            logger.Info($"{nameof(GetAllReportAbuse)} is called");
            try
            {
                long userId = IdentityHelper.GetUserId();
                var reports = _vbPlayService.GetAllReportAbuse(userId, Helper.GetClientOffset());
                Response response = new Response
                {
                    Status = true,
                    Data = reports,
                    Message = $"Action {nameof(GetAllReportAbuse)} completed"
                };

                logger.Info($"{nameof(GetAllReportAbuse)} is complete");

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [JWTAuthenticationFilter]
        [Route("ReportAbuse")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage ReportAbuse(long contestId)
        {
            logger.Info($"{nameof(ReportAbuse)} is called");
            if (contestId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Constants.INVALID_ARGUMENT });
            }
            try
            {
                //Fetching Current User's ID a.k.a User who is reporting the content.
                long userId = IdentityHelper.GetUserId();
                if(!_vbPlayService.IsContestAlreadyReported(contestId, userId))
                {
                    _vbPlayService.ReportAbuse(contestId, userId);
                    var usersToNotify = _vbPlayService.GetUserIdsOfAdminAndTrainerOfACompany(userId);
                    _notificationService.SendNotifications(usersToNotify, Constants.ReportAbuseMessage, Constants.ReportAbuseTitle);
                }
                Response response = new Response { Status = true, Message = $"Action {nameof(ReportAbuse)} completed" };
                logger.Info($"{nameof(ReportAbuse)} is complete");

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contestId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [JWTAuthenticationFilter]
        [Route("ReportAbuse")]
        [HttpDelete]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage DeleteReportAbuse(long contestId)
        {
            logger.Info($"{nameof(DeleteReportAbuse)} is called");
            if (contestId <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Constants.INVALID_ARGUMENT });
            }
            try
            {
                _vbPlayService.DeleteReportAbuse(contestId);
                Response response = new Response { Status = true, Message = $"Action {nameof(DeleteReportAbuse)} completed" };
                logger.Info($"{nameof(DeleteReportAbuse)} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        private List<BroadcastedContentViewModel> FilterExpiredContent(List<BroadcastedContentViewModel> contents)
        {
            var expiredContents = new List<BroadcastedContentViewModel>();
            foreach (var content in contents)
            {
                if (DateTime.UtcNow > content.BroadcastingDate)
                {
                    expiredContents.Add(content);
                    if(!content.IsExtendDateNotified)
                    {
                        if (!content.IsOptOutForNotification)
                        {
                            var userToNotify = new List<long>();
                            userToNotify.Add(content.CreatedBy);
                            _notificationService.SendNotifications(userToNotify, string.Format(Constants.BroadcastMessage, content.ContentType, content.ContentId, content.Name), Constants.BroadcastTitle);
                            _vbPlayService.SetNotificationExtenstionFlag(content, true);
                        }

                    }
                }
            }

            foreach(var expiredContent in expiredContents)
            {
                contents.Remove(expiredContent);
            }

            return contents.OrderByDescending(x => x.BroadcastingDate).ToList();
        }
    }
}
