﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWork.Notification;
using VirtualBrandWork.Notification.Interfaces;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
  //  [JWTAuthenticationFilter]
    [RoutePrefix("api/userinformation")]
    public class UserInformationController : ApiController
    {
        private readonly IUserInformationService _userInformationService;
        private readonly IEmailService _emailService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UserInformationController));
        public UserInformationController(IUserInformationService userInformationService, IEmailService emailService)
        {
            _userInformationService = userInformationService;
            _emailService = emailService;
        }

        /// <summary>
        /// get all users list 
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //[Route("index")]
        //[AllowAnonymous]
        //public HttpResponseMessage Index()
        //{
        //    logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_API} is called");
        //    try
        //    {
        //        IEnumerable<ManageUserViewModel> users = _userInformationService.GetAllSubscription();
        //        Response response = new Response { Data = subscriptions, Status = true };
        //        logger.Info($"{Constants.GET_ALL_SUBSCRIPTION_API} is complete");
        //        return Request.CreateResponse(HttpStatusCode.OK, response);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.Message, ex);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError);
        //    }
        //}

        [HttpGet]
        [Route("countries")]
        public HttpResponseMessage GetCountries()
        {
            logger.Info($"{Constants.GET_ALL_COUNTRIES} is called");
            try
            {
                List<CountryViewModel> countries = null;
                countries = _userInformationService.GetCountries().OrderBy(x => x.Name).ToList();
                //filtering US and prepending it to the top of the list
                string countryNameToFilter = Constants.UnitedStatesFilter.ToLower();
                var filteredCountry = countries.FirstOrDefault(x => x.Name.ToLower().Equals(countryNameToFilter));
                countries.Remove(filteredCountry);
                countries.Insert(0, filteredCountry);

                Response response = new Response { Data = countries, Status = true };
                logger.Info($"{Constants.GET_ALL_COUNTRIES} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("states")]
        public HttpResponseMessage GetStates(StateViewModel state)
        {
            logger.Info($"{Constants.GET_ALL_STATES} is called");
            if (state.CountryID == 0 || state.CountryID == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }

            try
            {
                List<StateViewModel> states = null;
                states = _userInformationService.GetStates(Convert.ToInt32(state.CountryID));
                Response response = new Response { Data = states.OrderBy(x =>x.Name), Status = true };
                logger.Info($"{Constants.GET_ALL_STATES} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("cities")]
        public HttpResponseMessage GetCities(CityViewModel city)
        {
            logger.Info($"{Constants.GET_ALL_CITIES} is called");
            if (city.StateID == 0 || city.StateID == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<CityViewModel> cities = null;
                cities = _userInformationService.GetCities(Convert.ToInt32(city.StateID));
                Response response = new Response { Data = cities.OrderBy(x => x.Name) , Status = true };
                logger.Info($"{Constants.GET_ALL_CITIES} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetLocation")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage GetLocation(CityViewModel city)
        {
            logger.Info("GetLocation is called");
            if (String.IsNullOrEmpty(city.Name))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<CityViewModel> cities = _userInformationService.GetLocation(city.Name);
                Response response = new Response { Data = cities.OrderBy(x => x.Name), Status = true };
                logger.Info("GetLocation is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        [Route("WriteToUs")]
        public HttpResponseMessage WriteToUs(WriteToUsViewModel writeToUs)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                _emailService.SendWriteToUs(writeToUs.Email, writeToUs.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        [Route("GetCompanyUsers")]
        [JWTAuthenticationFilter]
        [CheckModelForNull]
        public HttpResponseMessage GetCompanyUsers(UserRequestViewModel users)
        {
            logger.Info($"{Constants.GET_COMPANY_USERS} is called");

            try
            {
                users.UserId = IdentityHelper.GetUserId();
                List<UserReponseViewModel> userReponseViewModel = _userInformationService.GetCompanyUsers(users);
                Response response = new Response { Data = userReponseViewModel, Status = true };
                logger.Info($"{Constants.GET_COMPANY_USERS} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]        
        [Route("GetUserProfile")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage GetUserProfile(UserProfileViewModel user)
        {
            logger.Info($"{Constants.GET_USER_PROFILE} is called");
            if (user.UserId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                UserProfileViewModel userProfile = _userInformationService.GetUserProfile(user);
                Response response = new Response { Data = userProfile, Status = true };
                logger.Info($"{Constants.GET_USER_PROFILE} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("AddProfileImage")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage AddProfileImage(UserRequestViewModel user)
        {
            logger.Info($"{Constants.ADD_PROFILE_IMAGE} is called");
            if (string.IsNullOrEmpty(user.ProfilePictue))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                user.UserId = IdentityHelper.GetUserId();
                _userInformationService.AddProfileImage(user);
                logger.Info($"{Constants.ADD_PROFILE_IMAGE} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = null, Status = true, Message = Constants.IMAGE_ADDED_SUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdateUserRole")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage UpdateUserRole(UserRequestViewModel user)
        {
            logger.Info($"{Constants.UPDATE_USER_ROLE} is called");
            if (user.RoleId == 0 || user.UserId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                _userInformationService.UpdateUserRole(user);
                logger.Info($"{Constants.UPDATE_USER_ROLE} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = null, Status = true, Message = Constants.USER_ROLE_UPDATED_SUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(user, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdateUserStatus")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage UpdateUserStatus(UserRequestViewModel user)
        {
            logger.Info($"{Constants.UPDATE_USER_STATUS} is called");
            if (user.UserId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                if (user.IsActive)
                {
                    CompanyViewModel company = new CompanyViewModel();
                    company.UserId = IdentityHelper.GetUserId();
                    bool limitNotExceeded = _userInformationService.CheckUserLimitForCompany(company);
                    if (!limitNotExceeded)
                    {
                        logger.Info($"{Constants.UPDATE_USER_STATUS} is complete");
                        return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = false, Message = Constants.USER_LIMIT_EXCEEDED_FOR_COMPANY });
                    }
                }
                bool isActive =_userInformationService.UpdateUserStatus(user);
                Response response = new Response { Status = true };
                if (isActive)
                {
                    response.Message = Constants.USER_ACTIVATED;
                }
                else
                {
                    response.Message = Constants.USER_DEACTIVATED;
                }
                logger.Info($"{Constants.UPDATE_USER_STATUS} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(user, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("CheckUserLimitForCompany")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage CheckUserLimitForCompany()
        {
            logger.Info($"{Constants.CHECK_USER_LIMIT_FOR_COMPANY} is called");

            Response response = new Response();
            try
            {
                CompanyViewModel company = new CompanyViewModel();
                company.UserId = IdentityHelper.GetUserId();

                bool limitNotExceeded = _userInformationService.CheckUserLimitForCompany(company);
                if (!limitNotExceeded)
                {
                    response.Message = Constants.USER_LIMIT_EXCEEDED_FOR_COMPANY;
                }
                response.Status = limitNotExceeded;
                logger.Info($"{Constants.CHECK_USER_LIMIT_FOR_COMPANY} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("AddUserInvite")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage AddUserInvite(UserInviteViewModel userInvite)
        {
            logger.Info($"{Constants.CHECK_USER_LIMIT_FOR_COMPANY} is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            Response response = new Response();
            try
            {
                userInvite.UserId = IdentityHelper.GetUserId();
                CompanyViewModel company = new CompanyViewModel();
                company.UserId = IdentityHelper.GetUserId();

                bool limitNotExceeded = _userInformationService.CheckUserLimitForCompany(company);
                if (!limitNotExceeded)
                {
                    response.Message = Constants.USER_LIMIT_EXCEEDED_FOR_COMPANY;
                    response.Status = limitNotExceeded;
                }
                else
                {
                    bool isEmailRegistered = _userInformationService.IsUserExist(userInvite.EmailId);
                    if (isEmailRegistered)
                    {
                        response.Status = false;
                        response.Message = "This email id is already registered with us!";
                    }
                    else
                    {
                        _userInformationService.AddUserInvite(userInvite);
                        string encodedEmail = HttpUtility.UrlEncode(EncodeDecodeString.Encrypt(userInvite.EmailId.Trim()));
                        string userName = $"{userInvite.FirstName} {userInvite.LastName}";
                        _emailService.SendUserInviteNotification(userInvite.EmailId, encodedEmail, userName);
                        response.Status = true;
                    }
                }
            
                logger.Info($"{Constants.CHECK_USER_LIMIT_FOR_COMPANY} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("GetUserInvite")]
        public HttpResponseMessage GetUserInvite(UserInviteViewModel user)
        {
            logger.Info("GetUserInvite called");

            if (String.IsNullOrEmpty(user.EncodedEmail))
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);
            Response response = new Response();
            try
            {
                user.EmailId = EncodeDecodeString.Decrypt(HttpUtility.UrlDecode(user.EncodedEmail));
                response = _userInformationService.CanUserRegister(user.EmailId);
                if (response.Status)
                {
                    UserInviteViewModel userInvite = _userInformationService.GetUserInvite(user.EmailId);
                    response = new Response { Data = userInvite, Status = true };
                }
                logger.Info("GetUserInvite completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdateNotificationSetting")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage UpdateNotificationSetting(UserRequestViewModel user)
        {
            logger.Info($"{Constants.UPDATE_NOTIFICATION_SETTING} is called");
            bool result;
            try
            {
                result = _userInformationService.UpdateNotificationSetting(user);
                logger.Info($"{Constants.UPDATE_NOTIFICATION_SETTING} is complete");
                if (result)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = null, Status = true, Message = Constants.NOTIFICATION_SETTING_UPDATED_SUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = null, Status = false, Message = Constants.NOTIFICATION_SETTING_UPDATED_UNSUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("UpdateUserProfile")]
        [JWTAuthenticationFilter]
        public HttpResponseMessage UpdateUserProfile(UserProfileViewModel user)
        {
            logger.Info($"{Constants.UPDATE_USER_PROFILE} is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                user.UserId = IdentityHelper.GetUserId();
                UserProfileViewModel response = _userInformationService.UpdateUserProfile(user);
                logger.Info($"{Constants.UPDATE_USER_PROFILE} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = response, Status = true, Message = Constants.USER_UPDATED_SUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(user, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// api to update about company from company profile tab
        /// </summary>
        /// <param name="companyInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateCompanyProfile")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage UpdateCompanyProfile(CompanyInfoViewModel companyInfo)
        {
            logger.Info("UpdateCompanyProfile is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                companyInfo.UserId = IdentityHelper.GetUserId();
                CompanyInfoViewModel response = _userInformationService.UpdateCompanyProfile(companyInfo);
                logger.Info("UpdateCompanyProfile is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = response, Status = true, Message = Constants.COMPANY_DESCRIPTION_UPDATED_SUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(companyInfo, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        [Route("GetUserProfileStatus")]
        [AuthorizeUser(Role ="Admin,Trainer")]
        public HttpResponseMessage GetUserProfileStatus(UserProfileViewModel user)
        {
            logger.Info($"{Constants.GET_USER_PROFILE_STATUS} is called");
            try
            {
                UserProfileViewModel response = _userInformationService.GetUserProfileStatus(user);
                logger.Info($"{Constants.GET_USER_PROFILE_STATUS} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = response, Status = true, Message = Constants.USER_UPDATED_SUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetUserCreatedPlaylistList")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetUserCreatedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylistRequest)
        {
            logger.Info("GetUserCreatedPlaylistList is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                List<PlaylistListViewModel> associatedPlaylist = _userInformationService.GetUserAssociatedPlaylistList(associatedPlaylistRequest);
                logger.Info("GetUserCreatedPlaylistList is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = associatedPlaylist, Status = associatedPlaylist == null? false : true});
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(associatedPlaylistRequest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetUserAssignedPlaylistList")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetUserAssignedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylistRequest)
        {
            logger.Info("GetUserAssignedPlaylistList is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                List<PlaylistListViewModel> associatedPlaylist = _userInformationService.GetUserAssignedPlaylistList(associatedPlaylistRequest);
                logger.Info("GetUserAssignedPlaylistList is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = associatedPlaylist, Status = associatedPlaylist == null ? false : true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(associatedPlaylistRequest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetUserAssociatedTeam")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetUserAssociatedTeam(long userId, string search)
        {
            logger.Info("GetUserAssociatedTeam is called");
            if (userId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<AssociatedTeamListViewModel> associatedTeams = _userInformationService.GetUserAssociatedTeam(userId, search);
                logger.Info("GetUserAssociatedTeam is complete");
                Response response = new Response
                {
                    Data = associatedTeams,
                    Status = associatedTeams.Count() == 0 ? false : true,
                    Message = associatedTeams.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetUserAssociatedUsers")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetUserAssociatedUsers(long userId, string search)
        {
            logger.Info("GetUserAssociatedUsers called");
            if (userId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<AssociatedUsersListViewModel> associatedUsers = _userInformationService.GetUserAssociatedUsers(userId, search);
                logger.Info("GetUserAssociatedUsers completed");
                Response response = new Response
                {
                    Data = associatedUsers,
                    Status = associatedUsers.Count() == 0 ? false : true,
                    Message = associatedUsers.Count() == 0 ? Constants.NO_RESULT_FOUND :null 
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("AddCompanyLogo")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage AddCompanyLogo(CompanyLogoRequestViewModel company)
        {
            logger.Info("AddCompanyLogo is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                company.UserId = IdentityHelper.GetUserId();
                _userInformationService.UpdateCompanyLogo(company);
                logger.Info("AddCompanyLogo is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = null, Status = true, Message = Constants.IMAGE_ADDED_SUCCESSFULLY }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetCompanyInfo")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage GetCompanyInfo()
        {
            logger.Info("GetCompanyInfo is called");

            try
            {
                CompanyInfoViewModel companyInfo = _userInformationService.GetCompanyInfo(IdentityHelper.GetUserId());
                Response response = new Response { Data = companyInfo, Status = true };
                logger.Info("GetCompanyInfo is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("DeleteProfileImage")]
        [HttpPost]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage DeleteProfileImage(PrifileDeleteRequestModel profile)
        {
            logger.Info("DeletePlaylist is called");
            try
            {
                if (profile != null)
                {
                    CompanyLogoRequestViewModel objCompanyLogoRequestViewModel = new CompanyLogoRequestViewModel
                    {
                        CompanyLogo = profile.ImageName,
                        RoleId = profile.RoleId,
                        UserId = IdentityHelper.GetUserId(),
                        CreatedBy = profile.CreatedBy,
                        CreatedDate = profile.CreatedDate,
                        ModifiedBy = profile.ModifiedBy,
                        ModifiedDate = profile.ModifiedDate
                    };

                    if (profile.IsCompanyLogo)
                    {
                        _userInformationService.DeleteCompanyLogo(objCompanyLogoRequestViewModel);
                    }
                    else
                    {
                        _userInformationService.DeleteProfileImage(objCompanyLogoRequestViewModel);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Message = Constants.INVALID_ARGUMENT });
                }
                logger.Info("DeleteProfile image is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Message = Constants.ProfileDeleted });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("Locations")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage Locations(LocationsViewModel objLocations)
        {
            logger.Info("Locations is called");
            try
            {
                if (objLocations.LocationId == 0 && _userInformationService.LocationExists(objLocations, IdentityHelper.GetUserId()))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = false, Message = Constants.LOCATION_EXISTS });
                }
                if (objLocations.IsAdmin)
                {
                    objLocations.AdminId = Convert.ToInt32(IdentityHelper.GetUserId());
                }
                else
                {
                    objLocations.TrainerId = Convert.ToInt32(IdentityHelper.GetUserId());
                }
                _userInformationService.SaveLocations(objLocations, IdentityHelper.GetUserId());
                Response response = new Response { Data = null, Status = true, Message = (objLocations.LocationId == 0) ? Constants.LOCATION_ADDED : Constants.LOCATION_UPDATED };
                logger.Info("Locations is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("DeleteLocation")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage DeleteLocation(int locationId)
        {
            logger.Info("Locations is called");
            try
            {
                _userInformationService.DeleteLocation(locationId);
                Response response = new Response { Data = null, Status = true };
                logger.Info("Locations is complete");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Message = Constants.LOCATION_DELETED });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetSignUpLocations")]
        public HttpResponseMessage GetSignUpLocations(LocationsSearch location)
        {
            logger.Info("GetLocations is called");
            try
            {
                if (location == null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = false, Message = Constants.INVALID_ARGUMENT });
                }

                List<LocationsViewModel> locationViewModel = _userInformationService.GetSignUpLocations(location);
                Response response = new Response { Data = locationViewModel, Status = true };
                logger.Info("GetLocations is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetLocations")]
        [JWTAuthenticationFilter]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetLocations(LocationsSearch location)
        {
            logger.Info("GetLocations is called");
            try
            {
                if (location == null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = false, Message = Constants.INVALID_ARGUMENT });
                }

                List<LocationsViewModel> locationViewModel = _userInformationService.GetLocations(location, IdentityHelper.GetUserId());
                Response response = new Response { Data = locationViewModel, Status = true };
                logger.Info("GetLocations is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
