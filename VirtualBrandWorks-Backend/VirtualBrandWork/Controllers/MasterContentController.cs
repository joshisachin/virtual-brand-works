﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/mastercontent")]
    public class MasterContentController : ApiController
    {
        private readonly IMasterContentService _masterContentService;
        private readonly IAWSS3FileStorage _awsS3FileStorage;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MasterContentController));

        public MasterContentController(IMasterContentService masterContentService, IAWSS3FileStorage awsS3FileStorage)
        {
            _masterContentService = masterContentService;
            _awsS3FileStorage = awsS3FileStorage;
        }

        /// <summary>
        /// Api to get the list of MasterContent
        /// </summary>
        /// <returns></returns>
        [Route("get_all_contents")]
        public HttpResponseMessage GetAllContents()
        {
            logger.Info($"{Constants.GET_ALL_CONTENT_API} is called");

            try
            {
                List<MasterContentViewModel> masterContents = _masterContentService.GetAllContents();
                Response response = new Response { Data = masterContents, Status = true };
                logger.Info($"{Constants.GET_ALL_CONTENT_API} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Api to create a MasterContent , accepts a MasterContent type object in param
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        [Route("create_content")]
        public HttpResponseMessage Create(MasterContentViewModel content)
        {
            logger.Info($"{Constants.MASTER_CONTENT_CREATE_API} is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                _masterContentService.Create(content);
                Response response = new Response() { Status = true };
                logger.Info($"{Constants.MASTER_CONTENT_CREATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(content, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Api to update a particular Master Content
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        [Route("update_content")]
        public HttpResponseMessage Update(MasterContentViewModel content)
        {
            logger.Info($"{Constants.MASTER_CONTENT_UPDATE_API} is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                _masterContentService.Update(content);
                Response response = new Response() { Status = true };
                logger.Info($"{Constants.MASTER_CONTENT_UPDATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(content, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Api to get details of any specififc Content
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        [Route("details_content")]
        public HttpResponseMessage Details(MasterContentViewModel content)
        {
            logger.Info($"{Constants.MASTER_CONTENT_DETAILS_API} is called");

            if (content.MasterContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                MasterContentViewModel masterContentViewModel = _masterContentService.GetContent(content.MasterContentId);
                Response response = new Response() { Data = masterContentViewModel, Status = true };
                logger.Info($"{Constants.MASTER_CONTENT_DETAILS_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(content, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete_content")]
        public HttpResponseMessage Delete(MasterContentViewModel content)
        {
            logger.Info($"{Constants.MASTER_CONTENT_DELETE_API} is called");

            if (content.MasterContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }

            try
            {
                _masterContentService.Delete(content.MasterContentId);
                Response response = new Response() { Status = true };
                logger.Info($"{Constants.MASTER_CONTENT_DELETE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(content, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Get master contents
        /// </summary>
        /// <param name="foderId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("get_contents_by_folder")]
        public HttpResponseMessage GetMasterContentsByFolderId(MasterFolderViewModel masterFolder)
        {
            logger.Info($"{Constants.GET_CONTENTS_BY_FOLDER_API} is called");

            //  if (!ModelState.IsValid)
            //return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                List<ContentViewModel> response = _masterContentService.GetMasterContentsByFolderId(masterFolder.MasterFolderId);
                logger.Info($"{Constants.GET_CONTENTS_BY_FOLDER_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = response, Status = true }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(masterFolder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// publish or unpublish a content.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("publish")]
        public HttpResponseMessage PubishUnpublishContent(MasterContentViewModel content)
        {
            logger.Info("api/mastercontent/publish is called");

            Response response = new Response();
            if (content.MasterContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                int checkPublishOrUnpublish = (int)Constants.PublishedOrUnpublish.ContentNotFound;
                //first create a thumbnail and then set the bit in database for publish if flag is true then only create thumbnail
                if (content.flag)
                {
                    MasterContentViewModel contentExisting = _masterContentService.GetContent(content.MasterContentId);
                    if (contentExisting.ContentUrl.Contains(".mp4") || contentExisting.ContentUrl.Contains(".mov") || contentExisting.ContentUrl.Contains(".3gp"))
                    {
                        var isThumbCreated = _awsS3FileStorage.CreateThumbnailFromFile(contentExisting.ContentUrl, contentExisting.MimeType);
                        if (isThumbCreated)
                        {
                            checkPublishOrUnpublish = _masterContentService.PublishUnpublishContent(content.MasterContentId);
                            response.Status = true;
                        }
                        else
                        {
                            response.Status = false;
                        }
                    }
                    else
                    {
                        checkPublishOrUnpublish = _masterContentService.PublishUnpublishContent(content.MasterContentId);
                        response.Status = true;
                    }
                }
                else
                {
                    checkPublishOrUnpublish = _masterContentService.PublishUnpublishContent(content.MasterContentId);
                    response.Status = true;
                }
                if (checkPublishOrUnpublish == (int)Constants.PublishedOrUnpublish.Published)
                {
                    response.Message = Constants.CONTENT_PUBLISHED_SUCCESSFULLY;
                }
                else if(checkPublishOrUnpublish == (int)Constants.PublishedOrUnpublish.Unpublished)
                {
                    response.Message = Constants.CONTENT_UNPUBLISHED_SUCCESSFULLY;
                }
                logger.Info($"{"api/mastercontent/publish is completed"}");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(content, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

        }

        [HttpGet]
        [Route("get_all_contents_for_preveiew_pop_up")]
        public HttpResponseMessage GetFolderAndContentForPreviewPopUp(long folderId)
        {
            logger.Info($"{Constants.GET_CONTENTS_BY_FOLDER_API} is called");

            try
            {
                ContentPreviewViewModel response = _masterContentService.GetFolderAndContentForPreviewPopUp(folderId, IdentityHelper.GetUserId());
                logger.Info($"{Constants.GET_CONTENTS_BY_FOLDER_API} is completed");

                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = response, Status = true }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folderId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Api to get the list of MasterContent
        /// </summary>
        /// <returns></returns>
        [Route("GetAwss3Policy")]
        [AuthorizeUser(Role ="SuperAdmin")]
        public HttpResponseMessage GetAwss3Policy(string fileId)
        {
            logger.Info($"{Constants.GET_AWSS3_POLICY} is called");

            try
            {
                FileUploadViewModel fileUploadViewModel = new FileUploadViewModel(_awsS3FileStorage.PublicKey,
                                                                  _awsS3FileStorage.PrivateKey,
                                                                  _awsS3FileStorage.BucketName, "201");
                if (!string.IsNullOrEmpty(fileId))
                {
                    fileUploadViewModel.FileId = fileId;
                }

                fileUploadViewModel.SetPolicy(_awsS3FileStorage.GetPolicyString(fileUploadViewModel.FileId, fileUploadViewModel.RedirectUrl));
                Response response = new Response { Data = fileUploadViewModel, Status = true };
                logger.Info($"{Constants.GET_AWSS3_POLICY} is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("GetAwss3PolicyAndSignature")]
        public HttpResponseMessage GetAwss3PolicyAndSignature(string fileType)
        {
            logger.Info($"{Constants.GET_AWSS3_POLICY} is called");

            try
            {
                FileUploadViewModel fileUploadViewModel = new FileUploadViewModel(_awsS3FileStorage.PublicKey,
                                                                  _awsS3FileStorage.PrivateKey,
                                                                  _awsS3FileStorage.BucketName, "201");

                fileUploadViewModel.FileId = fileUploadViewModel.FileId + "." + fileType;
                fileUploadViewModel.SetPolicy(_awsS3FileStorage.GetPolicyString(fileUploadViewModel.FileId, fileUploadViewModel.RedirectUrl));
                AwsPolicySignatureViewModel awsPolicyAndSignature = new AwsPolicySignatureViewModel
                {
                    Policy = fileUploadViewModel.Base64EncodedPolicy,
                    FormAction = fileUploadViewModel.FormAction,
                    Signature = fileUploadViewModel.Signature,
                    FileId = fileUploadViewModel.FileId
                };
                Response response = new Response { Data = awsPolicyAndSignature, Status = true };
                logger.Info($"{Constants.GET_AWSS3_POLICY} is complete");
                return Request.CreateResponse(HttpStatusCode.OK,response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetMasterFolderContents")]
        [AuthorizeUser(Role = "SuperAdmin,Admin,Trainer")]
        public HttpResponseMessage GetMasterFolderContents(FolderViewModel folder)
        {
            logger.Info($"{Constants.GET_MASTER_FOLDER_CONTENTS} is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                long userId = IdentityHelper.GetUserId();
                FolderContentViewModel content = _masterContentService.GetMasterFolderContents(folder, userId);
                Response response = new Response
                {
                    Data = content,
                    Status = content == null ? false : true
                };
                logger.Info($"{Constants.GET_MASTER_FOLDER_CONTENTS} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("SaveSelectedMasterContentFolder")]
        [AuthorizeUser(Role = "SuperAdmin,Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage SaveSelectedMasterContentFolder(UserSavedFoldersViewModel request)
        {
            logger.Info($"{Constants.SAVE_SELECTED_MASTER_CONTENT_FOLDER} is called");

            try
            {
                _masterContentService.SaveSelectedMasterContentFolder(request, IdentityHelper.GetUserId());
                Response response = new Response { Status = true };
                logger.Info($"{Constants.SAVE_SELECTED_MASTER_CONTENT_FOLDER} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdateMasterContentFolderOrders")]
        [AuthorizeUser(Role = "SuperAdmin,Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage UpdateMasterContentFolderOrders(List<SortFoldersViewModel> request)
        {
            logger.Info($"{Constants.UPDATE_MASTER_CONTENT_FOLDER_ORDERS} is called");

            try
            {
                _masterContentService.UpdateMasterContentFolderOrders(request, IdentityHelper.GetUserId());
                Response response = new Response { Status = true };
                logger.Info($"{Constants.UPDATE_MASTER_CONTENT_FOLDER_ORDERS} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetMasterContentSelectedFolders")]
        [AuthorizeUser(Role = "SuperAdmin,Admin,Trainer")]
        public HttpResponseMessage GetMasterContentSelectedFolders()
        {
            logger.Info($"{Constants.GET_MASTER_CONTENT_SELECTED_FOLDERS} is called");
            try
            {
                List<FolderViewModel> folderViewModel = _masterContentService.GetMasterContentSelectedFolders(IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Status = true,
                    Data = folderViewModel
                };
                logger.Info($"{Constants.GET_MASTER_CONTENT_SELECTED_FOLDERS} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [Route("GetMasterFolderContentDetail")]
        [CheckModelForNull]
        public HttpResponseMessage GetMasterFolderContentDetail(ContentViewModel masterContent)
        {
            logger.Info("GetMasterFolderContentDetail is called");
            if (masterContent.ContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                masterContent.IPAddress = Helper.GetClientIp();
                FolderContentDetailViewModel content = _masterContentService.GetMasterFolderContentDetail(masterContent, IdentityHelper.GetUserId());
                Response response = new Response { Status = true, Data = content };
                logger.Info("GetMasterFolderContentDetail is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(masterContent, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("GetMasterContentsByType")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetMasterContentsByType(FolderContentRequestViewModel folder)
        {
            logger.Info($"{Constants.GET_MASTER_CONTENT_BY_TYPE} is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                List<ContentViewModel> contents = _masterContentService.GetMasterContentsByType(folder);
                Response response = new Response
                {
                    Status = true,
                    Data = contents
                };
                logger.Info($"{Constants.GET_MASTER_CONTENT_BY_TYPE} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}

