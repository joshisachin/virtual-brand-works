﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/CompanyContent")]
    public class CompanyContentController : ApiController
    {
        private readonly ICompanyContentService _companyContentService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CompanyContentController));
        public CompanyContentController(ICompanyContentService companyContentService)
        {
            _companyContentService = companyContentService;
        }

        [HttpPost]
        [Route("GetCompanyContentFolders")]
        [AuthorizeUser(Role = "SuperAdmin,Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage GetCompanyContentFolders(CompanyContentFolderRequestViewModel request)
        {
            logger.Info($"{Constants.GET_COMPANY_CONTENT_FOLDERS} is called");

            try
            {
                request.UserId = IdentityHelper.GetUserId();
                List<FolderViewModel> companyContentFolderViewModel = _companyContentService.GetCompanyContentFolders(request);
                Response response = new Response
                {
                    Data = companyContentFolderViewModel,
                    Status = companyContentFolderViewModel == null ? false : true
                };
                logger.Info($"{Constants.GET_COMPANY_CONTENT_FOLDERS} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(request, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Create Content api for a company
        /// </summary>
        /// <param name="companyContent"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateContent")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage CreateContent(CompanyContentViewModel companyContent)
        {
            logger.Info("CreateContent is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                companyContent.UserId = IdentityHelper.GetUserId();
                ContentResponseViewModel content = _companyContentService.CreateContent(companyContent);
                Response response = new Response { Status = true, Data = content };
                logger.Info("CreateContent is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(companyContent, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [Route("GetContentConfirmation")]
        [CheckModelForNull]
        public HttpResponseMessage GetContentConfirmation(ContentViewModel companyContent)
        {
            logger.Info("GetContentConfirmation is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                //companyContent.UserId = IdentityHelper.GetUserId();
                CompanyContentResponse content = _companyContentService.GetContentConfirmation(companyContent);
                Response response = new Response
                {
                    Data = content,
                    Status = content == null ? false : true
                };
                logger.Info("GetContentConfirmation is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(companyContent, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [Route("PublishCompanyContent")]
        [CheckModelForNull]
        public HttpResponseMessage PublishCompanyContent(ContentViewModel companyContent)
        {
            logger.Info("GetContentConfirmation is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                //companyContent.UserId = IdentityHelper.GetUserId();
                _companyContentService.PublishCompanyContent(companyContent);
                Response response = new Response { Status = true};
                logger.Info("GetContentConfirmation is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(companyContent, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// api to get the content detail and folder detail of requested content for a company
        /// </summary>
        /// <param name="companyContent"></param>
        /// <returns></returns>

        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [Route("GetCompanyFolderContentDetail")]
        [CheckModelForNull]
        public HttpResponseMessage GetCompanyFolderContentDetail(ContentViewModel companyContent)
        {
            logger.Info("GetCompanyFolderContentDetail is called");
            if (companyContent.ContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                companyContent.IPAddress = Helper.GetClientIp();
                FolderContentDetailViewModel content = _companyContentService.GetCompanyFolderContentDetail(companyContent, IdentityHelper.GetUserId());
                Response response = new Response { Status = true, Data = content };
                logger.Info("GetCompanyFolderContentDetail is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(companyContent, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [Route("GetQuizDetails")]
        [CheckModelForNull]
        public HttpResponseMessage GetQuizDetails(long contentId)
        {
            logger.Info("GetQuizDetails is called");
            try
            {
                if (contentId == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new HttpError(Constants.QUIZ_ID_CANNOT_BE_EMPTY));

                QuizViewModel response = _companyContentService.GetQuizDetails(contentId);
                logger.Info("GetQuizDetails is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = response, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(contentId, Formatting.Indented)}", ex); 
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// api to create a new folder for a company
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateFolder")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage CreateFolder(CompanyFolderViewModel folder)
        {
            logger.Info("CreateFolder is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            Response response = new Response();

            try
            {
                folder.UserId = IdentityHelper.GetUserId();
                bool isFolderExist = _companyContentService.IsCompanyFolderExists(folder);
                if (!isFolderExist)
                {
                    bool isFolderShortNameExist = _companyContentService.IsCompanyFolderShortNameExists(folder);
                    if (isFolderShortNameExist)
                    {
                        response.Status = false;
                        response.Message = Constants.FOLDER_SHORTNAME_ALREADY_EXIST;
                        return Request.CreateResponse(HttpStatusCode.OK, response);
                    }
                    folder.UserId = IdentityHelper.GetUserId();
                    FolderDetailViewModel createdFolder =_companyContentService.CreateFolder(folder);
                    response.Status = true;
                    response.Data = createdFolder;
                    logger.Info("CreateFolder is completed");
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                response.Status = false;
                response.Message = Constants.FOLDER_NAME_ALREADY_EXIST;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// api to update a folder for a company
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateFolder")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage UpdateFolder(CompanyFolderViewModel folder)
        {
            logger.Info("UpdateFolder is called");

            if (!ModelState.IsValid || folder.CompanyLibraryFolderId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            Response response = new Response();

            try
            {
                folder.UserId = IdentityHelper.GetUserId();
                bool isFolderExist = _companyContentService.IsCompanyFolderExists(folder);
                if (!isFolderExist)
                {
                    bool isFolderShortNameExist = _companyContentService.IsCompanyFolderShortNameExists(folder);
                    if (isFolderShortNameExist)
                    {
                        response.Status = false;
                        response.Message = Constants.FOLDER_SHORTNAME_ALREADY_EXIST;
                        return Request.CreateResponse(HttpStatusCode.OK, response);
                    }

                    folder.UserId = IdentityHelper.GetUserId();
                    _companyContentService.UpdateFolder(folder);
                    response.Status = true;
                    logger.Info("UpdateFolder is completed");
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                response.Status = false;
                response.Message = Constants.FOLDER_NAME_ALREADY_EXIST;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetCompanyFolderContents")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetCompanyFolderContents(FolderViewModel folder)
        {
            logger.Info("GetCompanyFolderContents is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                long userId = IdentityHelper.GetUserId();
                FolderContentViewModel content = _companyContentService.GetCompanyFolderContents(folder,userId);
                Response response = new Response
                {
                    Data = content,
                    Status = content == null ? false : true
                };
                logger.Info("GetCompanyFolderContents is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("SaveSelectedCompanyContentFolder")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage SaveSelectedCompanyContentFolder(UserSavedFoldersViewModel request)
        {
            logger.Info($"{Constants.SAVE_SELECTED_MASTER_CONTENT_FOLDER} is called");

            try
            {
                _companyContentService.SaveSelectedCompanyContentFolder(request, IdentityHelper.GetUserId());
                Response response = new Response { Status = true };
                logger.Info($"{Constants.SAVE_SELECTED_MASTER_CONTENT_FOLDER} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(request, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdateCompanyContentFolderOrders")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage UpdateCompanyContentFolderOrders(List<SortFoldersViewModel> request)
        {
            logger.Info("UpdateCompanyContentFolderOrders is called");

            try
            {
                _companyContentService.UpdateCompanyContentFolderOrders(request, IdentityHelper.GetUserId());
                Response response = new Response { Status = true };
                logger.Info("UpdateCompanyContentFolderOrders is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(request, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetCompanyContentSelectedFolders")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetCompanyContentSelectedFolders()
        {
            logger.Info("GetCompanyContentSelectedFolders is called");
            try
            {
                List<FolderViewModel> folderViewModel = _companyContentService.GetCompanyContentSelectedFolders(IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Status = true,
                    Data = folderViewModel
                };
                logger.Info("GetCompanyContentSelectedFolders is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("GetFolderAndContentForPreviewPopUp")]
        public HttpResponseMessage GetFolderAndContentForPreviewPopUp(long folderId)
        {
            logger.Info("GetFolderAndContentForPreviewPopUp is called");

            try
            {
                ContentPreviewViewModel response = _companyContentService.GetFolderAndContentForPreviewPopUp(folderId);

                logger.Info("GetFolderAndContentForPreviewPopUp is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = response, Status = true }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folderId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetCompanyFolderList")]
        public HttpResponseMessage GetCompanyFolderList()
        {
            logger.Info("GetCompanyFolderList is called");

            try
            {
                List<FolderDetailViewModel> folders = _companyContentService.GetCompanyFolderList(IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Data = folders,
                    Status = folders == null ? false : true
                };
                logger.Info("GetCompanyFolderList is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("GetCompanyContentsByType")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetCompanyContentsByType(FolderContentRequestViewModel folder)
        {
            logger.Info("GetCompanyContentsByType is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                List<ContentViewModel> contents = _companyContentService.GetCompanyContentsByType(folder);
                Response response = new Response
                {
                    Data = contents,
                    Status = contents == null ? false :true,
                };
                logger.Info("GetCompanyContentsByType is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("GetUserCreatedContentList")]
        [HttpGet]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetUserCreatedContentList()
        {
            logger.Info("GetUserCreatedContentList is called");
            try
            {
                List<ContentViewModel> contents = _companyContentService.GetUserCreatedContentList(IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Data = contents,
                    Status = contents == null ? false : true,
                };
                logger.Info("GetUserCreatedContentList is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("DeleteCompanyContent")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage DeleteCompanyContent(ContentViewModel content)
        {
            logger.Info("DeleteCompanyContent is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                bool isDeleteSuccessful = _companyContentService.DeleteCompanyContent(content.ContentId);
                logger.Info("DeleteCompanyContent is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = isDeleteSuccessful, Message = isDeleteSuccessful ? Constants.CONTENT_DELETED_SUCCESSFULLY : Constants.CONTENT_ASSOCIATED_TO_PLAYLIST});
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("BroadcastCompanyContent")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage BroadcastCompanyContent(ContentViewModel content)
        {
            logger.Info("BroadCastCompanyContent is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                bool isBroadCasted = _companyContentService.BroadCastCompanyContent(content);
                logger.Info("BroadCastCompanyContent is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Message = isBroadCasted ? Constants.CONTENT_BROADCASTED_SUCCESSFULLY : Constants.CONTENT_UNBROADCASTED_SUCCESSFULLY });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("CompanyContentView")]
        [HttpPost]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage CompanyContentView(ContentViewModel content)
        {
            logger.Info("CompanyContentView is called");
            if (!ModelState.IsValid || content.ContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                content.IPAddress = Helper.GetClientIp();
                _companyContentService.CompanyContentView(content, IdentityHelper.GetUserId());
                logger.Info("CompanyContentView is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("UpdateCompanyContentVisibility")]
        [HttpPost]
        public HttpResponseMessage UpdateCompanyContentVisibility(CompanyContentVisibilityViewModel content)
        {
            logger.Info("UpdateCompanyContentVisibility is called");
            if (content.ContentId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                content.UserId = IdentityHelper.GetUserId();
                _companyContentService.UpdateCompanyContentVisibility(content);
                logger.Info("UpdateCompanyContentVisibility is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
