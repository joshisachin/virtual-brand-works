﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using VirtualBrandWork.Models;
using System.Net;
using VirtualBrandWork.Filters;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Common;
using VirtualBrandWork.Notification;
using Service.Interfaces;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNet.Identity.Owin;
using Facebook;
using static VirtualBrandWork.Entity.Constants;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Notification.Interfaces;
using System.Threading.Tasks;

namespace VirtualBrandWork.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountController));
        private readonly IStripePaymentUtility _stripePaymentUtility;
        private readonly INotificationService _notificationService;
        private readonly ISubscriptionService _subscriptionService;
        public AccountController(IUserService userService, IStripePaymentUtility stripePaymentUtility, IEmailService emailService, INotificationService notificationService, ISubscriptionService subscriptionService)
        {
            _userService = userService;
            _stripePaymentUtility = stripePaymentUtility;
            _emailService = emailService;
            _notificationService = notificationService;
            _subscriptionService = subscriptionService;
        }

        [AllowAnonymous]
        [Route("login")]
        public HttpResponseMessage Login(LoginViewModel model)
        {
            logger.Info($"{Entity.Constants.ACCOUNT_LOGIN}  is called");

            try
            {
                //get user 
                UserContext user = _userService.ValidateUser(model.Email, model.Password);
                Response response = new Response();
                if (user == null)
                {
                    response.Message = "You have entered an incorrect username and/or password.";
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                if (!user.IsActive)
                {
                    response.Message = Entity.Constants.USER_LOGIN_DEACTIVATED;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                if ((!string.IsNullOrEmpty(model.SaLoginRequestKey) && model.SaLoginRequestKey.Equals(SA_LOGIN_REQUEST_KEY) && !user.RoleId.Equals((int)UserRoles.SuperAdmin))
                    || (string.IsNullOrEmpty(model.SaLoginRequestKey) && user.RoleId.Equals((int)UserRoles.SuperAdmin)))
                {
                    response.Message = "Invalid User Role Access";
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                bool companyStatus = CheckCompanyStatus(user);
                if (!companyStatus)
                {
                    response.Message = Entity.Constants.COMPANY_ACCOUNT_DEACTIVATED;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                string statusCode = Guid.NewGuid().ToString();

                _userService.SaveUsersLoginStatus(model.Email, statusCode);

                _notificationService.SaveFcmToken(user.UserId, model.FCMToken);
                
                //Parallel.Invoke(() => _userService.SaveUsersLoginStatus(model.Email, statusCode), () => _notificationService.SaveFcmToken(user.UserId, model.FCMToken));

                //validate subscription
                user.IsSubscriptionActive = _userService.ValidateSubscription(user.UserId);

                AuthenticationModule authentication = new AuthenticationModule();
                string token = authentication.GenerateTokenForUser(user, statusCode);
                user.JWTToken = token;
                response = new Response { Data = user, Status = true };
                Request.GetOwinContext().Authentication.SignIn();
                logger.Info($"{Entity.Constants.ACCOUNT_LOGIN}  is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// External Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("external_login")]
        public HttpResponseMessage ExternalLogin(ExternalLoginViewModel externalLogin)
        {
            logger.Info("ExternaLogin called");

            if (!ModelState.IsValid || String.IsNullOrEmpty(externalLogin.AccessToken))
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);

            Response response = new Response();
            try
            {
                //retrive user email based on external login provider
                if (externalLogin.Provider.ToUpper().Equals("FACEBOOK"))
                {
                    var fb = new FacebookClient(externalLogin.AccessToken);
                    dynamic myInfo = fb.Get("/me?fields=email");
                    externalLogin.Email = myInfo.email;
                }
                else if (externalLogin.Provider.ToUpper().Equals("GOOGLE"))
                {
                    var web = new WebClient();
                    string userInfo = web.DownloadString("https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + externalLogin.AccessToken);

                    GoogleUserInfo googleUserInfo = JsonConvert.DeserializeObject<GoogleUserInfo>(userInfo);

                    externalLogin.Email = googleUserInfo.email;
                }

                //get UserCOntext 
                UserContext user = _userService.ValidateExternalLogin(externalLogin.Email);

                if (user == null)
                {
                    response.Message = Entity.Constants.EXTERNAL_LOGIN_USER_NOT_REGISTERED;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                bool companyStatus = CheckCompanyStatus(user);
                if (!companyStatus)
                {
                    response.Message = Entity.Constants.COMPANY_ACCOUNT_DEACTIVATED;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                string statusCode = Guid.NewGuid().ToString();

                _userService.SaveUsersLoginStatus(externalLogin.Email, statusCode);

                AuthenticationModule authentication = new AuthenticationModule();
                string token = authentication.GenerateTokenForUser(user, statusCode);
                user.JWTToken = token;
                response = new Response { Data = user, Status = true };
                Request.GetOwinContext().Authentication.SignIn();
                logger.Info("ExternalLogin is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(externalLogin, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST api/Account/ForgotPassword
        [AllowAnonymous]
        [HttpPost]
        [Route("forgot_password")]
        public HttpResponseMessage ForgotPassword(ForgotPasswordViewModel model)
        {
            logger.Info($"{Entity.Constants.FORGOT_PASSWORD}  is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);

            try
            {
                bool flag = false;
                Response response = new Response();
                Entity.ViewModels.UserViewModel user = _userService.ValidateUserWithEmail(model.EmailId);
                if (user != null)
                {
                    Random generator = new Random();
                    string resetCode = generator.Next(0, 999999).ToString("D6");
                    string userName = $"{user.FirstName} {user.LastName}";
                    _emailService.SendResetPasswordCode(userName, user.EmailId, resetCode, "ResetPassword");
                    flag = _userService.SetPasswordResetCode(user, resetCode);
                }
                else
                {
                    response.Message = "This email is not registered";
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                if (flag)
                {
                    response.Status = true;
                    logger.Info($"{Entity.Constants.FORGOT_PASSWORD}  is completed");
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // POST api/Account/ResetPassword
        [AllowAnonymous]
        [HttpPost]
        [Route("reset_password")]
        public HttpResponseMessage ResetPassword(ResetPasswordViewModel model)
        {
            logger.Info($"{Entity.Constants.RESET_PASSWORD}  is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);

            Response response = new Response();
            try
            {
                int result = _userService.ResetPassword(model);

                if (result == Convert.ToInt32(Entity.Constants.ResetPasswordResponse.Invalid))
                {
                    response.Message = Entity.Constants.RESET_PASSWORD_CODE_INVALID;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else if (result == Convert.ToInt32(Entity.Constants.ResetPasswordResponse.Success))
                {
                    response.Status = true;
                    logger.Info($"{Entity.Constants.RESET_PASSWORD}  is completed");
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    response.Message = Entity.Constants.RESET_PASSWORD_CODE_EXPIRED;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(model, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("change_password")]
        public HttpResponseMessage ChangePassword(ChangePasswordViewModel changePassword)
        {
            logger.Info($"{Entity.Constants.CHANGE_PASSWORD}  is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);

            try
            {
                bool flag = false;
                flag = _userService.ChangePassword(changePassword, Convert.ToInt32(changePassword.UserId));
                Response response = new Response();
                if (flag)
                {
                    response.Status = true;
                    logger.Info($"{Entity.Constants.CHANGE_PASSWORD}  is completed");
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    response.Message = "You have entered an incorrect password";
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(changePassword, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [AllowAnonymous]
        [Route("validate_company")]
        public HttpResponseMessage ValidateCompany(UserContext user)
        {
            logger.Info("ValidateCompany called");

            if (user.UserId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);
            Response response = new Response();
            try
            {
                bool companyStatus = CheckCompanyStatus(user);
                if (!companyStatus)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                response.Status = true;
                response.Data = Entity.Constants.COMPANY_ACTIVATED;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("signup_as_user")]
        public HttpResponseMessage SignUpAsUser(SignUpViewModel signUpUser)
        {
            logger.Info("SignUpAsUser called");

            if (signUpUser == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);

            try
            {
                //get user 
                Entity.ViewModels.UserViewModel user = _userService.ValidateUserWithEmail(signUpUser.Email);
                Response response = new Response();
                if (user != null)
                {
                    response.Message = Entity.Constants.EMAIL_ALREADY_REGISTERED;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    Entity.ViewModels.UserViewModel userWithTeamHandle = _userService.ValidateUserWithHandleName(signUpUser.HandleName);
                    if (userWithTeamHandle != null)
                    {
                        response.Message = Entity.Constants.HANDLENAME_ALREADY_REGISTERED;
                        return Request.CreateResponse(HttpStatusCode.OK, response);
                    }
                    _userService.SignUpAsUser(signUpUser);
                    response.Message = Entity.Constants.SIGNUP_SUCCESSFUL;
                    response.Status = true;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(signUpUser, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [AllowAnonymous]
        [Route("ValidateUserEmailAndCompanyName")]
        public HttpResponseMessage ValidateUserEmailAndCompanyName(SignUpViewModel signUpViewModel)
        {
            logger.Info("ValidateUserEmailAndCompanyName called");

            if (string.IsNullOrEmpty(signUpViewModel.Email) && string.IsNullOrEmpty(signUpViewModel.CompanyName))
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);
            try
            {
                List<ValidationSummaryViewModel> model = _userService.ValidateUserEmailAndCompanyName(signUpViewModel);
                logger.Info("ValidateUserEmailAndCompanyName completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = model, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [AllowAnonymous]
        [Route("SaveAdminDetails")]
        public HttpResponseMessage SaveAdminDetails(SignUpViewModel signUpViewModel)
        {
            logger.Info("SaveAdminDetails called");

            if (string.IsNullOrEmpty(signUpViewModel.Email) && string.IsNullOrEmpty(signUpViewModel.CompanyName))
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);
            try
            {
                Entity.ViewModels.UserViewModel userWithTeamHandle = _userService.ValidateUserWithHandleName(signUpViewModel.HandleName);
                if (userWithTeamHandle != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Response() { Message = Entity.Constants.HANDLENAME_ALREADY_REGISTERED });
                }
                Response response = _userService.SaveAdminDetails(signUpViewModel);
                logger.Info("SaveAdminDetails completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false });
            }
        }

        [AllowAnonymous]
        [Route("DeleteSignUpDetails")]
        [HttpPost]
        public HttpResponseMessage DeleteSignUpDetails(SignUpViewModel model)
        {
            logger.Info("DeleteSignUpDetails called");
            try
            {
                Response response = _userService.DeleteSignUpDetails(model.Email);
                logger.Info("DeleteSignUpDetails completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("ChangeSubscriptionPlan")]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage ChangeSubscriptionPlan(long planId, string stripeSubscriptionPlanId, string oldPlanName)
        {
            logger.Info("ChangeSubscriptionPlan called");
            if (planId == 0 || string.IsNullOrEmpty(stripeSubscriptionPlanId) || string.IsNullOrEmpty(oldPlanName))
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);
            try
            {
                UserMyPlanNotiFicationViewModel notificationInfo = _userService.ChangeSubscriptionPlan(IdentityHelper.GetUserId(), planId, stripeSubscriptionPlanId, oldPlanName);
                _emailService.SendSubscriptionUpdatedNotificationEmail(notificationInfo);
                logger.Info("ChangeSubscriptionPlan completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("CancelSubscriptionPlan")]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage CancelSubscriptionPlan(CancelSubscriptionRequestViewModel plan)
        {
            logger.Info("CancelSubscriptionPlan called");
            if (plan.PlanId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Entity.Constants.INVALID_ARGUMENT);
            }
            try
            {
                UserMyPlanNotiFicationViewModel notificationInfo = _userService.CancelSubscriptionPlan(IdentityHelper.GetUserId(), plan.PlanId);
                _emailService.SendSubscriptionScheduledToCancelEmail(notificationInfo);
                logger.Info("CancelSubscriptionPlan completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(plan, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("GetCreateCardCheckoutSession")]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage GetCreateCardCheckoutSession(CreateCardCheckoutRequestViewModel checkoutRequest)
        {
            logger.Info("GetCreateCardCheckoutSession called");
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                string sessionId = _userService.GetCreateCardCheckoutSession(IdentityHelper.GetUserId(), checkoutRequest.SuccessUrl, checkoutRequest.CancelUrl);
                logger.Info("GetCreateCardCheckoutSession completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = sessionId, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(checkoutRequest, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("DetachPaymentMethod")]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage DetachPaymentMethod(string paymentMethodId)
        {
            logger.Info("DetachPaymentMethod called");
            try
            {
                _stripePaymentUtility.DetachPaymethod(paymentMethodId);
                logger.Info("DetachPaymentMethod completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("SetDefaultPaymentMethod")]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage SetDefaultPaymentMethod(string paymentMethodId)
        {
            logger.Info("SetDefaultPaymentMethod called");
            try
            {
                _userService.SetDefaultPaymentMethod(IdentityHelper.GetUserId(), paymentMethodId);
                logger.Info("SetDefaultPaymentMethod completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("PaymentMethods")]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage PaymentMethods()
        {
            logger.Info("PaymentMethods called");
            try
            {
                PaymentMethodViewModel paymentMethods = _userService.PaymentMethods(IdentityHelper.GetUserId());
                logger.Info("PaymentMethods completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = paymentMethods, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [HttpPost]
        [Route("Logout")]
        public HttpResponseMessage Logout()
        {
            logger.Info("Logout called");
            try
            {
                string authToken = Request.Headers.GetValues("Authorization").FirstOrDefault();
                var auth = new AuthenticationModule();
                System.IdentityModel.Tokens.Jwt.JwtSecurityToken userPayloadToken = auth.GenerateUserClaimFromJWT(authToken.Replace("Bearer ", ""));
                if (userPayloadToken != null)
                {
                    var identity = auth.PopulateUserIdentity(userPayloadToken);
                    _userService.RemoveLoginStatus(identity.UserId, identity.StatusCode);
                }

                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true, Message = "Logout Successfully." });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [HttpGet]        
        [Route("GetNotifications")]
        public HttpResponseMessage GetNotifications(long userId)
        {
            logger.Info("Notification called");
            try
            {
                List<Notifications> objNotifications = _notificationService.GetNotifications(userId);

                return Request.CreateResponse(HttpStatusCode.OK, new Response() {Data = objNotifications, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [HttpGet]
        [JWTAuthenticationFilter]
        [Route("ReadNotifications")]
        public HttpResponseMessage ReadNotifications(long userId)
        {
            logger.Info("Read Notification called");
            try
            {
                string authToken = Request.Headers.GetValues("Authorization").FirstOrDefault();

                _notificationService.ReadNotifications(userId);

                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }


        [HttpGet]
        [Route("RenewToken")]
        public HttpResponseMessage RenewToken()
        {
            logger.Info("RenewToken called");
            try
            {
                string authToken = Request.Headers.GetValues("Authorization").FirstOrDefault();
                var auth = new AuthenticationModule();
                System.IdentityModel.Tokens.Jwt.JwtSecurityToken userPayloadToken = auth.GetUserClaimFromExpiredToken(authToken.Replace("Bearer ", ""));
                if (userPayloadToken != null)
                {
                    var identity = auth.PopulateUserIdentity(userPayloadToken);
                    UserContext user = _userService.GetUserContextFromUserId(identity.UserId);

                    string statusCode = Guid.NewGuid().ToString();

                    _userService.SaveUsersLoginStatus(_userService.GetEmailIdFromUserId(user.UserId), statusCode);

                   // _notificationService.SaveFcmToken(user.UserId, model.FCMToken);

                    AuthenticationModule authentication = new AuthenticationModule();
                    string token = authentication.GenerateTokenForUser(user, statusCode);
                    Response response = new Response { Data = token, Status = true };
                    Request.GetOwinContext().Authentication.SignIn();
                    logger.Info($"RenewToken  is completed");
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = false, Message = "Invalid Token" });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [JWTAuthenticationFilter]
        [HttpGet]
        [Route("MyPlan")]
        public HttpResponseMessage MyPlan()
        {
            logger.Info("MyPlan called");
            try
            {
                long companyId = _userService.GetCompanyIdFromUserId(IdentityHelper.GetUserId());
                CompanySubscriptionViewModel companySubscriptions = _subscriptionService.GetSubscriptionDetailsForCompany(companyId);
                if(companySubscriptions != null)
                {
                    companySubscriptions.PaymentCard = _userService.PaymentMethods(IdentityHelper.GetUserId());
                }
                logger.Info("MyPlan completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = companySubscriptions, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        /// <summary>
        /// update payment method billing address
        /// </summary>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("UpdateBillingAddress")]
        [AuthorizeUser(Role = "Admin")]
        public HttpResponseMessage UpdateBillingAddress(MyPlanBillingAddressViewModel billingDetails)
        {
            logger.Info("UpdateBillingAddress called");
            try
            {
                _userService.UpdateBillingAddress(IdentityHelper.GetUserId(), billingDetails);
                logger.Info("MyPlan completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }


        /// <summary>
        /// api to set the new paymentMethod as default
        /// </summary>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("UpdatePaymentMethodSuccess")]
        public HttpResponseMessage UpdatePaymentMethodSuccess([FromBody] string SessionId)
        {
            logger.Info("UpdatePaymentMethodSuccess called");
            try
            {
                UserMyPlanNotiFicationViewModel notificationInfo = _userService.UpdatePaymentMethodSuccess(IdentityHelper.GetUserId(), SessionId);
                _emailService.SendUpdatePaymentInfoEmail(notificationInfo);
                logger.Info("UpdatePaymentMethodSuccess completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        /// <summary>
        /// api to set the new paymentMethod as default
        /// </summary>
        /// <returns></returns>
        [JWTAuthenticationFilter]
        [HttpPost]
        [Route("UpdatePaymentMethodFailure")]
        public HttpResponseMessage UpdatePaymentMethodFailure([FromBody] string SessionId)
        {
            logger.Info("UpdatePaymentMethodFailure called");
            try
            {
                Stripe.Checkout.Session session = _stripePaymentUtility.GetCheckoutSession(SessionId);
                StripeSessionResponse stripeResponse = JsonConvert.DeserializeObject<StripeSessionResponse>(session.RawJObject.ToString());
                UserMyPlanNotiFicationViewModel userInfo = _userService.GetUserNameAndEmail(IdentityHelper.GetUserId());
                _emailService.SendUpdatePaymentInfoFailureEmail(userInfo);
                logger.Info("UpdatePaymentMethodFailure completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Webhook")]
        public HttpResponseMessage Webhook(Stripe.Subscription sub)
        {
            logger.Info("Webhook called");
            try
            {
                //Stripe.Checkout.Session session = _stripePaymentUtility.GetCheckoutSession(SessionId);
                //StripeSessionResponse stripeResponse = JsonConvert.DeserializeObject<StripeSessionResponse>(session.RawJObject.ToString());
                //UserMyPlanNotiFicationViewModel userInfo = _userService.GetUserNameAndEmail(IdentityHelper.GetUserId());
                //_emailService.SendUpdatePaymentInfoFailureEmail(userInfo);
                logger.Info("Webhook completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = SOME_ERROR_OCCUREED });
            }
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private bool CheckCompanyStatus(UserContext user)
        {
            if (!user.UserRole.Equals(UserRoles.SuperAdmin.ToString()))
            {
                bool check = _userService.ValidateCompanyStatus(user.UserId);
                return check;
            }
            return true;
        }
        #endregion
    }
}
