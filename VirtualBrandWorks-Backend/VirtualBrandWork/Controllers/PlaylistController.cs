﻿using AutoMapper;
using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/Playlist")]
    public class PlaylistController : ApiController
    {
        private readonly IPlaylistService _playlistService;
        private readonly INotificationService _notificationService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PlaylistController));
        public PlaylistController(IPlaylistService playlistService, INotificationService notificationService)
        {
            _playlistService = playlistService;
            _notificationService = notificationService;
        }

        [HttpPost]
        [Route("SendMessage")]
        [AuthorizeUser(Role = "Admin,Trainer,user")]
        [CheckModelForNull]
        public HttpResponseMessage SendMessage(MessageViewModel messageViewModel)
        {
            logger.Info("SendMessage is called");

            try
            {
                messageViewModel.SenderId = IdentityHelper.GetUserId();
                MessageViewModel message = _playlistService.SendMessage(messageViewModel);
                _notificationService.SendNotifications(new List<long>() { messageViewModel.ReceiverId }, string.Format(Constants.MessageBody, IdentityHelper.GetUserName()), Constants.MessageTitle);
                logger.Info("SendMessage is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = message, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(messageViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.SOME_ERROR_OCCUREED });
            }
        }

        [HttpPost]
        [Route("ReceiveMessage")]
        [AuthorizeUser(Role = "Admin,Trainer,user")]
        [CheckModelForNull]
        public HttpResponseMessage ReceiveMessage(MessageViewModel messageViewModel)
        {
            logger.Info("ReceiveMessage is called");

            try
            {
                messageViewModel.ReceiverId = IdentityHelper.GetUserId();
                List<MessageViewModel> messages = _playlistService.ReceiveMessage(messageViewModel);
                logger.Info("ReceiveMessage is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response() { Data = messages, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(messageViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.SOME_ERROR_OCCUREED });
            }
        }

        [HttpGet]
        [Route("GetPlaylistTopPerofermers")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPlaylistTopPerofermers(long playListId, bool isTopTen)
        {
            logger.Info("GetPlaylistTopPerofermers called");
            if (playListId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<PlaylistTopPerformerViewModel> playlistTopPerformers = _playlistService.GetPlaylistTopPerofermers(playListId, isTopTen);
                logger.Info("GetPlaylistTopPerofermers completed");
                Response response = new Response
                {
                    Data = playlistTopPerformers,
                    Status = playlistTopPerformers == null ? false : true,
                    Message = playlistTopPerformers == null ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        /// <summary>
        /// leaderboard api all- tab
        /// </summary>
        /// <param name="playListId"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPlaylistLeaderboard")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPlaylistLeaderboard(long playListId, int? sortBy)
        {
            logger.Info("GetPlaylistLeaderboard called");
            if (playListId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<PlaylistLeaderboardViewModel> leaderboards = _playlistService.GetPlaylistLeaderboard(playListId, sortBy);
                List<LeaderboardViewModel> playlistLeaderboard = new List<LeaderboardViewModel>();
                playlistLeaderboard = Mapper.Map<List<PlaylistLeaderboardViewModel>, List<LeaderboardViewModel>>(leaderboards);
                logger.Info("GetPlaylistLeaderboard completed");
                Response response = new Response
                {
                    Data = playlistLeaderboard,
                    Status = playlistLeaderboard.Count() == 0 ? false : true,
                    Message = playlistLeaderboard.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// api to get dropdown list of playlist
        /// </summary>
        /// <param name="playListId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllPlaylist")]
        public HttpResponseMessage GetAllPlaylist()
        {
            logger.Info("GetAllPlaylist called");
            try
            {
                List<PlaylistDropdownViewModel> playlists = _playlistService.GetAllPlaylist(IdentityHelper.GetUserId());
                logger.Info("GetAllPlaylist completed");
                Response response = new Response
                {
                    Data = playlists,
                    Status = playlists.Count() == 0 ? false : true,
                    Message = playlists.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        //learderboardbyteam api
        [HttpGet]
        [Route("GetPlaylistLeaderboardByTeam")]
        [AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPlaylistLeaderboardByTeam(long playListId, int? sortBy)
        {
            logger.Info("GetPlaylistLeaderboard called");
            if (playListId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                List<LeaderboardByTeamViewModel> leaderboards = _playlistService.GetPlaylistLeaderboardByTeam(playListId, sortBy);
                logger.Info("GetPlaylistLeaderboard completed");
                Response response = new Response
                {
                    Data = leaderboards,
                    Status = leaderboards.Count() == 0 ? false : true,
                    Message = leaderboards.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
