﻿using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWork.Notification;
using VirtualBrandWork.Notification.Interfaces;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/companies")]
    public class CompanyController : ApiController
    {
        private readonly ICompanyService _companyService;
        private readonly IEmailService _emailService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CategoriesController));

        public CompanyController(ICompanyService categoriesService, IEmailService emailService)
        {
            _companyService = categoriesService;
            _emailService = emailService;
    }
        [HttpPost]
        [Route("get_users_by_company_id")]
        //[Authorize(Roles="SuperAdmin")]
        public HttpResponseMessage GetUsersByCompanyId(CompanyViewModel company)
        {
            if(company.CompanyId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            logger.Info("Index is called");
            try
            {
                IEnumerable<UserViewModel> users = _companyService.GetUsersByCompanyId(company.CompanyId);
                Response response = new Response { Data = users, Status = true };
                logger.Info("Index is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// activates or deactivates a company
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("activate_deactivate_company")]
        //[Authorize(Roles="SuperAdmin")]
        public HttpResponseMessage ActivateOrDeactivaeCompany(CompanyViewModel company)
        {
            if (company.CompanyId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            logger.Info("ActivateOrDeactivaeCompany called");
            try
            {
                CompanyViewModel companyInfo = _companyService.ActivateOrDeactivateCompany(company.CompanyId);
                if (companyInfo.Status)
                {                    
                    _emailService.SendActiveDeactiveNotification(Constants.COMPANY_ACTIVATED, companyInfo.AdminEmail, companyInfo.Name, true);
                }
                else
                {
                    _emailService.SendActiveDeactiveNotification(Constants.COMPANY_DEACTIVATED, companyInfo.AdminEmail, companyInfo.Name, false);
                }
                Response response = new Response { Status = true };
                logger.Info("ActivateOrDeactivaeCompany completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("get_all_company")]
       // [Authorize]
        public HttpResponseMessage GetAllCompany()
        {
            logger.Info("GetAllCompany is called");
            try
            {
                IEnumerable<CompanyViewModel> companies = _companyService.GetAllCompany();
                Response response = new Response { Data = companies, Status = true };
                logger.Info("GetAllCompany is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
