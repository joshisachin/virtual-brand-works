﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/masterfolder")]
    public class MasterFolderController : ApiController
    {
        private readonly IMasterFolderService _masterfolderService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MasterFolderController));
        //private readonly long _userid = IdentityHelper.GetUserId();
        public MasterFolderController(IMasterFolderService masterfolderService)
        {
            _masterfolderService = masterfolderService;
        }

        [Route("get_all_folders")]
        public HttpResponseMessage GetAllFolders()
        {
            logger.Info($"{Constants.GET_ALL_FOLDER_API} is called");

            try
            {
                List<MasterFolderViewModel> masterFolderViewModel = _masterfolderService.GetAllFolders();
                Response response = new Response { Data = masterFolderViewModel, Status = true };
                logger.Info($"{Constants.GET_ALL_FOLDER_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [HttpPost]
        [Route("get_all_folders_with_content")]
        public HttpResponseMessage GetAllFoldersAndContent(CompanyContentFolderRequestViewModel request)
        {
            //string industry = null,string searchString = null
            logger.Info($"{Constants.GET_ALL_FOLDER_API} is called");

            try
            {
                request.UserId = IdentityHelper.GetUserId();
                List<FolderViewModel> masterFolderViewModel = _masterfolderService.GetFilterFolders(request);
                Response response = new Response { Data = masterFolderViewModel, Status = true };
                logger.Info($"{Constants.GET_ALL_FOLDER_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [Route("create_folder")]
        public HttpResponseMessage Create(MasterFolderViewModel folder)
        {
            logger.Info($"{Constants.FOLDER_CREATE_API} is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                Response response = new Response();
                response = _masterfolderService.Create(folder);
                logger.Info($"{Constants.FOLDER_CREATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [Route("update_folder")]
        public HttpResponseMessage Update(MasterFolderViewModel folder)
        {
            logger.Info($"{Constants.FOLDER_UPDATE_API} is called");

            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                _masterfolderService.Update(folder);
                Response response = new Response() { Status = true };
                logger.Info($"{Constants.FOLDER_UPDATE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [Route("details_folder")]
        public HttpResponseMessage Details(MasterFolderViewModel folder)
        {
            logger.Info($"{Constants.FOLDER_DETAILS_API} is called");

            if (folder.MasterFolderId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                MasterFolderViewModel masterFolderViewModel = _masterfolderService.GetFolder(Convert.ToInt64(folder.MasterFolderId));
                Response response = new Response { Data = masterFolderViewModel, Status = true };
                logger.Info($"{Constants.FOLDER_DETAILS_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [HttpDelete]
        [Route("delete_folder")]
        public HttpResponseMessage Delete(MasterFolderViewModel folder)
        {
            logger.Info($"{Constants.FOLDER_DELETE_API} is called");

            if (folder.MasterFolderId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                Response response = _masterfolderService.Delete(folder.MasterFolderId);
                logger.Info($"{Constants.FOLDER_DELETE_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

        }
        /// <summary>
        /// Get master folders
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("get_folders_by_category")]
        public HttpResponseMessage GetFoldersByCategoyId(CategoryViewModel categoryViewModel)
        {
            logger.Info($"{Constants.GET_FOLDERS_BY_CATEGORY_API} is called");
            // need to change response for response
            try
            {
                Response response = _masterfolderService.GetFoldersByCategoyId(categoryViewModel.CategoryId);
                logger.Info($"{Constants.GET_FOLDERS_BY_CATEGORY_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(categoryViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("ValidateFolderName")]
        public HttpResponseMessage ValidateFolderName(MasterFolderViewModel folder)
        {
            logger.Info($"{Constants.VALIDATE_FOLDER_NAME_API} is called");

            if (string.IsNullOrEmpty(folder.Name) || string.IsNullOrEmpty(folder.ShortName))
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);

            try
            {
                Response response = new Response();
                bool isExists = _masterfolderService.IsFolderExists(folder);
                if (!isExists)
                {
                    bool isShortNameExists = _masterfolderService.IsFolderShortNameExists(folder);
                    if (!isShortNameExists)
                    {
                        response.Status = true;
                    }
                    else
                    {
                        response.Message = "Folder already exist with same short name";
                    }
                }
                else
                    response.Message = "Folder already exists with same name";
                logger.Info($"{Constants.VALIDATE_FOLDER_NAME_API} is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(folder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
