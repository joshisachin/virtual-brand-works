﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWork.Notification.Interfaces;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/PrizeStore")]
    public class PrizeStoreController : ApiController
    {
        private readonly IPrizeStoreService _prizeStoreService;
        private readonly IEmailService _emailService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PrizeStoreController));
        public PrizeStoreController(IPrizeStoreService prizeStoreService, IEmailService emailService)
        {
            _prizeStoreService = prizeStoreService;
            _emailService = emailService;
        }

        /// <summary>
        /// api to get prize categories
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPrizeCategories")]
        //[AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPrizeCategories()
        {
            logger.Info("GetPrizeCategories is called");

            try
            {
                PrizeCreateHelperViewModel prizeCategories = _prizeStoreService.GetPrizeCategories();
                Response response = new Response
                {
                    Status = (prizeCategories != null) ? true : false,
                    Data = prizeCategories,
                    Message = (prizeCategories != null) ? string.Empty : Constants.NO_RESULT_FOUND
                };
                logger.Info("GetPrizeCategories is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Create Prize a
        /// </summary>
        /// <param name="prize"></param>
        /// <returns></returns>
        [HttpPost]
        [CheckModelForNull]
        [Route("CreatePrize")]
        public HttpResponseMessage CreatePrize(PrizeStoreViewModel prize)
        {
            logger.Info("CreateContest is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                _prizeStoreService.CreatePrize(prize, IdentityHelper.GetUserId(), Helper.GetClientOffset());
                logger.Info("CreateContest is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(prize, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to get prize list
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPrizeStore")]
        //[AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPrizeStore(int? sortBy)
        {
            logger.Info("GetPrizeStore is called");

            try
            {
                List<PrizeListViewModel> prizeList = _prizeStoreService.GetPrizeStore(IdentityHelper.GetUserId(), Helper.GetClientOffset(), sortBy);
                Response response = new Response
                {
                    Status = (prizeList.Count() > 0) ? true : false,
                    Data = prizeList,
                    Message = (prizeList.Count > 0) ? string.Empty : Constants.NO_RESULT_FOUND
                };
                logger.Info("GetPrizeStore is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }


        /// <summary>
        /// api to get prize list
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyCreatedPrize")]
        //[AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetMyCreatedPrize(int? sortBy)
        {
            logger.Info("GetMyCreatedPrize is called");

            try
            {
                List<PrizeListViewModel> myCreatedPrizeList = _prizeStoreService.GetMyCreatedPrize(IdentityHelper.GetUserId(), Helper.GetClientOffset(), sortBy);
                Response response = new Response
                {
                    Status = (myCreatedPrizeList.Count() > 0) ? true : false,
                    Data = myCreatedPrizeList,
                    Message = (myCreatedPrizeList.Count > 0) ? string.Empty : Constants.NO_RESULT_FOUND
                };
                logger.Info("GetMyCreatedPrize is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to get prize info
        /// </summary>
        /// <param name="prizeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPrizeInfo")]
        //[AuthorizeUser(Role = "Admin,Trainer,User")]
        public HttpResponseMessage GetPrizeInfo(long prizeId)
        {
            logger.Info("GetPrizeInfo is called");

            try
            {
                PrizeStoreViewModel prizeInfo = _prizeStoreService.GetPrizeInfo(prizeId);
                Response response = new Response
                {
                    Status = (prizeInfo != null) ? true : false,
                    Data = prizeInfo,
                    Message = (prizeInfo != null) ? string.Empty : Constants.NO_RESULT_FOUND
                };
                logger.Info("GetPrizeInfo is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Update Prize 
        /// </summary>
        /// <param name="prize"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdatePrize")]
        public HttpResponseMessage UpdatePrize(PrizeStoreViewModel prize)
        {
            logger.Info("UpdatePrize is called");

            try
            {
                _prizeStoreService.UpdatePrize(prize, Helper.GetClientOffset());
                logger.Info("UpdatePrize is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(prize, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Update Prize Status 
        /// </summary>
        /// <param name="prize"></param>
        /// <returns></returns>
        [HttpPost]
        [CheckModelForNull]
        [Route("UpdatePrizeStatus")]
        public HttpResponseMessage UpdatePrizeStatus(PrizeStatusViewModel prize)
        {
            logger.Info("UpdatePrizeStatus is called");
            if (prize.PrizeId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                bool isPrizeActive = _prizeStoreService.UpdatePrizeStatus(prize);
                logger.Info("UpdatePrizeStatus is completed");
                Response response = new Response
                {
                    Status =  true,
                    Message = isPrizeActive ? Constants.PrizeActivatedSuccessfully : Constants.PrizeDeactivatedSuccessfully
                };
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(prize, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// Avail Prize 
        /// </summary>
        /// <param name="prize"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AvailPrize")]
        public HttpResponseMessage AvailPrize(PrizeStoreViewModel prize)
        {
            logger.Info("AvailPrize is called");
            if (prize.PrizeId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                AvailPrizeResponseViewModel availedPrize = _prizeStoreService.AvailPrize(prize.PrizeId, IdentityHelper.GetUserId());
                if(availedPrize.StatusType == (int)Constants.AvailPrizeStatus.SuccessfullyAvailed && availedPrize.User != null)
                {
                    _emailService.SendPrizeAvailedNotification(availedPrize);
                }
                Response response = new Response
                {
                    Status = (availedPrize.StatusType == (int)Constants.AvailPrizeStatus.SuccessfullyAvailed) ? true : false,
                    Message = (availedPrize.StatusType == (int)Constants.AvailPrizeStatus.SuccessfullyAvailed) ? Constants.PrizeSuccessfullyAvailed : 
                                                          ((availedPrize.StatusType == (int)Constants.AvailPrizeStatus.InsufficientBalance) ? Constants.InsufficientBalance : Constants.PrizeOutOfStock)
                };
                logger.Info("AvailPrize is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(prize, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }

        /// <summary>
        /// api to delete a prize
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeletePrize")]
        public HttpResponseMessage DeletePrize(long prizeId)
        {
            logger.Info("DeletePrize is called");
            if (prizeId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                bool isDeleted = _prizeStoreService.DeletePrize(prizeId);
                Response response = new Response
                {
                    Status = isDeleted ? true : false,
                    Message = isDeleted ? Constants.PRIZE_DELETED_SUCCESSFULLY : Constants.PRIZE_CANNOT_BE_DELETED
                };
                logger.Info("DeletePrize is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(prizeId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new Response() { Status = false, Message = Constants.INTERNAL_SERVER_ERROR });
            }
        }
    }
}
