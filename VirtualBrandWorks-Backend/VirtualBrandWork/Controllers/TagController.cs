﻿using Repository.Repository;
using Service;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/Tag")]
    public class TagController : ApiController
    {
        private readonly ITagService _tagService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TagController));
        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }


        /// <summary>
        /// get tags list in SelectListItem
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("get_tags")]
        public HttpResponseMessage GetTags()
        {
            logger.Info($"{Constants.GET_TAGS_LIST} is called");
            try
            {
                List<TagViewModel> tags = _tagService.GetTags();
                Response response = new Response { Data = tags, Status = true };
                logger.Info(string.Concat(Constants.GET_TAGS_LIST, " is complete"));
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// create a new tag
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("create_tag")]
        public HttpResponseMessage Create(TagViewModel tag)
        {
            logger.Info($"{Constants.CREATE_TAG} is called");
            Response response = new Response();
            if (!ModelState.IsValid)
            {
                response.Message = "One or more entered values are invalid, please check.";
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            try
            {
                bool isExists = _tagService.IsTagExists(tag);
                if (isExists)
                    response.Message = "Tag is already exists with same name";
                else
                {
                    _tagService.Create(tag);
                    response.Status = true;
                }
                logger.Info(string.Concat(Constants.CREATE_TAG, " is complete"));
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetTagSuggestion")]
        public HttpResponseMessage GetTagSuggestion(string suggestion, string type)
        {
            logger.Info("GetTagSuggestion is called");
            try
            {
                List<TagViewModel> tags = _tagService.GetTagSuggestion(type, suggestion, IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Data = tags,
                    Status = tags == null ? false : true
                };
                logger.Info("GetTagSuggestion is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetTagsWithIndustries")]
        public HttpResponseMessage GetTagsWithIndustries()
        {
            List<TagViewModel> tagsWithIndustries = _tagService.GetTagsWithIndustries();
            Response response = new Response
            {
                Data = tagsWithIndustries,
                Status = tagsWithIndustries != null
            };
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }

        [HttpPost]
        [Route("GetTag")]
        public HttpResponseMessage GetTag(TagViewModel model)
        {
            logger.Info("GetTagSuggestion is called");
            try
            {
                var tag = _tagService.GetTagById(model.TagId);
                Response response = new Response
                {
                    Data = tag,
                    Status = tag != null
                };
                logger.Info("GetTagSuggestion is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("edit_tag")]
        public HttpResponseMessage EditTag(TagViewModel model)
        {
            logger.Info("EditTag is called");
            try
            {
                _tagService.EditTag(model);
                Response response = new Response
                {
                    Status = true
                };
                logger.Info("EditTag is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("delete_tag")]
        public HttpResponseMessage DeleteTag(TagViewModel model)
        {
            logger.Info("Delete is called");
            try
            {
                _tagService.Delete(model.TagId);
                Response response = new Response
                {
                    Status = true
                };
                logger.Info("Delete is complete");
                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
