﻿using Newtonsoft.Json;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Filters;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.Controllers
{
    [JWTAuthenticationFilter]
    [RoutePrefix("api/BuildPlaylist")]
    public class BuildPlaylistController : ApiController
    {
        private readonly IBuildPlaylistService _buildPlaylistService;
        private readonly INotificationService _notificationService;
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(BuildPlaylistController));
        public BuildPlaylistController(IBuildPlaylistService buildPlaylistService, INotificationService notificationService)
        {
            _buildPlaylistService = buildPlaylistService;
            _notificationService = notificationService;
        }

        [HttpPost]
        [Route("SavePlaylistFolders")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage SavePlaylistFolders(PlaylistUpdateViewModel playlist)
        {
            logger.Info("SavePlaylistFolders is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                int result = _buildPlaylistService.SavePlaylistFolders(playlist.PlayListId, IdentityHelper.GetUserId());
                Response response = new Response();
                if (result == (int)Constants.SavePlaylistFolderStatus.PlaylistDoNotExist)
                {
                    response.Status = false;
                    response.Message = Constants.NO_RESULT_FOUND;
                }
                else if (result == (int)Constants.SavePlaylistFolderStatus.FolderCountZero)
                {
                    response.Status = false;
                    response.Message = Constants.NO_FOLDER_SELECTED;
                }
                else
                {
                    response.Status = true;
                }
                logger.Info("SavePlaylistFolders is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlist, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpGet]
        [Route("GetPlaylistFolders")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage GetPlaylistFolders(long playlistId, bool isAllContentCount)
        {
            logger.Info("GetPlaylistFolders is called");

            try
            {
                List<PlaylistFolderViewModel>  folders = _buildPlaylistService.GetPlaylistFolders(playlistId, IdentityHelper.GetUserId(), isAllContentCount);
                Response response = new Response
                {
                    Data = folders,
                    Status = folders.Count() == 0 ? false : true,
                    Message = folders.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                logger.Info("GetPlaylistFolders is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        [Route("UpdateSortOrderForPlaylistFolderUserSaved")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage UpdateSortOrderForPlaylistFolderUserSaved(PlaylistFolderUserSavedListViewModel playList)
        {
            logger.Info("UpdateSortOrderForPlaylistFolder is called");

            try
            {
                _buildPlaylistService.UpdateSortOrderForPlaylistFolderUserSaved(playList.folders);
                Response response = new Response { Status = true };
                logger.Info("UpdateSortOrderForPlaylistFolder is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playList, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("CreatePlaylist")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage CreatePlaylist(PlaylistViewModel playList)
         {
            logger.Info("CreatePlaylist is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                playList.UserId = IdentityHelper.GetUserId();
                long playlistId = _buildPlaylistService.CreatePlaylist(playList);
                Response response = new Response { Status = true , Data = playlistId };
                logger.Info("CreatePlaylist is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playList, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetPlayListContents")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetPlayListContents(long playListFolderId, bool isPlaylistExist = false)
        {
            logger.Info("GetPlayListContents is called");
            if (playListFolderId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                FolderContentViewModel folderContents = _buildPlaylistService.GetPlayListContents(playListFolderId, isPlaylistExist, IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Data = folderContents,
                    Status = folderContents == null ? false : true,
                    Message = folderContents == null ? Constants.NO_RESULT_FOUND : null
                };
                logger.Info("GetPlayListContents is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playListFolderId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdatePlayListContents")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage UpdatePlayListContents(PlaylistContentViewModel playlistContentViewModel)
        {
            logger.Info("UpdatePlayListContents is called");
            if (playlistContentViewModel.PlaylistContents.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = false, Message = Constants.ATLEAST_ONE_CONTENT_REQUIRED });
            }
            try
            {
                _buildPlaylistService.UpdatePlayListContents(playlistContentViewModel, IdentityHelper.GetUserId());
                Response response = new Response { Status = true };
                logger.Info("UpdatePlayListContents is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistContentViewModel, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetPlaylistFooterData")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        [CheckModelForNull]
        public HttpResponseMessage GetPlaylistFooterData()
        {
            logger.Info("GetPlaylistFooterData is called");
            try
            {
                List<FolderViewModel> folders = _buildPlaylistService.GetPlaylistFooterData(IdentityHelper.GetUserId());
                Response response = new Response
                {
                    Data = folders,
                    Status = folders.Count() == 0 ? false : true,
                    Message = folders.Count() == 0 ? Constants.NO_RESULT_FOUND : null
                };
                logger.Info("GetPlaylistFooterData is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(IdentityHelper.GetUserId(), Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("SaveBuildPlaylistStatus")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage SaveBuildPlaylistStatus(BuildPlaylistSavedStateViewModel state)
        {
            logger.Info("SaveBuildPlaylistStatus is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                state.UserId = IdentityHelper.GetUserId();
                _buildPlaylistService.SaveBuildPlaylistStatus(state);
                logger.Info("SaveSelectedPlaylistContent is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(state, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetBuildPlaylistSavedState")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetBuildPlaylistSavedState(long playlistId)
        {
            logger.Info("GetBuildPlaylistSavedState is called");
            try
            {
                BuildPlaylistSavedStateViewModel buildPlaylistSavedState = _buildPlaylistService.GetBuildPlaylistSavedState(playlistId,IdentityHelper.GetUserId());
                logger.Info("GetBuildPlaylistSavedState is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true, Data = buildPlaylistSavedState });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(IdentityHelper.GetUserId(), Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("AssignPlaylistTo")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage AssignPlaylistTo(PlaylistAssignToViewModel playlistDetail)
        {
            logger.Info("AssignPlaylistTo is called");

            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
                
            try
            {
                playlistDetail.UserId = IdentityHelper.GetUserId();
                List<long> usersToBeNotified = _buildPlaylistService.AssignPlaylistTo(playlistDetail, Helper.GetClientOffset());
                PlaylistDetailViewModel objPlaylistDetail = _buildPlaylistService.GetPlaylistDetail(playlistDetail.PlayListId);
                _notificationService.SendNotifications(usersToBeNotified, string.Format(Constants.PlayListAssigned, objPlaylistDetail.Name, playlistDetail.PlayListId), Constants.PlayListAssignedTitle);

                logger.Info("AssignPlaylistTo is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistDetail, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetPlaylistDetail")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetPlaylistDetail(long playlistId)
        {
            logger.Info("GetPlaylistDetail is called");

            if (playlistId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }

            try
            {
                PlaylistDetailViewModel playlist =_buildPlaylistService.GetPlaylistDetail(playlistId);
                logger.Info("GetPlaylistDetail is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = playlist, Status = playlist == null ? false : true});
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetPlaylistAssignedUserAndTeam")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetPlaylistAssignedUserAndTeam(long playlistId)
        {
            logger.Info("GetPlaylistAssignedUserAndTeam is called");

            if (playlistId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }

            try
            {
                PlaylistAssignedUserOrTeam assignedUsers = _buildPlaylistService.GetPlaylistAssignedUserAndTeam(playlistId);
                logger.Info("GetPlaylistAssignedUserAndTeam is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = assignedUsers, Status = assignedUsers == null ? false : true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("LaunchPlaylist")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage LaunchPlaylist(PlaylistUpdateViewModel playlist)
        {
            logger.Info("LaunchPlaylist is called");

            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }
            try
            {
                playlist.UserId = IdentityHelper.GetUserId();
                List<long> objPlaylistAssignedUserOrTeam =  _buildPlaylistService.LaunchPlaylist(playlist);
                PlaylistDetailViewModel objPlaylistDetail = _buildPlaylistService.GetPlaylistDetail(playlist.PlayListId);
                _notificationService.SendNotifications(objPlaylistAssignedUserOrTeam.Distinct().ToList(), string.Format(Constants.PlayListAssigned,objPlaylistDetail.Name,playlist.PlayListId), Constants.PlayListAssignedTitle);
                logger.Info("LaunchPlaylist is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlist, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetUsersAndTeamsForAssignPlaylistToMore")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetUsersAndTeamsForAssignPlaylistToMore(long playlistId, int locationId)
        {
            logger.Info("GetUsersAndTeamsForAssignPlaylistToMore is called");

            if (playlistId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
             
            try
            {
                long userId = IdentityHelper.GetUserId();
                PlaylistAssignedUserOrTeam usersAndTeams = _buildPlaylistService.GetUsersAndTeamsForAssignPlaylistToMore(playlistId, locationId, userId);
                logger.Info("GetUsersAndTeamsForAssignPlaylistToMore is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = usersAndTeams, Status = true });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpGet]
        [Route("GetPlaylistForUpdate")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage GetPlaylistForUpdate(long playlistId)
        {
            logger.Info("GetPlaylistForUpdate is called");

            try
            {
                PlaylistViewModel playlist = _buildPlaylistService.GetPlaylistForUpdate(playlistId, IdentityHelper.GetUserId());
                Response response = new Response { Data = playlist, Status = true };
                logger.Info("GetPlaylistForUpdate is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetPlayListContentsForAddQuestion")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        public HttpResponseMessage GetPlayListContentsForAddQuestion(long playListFolderId)
        {
            logger.Info("GetPlaylistAssignedUserAndTeam is called");

            if (playListFolderId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }

            try
            {
                List<PlaylistContentResponseViewModel> contents = _buildPlaylistService.GetPlayListContentsForAddQuestion(playListFolderId, IdentityHelper.GetUserId());
                logger.Info("GetPlaylistAssignedUserAndTeam is completed");
                return Request.CreateResponse(HttpStatusCode.OK, new Response { Data = contents, Status = contents == null ? false : true, Message = contents == null? Constants.NO_RESULT_FOUND : null });
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playListFolderId, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdateSortOrderForPlaylistFolder")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage UpdateSortOrderForPlaylistFolder(PlaylistFolderSortViewModel playlistFolder)
        {
            logger.Info("UpdateSortOrderForPlaylistFolder is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response() { Message = Helper.GetErrorMessages(ModelState) });
            }

            try
            {
                _buildPlaylistService.UpdateSortOrderForPlaylistFolder(playlistFolder.playlistId, playlistFolder.playlistFolderId, playlistFolder.destinationSortOrder);
                Response response = new Response { Status = true };
                logger.Info("UpdateSortOrderForPlaylistFolder is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playlistFolder, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("TurnOffExistingQuiz")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage TurnOffExistingQuiz(TurnOffExistingQuizViewModel turnOffExistingQuiz)
        {
            logger.Info("TurnOffExistingQuiz is called");
            if (turnOffExistingQuiz.PlaylistContentMasterId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }

            try
            {
                _buildPlaylistService.TurnOffExistingQuiz(turnOffExistingQuiz.PlaylistContentMasterId, turnOffExistingQuiz.IsTurnedOffExistingQuiz);
                Response response = new Response { Status = true };
                logger.Info("TurnOffExistingQuiz is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(turnOffExistingQuiz, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("TurnOffExistingQuizForAll")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage TurnOffExistingQuizForAll(TurnOffExistingQuizViewModel turnOffExistingQuiz)
        {
            logger.Info("TurnOffExistingQuizForAll is called");
            if (turnOffExistingQuiz.PlaylistFolderId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }

            try
            {
                _buildPlaylistService.TurnOffExistingQuizForAll(turnOffExistingQuiz.PlaylistFolderId, turnOffExistingQuiz.IsTurnedOffExistingQuiz);
                Response response = new Response { Status = true };
                logger.Info("TurnOffExistingQuizForAll is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(turnOffExistingQuiz, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("UpdatePlaylist")]
        [AuthorizeUser(Role = "Admin,Trainer")]
        //[CheckModelForNull]
        public HttpResponseMessage UpdatePlaylist(PlaylistNameAndDescriptionUpdateViewModel playList)
        {
            logger.Info("UpdatePlaylist is called");
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.INVALID_ARGUMENT);
            }
            try
            {
                playList.UserId = IdentityHelper.GetUserId();
                _buildPlaylistService.UpdatePlaylist(playList);
                Response response = new Response { Status = true };
                logger.Info("UpdatePlaylist is completed");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message} {JsonConvert.SerializeObject(playList, Formatting.Indented)}", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
