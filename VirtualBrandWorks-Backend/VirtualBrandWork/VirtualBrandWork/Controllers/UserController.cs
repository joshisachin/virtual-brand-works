﻿using Repository.Interfaces;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Models;

namespace VirtualBrandWork.Controllers
{
    [Authorize]
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private readonly IUserRepository _UserRepository;
        public UserController()
        {
        }
        public UserController(IUserRepository userRepository)
        {
            _UserRepository = userRepository;
        }

        /// <summary>
        /// login and get the user details from the database
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("GetUser")]
        public async Task<HttpResponseMessage> Get(LoginViewModel model)
        {
            HttpRequestHeaders headers = this.Request.Headers;
            try
            {
                using (UserService _repo = new UserService(_UserRepository))
                {
                    var result = _repo.GetUser(model.Email, model.Password);
                    if (result != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
                // return Request.CreateResponse(HttpStatusCode.InternalServerError, Helper.SetResponse(message: Resource.SomeErrorOccuredTryAgainLater));
            }
        }
    }
}
