﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Models;
using VirtualBrandWorks.common;

namespace VirtualBrandWork.UIControllers
{
    public class AccountController : Controller
    {

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            // //var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: true);


            var getTokenUrl = string.Format("http://localhost:49699");

            using (HttpClient httpClient = new HttpClient())
            {
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", model.Email),
                    new KeyValuePair<string, string>("password", model.Password)
                });

                HttpResponseMessage result = httpClient.PostAsync(getTokenUrl + "/Token", content).Result;

                string resultContent = result.Content.ReadAsStringAsync().Result;

                var token = JsonConvert.DeserializeObject<Token>(resultContent);
                if (token.access_token != null)
                {
                    AuthenticationProperties options = new AuthenticationProperties();

                    options.AllowRefresh = true;
                    options.IsPersistent = true;
                    options.ExpiresUtc = DateTime.UtcNow.AddSeconds(int.Parse(token.expires_in));

                    var claims = new[]
                    {
                    new Claim(ClaimTypes.Name, model.Email),
                    new Claim("AcessToken", string.Format("Bearer {0}", token.access_token)),
                };

                    var identity = new ClaimsIdentity(claims, "ApplicationCookie");

                    //// need to call the API to access the user data
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.access_token);
                    HttpResponseMessage response = httpClient.GetAsync(getTokenUrl + "/api/Account/UserInfo").Result;
                    string message = response.Content.ReadAsStringAsync().Result;
                    if (response.IsSuccessStatusCode)
                    {
                        Request.GetOwinContext().Authentication.SignIn(options, identity);
                        return RedirectToAction("Index", "Home");
                    }
                    //System.Console.WriteLine("URL responese : " + message);
                    return RedirectToAction("Login");
                }
                else
                {
                    return RedirectToAction("Login");
                    // return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid AccessToken");
                }
            }
        }

        public ActionResult LogOut()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");

            return RedirectToAction("Login");
        }

        //// POST: Account/Create
        //[HttpPost]
        //public ActionResult Register( user)
        //{
        //    try
        //    {
        //        //ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
        //        using (var client = new HttpClient())
        //        {
        //            var uri = "http://localhost:49699/api/Account";
        //            //Passing service base url  
        //            client.BaseAddress = new Uri("http://localhost:49699/api/Account");

        //            client.DefaultRequestHeaders.Clear();
        //            //Define request data format  

        //            HttpContent inputContent = new StringContent(user.ToString(), Encoding.UTF8, "application/json");

        //            //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //            HttpResponseMessage response = client.PostAsJsonAsync(uri + "/Register", user).Result;

        //            //Checking the response is successful or not which is sent using HttpClient  
        //            if (response.IsSuccessStatusCode)
        //            {
        //                //Storing the response details recieved from web api   
        //                var EmpResponse = response.Content.ReadAsStringAsync().Result;

        //                //Deserializing the response recieved from web api and storing into the Employee list  
        //                //EmpInfo = JsonConvert.DeserializeObject<List<Employee>>(EmpResponse);

        //            }
        //            //returning the employee list to view  
        //            return RedirectToAction("Login");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return View();
        //    }
        //}
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
