﻿using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VirtualBrandWork.Entity;

namespace VirtualBrandWork.UIControllers
{
    public class UserTagController : Controller
    {
        private readonly IUserTagService _userTagService;
        public UserTagController(IUserTagService userTagService)
        {
            _userTagService = userTagService;
        }
        // GET: UserTag/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserTag/Create
        [HttpPost]
        public ActionResult Create(UserTag model)
        {
            try
            {
                _userTagService.Add(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

    }
}
