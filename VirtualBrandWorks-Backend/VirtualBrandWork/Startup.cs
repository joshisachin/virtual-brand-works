﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using VirtualBrandWork.Models;
using System.Web.Configuration;

[assembly: OwinStartup(typeof(VirtualBrandWork.Startup))]

namespace VirtualBrandWork
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseWebApi(config);

        }
    }
}
