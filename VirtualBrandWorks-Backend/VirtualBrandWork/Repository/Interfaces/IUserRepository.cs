﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;

namespace Repository.Interfaces
{
    public interface IUserRepository
    {
        User GetUser(string username, string password);
    }
}
