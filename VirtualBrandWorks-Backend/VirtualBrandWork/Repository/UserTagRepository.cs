﻿using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;

namespace Repository
{
    public class UserTagRepository : IUserTagRepository
    {
        public void Add(UserTag tag)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                dbContext.UserTags.Add(tag);
                dbContext.SaveChanges();
            }
        }
    }
}
