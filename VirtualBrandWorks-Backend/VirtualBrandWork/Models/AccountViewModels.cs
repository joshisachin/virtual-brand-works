﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VirtualBrandWork.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
        public string Email { get; set; }
        public string AccessToken { get; set; }
        public string Provider { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Username cannot be left blank.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "You have entered an invalid email.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password cannot be left blank")]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = "Please enter a valid password")]
        [MaxLength(15, ErrorMessage = "Password can't be more than 15 characters"), MinLength(8, ErrorMessage = "Password can't be less than 8 characters")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }

        public string SaLoginRequestKey { get; set; }

        public string FCMToken { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Email cannot be left blank")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Please enter a valid email format")]
        public string EmailId { get;set;}
    }

    public class GoogleUserInfo
    {
        public string id { get; set; }
        public string email { get; set; }
        public string verified_email { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string picture { get; set; }
        public string locale { get; set; }
    }
}
