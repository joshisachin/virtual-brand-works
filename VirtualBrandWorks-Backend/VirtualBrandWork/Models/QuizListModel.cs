﻿using System.Collections.Generic;

namespace VirtualBrandWork.Models
{
    public class QuizListModel
    {
        public int CategoryId { get; set; }
        public int FolderId { get; set; }
        public List<ContentModel> ContentModelCollection { get; set; }
        public List<CategoryModel> CategoryModelCollection { get; set; }
    }

    public class QuizModel
    {
        public int QuizId { get; set; }
        public string QuizName { get; set; }
    }

    public class CategoryModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class FolderModel
    {
        public int FolderId { get; set; }
        public string FolderName { get; set; }
    }

    public class ContentModel
    {
        public int ContentId { get; set; }
        public string ContentName { get; set; }
        public bool IsQuestionAdded { get; set; }
        public bool IsTurnOffExistingQuiz { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ShortName { get; set; }
    }
}