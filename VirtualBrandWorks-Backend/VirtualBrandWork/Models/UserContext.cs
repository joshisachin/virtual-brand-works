﻿namespace VirtualBrandWork.Models
{
    public class UserViewModel
    {
        public long UserId { get; set; }
        public string JWTToken { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }

    }
}