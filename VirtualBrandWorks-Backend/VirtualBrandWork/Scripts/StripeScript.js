// Create a Checkout Session with the selected plan ID
var createCheckoutSession = function (pId) {
    return fetch("../Payment/CreateCheckoutSession", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            planId: pId,
            email: $("#hdnEmail").val(),
            subscription: $("#hdnSubscripton").val(),
            amount: $("#hdnPlanPrice").val(),
            billingCycle: $("#hdnPlanType").val(),
            firstName: $("#hdnFirstName").val(),
            LastName: $("#hdnLastName").val()
        })
    }).then(result =>result.json())
    .then(function (data) {
        return data;
    });
};

// Handle any errors returned from Checkout
var handleResult = function (result) {
    if (result.error) {
        var displayError = document.getElementById("error-message");
        displayError.textContent = result.error.message;
    }
};

/* Get your Stripe publishable key to initialize Stripe.js */
// Setup event handler to create a Checkout Session when button is clicked

$(document).ready(function () {
    if (document.getElementById("btnPay") != null) {
        document
       .getElementById("btnPay")
       .addEventListener("click", function (evt) {
           fetch("../Payment/Setup", {
               method: "POST",
               headers: {
                   "Content-Type": "application/json"
               },
               body: JSON.stringify({
                   planId: $("#hdnSelectedPlanPrice").val()
               })
           })
        .then(result =>result.json())
        .then(function (json) {
            //debugger;
            var publishableKey = json.PublishableKey;
            var planId = json.PlanId;
            var stripe = Stripe(publishableKey);

            createCheckoutSession(planId).then(function (data) {
                // Call Stripe.js method to redirect to the new Checkout page
                stripe
                  .redirectToCheckout({
                      sessionId: data.SessionId
                  })
                  .then(handleResult);
            });
        });
       });
    }

    if (document.getElementById("btnSave") != null) {
        document
       .getElementById("btnSave")
       .addEventListener("click", function (evt) {
           fetch("../Payment/Save", {
               method: "POST",
               headers: {
                   "Content-Type": "application/json"
               },
               body: JSON.stringify({
                   email: $("#hdnEmail").val(),
                   firstName: $("#hdnFirstName").val(),
                   LastName: $("#hdnLastName").val()
               })
           })
        .then(result =>result.json())
        .then(function (json) {
            if (json.Status) {
                window.location = "../Account/Login";
            }
        });
       });
    }
});


