﻿using AutoMapper;
using Repository.Model;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap <CategoryViewModel, Category>();
            CreateMap<MasterFolderViewModel, MasterFolder>();
            CreateMap<MasterFolder, MasterFolderViewModel>();
            CreateMap<SubscriptionViewModel, Subscription>();
            CreateMap<Subscription, SubscriptionViewModel>();
            CreateMap<MasterContent, MasterContentViewModel>();
            CreateMap<MasterContentViewModel, MasterContent>();
            CreateMap<User, UserViewModel>();
            CreateMap<UserViewModel, User>();
            CreateMap<TagViewModel, Tag>()
                .ForMember(x => x.IndustryId, opt => opt.MapFrom(src => src.CategoryId));
            CreateMap<Tag, TagViewModel>()
                .ForMember(x => x.CategoryId, opt => opt.MapFrom(src => src.IndustryId));
            CreateMap<Role, RoleViewModel>();
            CreateMap<RoleViewModel, Role>();
            CreateMap<UserInvite, UserInviteViewModel>();
            CreateMap<UserInviteViewModel, UserInvite>();
            CreateMap<Team, TeamListViewModel>();
            CreateMap<TeamListViewModel, Team>();
            CreateMap<CompanyFolderViewModel, CompanyLibraryFolder>();
            CreateMap<CompanyLibraryFolder, CompanyFolderViewModel>();
            CreateMap<CompanyContentViewModel, CompanyLibraryContent>();
            CreateMap<CompanyLibraryContent, CompanyContentViewModel>();
            CreateMap<PlaylistFolderUserSaved, PlaylistFolderUserSavedViewModel>();
            CreateMap<PlaylistFolderUserSavedViewModel, PlaylistFolderUserSaved>();
            CreateMap<PlaylistLeaderboardViewModel, LeaderboardViewModel>()
                .ForMember(x => x.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(x => x.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(x => x.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(x => x.PlaylistCompletePercentage, opt => opt.MapFrom(src => src.PlaylistCompletePercentage))
                .ForMember(x => x.Color, opt => opt.MapFrom(src => src.Color));
            CreateMap<VisibilityControlContentViewModel, BroadcastedContentViewModel>()
                .ForMember(x => x.ContentId, opt => opt.MapFrom(src => src.ContentId))
                .ForMember(x => x.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(x => x.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(x => x.ProfilePicture, opt => opt.MapFrom(src => src.ProfilePicture))
                .ForMember(x => x.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(x => x.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(x => x.ContentUrl, opt => opt.MapFrom(src => src.ContentUrl))
                .ForMember(x => x.MimeType, opt => opt.MapFrom(src => src.MimeType))
                .ForMember(x => x.Likes, opt => opt.MapFrom(src => src.Likes))
                .ForMember(x => x.Views, opt => opt.MapFrom(src => src.Views))
                .ForMember(x => x.IsAwsContent, opt => opt.MapFrom(src => src.IsAwsContent))
                .ForMember(x => x.IsCompanyLibraryContent, opt => opt.MapFrom(src => src.IsCompanyLibraryContent));
            CreateMap<Locations, LocationsViewModel>();
            CreateMap<State, StateViewModel>();
            CreateMap<StateViewModel, State>();
            CreateMap<City, CityViewModel>();
            CreateMap<CityViewModel, City>();
        }
    }
}