﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace VirtualBrandWork.Common
{
    public class APIClient
    {
        public T Get<T>(Uri endpoint, Dictionary<string, string> headerParams) where T : class
        {
            return ProcessRequest<T>(endpoint, "GET", headerParams);
        }

        public T Post<T>(Uri endpoint, Dictionary<string, string> headerParams, string data) where T : class
        {
            string messageJson = Query(endpoint, "POST", headerParams, data);

            T messageObject = JsonConvert.DeserializeObject<T>(messageJson);
            return messageObject;
        }

        public T Delete<T>(Uri endpoint, Dictionary<string, string> headerParams, string data = null) where T : class
        {
            string messageJson = Query(endpoint, "DELETE", headerParams, data);
            T messageObject = JsonConvert.DeserializeObject<T>(messageJson);
            return messageObject;
        }
        private T ProcessRequest<T>(Uri endpoint, string methodType, Dictionary<string, string> headerParams) where T : class
        {
            string messageJson = Query(endpoint, methodType, headerParams);
            T messageObject = JsonConvert.DeserializeObject<T>(messageJson);
            return messageObject;
        }

        private string Query(Uri endpoint, string method, Dictionary<string, string> headerParams, string paramData = "")
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            webRequest.Method = method;

            SetHeaders(headerParams, webRequest);

            try
            {
                if (!string.IsNullOrEmpty(paramData))
                {
                    byte[] data = Encoding.UTF8.GetBytes(paramData);
                    using (Stream stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }

                using (WebResponse webResponse = webRequest.GetResponse())
                using (Stream str = webResponse.GetResponseStream())
                using (StreamReader sr = new StreamReader(str))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                using (HttpWebResponse response = (HttpWebResponse)wex.Response)
                {
                    if (response == null)
                    {
                        throw;
                    }

                    using (Stream str = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(str))
                        {
                            throw new Exception(sr.ReadToEnd());
                        }
                    }
                }
            }
        }

        private void SetHeaders(Dictionary<string, string> headerParams, HttpWebRequest webRequest)
        {
            webRequest.ContentType = "application/json";
            if (headerParams != null)
            {
                foreach (KeyValuePair<string, string> header in headerParams)
                {
                    webRequest.Headers.Add(header.Key, header.Value);
                }
            }
        }
    }
}
