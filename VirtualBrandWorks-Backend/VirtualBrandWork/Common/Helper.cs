﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.ModelBinding;
using VirtualBrandWork.Models;

namespace VirtualBrandWorks.common
{
    public static class Helper
    {
        public static readonly HttpClient _client = new HttpClient();

        public static HttpClient GetHttpClient()
        {
            return _client;
        }

        //internal static object GetUserToken(LoginViewModel model)
        //{
        //    var getTokenUrl = string.Format("http://localhost:49699/Token");

        //    using (HttpClient httpClient = new HttpClient())
        //    {
        //        HttpContent content = new FormUrlEncodedContent(new[]
        //        {
        //            new KeyValuePair<string, string>("grant_type", "password"),
        //            new KeyValuePair<string, string>("username", model.Email),
        //            new KeyValuePair<string, string>("password", model.Password)
        //        });

        //        HttpResponseMessage result = httpClient.PostAsync(getTokenUrl, content).Result;

        //        string resultContent = result.Content.ReadAsStringAsync().Result;

        //        var token = JsonConvert.DeserializeObject<Token>(resultContent);

        //        var token = Helper.GetUserToken(model);

        //        AuthenticationProperties options = new AuthenticationProperties();

        //        options.AllowRefresh = true;
        //        options.IsPersistent = true;
        //        options.ExpiresUtc = DateTime.UtcNow.AddSeconds(int.Parse(token.expires_in));

        //        var claims = new[]
        //        {
        //            new Claim(ClaimTypes.Name, model.Email),
        //            new Claim("AcessToken", string.Format("Bearer {0}", token.access_token)),
        //        };

        //        var identity = new ClaimsIdentity(claims, "ApplicationCookie");

        //        Request.GetOwinContext().Authentication.SignIn(options, identity);
        //        //return token;
        //    }
        //}

        internal static string GetErrorMessages(ModelStateDictionary modelState)
        {
            
            var query = from state in modelState.Values
                        from error in state.Errors
                        where error.Exception != null
                        select error.Exception.Message;

            if(query.Count() == 0)
            {
                query = from state in modelState.Values
                        from error in state.Errors
                        where error.ErrorMessage != null
                        select error.ErrorMessage;
            }

            var errorList = string.Join("\n", query.ToList());
            return errorList;
        }

        internal static string GetClientIp()
        {
            //if (HttpContext.Current != null)
            //{
            //    return HttpContext.Current.Request.UserHostAddress;
            //}
            //else
            //{
            //    return null;
            //}

            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            if (IP4Address != String.Empty)
            {
                return IP4Address;
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            return IP4Address;
        }

        internal static double GetClientOffset()
        {
            return Convert.ToDouble(HttpContext.Current.Request.Headers["Offset"]);
        }
    }


}