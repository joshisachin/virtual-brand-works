﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using VirtualBrandWork.Filters;

namespace VirtualBrandWork.Common
{
    public class IdentityHelper
    {
        public static long GetUserId() {
            return (Thread.CurrentPrincipal.Identity as JWTAuthenticationIdentity).UserId;
        }

        public static string GetUserName()
        {
            return (Thread.CurrentPrincipal.Identity as JWTAuthenticationIdentity).UserName;
        }
    }
}