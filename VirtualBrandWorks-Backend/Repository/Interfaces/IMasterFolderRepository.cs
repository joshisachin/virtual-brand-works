﻿using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface IMasterFolderRepository
    {
        List<MasterFolderViewModel> GetAllFolders();
        List<FolderViewModel> GetFilterFolders(CompanyContentFolderRequestViewModel request);
        bool IsFolderExists(MasterFolderViewModel folderViewModel);
        Response Create(MasterFolderViewModel folderViewModel);
        void Update(MasterFolderViewModel folderViewModel);
        Response Delete(long folderId);
        MasterFolderViewModel GetFolder(long folderId);
        Response GetFoldersByCategoyId(long categoryId);
       bool IsFolderShortNameExists(MasterFolderViewModel folderViewModel);
    }
}
