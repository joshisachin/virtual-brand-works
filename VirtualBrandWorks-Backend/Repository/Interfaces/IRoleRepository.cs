﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface IRoleRepository
    {
        List<RoleViewModel> GetRoles();
    }
}
