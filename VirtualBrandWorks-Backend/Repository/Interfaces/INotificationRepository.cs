﻿using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface INotificationRepository
    {
        NotificationTypeViewModel GetNotificationData(Constants.NotificationType notificationType);

        List<FCMUserToken> GetFcmTokens(long[] userIds);
        void SaveNotificationData(long userId, long? notificationTypeId, bool isSuccess, string token, string messageId, string errorMessage, string multicastId, string body, string title);
        void SaveFcmToken(long userId, string token);
        List<Notifications> GetNotifications(long userId);
        void ReadNotifications(long userId);
    }
}
