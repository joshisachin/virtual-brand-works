﻿using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface IUserRepository
    {
        UserContext ValidateUser(string username);
        UserViewModel ValidateUserWithEmail(string emailId);

        //method to validate a user using email
        UserContext ValidateExternalLogin(string email);
        bool SetPasswordResetCode(UserViewModel user, string resetCode);
        int ResetPassword(ResetPasswordViewModel user);
        bool ChangePassword(ChangePasswordViewModel changePassword, int userId);
        bool ValidatePasswordResetCode(string resetCode);
        bool ValidateCompanyStatus(long userId);
        UserInviteViewModel GetUserInvite(string email);
        void SignUpAsUser(SignUpViewModel signUpUser);
        List<ValidationSummaryViewModel> ValidateUserEmailAndCompanyName(SignUpViewModel signUpViewModel);
        Response SaveAdminDetails(SignUpViewModel signUpViewModel);
        Response DeleteSignUpDetails(string email);
        Response UpdateSignUpDetailsPaymentSuccess(string email, string customerId, string subscriptionId, bool isTrial);
        void ChangeSubscriptionPlan(long userId, long planId);
        void CancelSubscriptionPlan(long userId, long planId);
        string GetCustomerId(long userId);
        bool UserLoginStatus(long userId, string statusCode);
        bool RemoveLoginStatus(long userId, string statusCode);
        void SaveUsersLoginStatus(string email, string statusCode);
        UserContext GetUserContextFromUserId(long userId);
        string GetEmailIdFromUserId(long userId);
        long GetCompanyIdFromUserId(long userId);
        string GetSubscriptionFromPlanPriceId(long planId);
        UserMyPlanNotiFicationViewModel GetUserNameAndEmail(long userId);
        long LimitActiveUsersAsPerPlan(long userId, long planId);
        SubscriptionPriceDetailViewModel GetSubscriptionDetailsFromPlanPriceId(long planId);
        UserViewModel ValidateUserWithHandleName(string hanadleName);
    }
}
