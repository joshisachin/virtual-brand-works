﻿using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface IMasterContentRepository
    {
        List<MasterContentViewModel> GetAllContents();
        MasterContentViewModel GetContent(long contentId);
        void Create(MasterContentViewModel content);
        void Update(MasterContentViewModel content);
        void Delete(long contentId);
        List<ContentViewModel> GetMasterContentsByFolderId(long masterFolderId);
        int PublishUnpublishContent(long id);
        ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long masterFolderId, long userId);
        FolderContentViewModel GetMasterFolderContents(FolderViewModel folder, long userId);
        void SaveSelectedMasterContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId);
        List<FolderViewModel> GetMasterContentSelectedFolders(long userId);
        void UpdateMasterContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId);
        FolderContentDetailViewModel GetMasterFolderContentDetail(ContentViewModel requestContent, long userId);
        List<ContentViewModel> GetMasterContentsByType(FolderContentRequestViewModel folder);
    }
}
