﻿using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface ICategoriesRepository
    {
        List<CategoryViewModel> GetAllCategories();
        bool IsCategoryExists(CategoryViewModel categoryId);
        void Create(CategoryViewModel category);
        CategoryViewModel GetCategory(long categoryId);
        void Update(CategoryViewModel category);
        Response Delete(long categoryId);
        bool IsAssociatedCompany(long categoryId);

    }
}
