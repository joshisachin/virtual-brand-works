﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface ICompanyContentRepository
    {
        List<FolderViewModel> GetCompanyContentFolders(CompanyContentFolderRequestViewModel request);
        //method to create content for a company
        ContentResponseViewModel CreateContent(CompanyContentViewModel companyContent);
        CompanyContentResponse GetContentConfirmation(ContentViewModel companyContent);
        FolderContentDetailViewModel GetCompanyFolderContentDetail(ContentViewModel requestContent, long userId);
        QuizViewModel GetQuizDetails(long contentId);
        FolderDetailViewModel CreateFolder(CompanyFolderViewModel folderViewModel);
        void UpdateFolder(CompanyFolderViewModel folderViewModel);
        bool IsCompanyFolderExists(CompanyFolderViewModel folderViewModel);
        bool IsCompanyFolderShortNameExists(CompanyFolderViewModel folderViewModel);
        FolderContentViewModel GetCompanyFolderContents(FolderViewModel folder, long userId);
        void SaveSelectedCompanyContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId);
        List<FolderViewModel> GetCompanyContentSelectedFolders(long userId);
        void UpdateCompanyContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId);
        ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long folderId);
        void PublishCompanyContent(ContentViewModel content);
        List<FolderDetailViewModel> GetCompanyFolderList(long userId);
        List<ContentViewModel> GetCompanyContentsByType(FolderContentRequestViewModel folder);
        //method for user to see the list of created content
        List<ContentViewModel> GetUserCreatedContentList(long userId);

        // method to delete company content which is not in use - connected to a playlist
        bool DeleteCompanyContent(long contentId);
        bool BroadCastCompanyContent(ContentViewModel content);
        void CompanyContentView(ContentViewModel requestContent, long userId);
        //update visibility level of company content
        void UpdateCompanyContentVisibility(CompanyContentVisibilityViewModel content);
    }
}
