﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using static VirtualBrandWork.Entity.Constants;

namespace Repository.Interfaces
{
    public interface IVBPlayRepository
    {
        List<VBPlayViewModel> GetVBPlayContest(long userId, ContestType contestType);
        VBPlayViewModel CreateContest(VBPlayCreateViewModel vbPlayContest, long userId);
        long JoinContest(VBPlayViewModel vbPlayViewModel, long userId);
        VBPlayViewModel GetContest(long contestId);
        ContestChallengeAndEntriesViewModel ViewContest(long userId, long contestId, string search);
        VBPlayViewModel GetContestConfirmation(long contestId);
        ChallengeEntryViewModel GetContestEntryConfirmation(long contestEntryId);
        void BroadcastContest(ContestBroadcastRequestViewModel contest);
        List<BroadcastedContentViewModel> GetBroadcastedContent(string search, long userId);
        long LikeContest(VBPlayContentCommonViewModel contestContent, long userId);
        void ContestContentViews(SharedContentViewModel contest);
        VBPlayViewModel SubmitContestEntry(ContestEntrySubmitViewModel contestEntry);
        ChallengeEntryViewModel GetContestEntry(long contestEntryId);
        ChallengeEntryViewModel UpdateContestEntry(VBPlayCreateViewModel contestEntry);
        string GetContestContentUrl(VBPlayCreateViewModel contestEntry);
        BroadcastedContentViewModel GetSharedContent(long contentId, Constants.SharedContentType contentType, long userId);
        //method to get the info of earned/reedemed points
        EarnedPointsViewModel GetEarnedPoints(long userId);
        //delete a contest 
        bool DeleteContest(VBPlayViewModel contest);
        void ExtendContentDate(long contestId, long notificationId, string type);
        void OptOutNotification(long notificationId);
        //update visibility level for a contest content
        void UpdateVbPlayContentVisibility(ContestVisibilityViewModel contest);
        VBPlayViewModel SubmitContest(ContestEntrySubmitViewModel contest);
        string GetContestUrl(VBPlayUpdateViewModel contest);
        VBPlayViewModel UpdateContest(VBPlayUpdateViewModel vbPlayContest);
        List<ReportAbuseViewModel> GetAllReportAbuse(long userId);
        void ReportAbuse(long contestId, long userId);
        void DeleteReportAbuse(long contestId);
        void SetNotificationExtenstionFlag(BroadcastedContentViewModel broadcastedContent, bool flag);
        List<long> GetUserIdsOfAdminAndTrainerOfACompany(long userId);
        bool IsContestAlreadyReported(long contestId, long userId);
    }
}
