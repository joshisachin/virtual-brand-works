﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface ITagRepository
    {
        List<TagViewModel> GetTags();
        void Create(TagViewModel tag);
        bool IsTagExists(TagViewModel tag);
        List<TagViewModel> GetTagSuggestion(string type, string suggestion, long userId);
        List<TagViewModel> GetTagsWithIndustries();
        void EditTag(TagViewModel tagViewModel);
        TagViewModel GetTagById(long tagId);
        void Delete(long tagId);
    }
}
