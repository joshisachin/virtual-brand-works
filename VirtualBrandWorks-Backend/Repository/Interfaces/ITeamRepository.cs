﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface ITeamRepository
    {
        //method to get list of teams within a company
        List<TeamListViewModel> GetTeamList(TeamListViewModel team);
        //method to get the detail of a team along with the list of users and associated tags
        TeamDetailViewModel GetTeamDetail(TeamDetailViewModel team);
        //method to get only trainers of a company
        List<TrainerListViewModel> GetTrainers(long UserId);
        //method to create a team for a company
        void CreateTeam(CreateTeamViewModel team);
        //method to retrieve the list of users for a company
        List<UserListViewModel> GetUsers(UserListRequestViewModel request);
        List<long> UpdateTeam(TeamDetailViewModel team);
        void DeleteTeam(long teamId);
        bool ValidateTeamHandleName(string name);
    }
}
