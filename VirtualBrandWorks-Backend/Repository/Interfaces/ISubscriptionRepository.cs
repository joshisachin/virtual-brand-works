﻿using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface ISubscriptionRepository
    {
        IEnumerable<SubscriptionViewModel> GetSubscriptionList();
        void Create(SubscriptionViewModel subscriptionViewModel);
        SubscriptionViewModel GetSubscriptionById(SubscriptionViewModel subscription);
        void Update(SubscriptionViewModel subscriptionViewModel);
        void Delete(int subscriptionId);
        List<SubscriptionViewModel> GetSubscriptionListForAdmin();
        List<SubscriptionPlanPriceViewModel> GetSubscriptionListForAdmin(string productId);
        CompanySubscriptionViewModel GetSubscriptionDetailsForCompany(long companyId);
        string GetStripeSubscriptionId(long userId);
        List<SubscribedUserViewModel> GetSubscribedUsersForSubscription(long planId);
    }
}
