﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface IPrizeStoreRepository
    {
        PrizeCreateHelperViewModel GetPrizeCategories();
        void CreatePrize(PrizeStoreViewModel prize, long userId);
        List<PrizeListViewModel> GetPrizeStore(long userId);
        List<PrizeListViewModel> GetMyCreatedPrize(long userId);
        PrizeStoreViewModel GetPrizeInfo(long prizeId);
        string UpdatePrize(PrizeStoreViewModel prize);
       // string GetOldThumbnailForPrize(long prizeId);
        bool UpdatePrizeStatus(PrizeStatusViewModel prize);
        AvailPrizeResponseViewModel AvailPrize(long prizeId, long userId);
        bool DeletePrize(long prizeId);
    }
}
