﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
   public interface IPlaylistRepository
    {
        MessageViewModel SendMessage(MessageViewModel messageViewModel);
        List<MessageViewModel> ReceiveMessage(MessageViewModel messageViewModel);
        List<PlaylistTopPerformerViewModel>  GetPlaylistTopPerofermers(long playListId, bool isTopTen);
        List<PlaylistLeaderboardViewModel> GetPlaylistLeaderboard(long playListId);
        List<PlaylistDropdownViewModel> GetAllPlaylist(long userId);
        List<PlaylistLeaderboardViewModel> GetPlaylistLeaderboardByTeam(long playListId);
    }
}
