﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface IUserInformationRepository
    {
        List<CountryViewModel> GetCountries();
        List<StateViewModel> GetStates(int countryID);
        List<CityViewModel> GetCities(int stateID);
        List<CityViewModel> GetLocation(string location);
        UserProfileViewModel GetUserProfile(UserProfileViewModel user);
        List<UserReponseViewModel> GetCompanyUsers(UserRequestViewModel users);
        void UpdateUserRole(UserRequestViewModel userRequestViewModel);
        bool UpdateUserStatus(UserRequestViewModel userRequestViewModel);
        bool CheckUserLimitForCompany(CompanyViewModel company);
        bool IsUserExist(string email);
        void AddUserInvite(UserInviteViewModel userInvite);
        UserInviteViewModel GetUserInvite(string email);
        bool UpdateNotificationSetting(UserRequestViewModel userRequestViewModel);
        string UpdateProfilePicture(long userId, string profilePictureName);
        UserProfileViewModel UpdateUserProfile(UserProfileViewModel userProfileViewModel);
        CompanyInfoViewModel UpdateCompanyProfile(CompanyInfoViewModel companyInfo);
        UserProfileViewModel GetUserProfileStatus(UserProfileViewModel user);
        List<PlaylistListViewModel> GetUserAssociatedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist);
        List<AssociatedTeamListViewModel> GetUserAssociatedTeam(long userId, string search);
        List<PlaylistListViewModel> GetUserAssignedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist);
        List<AssociatedUsersListViewModel> GetUserAssociatedUsers(long userId, string search);
        Response CanUserRegister(string emailId);
        string UpdateCompanyLogo(long userId, string CompanyLogo);
        CompanyInfoViewModel GetCompanyInfo(long userId);
        string DeleteCompanyLogo(long userId, string CompanyLogo);
        string DeleteProfileImage(long userId, string CompanyLogo);
        void SaveLocations(LocationsViewModel locationsViewModel, long userId);
        void DeleteLocation(int locationId);
        bool LocationExists(LocationsViewModel objLocationVM, long userId);
        List<LocationsViewModel> GetSignUpLocations(LocationsSearch location);
        List<LocationsViewModel> GetLocations(LocationsSearch location, long userId);
        List<LocationsViewModel> GetLocationsByCompanyId(long companyId);
    }
}
