﻿using Repository.Model;
using System.Collections.Generic;

namespace Repository.Interfaces
{
    public interface IUserTagRepository
    {
        void Add(UserTag tag);
        List<long?> GetUsersFromTags(List<long> tagIds, long userId);
    }
}
