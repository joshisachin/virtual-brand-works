﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
   public interface IQuizRepository
    {
        Response AddQuestion(QuestionAnswerViewModel questionAnswerViewModel);
        Response SaveQuiz(QuizViewModel quizVirtualModel);
        QuizViewModel GetQuizDetails(long quizId);
        QuestionAnswerViewModel GetQuestionDetails(long questionId);
        Response UpdateQuestion(QuestionAnswerViewModel questionAnswerViewModel);
        Response DeleteQuiz(long quizId);
        Response DeleteQuestion(long questionId);
    }
}
