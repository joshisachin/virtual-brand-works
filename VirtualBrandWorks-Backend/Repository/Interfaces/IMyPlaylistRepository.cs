﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Interfaces
{
    public interface IMyPlaylistRepository
    {
        List<PlaylistListViewModel> GetMyCreatedPlaylist(long userId, bool isLaunched);
        List<PlaylistListViewModel> GetMyPlaylist(long userId, bool isCompleted);
        MyPlaylistDetailViewModel GetMyPlaylistDetail(long playlistId, long userId, bool isSelfCreated);
        ViewResultDetailAndFoldersViewModel ViewResultDetailAndFolders(long playlistId, long userId);
        List<PlaylistContentResponseViewModel> ViewResultContentList(long playListFolderId, long userId);
        CompanyQuizViewModel GetQuizResult(long quizId, long playlistId, long userId, bool isMasterQuiz);
        MyPlaylistFolderDetailViewModel GetMyPlaylistFolderDetail(long playListFolderId, long playlistContentMasterId, long userId, string ipAddress);
        List<PlaylistContentResponseViewModel> GetMyPlaylistFolderContentList(long playListFolderId, long contentId);
        List<PlaylistUsersViewModel> GetPlaylistUsers(long playListId, long userId);
        PlaylistAssociatedUserViewModel AssociatedUsers(long playListId, long teamId, bool isTeam);
        List<AssociatedTeamListViewModel> GetPlaylistAssociatedTeams(long playListId);
        List<PlaylistListViewModel> GetMyDashboardPlaylist(long userId);
        void DeletePlaylist(PlaylistViewModel playlist);
        long CloneMyCreatedPlaylist(PlaylistViewModel playlist);
    }
}
