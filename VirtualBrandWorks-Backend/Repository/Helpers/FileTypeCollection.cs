﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;

namespace Repository.Helpers
{
    public class FileTypeCollection
    {
        public static string[] GetFileTypes(int type)
        {
            switch (type)
            {
                case (int)Constants.ContentType.Image:
                    return AllowedImageType;
                case (int)Constants.ContentType.Video:
                    return AllowedVideoType;
                case (int)Constants.ContentType.Document:
                    return AllowedDocType;
                default:
                    return new string[] { };
            }
        }

        public static string[] AllowedImageType = new string[] { ".png", ".jpg", ".jpeg", ".gif", ".tiff", ".bmp" };
        public static string[] AllowedVideoType = new string[] { ".mp4", ".mov", ".3gp" };
        public static string[] AllowedDocType = new string[] { ".pdf", ".doc", ".docx" };
    }
}
