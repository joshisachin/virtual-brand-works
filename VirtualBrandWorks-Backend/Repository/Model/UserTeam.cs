namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserTeam")]
    public partial class UserTeam
    {
        public long UserTeamId { get; set; }

        public long UserId { get; set; }

        public long TeamId { get; set; }

        public virtual Team Team { get; set; }

        public virtual User User { get; set; }

    }
}
