﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VbPlayContentUserView")]
    public partial class VbPlayContentUserView
    {
        [Key]
        public long VbPlayContentUserViewId { get; set; }
        public long UserId { get; set; }
        public string IPAddress { get; set; }
        public long ContestContentId { get; set; }
        public bool IsContestEntry { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}

