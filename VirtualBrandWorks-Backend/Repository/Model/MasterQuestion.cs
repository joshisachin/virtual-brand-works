namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MasterQuestion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MasterQuestion()
        {
            MasterAnswers = new HashSet<MasterAnswer>();
            MasterAnswers1 = new HashSet<MasterAnswer>();
        }

        [Key]
        public long MasterQuestionsId { get; set; }

        [StringLength(500)]
        public string Question { get; set; }

        public long? MasterQuizId { get; set; }

        public bool IsDeleted { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MasterAnswer> MasterAnswers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MasterAnswer> MasterAnswers1 { get; set; }

        public virtual MasterQuiz MasterQuiz { get; set; }

        public virtual MasterQuiz MasterQuiz1 { get; set; }
    }
}
