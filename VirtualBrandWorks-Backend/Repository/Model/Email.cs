namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Email")]
    public partial class Email
    {
        public long EmailId { get; set; }

        public long UserId { get; set; }

        [StringLength(50)]
        public string DestinationEmail { get; set; }

        public long? EmailTypeId { get; set; }

        [StringLength(200)]
        public string Subject { get; set; }

        [Column(TypeName = "text")]
        public string Body { get; set; }

        public bool? IsDeleted { get; set; }

        public virtual EmailType EmailType { get; set; }

        public virtual EmailType EmailType1 { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
