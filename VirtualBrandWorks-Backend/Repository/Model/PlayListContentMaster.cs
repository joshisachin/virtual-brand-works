namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PlayListContentMaster")]
    public partial class PlayListContentMaster
    {
        public long PlayListContentMasterId { get; set; }

        public long PlayListMasterId { get; set; }

        public long ContentId { get; set; }

        public int QuizId { get; set; }
        public int SortOrder { get; set; }
        public bool IsTurnedOffExistingQuiz { get; set; }

        public bool IsDeleted { get; set; }

        public virtual PlayListMaster PlayListMaster { get; set; }

        public virtual PlayListMaster PlayListMaster1 { get; set; }
    }
}
