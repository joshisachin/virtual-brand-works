namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MasterContent")]
    public partial class MasterContent
    {
        public long MasterContentId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(20)]
        public string ShortName { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public int? MasterQuizId { get; set; }

        public bool IsPublished { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        public long? MasterFolderId { get; set; }

        [StringLength(300)]
        public string ContentUrl { get; set; }
        public string MimeType { get; set; }
        public long ViewCount { get; set; }

        public virtual MasterFolder MasterFolder { get; set; }

        public long DynamicFolderContentId { get; set; }
    }
}
