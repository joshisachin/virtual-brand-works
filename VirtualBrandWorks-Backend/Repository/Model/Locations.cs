﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("Locations")]
    public class Locations
    {
        [Key]
        public int LocationId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public string LocationName { get; set; }
        public int? TrainerId { get; set; }
        public int? AdminId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? CountryId { get; set; }
        public long? CompanyId { get; set; }
    }
}
