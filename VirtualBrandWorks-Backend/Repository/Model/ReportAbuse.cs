﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("ReportAbuse")]
    public partial class ReportAbuse
    {
        [Key]
        public long ReportAbuseId { get; set; }
        public long? ContentId { get; set; }
        public long? UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
