﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserPlaylistBadge")]
    public partial class UserPlaylistBadge
    {
        [Key]
        public long UserPlaylistBadgeId { get; set; }

        public long UserId { get; set; }

        public long PlaylistId { get; set; }
        public int BadgeId { get; set; }

    }
}
