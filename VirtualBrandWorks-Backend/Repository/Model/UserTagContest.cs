﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("UserTagContest")]
    public class UserTagContest
    {
        [Key]
        public long UserTagContestID { get; set; }
        public long? UserID { get; set; }
        public long? TagId { get; set; }
        public long? ContestID { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
