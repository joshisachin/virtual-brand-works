﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PublishedTeam")]
    public partial class PublishedTeam
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PublishedTeam()
        {
            PublishedUserTeams = new HashSet<PublishedUserTeam>();
        }

        public long PublishedTeamId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public long TrainerId { get; set; }

        public long CompanyId { get; set; }

        public bool Status { get; set; }
        public int CityId { get; set; }
        public long MainTeamId { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        public long PlayListAssignedToId { get; set; }

        public virtual Company Company { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PublishedUserTeam> PublishedUserTeams { get; set; }

    }
}
