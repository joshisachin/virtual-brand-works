﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PointsTransaction")]
    public partial class PointsTransaction
    {
        [Key]
        public int PointsTransaction_Id { get; set; }

        public long? PlaylistId { get; set; }
        public long? ContestId { get; set; }

        public long UserId { get; set; }
        public string Description { get; set; }
        public long? Points { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? Balance { get; set; }
        public bool IsEarned { get; set; }
        public string Type { get; set; }

    }
}
