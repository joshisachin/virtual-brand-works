﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("CompanySubscription")]
    public class CompanySubscription
    {
        [Key]
        public int CompanySubscriptionId { get; set; }
        public long CompanyId { get; set; }
        public long SubscriptionPlanPriceId { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsObselete { get; set; }
    }
}
