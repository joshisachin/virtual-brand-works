﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PrizeCategory")]
    public partial class PrizeCategory
    {
        [Key]
        public int PrizeCategoryId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

    }
}
