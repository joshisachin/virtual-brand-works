namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyContentQuestion")]
    public partial class CompanyContentQuestion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompanyContentQuestion()
        {
            CompanyContentAnswers = new HashSet<CompanyContentAnswer>();
        }

        public long CompanyContentQuestionId { get; set; }

        [Required]
        [StringLength(500)]
        public string Question { get; set; }

        public long QuizId { get; set; }

        public bool IsDeleted { get; set; }

        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyContentAnswer> CompanyContentAnswers { get; set; }

        public virtual CompanyQuiz CompanyQuiz { get; set; }

    }
}
