namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Prize")]
    public partial class Prize
    {
        public long PrizeId { get; set; }
        
        public int PrizeCategoryId { get; set; }
        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? Points { get; set; }

        [StringLength(500)]
        public string ThumbnailImage { get; set; }

        public int? Quantity { get; set; }

        public string TimeOffDuration { get; set; }

        public string BrandName { get; set; }
        public int PrizeAmount { get; set; }
        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }
        public string Remarks { get; set; }

    }
}
