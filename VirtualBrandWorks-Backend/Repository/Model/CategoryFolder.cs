﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("CategoryFolder")]
    public class CategoryFolder
    {
        [Key]
        public long CategoryFolderId { get; set; }
        public long CategoryId { get; set; }
        public long FolderId { get; set; }
    }
}
