namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyLibraryContent")]
    public partial class CompanyLibraryContent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompanyLibraryContent()
        {
            CompanyContentTags = new HashSet<CompanyContentTag>();
            CompanyLibraries = new HashSet<CompanyLibrary>();
        }

        public long CompanyLibraryContentId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(20)]
        public string ShortName { get; set; }

        public long? CompanyQuizId { get; set; }

        public bool IsPublished { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        [StringLength(300)]
        public string ContentUrl { get; set; }

        public long FolderId { get; set; }
        public string Description { get; set; }
        public bool IsBroadcasted { get; set; }
        public string MimeType { get; set; }
        public long ViewCount { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyContentTag> CompanyContentTags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyLibrary> CompanyLibraries { get; set; }

        [ForeignKey("FolderId")]
        public virtual CompanyLibraryFolder CompanyLibraryFolder { get; set; }

        public DateTime? BroadcastingDate { get; set; }
        public bool? IsExtendDateNotified { get; set; }
        public bool? IsOptOutForNotification { get; set; }
        public int Visibility { get; set; }

    }
}
