﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Model
{
    [Table("FCMUserToken")]
    public class FCMUserToken
    {
        public long FCMUserTokenId { get; set; }
        public long UserId { get; set; }
        public string TokenId { get; set; }
        public virtual User User { get; set; }
    }
}
