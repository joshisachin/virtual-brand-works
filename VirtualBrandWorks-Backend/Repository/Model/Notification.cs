namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Notification")]
    public partial class Notification
    {
        public long NotificationId { get; set; }
        public long UserId { get; set; }
        public DateTime? SentOn { get; set; }
        public bool? IsSuccess { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
        public string Token { get; set; }
        public long? NotificationTypeId { get; set; }
        public string ErrorMessage { get; set; }
        public string MessageId { get; set; }
        public string MulticastId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public bool IsRead { get; set; }
        public bool? IsOptOut { get; set; }

    }
}
