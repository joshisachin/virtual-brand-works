﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Repository.Model
{
    [Table("SubscriptionPlanPrice")]
   public class SubscriptionPlanPrice
    {
        [Key]
        public long SubscriptionPlanPriceId { get; set; }
        public int SubscriptionId { get; set; }
        public string ProductId { get; set; }
        public string PlanId { get; set; }
        public int PlanType { get; set; }
        public double? Price { get; set; }
    }
}
