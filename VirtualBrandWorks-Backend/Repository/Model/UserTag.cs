namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserTag")]
    public partial class UserTag
    {
        public long UserTagId { get; set; }

        public long? TagId { get; set; }

        public long? UserId { get; set; }

        public virtual Tag Tag { get; set; }

        public virtual User User { get; set; }

    }
}
