﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PublishedPlaylistQuiz")]
    public partial class PublishedPlaylistQuiz
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PublishedPlaylistQuiz()
        {
            PublishedPlaylistQuestions = new HashSet<PublishedPlaylistQuestion>();
        }

        [Key]
        public long PublishedPlaylistQuizId { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }
        public long MainQuizId { get; set; }
        public long PlaylistContentMasterId { get; set; }
        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }

        public virtual PlayListContentMaster PlayListContentMaster { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PublishedPlaylistQuestion> PublishedPlaylistQuestions { get; set; }

    }
}
