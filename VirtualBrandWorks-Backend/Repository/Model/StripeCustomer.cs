﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("StripeCustomer")]
    public class StripeCustomer
    {
        [Key]
        public long StripeCustomerId { get; set; }
        public long UserId { get; set; }
        public string CustomerId { get; set; }
        public virtual User User{ get; set; }
    }
}
