namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserRole")]
    public partial class UserRole
    {
        public long UserRoleId { get; set; }

        public long RoleId { get; set; }

        public long UserId { get; set; }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }

    }
}
