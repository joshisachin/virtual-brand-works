﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("PlaylistPoints")]
    public class PlaylistPoints
    {
        [Key]
        public long PlaylistPointId { get; set; }
        public long PlaylistId { get; set; }
        public long UserId { get; set; }
        public long Points { get; set; }
        public int PointTypeId { get; set; }
    }
}
