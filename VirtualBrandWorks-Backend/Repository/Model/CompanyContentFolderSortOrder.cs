﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Model
{
    [Table("CompanyContentFolderSortOrder")]
    public class CompanyContentFolderSortOrder
    {
        [Key]
        public long CompanyContentFolderSortOrderId { get; set; }
        public long UserId { get; set; }
        public long FolderId { get; set; }
        public long SortOder { get; set; }
    }
}
