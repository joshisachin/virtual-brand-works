﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ContestMasterUserLike")]
    public partial class ContestMasterUserLike
    {
        public long ContestMasterUserLikeId { get; set; }

        public long ContestMasterId { get; set; }

        public long UserId { get; set; }

        public virtual ContestMaster Contest { get; set; }
        public virtual User User { get; set; }
    }
}