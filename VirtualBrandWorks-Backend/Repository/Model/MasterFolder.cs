namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MasterFolder")]
    public partial class MasterFolder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MasterFolder()
        {
            MasterContents = new HashSet<MasterContent>();
        }

        public long MasterFolderId { get; set; }

        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(15)]
        public string ShortName { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        public long? CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public bool? isDynamicFolder { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MasterContent> MasterContents { get; set; }
    }
}
