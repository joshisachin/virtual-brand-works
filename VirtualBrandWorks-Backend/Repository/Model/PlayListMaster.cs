namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PlayListMaster")]
    public partial class PlayListMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PlayListMaster()
        {
            PlayListContentMasters = new HashSet<PlayListContentMaster>();
            PlayListContentMasters1 = new HashSet<PlayListContentMaster>();
        }

        public long PlayListMasterId { get; set; }

        public long PlayListId { get; set; }

        public int FolderId { get; set; }
        public int SortOrder { get; set; }

        public bool IsFromMasterLibrary { get; set; }

        public virtual PlayList PlayList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayListContentMaster> PlayListContentMasters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayListContentMaster> PlayListContentMasters1 { get; set; }
    }
}
