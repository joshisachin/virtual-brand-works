﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("NotificationType")]
    public class NotificationType
    {
        [Key]
        public long NotificationTypeId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
