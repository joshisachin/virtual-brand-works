﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("PlaylistFolderUserSaved")]
    public class PlaylistFolderUserSaved
    {
        [Key]
        public long PlaylistFolderUserSavedId { get; set; }
        public long UserId { get; set; }
        public long FolderId { get; set; }
        public bool IsMasterLibraryFolder { get; set; }
        public int SortOrder { get; set; }
    }
}














