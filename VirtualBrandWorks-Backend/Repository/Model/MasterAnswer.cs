namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MasterAnswer
    {
        [Key]
        public long MasterAnswersId { get; set; }

        public long? QuestionId { get; set; }

        [StringLength(200)]
        public string Options { get; set; }

        public bool IsAnswer { get; set; }

        public bool IsDeleted { get; set; }

        public virtual MasterQuestion MasterQuestion { get; set; }

        public virtual MasterQuestion MasterQuestion1 { get; set; }
    }
}
