﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("PlaylistContentUserSaved")]
    public class PlaylistContentUserSaved
    {
        [Key]
        public long PlaylistContentUserSavedId { get; set; }
        public long PlaylistFolderUserSavedId { get; set; }
        public long ContentId { get; set; }
        public int SortOrder { get; set; }
        public bool IsContentSaved { get; set; }
        public long UserId { get; set; }
    }
}
