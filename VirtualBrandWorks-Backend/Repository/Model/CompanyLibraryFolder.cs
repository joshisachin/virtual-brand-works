﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyLibraryFolder")]
    public partial class CompanyLibraryFolder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompanyLibraryFolder()
        {
            CompanyLibraries = new HashSet<CompanyLibrary>();
        }

        [Key]
        public long CompanyLibraryFolderId { get; set; }
        public long CompanyId { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(15)]
        public string ShortName { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsDynamicFolder { get; set; }

      //  [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyLibrary> CompanyLibraries { get; set; }

    }
}
