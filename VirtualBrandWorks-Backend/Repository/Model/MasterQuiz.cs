namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MasterQuiz")]
    public partial class MasterQuiz
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MasterQuiz()
        {
            MasterQuestions = new HashSet<MasterQuestion>();
            MasterQuestions1 = new HashSet<MasterQuestion>();
        }

        public long MasterQuizId { get; set; }
        public long MasterContentId { get; set; }

        [StringLength(100)]
        public string QuizName { get; set; }

        [StringLength(20)]
        public string QuizShortName { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MasterQuestion> MasterQuestions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MasterQuestion> MasterQuestions1 { get; set; }
        public virtual MasterContent MasterContent1 { get; set; }
    }
}
