namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AvailPrize")]
    public partial class AvailPrize
    {
        public long AvailPrizeId { get; set; }

        public long UserId { get; set; }

        public long? PrizeRedeemCodeId { get; set; }

        public DateTime? AvailDate { get; set; }

        public virtual User User { get; set; }

    }
}
