﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("MasterFolderUserSaved")]
   public class MasterFolderUserSaved
    {
        [Key]
        public long MasterFolderUserSavedId { get; set; }
        public long UserId { get; set; }
        public long FolderId { get; set; }
    }
}
