﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("UserPlayListStatus")]
    public class UserPlayListStatus
    {
        [Key]
        public long UserPlayListStatus_Id { get; set; }
        public long UserId { get; set; }
        public long PlaylistId { get; set; }
        public bool IsComplete { get; set; }
    }
}
