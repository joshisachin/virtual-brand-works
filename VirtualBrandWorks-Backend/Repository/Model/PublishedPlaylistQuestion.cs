﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PublishedPlaylistQuestion")]
    public partial class PublishedPlaylistQuestion
    {
        [Key]
        public long PublishedPlaylistQuestionId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PublishedPlaylistQuestion()
        {
            PublishedPlaylistAnswers = new HashSet<PublishedPlaylistAnswer>();
        }

        [Required]
        [StringLength(500)]
        public string Question { get; set; }

        public long PublishedPlaylistQuizId { get; set; }

        public long MainQuestionId { get; set; }

        public bool IsDeleted { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PublishedPlaylistAnswer> PublishedPlaylistAnswers { get; set; }

        public virtual PublishedPlaylistQuiz PublishedPlaylistQuiz { get; set; }

    }
}
