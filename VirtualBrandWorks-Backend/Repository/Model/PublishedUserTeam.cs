﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PublishedUserTeam")]
    public partial class PublishedUserTeam
    {
        public long PublishedUserTeamId { get; set; }

        public long UserId { get; set; }

        public long PublishedTeamId { get; set; }

        public virtual PublishedTeam Team { get; set; }

        public virtual User User { get; set; }

    }
}