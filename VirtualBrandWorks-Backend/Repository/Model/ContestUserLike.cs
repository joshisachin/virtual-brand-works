﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ContestUserLike")]
    public partial class ContestUserLike
    {
        public long ContestUserLikeId { get; set; }

        public long ContestId { get; set; }

        public long UserId { get; set; }

        public virtual Contest Contest { get; set; }
        public virtual User User { get; set; }
    }
}