namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyLibrary")]
    public partial class CompanyLibrary
    {
        public long CompanyLibraryId { get; set; }

        public long? CompanyId { get; set; }

        public long CompanyLibraryFolderId { get; set; }

        public long CompanyLibraryContentId { get; set; }

        public bool IsDynamicContent { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public virtual Company Company { get; set; }

        public virtual Company Company1 { get; set; }

        public virtual CompanyLibraryFolder CompanyLibraryFolder { get; set; }

        public virtual CompanyLibraryContent CompanyLibraryContent { get; set; }

        public virtual CompanyLibraryFolder CompanyLibraryFolder1 { get; set; }

        public virtual CompanyLibraryContent CompanyLibraryContent1 { get; set; }
    }
}
