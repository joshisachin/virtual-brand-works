﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("State")]
    public class State
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public int CountryID { get; set; }
        public string StateCode { get; set; }
        public virtual Country Country { get; set; }
    }
}
