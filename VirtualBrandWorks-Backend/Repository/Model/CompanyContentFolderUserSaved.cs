﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Model
{
    [Table("CompanyContentFolderUserSaved")]
    public class CompanyContentFolderUserSaved
    {
        [Key]
        public long CompanyContentFolderUserSavedId { get; set; }
        public long UserId { get; set; }
        public long FolderId { get; set; }
    }
}
