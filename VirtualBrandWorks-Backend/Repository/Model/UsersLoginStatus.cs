﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
   public class UsersLoginStatus
    {
        [Key]
        public int StatusId { set; get; }
        public string UserName { set; get; }
        public string StatusCode { set; get; }
        public DateTime? CreatedDate { set; get; }
    }
}
