namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PlayListAssignedTo")]
    public partial class PlayListAssignedTo
    {
        public long PlayListAssignedToId { get; set; }

        public long PlayListId { get; set; }

        public bool IsAssignedToTeam { get; set; }

        public long? TeamId { get; set; }

        public long? UserId { get; set; }

        public long PublishedTeamId { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public virtual PlayList PlayList { get; set; }

    }
}
