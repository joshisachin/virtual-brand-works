﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("ContestTag")]
    public class ContestTag
    {
        [Key]
        public long ContestTagId { get; set; }
        public long ContestId { get; set; }
        public long TagId { get; set; }
        public virtual Contest Contest { get; set; }
    }
}
