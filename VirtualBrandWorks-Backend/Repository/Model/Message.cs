namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Message")]
    public partial class Message
    {
        public long MessageId { get; set; }

        public long PlayListId { get; set; }

        public long SenderId { get; set; }

        public long ReceiverId { get; set; }

        public DateTime Date { get; set; }

        public int MessageType { get; set; }

        public bool IsMessageRead { get; set; }

        [Column("Message", TypeName = "text")]
        public string Message1 { get; set; }

        public virtual PlayList PlayList { get; set; }
    }
}
