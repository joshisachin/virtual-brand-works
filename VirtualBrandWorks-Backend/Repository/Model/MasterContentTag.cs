﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MasterContentTag")]
    public partial class MasterContentTag
    {
        public long MasterContentTagId { get; set; }

        public long MasterContentId { get; set; }

        public long TagId { get; set; }

        public virtual MasterContent MasterContent { get; set; }

        public virtual Tag Tag { get; set; }

    }
}
