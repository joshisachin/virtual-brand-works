namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Subscription")]
    public partial class Subscription
    {
        public int SubscriptionId { get; set; }

        [StringLength(50)]
        public string PlanName { get; set; }

        [StringLength(300)]
        public string PlanDescription { get; set; }

        public long? NumberOfUsers { get; set; }

        public double? CostForMonthlyBilling { get; set; }

        public double? CostForAnnualBilling { get; set; }

        public bool Status { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }
        public string ProductId { get; set; }
    }
}
