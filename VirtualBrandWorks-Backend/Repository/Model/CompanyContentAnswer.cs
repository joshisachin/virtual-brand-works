namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyContentAnswer")]
    public partial class CompanyContentAnswer
    {
        public long CompanyContentAnswerId { get; set; }

        public long CompanyQuestionId { get; set; }

        [StringLength(200)]
        public string Options { get; set; }

        public bool IsAnswer { get; set; }

        public bool IsDeleted { get; set; }

        public virtual CompanyContentQuestion CompanyContentQuestion { get; set; }
        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
