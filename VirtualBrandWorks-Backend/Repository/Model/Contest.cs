namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Contest")]
    public partial class Contest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Contest()
        {
            ContestMasters = new HashSet<ContestMaster>();
            ContestTags = new HashSet<ContestTag>();
        }

        public long ContestId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [StringLength(500)]
        public string Rules { get; set; }

        public bool IsBroadCasted { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }
        public string UploadedContentUrl { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public string MimeType { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsWinner { get; set; }
        public bool? IsWinnerAnnounced { get; set; }
        public bool? IsExtendDateNotified { get; set; }
        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContestMaster> ContestMasters { get; set; }

        public virtual ICollection<ContestTag> ContestTags { get; set; }
        public long Likes { get; internal set; }
        public long Views { get; internal set; }
        public DateTime? BroadcastingDate { get; set; }
        public bool? IsOptOutForNotification { get; set; }
        public int Visibility { get; set; }
        public bool? IsSubmitted { get; set; }
    }
}
