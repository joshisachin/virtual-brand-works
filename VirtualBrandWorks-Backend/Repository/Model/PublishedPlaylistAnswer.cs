﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PublishedPlaylistAnswer")]
    public partial class PublishedPlaylistAnswer
    {
        [Key]
        public long PublishedPlaylistAnswerId { get; set; }

        public long PublishedPlaylistQuestionId { get; set; }

        [StringLength(200)]
        public string Options { get; set; }

        public bool IsAnswer { get; set; }

        public bool IsDeleted { get; set; }

        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PublishedPlaylistQuestion PublishedPlaylistQuestion { get; set; }

    }
}

