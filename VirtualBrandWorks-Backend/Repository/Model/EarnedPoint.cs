namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EarnedPoint")]
    public partial class EarnedPoint
    {
        public long EarnedPointId { get; set; }

        [StringLength(20)]
        public string Type { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(100)]
        public string Contest { get; set; }

        [StringLength(20)]
        public string EarnedReedemed { get; set; }

        public int? Points { get; set; }

        public int? Balance { get; set; }

        public long? UserId { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
    }
}
