﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserContentViews")]
    public partial class UserContentViews
    {
        [Key]
        public long ContentViewCounterId { get; set; }
        public long UserId { get; set; }
        public string IPAddress { get; set; }
        public long ContentId { get; set; }
        public bool IsMasterContent { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
