namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ContestMaster")]
    public partial class ContestMaster
    {
        public long ContestMasterId { get; set; }

        public long ContestId { get; set; }

        public long UserId { get; set; }

        [StringLength(200)]
        public string UploadedContentUrl { get; set; }

        public long JoinedByUserId { get; set; }

        public DateTime JoinedDate { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(100)]
        public string ContentTitle { get; set; }

        public long Likes { get; set; }

        public long Views { get; set; }

        public bool IsWinner { get; set; }
        public bool IsAwsContent { get; set; }
        public bool IsYoutubeContent { get; set; }
        public bool IsFacebookContent { get; set; }
        public bool IsSubmitted { get; set; }
        public string MimeType { get; set; }
        public bool IsBroadcasted { get; set; }
        public DateTime? BroadcastingDate { get; set; }
        public bool? IsOptOutForNotification { get; set; }
        public bool? IsExtendDateNotified { get; set; }
        public int Visibility { get; set; }
        public virtual Contest Contest { get; set; }

        public virtual User User { get; set; }

    }
}
