namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserAnsweredQuiz")]
    public partial class UserAnsweredQuiz
    {
        public long UserAnsweredQuizId { get; set; }

        public long UserId { get; set; }

        public long? QuestionId { get; set; }

        public int? AnswereId { get; set; }

        public long? QuizId { get; set; }
        public long PlayListId { get; set; }

        public bool IsMasterQuiz { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public virtual User User { get; set; }
    }
}
