﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Model
{
    [Table("MasterFolderSortOrder")]
    public class MasterFolderSortOrder
    {
        [Key]
        public long MasterFolderSortOrderId { get; set; }
        public long UserId { get; set; }
        public long FolderId { get; set; }
        public long SortOder { get; set; }
    }
}
