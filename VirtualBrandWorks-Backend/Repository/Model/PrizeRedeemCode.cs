﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PrizeRedeemCode")]
    public partial class PrizeRedeemCode
    {
        [Key]
        public long PrizeRedeemCodeId { get; set; }
        
        public long PrizeId { get; set; }

        public int PrizeCategoryId { get; set; }

        [StringLength(50)]
        public string RedeemCode { get; set; }

        public bool IsValid { get; set; }

    }
}
