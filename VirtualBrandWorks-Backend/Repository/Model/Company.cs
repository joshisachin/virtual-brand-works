namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Company")]
    public partial class Company
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Company()
        {
            CompanyLibraries = new HashSet<CompanyLibrary>();
            CompanyLibraries1 = new HashSet<CompanyLibrary>();
            Teams = new HashSet<Team>();
            Users = new HashSet<User>();
            PublishedTeams = new HashSet<PublishedTeam>();
        }

        public long CompanyId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [StringLength(100)]
        public string EmailId { get; set; }

        [StringLength(15)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string AdminFirstName { get; set; }

        [StringLength(50)]
        public string AdminLastName { get; set; }

        [StringLength(100)]
        public string AdminEmailId { get; set; }

        [StringLength(50)]
        public string AdminPassword { get; set; }

        [StringLength(15)]
        public string AdminPhoneNumber { get; set; }
        public bool Status { get; set; }

        public bool IsDeleted { get; set; }
        public string StripeSubscriptionId { get; set; }
        public string CompanyLogo { get; set; }
        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyLibrary> CompanyLibraries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyLibrary> CompanyLibraries1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Team> Teams { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PublishedTeam> PublishedTeams { get; set; }

    }
}
