namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyContentTag")]
    public partial class CompanyContentTag
    {
        public long CompanyContentTagId { get; set; }

        public long CompanyLibraryContentId { get; set; }

        public long TagId { get; set; }

        public virtual CompanyLibraryContent CompanyLibraryContent { get; set; }

        public virtual Tag Tag { get; set; }

    }
}
