﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [Table("UserQuizResult")]
    public class UserQuizResult
    {
        [Key]
        public long UserQuizResultId { get; set; }
        public long UserId { get; set; }
        public long PlaylistId { get; set; }
        public long PlayListContentMasterId { get; set; }
        public long QuizId { get; set; }
        public bool IsMasterQuiz { get; set; }
        public int MarksObtained { get; set; }
        public int TotalMarks { get; set; }
        public int NumberOfAttempt { get; set; }
        public int TotalNumberOfQuestion { get; set; }
        public int TotalCorrectAnswer { get; set; }
        public bool IsPassed { get; set; }
        public bool IsComplete { get; set; }

        public virtual PlayListContentMaster PlayListContentMaster { get; set; }
    }
}
