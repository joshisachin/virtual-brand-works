﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BuildPlaylistSavedState")]
    public partial class BuildPlaylistSavedState
    {
        [Key]
        public long BuildPlaylistSavedStateId { get; set; }
        public long UserId { get; set; }
        public long PlaylistId { get; set; }
        public int StepNumber { get; set; }
    }
}

