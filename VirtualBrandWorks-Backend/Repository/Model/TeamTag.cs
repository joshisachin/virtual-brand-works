﻿namespace Repository.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TeamTag")]
    public partial class TeamTag
    {
        public long TeamTagId { get; set; }

        public long TagId { get; set; }

        public long TeamId { get; set; }

        public virtual Tag Tag { get; set; }
        public virtual Team Team { get; set; }
    }
}
