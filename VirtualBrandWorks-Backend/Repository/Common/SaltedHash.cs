﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace VirtualBrandWork.Common
{
    public class SaltedHash
    {
        public static string GetSalt()
        {
            byte[] salt = GenerateSalt();
            return Convert.ToBase64String(salt);
        }

        public static string GetHashPasswordWithSalt(string plainText, string salt)
        {
            var hashedPassword = HashPasswordWithSalt(Encoding.UTF8.GetBytes(plainText), Encoding.UTF8.GetBytes(salt));
            return Convert.ToBase64String(hashedPassword);
        }

        private static byte[] GenerateSalt()
        {
            const int saltLength = 32;

            using (var randomNumberGenerator = new RNGCryptoServiceProvider())
            {
                var randomNumber = new byte[saltLength];
                randomNumberGenerator.GetBytes(randomNumber);

                return randomNumber;
            }
        }

        private static byte[] Combine(byte[] first, byte[] second)
        {
            var ret = new byte[first.Length + second.Length];

            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);

            return ret;
        }

        private static byte[] HashPasswordWithSalt(byte[] toBeHashed, byte[] salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var combinedHash = Combine(toBeHashed, salt);

                return sha256.ComputeHash(combinedHash);
            }
        }
    }
}
