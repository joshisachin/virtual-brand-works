﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualBrandWork.Common
{
    public class Response
    {
        public object Data { get; set; }
        public bool Status { get; set; }

        public string Message { get; set; }
    }
}