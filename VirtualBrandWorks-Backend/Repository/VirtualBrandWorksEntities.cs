namespace Repository
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Model;
    using Interfaces;
    using System.Web;
    using System.Security.Claims;
    using VirtualBrandWork.Entity.ViewModels;

    public partial class VirtualBrandWorksEntities : DbContext
    {
        public VirtualBrandWorksEntities()
            : base("name=VirtualBrandWorksEntities")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        public override int SaveChanges()
        {
            var userIdValue = string.Empty;
            //get named identifier for identity
            var claimsIdentity = HttpContext.Current.User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userIdValue = userIdClaim.Value;
                }
            }

            var added = ChangeTracker.Entries<IBaseEntity>().Where(E => E.State == EntityState.Added).ToList();

            added.ForEach(E =>
            {
                E.Property(x => x.CreatedDate).CurrentValue = DateTime.UtcNow;
                //E.Property(x => x.Created).IsModified = true;

                E.Property(x => x.CreatedBy).CurrentValue = Convert.ToInt64(userIdValue);
                //E.Property(x => x.Created).IsModified = true;
            });

            var modified = ChangeTracker.Entries<IBaseEntity>().Where(E => E.State == EntityState.Modified).ToList();

            modified.ForEach(E =>
            {
                E.Property(x => x.ModifiedDate).CurrentValue = DateTime.UtcNow;
                E.Property(x => x.ModifiedDate).IsModified = true;

                E.Property(x => x.CreatedDate).CurrentValue = E.Property(x => x.CreatedDate).OriginalValue;
                E.Property(x => x.CreatedDate).IsModified = false;

                E.Property(x => x.ModifiedBy).CurrentValue = Convert.ToInt64(userIdValue);
                E.Property(x => x.ModifiedBy).IsModified = true;

                E.Property(x => x.CreatedBy).CurrentValue = E.Property(x => x.CreatedBy).OriginalValue;
                E.Property(x => x.CreatedBy).IsModified = false;

            });

            return base.SaveChanges();
        }
        public virtual DbSet<AvailPrize> AvailPrizes { get; set; }
        public virtual DbSet<Badge> Badges { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyContentAnswer> CompanyContentAnswers { get; set; }
        public virtual DbSet<CompanyContentQuestion> CompanyContentQuestions { get; set; }
        public virtual DbSet<CompanyContentTag> CompanyContentTags { get; set; }
        public virtual DbSet<CompanyLibrary> CompanyLibraries { get; set; }
        public virtual DbSet<CompanyLibraryContent> CompanyLibraryContents { get; set; }
        public virtual DbSet<CompanyLibraryFolder> CompanyLibraryFolders { get; set; }
        public virtual DbSet<CompanyQuiz> CompanyQuizs { get; set; }
        public virtual DbSet<Contest> Contests { get; set; }
        public virtual DbSet<ContestMaster> ContestMasters { get; set; }
        public virtual DbSet<EarnedPoint> EarnedPoints { get; set; }
        public virtual DbSet<Email> Emails { get; set; }
        public virtual DbSet<EmailType> EmailTypes { get; set; }
        public virtual DbSet<MasterAnswer> MasterAnswers { get; set; }
        public virtual DbSet<MasterContent> MasterContents { get; set; }
        public virtual DbSet<MasterFolder> MasterFolders { get; set; }
        public virtual DbSet<MasterQuestion> MasterQuestions { get; set; }
        public virtual DbSet<MasterQuiz> MasterQuizs { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<PlayList> PlayLists { get; set; }
        public virtual DbSet<PlayListAssignedTo> PlayListAssignedToes { get; set; }
        public virtual DbSet<PlayListContentMaster> PlayListContentMasters { get; set; }
        public virtual DbSet<PlayListMaster> PlayListMasters { get; set; }
        public virtual DbSet<Prize> Prizes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<UserAnsweredQuiz> UserAnsweredQuizs { get; set; }
        public virtual DbSet<UserQuizResult> UserQuizResults { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserTag> UserTags { get; set; }
        public virtual DbSet<UserTeam> UserTeams { get; set; }
        public virtual DbSet<MasterContentTag> MasterContentTags { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<UserInvite> UserInvites { get; set; }
        public virtual DbSet<SubscriptionPlanPrice> SubscriptionPlanPrice { get; set; }
        public virtual DbSet<CompanySubscription> CompanySubscription { get; set; }
        public virtual DbSet<CategoryFolder> CategoryFolder { get; set; }
        public virtual DbSet<CompanyContentFolderSortOrder> CompanyContentFolderSortOrder { get; set; }
        public virtual DbSet<CompanyContentFolderUserSaved> CompanyContentFolderUserSaved { get; set; }
        public virtual DbSet<MasterFolderSortOrder> MasterFolderSortOrder { get; set; }
        public virtual DbSet<MasterFolderUserSaved> MasterFolderUserSaved { get; set; }
        public virtual DbSet<PlaylistFolderUserSaved> PlaylistFolderUserSaved { get; set; }
        public virtual DbSet<PlaylistContentUserSaved> PlaylistContentUserSaved { get; set; }
        public virtual DbSet<BuildPlaylistSavedState> BuildPlaylistSavedState { get; set; }
        public virtual DbSet<PlaylistPoints> PlaylistPoint { get; set; }
        public virtual DbSet<StripeCustomer> StripeCustomer { get; set; }
        public virtual DbSet<NotificationType> NotificationType { get; set; }
        public virtual DbSet<FCMUserToken> FCMUserToken { get; set; }
        public virtual DbSet<ContestTag> ContestTags { get; set; }
        public virtual DbSet<UserPlayListStatus> UserPlayListStatus { get; set; }
        public virtual DbSet<UsersLoginStatus> UsersLoginStatus { get; set; }
        public virtual DbSet<TeamTag> TeamTags { get; set; }
        public virtual DbSet<UserContentViews> UserContentViews { get; set; }
        public virtual DbSet<PublishedPlaylistQuiz> PublishedPlaylistQuiz { get; set; }
        public virtual DbSet<PublishedPlaylistQuestion> PublishedPlaylistQuestion { get; set; }
        public virtual DbSet<PublishedPlaylistAnswer> PublishedPlaylistAnswer { get; set; }
        public virtual DbSet<PublishedTeam> PublishedTeams { get; set; }
        public virtual DbSet<PublishedUserTeam> PublishedUserTeams { get; set; }
        public virtual DbSet<ContestUserLike> ContestUserLikes { get; set; }
        public virtual DbSet<ContestMasterUserLike> ContestMasterUserLikes { get; set; }
        public virtual DbSet<VbPlayContentUserView> VbPlayContentUserViews { get; set; }
        public virtual DbSet<PointsTransaction> PointsTransactions { get; set; }
        public virtual DbSet<UserPlaylistBadge> UserPlaylistBadges { get; set; }
        public virtual DbSet<PrizeCategory> PrizeCategories { get; set; }
        public virtual DbSet<PrizeRedeemCode> PrizeRedeemCodes { get; set; }
        public virtual DbSet<UserTagContest> UserTagContest { get; set; }
        public virtual DbSet<Locations> Locations { get; set; }
        public virtual DbSet<ReportAbuse> ReportAbuses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>()
                .Property(e => e.AdminFirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .Property(e => e.AdminLastName)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanyLibraries)
                .WithOptional(e => e.Company)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanyLibraries1)
                .WithOptional(e => e.Company1)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Teams)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.PublishedTeams)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.CompanyId);


            modelBuilder.Entity<Company>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Company)
                .HasForeignKey(e => e.CompanyId);


            modelBuilder.Entity<CompanyContentQuestion>()
                .HasMany(e => e.CompanyContentAnswers)
                .WithRequired(e => e.CompanyContentQuestion)
                .HasForeignKey(e => e.CompanyQuestionId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<CompanyLibraryContent>()
                .HasMany(e => e.CompanyContentTags)
                .WithRequired(e => e.CompanyLibraryContent)
                .HasForeignKey(e => e.CompanyLibraryContentId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<CompanyLibraryContent>()
                .HasMany(e => e.CompanyLibraries)
                .WithRequired(e => e.CompanyLibraryContent)
                .HasForeignKey(e => e.CompanyLibraryContentId)
                .WillCascadeOnDelete(false);



            modelBuilder.Entity<CompanyLibraryFolder>()
                .HasMany(e => e.CompanyLibraries)
                .WithRequired(e => e.CompanyLibraryFolder)
                .HasForeignKey(e => e.CompanyLibraryFolderId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<CompanyQuiz>()
                .HasMany(e => e.CompanyContentQuestions)
                .WithRequired(e => e.CompanyQuiz)
                .HasForeignKey(e => e.QuizId);


            modelBuilder.Entity<Contest>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Contest>()
                .Property(e => e.Rules)
                .IsUnicode(false);

            modelBuilder.Entity<Contest>()
                .HasMany(e => e.ContestMasters)
                .WithRequired(e => e.Contest)
                .HasForeignKey(e => e.ContestId);


            modelBuilder.Entity<ContestMaster>()
                .Property(e => e.UploadedContentUrl)
                .IsUnicode(false);

            modelBuilder.Entity<ContestMaster>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ContestMaster>()
                .Property(e => e.ContentTitle)
                .IsUnicode(false);

            modelBuilder.Entity<EarnedPoint>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<EarnedPoint>()
                .Property(e => e.Contest)
                .IsUnicode(false);

            modelBuilder.Entity<EarnedPoint>()
                .Property(e => e.EarnedReedemed)
                .IsUnicode(false);

            modelBuilder.Entity<Email>()
                .Property(e => e.Body)
                .IsUnicode(false);

            modelBuilder.Entity<EmailType>()
                .Property(e => e.Template)
                .IsUnicode(false);

            modelBuilder.Entity<EmailType>()
                .HasMany(e => e.Emails)
                .WithOptional(e => e.EmailType)
                .HasForeignKey(e => e.EmailTypeId);

            modelBuilder.Entity<EmailType>()
                .HasMany(e => e.Emails1)
                .WithOptional(e => e.EmailType1)
                .HasForeignKey(e => e.EmailTypeId);

            modelBuilder.Entity<MasterQuestion>()
                .Property(e => e.Question)
                .IsUnicode(false);

            modelBuilder.Entity<MasterQuestion>()
                .HasMany(e => e.MasterAnswers)
                .WithOptional(e => e.MasterQuestion)
                .HasForeignKey(e => e.QuestionId);

            modelBuilder.Entity<MasterQuestion>()
                .HasMany(e => e.MasterAnswers1)
                .WithOptional(e => e.MasterQuestion1)
                .HasForeignKey(e => e.QuestionId);

            modelBuilder.Entity<MasterQuiz>()
                .HasMany(e => e.MasterQuestions)
                .WithOptional(e => e.MasterQuiz)
                .HasForeignKey(e => e.MasterQuizId);

            modelBuilder.Entity<MasterQuiz>()
                .HasMany(e => e.MasterQuestions1)
                .WithOptional(e => e.MasterQuiz1)
                .HasForeignKey(e => e.MasterQuizId);

            modelBuilder.Entity<Message>()
                .Property(e => e.Message1)
                .IsUnicode(false);

            modelBuilder.Entity<PlayList>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<PlayList>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<PlayList>()
                .HasMany(e => e.Messages)
                .WithRequired(e => e.PlayList)
                .HasForeignKey(e => e.PlayListId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<PlayList>()
                .HasMany(e => e.PlayListAssignedToes)
                .WithRequired(e => e.PlayList)
                .HasForeignKey(e => e.PlayListId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<PlayList>()
                .HasMany(e => e.PlayListMasters)
                .WithRequired(e => e.PlayList)
                .HasForeignKey(e => e.PlayListId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<PlayListMaster>()
                .HasMany(e => e.PlayListContentMasters)
                .WithRequired(e => e.PlayListMaster)
                .HasForeignKey(e => e.PlayListMasterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PlayListMaster>()
                .HasMany(e => e.PlayListContentMasters1)
                .WithRequired(e => e.PlayListMaster1)
                .HasForeignKey(e => e.PlayListMasterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prize>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Prize>()
                .Property(e => e.ThumbnailImage)
                .IsUnicode(false);


            modelBuilder.Entity<Role>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.Role)
                .HasForeignKey(e => e.RoleId);

            //modelBuilder.Entity<Role>()
            //    .HasMany(e => e.UserRoles1)
            //    .WithOptional(e => e.Role1)
            //    .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<Tag>()
                .HasMany(e => e.CompanyContentTags)
                .WithRequired(e => e.Tag)
                .HasForeignKey(e => e.TagId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Tag>()
                .HasMany(e => e.UserTags)
                .WithOptional(e => e.Tag)
                .HasForeignKey(e => e.TagId);

            modelBuilder.Entity<Tag>()
                .HasMany(e => e.TeamTags)
                .WithRequired(e => e.Tag)
                .HasForeignKey(e => e.TagId);

            modelBuilder.Entity<Team>()
                .HasMany(e => e.UserTeams)
                .WithRequired(e => e.Team)
                .HasForeignKey(e => e.TeamId);

            modelBuilder.Entity<Team>()
               .HasMany(e => e.TeamTags)
               .WithRequired(e => e.Team)
               .HasForeignKey(e => e.TeamId);

            modelBuilder.Entity<User>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AvailPrizes)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<User>()
                .HasMany(e => e.Contests)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy);


            modelBuilder.Entity<User>()
                .HasMany(e => e.ContestMasters)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId);


            modelBuilder.Entity<User>()
                .HasMany(e => e.EarnedPoints)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.EarnedPoints1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Emails)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Emails1)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Notifications)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Notifications1)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<User>()
                .HasMany(e => e.Teams)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.TrainerId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PublishedTeams)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.TrainerId);


            modelBuilder.Entity<User>()
                .HasMany(e => e.UserAnsweredQuizs)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<User>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.UserRoles1)
            //    .WithOptional(e => e.User1)
            //    .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserTags)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.UserId);


            modelBuilder.Entity<User>()
                .HasMany(e => e.UserTeams)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.PublishedUserTeams)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
               .HasMany(e => e.StripeCustomer)
               .WithRequired(e => e.User)
               .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
              .HasMany(e => e.FCMUserToken)
              .WithRequired(e => e.User)
              .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Contest>()
              .HasMany(e => e.ContestTags)
              .WithRequired(e => e.Contest)
              .HasForeignKey(e => e.ContestId);

            modelBuilder.Entity<PublishedPlaylistQuestion>()
              .HasMany(e => e.PublishedPlaylistAnswers)
              .WithRequired(e => e.PublishedPlaylistQuestion)
              .HasForeignKey(e => e.PublishedPlaylistQuestionId)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<PublishedPlaylistQuiz>()
              .HasMany(e => e.PublishedPlaylistQuestions)
              .WithRequired(e => e.PublishedPlaylistQuiz)
              .HasForeignKey(e => e.PublishedPlaylistQuizId);

            modelBuilder.Entity<PublishedTeam>()
              .HasMany(e => e.PublishedUserTeams)
              .WithRequired(e => e.Team)
              .HasForeignKey(e => e.PublishedTeamId);

        }
    }
}
