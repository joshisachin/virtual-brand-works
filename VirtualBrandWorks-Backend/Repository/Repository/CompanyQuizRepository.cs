﻿using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class CompanyQuizRepository : ICompanyQuizRepository
    {
        public void AddQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                questionAnswerViewModel.ForEach(question =>
                {
                    CompanyContentQuestion cq = new CompanyContentQuestion();
                    cq.Question = question.Question.Question;
                    cq.QuizId = question.QuizId;
                    cq.CreatedBy = userId;
                    cq.CreatedDate = DateTime.UtcNow;
                    question.Answers.ForEach(obj =>
                    cq.CompanyContentAnswers.Add(new CompanyContentAnswer()
                    {
                        Options = obj.Answer,
                        IsAnswer = obj.IsRightAnswer,
                        CompanyQuestionId = cq.CompanyContentQuestionId,
                        CreatedBy = userId,
                        CreatedDate = DateTime.UtcNow
                    }));
                    dbContext.CompanyContentQuestions.Add(cq);
                    dbContext.CompanyContentAnswers.AddRange(cq.CompanyContentAnswers);
                    dbContext.SaveChanges();
                });
            }
        }

        public void DeleteQuestion(long questionId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var ques = dbContext.CompanyContentQuestions.Where(obj => obj.CompanyContentQuestionId == questionId).SingleOrDefault();
                if (ques != null)
                {
                    var answers = (from a in dbContext.CompanyContentAnswers
                                   where a.CompanyQuestionId == questionId
                                   select a
                                   ).ToList();
                    dbContext.CompanyContentAnswers.RemoveRange(answers);
                    dbContext.CompanyContentQuestions.Remove(ques);
                    dbContext.SaveChanges();
                }
            }
        }

        public void DeleteQuiz(long quizId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var quiz = dbContext.CompanyQuizs.Where(obj => obj.QuizId == quizId).SingleOrDefault();
                if (quiz != null)
                {
                    var questions = dbContext.CompanyContentQuestions.Where(obj => obj.QuizId == quizId).ToList();
                    var answers = (from q in questions
                                   join a in dbContext.CompanyContentAnswers on q.CompanyContentQuestionId equals a.CompanyQuestionId
                                   where q.QuizId == quizId
                                   select a
                                   ).ToList();
                    dbContext.CompanyContentAnswers.RemoveRange(answers);
                    dbContext.CompanyContentQuestions.RemoveRange(questions);
                    dbContext.CompanyQuizs.Remove(quiz);
                    dbContext.SaveChanges();
                }
            }
        }

        public CompanyQuizViewModel GetQuizDetails(long quizId, bool isMasterFolder, bool isTurnedOffExistingQuiz)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if(isMasterFolder && !isTurnedOffExistingQuiz)
                {
                    return (from mq in dbContext.MasterQuizs
                            join mques in dbContext.MasterQuestions on mq.MasterQuizId equals mques.MasterQuizId
                            into d
                            from data in d.DefaultIfEmpty()
                            where mq.MasterQuizId == quizId
                            select new CompanyQuizViewModel()
                            {
                                QuizId = mq.MasterQuizId,
                                QuizName = mq.QuizName,
                                QuestionAnswers = (from mq in dbContext.MasterQuestions
                                                   where mq.MasterQuizId == quizId
                                                   select new QuestionAnswerViewModel()
                                                   {
                                                       Question = new QuestionViewModel() { QuestionId = mq.MasterQuestionsId, Question = mq.Question },
                                                       Answers = (from mans in dbContext.MasterAnswers
                                                                  where mans.QuestionId == mq.MasterQuestionsId
                                                                  select new AnswerViewModel()
                                                                  {
                                                                      AnswerId = mans.MasterAnswersId,
                                                                      Answer = mans.Options,
                                                                      IsRightAnswer = mans.IsAnswer
                                                                  }).ToList(),
                                                       RightAnsId = (from mans in dbContext.MasterAnswers
                                                                     where mans.QuestionId == mq.MasterQuestionsId && mans.IsAnswer
                                                                     select mans.MasterAnswersId).FirstOrDefault()
                                                   }).ToList()
                            }).FirstOrDefault();
                }
                else
                {
                    return (from cq in dbContext.CompanyQuizs
                            join cques in dbContext.CompanyContentQuestions on cq.QuizId equals cques.QuizId
                            into d
                            from data in d.DefaultIfEmpty()
                            where cq.QuizId == quizId
                            select new CompanyQuizViewModel()
                            {
                                QuizId = cq.QuizId,
                                QuizName = cq.Name,
                                QuestionAnswers = (from cq in dbContext.CompanyContentQuestions
                                                   where cq.QuizId == quizId
                                                   select new QuestionAnswerViewModel()
                                                   {
                                                       Question = new QuestionViewModel() { QuestionId = cq.CompanyContentQuestionId, Question = cq.Question },
                                                       Answers = (from cans in dbContext.CompanyContentAnswers
                                                                  where cans.CompanyQuestionId == cq.CompanyContentQuestionId
                                                                  select new AnswerViewModel()
                                                                  {
                                                                      AnswerId = cans.CompanyContentAnswerId,
                                                                      Answer = cans.Options,
                                                                      IsRightAnswer = cans.IsAnswer
                                                                  }).ToList(),
                                                       RightAnsId = (from cans in dbContext.CompanyContentAnswers
                                                                     where cans.CompanyQuestionId == cq.CompanyContentQuestionId && cans.IsAnswer
                                                                     select cans.CompanyContentAnswerId).FirstOrDefault()
                                                   }).ToList()
                            }).FirstOrDefault();
                }
            }
        }

        public long SaveQuiz(CompanyQuizCreateViewModel companyQuiz)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                bool isExistingQuiz = dbContext.CompanyQuizs.Any(cq => !cq.IsDeleted && cq.IsPlaylistContentQuiz == companyQuiz.IsPlaylistContentQuiz && cq.IsMasterContent == companyQuiz.IsMasterContent && cq.ContentId == companyQuiz.ContentId);
                if (isExistingQuiz)
                {
                    return Constants.QUIZ_ALREADY_EXISTS;
                }
                CompanyQuiz newCompanyQuiz = new CompanyQuiz()
                {
                    Name = companyQuiz.QuizName,
                    IsPlaylistContentQuiz = companyQuiz.IsPlaylistContentQuiz,
                    IsMasterContent = companyQuiz.IsMasterContent,
                    CreatedDate = DateTime.UtcNow,
                    ContentId = companyQuiz.ContentId,
                    CreatedBy = companyQuiz.UserId
                };
                dbContext.CompanyQuizs.Add(newCompanyQuiz);
                dbContext.SaveChanges();
                return newCompanyQuiz.QuizId;
            }
        }

        public void UpdateQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                questionAnswerViewModel.ForEach(question =>
                {
                    CompanyContentQuestion cq = dbContext.CompanyContentQuestions.Where(obj => obj.CompanyContentQuestionId == question.Question.QuestionId).SingleOrDefault();
                    if (cq != null)
                    {
                        cq.Question = question.Question.Question;
                        cq.ModifiedBy = userId;
                        cq.ModifiedDate = DateTime.UtcNow;
                        cq.CompanyContentAnswers.ToList().ForEach(obj =>
                        {
                            obj.Options = question.Answers.Where(o => o.AnswerId == obj.CompanyContentAnswerId).Select(a => a.Answer).FirstOrDefault();
                            obj.IsAnswer = question.Answers.Where(o => o.AnswerId == obj.CompanyContentAnswerId).Select(a => a.IsRightAnswer).FirstOrDefault();
                            obj.ModifiedBy = userId;
                            obj.ModifiedDate = DateTime.UtcNow;
                        });
                        dbContext.SaveChanges();
                    }
                });
            }
        }

        /// <summary>
        /// method to get the quiz to play
        /// Note : isMasterContent is true when IsMasterLibraryFolder is true
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="isMasterContent"></param>
        /// <param name="isTurnedOffExistingQuiz"></param>
        /// <returns></returns>
        public CompanyQuizViewModel GetQuizToPlay(long quizId, long userId , bool isMasterContent, bool isTurnedOffExistingQuiz) 
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var isMasterQuiz = (isMasterContent && !isTurnedOffExistingQuiz) == true ? true : false;

                return (from m in dbContext.PublishedPlaylistQuiz
                        where !m.IsDeleted && m.PublishedPlaylistQuizId == quizId
                        select new CompanyQuizViewModel()
                        {
                            QuizId = m.PublishedPlaylistQuizId,
                            QuizName = m.Name,
                            QuestionAnswers = (from mq in dbContext.PublishedPlaylistQuestion
                                               where mq.PublishedPlaylistQuizId == quizId
                                               select new QuestionAnswerViewModel()
                                               {
                                                   Question = new QuestionViewModel() { QuestionId = mq.PublishedPlaylistQuestionId, Question = mq.Question },
                                                   Answers = (from mans in dbContext.PublishedPlaylistAnswer
                                                              where mans.PublishedPlaylistQuestionId == mq.PublishedPlaylistQuestionId
                                                              select new AnswerViewModel()
                                                              {
                                                                  AnswerId = mans.PublishedPlaylistAnswerId,
                                                                  Answer = mans.Options
                                                              }).ToList()
                                               }).ToList()
                        }).FirstOrDefault();

                //    if (isMasterQuiz)
                //    {
                //        return (from m in dbContext.PublishedPlaylistQuiz
                //                where !m.IsDeleted && m.PublishedPlaylistQuizId == quizId
                //                select new CompanyQuizViewModel()
                //                {
                //                    QuizId = m.PublishedPlaylistQuizId,
                //                    QuizName = m.Name,
                //                    QuestionAnswers = (from mq in dbContext.PublishedPlaylistQuestion
                //                                       where mq.PublishedPlaylistQuizId == quizId
                //                                       select new QuestionAnswerViewModel()
                //                                       {
                //                                           Question = new QuestionViewModel() { QuestionId = mq.PublishedPlaylistQuestionId, Question = mq.Question },
                //                                           Answers = (from mans in dbContext.PublishedPlaylistAnswer
                //                                                      where mans.PublishedPlaylistQuestionId == mq.PublishedPlaylistQuestionId
                //                                                      select new AnswerViewModel()
                //                                                      {
                //                                                          AnswerId = mans.PublishedPlaylistAnswerId,
                //                                                          Answer = mans.Options
                //                                                      }).ToList()
                //                                       }).ToList()
                //                }).FirstOrDefault();

                //        //return (from m in dbContext.MasterQuizs
                //        //        where !m.IsDeleted && m.MasterQuizId == quizId 
                //        //        select  new CompanyQuizViewModel()
                //        //        {
                //        //            QuizId = m.MasterQuizId,
                //        //            QuizName = m.QuizName,
                //        //            QuestionAnswers = (from mq in dbContext.MasterQuestions
                //        //                               where mq.MasterQuizId == quizId
                //        //                               select new QuestionAnswerViewModel()
                //        //                               {
                //        //                                   Question = new QuestionViewModel() { QuestionId = mq.MasterQuestionsId, Question = mq.Question },
                //        //                                   Answers = (from mans in dbContext.MasterAnswers
                //        //                                              where mans.QuestionId == mq.MasterQuestionsId
                //        //                                              select new AnswerViewModel()
                //        //                                              {
                //        //                                                  AnswerId = mans.MasterAnswersId,
                //        //                                                  Answer = mans.Options
                //        //                                              }).ToList()
                //        //                               }).ToList()
                //        //        }).FirstOrDefault();
                //    }
                //    else
                //    {
                //        return (from c in dbContext.CompanyQuizs
                //                where !c.IsDeleted && c.QuizId == quizId
                //                select new CompanyQuizViewModel()
                //                {
                //                    QuizId = c.QuizId,
                //                    QuizName = c.Name,
                //                    QuestionAnswers = (from cq in dbContext.CompanyContentQuestions
                //                                       where cq.QuizId == quizId
                //                                       select new QuestionAnswerViewModel()
                //                                       {
                //                                           Question = new QuestionViewModel() { QuestionId = cq.CompanyContentQuestionId, Question = cq.Question },
                //                                           Answers = (from cans in dbContext.CompanyContentAnswers
                //                                                      where cans.CompanyQuestionId == cq.CompanyContentQuestionId
                //                                                      select new AnswerViewModel()
                //                                                      {
                //                                                          AnswerId = cans.CompanyContentAnswerId,
                //                                                          Answer = cans.Options
                //                                                      }).ToList()
                //                                       }).ToList()
                //                }).FirstOrDefault();
                //    }
                //}
            }
        }

        public PassFailViewModel SubmitQuiz(QuestionAnswerAttemptViewModel quizResponse)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        dbContext.UserAnsweredQuizs.RemoveRange(dbContext.UserAnsweredQuizs.Where(uaq => uaq.UserId == quizResponse.UserId && uaq.PlayListId == quizResponse.PlaylistId && uaq.QuizId == quizResponse.QuizId && uaq.IsMasterQuiz == quizResponse.IsMasterQuiz).ToList());
                        quizResponse.QuestionAttempt.ForEach(a =>
                        {
                            dbContext.UserAnsweredQuizs.Add(new UserAnsweredQuiz
                            {
                                UserId = quizResponse.UserId,
                                PlayListId = quizResponse.PlaylistId,
                                QuestionId = a.QuestionId,
                                AnswereId = (int)a.AnswerId, 
                                QuizId = quizResponse.QuizId,
                                IsMasterQuiz = quizResponse.IsMasterQuiz,
                                CreatedBy = quizResponse.UserId,
                                CreatedDate = DateTime.UtcNow
                            });
                        });
                        dbContext.SaveChanges();

                        int rightAnsCount = 0;
                        int wrongAnsCount = 0;

                        //if (!quizResponse.IsMasterQuiz)
                        //{
                            quizResponse.QuestionAttempt.ForEach(a =>
                            {
                                var rightAnsId = (from cans in dbContext.PublishedPlaylistAnswer
                                                  where cans.PublishedPlaylistQuestionId == a.QuestionId && cans.IsAnswer
                                                  select cans.PublishedPlaylistAnswerId).FirstOrDefault();
                                if (a.AnswerId == rightAnsId)
                                {
                                    rightAnsCount++;
                                }
                                else
                                {
                                    wrongAnsCount++;
                                }
                            });
                        //}
                        //else
                        //{
                        //    quizResponse.QuestionAttempt.ForEach(a =>
                        //    {
                        //        var rightAnsId = (from mans in dbContext.MasterAnswers
                        //                          where mans.QuestionId == a.QuestionId && mans.IsAnswer
                        //                          select mans.MasterAnswersId).FirstOrDefault();
                        //        if (a.AnswerId == rightAnsId)
                        //        {
                        //            rightAnsCount++;
                        //        }
                        //        else
                        //        {
                        //            wrongAnsCount++;
                        //        }
                        //    });
                        //}

                        //update marks in userquizresult table for user
                        int marksObtained = ((rightAnsCount * Constants.CorrectAnswerMarks) - (wrongAnsCount * Constants.WrongAnswerMarks));
                        int totalMarks = (quizResponse.QuestionAttempt.Count() * Constants.CorrectAnswerMarks);
                        bool isUserPass = (((double)marksObtained/(double)totalMarks) * 100) > Constants.PassingPercentage ? true : false;
                        
                        var userQuizResult = dbContext.UserQuizResults.Where(uq => uq.UserId == quizResponse.UserId && uq.PlaylistId == quizResponse.PlaylistId && uq.QuizId == quizResponse.QuizId && uq.IsMasterQuiz == quizResponse.IsMasterQuiz).FirstOrDefault();
                        if (userQuizResult != null)
                        {
                            userQuizResult.NumberOfAttempt = userQuizResult.NumberOfAttempt + 1; // increase the number of attempt by 1
                            userQuizResult.MarksObtained = marksObtained;
                            userQuizResult.IsPassed = isUserPass;
                            userQuizResult.IsComplete = true;
                        }
                        else
                        {
                          userQuizResult =  dbContext.UserQuizResults.Add(new UserQuizResult
                          {
                              UserId = quizResponse.UserId,
                              PlaylistId = quizResponse.PlaylistId,
                              QuizId = quizResponse.QuizId,
                              IsMasterQuiz = quizResponse.IsMasterQuiz,
                              MarksObtained = marksObtained,
                              TotalMarks = totalMarks,
                              IsPassed = isUserPass,
                              IsComplete = true,
                              NumberOfAttempt = 1,
                              TotalNumberOfQuestion = quizResponse.QuestionAttempt.Count(),
                              TotalCorrectAnswer = rightAnsCount,
                              PlayListContentMasterId = quizResponse.PlaylistContentMasterId
                          });
                        }

                        //UpdatePlaylistStatus(dbContext, quizResponse.PlaylistId, quizResponse.UserId);

                        dbContext.SaveChanges();
                     
                        transaction.Commit();

                        //now need to check whether user has completed the playlist after finishing this quiz, if yes need to mark playlist completed for PARTICULAR USER
                        UpdatePlaylistStatus(dbContext, quizResponse.PlaylistId, quizResponse.UserId);

                        if (isUserPass)
                        {
                            return new PassFailViewModel
                            {
                                PlaylistId = quizResponse.PlaylistId,
                                UserId = quizResponse.UserId,
                                QuizId = quizResponse.QuizId,
                                IsMasterQuiz = quizResponse.IsMasterQuiz,
                                IsUserPassed = isUserPass,
                                NumberOfQuizAttempts = userQuizResult.NumberOfAttempt,
                                NumberOfRightAnswer = rightAnsCount,
                                NumberOfWrongAnswer = wrongAnsCount,
                                MarksObtained = marksObtained,
                                TotalMarks = totalMarks
                            };
                        }
                        else return new PassFailViewModel { PlaylistId = quizResponse.PlaylistId, UserId = quizResponse.UserId, QuizId = quizResponse.QuizId, IsMasterQuiz = quizResponse.IsMasterQuiz, IsUserPassed = isUserPass, NumberOfQuizAttempts = userQuizResult.NumberOfAttempt, };
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        } 

        //private method
        private void UpdatePlaylistStatus(VirtualBrandWorksEntities dbContext, long playListId, long userId)
        {
                var query = "Exec UpdatePlaylistStatus @playListId, @userId";

                var parameters = new List<SqlParameter> {
                        new SqlParameter("@playListId", playListId),
                        new SqlParameter("@userId",userId)
                    };

                var response = dbContext.Database.SqlQuery<int>(query, parameters.ToArray()).ToList();
                dbContext.SaveChanges();
        }
    }
}