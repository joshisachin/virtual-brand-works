﻿using System;
using Repository.Interfaces;
using System.Linq;
using VirtualBrandWork.Common;
using Repository.Model;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Entity;
using System.Data.Entity.Validation;

namespace Repository.Repository
{
    public class QuizRepository : IQuizRepository
    {
        public Response AddQuestion(QuestionAnswerViewModel questionAnswerViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                try
                {
                    MasterQuestion mq = new MasterQuestion();
                    mq.Question = questionAnswerViewModel.Question.Question;
                    mq.MasterQuizId = questionAnswerViewModel.QuizId;
                    questionAnswerViewModel.Answers.ForEach(obj =>
                    mq.MasterAnswers.Add(new MasterAnswer()
                    {
                        Options = obj.Answer,
                        IsAnswer = obj.IsRightAnswer,
                        QuestionId = mq.MasterQuestionsId
                    }));
                    dbContext.MasterQuestions.Add(mq);
                    dbContext.MasterAnswers.AddRange(mq.MasterAnswers);
                    dbContext.SaveChanges();
                    return new Response() { Status = true, Message = Constants.QUESTION_ADDED };
                }
                catch (Exception e)
                {
                    DbEntityValidationException ex = e is DbEntityValidationException ? e as DbEntityValidationException : null;
                    if (ex == null) { throw e; }
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join(";", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, ";" + "The validation errors are: ;", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }

        public Response DeleteQuiz(long quizId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                try
                {
                    var quiz = dbContext.MasterQuizs.Where(obj => obj.MasterQuizId == quizId).FirstOrDefault();
                    if (quiz != null)
                    {
                        var questions = dbContext.MasterQuestions.Where(obj => obj.MasterQuizId == quizId).ToList();
                        var answers = (from q in questions
                                       join a in dbContext.MasterAnswers on q.MasterQuestionsId equals a.QuestionId
                                       where q.MasterQuizId == quizId
                                       select a
                                       ).ToList();
                        dbContext.MasterAnswers.RemoveRange(answers);
                        dbContext.MasterQuestions.RemoveRange(questions);
                        dbContext.MasterQuizs.Remove(quiz);
                        dbContext.SaveChanges();
                        return new Response() { Status = true, Message = Constants.QUIZ_DELETED };
                    }
                    return new Response() { Status = false, Message = Constants.NO_QUIZ_FOUND };
                }
                catch (Exception e)
                {
                    DbEntityValidationException ex = e is DbEntityValidationException ? e as DbEntityValidationException : null;
                    if (ex == null) { throw e; }
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join(";", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, ";" + "The validation errors are: ;", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }

        public QuestionAnswerViewModel GetQuestionDetails(long questionId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                try
                {
                    return (from mq in dbContext.MasterQuestions
                            where mq.MasterQuestionsId == questionId
                            select new QuestionAnswerViewModel()
                            {
                                Question = new QuestionViewModel() { QuestionId = mq.MasterQuestionsId, Question = mq.Question },
                                Answers = (from mans in dbContext.MasterAnswers
                                           where mans.QuestionId == mq.MasterQuestionsId
                                           select new AnswerViewModel()
                                           {
                                               AnswerId = mans.MasterAnswersId,
                                               Answer = mans.Options,
                                               IsRightAnswer = mans.IsAnswer
                                           }).ToList()
                            }).FirstOrDefault();
                }
                catch (Exception e)
                {
                    DbEntityValidationException ex = e is DbEntityValidationException ? e as DbEntityValidationException : null;
                    if (ex == null) { throw e; }
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join(";", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, ";" + "The validation errors are: ;", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }

        public QuizViewModel GetQuizDetails(long quizId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                try
                {
                    return (from mq in dbContext.MasterQuizs
                            join mques in dbContext.MasterQuestions on mq.MasterQuizId equals mques.MasterQuizId
                            into d
                            from data in d.DefaultIfEmpty()
                            where mq.MasterQuizId == quizId
                            select new QuizViewModel()
                            {
                                QuizId = mq.MasterQuizId,
                                QuizName = mq.QuizName,
                                QuizShortName = mq.QuizShortName,
                                QuizCreatedDate = mq.CreatedDate,
                                QuizModifiedDate = mq.ModifiedDate,
                                QuestionAsnwers = (from mq in dbContext.MasterQuestions
                                                   where mq.MasterQuizId == quizId
                                                   select new QuestionAnswerViewModel()
                                                   {
                                                       Question = new QuestionViewModel() { QuestionId = mq.MasterQuestionsId, Question = mq.Question },
                                                       Answers = (from mans in dbContext.MasterAnswers
                                                                  where mans.QuestionId == mq.MasterQuestionsId
                                                                  select new AnswerViewModel()
                                                                  {
                                                                      AnswerId = mans.MasterAnswersId,
                                                                      Answer = mans.Options,
                                                                      IsRightAnswer = mans.IsAnswer
                                                                  }).ToList(),
                                                       RightAnsId = (from mans in dbContext.MasterAnswers
                                                                     where mans.QuestionId == mq.MasterQuestionsId && mans.IsAnswer
                                                                     select mans.MasterAnswersId).FirstOrDefault()
                                                   }).ToList()
                            }).FirstOrDefault();
                }
                catch (Exception e)
                {
                    DbEntityValidationException ex = e is DbEntityValidationException ? e as DbEntityValidationException : null;
                    if (ex == null) { throw e; }
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join(";", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, ";" + "The validation errors are: ;", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }

        public Response SaveQuiz(QuizViewModel quizViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                try
                {
                    var isQuizExist = dbContext.MasterQuizs.Select(obj => obj).Where(o => o.QuizName.ToLower().Equals(quizViewModel.QuizName) && o.MasterContentId == quizViewModel.ContentId).FirstOrDefault();
                    if (isQuizExist == null)
                    {
                        dbContext.MasterQuizs.Add(new MasterQuiz() { QuizName = quizViewModel.QuizName, CreatedDate = DateTime.UtcNow, QuizShortName = quizViewModel.ShortName, MasterContentId = quizViewModel.ContentId });
                        dbContext.SaveChanges();
                        return new Response() { Status = true, Message = Constants.QUIZ_ADDED };
                    }
                    else
                    {
                        return new Response() { Status = false, Message = Constants.QUIZ_NAME_ALREADY_EXIST };

                    }
                }
                catch (Exception e)
                {
                    DbEntityValidationException ex = e is DbEntityValidationException ? e as DbEntityValidationException : null;
                    if (ex == null) { throw e; }
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join(";", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, ";" + "The validation errors are: ;", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }

        public Response UpdateQuestion(QuestionAnswerViewModel questionAnswerViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                try
                {
                    MasterQuestion mq = dbContext.MasterQuestions.Where(obj => obj.MasterQuestionsId == questionAnswerViewModel.Question.QuestionId).FirstOrDefault();
                    if (mq != null)
                    {
                        mq.Question = questionAnswerViewModel.Question.Question;
                        mq.MasterAnswers.ToList().ForEach(obj =>
                        {
                            obj.Options = questionAnswerViewModel.Answers.Where(o => o.AnswerId == obj.MasterAnswersId).Select(a => a.Answer).FirstOrDefault();
                            obj.IsAnswer = questionAnswerViewModel.Answers.Where(o => o.AnswerId == obj.MasterAnswersId).Select(a => a.IsRightAnswer).FirstOrDefault();
                        });

                        dbContext.SaveChanges();
                        return new Response() { Status = true, Message = Constants.QUESTION_UPDATED };
                    }
                    return new Response() { Status = false, Message = Constants.NO_QUESTION_FOUND };
                }
                catch (Exception e)
                {
                    DbEntityValidationException ex = e is DbEntityValidationException ? e as DbEntityValidationException : null;
                    if (ex == null) { throw e;}
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join(";", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, ";" + "The validation errors are: ;", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }


        public Response DeleteQuestion(long questionId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                try
                {
                    var ques = dbContext.MasterQuestions.Where(obj => obj.MasterQuestionsId == questionId).FirstOrDefault();
                    if (ques != null)
                    {
                        var answers = (from a in dbContext.MasterAnswers 
                                       where a.QuestionId == questionId
                                       select a
                                       ).ToList();
                        dbContext.MasterAnswers.RemoveRange(answers);
                        dbContext.MasterQuestions.Remove(ques);
                        dbContext.SaveChanges();
                        return new Response() { Status = true, Message = Constants.QUESTION_DELETED };
                    }
                    return new Response() { Status = false, Message = Constants.NO_QUESTION_FOUND };
                }
                catch (Exception e)
                {
                    DbEntityValidationException ex = e is DbEntityValidationException ? e as DbEntityValidationException : null;
                    if (ex == null) { throw e; }
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join(";", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, ";" + "The validation errors are: ;", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }
    }
}
