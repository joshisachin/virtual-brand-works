﻿using AutoMapper;
using Repository.Helpers;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class BuildPlaylistRepository : IBuildPlaylistRepository
    {
        public int SavePlaylistFolders(long playlistId , long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (!dbContext.PlayLists.Any(pl => pl.PlayListId == playlistId && !pl.IsLaunched))
                {
                    return (int)Constants.SavePlaylistFolderStatus.PlaylistDoNotExist;
                }
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        List<PlaylistFolderUserSavedViewModel> playlistFolders = ((from m in dbContext.MasterFolderUserSaved
                                                                                   where m.UserId == userId
                                                                                   select new PlaylistFolderUserSavedViewModel
                                                                                   {
                                                                                       UserId = userId,
                                                                                       FolderId = m.FolderId,
                                                                                       IsMasterLibraryFolder = true
                                                                                   }).Union(from c in dbContext.CompanyContentFolderUserSaved
                                                                                            where c.UserId == userId
                                                                                            select new PlaylistFolderUserSavedViewModel
                                                                                            {
                                                                                                UserId = userId,
                                                                                                FolderId = c.FolderId,
                                                                                                IsMasterLibraryFolder = false
                                                                                            })).ToList();

                        if (playlistFolders.Count == 0)
                        {
                            return (int)Constants.SavePlaylistFolderStatus.FolderCountZero;
                        }

                        var folderOrder = dbContext.PlayListMasters.Where(pm => pm.PlayListId == playlistId).Count();
                        playlistFolders.ForEach(p =>
                        {
                            if (dbContext.PlayListMasters.Any(m => m.PlayListId == playlistId && m.FolderId == p.FolderId && m.IsFromMasterLibrary == p.IsMasterLibraryFolder) == false)
                            {
                                folderOrder++;
                                PlayListMaster master = new PlayListMaster
                                {
                                    PlayListId = playlistId,
                                    FolderId = (int)p.FolderId,
                                    SortOrder = folderOrder,
                                    IsFromMasterLibrary = p.IsMasterLibraryFolder
                                };
                                dbContext.PlayListMasters.Add(master);
                                dbContext.SaveChanges();

                                if (p.IsMasterLibraryFolder)
                                {
                                    int order = 0;
                                    dbContext.MasterContents.Where(mc => !mc.IsDeleted && mc.IsPublished && mc.MasterFolderId == p.FolderId).Select(o => o).ToList().ForEach(masterContent =>
                                    {
                                        order++;
                                        dbContext.PlayListContentMasters.Add(new PlayListContentMaster()
                                        {
                                            PlayListMasterId = master.PlayListMasterId,
                                            ContentId = masterContent.MasterContentId,
                                            SortOrder = order,
                                        });
                                    });
                                }
                                else
                                {
                                    int order = 0;
                                    dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.IsPublished && c.FolderId == p.FolderId).Select(o => o).ToList().ForEach(companyContent =>
                                    {
                                        order++;
                                        dbContext.PlayListContentMasters.Add(new PlayListContentMaster()
                                        {
                                            PlayListMasterId = master.PlayListMasterId,
                                            ContentId = companyContent.CompanyLibraryContentId,
                                            SortOrder = order,
                                        });
                                    });
                                }
                            }
                        });
                        dbContext.SaveChanges();

                        var playlistMaster = dbContext.PlayListMasters.Where(m => m.PlayListId == playlistId).ToList();
                        playlistMaster.ForEach(m =>
                        {
                            if (playlistFolders.Any(p => p.FolderId == m.FolderId && p.IsMasterLibraryFolder == m.IsFromMasterLibrary) == false)
                            {
                                dbContext.PlayListContentMasters.RemoveRange(dbContext.PlayListContentMasters.Where(c => c.PlayListMasterId == m.PlayListMasterId).ToList());
                                dbContext.PlayListMasters.Remove(m);
                            }
                        });
                        dbContext.SaveChanges();

                        var playlistSavedState = dbContext.BuildPlaylistSavedState.Where(b => b.PlaylistId == playlistId).FirstOrDefault();
                        if (playlistSavedState != null /*&& playlistSavedState.StepNumber < (int)Constants.BuildPlaylistStage.Two*/)
                        {
                            playlistSavedState.StepNumber = (int)Constants.BuildPlaylistStage.Two;
                            dbContext.SaveChanges();
                        }

                        transaction.Commit();
                        return (int)Constants.SavePlaylistFolderStatus.OK;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// method to get playlist folders , playlistId is required to identify whether playlist exists or not
        /// </summary>
        /// <param name="playlistId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<PlaylistFolderViewModel> GetPlaylistFolders(long playlistId, long userId, bool isAllContentCount)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return ((from p in dbContext.PlayLists
                         join pf in dbContext.PlayListMasters on p.PlayListId equals pf.PlayListId
                         join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                         where pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                         select new PlaylistFolderViewModel
                         {
                             PlayListFolderId = pf.PlayListMasterId,
                             PlayListId = pf.PlayListId,
                             PlaylistName = p.Name,
                             FolderId = pf.FolderId,
                             FolderName = mf.Name,
                             FolderShortName = mf.ShortName.ToLower(),
                             IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                             SortOrder = pf.SortOrder,
                             NumberOfContent = isAllContentCount ? dbContext.MasterContents.Where(mc => !mc.IsDeleted && mc.IsPublished && mc.MasterFolderId == pf.FolderId).ToList().Count() : dbContext.PlayListContentMasters.Where(pc => !pc.IsDeleted && pc.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                         }).Union(from p in dbContext.PlayLists
                                  join pf in dbContext.PlayListMasters on p.PlayListId equals pf.PlayListId
                                  join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                  where !pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                                  select new PlaylistFolderViewModel()
                                  {
                                      PlayListFolderId = pf.PlayListMasterId,
                                      PlayListId = pf.PlayListId,
                                      PlaylistName = p.Name,
                                      FolderId = pf.FolderId,
                                      FolderName = cf.Name,
                                      FolderShortName = cf.ShortName.ToLower(),
                                      IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                      SortOrder = pf.SortOrder,
                                      NumberOfContent = isAllContentCount ? dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.IsPublished && c.FolderId == pf.FolderId).ToList().Count() : dbContext.PlayListContentMasters.Where(pc => !pc.IsDeleted && pc.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                  })).OrderBy(p => p.SortOrder).ToList();
            }
        }

        public void UpdateSortOrderForPlaylistFolderUserSaved(List<PlaylistFolderUserSavedViewModel> folders)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                foreach (var folder in folders)
                {
                    PlaylistFolderUserSaved savedFolder = dbContext.PlaylistFolderUserSaved.Where(sf => sf.PlaylistFolderUserSavedId == folder.PlaylistFolderUserSavedId).FirstOrDefault();
                    if (savedFolder != null)
                    {
                        savedFolder.SortOrder = folder.SortOrder;
                        dbContext.SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// method to create a playlist
        /// </summary>
        /// <param name="playlist"></param>
        public long CreatePlaylist(PlaylistViewModel playlist)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {  //Create a PlayList
                            PlayList newPlaylist = new PlayList
                            {
                                Name = playlist.Name,
                                Description = playlist.Description,
                                CreatedBy = playlist.UserId,
                                CreatedDate = DateTime.UtcNow
                            };
                            dbContext.PlayLists.Add(newPlaylist);
                            dbContext.SaveChanges();

                            //Save the playlist step number for the user
                            dbContext.BuildPlaylistSavedState.Add(new BuildPlaylistSavedState
                            {
                                UserId = playlist.UserId,
                                StepNumber = (int)Constants.BuildPlaylistStage.One,
                                PlaylistId = newPlaylist.PlayListId
                            });
                            dbContext.SaveChanges();

                        transaction.Commit();
                        return newPlaylist.PlayListId;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }

            }
        }

        public PlaylistViewModel GetPlaylistForUpdate(long playlistId, long userId)
        {
            using(VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PlaylistViewModel playlist = (from p in dbContext.PlayLists
                                              where !p.IsDeleted && !p.IsLaunched && p.PlayListId == playlistId
                                              select new PlaylistViewModel
                                              {
                                                  PlayListId = p.PlayListId,
                                                  Name = p.Name,
                                                  Description = p.Description
                                              }).FirstOrDefault();

                return playlist;
            }
        }


        /// <summary>
        /// PlayListFolderId: folder Id which is saved for a playlist for a particular user
        /// </summary>
        /// <param name="PlayListFolderId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public FolderContentViewModel GetPlayListContents(long playListFolderId, bool isPlaylistExist, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (dbContext.PlayListMasters.Any(obj => obj.PlayListMasterId == playListFolderId && obj.IsFromMasterLibrary))
                    return (from pf in dbContext.PlayListMasters
                            join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                            where pf.PlayListMasterId == playListFolderId && pf.IsFromMasterLibrary
                            select new FolderContentViewModel()
                            {
                                Folder = new FolderViewModel() { FolderId = mf.MasterFolderId, Name = mf.Name, ShortName = mf.ShortName.ToLower(), Description = string.Empty, IsMasterFolder = true, NumberOfContent = dbContext.MasterContents.Where(c => !c.IsDeleted && c.MasterFolderId == mf.MasterFolderId && c.IsPublished).ToList().Count() },
                                Contents = (from mc in dbContext.MasterContents
                                            join saveContent in dbContext.PlayListContentMasters on mc.MasterContentId equals saveContent.ContentId into sg
                                            from selectFolder in sg.Where(x => x.PlayListMasterId == playListFolderId).DefaultIfEmpty()
                                            where mc.IsPublished == true && !mc.IsDeleted && mc.MasterFolderId == mf.MasterFolderId
                                            select new ContentViewModel()
                                            {
                                                ContentId = mc.MasterContentId,
                                                ContentName = mc.Name,
                                                CreatedDate = mc.CreatedDate,
                                                ModifiedDate = mc.ModifiedDate,
                                                ShortName = mc.ShortName,
                                                FolderName = mf.Name,
                                                FolderId = mf.MasterFolderId,
                                                ContentURL = mc.ContentUrl,
                                                ContentDescription = mc.Description,
                                                ViewCount = 0,
                                                SortOrder = selectFolder.SortOrder,
                                                IsContentSaved = (selectFolder != null) ? true : false,
                                                MimeType = mc.MimeType
                                            }).ToList()
                            }).FirstOrDefault();
                else return (from pf in dbContext.PlayListMasters
                             join clf in dbContext.CompanyLibraryFolders on pf.FolderId equals clf.CompanyLibraryFolderId
                             where pf.PlayListMasterId == playListFolderId && !pf.IsFromMasterLibrary
                             select new FolderContentViewModel()
                             {
                                 Folder = new FolderViewModel() { FolderId = clf.CompanyLibraryFolderId, Name = clf.Name, ShortName = clf.ShortName.ToLower(), Description = string.Empty, IsMasterFolder = false, NumberOfContent = dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.FolderId == clf.CompanyLibraryFolderId && c.IsPublished).ToList().Count() },
                                 Contents = (from cc in dbContext.CompanyLibraryContents
                                             join saveContent in dbContext.PlayListContentMasters on cc.CompanyLibraryContentId equals saveContent.ContentId into sg
                                             from selectFolder in sg.Where(x => x.PlayListMasterId == playListFolderId).DefaultIfEmpty()
                                             where cc.IsPublished == true && !cc.IsDeleted && cc.FolderId == clf.CompanyLibraryFolderId
                                             select new ContentViewModel()
                                             {
                                                 ContentId = cc.CompanyLibraryContentId,
                                                 ContentName = cc.Name,
                                                 CreatedDate = cc.CreatedDate,
                                                 ModifiedDate = cc.ModifiedDate,
                                                 ShortName = cc.ShortName,
                                                 FolderName = clf.Name,
                                                 FolderId = clf.CompanyLibraryFolderId,
                                                 ContentURL = cc.ContentUrl,
                                                 ContentDescription = cc.Description,
                                                 ViewCount = 0,
                                                 SortOrder = selectFolder.SortOrder,
                                                 IsContentSaved = (selectFolder != null) ? true : false,
                                                 MimeType = cc.MimeType
                                             }).ToList()
                             }).FirstOrDefault();

            }
        }



        public void UpdatePlayListContents(PlaylistContentViewModel playlistContentViewModel, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var folderId = playlistContentViewModel.PlaylistContents.Select(x => x.PlaylistFolderId).FirstOrDefault();
                dbContext.PlayListContentMasters.RemoveRange(dbContext.PlayListContentMasters.Where(c => c.PlayListMasterId == folderId).ToList());
                playlistContentViewModel.PlaylistContents.ForEach(obj =>
                {
                    dbContext.PlayListContentMasters.Add(new PlayListContentMaster
                    {
                        PlayListMasterId = obj.PlaylistFolderId,
                        ContentId = obj.PlaylistContentId,
                        SortOrder = obj.SortOrder,
                    });
                });
                dbContext.SaveChanges();
            }
        }


        public List<FolderViewModel> GetPlaylistFooterData(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from cs in dbContext.CompanyContentFolderUserSaved
                        join c in dbContext.CompanyLibraryFolders on cs.FolderId equals c.CompanyLibraryFolderId
                        where cs.UserId == userId
                        select new FolderViewModel()
                        {
                            FolderId = c.CompanyLibraryFolderId,
                            Name = c.Name,
                            ShortName = c.ShortName,
                            IsMasterFolder = false,
                            IsDynamicFolder = c.IsDynamicFolder
                        }).Union((from mfus in dbContext.MasterFolderUserSaved
                                  join mf in dbContext.MasterFolders on mfus.FolderId equals mf.MasterFolderId
                                  where mfus.UserId == userId
                                  select new FolderViewModel()
                                  {
                                      FolderId = mf.MasterFolderId,
                                      Name = mf.Name,
                                      ShortName = mf.ShortName,
                                      IsMasterFolder = true,
                                      IsDynamicFolder = (bool)mf.isDynamicFolder
                                  })).ToList();
            }
        }

        /// <summary>
        /// method to create state of a playling that is being build
        /// </summary>
        /// <param name="state"></param>
        public void SaveBuildPlaylistStatus(BuildPlaylistSavedStateViewModel state)
        {
            using(VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                BuildPlaylistSavedState newBuildPlaylistState = new BuildPlaylistSavedState
                {
                    UserId = state.UserId,
                    StepNumber = state.StepNumber
                };
                dbContext.BuildPlaylistSavedState.Add(newBuildPlaylistState);
                dbContext.SaveChanges();
            }
        }

        /// <summary>
        /// method to get the state of build playlist for a user, returns null if no corresponding data exists
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public BuildPlaylistSavedStateViewModel GetBuildPlaylistSavedState(long playlistId, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from b in dbContext.BuildPlaylistSavedState
                        where b.UserId == userId && b.PlaylistId == playlistId
                        select new BuildPlaylistSavedStateViewModel
                        {
                            UserId = b.UserId,
                            PlaylistId = b.PlaylistId,
                            StepNumber = b.StepNumber
                        }).FirstOrDefault();
            }
        }

        /// <summary>
        /// method to assign users or teams to playlist , along with details like startdate and enddate.
        /// </summary>
        /// <param name="playlistDetail"></param>
        public List<long> AssignPlaylistTo(PlaylistAssignToViewModel playlistDetail)
        {
            using(VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //For Replica to be inserted in PublishTeams and PublishUserTeams
                        //  long? publishedTeamId = dbContext.PlayListAssignedToes.Where(m => m.PlayListId == playlistDetail.PlayListId).FirstOrDefault()?.PublishedTeamId;
                        var publishedTeamIds = dbContext.PlayListAssignedToes.Where(m => m.IsAssignedToTeam && m.PlayListId == playlistDetail.PlayListId).ToList().Select(n => n.PublishedTeamId);

                        List<PlayListAssignedTo> objPlaylistAssignedTo = dbContext.PlayListAssignedToes.Where(m => m.PlayListId == playlistDetail.PlayListId).ToList();

                        List<long> usersToBeNotified = GetUsersForAssignPlaylistToMore(objPlaylistAssignedTo, playlistDetail);

                        var existingPlaylist = dbContext.PlayLists.Where(p => !p.IsDeleted && p.PlayListId == playlistDetail.PlayListId).FirstOrDefault();
                        if (existingPlaylist != null)
                        {
                            existingPlaylist.StartDate = playlistDetail.StartDate;
                            existingPlaylist.EndDate = playlistDetail.EndDate.AddDays(1).AddSeconds(-1); ;
                            existingPlaylist.LocationId = playlistDetail.LocationId;
                            dbContext.SaveChanges();

                            dbContext.PlayListAssignedToes.RemoveRange(dbContext.PlayListAssignedToes.Where(p => p.PlayListId == existingPlaylist.PlayListId).ToList());
                            dbContext.SaveChanges();

                            if (playlistDetail.Users.Count() > 0)
                            {
                                foreach (var userId in playlistDetail.Users)
                                {
                                    PlayListAssignedTo assignUser = new PlayListAssignedTo
                                    {
                                        PlayListId = existingPlaylist.PlayListId,
                                        UserId = userId,
                                        IsAssignedToTeam = false,
                                    };
                                    dbContext.PlayListAssignedToes.Add(assignUser);
                                }
                            }
                            if (playlistDetail.Teams.Count() > 0)
                            {
                                foreach (var teamId in playlistDetail.Teams)
                                {
                                    PlayListAssignedTo assignTeam = new PlayListAssignedTo
                                    {
                                        PlayListId = existingPlaylist.PlayListId,
                                        TeamId = teamId,
                                        IsAssignedToTeam = true
                                    };
                                    dbContext.PlayListAssignedToes.Add(assignTeam);
                                }
                            }
                            dbContext.SaveChanges();

                            //Save the playlist step number for the user
                            var playlistSavedState = dbContext.BuildPlaylistSavedState.Where(b => b.PlaylistId == playlistDetail.PlayListId).FirstOrDefault();
                            if (playlistSavedState != null /*&& playlistSavedState.StepNumber < (int)Constants.BuildPlaylistStage.Five*/)
                            {
                                playlistSavedState.StepNumber = (int)Constants.BuildPlaylistStage.Five;
                                dbContext.SaveChanges();
                            }

                            //if (publishedTeamId.HasValue && publishedTeamId.Value > 0)
                            //{
                            //    dbContext.PublishedUserTeams.RemoveRange(dbContext.PublishedUserTeams.Where(m => m.PublishedTeamId == publishedTeamId).ToList());

                            //    dbContext.PublishedTeams.Remove(dbContext.PublishedTeams.Where(m => m.PublishedTeamId == publishedTeamId).FirstOrDefault());

                            //    dbContext.SaveChanges();
                            //}
                            foreach(var id in publishedTeamIds)
                            {
                                dbContext.PublishedUserTeams.RemoveRange(dbContext.PublishedUserTeams.Where(m => m.PublishedTeamId == id).ToList());

                                dbContext.PublishedTeams.RemoveRange(dbContext.PublishedTeams.Where(m => m.PublishedTeamId == id).ToList());

                                dbContext.SaveChanges();
                            }

                            transaction.Commit(); 
                            
                            // calling sp to create replicas for assigned teams
                            CreatePublishedTeamReplica(dbContext, playlistDetail.PlayListId);

                            
                        }

                        return usersToBeNotified;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        private List<long> GetUsersForAssignPlaylistToMore(List<PlayListAssignedTo> objExistingPlaylistAssignedTo, PlaylistAssignToViewModel playlistNewUsersDetail)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                List<long> objExistingUsers = new List<long>();

                foreach (var item in objExistingPlaylistAssignedTo.Where(m=>m.TeamId == null))
                {
                    objExistingUsers.Add(item.UserId.Value);
                }

                foreach (var item in objExistingPlaylistAssignedTo.Where(m => m.UserId == null))
                {
                    objExistingUsers.AddRange(dbContext.UserTeams.Where(m => m.TeamId == item.TeamId).Select(m => m.UserId).ToList());
                }

                //New users
                List<long> objNewUsers = new List<long>();

                foreach (var item in playlistNewUsersDetail.Teams)
                {
                    objNewUsers.AddRange(dbContext.UserTeams.Where(m => m.TeamId == item).Select(m => m.UserId).ToList());
                }

                foreach (var item in playlistNewUsersDetail.Users)
                {
                    objNewUsers.Add(item);
                }

                var fromExisting = objExistingUsers.Except(objNewUsers).ToList();

                var fromNew = objNewUsers.Except(objExistingUsers).ToList();

                return fromExisting.Concat(fromNew).ToList();
            }
        }

        public PlaylistDetailViewModel GetPlaylistDetail(long playlistId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PlaylistDetailViewModel playlistDetails = new PlaylistDetailViewModel();
                PlayList playlist = dbContext.PlayLists.Where(p => !p.IsDeleted && p.PlayListId == playlistId).FirstOrDefault();
                if(playlist != null)
                {
                    playlistDetails.Name = playlist.Name;
                    playlistDetails.StartDate = playlist.StartDate.ToString();
                    playlistDetails.EndDate = playlist.EndDate.ToString();
                    playlistDetails.Location = dbContext.Cities.Where(c => c.ID == playlist.LocationId).FirstOrDefault()?.Name;
                }
                return playlistDetails;
            }
        }

        public PlaylistAssignedUserOrTeam GetPlaylistAssignedUserAndTeam(long playlistId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PlaylistAssignedUserOrTeam playlistAssignedTo = new PlaylistAssignedUserOrTeam();
                playlistAssignedTo.Users = (from a in dbContext.PlayListAssignedToes
                                                    join u in dbContext.Users on a.UserId equals u.UserId
                                                    where !u.IsDeleted && a.PlayListId == playlistId
                                                    select new UserDetailForTeamViewModel
                                                    {
                                                        UserId = u.UserId,
                                                        FirstName = u.FirstName,
                                                        LastName = u.LastName,
                                                        ProfilePicture = u.ProfilePicture
                                                    }).ToList();

                playlistAssignedTo.Teams = (from p in dbContext.PlayListAssignedToes
                                                    join t in dbContext.Teams on p.TeamId equals t.TeamId
                                                    where !t.isDeleted && p.PlayListId == playlistId
                                                    select new TeamDetailViewModel
                                                    {
                                                        TeamName = t.Name,
                                                        NumberOfUsers = dbContext.UserTeams.Where(ut => ut.TeamId == p.TeamId).ToList().Count()
                                                    }).ToList();
                return playlistAssignedTo;
            }
        }

        public List<long> LaunchPlaylist(PlaylistUpdateViewModel playlist)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {

                PlayList existingPlaylist = dbContext.PlayLists.Where(p => !p.IsDeleted && p.PlayListId == playlist.PlayListId).FirstOrDefault();
                if (existingPlaylist != null)
                {
                    existingPlaylist.IsLaunched = playlist.IsLaunched;
                    dbContext.SaveChanges();
                }
                UpdatePlaylistUsers(dbContext, playlist.PlayListId);
                // creating quizs for playlist on launch
                CreatePublishedQuizReplica(dbContext, playlist.PlayListId);

                return GetUsersFromTeam(GetPlaylistAssignedUserAndTeam(playlist.PlayListId));

            }
        }

        private List<long> GetUsersFromTeam(PlaylistAssignedUserOrTeam objPlaylistAssignedUserOrTeam)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                List<long> users = new List<long>();

                foreach (var item in objPlaylistAssignedUserOrTeam.Teams)
                {
                    users.AddRange(dbContext.UserTeams.Where(m => m.TeamId == item.TeamId).Select(m=>m.UserId).ToList());
                }

                foreach (var item in objPlaylistAssignedUserOrTeam.Users)
                {
                    users.Add(item.UserId);
                }
                
                return users;
            }
        }

        public PlaylistAssignedUserOrTeam GetUsersAndTeamsForAssignPlaylistToMore(long playlistId, int locationId,long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                //Save the playlist step number for the user
                var playlistSavedState = dbContext.BuildPlaylistSavedState.Where(b => b.PlaylistId == playlistId).FirstOrDefault();
                if (playlistSavedState != null /*&& playlistSavedState.StepNumber < (int)Constants.BuildPlaylistStage.Four*/)
                {
                    playlistSavedState.StepNumber = (int)Constants.BuildPlaylistStage.Four;
                    dbContext.SaveChanges();
                }

                var companyId = dbContext.Users.Where(u => !u.IsDeleted && u.UserId == userId).First().CompanyId;
                PlaylistAssignedUserOrTeam playlistAssignedTo = new PlaylistAssignedUserOrTeam();
                playlistAssignedTo.Users = (from u in dbContext.Users
                                                    join a in dbContext.PlayListAssignedToes on u.UserId equals a.UserId
                                                    into d
                                                    from data in d.Where(o => o.PlayListId == playlistId).DefaultIfEmpty()
                                                    where !u.IsDeleted && u.IsActive && u.CompanyId == companyId && (locationId == 0 || u.CityId == locationId)
                                                    select new UserDetailForTeamViewModel
                                                    {
                                                        UserId = u.UserId,
                                                        FirstName = u.FirstName,
                                                        LastName = u.LastName,
                                                        ProfilePicture = u.ProfilePicture,
                                                        IsSelected = data == null ? false : true,
                                                        TagList = (from ut in dbContext.UserTags
                                                                   join ta in dbContext.Tags on ut.TagId equals ta.TagId
                                                                   where !ta.IsDeleted && ut.UserId == u.UserId
                                                                   select ta.Name).ToList(),
                                                    }).ToList();

                playlistAssignedTo.Teams = (from t in dbContext.Teams
                                            join p in dbContext.PlayListAssignedToes on t.TeamId equals p.TeamId
                                            into d
                                            from data in d.Where(o => o.PlayListId == playlistId).DefaultIfEmpty()
                                            where !t.isDeleted && t.Status && t.CompanyId == companyId && (locationId == 0 || t.CityId == locationId)
                                            select new TeamDetailViewModel
                                            {
                                                TeamId = t.TeamId,
                                                TeamName = t.Name,
                                                IsSelected = data == null ? false : true,
                                                TagList = (from teamtag in dbContext.TeamTags
                                                           join ta in dbContext.Tags on teamtag.TagId equals ta.TagId
                                                           where !ta.IsDeleted && teamtag.TeamId == t.TeamId
                                                           select ta.Name).ToList(),
                                            }).ToList();

                PlayList playlist = dbContext.PlayLists.Where(p => !p.IsDeleted && p.PlayListId == playlistId).FirstOrDefault();
                if (playlist != null)
                {
                    if(playlist.StartDate != null && playlist.EndDate != null)
                    {
                        playlistAssignedTo.StartDate = playlist.StartDate.Value.ToLocalTime();
                        playlistAssignedTo.EndDate = playlist.EndDate.Value.ToLocalTime();
                    }
                }
                return playlistAssignedTo;
            }
        }

        /// <summary>
        /// PlayListFolderId: folder Id which is saved for a playlist for a particular user
        /// </summary>
        /// <param name="PlayListFolderId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<PlaylistContentResponseViewModel> GetPlayListContentsForAddQuestion(long playListFolderId, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                //Save the playlist step number for the user
                var playlistId = dbContext.PlayListMasters.Where(pm => pm.PlayListMasterId == playListFolderId).First().PlayListId;
                var playlistSavedState = dbContext.BuildPlaylistSavedState.Where(b => b.PlaylistId == playlistId).FirstOrDefault();
                if (playlistSavedState != null /*&& playlistSavedState.StepNumber < (int)Constants.BuildPlaylistStage.Three*/)
                {
                    playlistSavedState.StepNumber = (int)Constants.BuildPlaylistStage.Three;
                    dbContext.SaveChanges();
                }

                if (dbContext.PlayListMasters.Any(obj => obj.PlayListMasterId == playListFolderId && obj.IsFromMasterLibrary))
                    return (from mc in dbContext.MasterContents
                            join saveContent in dbContext.PlayListContentMasters on mc.MasterContentId equals saveContent.ContentId
                            where mc.IsPublished == true && !mc.IsDeleted && saveContent.ContentId == mc.MasterContentId && saveContent.PlayListMasterId == playListFolderId
                            select new PlaylistContentResponseViewModel()
                            {
                                PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                Content = new ContentViewModel
                                {
                                    ContentId = mc.MasterContentId,
                                    ContentName = mc.Name,
                                    CreatedDate = mc.CreatedDate,
                                    ModifiedDate = mc.ModifiedDate,
                                    ShortName = mc.ShortName,
                                    //  FolderName = mf.Name,
                                    //  FolderId = mf.MasterFolderId,
                                    ContentURL = mc.ContentUrl,
                                    ContentDescription = mc.Description,
                                    ViewCount = 0,
                                    SortOrder = saveContent.SortOrder,
                                    IsContentSaved = true,
                                    MimeType = mc.MimeType,
                                    QuizId = saveContent.IsTurnedOffExistingQuiz == false? (dbContext.MasterQuizs.Where(q => q.MasterContentId == mc.MasterContentId).FirstOrDefault() == null ? 0 : dbContext.MasterQuizs.Where(q => q.MasterContentId == mc.MasterContentId).FirstOrDefault().MasterQuizId) 
                                                                                         : (dbContext.CompanyQuizs.Where(q => q.ContentId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.CompanyQuizs.Where(q => q.ContentId == saveContent.PlayListContentMasterId).FirstOrDefault().QuizId)
                                }
                            }).OrderBy(o => o.Content.SortOrder).ToList();

                else return (from cc in dbContext.CompanyLibraryContents
                             join saveContent in dbContext.PlayListContentMasters on cc.CompanyLibraryContentId equals saveContent.ContentId
                             where cc.IsPublished == true && !cc.IsDeleted && saveContent.ContentId == cc.CompanyLibraryContentId && saveContent.PlayListMasterId == playListFolderId
                             select new PlaylistContentResponseViewModel()
                             {
                                 PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                 IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                 Content = new ContentViewModel
                                 {
                                     ContentId = cc.CompanyLibraryContentId,
                                     ContentName = cc.Name,
                                     CreatedDate = cc.CreatedDate,
                                     ModifiedDate = cc.ModifiedDate,
                                     ShortName = cc.ShortName,
                                     // FolderName = clf.Name,
                                     // FolderId = clf.CompanyLibraryFolderId,
                                     ContentURL = cc.ContentUrl,
                                     ContentDescription = cc.Description,
                                     ViewCount = 0,
                                     SortOrder = saveContent.SortOrder,
                                     IsContentSaved = true,
                                     MimeType = cc.MimeType,
                                     QuizId = saveContent.IsTurnedOffExistingQuiz == false ? (dbContext.CompanyQuizs.Where(q => q.ContentId == cc.CompanyLibraryContentId).FirstOrDefault() == null ? 0 : dbContext.CompanyQuizs.Where(q => q.ContentId == cc.CompanyLibraryContentId).FirstOrDefault().QuizId)
                                                                                         : (dbContext.CompanyQuizs.Where(q => q.ContentId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.CompanyQuizs.Where(q => q.ContentId == saveContent.PlayListContentMasterId).FirstOrDefault().QuizId)
                                 }
                             }).OrderBy(o => o.Content.SortOrder).ToList();
            }
        }

        public void UpdateSortOrderForPlaylistFolder(long playlistId,long playlistFolderId, int destinationSortOrder)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PlayListMaster playlistMaster = dbContext.PlayListMasters.Where(p => p.PlayListMasterId == playlistFolderId).FirstOrDefault();
                if(playlistMaster != null)
                {
                    var currentSortOrder = playlistMaster.SortOrder;
                    if(currentSortOrder < destinationSortOrder)
                    {
                        var playlistFolders = dbContext.PlayListMasters.Where(m => m.PlayListId == playlistId).Where(p => p.SortOrder >= currentSortOrder && p.SortOrder <= destinationSortOrder).ToList();
                        playlistFolders.ForEach(p =>
                        {
                            if(p.SortOrder == currentSortOrder)
                            {
                                p.SortOrder = destinationSortOrder;
                            }
                            else
                            {
                                p.SortOrder--;
                            }
                        });
                    }
                    else if(currentSortOrder > destinationSortOrder)
                    {
                        var playlistFolders = dbContext.PlayListMasters.Where(m => m.PlayListId == playlistId).Where(p => p.SortOrder>=destinationSortOrder && p.SortOrder <= currentSortOrder).ToList();
                        playlistFolders.ForEach(p =>
                        {
                            if (p.SortOrder == currentSortOrder)
                            {
                                p.SortOrder = destinationSortOrder;
                            }
                            else
                            {
                                p.SortOrder++;
                            }
                        });
                    }
                    dbContext.SaveChanges();
                }
            }
        }

        public void TurnOffExistingQuiz(long playlistContentMasterId, bool isTurnedOffExistingQuiz)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PlayListContentMaster playlistContent = dbContext.PlayListContentMasters.Where(pc => pc.PlayListContentMasterId == playlistContentMasterId).FirstOrDefault();
                if(playlistContent != null)
                {
                    playlistContent.IsTurnedOffExistingQuiz = isTurnedOffExistingQuiz;
                }
                dbContext.SaveChanges();
            }
        }
        public void TurnOffExistingQuizForAll(long playlistFolderId, bool isTurnedOffExistingQuiz)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var playlistContents = dbContext.PlayListContentMasters.Where(pc => pc.PlayListMasterId == playlistFolderId).ToList();
                if (playlistContents.Count > 0)
                {
                    playlistContents = playlistContents.Select(pc => { pc.IsTurnedOffExistingQuiz = isTurnedOffExistingQuiz; return pc; }).ToList();
                }
                dbContext.SaveChanges();
            }
        }



        /// <summary>
        /// method to update playlist name and description
        /// </summary>
        /// <param name="playlist"></param>
        public void UpdatePlaylist(PlaylistNameAndDescriptionUpdateViewModel playlist)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PlayList existingPlaylist = dbContext.PlayLists.Where(p => !p.IsDeleted && p.CreatedBy == playlist.UserId && p.PlayListId == playlist.PlayListId).FirstOrDefault();
                if (existingPlaylist != null)
                {
                    existingPlaylist.Name = playlist.Name;
                    existingPlaylist.Description = playlist.Description;
                    dbContext.SaveChanges();
                }
            }
        }


        //private method
        private void UpdatePlaylistUsers(VirtualBrandWorksEntities dbContext, long playListId)
        {
            var query = "Exec UpdatePlaylistUsers @playListId";

            var parameters = new List<SqlParameter> {
                        new SqlParameter("@playListId", playListId)
                    };

            var response = dbContext.Database.SqlQuery<int>(query, parameters.ToArray()).ToList();
            dbContext.SaveChanges();
        }

        private void CreatePublishedQuizReplica(VirtualBrandWorksEntities dbContext, long playListId)
        {
            var query = "Exec CreatePublishedQuizReplica @playListId";

            var parameters = new List<SqlParameter> {
                        new SqlParameter("@playListId", playListId)
                    };

            var response = dbContext.Database.SqlQuery<object>(query, parameters.ToArray()).ToList();
            dbContext.SaveChanges();
        }

        private void CreatePublishedTeamReplica(VirtualBrandWorksEntities dbContext, long playListId)
        {
            var query = "Exec CreatePublishedTeamReplica @playListId";

            var parameters = new List<SqlParameter> {
                        new SqlParameter("@playListId", playListId)
                    };

            var response = dbContext.Database.SqlQuery<object>(query, parameters.ToArray()).ToList();
            dbContext.SaveChanges();
        }
    }
}
