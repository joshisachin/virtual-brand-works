﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class RoleRepository : IRoleRepository
    {
        public List<RoleViewModel> GetRoles()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<RoleViewModel> roles = new List<RoleViewModel>();
                List<Role> roleList = context.Roles.Where(r => !r.IsDeleted && !r.IsSuperAdmin).ToList();
                roles = Mapper.Map<List<Role>, List<RoleViewModel>>(roleList);
                return roles;
            }
        }
    }
}
