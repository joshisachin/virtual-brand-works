﻿using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class PlaylistRepository : IPlaylistRepository
    {
        public List<MessageViewModel> ReceiveMessage(MessageViewModel messageViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                string senderProfilePicture = dbContext.Users.Where(u => u.UserId == messageViewModel.SenderId).Select(o => o.ProfilePicture).SingleOrDefault();
                string receiverProfilePicture = dbContext.Users.Where(u => u.UserId == messageViewModel.ReceiverId).Select(o => o.ProfilePicture).SingleOrDefault();
                string senderUsername = dbContext.Users.Where(u => u.UserId == messageViewModel.SenderId).Select(o => o.FirstName + " " + o.LastName).SingleOrDefault();
                string receiverUsername = dbContext.Users.Where(u => u.UserId == messageViewModel.ReceiverId).Select(o => o.FirstName + " " + o.LastName).SingleOrDefault();

                var messagesToMarkAsRead = dbContext.Messages.Where(m => m.SenderId == messageViewModel.SenderId && m.ReceiverId == messageViewModel.ReceiverId).ToList();
                foreach(var msg in messagesToMarkAsRead)
                {
                    msg.IsMessageRead = true;
                }
                dbContext.SaveChanges();

                return (from m in dbContext.Messages
                        join p in dbContext.PlayLists on m.PlayListId equals p.PlayListId
                        where m.PlayListId == messageViewModel.PlayListId
                        && ((m.ReceiverId == messageViewModel.ReceiverId && m.SenderId == messageViewModel.SenderId) || (m.SenderId == messageViewModel.ReceiverId && m.ReceiverId == messageViewModel.SenderId))
                        select new MessageViewModel()
                        {
                            Date = m.Date,
                            Message = m.Message1,
                            MessageType = m.MessageType,
                            PlayListId = m.PlayListId,
                            ReceiverId = m.ReceiverId,
                            SenderId = m.SenderId,
                            SenderImage = senderProfilePicture,
                            SenderName = senderUsername,
                            ReceiverImage = receiverProfilePicture,
                            ReceiverName = receiverUsername,
                            MessageId = m.MessageId
                        }).OrderBy(o => o.MessageId).Skip(messageViewModel.PageSize * (messageViewModel.PageIndex - 1))
                         .Take(messageViewModel.PageSize).ToList();
            }
        }

        public MessageViewModel SendMessage(MessageViewModel messageViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Model.Message msgModel = new Model.Message();
                Model.Message msgImageModel = new Model.Message();
                DateTime timeOfMessage = new DateTime();
                if (!string.IsNullOrEmpty(messageViewModel.Message)){
                    msgModel = dbContext.Messages.Add(new Model.Message()
                    {
                        Date = DateTime.UtcNow,
                        Message1 = messageViewModel.Message,
                        PlayListId = messageViewModel.PlayListId,
                        MessageType = (int)Constants.MessageType.Text,
                        SenderId = messageViewModel.SenderId, 
                        ReceiverId = messageViewModel.ReceiverId,
                        IsMessageRead = false
                    });
                    timeOfMessage = msgModel.Date;
                }
                if (!string.IsNullOrEmpty(messageViewModel.ImageMessage))
                {
                    msgImageModel = dbContext.Messages.Add(new Model.Message()
                    {
                        Date = DateTime.UtcNow,
                        Message1 = messageViewModel.ImageMessage,
                        PlayListId = messageViewModel.PlayListId,
                        MessageType = (int)Constants.MessageType.Image,
                        SenderId = messageViewModel.SenderId,
                        ReceiverId = messageViewModel.ReceiverId,
                        IsMessageRead = false
                    });
                    timeOfMessage = msgImageModel.Date;
                }

                dbContext.SaveChanges();
                return new MessageViewModel() { Date = timeOfMessage, Message = msgModel.Message1, ImageMessage = msgImageModel.Message1 };
            }
        }

        public List<PlaylistTopPerformerViewModel> GetPlaylistTopPerofermers(long playListId, bool isTopTen)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
               return    (from uqr in dbContext.UserQuizResults
                              join u in dbContext.Users on uqr.UserId equals u.UserId
                              join upb in dbContext.UserPlaylistBadges on u.UserId equals upb.UserId into userBadge
                              from data in userBadge.Where(ub => ub.PlaylistId == playListId).DefaultIfEmpty()
                              where !u.IsDeleted && u.IsActive && uqr.PlaylistId == playListId
                              select new 
                              {
                                  UserId = u.UserId,
                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  ProfilePicture = u.ProfilePicture,
                                  Points = uqr.MarksObtained,
                                  BadgeId = data != null ? data.BadgeId : 0,
                                  UserQuizResultId = uqr.UserQuizResultId
                              }).GroupBy(x => x.UserId).Select(x => new PlaylistTopPerformerViewModel {
                                  UserId = x.Key, FirstName = x.Select(m => m.FirstName).FirstOrDefault(), LastName = x.Select(m => m.LastName).FirstOrDefault(),
                               ProfilePicture = x.Select(m=>m.ProfilePicture).FirstOrDefault(), Points = x.GroupBy(y =>y.UserQuizResultId).Select(m => m.FirstOrDefault()).Sum(m=>m.Points), Badges = x.Where(n => n.BadgeId != 0).Select(m => m.BadgeId).Distinct().ToList()}).OrderByDescending(m => m.Points).ToList();

            }
        }

        public List<PlaylistLeaderboardViewModel> GetPlaylistLeaderboard(long playListId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                
                IEnumerable<long> userids = (from pa in dbContext.PlayListAssignedToes
                                             join ut in dbContext.PublishedUserTeams on pa.PublishedTeamId equals ut.PublishedTeamId
                                             join u in dbContext.Users on ut.UserId equals u.UserId
                                             where pa.IsAssignedToTeam && !u.IsDeleted && u.IsActive && pa.PlayListId == playListId
                                             select u.UserId).ToList().Union((from pa in dbContext.PlayListAssignedToes
                                                                              join u in dbContext.Users on pa.UserId equals u.UserId
                                                                              where !pa.IsAssignedToTeam && !u.IsDeleted && u.IsActive && pa.PlayListId == playListId
                                                                              select u.UserId).ToList()).Distinct();
                List<PlaylistLeaderboardViewModel> leaderboards = (from u in dbContext.Users
                                                                   where !u.IsDeleted && userids.Contains(u.UserId)
                                                                   select new PlaylistLeaderboardViewModel
                                                                   {
                                                                       UserId = u.UserId,
                                                                       FirstName = u.FirstName,
                                                                       LastName = u.LastName,
                                                                       Folders = ((from pf in dbContext.PlayListMasters
                                                                                   join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                                                                   where pf.IsFromMasterLibrary && pf.PlayListId == playListId
                                                                                   select new PlaylistFolderViewModel
                                                                                   {
                                                                                       PlayListFolderId = pf.PlayListMasterId,
                                                                                       FolderId = mf.MasterFolderId,
                                                                                       NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                                                                       //completed calculation 
                                                                                       QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                                                    join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                                                    where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                                                    select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                                                       QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                                             join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                                             where pc.PlayListMasterId == pf.PlayListMasterId && uq.PlaylistId == playListId && uq.UserId == u.UserId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                                             select uq.QuizId).ToList().Count(),
                                                                                   }).Union(from pf in dbContext.PlayListMasters
                                                                                            join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                                                            where !pf.IsFromMasterLibrary && pf.PlayListId == playListId
                                                                                            select new PlaylistFolderViewModel()
                                                                                            {
                                                                                                PlayListFolderId = pf.PlayListMasterId,
                                                                                                FolderId = pf.FolderId,
                                                                                                NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                                                                                //completed calculation 
                                                                                                QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                                                             join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                                                             where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                                                             select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                                                                QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                                                      join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                                                      where pc.PlayListMasterId == pf.PlayListMasterId && uq.PlaylistId == playListId && uq.UserId == u.UserId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                                                      select uq.QuizId).ToList().Count(),
                                                                                            })).ToList()

            }).ToList();

                return leaderboards;

            }
        }

        public List<PlaylistDropdownViewModel> GetAllPlaylist(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long companyId = (long)dbContext.Users.Where(u => u.UserId == userId).First().CompanyId;
                return (from p in dbContext.PlayLists
                        join u in dbContext.Users on p.CreatedBy equals u.UserId
                        where !p.IsDeleted && p.IsLaunched && u.CompanyId == companyId
                        select new PlaylistDropdownViewModel
                        {
                            PlaylistId = p.PlayListId,
                            Name = p.Name
                        }).ToList();

            }
        }

        public List<PlaylistLeaderboardViewModel> GetPlaylistLeaderboardByTeam(long playListId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {

                //IEnumerable<long> userids = (from pa in dbContext.PlayListAssignedToes
                //                             join ut in dbContext.PublishedUserTeams on pa.PublishedTeamId equals ut.PublishedTeamId
                //                             join u in dbContext.Users on ut.UserId equals u.UserId
                //                             where pa.IsAssignedToTeam && !u.IsDeleted && pa.PlayListId == playListId
                //                             select u.UserId).ToList().Distinct();
                List<PlaylistLeaderboardViewModel> leaderboards = (from pa in dbContext.PlayListAssignedToes
                                                                   join ut in dbContext.PublishedUserTeams on pa.PublishedTeamId equals ut.PublishedTeamId
                                                                   join pt in dbContext.PublishedTeams on ut.PublishedTeamId equals pt.PublishedTeamId
                                                                   join u in dbContext.Users on ut.UserId equals u.UserId
                                                                   where pa.IsAssignedToTeam && !u.IsDeleted && pa.PlayListId == playListId
                                                                   select new PlaylistLeaderboardViewModel
                                                                   {
                                                                       UserId = u.UserId,
                                                                       FirstName = u.FirstName,
                                                                       LastName = u.LastName,
                                                                       TeamId =  pt.MainTeamId,
                                                                       TeamName = pt.Name,
                                                                       Folders = ((from pf in dbContext.PlayListMasters
                                                                                   join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                                                                   where pf.IsFromMasterLibrary && pf.PlayListId == playListId
                                                                                   select new PlaylistFolderViewModel
                                                                                   {
                                                                                       PlayListFolderId = pf.PlayListMasterId,
                                                                                       FolderId = mf.MasterFolderId,
                                                                                       NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                                                                       //completed calculation 
                                                                                       QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                                                    join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                                                    where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                                                    select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                                                       QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                                             join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                                             where pc.PlayListMasterId == pf.PlayListMasterId && uq.PlaylistId == playListId && uq.UserId == u.UserId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                                             select uq.QuizId).ToList().Count(),
                                                                                   }).Union(from pf in dbContext.PlayListMasters
                                                                                            join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                                                            where !pf.IsFromMasterLibrary && pf.PlayListId == playListId
                                                                                            select new PlaylistFolderViewModel()
                                                                                            {
                                                                                                PlayListFolderId = pf.PlayListMasterId,
                                                                                                FolderId = pf.FolderId,
                                                                                                NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                                                                                //completed calculation 
                                                                                                QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                                                             join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                                                             where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                                                             select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                                                                QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                                                      join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                                                      where pc.PlayListMasterId == pf.PlayListMasterId && uq.PlaylistId == playListId && uq.UserId == u.UserId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                                                      select uq.QuizId).ToList().Count(),
                                                                                            })).ToList()

                                                                   }).ToList();

                return leaderboards;

            }
        }
    }
}
