﻿using AutoMapper;
using Repository.Helpers;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class CompanyContentRepository : ICompanyContentRepository
    {
        public List<FolderViewModel> GetCompanyContentFolders(CompanyContentFolderRequestViewModel request)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                request.CompanyId = (long)dbContext.Users.Where(u => !u.IsDeleted && u.UserId == request.UserId).First().CompanyId;// get companyid for current user

                return (from clf in dbContext.CompanyLibraryFolders
                        join u in dbContext.Users on request.UserId equals u.UserId
                        join sf in dbContext.CompanyContentFolderUserSaved on clf.CompanyLibraryFolderId equals sf.FolderId into sg
                        from selectFolder in sg.Where(s => s.UserId == request.UserId).DefaultIfEmpty()
                        where (!clf.IsDeleted && clf.CompanyId == request.CompanyId && !clf.IsDynamicFolder && (string.IsNullOrEmpty(request.SearchText) || clf.Name.ToLower().Contains(request.SearchText.ToLower()) || clf.ShortName.ToLower().Contains(request.SearchText.ToLower())))
                        select new FolderViewModel()
                        {
                            FolderId = clf.CompanyLibraryFolderId,
                            Name = clf.Name,
                            ShortName = clf.ShortName.ToLower(),
                            IsDynamicFolder = clf.IsDynamicFolder,
                            IsSelectedFolder = (selectFolder != null && selectFolder.UserId == request.UserId ) ? true : false,
                            NumberOfContent = dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.FolderId == clf.CompanyLibraryFolderId && c.IsPublished).ToList().Count()
                        }).OrderByDescending(o => o.FolderId)
                         .Skip(request.PageSize * (request.PageIndex - 1))
                         .Take(request.PageSize).ToList().Union((from cf in dbContext.CompanyLibraryFolders
                                                                 join sf in dbContext.CompanyContentFolderUserSaved on cf.CompanyLibraryFolderId equals sf.FolderId into sg
                                                                 from selectFolder in sg.DefaultIfEmpty()
                                                                 where cf.IsDynamicFolder == true && cf.CompanyId == request.CompanyId
                                                                 select new FolderViewModel()
                                                                 {
                                                                     FolderId = cf.CompanyLibraryFolderId,
                                                                     Name = cf.Name,
                                                                     ShortName = cf.ShortName.ToLower(),
                                                                     IsDynamicFolder = true,
                                                                     CategoryId = 0,
                                                                     IsSelectedFolder = (selectFolder != null && selectFolder.UserId == request.UserId) ? true : false,
                                                                     NumberOfContent = dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.FolderId == cf.CompanyLibraryFolderId && c.IsPublished).ToList().Count()
                                                                 }).Distinct()).ToList(); ;
            }
        }

        public ContentResponseViewModel CreateContent(CompanyContentViewModel companyContent)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanyLibraryContent content = Mapper.Map<CompanyLibraryContent>(companyContent);
                if (content != null)
                {
                    using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            content.IsAwsContent = true;
                            content.CreatedBy = companyContent.UserId;
                            content.CreatedDate = DateTime.UtcNow;
                            content.Visibility = (int)Constants.ContentVisibility.Private;
                            dbContext.CompanyLibraryContents.Add(content);
                            dbContext.SaveChanges();

                            List<CompanyContentTag> tags = new List<CompanyContentTag>();
                            if (companyContent.Tags != null)
                            {
                                companyContent.Tags.ForEach(tagId =>
                                {
                                    tags.Add(new CompanyContentTag() { CompanyLibraryContentId = content.CompanyLibraryContentId, TagId = tagId });
                                });
                            }
                            dbContext.CompanyContentTags.AddRange(tags);
                            dbContext.SaveChanges();

                            if (companyContent.PlayListFolderId != 0)
                            {
                                var existingContentCounts = dbContext.PlayListContentMasters.Where(pcm => pcm.PlayListMasterId == companyContent.PlayListFolderId).Count();
                                dbContext.PlayListContentMasters.Add(new PlayListContentMaster()
                                {
                                    PlayListMasterId = companyContent.PlayListFolderId,
                                    ContentId = content.CompanyLibraryContentId,
                                    SortOrder = existingContentCounts + 1,
                                });
                                dbContext.SaveChanges();
                            }
                            transaction.Commit();
                            ContentResponseViewModel newContent = new ContentResponseViewModel
                            {
                                ContentId = content.CompanyLibraryContentId
                            };
                            return newContent;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }

                    }
                }
                return null;
            }
        }

        public CompanyContentResponse GetContentConfirmation(ContentViewModel companyContent)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.CompanyLibraryContents
                        where !c.IsDeleted && c.CompanyLibraryContentId == companyContent.ContentId
                        select new CompanyContentResponse
                        {
                            CompanyLibraryContentId = c.CompanyLibraryContentId,
                            Name = c.Name,
                            ShortName = c.ShortName,
                            Description = c.Description,
                            ContentUrl = c.ContentUrl,
                            MimeType = c.MimeType,
                            IsAwsContent = c.IsAwsContent,
                            IsYoutubeContent = c.IsYoutubeContent,
                            IsFacebookContent = c.IsFacebookContent,
                            Tags = (from con in dbContext.CompanyLibraryContents
                                    join ct in dbContext.CompanyContentTags on con.CompanyLibraryContentId equals ct.CompanyLibraryContentId
                                    join t in dbContext.Tags on ct.TagId equals t.TagId
                                    where !con.IsDeleted && con.CompanyLibraryContentId == companyContent.ContentId
                                    select new TagViewModel
                                    {
                                        TagId = t.TagId,
                                        Name = t.Name,
                                        ShortName = t.ShortName

                                    }).ToList()
                        }).FirstOrDefault();

            }
        }

        public void PublishCompanyContent(ContentViewModel content)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanyLibraryContent existingContent = dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.CompanyLibraryContentId == content.ContentId).FirstOrDefault();
                if(existingContent != null)
                {
                    existingContent.IsPublished = content.IsPublished;
                    dbContext.SaveChanges();
                }
            }
        }

        public FolderContentDetailViewModel GetCompanyFolderContentDetail(ContentViewModel requestContent, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var companyFolderContent = (from f in dbContext.CompanyLibraryFolders
                        join c in dbContext.CompanyLibraryContents on f.CompanyLibraryFolderId equals c.FolderId
                        where c.CompanyLibraryContentId == requestContent.ContentId
                        select new FolderContentDetailViewModel
                        {
                            Folder = new FolderViewModel
                            {
                                FolderId = f.CompanyLibraryFolderId,
                                Name = f.Name,
                                ShortName = f.ShortName.ToLower(),
                                NumberOfContent = dbContext.CompanyLibraryContents.Where(con => !con.IsDeleted && con.IsPublished && con.FolderId == c.FolderId).ToList().Count(),
                                IsSelectedFolder = dbContext.CompanyContentFolderUserSaved.Any(c => c.FolderId == f.CompanyLibraryFolderId && c.UserId == userId)
                            } ,
                            Content = new ContentViewModel
                            {
                                ContentId = c.CompanyLibraryContentId,
                                ContentName = c.Name,
                                CreatedDate = c.CreatedDate,
                                ContentDescription = c.Description,
                                ContentURL = c.ContentUrl,
                                ViewCount = c.ViewCount,
                                QuizId = dbContext.CompanyQuizs.Where(q => q.ContentId == c.CompanyLibraryContentId).FirstOrDefault() == null ? 0 : dbContext.CompanyQuizs.Where(q => q.ContentId == c.CompanyLibraryContentId).FirstOrDefault().QuizId,
                                MimeType = c.MimeType,
                                IsPublished = c.IsPublished,
                                IsBroadcasted = c.IsBroadcasted,
                                IsAwsContent = c.IsAwsContent,
                                IsYoutubeContent = c.IsYoutubeContent,
                                IsFacebookContent = c.IsFacebookContent,
                                Visibility = c.Visibility
                            }
                        }).FirstOrDefault();
              
                // increase the viewCount in db
                if(companyFolderContent != null && companyFolderContent.Content != null && !string.IsNullOrEmpty(requestContent.IPAddress))
                {
                    bool isViewAlreadyConsidered = dbContext.UserContentViews.Any(uc => uc.UserId == userId && uc.IPAddress == requestContent.IPAddress && uc.ContentId == companyFolderContent.Content.ContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                    if (!isViewAlreadyConsidered)
                    {
                        int viewCountForSameUserWithDifferentIP = dbContext.UserContentViews.Where(uc => uc.UserId == userId && uc.ContentId == companyFolderContent.Content.ContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                        if(viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                        {
                            dbContext.UserContentViews.Add(new UserContentViews()
                            {
                                UserId = userId,
                                IPAddress = requestContent.IPAddress,
                                ContentId = companyFolderContent.Content.ContentId,
                                IsMasterContent = false,
                                CreatedDate = DateTime.UtcNow
                            });

                            CompanyLibraryContent existingContent = dbContext.CompanyLibraryContents.Where(cl => cl.CompanyLibraryContentId == companyFolderContent.Content.ContentId).FirstOrDefault();
                            if (existingContent != null)
                            {
                                existingContent.ViewCount = existingContent.ViewCount + 1;
                                dbContext.SaveChanges();
                            }
                            dbContext.SaveChanges();
                        }
                    }
                }
                return companyFolderContent;
            }
        }

        public QuizViewModel GetQuizDetails(long contentId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {

                var quizId = dbContext.CompanyQuizs.Where(q => q.ContentId == contentId).FirstOrDefault()?.QuizId;
                if(quizId != null)
                {
                    return (from cq in dbContext.CompanyQuizs
                            join cques in dbContext.CompanyContentQuestions on cq.QuizId equals cques.QuizId
                            into d
                            from data in d.DefaultIfEmpty()
                            where cq.QuizId == quizId
                            select new QuizViewModel()
                            {
                                QuizId = cq.QuizId,
                                QuizName = cq.Name,
                                QuizShortName = cq.ShortName,
                                QuizCreatedDate = cq.CreatedDate,
                                QuizModifiedDate = cq.ModifiedDate,
                                QuestionAsnwers = (from cq in dbContext.CompanyContentQuestions
                                                   where cq.QuizId == quizId
                                                   select new QuestionAnswerViewModel()
                                                   {
                                                       Question = new QuestionViewModel() { QuestionId = cq.CompanyContentQuestionId, Question = cq.Question },
                                                       Answers = (from cans in dbContext.CompanyContentAnswers
                                                                  where cans.CompanyQuestionId == cq.CompanyContentQuestionId
                                                                  select new AnswerViewModel()
                                                                  {
                                                                      AnswerId = cans.CompanyContentAnswerId,
                                                                      Answer = cans.Options,
                                                                      IsRightAnswer = cans.IsAnswer
                                                                  }).ToList(),
                                                       RightAnsId = (from mans in dbContext.MasterAnswers
                                                                     where mans.QuestionId == cq.CompanyContentQuestionId && mans.IsAnswer
                                                                     select mans.MasterAnswersId).FirstOrDefault()
                                                   }).ToList()
                            }).FirstOrDefault();
                }
                return null;
            }
        }

        public FolderDetailViewModel CreateFolder(CompanyFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                folderViewModel.CompanyId = (long)context.Users.Where(u => u.UserId == folderViewModel.UserId).First().CompanyId;
                CompanyLibraryFolder folder = Mapper.Map<CompanyLibraryFolder>(folderViewModel);
                folder.CreatedBy = folderViewModel.UserId;
                folder.CreatedDate = DateTime.UtcNow;
                context.CompanyLibraryFolders.Add(folder);
                context.SaveChanges();
                FolderDetailViewModel newFolder = new FolderDetailViewModel
                {
                    FolderId = folder.CompanyLibraryFolderId,
                    Name = folder.Name,
                    ShortName = folder.ShortName
                };
                return newFolder;
            }
        }

        public void UpdateFolder(CompanyFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                CompanyLibraryFolder companyFolder = context.CompanyLibraryFolders.Where(c => c.CompanyLibraryFolderId == folderViewModel.CompanyLibraryFolderId).FirstOrDefault();

                if (companyFolder != null)
                {
                    companyFolder.Name = folderViewModel.Name;
                    companyFolder.ShortName = folderViewModel.ShortName;
                    companyFolder.ModifiedBy = folderViewModel.UserId;
                    companyFolder.ModifiedDate = DateTime.UtcNow;
                    context.SaveChanges();
                }
            }
        }

        public bool IsCompanyFolderExists(CompanyFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                folderViewModel.CompanyId = (long)context.Users.Where(u => u.UserId == folderViewModel.UserId).First().CompanyId;
                return (from f in context.CompanyLibraryFolders
                        where !f.IsDeleted && f.CompanyLibraryFolderId != folderViewModel.CompanyLibraryFolderId && f.CompanyId == folderViewModel.CompanyId && f.Name.ToLower() == folderViewModel.Name.ToLower()
                        select f).Any();
            }
        }

        public bool IsCompanyFolderShortNameExists(CompanyFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                folderViewModel.CompanyId = (long)context.Users.Where(u => u.UserId == folderViewModel.UserId).First().CompanyId;
                return (from f in context.CompanyLibraryFolders
                        where !f.IsDeleted && f.CompanyLibraryFolderId != folderViewModel.CompanyLibraryFolderId && f.CompanyId == folderViewModel.CompanyId && f.ShortName.ToLower() == folderViewModel.ShortName.ToLower()
                        select f).Any();
            }
        }

        public FolderContentViewModel GetCompanyFolderContents(FolderViewModel folder, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from f in dbContext.CompanyLibraryFolders
                        where f.CompanyLibraryFolderId == folder.FolderId
                        select new FolderContentViewModel()
                        {
                            Folder = new FolderViewModel() { FolderId = f.CompanyLibraryFolderId, Name = f.Name, ShortName = f.ShortName.ToLower(), Description = string.Empty, NumberOfContent = dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.FolderId == f.CompanyLibraryFolderId && c.IsPublished).ToList().Count(), IsSelectedFolder = dbContext.CompanyContentFolderUserSaved.Any(c => c.FolderId == f.CompanyLibraryFolderId && c.UserId == userId) },
                            Contents = (from c in dbContext.CompanyLibraryContents
                                        where (c.IsPublished == true || (c.CreatedBy == userId && !c.IsPublished)) && !c.IsDeleted && c.FolderId == folder.FolderId  && (string.IsNullOrEmpty(folder.SearchText) || c.Name.ToLower().Contains(folder.SearchText) || c.ShortName.ToLower().Contains(folder.SearchText))
                                        select new ContentViewModel()
                                        {
                                            ContentId = c.CompanyLibraryContentId,
                                            ContentName = c.Name,
                                            CreatedDate = c.CreatedDate,
                                            ModifiedDate = c.ModifiedDate,
                                            ShortName = c.ShortName,
                                            FolderName = f.Name,
                                            FolderId = f.CompanyLibraryFolderId,
                                            ContentURL = c.ContentUrl,
                                            ContentDescription = c.Description,
                                            ViewCount = c.ViewCount,
                                            MimeType = c.MimeType,
                                            IsPublished = c.IsPublished,
                                            IsAwsContent = c.IsAwsContent,
                                            IsYoutubeContent= c.IsYoutubeContent,
                                            IsFacebookContent = c.IsFacebookContent
                                        }).ToList()
                        }).FirstOrDefault();
            }
        }

        public void UpdateCompanyContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                dbContext.CompanyContentFolderSortOrder.RemoveRange((from cfs in dbContext.CompanyContentFolderSortOrder
                                                                     join so in SortOders on cfs.FolderId equals so.FolderId
                                                                     where cfs.UserId.Equals(UserId)
                                                                     select cfs).ToList());
                List<CompanyContentFolderSortOrder> companyContentFolderSortOrder = new List<CompanyContentFolderSortOrder>();
                SortOders.ForEach(o =>
                {
                    companyContentFolderSortOrder.Add(new CompanyContentFolderSortOrder() { FolderId = o.FolderId, SortOder = o.SortOder, UserId = UserId });
                });
                dbContext.CompanyContentFolderSortOrder.AddRange(companyContentFolderSortOrder);
                dbContext.SaveChanges();
            }
        }

        public void SaveSelectedCompanyContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (SavedFolders.IsAddRequest && !dbContext.CompanyContentFolderUserSaved.Where(f => f.FolderId == SavedFolders.FolderId && f.UserId == UserId).Any())
                    dbContext.CompanyContentFolderUserSaved.Add(new CompanyContentFolderUserSaved() { FolderId = SavedFolders.FolderId, UserId = UserId });
                else
                {
                    CompanyContentFolderUserSaved savedFolder = dbContext.CompanyContentFolderUserSaved.Where(f => f.FolderId == SavedFolders.FolderId && f.UserId == UserId).FirstOrDefault();
                    if (savedFolder != null)
                        dbContext.CompanyContentFolderUserSaved.Remove(savedFolder);
                }
                dbContext.SaveChanges();
            }
        }

        public List<FolderViewModel> GetCompanyContentSelectedFolders(long UserId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from cs in dbContext.CompanyContentFolderUserSaved
                        join c in dbContext.CompanyLibraryFolders on cs.FolderId equals c.CompanyLibraryFolderId
                        where !c.IsDeleted && cs.UserId == UserId
                        select new FolderViewModel()
                        {
                            FolderId = c.CompanyLibraryFolderId,
                            Name = c.Name,
                            ShortName = c.ShortName,
                            IsDynamicFolder = c.IsDynamicFolder
                        }).ToList();
            }
        }

        /// <summary>
        /// method for element rollover for company library
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        public ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long folderId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                ContentPreviewViewModel contentsFolderPreviewDetails = new ContentPreviewViewModel();

                var result = (from mc in dbContext.CompanyLibraryContents
                              join mf in dbContext.CompanyContentFolderUserSaved on mc.FolderId equals mf.FolderId
                              into d
                              from data in d.DefaultIfEmpty()
                              where mc.FolderId == folderId && mc.IsPublished == true && !mc.IsDeleted
                              select new ContentViewModel()
                              {
                                  ContentId = mc.CompanyLibraryContentId,
                                  ContentName = mc.Name,
                                  CreatedDate = mc.CreatedDate,
                                  ModifiedDate = mc.ModifiedDate,
                                  ShortName = mc.ShortName,
                                  IsSelectedFolder = data == null ? false : true,
                                  FolderName = mc.CompanyLibraryFolder.Name,
                                  FolderId = mc.CompanyLibraryFolder.CompanyLibraryFolderId,
                                  ContentURL = mc.ContentUrl,
                                  MimeType = mc.MimeType,
                                  IsAwsContent = mc.IsAwsContent,
                                  IsYoutubeContent = mc.IsYoutubeContent,
                                  IsFacebookContent = mc.IsFacebookContent
                              }).OrderByDescending(c => c.ModifiedDate).ToList();
                if (result.Count() > 0)
                {
                    contentsFolderPreviewDetails.FolderId = result[0].FolderId;
                    contentsFolderPreviewDetails.FolderName = result[0].FolderName;
                    contentsFolderPreviewDetails.NoOfDocs = result.Where(x => x.ContentURL.Contains(".pdf") || x.ContentURL.Contains(".doc") || x.ContentURL.Contains(".docx") || x.ContentURL.Contains(".txt")).ToList().Count();
                    contentsFolderPreviewDetails.NoOfVideo = result.Where(x => x.ContentURL.Contains(".mp4") || x.ContentURL.Contains(".mov") || x.ContentURL.Contains(".3gp")).ToList().Count();
                    contentsFolderPreviewDetails.NoOfImages = result.Where(x => x.ContentURL.Contains(".jpeg") || x.ContentURL.Contains(".jpg") || x.ContentURL.Contains(".gif") || x.ContentURL.Contains(".bmp") || x.ContentURL.Contains(".png")).ToList().Count();
                    //var onlyVideo = result.Where(x => x.ContentURL.Contains(".mp4") || x.ContentURL.Contains(".mov")).ToList();
                    //contentsFolderPreviewDetails.ContentList = onlyVideo.Count() > 3 ? onlyVideo.Take(3).ToList() : onlyVideo;
                    contentsFolderPreviewDetails.ContentList = result.Take(3).ToList();
                }
                else
                {
                    var folderDetails = (from mc in dbContext.CompanyLibraryFolders
                                         join mf in dbContext.CompanyContentFolderUserSaved on mc.CompanyLibraryFolderId equals mf.FolderId
                             into d
                                         from data in d.DefaultIfEmpty()
                                         where mc.CompanyLibraryFolderId == folderId
                                         select new FolderViewModel()
                                         {
                                             Name = mc.Name,
                                             FolderId = mc.CompanyLibraryFolderId,
                                             IsSelectedFolder = data == null ? false : true,
                                         }).FirstOrDefault();
                    contentsFolderPreviewDetails.FolderId = folderDetails.FolderId;
                    contentsFolderPreviewDetails.FolderName = folderDetails.Name;
                    contentsFolderPreviewDetails.IsSelectedFolder = folderDetails.IsSelectedFolder;
                }
                return contentsFolderPreviewDetails;
            }
        }

        public List<FolderDetailViewModel> GetCompanyFolderList(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long companyId = (long)dbContext.Users.Where(u => !u.IsDeleted && u.UserId == userId).First().CompanyId;
                return (from f in dbContext.CompanyLibraryFolders
                        where !f.IsDynamicFolder && f.CompanyId == companyId
                        select new FolderDetailViewModel
                        {
                            FolderId = f.CompanyLibraryFolderId,
                            Name = f.Name,
                            ShortName = f.ShortName
                        }).OrderBy(o => o.Name).ToList();
            }
        }

        public List<ContentViewModel> GetCompanyContentsByType(FolderContentRequestViewModel folder)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.CompanyLibraryContents
                        join f in dbContext.CompanyLibraryFolders on c.FolderId equals f.CompanyLibraryFolderId
                        where c.FolderId == folder.FolderId && c.IsPublished == true && !c.IsDeleted && c.CompanyLibraryContentId != folder.ContentId && (string.IsNullOrEmpty(folder.SearchText) || c.Name.ToLower().Contains(folder.SearchText) || c.ShortName.ToLower().Contains(folder.SearchText))
                       // let fileType = FileTypeCollection.GetFileTypes(folder.ContentType)
                        //where Array.IndexOf(fileType, mc.ContentUrl.Split('.').LastOrDefault()) > 0
                        select new ContentViewModel()
                        {
                            ContentId = c.CompanyLibraryContentId,
                            ContentName = c.Name,
                            FolderId = c.FolderId,
                            FolderName = f.Name,
                            CreatedDate = c.CreatedDate,
                            ModifiedDate = c.ModifiedDate,
                            ShortName = c.ShortName,
                            ContentURL = c.ContentUrl,
                            ContentDescription = c.Description,
                            ViewCount = c.ViewCount,
                            MimeType = c.MimeType,
                            IsAwsContent = c.IsAwsContent,
                            IsYoutubeContent = c.IsYoutubeContent,
                            IsFacebookContent = c.IsFacebookContent
                        }).ToList();

            }
        }

        public List<ContentViewModel> GetUserCreatedContentList(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.CompanyLibraryContents
                        where !c.IsDeleted && c.CreatedBy == userId 
                        select new ContentViewModel
                        {
                            ContentId = c.CompanyLibraryContentId,
                            ContentName = c.Name,
                            ShortName = c.ShortName,
                            ContentURL = c.ContentUrl,
                            FolderId = c.FolderId,
                            ContentDescription = c.Description,
                            CreatedDate = c.CreatedDate,
                            ModifiedDate = c.ModifiedDate,
                            IsPublished = c.IsPublished,
                            IsBroadcasted = c.IsBroadcasted,
                            MimeType = c.MimeType,
                            IsAwsContent = c.IsAwsContent,
                            IsYoutubeContent = c.IsYoutubeContent,
                            IsFacebookContent = c.IsFacebookContent
                        }).ToList();

            }
        }

        public bool DeleteCompanyContent(long contentId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanyLibraryContent existingContent = dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.CompanyLibraryContentId == contentId).FirstOrDefault();
                if(existingContent != null)
                {
                    //check whether this company content is in use in any playlist or not
                    bool isInUse = false;
                    var contents = dbContext.PlayListContentMasters.Where(pcm => !pcm.PlayListMaster.IsFromMasterLibrary && pcm.ContentId == existingContent.CompanyLibraryContentId).ToList();
                    foreach(var c in contents)
                    {
                        isInUse = dbContext.PlayLists.Any(p => !p.IsDeleted && p.PlayListId == c.PlayListMaster.PlayListId);
                        if (isInUse)
                        {
                            break;
                        }
                    }
                    if (!isInUse)
                    {
                        existingContent.IsDeleted = true;
                        dbContext.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
        }

        public bool BroadCastCompanyContent(ContentViewModel content)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanyLibraryContent existingContent = dbContext.CompanyLibraryContents.Where(c => !c.IsDeleted && c.IsPublished && c.CompanyLibraryContentId == content.ContentId).FirstOrDefault();
                if (existingContent != null)
                {
                    existingContent.IsBroadcasted = content.IsBroadcasted;
                    existingContent.BroadcastingDate = DateTime.UtcNow;
                    dbContext.SaveChanges();
                }
                return content.IsBroadcasted;
            }
        }

        public void CompanyContentView(ContentViewModel requestContent, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanyLibraryContent existingContent = dbContext.CompanyLibraryContents.Where(clc => !clc.IsDeleted && clc.IsPublished && clc.CompanyLibraryContentId == requestContent.ContentId).FirstOrDefault();
                // increase the viewCount in db
                if (existingContent != null && !string.IsNullOrEmpty(requestContent.IPAddress))
                {
                    bool isViewAlreadyConsidered = dbContext.UserContentViews.Any(uc => uc.UserId == userId && uc.IPAddress == requestContent.IPAddress && uc.ContentId == existingContent.CompanyLibraryContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                    if (!isViewAlreadyConsidered)
                    {
                        int viewCountForSameUserWithDifferentIP = dbContext.UserContentViews.Where(uc => uc.UserId == userId && uc.ContentId == existingContent.CompanyLibraryContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                        if (viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                        {
                            dbContext.UserContentViews.Add(new UserContentViews()
                            {
                                UserId = userId,
                                IPAddress = requestContent.IPAddress,
                                ContentId = requestContent.ContentId,
                                IsMasterContent = false,
                                CreatedDate = DateTime.UtcNow
                            });

                             existingContent.ViewCount = existingContent.ViewCount + 1;
                             dbContext.SaveChanges();
                        }
                    }
                }
            }
        }

        public void UpdateCompanyContentVisibility(CompanyContentVisibilityViewModel content)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanyLibraryContent exisitingContent = dbContext.CompanyLibraryContents.Where(c => c.CompanyLibraryContentId == content.ContentId /*&& c.CreatedBy == content.UserId*/).FirstOrDefault();
                if (exisitingContent != null)
                {
                    exisitingContent.Visibility = content.Visibility;
                    dbContext.SaveChanges();
                }
            }
        }
    }
}
