﻿using Repository.Interfaces;
using System.Linq;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using System;
using System.Collections.Generic;
using Repository.Model;

namespace Repository.Repository
{
    public class NotificationRepository : INotificationRepository
    {
        public List<FCMUserToken> GetFcmTokens(long[] userIds)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from f in dbContext.FCMUserToken
                        where userIds.Contains(f.UserId)
                        select new
                        {
                            UserId = f.UserId,
                            TokenId = f.TokenId
                        }).ToList().Select(x => new FCMUserToken()
                        {
                            UserId = x.UserId,
                            TokenId = x.TokenId
                        }).ToList();
            }
        }

        public NotificationTypeViewModel GetNotificationData(Constants.NotificationType notificationType)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                int type = (int)notificationType;
                return (from notification in dbContext.NotificationType
                        where notification.NotificationTypeId == type
                        select new NotificationTypeViewModel()
                        {
                            Body = notification.Body,
                            Title = notification.Title
                        }).Single();
            }
        }

        public void SaveNotificationData(long userId, long? notificationTypeId, bool isSuccess, string token, string messageId, string errorMessage, string multicastId, string body, string title)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Notification objNotification = dbContext.Notifications.Where(m => !m.IsRead && m.UserId == userId && m.Body == body && m.Title == title).FirstOrDefault();

                if (objNotification == null)
                {
                    dbContext.Notifications.Add(new Model.Notification()
                    {
                        ErrorMessage = errorMessage,
                        IsSuccess = isSuccess,
                        MessageId = messageId,
                        MulticastId = multicastId,
                        NotificationTypeId = notificationTypeId,
                        SentOn = DateTime.UtcNow,
                        Token = token,
                        UserId = userId,
                        Body = body,
                        Title = title,
                        IsRead = false
                    });
                    dbContext.SaveChanges();
                }
            }
        }


        /// <summary>
        /// Saving the FCM Token for push notifications
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        public void SaveFcmToken(long userId, string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
                {
                    FCMUserToken objFCMUserToken = dbContext.FCMUserToken.Where(m => m.UserId == userId && m.TokenId == token).FirstOrDefault();

                    if (objFCMUserToken == null)
                    {
                        dbContext.FCMUserToken.Add(new FCMUserToken()
                        {
                            UserId = userId,
                            TokenId = token
                        });
                        dbContext.SaveChanges();
                    }
                }
            }
        }

        public List<Notifications> GetNotifications(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from m in dbContext.Notifications
                 where
                 m.UserId == userId 
                 && (m.IsOptOut == null || m.IsOptOut == false)
                 select new Notifications()
                 {
                     UserId = m.UserId,
                     NotificationId = m.NotificationId,
                     Body = m.Body,
                     Title = m.Title,
                     IsRead = m.IsRead,
                     SentOn = m.SentOn
                 }).OrderByDescending(n => n.SentOn).ToList();
            }
        }

        public void ReadNotifications(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                List<Notification> objNotifications = dbContext.Notifications.Where(m => m.UserId == userId).ToList();

                foreach (var item in objNotifications)
                {
                    item.IsRead = true;
                    dbContext.SaveChanges();
                }
            }
        }
    }
}
