﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class VBPlayRepository : IVBPlayRepository
    {
        public List<VBPlayViewModel> GetVBPlayContest(long userId, Constants.ContestType contestType)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                switch (contestType)
                {
                    case Constants.ContestType.AllGames:
                        return GetAllGames(userId, dbContext);
                    case Constants.ContestType.JoinedGames:
                        return GetJoinedGames(userId, dbContext);
                    case Constants.ContestType.YourGames:
                        return GetYourGames(userId, dbContext);
                    case Constants.ContestType.TaggedGames:
                        return GetTaggedGames(userId, dbContext);
                    default:
                        return new List<VBPlayViewModel>();
                }
            }
        }

        private List<VBPlayViewModel> GetTaggedGames(long userId, VirtualBrandWorksEntities dbContext)
        {
            return (from c in dbContext.Contests
                    join ut in dbContext.UserTagContest on c.ContestId equals ut.ContestID
                    join u in dbContext.Users on c.CreatedBy equals u.UserId
                    join t in dbContext.Tags on ut.TagId equals t.TagId
                    where t.UserId != null && t.UserId == userId && !c.IsDeleted
                    select new VBPlayViewModel()
                    {
                        ContentUrl = c.UploadedContentUrl,
                        ContestId = c.ContestId,
                        Description = c.Description,
                        EndDate = c.EndDate,
                        Name = c.Name,
                        Rules = c.Rules,
                        StartDate = c.StartDate,
                        IsAwsContent = c.IsAwsContent,
                        IsYoutubeContent = c.IsYoutubeContent,
                        IsFacebookContent = c.IsFacebookContent,
                        CreatedDate = c.CreatedDate,
                        MimeType = c.MimeType,
                        Likes = c.Likes,
                        Views = c.Views,
                        Users = (from con in dbContext.ContestMasters
                                 where con.ContestId == c.ContestId
                                 select con.JoinedByUserId).Distinct().ToList().Union(from cont in dbContext.Contests
                                                                                      where cont.ContestId == c.ContestId
                                                                                      select cont.CreatedBy).Distinct().Count(),
                        Tags = (from con in dbContext.ContestTags
                                join t in dbContext.Tags on con.TagId equals t.TagId
                                where con.ContestId == c.ContestId
                                select new TagModel
                                {
                                    TagId = t.TagId,
                                    Name = t.Name,
                                    IsHandle = ((t.UserId == null || t.UserId == 0) && (t.TeamId == null || t.TeamId == 0)) ? false : true
                                }).ToList(),
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        ProfilePicture = u.ProfilePicture,
                        IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == c.ContestId && ul.UserId == userId),
                        IsJoinedByUser = (c.CreatedBy == userId || dbContext.ContestMasters.Any(cm => cm.ContestId == c.ContestId && cm.JoinedByUserId == userId)),
                        IsBroadCasted = c.IsBroadCasted,
                        IsCompleted = c.IsCompleted,
                        Visibility = c.Visibility,
                        IsSubmitted = (c.IsSubmitted.HasValue) ? c.IsSubmitted.Value : false
                    }).OrderByDescending(obj => obj.CreatedDate).ToList();
        }

        private List<VBPlayViewModel> GetYourGames(long userId, VirtualBrandWorksEntities dbContext)
        {
            return (from c in dbContext.Contests
                    join u in dbContext.Users on c.CreatedBy equals u.UserId
                    where !c.IsDeleted && c.CreatedBy == userId
                    select new VBPlayViewModel()
                    {
                        ContentUrl = c.UploadedContentUrl,
                        ContestId = c.ContestId,
                        Description = c.Description,
                        EndDate = c.EndDate,
                        Name = c.Name,
                        Rules = c.Rules,
                        StartDate = c.StartDate,
                        IsAwsContent = c.IsAwsContent,
                        IsYoutubeContent = c.IsYoutubeContent,
                        IsFacebookContent = c.IsFacebookContent,
                        CreatedDate = c.CreatedDate,
                        MimeType = c.MimeType,
                        Likes = c.Likes,
                        Views = c.Views,
                        Users = (from con in dbContext.ContestMasters
                                 where con.ContestId == c.ContestId
                                 select con.JoinedByUserId).Distinct().ToList().Union(from cont in dbContext.Contests
                                                                                      where cont.ContestId == c.ContestId
                                                                                      select cont.CreatedBy).Distinct().Count(),
                        Tags = (from con in dbContext.ContestTags
                                join t in dbContext.Tags on con.TagId equals t.TagId
                                where con.ContestId == c.ContestId
                                select new TagModel
                                {
                                    TagId = t.TagId,
                                    Name = t.Name,
                                    IsHandle = ((t.UserId == null || t.UserId == 0) && (t.TeamId == null || t.TeamId == 0)) ? false : true
                                }).ToList(),
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        ProfilePicture = u.ProfilePicture,
                        IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == c.ContestId && ul.UserId == userId),
                        IsJoinedByUser = (c.CreatedBy == userId || dbContext.ContestMasters.Any(cm => cm.ContestId == c.ContestId && cm.JoinedByUserId == userId)),
                        IsBroadCasted = c.IsBroadCasted,
                        IsCompleted = c.IsCompleted,
                        Visibility = c.Visibility,
                        IsSubmitted = (c.IsSubmitted.HasValue) ? c.IsSubmitted.Value : false
                    }).OrderByDescending(obj => obj.CreatedDate).ToList();
        }

        private List<VBPlayViewModel> GetJoinedGames(long userId, VirtualBrandWorksEntities dbContext)
        {
            return (from u in dbContext.Users
                    join c in dbContext.Contests on u.UserId equals c.CreatedBy
                    join cm in dbContext.ContestMasters on c.ContestId equals cm.ContestId
                    where !c.IsDeleted && cm.JoinedByUserId == userId
                    select new VBPlayViewModel()
                    {
                        ContentUrl = c.UploadedContentUrl,
                        ContestId = c.ContestId,
                        Description = c.Description,
                        EndDate = c.EndDate,
                        Name = c.Name,
                        Rules = c.Rules,
                        StartDate = c.StartDate,
                        IsAwsContent = c.IsAwsContent,
                        IsYoutubeContent = c.IsYoutubeContent,
                        IsFacebookContent = c.IsFacebookContent,
                        CreatedDate = c.CreatedDate,
                        IsSubmitted = cm.IsSubmitted,
                        MimeType = c.MimeType,
                        Likes = c.Likes,
                        Views = c.Views,
                        Users = (from con in dbContext.ContestMasters
                                 where con.ContestId == c.ContestId
                                 select con.JoinedByUserId).Distinct().ToList().Union(from cont in dbContext.Contests
                                                                                      where cont.ContestId == c.ContestId
                                                                                      select cont.CreatedBy).Distinct().Count(),
                        Tags = (from con in dbContext.ContestTags
                                join t in dbContext.Tags on con.TagId equals t.TagId
                                where con.ContestId == c.ContestId
                                select new TagModel
                                {
                                    TagId = t.TagId,
                                    Name = t.Name,
                                    IsHandle = ((t.UserId == null || t.UserId == 0) && (t.TeamId == null || t.TeamId == 0)) ? false : true
                                }).ToList(),
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        ProfilePicture = u.ProfilePicture,
                        IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == c.ContestId && ul.UserId == userId),
                        IsBroadCasted = c.IsBroadCasted,
                        IsCompleted = c.IsCompleted,
                        IsJoinedByUser = true,
                        Visibility = c.Visibility,
                        ChallengeEntry = cm.IsSubmitted ? null : (from user in dbContext.Users
                                                                  where user.UserId == userId
                                                                  select new ChallengeEntryViewModel
                                                                  {
                                                                      ContestEntryId = cm.ContestMasterId,
                                                                      JoinedByUserId = userId,
                                                                      FirstName = user.FirstName,
                                                                      LastName = user.LastName,
                                                                      ProfilePicture = user.ProfilePicture,
                                                                      Name = cm.ContentTitle,
                                                                      Description = cm.Description,
                                                                      ContentUrl = cm.UploadedContentUrl,
                                                                      IsAwsContent = cm.IsAwsContent,
                                                                      IsYoutubeContent = cm.IsYoutubeContent,
                                                                      IsFacebookContent = cm.IsFacebookContent,
                                                                      MimeType = cm.MimeType
                                                                  }).FirstOrDefault()
                    }).ToList().GroupBy(x => x.ContestId).Select(x => x.FirstOrDefault()).OrderByDescending(obj => obj.CreatedDate).ToList();
        }

        private List<VBPlayViewModel> GetAllGames(long userId, VirtualBrandWorksEntities dbContext)
        {
            //return (from u in dbContext.Users
            //        join c in dbContext.Contests on u.UserId equals c.CreatedBy
            //        join ct in dbContext.ContestTags on c.ContestId equals ct.ContestId
            //        join ut in dbContext.UserTags on ct.TagId equals ut.TagId
            //        where ut.UserId == userId
            //        select new VBPlayViewModel()
            //        {
            //            ContentUrl = c.UploadedContentUrl,
            //            ContestId = c.ContestId,
            //            Description = c.Description,
            //            EndDate = c.EndDate,
            //            Name = c.Name,
            //            Rules = c.Rules,
            //            StartDate = c.StartDate,
            //            IsAwsContent = c.IsAwsContent,
            //            CreatedDate = c.CreatedDate,
            //            MimeType = c.MimeType,
            //            Likes = c.Likes,
            //            Views = c.Views,
            //            Users = (from con in dbContext.ContestTags
            //                     join userTag in dbContext.UserTags on con.TagId equals userTag.TagId
            //                     where con.ContestId == c.ContestId
            //                     select userTag.UserId).Distinct().ToList().Count(),
            //            Tags = (from con in dbContext.ContestTags
            //                    join t in dbContext.Tags on con.TagId equals t.TagId
            //                    where con.ContestId == c.ContestId
            //                    select new TagModel
            //                    {
            //                        TagId = t.TagId,
            //                        Name = t.Name
            //                    }).ToList(),
            //            FirstName = u.FirstName,
            //            LastName = u.LastName,
            //            ProfilePicture = u.ProfilePicture,
            //        }).ToList().GroupBy(x => x.ContestId).Select(x => x.FirstOrDefault()).OrderByDescending(obj => obj.CreatedDate).ToList();

            long companyId = (long)dbContext.Users.Where(u => !u.IsDeleted && u.UserId == userId).First().CompanyId;// get companyid for current user

            var games = (from u in dbContext.Users
                         join c in dbContext.Contests on u.UserId equals c.CreatedBy
                         where !c.IsCompleted && !c.IsDeleted && u.CompanyId == companyId
                         select new VBPlayViewModel()
                         {
                             ContentUrl = c.UploadedContentUrl,
                             ContestId = c.ContestId,
                             Description = c.Description,
                             EndDate = c.EndDate,
                             Name = c.Name,
                             Rules = c.Rules,
                             StartDate = c.StartDate,
                             IsAwsContent = c.IsAwsContent,
                             IsYoutubeContent = c.IsYoutubeContent,
                             IsFacebookContent = c.IsFacebookContent,
                             CreatedDate = c.CreatedDate,
                             MimeType = c.MimeType,
                             Likes = c.Likes,
                             Views = c.Views,
                             Users = (from con in dbContext.ContestMasters
                                      where con.ContestId == c.ContestId
                                      select con.JoinedByUserId).Distinct().ToList().Union(from cont in dbContext.Contests
                                                                                           where cont.ContestId == c.ContestId
                                                                                           select cont.CreatedBy).Distinct().Count(),
                             Tags = (from con in dbContext.ContestTags
                                     join t in dbContext.Tags on con.TagId equals t.TagId
                                     where con.ContestId == c.ContestId
                                     select new TagModel
                                     {
                                         TagId = t.TagId,
                                         Name = t.Name,
                                         IsHandle = ((t.UserId == null || t.UserId == 0) && (t.TeamId == null || t.TeamId == 0)) ? false : true
                                     }).ToList(),
                             FirstName = u.FirstName,
                             LastName = u.LastName,
                             ProfilePicture = u.ProfilePicture,
                             IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == c.ContestId && ul.UserId == userId),
                             IsJoinedByUser = (c.CreatedBy == userId || dbContext.ContestMasters.Any(cm => cm.ContestId == c.ContestId && cm.JoinedByUserId == userId)),
                             IsBroadCasted = c.IsBroadCasted,
                             IsCompleted = c.IsCompleted,
                             Visibility = c.Visibility,
                             IsSubmitted = (c.IsSubmitted.HasValue) ? c.IsSubmitted.Value : false,
                             IsWinnerAnnounced = c.IsWinnerAnnounced == null ? false : (bool)c.IsWinnerAnnounced
                         }).ToList().OrderByDescending(obj => obj.CreatedDate).ToList();

            SetWinnerForExpiredContest(games);
            return games;
        }

        private List<VBPlayViewModel> SetWinnerForExpiredContest(List<VBPlayViewModel> games)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using(var transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var game in games)
                        {
                            if (DateTime.UtcNow > game.EndDate && !game.IsWinnerAnnounced)
                            {
                                var entries = (from con in dbContext.ContestMasters
                                               where con.ContestId == game.ContestId
                                               select con).ToList();
                                if (entries != null && entries.Count != 0)
                                {
                                    game.UserIdsToNotify = new List<long>();
                                    if (entries.Count == 1)
                                    {
                                        var winnerEntry = entries.First();
                                        game.UserIdsToNotify.Add(winnerEntry.UserId);
                                        var entry = dbContext.ContestMasters.FirstOrDefault(x => x.ContestMasterId == winnerEntry.ContestMasterId);
                                        entry.IsWinner = true;
                                        entry.IsBroadcasted = true;
                                        entry.BroadcastingDate = DateTime.UtcNow.AddDays(120);
                                        dbContext.SaveChanges();
                                        AddPointsForWinner(entry);
                                    }
                                    else
                                    {
                                        long maximumViews = entries.Max(x => x.Views);
                                        var winningEntries = entries.Where(x => x.Views == maximumViews);
                                        if(winningEntries.Count() > 3)
                                        {
                                            var top3Entries = winningEntries.OrderByDescending(x => x.Likes).Take(3);
                                            var userIdsWithMaximumViews = top3Entries.Select(x => x.UserId);
                                            game.UserIdsToNotify.AddRange(userIdsWithMaximumViews);
                                            foreach (var topEntry in top3Entries)
                                            {
                                                var entry = dbContext.ContestMasters.FirstOrDefault(x => x.ContestMasterId == topEntry.ContestMasterId);
                                                entry.IsWinner = true;
                                                entry.IsBroadcasted = true;
                                                entry.BroadcastingDate = DateTime.UtcNow.AddDays(120);
                                                dbContext.SaveChanges();
                                                AddPointsForWinner(topEntry);                                              
                                            }
                                        }
                                        else
                                        {
                                            var top3Entries = entries.OrderByDescending(x => x.Views)
                                                                     .ThenByDescending(x => x.Likes)
                                                                     .Take(3);
                                            var userIdsWithMaximumViews = top3Entries.Select(x => x.UserId);
                                            game.UserIdsToNotify.AddRange(userIdsWithMaximumViews);

                                            foreach (var topEntry in top3Entries)
                                            {
                                                var entry = dbContext.ContestMasters.FirstOrDefault(x => x.ContestMasterId == topEntry.ContestMasterId);
                                                entry.IsWinner = true;
                                                entry.IsBroadcasted = true;
                                                entry.BroadcastingDate = DateTime.UtcNow.AddDays(120);
                                                dbContext.SaveChanges();
                                                AddPointsForWinner(topEntry);
                                            }
                                        }
                                        
                                    }
                                }
                                var contest = dbContext.Contests.FirstOrDefault(x => x.ContestId == game.ContestId);
                                contest.IsWinnerAnnounced = true; //WinnerIsAnnounced
                                dbContext.SaveChanges();
                            }
                        }
                        transaction.Commit();
                    }
                    catch(Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
                
            }
            return games;
        }

        private void AddPointsForWinner(ContestMaster contest)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long bal = dbContext.EarnedPoints.Any(ep => ep.UserId == contest.UserId) ? (long)dbContext.EarnedPoints.Where(ep => ep.UserId == contest.UserId).Select(e => e.Points).FirstOrDefault() : 0;
                dbContext.PointsTransactions.Add(new PointsTransaction()
                {
                    ContestId = contest.ContestId,
                    UserId = contest.UserId,
                    Points = Constants.ParticipantWinner,
                    Description = contest.Description,
                    CreatedDate = DateTime.UtcNow,
                    IsEarned = true,
                    Type = Constants.PointsEarnedType.Participant.ToString(),
                    Balance = bal + Constants.ParticipantWinner
                });
                dbContext.SaveChanges();

                var userEarnedPoints = dbContext.EarnedPoints.Where(ep => ep.UserId == contest.UserId).FirstOrDefault();
                if (userEarnedPoints != null)
                {
                    userEarnedPoints.Points = userEarnedPoints.Points + Constants.ParticipantWinner;
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.EarnedPoints.Add(new EarnedPoint
                    {
                        UserId = contest.UserId,
                        Points = Constants.ParticipantWinner
                    });
                    dbContext.SaveChanges();
                }
            }             
        }



        public VBPlayViewModel CreateContest(VBPlayCreateViewModel vbPlayContest, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        Contest contest = dbContext.Contests.Add(new Contest()
                        {
                            CreatedBy = userId,
                            CreatedDate = DateTime.UtcNow,
                            Description = vbPlayContest.Description,
                            EndDate = vbPlayContest.EndDate,
                            IsAwsContent = vbPlayContest.IsAwsContent,
                            IsYoutubeContent = vbPlayContest.IsYoutubeContent,
                            IsFacebookContent = vbPlayContest.IsFacebookContent,
                            IsWinnerAnnounced = false,
                            IsExtendDateNotified = false,
                            IsBroadCasted = false,
                            IsDeleted = false,
                            Name = vbPlayContest.Name,
                            StartDate = vbPlayContest.StartDate,
                            Rules = vbPlayContest.Rules,
                            UploadedContentUrl = vbPlayContest.ContentUrl,
                            MimeType = vbPlayContest.MimeType,
                            Likes = 0,
                            Views = 0,
                            Visibility = (int)Constants.ContentVisibility.Private,
                            IsSubmitted = false
                        });
                        dbContext.SaveChanges();

                        if (vbPlayContest.TagIds != null)
                        {
                            vbPlayContest.TagIds.ForEach(tagId =>
                            {
                                dbContext.ContestTags.Add(new ContestTag()
                                {
                                    ContestId = contest.ContestId,
                                    TagId = tagId
                                });
                            });
                            dbContext.SaveChanges();
                        }
                        long bal = dbContext.EarnedPoints.Any(ep => ep.UserId == userId) ? (long)dbContext.EarnedPoints.Where(ep => ep.UserId == userId).Select(e => e.Points).FirstOrDefault() : 0;
                        dbContext.PointsTransactions.Add(new PointsTransaction()
                        {
                            ContestId = contest.ContestId,
                            UserId = userId,
                            Points = Constants.CreateContest,
                            Description = contest.Name,
                            CreatedDate = DateTime.UtcNow,
                            IsEarned = true,
                            Type = Constants.PointsEarnedType.Create.ToString(),
                            Balance = bal + Constants.CreateContest
                        });
                        dbContext.SaveChanges();

                        var userEarnedPoints = dbContext.EarnedPoints.Where(ep => ep.UserId == userId).FirstOrDefault();
                        if (userEarnedPoints != null)
                        {
                            userEarnedPoints.Points = userEarnedPoints.Points + Constants.CreateContest;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.EarnedPoints.Add(new EarnedPoint
                            {
                                UserId = userId,
                                Points = Constants.CreateContest
                            });
                            dbContext.SaveChanges();
                        }

                        // Save the entry for tagged users
                        SaveTaggedUsers(vbPlayContest.TagIds, contest.ContestId);

                        transaction.Commit();

                        //return contest.ContestId;
                        return (from c in dbContext.Contests
                                where !c.IsDeleted && c.ContestId == contest.ContestId
                                select new VBPlayViewModel
                                {
                                    ContestId = c.ContestId,
                                    Name = c.Name,
                                    Description = c.Description,
                                    ContentUrl = c.UploadedContentUrl,
                                    MimeType = c.MimeType,
                                    StartDate = c.StartDate,
                                    EndDate = c.EndDate,
                                    IsAwsContent = c.IsAwsContent,
                                    IsYoutubeContent = c.IsYoutubeContent,
                                    IsFacebookContent = c.IsFacebookContent,
                                    IsBroadCasted = c.IsBroadCasted
                                }).FirstOrDefault();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        private void SaveTaggedUsers(List<long> tagIds, long contestId)
        {
            if (tagIds != null)
            {
                List<UserTagContest> userTagContest = new List<UserTagContest>();

                using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
                {
                    foreach (var item in tagIds)
                    {
                        List<long> lstUsers = new List<long>();

                        var handleuserList = dbContext.Tags.Where(m => m.TagId == item && m.UserId != null).Select(m => (long)m.UserId).ToList();

                        lstUsers.AddRange(handleuserList);

                        var handleTeamList = dbContext.Tags.Where(m => m.TagId == item && m.TeamId != null).Select(m => m.TeamId).ToList();

                        foreach (var teams in handleTeamList)
                        {
                            lstUsers.AddRange(dbContext.UserTeams.Where(m => m.TeamId == teams).Select(m => m.UserId).ToList());
                        }

                        foreach (var user in lstUsers)
                        {
                            // Get the tags for userhandle and teamhandle
                            userTagContest.Add(new UserTagContest
                            {
                                ContestID = contestId,
                                CreatedDate = DateTime.Now,
                                TagId = item,
                                UserID = user
                            });
                        }
                    }
                    dbContext.UserTagContest.AddRange(userTagContest);
                    dbContext.SaveChanges();
                }
            }
        }

        public long JoinContest(VBPlayViewModel vbPlayViewModel, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                ContestMaster contestEntry = new ContestMaster
                {
                    ContentTitle = vbPlayViewModel.Name,
                    ContestId = vbPlayViewModel.ContestId,
                    Description = vbPlayViewModel.Description,
                    IsAwsContent = vbPlayViewModel.IsAwsContent,
                    IsYoutubeContent = vbPlayViewModel.IsYoutubeContent,
                    IsFacebookContent = vbPlayViewModel.IsFacebookContent,
                    IsExtendDateNotified = false,
                    IsWinner = false,
                    JoinedByUserId = userId,
                    JoinedDate = DateTime.UtcNow,
                    UploadedContentUrl = vbPlayViewModel.ContentUrl,
                    UserId = userId,
                    MimeType = vbPlayViewModel.MimeType,
                    IsSubmitted = vbPlayViewModel.IsSubmitted,
                    Likes = 0,
                    Views = 0,
                    Visibility = (int)Constants.ContentVisibility.Private,
                };
                dbContext.ContestMasters.Add(contestEntry);
                dbContext.SaveChanges();
                return contestEntry.ContestMasterId;
            }
        }

        public VBPlayViewModel GetContest(long contestId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from contest in dbContext.Contests
                        where !contest.IsDeleted && contest.ContestId == contestId
                        select new VBPlayViewModel()
                        {
                            ContentUrl = contest.UploadedContentUrl,
                            ContestId = contest.ContestId,
                            CreatedDate = contest.CreatedDate,
                            Description = contest.Description,
                            EndDate = contest.EndDate,
                            IsAwsContent = contest.IsAwsContent,
                            Name = contest.Name,
                            Rules = contest.Rules,
                            StartDate = contest.StartDate,
                            Likes = contest.Likes,
                            Views = contest.Views,
                            MimeType = contest.MimeType,
                            IsSubmitted = contest.IsSubmitted.HasValue ? contest.IsSubmitted.Value : false,
                            Tags = (from ct in dbContext.ContestTags
                                    join t in dbContext.Tags on ct.TagId equals t.TagId
                                    where ct.ContestId == contestId
                                    select new TagModel()
                                    {
                                        TagId = t.TagId,
                                        Name = t.Name
                                    }).ToList()
                        }).Single();
            }
        }

        public ContestChallengeAndEntriesViewModel ViewContest(long userId, long contestId, string search)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                ContestChallengeAndEntriesViewModel viewContest = new ContestChallengeAndEntriesViewModel();
                viewContest.ContestChallenge = (from u in dbContext.Users
                                                join c in dbContext.Contests on u.UserId equals c.CreatedBy
                                                where !c.IsDeleted && c.ContestId == contestId
                                                select new VBPlayViewModel()
                                                {
                                                    ContentUrl = c.UploadedContentUrl,
                                                    ContestId = c.ContestId,
                                                    Description = c.Description,
                                                    EndDate = c.EndDate,
                                                    Name = c.Name,
                                                    Rules = c.Rules,
                                                    StartDate = c.StartDate,
                                                    IsAwsContent = c.IsAwsContent,
                                                    IsYoutubeContent = c.IsYoutubeContent,
                                                    IsFacebookContent = c.IsFacebookContent,
                                                    CreatedDate = c.CreatedDate,
                                                    MimeType = c.MimeType,
                                                    Likes = c.Likes,
                                                    Views = c.Views,
                                                    Users = (from con in dbContext.ContestMasters
                                                             where con.ContestId == c.ContestId
                                                             select con.JoinedByUserId).Distinct().ToList().Union(from cont in dbContext.Contests
                                                                                                                  where cont.ContestId == c.ContestId
                                                                                                                  select cont.CreatedBy).Distinct().Count(),
                                                    Tags = (from ct in dbContext.ContestTags
                                                            join t in dbContext.Tags on ct.TagId equals t.TagId
                                                            where ct.ContestId == contestId
                                                            select new TagModel()
                                                            {
                                                                TagId = t.TagId,
                                                                Name = t.Name
                                                            }).ToList(),
                                                    FirstName = u.FirstName,
                                                    LastName = u.LastName,
                                                    ProfilePicture = u.ProfilePicture,
                                                    IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == c.ContestId && ul.UserId == userId),
                                                    IsCompleted = c.IsCompleted,
                                                    IsWinner = c.IsWinner,
                                                    EarnedPoints = dbContext.PointsTransactions.Any(pt => pt.ContestId == c.ContestId && pt.UserId == userId) ? (long)dbContext.PointsTransactions.Where(pt => pt.ContestId == c.ContestId && pt.UserId == userId).Sum(p => p.Points) : 0,
                                                    IsJoinedByUser = (c.CreatedBy == userId || dbContext.ContestMasters.Any(cm => cm.ContestId == c.ContestId && cm.JoinedByUserId == userId)),
                                                    IsCreator = c.CreatedBy == userId,
                                                    IsBroadCasted = c.IsBroadCasted,
                                                    Points = c.IsWinner ? Constants.ParticipantWinner : 0,
                                                    Visibility = c.Visibility
                                                }).FirstOrDefault();
                if (viewContest != null && viewContest.ContestChallenge != null)
                {
                    //if (!viewContest.ContestChallenge.IsCompleted)
                    //{
                    viewContest.ContestEntries = (from cm in dbContext.ContestMasters
                                                  join user in dbContext.Users on cm.JoinedByUserId equals user.UserId
                                                  where cm.ContestId == contestId && cm.IsSubmitted && (string.IsNullOrEmpty(search) || cm.ContentTitle.ToLower().Contains(search.ToLower()))
                                                  select new ChallengeEntryViewModel()
                                                  {
                                                      ContentUrl = cm.UploadedContentUrl,
                                                      ContestEntryId = cm.ContestMasterId,
                                                      Description = cm.Description,
                                                      JoinedDate = cm.JoinedDate,
                                                      Name = cm.ContentTitle,
                                                      IsAwsContent = cm.IsAwsContent,
                                                      IsYoutubeContent = cm.IsYoutubeContent,
                                                      IsFacebookContent = cm.IsFacebookContent,
                                                      Likes = cm.Likes,
                                                      Views = cm.Views,
                                                      MimeType = cm.MimeType,
                                                      FirstName = user.FirstName,
                                                      LastName = user.LastName,
                                                      ProfilePicture = user.ProfilePicture,
                                                      IsLikedByUser = dbContext.ContestMasterUserLikes.Any(ul => ul.ContestMasterId == cm.ContestMasterId && ul.UserId == userId),
                                                      IsWinner = cm.IsWinner,
                                                      IsJoinedByUser = dbContext.ContestMasters.Any(conm => conm.ContestId == cm.ContestId && conm.JoinedByUserId == userId),
                                                      IsCreator = cm.JoinedByUserId == userId,
                                                      IsBroadCasted = cm.IsBroadcasted,
                                                      IsContestEntry = true,
                                                      Points = cm.IsWinner ? Constants.ParticipantWinner : 0,
                                                      Visibility = cm.Visibility
                                                  }).OrderByDescending(obj => obj.Views)
                                                    .ThenByDescending(x => x.Likes)
                                                    .ToList();
                    //}
                    //else
                    //{
                    //    viewContest.ContestEntries = (from cm in dbContext.ContestMasters
                    //                                  join user in dbContext.Users on cm.JoinedByUserId equals user.UserId
                    //                                  where cm.ContestId == contestId && cm.IsWinner && (string.IsNullOrEmpty(search) || cm.ContentTitle.ToLower().Contains(search.ToLower()))
                    //                                  select new ChallengeEntryViewModel()
                    //                                  {
                    //                                      ContentUrl = cm.UploadedContentUrl,
                    //                                      ContestEntryId = cm.ContestMasterId,
                    //                                      Description = cm.Description,
                    //                                      JoinedDate = cm.JoinedDate,
                    //                                      Name = cm.ContentTitle,
                    //                                      IsAwsContent = cm.IsAwsContent,
                    //                                      IsYoutubeContent = cm.IsYoutubeContent,
                    //                                      IsFacebookContent = cm.IsFacebookContent,
                    //                                      Likes = cm.Likes,
                    //                                      Views = cm.Views,
                    //                                      MimeType = cm.MimeType,
                    //                                      FirstName = user.FirstName,
                    //                                      LastName = user.LastName,
                    //                                      ProfilePicture = user.ProfilePicture,
                    //                                      IsLikedByUser = dbContext.ContestMasterUserLikes.Any(ul => ul.ContestMasterId == cm.ContestMasterId && ul.UserId == userId),
                    //                                      IsJoinedByUser = dbContext.ContestMasters.Any(conm => conm.ContestId == cm.ContestId && conm.JoinedByUserId == userId),
                    //                                  }).OrderByDescending(obj => obj.JoinedDate).ToList();
                    //}
                }
                return viewContest;
            }
        }

        /// <summary>
        /// to get the preview of the created contest
        /// </summary>
        /// <param name="contestId"></param>
        /// <returns></returns>
        public VBPlayViewModel GetContestConfirmation(long contestId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.Contests
                        where !c.IsDeleted && c.ContestId == contestId
                        select new VBPlayViewModel
                        {
                            ContestId = c.ContestId,
                            Name = c.Name,
                            Description = c.Description,
                            ContentUrl = c.UploadedContentUrl,
                            MimeType = c.MimeType,
                            StartDate = c.StartDate,
                            EndDate = c.EndDate,
                            IsAwsContent = c.IsAwsContent,
                            IsBroadCasted = c.IsBroadCasted
                        })
                        .FirstOrDefault();
            }
        }

        /// <summary>
        /// to get the confirmation view of the entry that is being submitted against a contest 
        /// </summary>
        /// <param name="contestEntryId"></param>
        /// <returns></returns>
        public ChallengeEntryViewModel GetContestEntryConfirmation(long contestEntryId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.ContestMasters
                        where c.ContestMasterId == contestEntryId
                        select new ChallengeEntryViewModel
                        {
                            ContestEntryId = c.ContestMasterId,
                            Name = c.ContentTitle,
                            Description = c.Description,
                            ContentUrl = c.UploadedContentUrl,
                            MimeType = c.MimeType,
                            IsAwsContent = c.IsAwsContent,
                        }).FirstOrDefault();
            }
        }

        public void BroadcastContest(ContestBroadcastRequestViewModel contest)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (!contest.IsContestEntry)
                {
                    Contest newContest = dbContext.Contests.Where(c => !c.IsDeleted && c.ContestId == contest.ContestId).FirstOrDefault();
                    if (newContest != null)
                    {
                        newContest.IsBroadCasted = contest.IsBroadCasted;
                        newContest.BroadcastingDate = DateTime.UtcNow.AddDays(120);
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    ContestMaster newContestMaster = dbContext.ContestMasters.Where(cm => cm.ContestMasterId == contest.ContestId).FirstOrDefault();
                    if (newContestMaster != null)
                    {
                        newContestMaster.IsBroadcasted = contest.IsBroadCasted;
                        newContestMaster.BroadcastingDate = DateTime.UtcNow.AddDays(120);
                        dbContext.SaveChanges();
                    }
                }
            }
        }

        public List<BroadcastedContentViewModel> GetBroadcastedContent(string search, long userId) //sortby ? not clear
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long industryId = dbContext.Users.Where(u => u.UserId == userId).First().Industry;
                return  (from contest in dbContext.Contests
                        join u in dbContext.Users on contest.CreatedBy equals u.UserId
                        join ct in dbContext.ContestTags on contest.ContestId equals ct.ContestId into d
                        from con in d.DefaultIfEmpty()
                        join t in dbContext.Tags on con.TagId equals t.TagId into contag
                        from info in contag.DefaultIfEmpty()
                        where !contest.IsDeleted && contest.IsBroadCasted && u.Industry == industryId && (string.IsNullOrEmpty(search) || contest.Name.ToLower().Contains(search.ToLower()) || info.Name.ToLower().Contains(search.ToLower()))
                        && //(DateTime.UtcNow - (contest.BroadcastingDate.HasValue? contest.BroadcastingDate: DateTime.UtcNow)).Value.TotalDays < 120
                        DbFunctions.DiffDays(DateTime.UtcNow, contest.BroadcastingDate) <= 120
                        select new BroadcastedContentViewModel
                        {
                            ContentId = contest.ContestId,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            ProfilePicture = u.ProfilePicture,
                            Name = contest.Name,
                            Description = contest.Description,
                            ContentUrl = contest.UploadedContentUrl,
                            MimeType = Constants.VbPlayMimeType,
                            Likes = contest.Likes,
                            Views = contest.Views,
                            IsAwsContent = contest.IsAwsContent,
                            IsYoutubeContent = contest.IsYoutubeContent,
                            IsFacebookContent = contest.IsFacebookContent,
                            IsCompanyLibraryContent = false,
                            IsContestEntry = false,
                            BroadcastingDate = contest.BroadcastingDate ,
                            ContentType = Constants.Contest,
                            IsOptOutForNotification = contest.IsOptOutForNotification == null ? false : (bool)contest.IsOptOutForNotification,
                            IsExtendDateNotified = contest.IsExtendDateNotified == null ? false : (bool)contest.IsExtendDateNotified,
                            CreatedBy = u.UserId,
                            IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == contest.ContestId && ul.UserId == userId),
                        }).GroupBy(x => x.ContentId).Select(x => x.FirstOrDefault()).ToList().Union((from cm in dbContext.ContestMasters
                                                                                                     join u in dbContext.Users on cm.JoinedByUserId equals u.UserId
                                                                                                     join ct in dbContext.ContestTags on cm.ContestId equals ct.ContestId into d
                                                                                                     from con in d.DefaultIfEmpty()
                                                                                                     join t in dbContext.Tags on con.TagId equals t.TagId into contag
                                                                                                     from info in contag.DefaultIfEmpty()
                                                                                                     where cm.IsBroadcasted && u.Industry == industryId && (string.IsNullOrEmpty(search) || cm.ContentTitle.ToLower().Contains(search.ToLower()) || info.Name.ToLower().Contains(search.ToLower()))
                                                                                                     && //(DateTime.UtcNow - (contest.BroadcastingDate.HasValue? contest.BroadcastingDate: DateTime.UtcNow)).Value.TotalDays < 120
                                                                                                     DbFunctions.DiffDays(DateTime.UtcNow, cm.BroadcastingDate) <= 120
                                                                                                     select new BroadcastedContentViewModel
                                                                                                     {
                                                                                                         ContentId = cm.ContestMasterId,
                                                                                                         FirstName = u.FirstName,
                                                                                                         LastName = u.LastName,
                                                                                                         ProfilePicture = u.ProfilePicture,
                                                                                                         Name = cm.ContentTitle,
                                                                                                         Description = cm.Description,
                                                                                                         ContentUrl = cm.UploadedContentUrl,
                                                                                                         MimeType = Constants.VbPlayMimeType, //video/mp4
                                                                                                         Likes = cm.Likes,
                                                                                                         Views = cm.Views,
                                                                                                         IsAwsContent = cm.IsAwsContent,
                                                                                                         IsYoutubeContent = cm.IsYoutubeContent,
                                                                                                         IsFacebookContent = cm.IsFacebookContent,
                                                                                                         IsCompanyLibraryContent = false,
                                                                                                         IsContestEntry = true,
                                                                                                         BroadcastingDate = cm.BroadcastingDate,
                                                                                                         ContentType = Constants.ContestEntry,
                                                                                                         IsOptOutForNotification = cm.IsOptOutForNotification == null ? false : (bool)cm.IsOptOutForNotification,
                                                                                                         IsExtendDateNotified = cm.IsExtendDateNotified == null ? false : (bool)cm.IsExtendDateNotified,
                                                                                                         CreatedBy = u.UserId,
                                                                                                         IsLikedByUser = dbContext.ContestMasterUserLikes.Any(ul => ul.ContestMasterId == cm.ContestMasterId && ul.UserId == userId),
                                                                                                     }).GroupBy(x => x.ContentId).Select(x => x.FirstOrDefault()).ToList()).ToList().Union((from clc in dbContext.CompanyLibraryContents
                                                                                                                                                                                            join u in dbContext.Users on clc.CreatedBy equals u.UserId
                                                                                                                                                                                            join ct in dbContext.CompanyContentTags on clc.CompanyLibraryContentId equals ct.CompanyLibraryContentId into d
                                                                                                                                                                                            from con in d.DefaultIfEmpty()
                                                                                                                                                                                            join t in dbContext.Tags on con.TagId equals t.TagId into contag
                                                                                                                                                                                            from info in contag.DefaultIfEmpty()
                                                                                                                                                                                            where !clc.IsDeleted && clc.IsBroadcasted && clc.IsPublished && u.Industry == industryId && (string.IsNullOrEmpty(search) || clc.Name.ToLower().Contains(search.ToLower()) || info.Name.ToLower().Contains(search.ToLower()))
                                                                                                                                                                                            select new BroadcastedContentViewModel
                                                                                                                                                                                            {
                                                                                                                                                                                                ContentId = clc.CompanyLibraryContentId,
                                                                                                                                                                                                FirstName = u.FirstName,
                                                                                                                                                                                                LastName = u.LastName,
                                                                                                                                                                                                ProfilePicture = u.ProfilePicture,
                                                                                                                                                                                                Name = clc.Name,
                                                                                                                                                                                                Description = clc.Description,
                                                                                                                                                                                                ContentUrl = clc.ContentUrl,
                                                                                                                                                                                                MimeType = clc.MimeType,
                                                                                                                                                                                                Likes = 0,
                                                                                                                                                                                                Views = clc.ViewCount,
                                                                                                                                                                                                IsAwsContent = true,
                                                                                                                                                                                                IsCompanyLibraryContent = true,
                                                                                                                                                                                                BroadcastingDate = clc.BroadcastingDate,
                                                                                                                                                                                                ContentType = Constants.CompanyLibraryContent,
                                                                                                                                                                                                IsExtendDateNotified = clc.IsExtendDateNotified == null ? false : (bool)clc.IsExtendDateNotified,
                                                                                                                                                                                                IsOptOutForNotification = clc.IsOptOutForNotification == null ? false : (bool)clc.IsOptOutForNotification,
                                                                                                                                                                                                CreatedBy = u.UserId
                                                                                                                                                                                            }).GroupBy(x => x.ContentId).Select(x => x.FirstOrDefault()).ToList()).ToList();


            }
        }


        /// <summary>
        /// method to like or unlike a contest entry
        /// </summary>
        /// <param name="contest"></param>
        public long LikeContest(VBPlayContentCommonViewModel contestContent, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long contestOwner = 0;
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        if (!contestContent.IsContestEntry)
                        {
                            Contest existingContest = dbContext.Contests.Where(c => !c.IsDeleted && c.ContestId == contestContent.ContestContentId).FirstOrDefault();
                            if (existingContest != null)
                            {

                                bool isLikedByUser = dbContext.ContestUserLikes.Any(cu => cu.ContestId == contestContent.ContestContentId && cu.UserId == userId);
                                if (isLikedByUser)
                                {
                                    dbContext.ContestUserLikes.RemoveRange(dbContext.ContestUserLikes.Where(ul => ul.ContestId == contestContent.ContestContentId && ul.UserId == userId).ToList());
                                    existingContest.Likes = existingContest.Likes - 1; // removing the user's like count for the contest
                                    dbContext.SaveChanges();
                                }
                                else
                                {
                                    contestOwner = existingContest.CreatedBy;

                                    dbContext.ContestUserLikes.Add(new ContestUserLike
                                    {
                                        ContestId = contestContent.ContestContentId,
                                        UserId = userId
                                    });
                                    dbContext.SaveChanges();
                                    existingContest.Likes = existingContest.Likes + 1; // increasing likes count by one
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            ContestMaster existingContestMaster = dbContext.ContestMasters.Where(c => c.ContestMasterId == contestContent.ContestContentId).FirstOrDefault();
                            if (existingContestMaster != null)
                            {
                                bool isLikedByUser = dbContext.ContestMasterUserLikes.Any(cm => cm.ContestMasterId == existingContestMaster.ContestMasterId && cm.UserId == userId);
                                if (isLikedByUser)
                                {
                                    dbContext.ContestMasterUserLikes.RemoveRange(dbContext.ContestMasterUserLikes.Where(ul => ul.ContestMasterId == existingContestMaster.ContestMasterId && ul.UserId == userId).ToList());
                                    existingContestMaster.Likes = existingContestMaster.Likes - 1; // removing the user's like count for the contest
                                    dbContext.SaveChanges();
                                }
                                else
                                {
                                    contestOwner = existingContestMaster.UserId;

                                    dbContext.ContestMasterUserLikes.Add(new ContestMasterUserLike
                                    {
                                        ContestMasterId = existingContestMaster.ContestMasterId,
                                        UserId = userId
                                    });
                                    dbContext.SaveChanges();
                                    existingContestMaster.Likes = existingContestMaster.Likes + 1; // increasing likes count by one
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        transaction.Commit();
                        return contestOwner;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// method to increase view count of a video content
        /// </summary>
        /// <param name="contest"></param>
        public void ContestContentViews(SharedContentViewModel content)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                // increase the viewCount in db
                if (content != null && !string.IsNullOrEmpty(content.IPAddress))
                {
                    bool isViewConsidered = false;
                    if (content.ContentType == Constants.SharedContentType.Contest)
                    {
                        if (content.UserId != 0)
                        {
                            bool isViewAlreadyConsidered = dbContext.VbPlayContentUserViews.Any(uc => uc.UserId == content.UserId && uc.IPAddress == content.IPAddress && uc.ContestContentId == content.ContentId && !uc.IsContestEntry && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                int viewCountForSameUserWithDifferentIP = dbContext.VbPlayContentUserViews.Where(uc => uc.UserId == content.UserId && uc.ContestContentId == content.ContentId && !uc.IsContestEntry && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                                if (viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                                {
                                    dbContext.VbPlayContentUserViews.Add(new VbPlayContentUserView()
                                    {
                                        UserId = content.UserId,
                                        IPAddress = content.IPAddress,
                                        ContestContentId = content.ContentId,
                                        IsContestEntry = false,
                                        CreatedDate = DateTime.UtcNow
                                    });
                                    isViewConsidered = true;
                                }
                            }
                        }
                        else
                        {
                            bool isViewAlreadyConsidered = dbContext.VbPlayContentUserViews.Any(uc => uc.IPAddress == content.IPAddress && uc.ContestContentId == content.ContentId && !uc.IsContestEntry && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                dbContext.VbPlayContentUserViews.Add(new VbPlayContentUserView()
                                {
                                    UserId = content.UserId,
                                    IPAddress = content.IPAddress,
                                    ContestContentId = content.ContentId,
                                    IsContestEntry = false,
                                    CreatedDate = DateTime.UtcNow
                                });
                                isViewConsidered = true;
                            }
                        }
                        if (isViewConsidered)
                        {

                            Contest existingContest = dbContext.Contests.Where(c => c.ContestId == content.ContentId).FirstOrDefault();
                            if (existingContest != null)
                            {
                                existingContest.Views = existingContest.Views + 1;
                                dbContext.SaveChanges();

                                //Don't add points for expired content only views should be added.

                                bool isContentExpired = existingContest.IsWinnerAnnounced == null ? false : (bool)existingContest.IsWinnerAnnounced;
                                if (!isContentExpired)
                                {
                                    long bal = dbContext.EarnedPoints.Any(ep => ep.UserId == existingContest.CreatedBy) ? (long)dbContext.EarnedPoints.Where(ep => ep.UserId == existingContest.CreatedBy).Select(e => e.Points).FirstOrDefault() : 0;
                                    if (existingContest.Views == Constants.NumberOfViewsForGettingPoints)
                                    {
                                        dbContext.PointsTransactions.Add(new PointsTransaction()
                                        {
                                            ContestId = existingContest.ContestId,
                                            UserId = existingContest.CreatedBy,
                                            Points = Constants.HundredViewsContest,
                                            Description = existingContest.Name,
                                            CreatedDate = DateTime.UtcNow,
                                            IsEarned = true,
                                            Type = Constants.PointsEarnedType.Views.ToString(),
                                            Balance = bal + Constants.HundredViewsContest
                                        });
                                        //add the points to user earnedPoints
                                        var userEarnedPoints = dbContext.EarnedPoints.Where(ep => ep.UserId == existingContest.CreatedBy).FirstOrDefault();
                                        if (userEarnedPoints != null)
                                        {
                                            userEarnedPoints.Points = userEarnedPoints.Points + Constants.HundredViewsContest;
                                            dbContext.SaveChanges();
                                        }
                                    }

                                    if (content.IsVideo.HasValue && content.IsVideo.Value)
                                    {
                                        long viewerBal = dbContext.EarnedPoints.Any(ep => ep.UserId == content.UserId) ? (long)dbContext.EarnedPoints.Where(ep => ep.UserId == content.UserId).Select(e => e.Points).FirstOrDefault() : 0;
                                        dbContext.PointsTransactions.Add(new PointsTransaction()
                                        {
                                            ContestId = existingContest.ContestId,
                                            UserId = content.UserId,
                                            Points = Constants.ViewContest,
                                            Description = existingContest.Name,
                                            CreatedDate = DateTime.UtcNow,
                                            IsEarned = true,
                                            Type = Constants.PointsEarnedType.Views.ToString(),
                                            Balance = viewerBal + Constants.ViewContest
                                        });
                                        //add the points to user earnedPoints
                                        var userEarnedPoints = dbContext.EarnedPoints.Where(ep => ep.UserId == content.UserId).FirstOrDefault();
                                        if (userEarnedPoints != null)
                                        {
                                            userEarnedPoints.Points = userEarnedPoints.Points + Constants.ViewContest;
                                            dbContext.SaveChanges();
                                        }
                                        else
                                        {
                                            dbContext.EarnedPoints.Add(new EarnedPoint
                                            {
                                                UserId = content.UserId,
                                                Points = Constants.ViewContest
                                            });
                                            dbContext.SaveChanges();
                                        }
                                    }
                                }
                            }
                            dbContext.SaveChanges();
                        }

                    }
                    else if (content.ContentType == Constants.SharedContentType.ContestEntry)
                    {
                        if (content.UserId != 0)
                        {
                            bool isViewAlreadyConsidered = dbContext.VbPlayContentUserViews.Any(uc => uc.UserId == content.UserId && uc.IPAddress == content.IPAddress && uc.ContestContentId == content.ContentId && uc.IsContestEntry && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                int viewCountForSameUserWithDifferentIP = dbContext.VbPlayContentUserViews.Where(uc => uc.UserId == content.UserId && uc.ContestContentId == content.ContentId && uc.IsContestEntry && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                                if (viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                                {
                                    dbContext.VbPlayContentUserViews.Add(new VbPlayContentUserView()
                                    {
                                        UserId = content.UserId,
                                        IPAddress = content.IPAddress,
                                        ContestContentId = content.ContentId,
                                        IsContestEntry = true,
                                        CreatedDate = DateTime.UtcNow
                                    });

                                    isViewConsidered = true;
                                }
                            }
                        }
                        else
                        {
                            bool isViewAlreadyConsidered = dbContext.VbPlayContentUserViews.Any(uc => uc.IPAddress == content.IPAddress && uc.ContestContentId == content.ContentId && !uc.IsContestEntry && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                dbContext.VbPlayContentUserViews.Add(new VbPlayContentUserView()
                                {
                                    UserId = content.UserId,
                                    IPAddress = content.IPAddress,
                                    ContestContentId = content.ContentId,
                                    IsContestEntry = true,
                                    CreatedDate = DateTime.UtcNow
                                });
                                isViewConsidered = true;
                            }
                        }
                        if (isViewConsidered)
                        {
                            ContestMaster existingContest = dbContext.ContestMasters.Where(c => c.ContestMasterId == content.ContentId).FirstOrDefault();
                            if (existingContest != null)
                            {
                                existingContest.Views = existingContest.Views + 1;
                                dbContext.SaveChanges();

                                //Don't add points for expired content only views should be added.

                                Contest currentContest = dbContext.Contests.FirstOrDefault(x => x.ContestId == existingContest.ContestId);
                                bool isContentExpired = currentContest.IsWinnerAnnounced == null ? false : (bool)currentContest.IsWinnerAnnounced;

                                if (!isContentExpired)
                                {
                                    long bal = dbContext.EarnedPoints.Any(ep => ep.UserId == existingContest.JoinedByUserId) ? (long)dbContext.EarnedPoints.Where(ep => ep.UserId == existingContest.JoinedByUserId).Select(e => e.Points).FirstOrDefault() : 0;
                                    if (existingContest.Views == Constants.NumberOfViewsForGettingPoints)
                                    {
                                        dbContext.PointsTransactions.Add(new PointsTransaction()
                                        {
                                            ContestId = existingContest.ContestId,
                                            UserId = existingContest.JoinedByUserId,
                                            Points = Constants.HundredViewsContest,
                                            Description = existingContest.ContentTitle,
                                            CreatedDate = DateTime.UtcNow,
                                            IsEarned = true,
                                            Type = Constants.PointsEarnedType.Views.ToString(),
                                            Balance = bal + Constants.HundredViewsContest
                                        });
                                        //add the points to user earnedPoints
                                        var userEarnedPoints = dbContext.EarnedPoints.Where(ep => ep.UserId == existingContest.JoinedByUserId).FirstOrDefault();
                                        if (userEarnedPoints != null)
                                        {
                                            userEarnedPoints.Points = userEarnedPoints.Points + Constants.HundredViewsContest;
                                            dbContext.SaveChanges();
                                        }
                                    }

                                    if (content.IsVideo.HasValue && content.IsVideo.Value)
                                    {
                                        long viewerBal = dbContext.EarnedPoints.Any(ep => ep.UserId == content.UserId) ? (long)dbContext.EarnedPoints.Where(ep => ep.UserId == content.UserId).Select(e => e.Points).FirstOrDefault() : 0;
                                        dbContext.PointsTransactions.Add(new PointsTransaction()
                                        {
                                            ContestId = existingContest.ContestId,
                                            UserId = content.UserId,
                                            Points = Constants.ViewContest,
                                            Description = existingContest.ContentTitle,
                                            CreatedDate = DateTime.UtcNow,
                                            IsEarned = true,
                                            Type = Constants.PointsEarnedType.Views.ToString(),
                                            Balance = viewerBal + Constants.ViewContest
                                        });

                                        //add the points to user earnedPoints
                                        var userEarnedPoints = dbContext.EarnedPoints.Where(ep => ep.UserId == content.UserId).FirstOrDefault();
                                        if (userEarnedPoints != null)
                                        {
                                            userEarnedPoints.Points = userEarnedPoints.Points + Constants.ViewContest;
                                            dbContext.SaveChanges();
                                        }
                                        else
                                        {
                                            dbContext.EarnedPoints.Add(new EarnedPoint
                                            {
                                                UserId = content.UserId,
                                                Points = Constants.ViewContest
                                            });
                                            dbContext.SaveChanges();
                                        }
                                    }
                                }                               
                            }
                            dbContext.SaveChanges();
                        }
                    }
                    else
                    {
                        if (content.UserId != 0)
                        {
                            bool isViewAlreadyConsidered = dbContext.UserContentViews.Any(uc => uc.UserId == content.UserId && uc.IPAddress == content.IPAddress && uc.ContentId == content.ContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                int viewCountForSameUserWithDifferentIP = dbContext.UserContentViews.Where(uc => uc.UserId == content.UserId && uc.ContentId == content.ContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                                if (viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                                {
                                    dbContext.UserContentViews.Add(new UserContentViews()
                                    {
                                        UserId = content.UserId,
                                        IPAddress = content.IPAddress,
                                        ContentId = content.ContentId,
                                        IsMasterContent = false,
                                        CreatedDate = DateTime.UtcNow
                                    });
                                    isViewConsidered = true;
                                }
                            }
                        }
                        else
                        {
                            bool isViewAlreadyConsidered = dbContext.UserContentViews.Any(uc => uc.IPAddress == content.IPAddress && uc.ContentId == content.ContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                dbContext.UserContentViews.Add(new UserContentViews()
                                {
                                    UserId = content.UserId,
                                    IPAddress = content.IPAddress,
                                    ContentId = content.ContentId,
                                    IsMasterContent = false,
                                    CreatedDate = DateTime.UtcNow
                                });
                                isViewConsidered = true;
                            }
                        }
                        if (isViewConsidered)
                        {
                            CompanyLibraryContent existingContent = dbContext.CompanyLibraryContents.Where(cl => cl.CompanyLibraryContentId == content.ContentId).FirstOrDefault();
                            if (existingContent != null)
                            {
                                existingContent.ViewCount = existingContent.ViewCount + 1;
                                dbContext.SaveChanges();
                            }
                            dbContext.SaveChanges();
                        }

                    }
                }
            }
        }

        public VBPlayViewModel SubmitContestEntry(ContestEntrySubmitViewModel contestEntry)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var savedEntry = dbContext.ContestMasters.Where(cm => !cm.IsSubmitted && cm.ContestMasterId == contestEntry.ContestEntryId).FirstOrDefault();
                if (savedEntry != null)
                {
                    savedEntry.IsSubmitted = contestEntry.IsSubmitted;
                    dbContext.SaveChanges();
                }
                return (from c in dbContext.ContestMasters
                        join con in dbContext.Contests on c.ContestId equals con.ContestId
                        where c.ContestMasterId == contestEntry.ContestEntryId
                        select new VBPlayViewModel
                        {
                            ContestId = c.ContestMasterId,
                            Name = c.ContentTitle,
                            Description = c.Description,
                            ContentUrl = c.UploadedContentUrl,
                            MimeType = c.MimeType,
                            IsAwsContent = c.IsAwsContent,
                            IsYoutubeContent = c.IsYoutubeContent,
                            IsFacebookContent = c.IsFacebookContent,
                            IsContestEntry = true,
                            EndDate = con.EndDate
                        }).FirstOrDefault();
            }
        }

        public ChallengeEntryViewModel GetContestEntry(long contestEntryId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.ContestMasters
                        where c.ContestMasterId == contestEntryId
                        select new ChallengeEntryViewModel
                        {
                            ContestEntryId = c.ContestMasterId,
                            Name = c.ContentTitle,
                            Description = c.Description,
                            ContentUrl = c.UploadedContentUrl,
                            MimeType = c.MimeType,
                            IsAwsContent = c.IsAwsContent,
                            IsYoutubeContent = c.IsYoutubeContent,
                            IsFacebookContent = c.IsFacebookContent,
                        }).FirstOrDefault();
            }
        }


        public ChallengeEntryViewModel UpdateContestEntry(VBPlayCreateViewModel contestEntry)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                ContestMaster existingEntry = dbContext.ContestMasters.Where(cm => cm.ContestMasterId == contestEntry.ContestId).FirstOrDefault();
                if (existingEntry != null)
                {
                    existingEntry.ContentTitle = contestEntry.Name;
                    existingEntry.Description = contestEntry.Description;
                    existingEntry.JoinedDate = DateTime.UtcNow;
                    if (!string.IsNullOrEmpty(contestEntry.ContentUrl))
                    {
                        existingEntry.UploadedContentUrl = contestEntry.ContentUrl;
                        existingEntry.MimeType = contestEntry.MimeType;
                        existingEntry.IsAwsContent = contestEntry.IsAwsContent;
                        existingEntry.IsYoutubeContent = contestEntry.IsYoutubeContent;
                        existingEntry.IsFacebookContent = contestEntry.IsFacebookContent;
                    }
                    dbContext.SaveChanges();
                }
                return GetContestEntry(contestEntry.ContestId);
            }
        }

        public string GetContestContentUrl(VBPlayCreateViewModel contestEntry)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                string contentUrl = string.Empty;
                ContestMaster existingEntry = dbContext.ContestMasters.Where(cm => cm.ContestMasterId == contestEntry.ContestId).FirstOrDefault();
                if (existingEntry != null)
                {
                    contentUrl = existingEntry.UploadedContentUrl;
                }
                return contentUrl;
            }
        }

        public BroadcastedContentViewModel GetSharedContent(long contentId, Constants.SharedContentType contentType, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                VisibilityControlContentViewModel sharedContent = new VisibilityControlContentViewModel();
                //  var allowedVisibilityLevel = (int)Constants.ContentVisibility.Public;
                if (contentType == Constants.SharedContentType.Contest)
                {
                    sharedContent = (from contest in dbContext.Contests
                                     join u in dbContext.Users on contest.CreatedBy equals u.UserId
                                     where !contest.IsDeleted && contest.ContestId == contentId
                                     select new VisibilityControlContentViewModel
                                     {
                                         ContentId = contest.ContestId,
                                         FirstName = u.FirstName,
                                         LastName = u.LastName,
                                         ProfilePicture = u.ProfilePicture,
                                         Name = contest.Name,
                                         Description = contest.Description,
                                         ContentUrl = contest.UploadedContentUrl,
                                         Likes = contest.Likes,
                                         Views = contest.Views,
                                         IsAwsContent = contest.IsAwsContent,
                                         IsYoutubeContent = contest.IsYoutubeContent,
                                         IsFacebookContent = contest.IsFacebookContent,
                                         IsCompanyLibraryContent = false,
                                         CreatedBy = contest.CreatedBy,
                                         Visibility = contest.Visibility
                                     }).FirstOrDefault();
                }
                else if (contentType == Constants.SharedContentType.ContestEntry)
                {
                    sharedContent = (from cm in dbContext.ContestMasters
                                     join u in dbContext.Users on cm.JoinedByUserId equals u.UserId
                                     where cm.ContestMasterId == contentId
                                     select new VisibilityControlContentViewModel
                                     {
                                         ContentId = cm.ContestId,
                                         FirstName = u.FirstName,
                                         LastName = u.LastName,
                                         ProfilePicture = u.ProfilePicture,
                                         Name = cm.ContentTitle,
                                         Description = cm.Description,
                                         ContentUrl = cm.UploadedContentUrl,
                                         Likes = cm.Likes,
                                         Views = cm.Views,
                                         IsAwsContent = cm.IsAwsContent,
                                         IsYoutubeContent = cm.IsYoutubeContent,
                                         IsFacebookContent = cm.IsFacebookContent,
                                         IsCompanyLibraryContent = false,
                                         CreatedBy = cm.JoinedByUserId,
                                         Visibility = cm.Visibility
                                     }).FirstOrDefault();
                }
                else
                {
                    sharedContent = (from clc in dbContext.CompanyLibraryContents
                                     join u in dbContext.Users on clc.CreatedBy equals u.UserId
                                     where !clc.IsDeleted && clc.IsBroadcasted && clc.IsPublished && clc.CompanyLibraryContentId == contentId
                                     select new VisibilityControlContentViewModel
                                     {
                                         ContentId = clc.CompanyLibraryContentId,
                                         FirstName = u.FirstName,
                                         LastName = u.LastName,
                                         ProfilePicture = u.ProfilePicture,
                                         Name = clc.Name,
                                         Description = clc.Description,
                                         ContentUrl = clc.ContentUrl,
                                         MimeType = clc.MimeType,
                                         Likes = 0,
                                         Views = clc.ViewCount,
                                         IsAwsContent = true,
                                         IsCompanyLibraryContent = true,
                                         CreatedBy = clc.CreatedBy,
                                         Visibility = clc.Visibility
                                     }).FirstOrDefault();
                }
                BroadcastedContentViewModel resultContent = Mapper.Map<VisibilityControlContentViewModel, BroadcastedContentViewModel>(sharedContent);
                if (sharedContent.Visibility == (int)Constants.ContentVisibility.Public)
                {
                    return resultContent;
                }
                else
                {
                    if (userId != 0)
                    {
                        long companyIdOfLoggedInUser = (long)dbContext.Users.Where(u => u.UserId == userId).First().CompanyId;
                        long companyIdOfContentCreator = (long)dbContext.Users.Where(u => u.UserId == sharedContent.CreatedBy).First().CompanyId;
                        if (companyIdOfLoggedInUser == companyIdOfContentCreator)
                        {
                            return resultContent;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public EarnedPointsViewModel GetEarnedPoints(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                EarnedPointsViewModel earnedPoints = new EarnedPointsViewModel();
                earnedPoints.PointsHistory = (from pt in dbContext.PointsTransactions
                                              join cont in dbContext.Contests on pt.ContestId equals cont.ContestId into d
                                              from conPoints in d.DefaultIfEmpty()
                                              join p in dbContext.PlayLists on pt.PlaylistId equals p.PlayListId into e
                                              from playPoints in e.DefaultIfEmpty()
                                              where pt.UserId == userId
                                              select new PointsTransactionViewModel
                                              {
                                                  Type = pt.Type,
                                                  CreatedDate = pt.CreatedDate,
                                                  Contest = (playPoints == null && conPoints == null) ? pt.Description : (playPoints == null ? conPoints.Name : playPoints.Name),
                                                  IsEarned = pt.IsEarned,
                                                  Points = pt.Points,
                                                  Balance = pt.Balance,
                                              }).ToList();

                if (earnedPoints != null && earnedPoints.PointsHistory != null && earnedPoints.PointsHistory.Count() > 0)
                {
                    earnedPoints.PointsHistory.ForEach(pt =>
                    {
                        if (pt.CreatedDate != null)
                        {
                            pt.CreatedDate = pt.CreatedDate.Value.ToLocalTime();
                        }
                    });
                    earnedPoints.NumberOfContestCreated = earnedPoints.PointsHistory.Where(ph => ph.Type.ToLower().Equals(Constants.PointsEarnedType.Create.ToString().ToLower())).Count();
                    earnedPoints.ContestCreatedEarnedPoints = earnedPoints.NumberOfContestCreated == 0 ? 0 : (long)earnedPoints.PointsHistory.Where(ph => ph.Type.ToLower().Equals(Constants.PointsEarnedType.Create.ToString().ToLower())).Sum(p => p.Points);
                    earnedPoints.NumberOfContestPlayed = earnedPoints.PointsHistory.Where(ph => ph.Type.ToLower().Equals(Constants.PointsEarnedType.Participant.ToString().ToLower())).Count();
                    earnedPoints.ContestPlayedEarnedPoints = earnedPoints.NumberOfContestPlayed == 0 ? 0 : (long)earnedPoints.PointsHistory.Where(ph => ph.Type.ToLower().Equals(Constants.PointsEarnedType.Participant.ToString().ToLower())).Sum(p => p.Points);
                    earnedPoints.TotalEarnedPoints = earnedPoints.PointsHistory.Any(ph => ph.IsEarned) ? (long)earnedPoints.PointsHistory.Where(ph => ph.IsEarned).Sum(p => p.Points) : 0;
                    earnedPoints.PointsRedeemed = earnedPoints.PointsHistory.Any(ph => !ph.IsEarned) ? (long)earnedPoints.PointsHistory.Where(ph => !ph.IsEarned).Sum(p => p.Points) : 0;
                    earnedPoints.BalancePoints = dbContext.EarnedPoints.Any(ep => ep.UserId == userId) ? (long)dbContext.EarnedPoints.Where(ep => ep.UserId == userId).Select(p => p.Points).FirstOrDefault() : 0;
                }
                return earnedPoints;

            }
        }

        //delete a contest 
        public bool DeleteContest(VBPlayViewModel contest)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Contest existingContest = dbContext.Contests.Where(c => !c.IsDeleted && c.ContestId == contest.ContestId && !c.IsCompleted).FirstOrDefault();
                if(DeleteReportedContent(contest, dbContext))
                {
                    return true;
                }
                if (existingContest != null && existingContest.EndDate > DateTime.UtcNow)
                {

                    if (existingContest.StartDate < DateTime.UtcNow && existingContest.EndDate > DateTime.UtcNow)
                    {
                        bool contestEntryExist = dbContext.ContestMasters.Any(cm => cm.IsSubmitted && cm.ContestId == existingContest.ContestId);
                        if (!contestEntryExist)
                        {
                            existingContest.IsDeleted = true;
                            dbContext.SaveChanges();
                            
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        existingContest.IsDeleted = true;
                        dbContext.SaveChanges();
                        return true;
                    }
                }
                return false;

            }
        }

        private bool DeleteReportedContent(VBPlayViewModel contest, VirtualBrandWorksEntities dbContext)
        {
            if (IsReportedContent(contest.ContestId))
            {
                Contest existingContest = dbContext.Contests.Where(c => !c.IsDeleted && c.ContestId == contest.ContestId).FirstOrDefault();

                if (existingContest != null)
                {

                    existingContest.IsDeleted = true;
                    dbContext.SaveChanges();
                    RemovePointsForContentDeletedForReportAbuse(contest, dbContext);
                    return true;
                }
            }
            return false;
        }

        private void RemovePointsForContentDeletedForReportAbuse(VBPlayViewModel contest, VirtualBrandWorksEntities dbContext)
        {
            //Removing points which were given to the user for created content.
            var deletedContest = dbContext.Contests.FirstOrDefault(x => x.ContestId == contest.ContestId && x.IsDeleted);
            var userId = deletedContest.CreatedBy;
            var transactions = dbContext.PointsTransactions.Where(x => x.UserId == userId && x.ContestId == contest.ContestId 
                                    && (x.Type == Constants.PointsEarnedType.Create.ToString() || x.Type == Constants.PointsEarnedType.Views.ToString())).ToList();
            if(transactions != null)
            {
                dbContext.PointsTransactions.RemoveRange(transactions);
                dbContext.SaveChanges();
                long pointsToRemove = (long)transactions.Sum(x => x.Points);
                var userEarnedPoints = dbContext.EarnedPoints.Where(ep => ep.UserId == userId).FirstOrDefault();
                if (userEarnedPoints != null)
                {
                    userEarnedPoints.Points = userEarnedPoints.Points - (int)pointsToRemove;
                    dbContext.SaveChanges();
                }
            }
        }

        private bool IsReportedContent(long contestId)
        {
            using( VirtualBrandWorksEntities dbcontext = new VirtualBrandWorksEntities())
            {
                return dbcontext.ReportAbuses.Any(x => x.ContentId == contestId);
            }
        }

        public void ExtendContentDate(long contestId, long notificationId, string type)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var notification = dbContext.Notifications.FirstOrDefault(x => x.NotificationId == notificationId);
                if (notification == null) throw new Exception("Notification does not exists.");
                bool isOptOut = notification.IsOptOut.HasValue? (bool)notification.IsOptOut : false;
                if(!isOptOut)
                {
                    if (type == Constants.Contest)
                    {
                        Contest objContest = dbContext.Contests.Where(m => m.ContestId == contestId).FirstOrDefault();

                        if (objContest != null)
                        {
                            objContest.BroadcastingDate = objContest.BroadcastingDate.HasValue ?
                               objContest.BroadcastingDate.Value.AddDays(120) : DateTime.Now.AddDays(120);
                            objContest.IsOptOutForNotification = true;
                        }
                    }
                    else if (type == Constants.ContestEntry)
                    {
                        ContestMaster objContestEntry = dbContext.ContestMasters.Where(m => m.ContestMasterId == contestId).FirstOrDefault();

                        if (objContestEntry != null)
                        {
                            objContestEntry.BroadcastingDate = objContestEntry.BroadcastingDate.HasValue ?
                               objContestEntry.BroadcastingDate.Value.AddDays(120) : DateTime.Now.AddDays(120);
                            objContestEntry.IsOptOutForNotification = true;
                        }
                    }
                    else if (type == Constants.CompanyLibraryContent)
                    {
                        CompanyLibraryContent objCompanyLibraryContent = dbContext.CompanyLibraryContents.Where(m => m.CompanyLibraryContentId == contestId).FirstOrDefault();

                        if (objCompanyLibraryContent != null)
                        {
                            objCompanyLibraryContent.BroadcastingDate = objCompanyLibraryContent.BroadcastingDate.HasValue ?
                               objCompanyLibraryContent.BroadcastingDate.Value.AddDays(120) : DateTime.Now.AddDays(120);
                            objCompanyLibraryContent.IsOptOutForNotification = true;
                        }
                    }

                    dbContext.SaveChanges();

                    OptOutNotification(notificationId);
                }              
            }
        }

        public void OptOutNotification(long notificationId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Notification objnotification = dbContext.Notifications.Where(m => m.NotificationId == notificationId).FirstOrDefault();

                if (objnotification != null)
                {
                    objnotification.IsOptOut = true;
                }

                dbContext.SaveChanges();
            }
        }

        public void UpdateVbPlayContentVisibility(ContestVisibilityViewModel contest)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (!contest.IsContestEntry)
                {
                    Contest existingContest = dbContext.Contests.Where(c => !c.IsDeleted && c.ContestId == contest.ContestId && c.CreatedBy == contest.UserId).FirstOrDefault();
                    if (existingContest != null)
                    {
                        existingContest.Visibility = contest.Visibility;
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    ContestMaster exisitingContestMaster = dbContext.ContestMasters.Where(cm => cm.ContestMasterId == contest.ContestId && cm.JoinedByUserId == contest.UserId).FirstOrDefault();
                    if (exisitingContestMaster != null)
                    {
                        exisitingContestMaster.Visibility = contest.Visibility;
                        dbContext.SaveChanges();
                    }
                }
            }
        }

        public VBPlayViewModel SubmitContest(ContestEntrySubmitViewModel contestEntry)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var savedEntry = dbContext.Contests.Where(cm => (cm.IsSubmitted.HasValue && !cm.IsSubmitted.Value) && cm.ContestId == contestEntry.ContestEntryId).FirstOrDefault();
                if (savedEntry != null)
                {
                    savedEntry.IsSubmitted = contestEntry.IsSubmitted;
                    dbContext.SaveChanges();
                }
                return (from c in dbContext.Contests
                        where c.ContestId == contestEntry.ContestEntryId
                        select new VBPlayViewModel
                        {
                            ContestId = c.ContestId,
                            Name = c.Name,
                            Description = c.Description,
                            ContentUrl = c.UploadedContentUrl,
                            MimeType = c.MimeType,
                            IsAwsContent = c.IsAwsContent,
                            IsYoutubeContent = c.IsYoutubeContent,
                            IsFacebookContent = c.IsFacebookContent,
                            IsContestEntry = true,
                            EndDate = c.EndDate
                        }).FirstOrDefault();
            }
        }

        public string GetContestUrl(VBPlayUpdateViewModel contest)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                string contentUrl = string.Empty;
                Contest existingEntry = dbContext.Contests.Where(cm => cm.ContestId == contest.ContestId).FirstOrDefault();
                if (existingEntry != null)
                {
                    contentUrl = existingEntry.UploadedContentUrl;
                }
                return contentUrl;
            }
        }

        public VBPlayViewModel UpdateContest(VBPlayUpdateViewModel contest)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Contest existingEntry = dbContext.Contests.Where(cm => cm.ContestId == contest.ContestId).FirstOrDefault();
                if (existingEntry != null)
                {
                    existingEntry.Name = contest.Name;
                    existingEntry.Description = contest.Description;
                    existingEntry.StartDate = contest.StartDate;
                    existingEntry.EndDate = contest.EndDate;
                    existingEntry.Rules = contest.Rules;

                    if (!string.IsNullOrEmpty(contest.ContentUrl))
                    {
                        existingEntry.UploadedContentUrl = contest.ContentUrl;
                        existingEntry.MimeType = contest.MimeType;
                        existingEntry.IsAwsContent = contest.IsAwsContent;
                        existingEntry.IsYoutubeContent = contest.IsYoutubeContent;
                        existingEntry.IsFacebookContent = contest.IsFacebookContent;
                        existingEntry.MimeType = contest.MimeType;
                    }

                    SaveTaggedUsers(contest.TagIds, contest.ContestId);

                    dbContext.SaveChanges();
                }
                return GetContest(contest.ContestId);
            }
        }

        public List<ReportAbuseViewModel> GetAllReportAbuse(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var userRole = (Constants.UserRoles)dbContext.UserRoles.FirstOrDefault(x => x.UserId == userId).RoleId; 
                if (userRole == Constants.UserRoles.Admin)
                {
                    long companyId = (long)dbContext.Users.Where(u => !u.IsDeleted && u.UserId == userId).First().CompanyId;

                    var reports = (from ra in dbContext.ReportAbuses
                                   join c in dbContext.Contests on ra.ContentId equals c.ContestId
                                   join u in dbContext.Users on ra.UserId equals u.UserId
                                   where !c.IsDeleted && u.CompanyId == companyId
                                   select new ReportAbuseViewModel()
                                   {
                                       ReportAbuseId = ra.ReportAbuseId,
                                       UserId = c.CreatedBy,
                                       ContentUrl = c.UploadedContentUrl,
                                       ContestId = c.ContestId,
                                       Description = c.Description,
                                       EndDate = c.EndDate,
                                       Name = c.Name,
                                       Rules = c.Rules,
                                       StartDate = c.StartDate,
                                       IsAwsContent = c.IsAwsContent,
                                       IsYoutubeContent = c.IsYoutubeContent,
                                       IsFacebookContent = c.IsFacebookContent,
                                       CreatedDate = c.CreatedDate,
                                       MimeType = c.MimeType,
                                       Likes = c.Likes,
                                       Views = c.Views,
                                       Users = (from con in dbContext.ContestMasters
                                                where con.ContestId == c.ContestId
                                                select con.JoinedByUserId).Distinct().ToList().Union(from cont in dbContext.Contests
                                                                                                     where cont.ContestId == c.ContestId
                                                                                                     select cont.CreatedBy).Distinct().Count(),
                                       Tags = (from con in dbContext.ContestTags
                                               join t in dbContext.Tags on con.TagId equals t.TagId
                                               where con.ContestId == c.ContestId
                                               select new TagModel
                                               {
                                                   TagId = t.TagId,
                                                   Name = t.Name,
                                                   IsHandle = ((t.UserId == null || t.UserId == 0) && (t.TeamId == null || t.TeamId == 0)) ? false : true
                                               }).ToList(),
                                       FirstName = dbContext.Users.FirstOrDefault(x => x.UserId == c.CreatedBy).FirstName,
                                       LastName = dbContext.Users.FirstOrDefault(x => x.UserId == c.CreatedBy).LastName,
                                       ProfilePicture = dbContext.Users.FirstOrDefault(x => x.UserId == c.CreatedBy).ProfilePicture,
                                       IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == c.ContestId && ul.UserId == userId),
                                       IsJoinedByUser = (c.CreatedBy == userId || dbContext.ContestMasters.Any(cm => cm.ContestId == c.ContestId && cm.JoinedByUserId == userId)),
                                       IsBroadCasted = c.IsBroadCasted,
                                       IsCompleted = c.IsCompleted,
                                       Visibility = c.Visibility,
                                       IsSubmitted = (c.IsSubmitted.HasValue) ? c.IsSubmitted.Value : false
                                   }).ToList().GroupBy(x => x.ContestId).Select(x => x.FirstOrDefault()).ToList();

                    foreach(var report in reports)
                    {
                        report.ReportedByUsers = new List<string>();
                        List<string> user = (from rp in dbContext.ReportAbuses
                                             join c in dbContext.Contests on rp.ContentId equals c.ContestId
                                             join u in dbContext.Users on rp.UserId equals u.UserId
                                             where !c.IsDeleted && u.CompanyId == companyId && c.ContestId == report.ContestId
                                             select new { UserName = u.FirstName + " " + u.LastName }).Distinct().Select(x => x.UserName).ToList();
                        report.ReportedByUsers.AddRange(user);
                    }
                    return reports;
                }
                else
                {
                    long companyId = (long)dbContext.Users.Where(u => !u.IsDeleted && u.UserId == userId).First().CompanyId;// get companyid for current user

                    var reports = (from u in dbContext.Users
                                   join c in dbContext.Contests on u.UserId equals c.CreatedBy
                                   join r in dbContext.ReportAbuses on c.ContestId equals r.ContentId
                                   where !c.IsDeleted && u.CompanyId == companyId
                                   select new ReportAbuseViewModel()
                                   {
                                       ReportAbuseId = r.ReportAbuseId,
                                       UserId = c.CreatedBy,
                                       ContentUrl = c.UploadedContentUrl,
                                       ContestId = c.ContestId,
                                       Description = c.Description,
                                       EndDate = c.EndDate,
                                       Name = c.Name,
                                       Rules = c.Rules,
                                       StartDate = c.StartDate,
                                       IsAwsContent = c.IsAwsContent,
                                       IsYoutubeContent = c.IsYoutubeContent,
                                       IsFacebookContent = c.IsFacebookContent,
                                       CreatedDate = c.CreatedDate,
                                       MimeType = c.MimeType,
                                       Likes = c.Likes,
                                       Views = c.Views,
                                       Users = (from con in dbContext.ContestMasters
                                                where con.ContestId == c.ContestId
                                                select con.JoinedByUserId).Distinct().ToList().Union(from cont in dbContext.Contests
                                                                                                     where cont.ContestId == c.ContestId
                                                                                                     select cont.CreatedBy).Distinct().Count(),
                                       Tags = (from con in dbContext.ContestTags
                                               join t in dbContext.Tags on con.TagId equals t.TagId
                                               where con.ContestId == c.ContestId
                                               select new TagModel
                                               {
                                                   TagId = t.TagId,
                                                   Name = t.Name,
                                                   IsHandle = ((t.UserId == null || t.UserId == 0) && (t.TeamId == null || t.TeamId == 0)) ? false : true
                                               }).ToList(),
                                       FirstName = dbContext.Users.FirstOrDefault(x => x.UserId == c.CreatedBy).FirstName,
                                       LastName = dbContext.Users.FirstOrDefault(x => x.UserId == c.CreatedBy).LastName,
                                       ProfilePicture = dbContext.Users.FirstOrDefault(x => x.UserId == c.CreatedBy).ProfilePicture,
                                       IsLikedByUser = dbContext.ContestUserLikes.Any(ul => ul.ContestId == c.ContestId && ul.UserId == userId),
                                       IsJoinedByUser = (c.CreatedBy == userId || dbContext.ContestMasters.Any(cm => cm.ContestId == c.ContestId && cm.JoinedByUserId == userId)),
                                       IsBroadCasted = c.IsBroadCasted,
                                       IsCompleted = c.IsCompleted,
                                       Visibility = c.Visibility,
                                       IsSubmitted = (c.IsSubmitted.HasValue) ? c.IsSubmitted.Value : false
                                   }).ToList().GroupBy(x => x.ContestId).Select(x => x.FirstOrDefault()).ToList();
                    foreach (var report in reports)
                    {
                        report.ReportedByUsers = new List<string>();
                        List<string> user = (from rp in dbContext.ReportAbuses
                                             join c in dbContext.Contests on rp.ContentId equals c.ContestId
                                             join u in dbContext.Users on rp.UserId equals u.UserId
                                             where !c.IsDeleted && u.CompanyId == companyId && c.ContestId == report.ContestId
                                             select new { UserName = u.FirstName + " " + u.LastName }).Distinct().Select(x => x.UserName).ToList();
                        report.ReportedByUsers.AddRange(user);
                    }
                    return reports;
                }   
            }
        }

        public void ReportAbuse(long contestId, long userId)
        {
            using (VirtualBrandWorksEntities dbcontext = new VirtualBrandWorksEntities())
            {
                var contest = dbcontext.Contests.FirstOrDefault(x => x.IsDeleted == false && x.ContestId == contestId);
                var user = dbcontext.Users.FirstOrDefault(x => x.IsDeleted == false && x.UserId == userId && x.IsActive == true);
                if(user != null && contest != null)
                {
                    var isReportedAlready = IsContestAlreadyReported(contestId, userId);
                    if (!isReportedAlready)
                    {
                        dbcontext.ReportAbuses.Add(new ReportAbuse
                        {
                            ContentId = contestId,
                            UserId = userId,
                            CreatedDate = DateTime.UtcNow
                        });
                        dbcontext.SaveChanges();
                    }
                }
            }
        }

        public void DeleteReportAbuse(long contestId)
        {
            using (VirtualBrandWorksEntities dbcontext = new VirtualBrandWorksEntities())
            {
                var reports = dbcontext.ReportAbuses.Where(x => x.ContentId == contestId).ToList();
                if (reports != null && reports.Count > 0)
                {
                    dbcontext.ReportAbuses.RemoveRange(reports);
                    dbcontext.SaveChanges();
                }
            }
        }

        public void SetNotificationExtenstionFlag(BroadcastedContentViewModel broadcastedContent, bool flag)
        {
            using (VirtualBrandWorksEntities dbcontext = new VirtualBrandWorksEntities())
            {
                if(broadcastedContent.ContentType == Constants.Contest)
                {
                    var contest = dbcontext.Contests.FirstOrDefault(x => x.ContestId == broadcastedContent.ContentId);
                    if(contest != null)
                    {
                        contest.IsExtendDateNotified = flag;
                        dbcontext.SaveChanges();
                    }

                }

                if(broadcastedContent.ContentType == Constants.ContestEntry)
                {
                    var contestMasters = dbcontext.ContestMasters.FirstOrDefault(x => x.ContestMasterId == broadcastedContent.ContentId);
                    if(contestMasters != null)
                    {
                        contestMasters.IsExtendDateNotified = flag;
                        dbcontext.SaveChanges();
                    }

                }
                
                if(broadcastedContent.ContentType == Constants.CompanyLibraryContent)
                {
                    var companyLibraryContent = dbcontext.CompanyLibraryContents.FirstOrDefault(x => x.CompanyLibraryContentId == broadcastedContent.ContentId);
                    if(companyLibraryContent != null)
                    {
                        companyLibraryContent.IsExtendDateNotified = flag;
                        dbcontext.SaveChanges();
                    }

                }
            }
        }

        public List<long> GetUserIdsOfAdminAndTrainerOfACompany(long userId)
        {
            using (VirtualBrandWorksEntities dbcontext = new VirtualBrandWorksEntities())
            {
                var user = dbcontext.Users.FirstOrDefault(x => x.UserId == userId && !x.IsDeleted && x.IsActive);
                var listOfUsers = new List<long>();
                if (user != null)
                {
                    var companyId = user.CompanyId;
                    var users = (from u in dbcontext.Users
                                 join ur in dbcontext.UserRoles on u.UserId equals ur.UserId
                                 where u.CompanyId == companyId && (ur.RoleId == 2 || ur.RoleId == 3) && u.IsActive && !u.IsDeleted
                                 select u)
                                .Select(x => x.UserId).ToList();
                    listOfUsers = users;
                }
                return listOfUsers;
            }
        }
        
        public bool IsContestAlreadyReported(long contestId, long userId)
        {
            using (VirtualBrandWorksEntities dbcontext = new VirtualBrandWorksEntities())
            {
                return dbcontext.ReportAbuses.Any(x => x.UserId == userId && x.ContentId == contestId);
            }
        }
    }
}
