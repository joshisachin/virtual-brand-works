﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using Repository.Helpers;
using VirtualBrandWork.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace Repository.Repository
{
    public class UserInformationRepository : IUserInformationRepository
    {
        public List<CountryViewModel> GetCountries()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<CountryViewModel> countries = new List<CountryViewModel>();
                List<Country> countryList = context.Countries.ToList();
                countries = Mapper.Map<List<Country>, List<CountryViewModel>>(countryList);
                return countries;
            }
        }

        public List<StateViewModel> GetStates(int countryID)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<StateViewModel> states = new List<StateViewModel>();
                List<State> stateList = context.States.Where(s => s.CountryID == countryID).ToList();
                states = Mapper.Map<List<State>, List<StateViewModel>>(stateList);
                return states;
            }
        }

        public List<CityViewModel> GetCities(int stateID)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<CityViewModel> cities = new List<CityViewModel>();
                List<City> cityList = context.Cities.Where(c => c.StateID == stateID).ToList();
                cities = Mapper.Map<List<City>, List<CityViewModel>>(cityList);
                return cities;
            }
        }

        public List<CityViewModel> GetLocation(string location)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<CityViewModel> cities = new List<CityViewModel>();
                List<City> cityList = context.Cities.Where(c => c.Name.StartsWith(location)).Take(Constants.LimitDropDownSize).ToList();
                cities = Mapper.Map<List<City>, List<CityViewModel>>(cityList);
                return cities;
            }
        }

        public List<UserReponseViewModel> GetCompanyUsers(UserRequestViewModel request)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                bool IsUserExist = context.Users.Where(u => !u.IsDeleted && u.UserId == request.UserId).Any();
                if (IsUserExist)
                {
                    if (string.IsNullOrEmpty(request.SortBy))
                    {
                        request.SortBy = Constants.DEFAULT_SORTING_COMPANY_USERS;
                    }
                    request.CompanyId = (long)context.Users.Where(u => !u.IsDeleted && u.UserId == request.UserId).First().CompanyId;
                    return (from c in context.Companies
                            join u in context.Users on c.CompanyId equals u.CompanyId
                            join ur in context.UserRoles on u.UserId equals ur.UserId
                            join city in context.Cities on u.CityId equals city.ID
                            where !c.IsDeleted
                            && !u.IsDeleted && c.CompanyId == request.CompanyId
                            && request.UserRole.Contains(ur.RoleId.ToString())
                            && (string.IsNullOrEmpty(request.Search)
                                        || (u.FirstName+" "+u.LastName).ToLower().Contains(request.Search.ToLower()))
                            select new UserReponseViewModel
                            {
                                UserId = u.UserId,
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                ProfilePicture = u.ProfilePicture,
                                Email = u.EmailId,
                                RoleId = ur.RoleId,
                                Status = u.IsActive,
                                ContactNo = u.PhoneNumber,
                                Location = city.Name,
                                TotalStudent = (from t in context.Teams
                                                join ut in context.UserTeams on t.TeamId equals ut.TeamId
                                                where t.TrainerId == u.UserId
                                                select ut.UserId).Distinct().Count(),//update
                                TotalTeam = context.Teams.Where(t => !t.isDeleted && t.TrainerId == u.UserId).Count(),//update
                                AssignedPlayList = 0, //update,
                                HandleName = u.UserHandle
                            }).OrderBy(request.SortBy)
                            .Skip(request.PageSize * (request.PageIndex - 1))
                            .Take(request.PageSize).ToList();
                }
                return null;

            }
        }

        public UserProfileViewModel GetUserProfile(UserProfileViewModel user)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                long roleId = context.UserRoles.Where(r => r.UserId == user.UserId).First().RoleId;
                return (from u in context.Users
                        join c in context.Countries on u.CountryId equals c.ID
                        join s in context.States on u.StateId equals s.ID
                        join city in context.Cities on u.CityId equals city.ID
                        join comp in context.Companies on u.CompanyId equals comp.CompanyId
                        where !u.IsDeleted && u.UserId == user.UserId
                        select new UserProfileViewModel
                        {
                            UserId = u.UserId,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            ProfilePicture = u.ProfilePicture,
                            Status = u.IsActive,
                            Description = u.Description,
                            EmailId = u.EmailId,
                            PhoneNumber = u.PhoneNumber,
                            AddressOne = u.Address,
                            AddressTwo = u.Address2,
                            Country = c.Name,
                            State = s.Name,
                            City = city.Name,
                            CountryId = (int)u.CountryId,
                            StateId = (int)u.StateId,
                            CityId = (int)u. CityId,
                            Zip = u.Zip,
                            DateOfJoining = u.CreatedDate,
                            Tags = (from u in context.Users
                                    join ut in context.UserTags on u.UserId equals ut.UserId
                                    join t in context.Tags on ut.TagId equals t.TagId
                                    where !u.IsDeleted && u.UserId == user.UserId
                                    select new TagViewModel
                                    {
                                        TagId = t.TagId,
                                        Name = t.Name,
                                        ShortName = t.ShortName

                                    }).ToList(),
                            HandleName = u.UserHandle

                        }).Single();
            }
        }

        public void UpdateUserRole(UserRequestViewModel userRequestViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                UserRole userRole = (from u in context.Users
                                     join ur in context.UserRoles on u.UserId equals ur.UserId
                                     where u.UserId == userRequestViewModel.UserId && !u.IsDeleted
                                     select ur).Single();
                userRole.RoleId = Convert.ToInt64(userRequestViewModel.RoleId);
                context.SaveChanges();
            }
        }

        public bool UpdateUserStatus(UserRequestViewModel userRequestViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                User user = context.Users.Where(obj => obj.UserId == userRequestViewModel.UserId && !obj.IsDeleted).Single();
                user.IsActive = userRequestViewModel.IsActive;
                user.ModifiedBy = userRequestViewModel.UserId;
                user.ModifiedDate = DateTime.UtcNow;
                context.SaveChanges();
                return user.IsActive;
            }
        }

        public string UpdateProfilePicture(long userId, string profilePictureName)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                string oldPicName = string.Empty;
                User user = (from u in context.Users
                             where u.UserId == userId
                             select u).FirstOrDefault();
                if (user != null)
                {
                    oldPicName = user.ProfilePicture;
                    user.ProfilePicture = profilePictureName;
                    context.SaveChanges();
                }
                return oldPicName;
            }
        }

        public bool CheckUserLimitForCompany(CompanyViewModel company)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                bool IsUserExist = context.Users.Where(u => !u.IsDeleted && u.UserId == company.UserId).Any();
                if (IsUserExist)
                {
                    company.CompanyId = (long)context.Users.Where(u => !u.IsDeleted && u.UserId == company.UserId).First().CompanyId;
                    //get the number of active registered users for the company 
                    var users = context.Users.Where(u => !u.IsDeleted && u.IsActive && u.CompanyId == company.CompanyId);

                    if (users.Count() > 0)
                    {
                        var usersUpto = (from c in context.CompanySubscription
                                         join sp in context.SubscriptionPlanPrice on c.SubscriptionPlanPriceId equals sp.SubscriptionPlanPriceId
                                         join s in context.Subscriptions on sp.SubscriptionId equals s.SubscriptionId
                                         where !s.IsDeleted && c.CompanyId == company.CompanyId && c.IsActive
                                         select new
                                         {
                                             c.CompanySubscriptionId,
                                             s.NumberOfUsers
                                         }
                                       ).OrderByDescending(x => x.CompanySubscriptionId).Select(x => x.NumberOfUsers).FirstOrDefault();

                        if (usersUpto != null)
                        {
                            if (usersUpto > users.Count())
                            {
                                return true;
                            }
                        }
                        return false;
                    }
                }
                return false;
            }
        }

        public bool IsUserExist(string email)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
               return context.Users.Any(u => !u.IsDeleted && u.EmailId.Equals(email,StringComparison.OrdinalIgnoreCase));
            }
        }
        public void AddUserInvite(UserInviteViewModel userInvite)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                bool IsUserExist = context.Users.Where(u => !u.IsDeleted && u.UserId == userInvite.UserId).Any();
                if (IsUserExist)
                {
                    userInvite.CompanyId = (long)context.Users.Where(u => !u.IsDeleted && u.UserId == userInvite.UserId).First().CompanyId;
                    if (userInvite.RoleId == Convert.ToInt32(Constants.UserRoles.Trainer) || userInvite.RoleId == Convert.ToInt32(Constants.UserRoles.User))
                    {
                        UserInvite user = new UserInvite();
                        user = Mapper.Map<UserInviteViewModel, UserInvite>(userInvite);
                        user.CreatedDate = DateTime.UtcNow;
                        context.UserInvites.Add(user);
                        context.SaveChanges();
                    }
                }
            }
        }

        public UserInviteViewModel GetUserInvite(string email)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from u in dbContext.UserInvites
                        join c in dbContext.Companies on u.CompanyId equals c.CompanyId
                        where u.EmailId.Equals(email)
                        select new UserInviteViewModel()
                        {
                            UserInviteId = u.UserInviteId,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            RoleId = u.RoleId,
                            CompanyId = u.CompanyId,
                            EmailId = u.EmailId,
                            CompanyName = c.Name
                        }).FirstOrDefault();

            }
        }

        public bool UpdateNotificationSetting(UserRequestViewModel userRequestViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                User user = context.Users.Where(obj => obj.UserId == userRequestViewModel.UserId && obj.IsActive && !obj.IsDeleted).SingleOrDefault();
                if (user != null)
                {
                    user.IsNotificationEnabled = userRequestViewModel.IsNotificationEnabled;
                    user.ModifiedBy = userRequestViewModel.ModifiedBy;
                    user.ModifiedDate = DateTime.UtcNow;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public UserProfileViewModel UpdateUserProfile(UserProfileViewModel userProfileViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        User user = dbContext.Users.Where(obj => obj.UserId == userProfileViewModel.UserId).Select(u => u).Single();
                        user.FirstName = userProfileViewModel.FirstName;
                        user.LastName = userProfileViewModel.LastName;
                        user.Description = userProfileViewModel.Description;
                        user.Address = userProfileViewModel.AddressOne;
                        user.Address2 = userProfileViewModel.AddressTwo;
                        user.CityId = userProfileViewModel.CityId;
                        user.StateId = userProfileViewModel.StateId;
                        user.CountryId = userProfileViewModel.CountryId;
                        user.Zip = userProfileViewModel.Zip;
                        user.PhoneNumber = userProfileViewModel.PhoneNumber;

                        dbContext.UserTags.RemoveRange((from ut in dbContext.UserTags
                                                        where ut.UserId == userProfileViewModel.UserId
                                                        select ut).ToList());

                        dbContext.SaveChanges();

                        List<UserTag> tags = new List<UserTag>();
                        if (userProfileViewModel.Tags != null)
                        {
                            userProfileViewModel.Tags.ForEach(obj =>
                            {
                                tags.Add(new UserTag() { UserId = userProfileViewModel.UserId, TagId = obj.TagId });
                            });
                        }

                        dbContext.UserTags.AddRange(tags);

                        dbContext.SaveChanges();

                        transaction.Commit();

                        return GetUserProfile(userProfileViewModel);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
        }

        public CompanyInfoViewModel UpdateCompanyProfile(CompanyInfoViewModel companyInfo)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long roleId = dbContext.UserRoles.Where(r => r.UserId == companyInfo.UserId).First().RoleId;
                if (roleId == (int)Constants.UserRoles.Admin)
                {
                    companyInfo.CompanyId = (long)dbContext.Users.Where(u => !u.IsDeleted && u.UserId == companyInfo.UserId).First().CompanyId;
                    if (companyInfo.CompanyId > 0)
                    {
                        Company existingCompany = dbContext.Companies.Where(c => !c.IsDeleted && c.CompanyId == companyInfo.CompanyId).FirstOrDefault();
                        if (existingCompany != null)
                        {
                            existingCompany.Description = companyInfo.AboutCompany;
                        }
                        dbContext.SaveChanges();
                    }
                    return GetCompanyInfo(companyInfo.UserId);
                }
                return null;
            }
        }

        public UserProfileViewModel GetUserProfileStatus(UserProfileViewModel user)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from u in dbContext.Users
                        join r in dbContext.UserRoles on u.UserId equals r.UserId
                        join c in dbContext.Companies on u.CompanyId equals c.CompanyId
                        where u.UserId == user.UserId && c.Status && u.IsActive
                        select new UserProfileViewModel()
                        {
                            Status = u.IsActive,
                            RoleId = r.RoleId
                        }).Single();
            }
        }

        public List<PlaylistListViewModel> GetUserAssociatedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var result = (from p in dbContext.PlayLists
                              where p.IsLaunched == associatedPlaylist.IsLaunched && p.CreatedBy == associatedPlaylist.UserId && (string.IsNullOrEmpty(associatedPlaylist.SearchText) || p.Name.Contains(associatedPlaylist.SearchText))
                              select new PlaylistListViewModel
                              {
                                  PlaylistId = p.PlayListId,
                                  Name = p.Name,
                                  StartDate = p.StartDate,
                                  EndDate = p.EndDate,
                                  IsExpired = EntityFunctions.TruncateTime(p.EndDate) < EntityFunctions.TruncateTime(DateTime.UtcNow),//p.IsCompleted,
                                  NumberOfUsers = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && !pa.IsAssignedToTeam).ToList().Count(),
                                  NumberOfTeams = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && pa.IsAssignedToTeam).ToList().Count(),
                                  Status = p.IsLaunched,
                                  //  MessageCount = dbContext.Messages.Where(m => m.PlayListId == p.PlayListId && !m.IsMessageRead).Count() ,
                                  Folders = ((from pf in dbContext.PlayListMasters
                                              join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                              where pf.IsFromMasterLibrary && pf.PlayListId == p.PlayListId
                                              select new PlaylistFolderViewModel
                                              {
                                                  FolderId = mf.MasterFolderId,
                                                  FolderName = mf.Name,
                                                  FolderShortName = mf.ShortName,
                                                  IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                  SortOrder = pf.SortOrder,
                                                  NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                              }).Union(from pf in dbContext.PlayListMasters
                                                       join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                       where !pf.IsFromMasterLibrary && pf.PlayListId == p.PlayListId
                                                       select new PlaylistFolderViewModel()
                                                       {
                                                           FolderId = pf.FolderId,
                                                           FolderName = cf.Name,
                                                           FolderShortName = cf.ShortName,
                                                           IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                           SortOrder = pf.SortOrder,
                                                           NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                                       })).OrderBy(j => j.SortOrder).ToList()
                              }).ToList();
                result.ForEach(r => { if (r.StartDate != null && r.EndDate != null) { r.StartDate = r.StartDate.Value.ToLocalTime(); r.EndDate = r.EndDate.Value.ToLocalTime(); } });
                return result;
            }
        }

        public List<PlaylistListViewModel> GetUserAssignedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var playlistIdsForUser = dbContext.PlayListAssignedToes.Where(p => p.UserId == associatedPlaylist.UserId).ToList().Select(y => y.PlayListId).ToList();

           //     List<long> teamIdsAssignedToUser = dbContext.UserTeams.Where(t => t.UserId == associatedPlaylist.UserId).ToList().Select(o => o.TeamId).ToList();
                List<long> publishedTeamIdsAssignedToUser = dbContext.PublishedUserTeams.Where(t => t.UserId == associatedPlaylist.UserId).ToList().Select(o => o.PublishedTeamId).ToList();

                List<long> playlistsAssignedAsTeam = dbContext.PlayListAssignedToes.Where(pl => publishedTeamIdsAssignedToUser.Contains((long)pl.PublishedTeamId)).ToList().Select(m => m.PlayListId).ToList();
                playlistIdsForUser.AddRange(playlistsAssignedAsTeam);
                playlistIdsForUser = playlistIdsForUser.Distinct().ToList();

                List<PlaylistListViewModel> playlistList = new List<PlaylistListViewModel>();
                foreach (var id in playlistIdsForUser)
                {
                    PlaylistListViewModel playlist = (from p in dbContext.PlayLists
                                                      where p.IsLaunched /*&& !p.IsCompleted*/ && p.PlayListId == id && (string.IsNullOrEmpty(associatedPlaylist.SearchText) || p.Name.Contains(associatedPlaylist.SearchText))
                                                      select new PlaylistListViewModel
                                                      {
                                                          PlaylistId = p.PlayListId,
                                                          Name = p.Name,
                                                          StartDate = p.StartDate,
                                                          EndDate = p.EndDate,
                                                          IsExpired = EntityFunctions.TruncateTime(p.EndDate) < EntityFunctions.TruncateTime(DateTime.UtcNow),//p.IsCompleted,
                                                          NumberOfUsers = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == id && !pa.IsAssignedToTeam).ToList().Count(),
                                                          NumberOfTeams = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == id && pa.IsAssignedToTeam).ToList().Count(),
                                                          Status = p.IsLaunched,
                                                          Folders = ((from pf in dbContext.PlayListMasters
                                                                      join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                                                      where pf.IsFromMasterLibrary && pf.PlayListId == id
                                                                      select new PlaylistFolderViewModel
                                                                      {
                                                                          FolderId = mf.MasterFolderId,
                                                                          FolderName = mf.Name,
                                                                          FolderShortName = mf.ShortName,
                                                                          IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                                          SortOrder = pf.SortOrder,
                                                                          NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                                                      }).Union(from pf in dbContext.PlayListMasters
                                                                               join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                                               where !pf.IsFromMasterLibrary && pf.PlayListId == id
                                                                               select new PlaylistFolderViewModel()
                                                                               {
                                                                                   FolderId = pf.FolderId,
                                                                                   FolderName = cf.Name,
                                                                                   FolderShortName = cf.ShortName,
                                                                                   IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                                                   SortOrder = pf.SortOrder,
                                                                                   NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                                                               })).OrderBy(j => j.SortOrder).ToList()
                                                      }).FirstOrDefault();
                    if(playlist != null)
                    {
                        playlistList.Add(playlist);
                    }
                }
                //till we do not have windows service running for IsCompleted Playlist
                //playlistList = playlistList.Where(p => !p.IsExpired).ToList();
                playlistList.ForEach(r => { if (r.StartDate != null && r.EndDate != null) { r.StartDate = r.StartDate.Value.ToLocalTime(); r.EndDate = r.EndDate.Value.ToLocalTime(); } });
                return playlistList;
            }
        }
        public List<AssociatedTeamListViewModel> GetUserAssociatedTeam(long userId, string search)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from t in dbContext.Teams
                        join ut in dbContext.UserTeams on t.TeamId equals ut.TeamId
                        where ut.UserId == userId && (string.IsNullOrEmpty(search) || t.Name.Contains(search))
                        select new AssociatedTeamListViewModel
                        {
                            TeamId = t.TeamId,
                            Name = t.Name,
                            CreatedDate = t.CreatedDate,
                            NumberOfUsers = dbContext.UserTeams.Where(userTeam => userTeam.TeamId == t.TeamId).ToList().Count(),
                            Status = t.Status
                        }).ToList();
            }
        }

        public List<AssociatedUsersListViewModel> GetUserAssociatedUsers(long userId, string search)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from t in dbContext.Teams
                        join ut in dbContext.UserTeams on t.TeamId equals ut.TeamId
                        join u in dbContext.Users on ut.UserId equals u.UserId
                        where !u.IsDeleted && !t.isDeleted && t.TrainerId == userId & (string.IsNullOrEmpty(search) || u.FirstName.Contains(search))
                        select new AssociatedUsersListViewModel
                        {
                            UserId = u.UserId,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            TeamName = t.Name,
                            Status = u.IsActive,
                            AssociatedPlaylist = (from a in dbContext.PlayListAssignedToes
                                                  join p in dbContext.PlayLists on a.PlayListId equals p.PlayListId
                                                  where a.UserId == u.UserId && p.IsLaunched
                                                  select p.Name).ToList()
                        }).ToList();
            }
        }

        public Response CanUserRegister(string emailId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanySubscription companySubscription = (from u in dbContext.UserInvites
                                                           join cs in dbContext.CompanySubscription on u.CompanyId equals cs.CompanyId
                                                           where u.EmailId == emailId && cs.IsActive
                                                           select cs
                       ).FirstOrDefault();

                if(companySubscription == null)
                {
                    return new Response() { Status = true, Message = string.Empty };
                }

                /// Check plan has expired or not
                //if (companySubscription.ExpireDate < DateTime.UtcNow)
                //    return new Response() { Status = false, Message = Constants.SUBSCRIPTION_PLAN_HAS_EXPIRED };

                SubscriptionPlanPrice subscriptionPlan = dbContext.SubscriptionPlanPrice
                    .Where(sp => sp.SubscriptionPlanPriceId == companySubscription.SubscriptionPlanPriceId)
                    .Select(sub => sub).SingleOrDefault();

                Subscription subscription = dbContext.Subscriptions
                    .Where(s => s.SubscriptionId == subscriptionPlan.SubscriptionId)
                    .Select(c => c).SingleOrDefault();

                long registerUserCount = (from u in dbContext.Users
                                          join cs in dbContext.CompanySubscription on u.CompanyId equals cs.CompanyId
                                          join csp in dbContext.SubscriptionPlanPrice on cs.SubscriptionPlanPriceId equals csp.SubscriptionPlanPriceId
                                          where csp.SubscriptionId == subscription.SubscriptionId 
                                                && u.CompanyId == companySubscription.CompanyId 
                                                && cs.IsActive
                                          select u
                       ).Count();
                /// Check subscription count 
                if (registerUserCount >= subscription.NumberOfUsers)
                    return new Response() { Status = false, Message = Constants.SUBSCRIPTION_PLAN_REACHED_LIMIT };

                return new Response() { Status = true, Message = string.Empty };
            }
        }

        public string UpdateCompanyLogo(long userId, string CompanyLogo)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                long companyId = (long)context.Users.Where(u => u.UserId == userId).First().CompanyId;
                string oldPicName = string.Empty;
                Company company = (from c in context.Companies
                             where c.CompanyId == companyId
                             select c).FirstOrDefault();
                if (company != null)
                {
                    oldPicName = company.CompanyLogo;
                    company.CompanyLogo = CompanyLogo;
                    context.SaveChanges();
                }
                return oldPicName;
            }
        }

        public CompanyInfoViewModel GetCompanyInfo(long userId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                long companyId = (long)context.Users.Where(u => u.UserId == userId).First().CompanyId;
                return (from c in context.Companies
                        where c.CompanyId == companyId
                        select new CompanyInfoViewModel
                        {
                            CompanyLogo = c.CompanyLogo,
                            AboutCompany = c.Description,
                            CompanyName = c.Name,
                            Status = c.Status
                        }).FirstOrDefault();
            }
        }

        public string DeleteCompanyLogo(long userId, string CompanyLogo)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                long companyId = (long)context.Users.Where(u => u.UserId == userId).First().CompanyId;
                string oldPicName = string.Empty;
                Company company = (from c in context.Companies
                                   where c.CompanyId == companyId
                                   select c).FirstOrDefault();
                if (company != null)
                {
                    oldPicName = company.CompanyLogo;
                    company.CompanyLogo = string.Empty;
                    context.SaveChanges();
                }
                return oldPicName;
            }
        }

        public string DeleteProfileImage(long userId, string CompanyLogo)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                string oldPicName = string.Empty;
                User user = (from u in context.Users
                             where u.UserId == userId
                             select u).FirstOrDefault();
                if (user != null)
                {
                    oldPicName = user.ProfilePicture;
                    user.ProfilePicture = string.Empty;
                    context.SaveChanges();
                }
                return oldPicName;
            }
        }

        public void SaveLocations(LocationsViewModel locationsViewModel, long userId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                User user = context.Users.FirstOrDefault(x => x.UserId == userId && !x.IsDeleted && x.IsActive);
                if (user is null) throw new Exception("User does not exist.");

                if (locationsViewModel.LocationId == 0)
                {
                    Locations objLocations = new Locations();
                    objLocations.LocationName = locationsViewModel.LocationName;
                    objLocations.StateId = locationsViewModel.StateId;
                    objLocations.CityId = locationsViewModel.CityId;
                    objLocations.AdminId = locationsViewModel.AdminId;
                    objLocations.TrainerId = locationsViewModel.TrainerId;
                    objLocations.CountryId = locationsViewModel.CountryId;
                    objLocations.CompanyId = user.CompanyId;
                    objLocations.CreatedDate = DateTime.Now;
                    context.Locations.Add(objLocations);
                }
                else
                {
                    Locations locations = (from u in context.Locations
                                 where u.LocationId == locationsViewModel.LocationId
                                 select u).FirstOrDefault();
                    if (locations != null)
                    {
                        locations.LocationName = locationsViewModel.LocationName;
                        locations.StateId = locationsViewModel.StateId;
                        locations.CityId = locationsViewModel.CityId;
                        locations.AdminId = locationsViewModel.AdminId;
                        locations.TrainerId = locationsViewModel.TrainerId;
                        locations.CountryId = locationsViewModel.CountryId;
                        locations.ModifiedDate = DateTime.Now;
                    }
                }
                context.SaveChanges();
            }
        }

        public void DeleteLocation(int locationId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Locations objLocation = (from u in dbContext.Locations
                 where u.LocationId == locationId
                 select u).FirstOrDefault();

                if (objLocation != null)
                {
                    dbContext.Locations.Remove(objLocation);
                    dbContext.SaveChanges();
                }
            }
        }

        public bool LocationExists(LocationsViewModel objLocationVM, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var user = dbContext.Users.FirstOrDefault(x => x.UserId == userId && !x.IsDeleted && x.IsActive);
                if (user is null) throw new Exception("User does not exist.");
                Locations objLocation;
                objLocation = (from u in dbContext.Locations
                               where u.LocationName.Trim().ToLower() == objLocationVM.LocationName.Trim().ToLower()
                               && u.CountryId == objLocationVM.CountryId && u.StateId == objLocationVM.StateId && u.CompanyId == user.CompanyId
                               select u).FirstOrDefault();

                if (objLocation != null)
                {
                    return true;
                }
                return false;
            }
        }

        public List<LocationsViewModel> GetSignUpLocations(LocationsSearch location)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<LocationsViewModel> locations = new List<LocationsViewModel>();
                List<Locations> locationsList = new List<Locations>();

                locationsList = context.Locations.Where(m => m.CompanyId == location.CompanyId).ToList();
                locations = Mapper.Map<List<Locations>, List<LocationsViewModel>>(locationsList);

                return locations;
            }
        }

        public List<LocationsViewModel> GetLocations(LocationsSearch location, long userId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<LocationsViewModel> locations = new List<LocationsViewModel>();
                List<Locations> locationsList = new List<Locations>();
                var user = context.Users.FirstOrDefault(x => x.UserId == userId && !x.IsDeleted && x.IsActive);
                if (user is null) throw new Exception("User does not exist.");

                if (!location.StateId.HasValue && !location.CityId.HasValue && !location.CountryId.HasValue)
                {
                    if (!string.IsNullOrEmpty(location.LocationName))
                    {
                        locationsList = context.Locations.Where(c => c.LocationName.StartsWith(location.LocationName) && c.CompanyId == user.CompanyId).Take(Constants.LimitDropDownSize).ToList();
                    }
                    else
                    {
                        locationsList = context.Locations.Where(x => x.CompanyId == user.CompanyId).Take(Constants.LimitDropDownSize).ToList();
                    }
                }
                else
                {
                    locationsList = context.Locations.Where(m => m.CountryId == location.CountryId && m.StateId == location.StateId && m.CityId == location.CityId && m.CompanyId == user.CompanyId).ToList();
                }

                locations = Mapper.Map<List<Locations>, List<LocationsViewModel>>(locationsList);

                foreach (var item in locations)
                {
                    item.CountryName = GetCountries().Where(m => m.ID == item.CountryId).FirstOrDefault()?.Name;
                    item.StateName = GetStates(item.CountryId.Value).Where(m => m.ID == item.StateId).FirstOrDefault()?.Name;
                    item.CityName = GetCities(item.StateId.Value).Where(m => m.ID == item.CityId).FirstOrDefault()?.Name;
                }
                return locations;
            }
        }    
        public List<LocationsViewModel> GetLocationsByCompanyId(long companyId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var isValidCompany = context.Companies.Any(x => x.CompanyId == companyId && !x.IsDeleted && x.Status);
                if (!isValidCompany) throw new Exception("Company is invalid");

                List<LocationsViewModel> locations = new List<LocationsViewModel>();
                List<Locations> locationsList = new List<Locations>();

                locationsList = context.Locations.Where(x => x.CompanyId == companyId).ToList();

                locations = Mapper.Map<List<Locations>, List<LocationsViewModel>>(locationsList);

                foreach (var item in locations)
                {
                    item.CountryName = GetCountries().Where(m => m.ID == item.CountryId).FirstOrDefault()?.Name;
                    item.StateName = GetStates(item.CountryId.Value).Where(m => m.ID == item.StateId).FirstOrDefault()?.Name;
                    item.CityName = GetCities(item.StateId.Value).Where(m => m.ID == item.CityId).FirstOrDefault()?.Name;
                }
                return locations;
            }
        }
    }
}
