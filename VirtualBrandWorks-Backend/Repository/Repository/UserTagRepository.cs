﻿using Repository.Interfaces;
using Repository.Model;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Repository
{
    public class UserTagRepository : IUserTagRepository
    {
        public void Add(UserTag tag)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                dbContext.UserTags.Add(tag);
                dbContext.SaveChanges();
            }
        }

        public List<long?> GetUsersFromTags(List<long> tagIds, long userId)
        {
            List<long?> userIds = new List<long?>();

            if (tagIds != null)
            {
                using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
                {
                    long companyId = (long)dbContext.Users.Where(u => u.UserId == userId).First().CompanyId;
                    foreach (var item in tagIds)
                    {
                        var tagMatchingUserIds = (from ut in dbContext.UserTags
                                                  join u in dbContext.Users on ut.UserId equals u.UserId
                                                  where ut.TagId == item && ut.UserId != null && u.CompanyId == companyId
                                                  select new { ut.UserId }).AsEnumerable().Select(m => m.UserId).ToList();

                        userIds.AddRange(tagMatchingUserIds);

                        userIds.AddRange(GetUsersList(item, companyId));
                    }

                    return userIds.Distinct().ToList();
                }
            }
            else
            {
                return userIds;
            }
        }

        private List<long?> GetUsersList(long item, long companyId)
        {
            List<long?> lstUsers = new List<long?>();
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                // Get the tags for userhandle and teamhandle
                var handleuserList = (from ut in dbContext.Tags
                                      join u in dbContext.Users on ut.UserId equals u.UserId
                                      where ut.TagId == item && (ut.UserId != null || ut.UserId != 0) && u.CompanyId == companyId
                                      select new { ut.UserId }).AsEnumerable().Select(m => m.UserId).ToList();

                lstUsers.AddRange(handleuserList);

                var teamList = (from ut in dbContext.Tags
                                      join u in dbContext.Teams on ut.TeamId equals u.TeamId
                                      where ut.TagId == item && (ut.TeamId != null || ut.TeamId != 0) && u.CompanyId == companyId
                                      select new { ut.TeamId }).AsEnumerable().Select(m => m.TeamId).ToList();

                foreach (var team in teamList)
                {
                    if (team.HasValue)
                    {
                        var usersList = dbContext.UserTeams.Where(m => m.TeamId == team.Value).Select(m => (long?)m.UserId).ToList();
                        lstUsers.AddRange(usersList);
                    }
                }
                return lstUsers;
            }
        }
    }
}
