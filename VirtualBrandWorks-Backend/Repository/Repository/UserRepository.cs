﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Common;
using System.Data.Entity.Validation;
using System.Data.Entity;

namespace Repository.Repository
{
    public class UserRepository : IUserRepository
    {
        public UserContext ValidateUser(string username)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var user = (from u in dbContext.Users
                            join ur in dbContext.UserRoles on u.UserId equals ur.UserId
                            join r in dbContext.Roles on ur.RoleId equals r.RoleId
                            where (u.EmailId.Equals(username) || u.UserHandle.ToLower().Equals(username.ToLower())) && u.IsPaymentCompleted
                            select new UserContext()
                            {
                                UserId = u.UserId,
                                UserName = u.FirstName,
                                UserRole = r.Name,
                                Industry = u.Industry,
                                RoleId = r.RoleId,
                                ProfilePicture = u.ProfilePicture,
                                CompanyLogo = dbContext.Companies.Any(us => u.CompanyId == us.CompanyId) == true ? dbContext.Companies.Where(c => c.CompanyId == u.CompanyId).FirstOrDefault().CompanyLogo : null,
                                IsActive = u.IsActive
                            }).FirstOrDefault();
                //if (user != null)
                //{
                //    if (!user.Password.Equals(password))
                //        return null;
                //}
                return user;
            }
        }

        public UserViewModel ValidateUserWithEmail(string emailId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.FirstOrDefault(u => u.EmailId.Equals(emailId));
                UserViewModel userViewModel = new UserViewModel();
                userViewModel = Mapper.Map<User, UserViewModel>(user);
                return userViewModel;

            }
        }

        public UserContext ValidateExternalLogin(string email)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from u in dbContext.Users
                        join ur in dbContext.UserRoles on u.UserId equals ur.UserId
                        join r in dbContext.Roles on ur.RoleId equals r.RoleId
                        where u.EmailId.Equals(email)
                        select new UserContext()
                        {
                            UserId = u.UserId,
                            UserName = u.FirstName,
                            UserRole = r.Name
                        }).FirstOrDefault();
            }
        }

        public bool SetPasswordResetCode(UserViewModel userViewModel, string resetCode)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.Where(u => u.UserId == userViewModel.UserId).FirstOrDefault();
                if (user != null)
                {
                    user.PasswordResetCode = resetCode;
                    user.ModifiedDate = DateTime.UtcNow;
                    dbContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    dbContext.Configuration.ValidateOnSaveEnabled = false;
                    dbContext.SaveChanges();
                }
                else
                {
                    return false;
                }
                return true;
            }
        }

        public int ResetPassword(ResetPasswordViewModel model)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.FirstOrDefault(u => u.EmailId.Equals(model.Email) && u.PasswordResetCode.Equals(model.ResetCode));
                if (user == null)
                {
                    return Convert.ToInt32(Constants.ResetPasswordResponse.Invalid);
                }
                TimeSpan differenceDuration = DateTime.UtcNow - ((DateTime)user.ModifiedDate);
                //taking a timespan of 10 minutes , to check whether the reset code is valide or expired
                TimeSpan expireIn = new TimeSpan(0, 10, 0); // 10 minutes
                if (differenceDuration <= expireIn)
                {
                    if (user != null)
                    {
                        user.PasswordHash = model.PasswordHash;
                        user.PasswordResetCode = null;
                        dbContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        dbContext.SaveChanges();
                    }
                    return Convert.ToInt32(Constants.ResetPasswordResponse.Success);
                }
                return Convert.ToInt32(Constants.ResetPasswordResponse.Expired);
            }
        }

        public bool ValidatePasswordResetCode(string resetCode)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.FirstOrDefault(u => u.PasswordResetCode.Equals(resetCode));
                if (user != null)
                {
                    return true;
                }
                return false;
            }
        }

        public bool ChangePassword(ChangePasswordViewModel changePassword, int userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.FirstOrDefault(u => u.UserId == userId);
                var saltedHash = SaltedHash.GetHashPasswordWithSalt(changePassword.OldPassword, user.Salt);

                if (user.PasswordHash.Equals(saltedHash))
                {
                    user.Salt = SaltedHash.GetSalt();
                    user.PasswordHash = SaltedHash.GetHashPasswordWithSalt(changePassword.NewPassword.Trim(), user.Salt.Trim());
                    user.ModifiedBy = user.UserId;
                    dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public bool ValidateCompanyStatus(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                bool status = false;
                User user = dbContext.Users.Where(u => u.UserId == userId).FirstOrDefault();
                if (user != null)
                {
                    Company company = dbContext.Companies.Where(c => c.CompanyId == user.CompanyId).FirstOrDefault();
                    if (company != null)
                    {
                        status = company.Status;
                    }
                }
                return status;
            }
        }
        public UserInviteViewModel GetUserInvite(string email)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from u in dbContext.UserInvites
                        join c in dbContext.Companies on u.CompanyId equals c.CompanyId
                        where u.EmailId.Equals(email)
                        select new UserInviteViewModel()
                        {
                            UserInviteId = u.UserInviteId,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            RoleId = u.RoleId,
                            CompanyId = u.CompanyId,
                            EmailId = u.EmailId,
                            CompanyName = c.Name
                        }).FirstOrDefault();

            }
        }

        public void SignUpAsUser(SignUpViewModel signUpUser)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = new User()
                {
                    FirstName = signUpUser.FirstName,
                    LastName = signUpUser.LastName,
                    EmailId = signUpUser.Email,
                    CompanyId = signUpUser.CompanyId,
                    PhoneNumber = signUpUser.PhoneNo,
                    Address = signUpUser.AddressOne,
                    Address2 = signUpUser.AddressTwo,
                    CityId = signUpUser.SelectedCityId,
                    StateId = signUpUser.SelectedStateId,
                    CountryId = signUpUser.SelectedCountryId,
                    Zip = signUpUser.ZipCode,
                    IsPaymentCompleted = true,
                    IsActive = true,
                    Salt = signUpUser.Salt,
                    PasswordHash = signUpUser.PasswordHash,
                    CreatedDate = DateTime.UtcNow,
                    Industry = (from u in dbContext.Users
                                join ur in dbContext.UserRoles on u.UserId equals ur.UserId
                                where !u.IsDeleted && ur.RoleId == 2 && u.CompanyId == signUpUser.CompanyId
                                select u.Industry).SingleOrDefault(),
                    UserHandle = signUpUser.HandleName,
                    LocationId = signUpUser.LocationId
                };
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
                user.CreatedBy = user.UserId;
                UserRole role = new UserRole()
                {
                    RoleId = signUpUser.RoleId,
                    UserId = user.UserId
                };
                dbContext.UserRoles.Add(role);
                dbContext.SaveChanges();
                SaveHandleNameForUser(user);
            }
        }

        private void SaveHandleNameForUser(User user)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                dbContext.Tags.Add(new Tag()
                {
                    Name = user.UserHandle,
                   CreatedBy = user.UserId,
                   ModifiedBy = user.UserId,
                   UserId = user.UserId,
                   CreatedDate = DateTime.Now,
                   ModifiedDate = DateTime.Now
                });
                dbContext.SaveChanges();
            }
        }

        public List<ValidationSummaryViewModel> ValidateUserEmailAndCompanyName(SignUpViewModel signUpViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                List<ValidationSummaryViewModel> ValidationSummary = new List<ValidationSummaryViewModel>();
                User user = dbContext.Users.FirstOrDefault(u => u.EmailId.ToLower().Equals(signUpViewModel.Email.ToLower()) && !u.IsDeleted);
                if (user != null)
                    ValidationSummary.Add(new ValidationSummaryViewModel() { ColumnName = "Email", ColumnValue = Constants.EMAIL_ALREADY_REGISTERED });

                Company company = dbContext.Companies.FirstOrDefault(u => u.Name.ToLower().Equals(signUpViewModel.CompanyName.ToLower())
                && !u.IsDeleted);
                if (company != null)
                    ValidationSummary.Add(new ValidationSummaryViewModel() { ColumnName = "Company", ColumnValue = Constants.Company_ALREADY_REGISTERED });

                return ValidationSummary;
            }
        }

        public Response SaveAdminDetails(SignUpViewModel signUpViewModel)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        ////Delete if any existing details exist with same email ID
                        DeleteSignUpDetails(signUpViewModel.Email);

                        Company company = dbContext.Companies.Add(new Company() { Name = signUpViewModel.CompanyName, IsDeleted = true,
                            StripeSubscriptionId = string.Empty });
                        dbContext.SaveChanges();

                        User user = dbContext.Users.Add(new User()
                        {
                            Address = signUpViewModel.AddressOne.Trim(),
                            Address2 = signUpViewModel.AddressTwo.Trim(),
                            CityId = signUpViewModel.SelectedCityId,
                            CompanyId = company.CompanyId,
                            CountryId = signUpViewModel.SelectedCountryId,
                            CreatedDate = DateTime.UtcNow,
                            EmailId = signUpViewModel.Email.Trim(),
                            FirstName = signUpViewModel.FirstName.Trim(),
                            Industry = signUpViewModel.SelectedCategoryId,
                            IsDeleted = true,
                            IsPaymentCompleted = false,
                            LastName = signUpViewModel.LastName.Trim(),
                            PhoneNumber = signUpViewModel.PhoneNo.Trim(),
                            StateId = signUpViewModel.SelectedStateId,
                            Zip = signUpViewModel.ZipCode.Trim(),
                            PasswordHash = signUpViewModel.PasswordHash,
                            Salt = signUpViewModel.Salt,
                            CreatedBy = 0, //needs to be updated
                            UserHandle = signUpViewModel.HandleName,
                            LocationId = signUpViewModel.LocationId
                        });
                        dbContext.SaveChanges();

                        dbContext.UserRoles.Add(new UserRole() { UserId = user.UserId, RoleId = signUpViewModel.RoleId });

                        dbContext.CompanySubscription.Add(new CompanySubscription()
                        {
                            CompanyId = company.CompanyId,
                            SubscriptionPlanPriceId = signUpViewModel.SubscriptionPlanPriceId,
                            IssueDate = DateTime.Now,
                            ExpireDate = signUpViewModel.SelectedPlanType == (int)Constants.PlanType.Monthly ? DateTime.UtcNow.AddMonths(1) : DateTime.UtcNow.AddYears(1),
                            IsActive = true
                        });
                        dbContext.SaveChanges();
                        transaction.Commit();
                        SaveHandleNameForUser(user);
                        return new Response() { Status = true };
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return new Response() { Status = false };
                    }
                }
            }
        }

        public Response DeleteSignUpDetails(string email)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.Where(obj => obj.EmailId.ToLower().Equals(email) && obj.IsDeleted && !obj.IsPaymentCompleted).FirstOrDefault();

                if (user != null)
                {
                    Company company = dbContext.Companies.Where(obj => obj.CompanyId == user.CompanyId && obj.IsDeleted).FirstOrDefault();

                    if (company != null)
                    {
                        //CompanySubscription cs = dbContext.CompanySubscription.Where(obj => obj.CompanyId == company.CompanyId).FirstOrDefault();
                        //if (cs != null)
                        //    dbContext.CompanySubscription.Remove(cs);

                        dbContext.Companies.Remove(company);
                    }

                    UserRole ur = dbContext.UserRoles.Where(obj => obj.UserId == user.UserId).FirstOrDefault();
                    if (ur != null)
                        dbContext.UserRoles.Remove(ur);

                    dbContext.Users.Remove(user);
                    dbContext.SaveChanges();
                    return new Response() { Status = true };
                }
                else
                    return new Response() { Status = false };
            }
        }

        public Response UpdateSignUpDetailsPaymentSuccess(string email, string customerId, string subscriptionId, bool isTrial)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.Where(obj => obj.EmailId.ToLower().Equals(email) && obj.IsDeleted && !obj.IsPaymentCompleted).FirstOrDefault();
                if (user != null)
                {
                    Company company = dbContext.Companies.Where(obj => obj.CompanyId == user.CompanyId && obj.IsDeleted).FirstOrDefault();
                    if (company != null)
                    {
                        company.IsDeleted = false;
                        company.Status = true;
                        company.StripeSubscriptionId = subscriptionId; ///// stripe subscription id 

                        dbContext.CompanyLibraryFolders.Add(new CompanyLibraryFolder
                        {
                            Name = "Dynamic Folder",
                            ShortName = "dc",
                            CompanyId = company.CompanyId,
                            IsDynamicFolder = true,
                            CreatedBy = user.UserId,
                            CreatedDate = DateTime.UtcNow
                        });
                    }

                    user.IsDeleted = false;
                    user.IsPaymentCompleted = true;
                    user.IsActive = true;

                    if (!isTrial)
                        dbContext.StripeCustomer.Add(new StripeCustomer() { CustomerId = customerId, UserId = user.UserId });

                    dbContext.SaveChanges();
                    return new Response() { Status = true };
                }
                else
                    return new Response() { Status = false };
            }
        }

        public void ChangeSubscriptionPlan(long userId, long planId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long companyId = (long)dbContext.Users.Where(obj => obj.UserId == userId).Select(u => u.CompanyId).Single();
                int planType = dbContext.SubscriptionPlanPrice.Where(p => p.SubscriptionPlanPriceId == planId).Select(o => o.PlanType).Single();

                /// old plan is earlier requested plan, which could not be active for any reason.
                CompanySubscription oldPlan = dbContext.CompanySubscription.Where(obj => obj.CompanyId == companyId && obj.IsActive).
                    Select(o => o).FirstOrDefault();
                if (oldPlan != null)
                {
                    oldPlan.IsActive = false;
                    oldPlan.IsObselete = true;
                }

                ///Adding new requested plan that would be activate after payment success.
                dbContext.CompanySubscription.Add(new CompanySubscription()
                {
                    CompanyId = companyId,
                    SubscriptionPlanPriceId = planId,
                    IssueDate = DateTime.Now,
                    ExpireDate = planType == (int)Constants.PlanType.Monthly ? DateTime.UtcNow.AddMonths(1) : DateTime.UtcNow.AddYears(1),
                    IsActive = true
                });
                dbContext.SaveChanges();
            }
        }

        public void CancelSubscriptionPlan(long userId, long planId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long companyId = (long)dbContext.Users.Where(obj => obj.UserId == userId).Select(u => u.CompanyId).Single();
                CompanySubscription plan = dbContext.CompanySubscription.Where(obj => obj.IsActive && obj.CompanyId == companyId && obj.SubscriptionPlanPriceId == planId).Select(o => o).FirstOrDefault();
                if (plan != null)
                {
                    plan.IsActive = false;
                    dbContext.SaveChanges();
                }
            }
        }

        public string GetCustomerId(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return dbContext.StripeCustomer.Where(obj => obj.UserId == userId).Select(u => u.CustomerId).Single();
            }
        }

        public void SaveUsersLoginStatus(string Email, string statusCode)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            { 
                dbContext.UsersLoginStatus.Add(new UsersLoginStatus()
                {
                    UserName = Email,
                    StatusCode = statusCode,
                    CreatedDate = DateTime.Now,

                }); 
                dbContext.SaveChanges();
            }

        }

        /// <summary>
        /// Check for the login status from database for token validity 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="statusCode">unique string</param>
        /// <returns></returns>
        public bool UserLoginStatus(long userId, string statusCode)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                UsersLoginStatus loginData = (from u in dbContext.Users
                                         join uls in dbContext.UsersLoginStatus on 
                                         u.EmailId equals uls.UserName
                                         where u.UserId == userId && uls.StatusCode == statusCode
                                         select uls).FirstOrDefault();
                if (loginData == null)
                {
                    loginData = (from u in dbContext.Users
                                 join uls in dbContext.UsersLoginStatus on
                                 u.UserHandle equals uls.UserName
                                 where u.UserId == userId && uls.StatusCode == statusCode
                                 select uls).FirstOrDefault();
                }

                if (loginData != null)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Delete the status code from database corresponding to userid for logout
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public bool RemoveLoginStatus(long userId, string statusCode)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                UsersLoginStatus objUserLoginStatus = (from u in dbContext.Users
                                join uls in dbContext.UsersLoginStatus on u.EmailId equals uls.UserName
                                where u.UserId == userId && uls.StatusCode == statusCode
                                select uls).FirstOrDefault();

                    if (objUserLoginStatus != null)
                    {
                        dbContext.UsersLoginStatus.Remove(objUserLoginStatus);
                        dbContext.SaveChanges();
                    }
                
                return true;
            }
        }

        public UserContext GetUserContextFromUserId(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from u in dbContext.Users
                            join ur in dbContext.UserRoles on u.UserId equals ur.UserId
                            join r in dbContext.Roles on ur.RoleId equals r.RoleId
                            where u.UserId == userId && u.IsPaymentCompleted
                            select new UserContext()
                            {
                                UserId = u.UserId,
                                UserName = u.FirstName,
                                UserRole = r.Name,
                                Industry = u.Industry,
                                RoleId = r.RoleId,
                                //ProfilePicture = u.ProfilePicture,
                                //CompanyLogo = dbContext.Companies.Any(us => u.CompanyId == us.CompanyId) == true ? dbContext.Companies.Where(c => c.CompanyId == u.CompanyId).FirstOrDefault().CompanyLogo : null,
                                IsActive = u.IsActive
                            }).FirstOrDefault();
            }
        }

        public string GetEmailIdFromUserId(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from u in dbContext.Users
                        where u.UserId == userId
                        select u.EmailId).FirstOrDefault();
            }
        }

        public long GetCompanyIdFromUserId(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (long)(from u in dbContext.Users
                        where u.UserId == userId
                        select u.CompanyId).FirstOrDefault();
            }
        }

        public string GetSubscriptionFromPlanPriceId(long planId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from sp in dbContext.SubscriptionPlanPrice
                        join s in dbContext.Subscriptions on sp.SubscriptionId equals s.SubscriptionId
                        where sp.SubscriptionPlanPriceId == planId
                        select s.PlanName).FirstOrDefault();
            }
        }

        public UserMyPlanNotiFicationViewModel GetUserNameAndEmail(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from u in dbContext.Users
                        where u.UserId == userId
                        select new UserMyPlanNotiFicationViewModel
                        {
                            UserName = u.FirstName,
                            Email = u.EmailId
                        }).FirstOrDefault();
            }
        }


        public long LimitActiveUsersAsPerPlan(long userId, long planId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long companyId = (long)dbContext.Users.Where(obj => obj.UserId == userId).Select(u => u.CompanyId).Single();
                long usersUpto = (long)(from sp in dbContext.SubscriptionPlanPrice
                                        join s in dbContext.Subscriptions on sp.SubscriptionId equals s.SubscriptionId
                                        where sp.SubscriptionPlanPriceId == planId
                                        select s.NumberOfUsers).FirstOrDefault();
                var companyUsers = dbContext.Users.Where(u => !u.IsDeleted && u.IsActive && u.CompanyId == companyId).ToList().Skip(Convert.ToInt32(usersUpto)).ToList();
                companyUsers.ForEach(cu =>
                {
                    cu.IsActive = false;
                });
                dbContext.SaveChanges();
                return usersUpto;
            }
        }

        public SubscriptionPriceDetailViewModel GetSubscriptionDetailsFromPlanPriceId(long planId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from sp in dbContext.SubscriptionPlanPrice
                        join s in dbContext.Subscriptions on sp.SubscriptionId equals s.SubscriptionId
                        where sp.SubscriptionPlanPriceId == planId
                        select new SubscriptionPriceDetailViewModel
                        {
                            PlanName = s.PlanName,
                            PlanType = sp.PlanType,
                            Price = sp.Price
                        }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Validate users with existing team handle
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public UserViewModel ValidateUserWithHandleName(string handleName)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                User user = dbContext.Users.FirstOrDefault(u => u.UserHandle.ToLower().Equals(handleName.ToLower()));
                UserViewModel userViewModel = new UserViewModel();
                userViewModel = Mapper.Map<User, UserViewModel>(user);
                return userViewModel;
            }
        }
    }
}
