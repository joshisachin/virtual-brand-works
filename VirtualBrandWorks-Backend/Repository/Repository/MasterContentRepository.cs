﻿using AutoMapper;
using Repository.Helpers;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class MasterContentRepository : IMasterContentRepository
    {
        public List<MasterContentViewModel> GetAllContents()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var dynamicFolderId = (context.MasterFolders.Where(x => x.Name == "Dynamic Folder" && x.ShortName == "DC").FirstOrDefault().MasterFolderId);

                List<MasterContentViewModel> masterContents = (from mc in context.MasterContents
                                                               where !mc.IsDeleted
                                                               select new MasterContentViewModel()
                                                               {
                                                                   MasterContentId = mc.MasterContentId,
                                                                   Name = mc.Name,
                                                                   ShortName = mc.ShortName,
                                                                   Description = mc.Description,
                                                                   MasterFolderId = mc.MasterFolderId,
                                                                   IsPublished = mc.IsPublished,
                                                                   IsAddDynamicFolder = (mc.MasterFolderId == dynamicFolderId ? true : false),
                                                                   SelectedCategoryNames = (from cf in context.CategoryFolder
                                                                                            join
                                                 category in context.Categories on cf.CategoryId equals category.CategoryId
                                                                                            where cf.FolderId == mc.MasterFolderId
                                                                                            select category.CategoryName).ToList(),
                                                               }).ToList();
                masterContents.ForEach(obj =>
                {
                    obj.CategoryName = string.Join(", ", obj.SelectedCategoryNames);
                });
                foreach (MasterContentViewModel content in masterContents)
                {
                    MasterFolder folder = context.MasterFolders.Where(f => !f.IsDeleted && f.MasterFolderId == content.MasterFolderId).FirstOrDefault();
                    if (folder != null)
                    {
                        content.FolderName = folder.Name;
                    }
                }
                return masterContents;
            }
        }
        public MasterContentViewModel GetContent(long contentId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                MasterContent content = context.MasterContents.Where(c => !c.IsDeleted && c.MasterContentId == contentId).FirstOrDefault();
                MasterContentViewModel contentViewModel = Mapper.Map<MasterContentViewModel>(content);
                long categoryId = Convert.ToInt64(context.MasterFolders.Where(m => m.MasterFolderId == contentViewModel.MasterFolderId).FirstOrDefault().CategoryId);
                contentViewModel.CategoryId = categoryId;
                // isDynamicFolder = (bool)(m.isDynamicFolder == null ? false : m.isDynamicFolder),
                contentViewModel.IsAddDynamicFolder = context.MasterContents.Where(c => !c.IsDeleted && c.MasterContentId == contentId && c.DynamicFolderContentId != 0).Any();
                var masterContentTags = context.MasterContentTags.Where(x => x.MasterContentId == contentViewModel.MasterContentId);
                if (masterContentTags != null)
                {
                    List<int> tagIds = new List<int>();
                    foreach (MasterContentTag masterContentTag in masterContentTags)
                    {
                        tagIds.Add(Convert.ToInt32(masterContentTag.TagId));
                    }
                    contentViewModel.TagIds = tagIds.ToArray();

                }
                return contentViewModel;
            }
        }
        public void Create(MasterContentViewModel contentViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                MasterContent content = Mapper.Map<MasterContent>(contentViewModel);
                if (content != null)
                {
                    //need to update CreatedBy and Modified By accordingly, for now using 0.
                    content.CreatedBy = 0;
                    content.ModifiedBy = 0;
                    content.CreatedDate = DateTime.UtcNow;
                    content.ModifiedDate = DateTime.UtcNow;
                    if (contentViewModel.IsAddDynamicFolder)
                    {
                        content.MasterFolderId = (context.MasterFolders.Where(x => x.Name == "Dynamic Folder" && x.ShortName == "DC").FirstOrDefault().MasterFolderId);
                        context.MasterContents.Add(content);
                        context.SaveChanges();
                    }
                    content.MasterFolderId = contentViewModel.MasterFolderId;
                    content.DynamicFolderContentId = (contentViewModel.IsAddDynamicFolder == true ? content.MasterContentId : 0);
                    context.MasterContents.Add(content);
                    if (contentViewModel.TagIdStrings != null)
                    {
                        string tagItem = contentViewModel.TagIdStrings[contentViewModel.TagIdStrings.Length - 1];
                        List<string> tags = tagItem.Split(',').ToList();

                        foreach (string tag in tags)
                        {
                            var tagAdd = new Tag() { Name = tag };
                            var tagExists = context.Tags.Where(x => x.Name == tag).ToList();
                            if (tagExists.Count() == 0)
                            {
                                context.Tags.Add(tagAdd);
                                context.SaveChanges();
                                context.MasterContentTags.Add(new MasterContentTag()
                                {
                                    MasterContentId = content.MasterContentId,
                                    TagId = tagAdd.TagId
                                });
                            }
                            else
                            {
                                context.MasterContentTags.Add(new MasterContentTag()
                                {
                                    MasterContentId = content.MasterContentId,
                                    TagId = tagExists[0].TagId
                                });
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
        }
        public void Update(MasterContentViewModel contentViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                MasterContent content = context.MasterContents.Where(c => !c.IsDeleted && c.MasterContentId == contentViewModel.MasterContentId).FirstOrDefault();
                if (content != null)
                {
                    if (contentViewModel.IsFileChanged)
                    {
                        if (content.ContentUrl != contentViewModel.ContentUrl)
                            content.IsPublished = false;
                        content.ContentUrl = contentViewModel.ContentUrl;
                        content.MimeType = contentViewModel.MimeType;
                    }
                    content.Description = contentViewModel.Description;
                    content.MasterFolderId = contentViewModel.MasterFolderId;
                    content.Name = contentViewModel.Name;
                    
                    content.ModifiedDate = DateTime.UtcNow;
                    if (contentViewModel.TagIdStrings != null)
                    {
                        string tagItem = contentViewModel.TagIdStrings[contentViewModel.TagIdStrings.Length - 1];
                        List<string> tags = tagItem.Split(',').ToList();
                        var tagMapped = context.MasterContentTags.Where(x => x.MasterContentId == contentViewModel.MasterContentId).ToList();

                        foreach (string tag in tags)
                        {
                            var tagAdd = new Tag() { Name = tag };
                            if (!tagMapped.Any(x => x.Tag.Name == tag)) //tagExiststsinListNotInDatabase
                            {
                                var tagExists = context.Tags.Where(x => x.Name == tag).ToList();
                                if (tagExists.Count() == 0)
                                {
                                    context.Tags.Add(tagAdd);
                                    context.SaveChanges();
                                    context.MasterContentTags.Add(new MasterContentTag()
                                    {
                                        MasterContentId = content.MasterContentId,
                                        TagId = tagAdd.TagId
                                    });
                                }
                                else
                                {
                                    context.MasterContentTags.Add(new MasterContentTag()
                                    {
                                        MasterContentId = content.MasterContentId,
                                        TagId = tagExists[0].TagId
                                    });
                                }
                            }
                        }
                        var tagExiststsinDatabaseNotInList = tagMapped.Where(a => !tags.Any(b => b.Contains(a.Tag.Name))).ToList(); //tagExiststsinDatabaseNotInList
                        foreach (var tag in tagExiststsinDatabaseNotInList)
                        {
                            var tagAdd = new Tag() { Name = tag.Tag.Name };
                            context.MasterContentTags.Remove(tag);
                        }
                    }
                    var dynamicFolderId = (context.MasterFolders.Where(x => x.Name == "Dynamic Folder" && x.ShortName == "DC").FirstOrDefault().MasterFolderId);
                    MasterContent DynamicContent = context.MasterContents.Where(c => c.MasterFolderId == dynamicFolderId && c.MasterContentId == content.DynamicFolderContentId).FirstOrDefault();
                    MasterContent masterContent = new MasterContent();
                    if (contentViewModel.IsAddDynamicFolder)
                    {
                        if (DynamicContent == null)
                        {
                            masterContent.ContentUrl = content.ContentUrl;
                            masterContent.MimeType = content.MimeType;
                            masterContent.Description = contentViewModel.Description;
                            masterContent.MasterFolderId = contentViewModel.MasterFolderId;
                            masterContent.Name = contentViewModel.Name;
                            masterContent.MasterFolderId = dynamicFolderId;
                            masterContent.CreatedDate = DateTime.UtcNow;
                            masterContent.ModifiedDate = DateTime.UtcNow;
                            context.MasterContents.Add(masterContent);
                            context.SaveChanges();
                            content.DynamicFolderContentId = masterContent.MasterContentId;
                        }
                        else
                        {
                            DynamicContent.ContentUrl = content.ContentUrl;
                            DynamicContent.MimeType = content.MimeType;
                            DynamicContent.Description = contentViewModel.Description;
                            DynamicContent.Name = contentViewModel.Name;
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        if (DynamicContent != null)
                        {
                            content.DynamicFolderContentId = 0;
                            context.MasterContents.Remove(DynamicContent);
                            context.SaveChanges();
                        }
                    }
                    context.SaveChanges();
                }
            }
        }
        public void Delete(long contentId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                MasterContent content = context.MasterContents.Where(s => !s.IsDeleted && s.MasterContentId == contentId).FirstOrDefault();
                if (content != null)
                {
                    if (content.DynamicFolderContentId != 0)
                    {
                        MasterContent dynamicContent = context.MasterContents.Where(c => !c.IsDeleted && c.MasterContentId == content.DynamicFolderContentId).FirstOrDefault();
                        if (dynamicContent != null)
                        {
                            dynamicContent.IsDeleted = true;
                        }
                    }
                    content.IsDeleted = true;
                    context.Entry(content).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        public List<ContentViewModel> GetMasterContentsByFolderId(long masterFolderId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                List<ContentViewModel> contents = (from mc in dbContext.MasterContents
                                                   join mq in dbContext.MasterQuizs on mc.MasterContentId equals mq.MasterContentId
                                                   into d
                                                   from data in d.DefaultIfEmpty()
                                                   where !mc.IsDeleted && mc.MasterFolderId == masterFolderId
                                                   select new ContentViewModel()
                                                   {
                                                       ContentId = mc.MasterContentId,
                                                       ContentName = mc.Name,
                                                       CreatedDate = mc.CreatedDate,
                                                       ModifiedDate = mc.ModifiedDate,
                                                       ShortName = mc.ShortName,
                                                       MimeType = mc.MimeType,
                                                       QuizName = data.QuizName,
                                                       QuizId = data.MasterQuizId,
                                                       IsQuestionAdded = (dbContext.MasterQuestions.Select(obj => obj).Where(o => o.MasterQuizId
                                                         == data.MasterQuizId).FirstOrDefault() == null ? false : true)
                                                   }).ToList();

                return contents;
            }
        }

        public int PublishUnpublishContent(long id)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                MasterContent content = dbContext.MasterContents.Where(mc => !mc.IsDeleted && mc.MasterContentId == id).FirstOrDefault();
                if(content != null)
                {
                    if (content.IsPublished)
                    {
                        content.IsPublished = false;
                        dbContext.SaveChanges();
                        return (int)Constants.PublishedOrUnpublish.Unpublished;
                    }
                    else
                    {
                        content.IsPublished = true;
                        dbContext.SaveChanges();
                        return (int)Constants.PublishedOrUnpublish.Published;
                    }
                }
                return (int)Constants.PublishedOrUnpublish.ContentNotFound;
            }
        }

        public ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long masterFolderId, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                ContentPreviewViewModel contentsFolderPreviewDetails = new ContentPreviewViewModel();

                var result = (from mc in dbContext.MasterContents
                              join mf in dbContext.MasterFolderUserSaved on mc.MasterFolderId equals mf.FolderId
                              into d
                              from data in d.Where(ds => ds.UserId == userId).DefaultIfEmpty()
                              where mc.MasterFolderId == masterFolderId && mc.IsPublished == true && !mc.IsDeleted
                              select new ContentViewModel()
                              {
                                  ContentId = mc.MasterContentId,
                                  ContentName = mc.Name,
                                  CreatedDate = mc.CreatedDate,
                                  ModifiedDate = mc.ModifiedDate,
                                  ShortName = mc.ShortName,
                                  IsSelectedFolder = data == null ? false : true,
                                  FolderName = mc.MasterFolder.Name,
                                  FolderId = mc.MasterFolder.MasterFolderId,
                                  ContentURL = mc.ContentUrl,
                                  IsAwsContent = true,
                                  MimeType = mc.MimeType
                              }).OrderByDescending(c => c.ModifiedDate).ToList();
                if (result.Count() > 0)
                {
                    contentsFolderPreviewDetails.FolderId = result[0].FolderId;
                    contentsFolderPreviewDetails.FolderName = result[0].FolderName;
                    contentsFolderPreviewDetails.NoOfDocs = result.Where(x => x.ContentURL.Contains(".pdf") || x.ContentURL.Contains(".doc") || x.ContentURL.Contains(".docx") || x.ContentURL.Contains(".txt")).ToList().Count();
                    contentsFolderPreviewDetails.NoOfVideo = result.Where(x => x.ContentURL.Contains(".mp4") || x.ContentURL.Contains(".mov") || x.ContentURL.Contains(".3gp")).ToList().Count();
                    contentsFolderPreviewDetails.NoOfImages = result.Where(x => x.ContentURL.Contains(".jpeg") || x.ContentURL.Contains(".jpg") || x.ContentURL.Contains(".gif") || x.ContentURL.Contains(".bmp") || x.ContentURL.Contains(".png")).ToList().Count();
                    //var onlyVideo = result.Where(x => x.ContentURL.Contains(".mp4") || x.ContentURL.Contains(".mov")).ToList();
                    //contentsFolderPreviewDetails.ContentList = onlyVideo.Count() > 3 ? onlyVideo.Take(3).ToList() : onlyVideo;
                    var content = result.FirstOrDefault();
                    if(content != null)
                    {
                        contentsFolderPreviewDetails.IsSelectedFolder = content.IsSelectedFolder;
                    }
                    contentsFolderPreviewDetails.ContentList = result.Take(3).ToList();
                }
                else
                {
                    var folderDetails = (from mc in dbContext.MasterFolders
                                         join mf in dbContext.MasterFolderUserSaved on mc.MasterFolderId equals mf.FolderId
                                         into d
                                         from data in d.Where(ds => ds.UserId == userId).DefaultIfEmpty()
                                         where mc.MasterFolderId == masterFolderId
                                         select new MasterFolderViewModel()
                                         {
                                             Name = mc.Name,
                                             MasterFolderId = mc.MasterFolderId,
                                             IsSelectedFolder = data == null ? false : true,
                                         }).FirstOrDefault();
                    contentsFolderPreviewDetails.FolderId = folderDetails.MasterFolderId;
                    contentsFolderPreviewDetails.FolderName = folderDetails.Name;
                    contentsFolderPreviewDetails.IsSelectedFolder = folderDetails.IsSelectedFolder;
                }
                return contentsFolderPreviewDetails;
            }
        }

        public FolderContentViewModel GetMasterFolderContents(FolderViewModel folder, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from mf in dbContext.MasterFolders
                        where mf.MasterFolderId == folder.FolderId
                        select new FolderContentViewModel()
                        {
                            Folder = new FolderViewModel() { FolderId = mf.MasterFolderId, Name = mf.Name, ShortName = mf.ShortName.ToLower(), Description = string.Empty, NumberOfContent = dbContext.MasterContents.Where(c => !c.IsDeleted && c.MasterFolderId == mf.MasterFolderId && c.IsPublished).ToList().Count(), IsSelectedFolder = dbContext.MasterFolderUserSaved.Any(m => m.FolderId ==mf.MasterFolderId  && m.UserId == userId) },
                            Contents = (from mc in dbContext.MasterContents
                                        where mc.MasterFolderId == folder.FolderId && mc.IsPublished == true && !mc.IsDeleted && (string.IsNullOrEmpty(folder.SearchText) || mc.Name.ToLower().Contains(folder.SearchText) || mc.ShortName.ToLower().Contains(folder.SearchText))
                                        select new ContentViewModel()
                                        {
                                            ContentId = mc.MasterContentId,
                                            ContentName = mc.Name,
                                            CreatedDate = mc.CreatedDate,
                                            ModifiedDate = mc.ModifiedDate,
                                            ShortName = mc.ShortName,
                                            FolderName = mf.Name,
                                            FolderId = mf.MasterFolderId,
                                            ContentURL = mc.ContentUrl,
                                            ContentDescription = mc.Description,
                                            ViewCount = mc.ViewCount,
                                            MimeType = mc.MimeType,
                                            IsAwsContent = true
                                        }).ToList()
                        }).FirstOrDefault();
            }
        }

        public void UpdateMasterContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                dbContext.MasterFolderSortOrder.RemoveRange((from cfs in dbContext.MasterFolderSortOrder
                                                                     join so in SortOders on cfs.FolderId equals so.FolderId
                                                                     where cfs.UserId.Equals(UserId)
                                                                     select cfs).ToList());
                List<MasterFolderSortOrder> masterFolderSortOrder = new List<MasterFolderSortOrder>();
                SortOders.ForEach(o =>
                {
                    masterFolderSortOrder.Add(new MasterFolderSortOrder() { FolderId = o.FolderId, SortOder = o.SortOder, UserId = UserId });
                });
                dbContext.MasterFolderSortOrder.AddRange(masterFolderSortOrder);
                dbContext.SaveChanges();
            }
        }

        public void SaveSelectedMasterContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (SavedFolders.IsAddRequest && !dbContext.MasterFolderUserSaved.Where(obj => obj.FolderId == SavedFolders.FolderId && obj.UserId == UserId).Any())
                    dbContext.MasterFolderUserSaved.Add(new MasterFolderUserSaved() { FolderId = SavedFolders.FolderId, UserId = UserId });
                else
                {
                    MasterFolderUserSaved savedFolder = dbContext.MasterFolderUserSaved.Where(obj => obj.FolderId == SavedFolders.FolderId && obj.UserId == UserId).FirstOrDefault();
                    if (savedFolder != null)
                        dbContext.MasterFolderUserSaved.Remove(savedFolder);
                }
                dbContext.SaveChanges();
            }
        }

        public List<FolderViewModel> GetMasterContentSelectedFolders(long UserId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from cs in dbContext.MasterFolderUserSaved
                        join m in dbContext.MasterFolders on cs.FolderId equals m.MasterFolderId
                        where !m.IsDeleted && cs.UserId == UserId 
                        select new FolderViewModel()
                        {
                            FolderId = m.MasterFolderId,
                            Name = m.Name,
                            ShortName = m.ShortName,
                            IsDynamicFolder = (bool)m.isDynamicFolder
                        }).ToList();
            }
        }

        public FolderContentDetailViewModel GetMasterFolderContentDetail(ContentViewModel requestContent, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var masterFolderContent = (from f in dbContext.MasterFolders
                        join c in dbContext.MasterContents on f.MasterFolderId equals c.MasterFolderId
                        where c.MasterContentId == requestContent.ContentId
                        select new FolderContentDetailViewModel
                        {
                            Folder = new FolderViewModel
                            {
                                FolderId = f.MasterFolderId,
                                Name = f.Name,
                                ShortName = f.ShortName.ToLower(),
                                NumberOfContent = dbContext.MasterContents.Where(con => !con.IsDeleted && con.IsPublished && con.MasterFolderId == c.MasterFolderId).ToList().Count(),
                                IsSelectedFolder = dbContext.MasterFolderUserSaved.Any(m => m.FolderId == f.MasterFolderId && m.UserId == userId)
                            },
                            Content = new ContentViewModel
                            {
                                ContentId = c.MasterContentId,
                                ContentName = c.Name,
                                CreatedDate = c.CreatedDate,
                                ContentDescription = c.Description,
                                ContentURL = c.ContentUrl,
                                ViewCount = c.ViewCount,
                                QuizId = dbContext.MasterQuizs.Where(q => q.MasterContentId == c.MasterContentId).FirstOrDefault() == null ? 0 : dbContext.MasterQuizs.Where(q => q.MasterContentId == c.MasterContentId).FirstOrDefault().MasterQuizId,
                                MimeType = c.MimeType,
                                IsAwsContent = true
                            }
                        }).FirstOrDefault();

                // increase the viewCount in db
                if (masterFolderContent != null && masterFolderContent.Content != null && !string.IsNullOrEmpty(requestContent.IPAddress))
                {
                    bool isViewAlreadyConsidered = dbContext.UserContentViews.Any(uc => uc.UserId == userId && uc.IPAddress == requestContent.IPAddress && uc.ContentId == masterFolderContent.Content.ContentId && uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                    if (!isViewAlreadyConsidered)
                    {
                        int viewCountForSameUserWithDifferentIP = dbContext.UserContentViews.Where(uc => uc.UserId == userId && uc.ContentId == masterFolderContent.Content.ContentId && uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                        if (viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                        {
                            dbContext.UserContentViews.Add(new UserContentViews()
                            {
                                UserId = userId,
                                IPAddress = requestContent.IPAddress,
                                ContentId = masterFolderContent.Content.ContentId,
                                IsMasterContent = true,
                                CreatedDate = DateTime.UtcNow
                            });

                            MasterContent existingContent = dbContext.MasterContents.Where(mc => mc.MasterContentId == masterFolderContent.Content.ContentId).FirstOrDefault();
                            if (existingContent != null)
                            {
                                existingContent.ViewCount = existingContent.ViewCount + 1;
                            }
                            dbContext.SaveChanges();
                        }
                    }
                }
                return masterFolderContent;
            }
        }

        public List<ContentViewModel> GetMasterContentsByType(FolderContentRequestViewModel folder)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from mc in dbContext.MasterContents
                        join mf in dbContext.MasterFolders on mc.MasterFolderId equals mf.MasterFolderId
                        where mc.MasterFolderId == folder.FolderId && mc.IsPublished == true && !mc.IsDeleted && mc.MasterContentId != folder.ContentId && (string.IsNullOrEmpty(folder.SearchText) || mc.Name.ToLower().Contains(folder.SearchText) || mc.ShortName.ToLower().Contains(folder.SearchText))
                        //let fileType = FileTypeCollection.GetFileTypes(folder.ContentType)
                         //where Array.IndexOf(fileType, mc.ContentUrl.Split('.').LastOrDefault()) > 0
                        select new ContentViewModel()
                        {
                            ContentId = mc.MasterContentId,
                            ContentName = mc.Name,
                            FolderId = (long)mc.MasterFolderId,
                            FolderName = mf.Name,
                            CreatedDate = mc.CreatedDate,
                            ModifiedDate = mc.ModifiedDate,
                            ShortName = mc.ShortName,
                            ContentURL = mc.ContentUrl,
                            ContentDescription = mc.Description,
                            ViewCount = mc.ViewCount,
                            MimeType = mc.MimeType,
                            IsAwsContent = true
                        }).ToList();

            }
        }
    }
}
