﻿using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class PrizeStoreRepository : IPrizeStoreRepository
    {
        public PrizeCreateHelperViewModel GetPrizeCategories()
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PrizeCreateHelperViewModel categories = new PrizeCreateHelperViewModel();
                categories.PrizeCategories = (from pc in dbContext.PrizeCategories
                        select new PrizeCategoryViewModel
                        {
                            PrizeCategoryId = pc.PrizeCategoryId,
                            Name = pc.Name
                        }).ToList();
                return categories;
            }
        }
        public void CreatePrize(PrizeStoreViewModel prize, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        Prize newPrize = dbContext.Prizes.Add(new Prize
                        {
                            PrizeCategoryId = prize.PrizeCategoryId,
                            Title = prize.Title,
                            Description = prize.Description,
                            StartDate = prize.StartDate,
                            EndDate = prize.EndDate.Value.AddDays(1).AddSeconds(-1),
                            Points = prize.Points,
                            ThumbnailImage = prize.ThumbnailImage,
                            Quantity = prize.Quantity,
                            TimeOffDuration = prize.TimeOffDuration,
                            BrandName = prize.BrandName,
                            PrizeAmount = prize.PrizeAmount,
                            CreatedBy = userId,
                            CreatedDate = DateTime.UtcNow,
                            IsActive = false,
                            Remarks = prize.Remarks
                        });
                        dbContext.SaveChanges();

                        if (newPrize.PrizeCategoryId == (int)Constants.PrizeCategory.Voucher || newPrize.PrizeCategoryId == (int)Constants.PrizeCategory.GiftCard)
                        {
                            prize.VoucherCodes.ForEach(vc =>
                            {
                                dbContext.PrizeRedeemCodes.Add(new PrizeRedeemCode
                                {
                                    PrizeId = newPrize.PrizeId,
                                    PrizeCategoryId = newPrize.PrizeCategoryId,
                                    RedeemCode = vc,
                                    IsValid = true
                                });
                                dbContext.SaveChanges();
                            });
                        }
                        else if (newPrize.PrizeCategoryId == (int)Constants.PrizeCategory.TimeOffPrize || newPrize.PrizeCategoryId == (int)Constants.PrizeCategory.InHouseFoodCertification || newPrize.PrizeCategoryId == (int)Constants.PrizeCategory.CashPrize)
                        {
                            //Generate unique code for each quantity
                            for (int i = 0; i < newPrize.Quantity; i++)
                            {
                                string newRedeemCode = RedeemCodeGenerator.GenerateRedeemCode();
                                dbContext.PrizeRedeemCodes.Add(new PrizeRedeemCode
                                {
                                    PrizeId = newPrize.PrizeId,
                                    PrizeCategoryId = newPrize.PrizeCategoryId,
                                    RedeemCode = newRedeemCode,
                                    IsValid = true
                                });
                                dbContext.SaveChanges();
                            }
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public List<PrizeListViewModel> GetPrizeStore(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long companyId = (long)dbContext.Users.Where(u => u.UserId == userId).First().CompanyId;
                DateTime dt = DateTime.UtcNow;
                return (from p in dbContext.Prizes
                        join u in dbContext.Users on p.CreatedBy equals u.UserId
                        where !p.IsDeleted && p.IsActive && u.CompanyId == companyId && p.StartDate <= dt && p.EndDate >= dt
                        select new PrizeListViewModel
                        {
                            PrizeId = p.PrizeId,
                            Title = p.Title,
                            Description = p.Description,
                            Points = p.Points,
                            ThumbnailImage = p.ThumbnailImage,
                            StartDate = p.StartDate,
                            EndDate = p.EndDate,
                            IsActive = p.IsActive,
                            AvailableCount = dbContext.PrizeRedeemCodes.Where(prc => prc.PrizeId == p.PrizeId && prc.IsValid).Count()
                        }).ToList();
            }
        }

        public List<PrizeListViewModel> GetMyCreatedPrize(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from p in dbContext.Prizes
                        where !p.IsDeleted && p.CreatedBy == userId
                        select new PrizeListViewModel
                        {
                            PrizeId = p.PrizeId,
                            PrizeCategoryId = p.PrizeCategoryId,
                            Title = p.Title,
                            Description = p.Description,
                            Points = p.Points,
                            ThumbnailImage = p.ThumbnailImage,
                            StartDate = p.StartDate,
                            EndDate = p.EndDate,
                            IsActive = p.IsActive,
                            AvailableCount = dbContext.PrizeRedeemCodes.Where(prc => prc.PrizeId == p.PrizeId && prc.IsValid).Count(),
                            Remarks = p.Remarks
                        }).ToList();
            }
        }

        public PrizeStoreViewModel GetPrizeInfo(long prizeId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from p in dbContext.Prizes
                        where !p.IsDeleted && p.PrizeId == prizeId
                        select new PrizeStoreViewModel
                        {
                            PrizeId = p.PrizeId,
                            PrizeCategoryId = p.PrizeCategoryId,
                            Title = p.Title,
                            Description = p.Description,
                            Points = p.Points,
                            ThumbnailImage = p.ThumbnailImage,
                            StartDate = p.StartDate,                            
                            EndDate = p.EndDate,
                       //     Quantity = p.Quantity,
                      //      TimeOffDuration = p.TimeOffDuration,
                       //     BrandName = p.BrandName,
                       //     PrizeAmount = p.PrizeAmount,
                            IsActive = p.IsActive,
                            Remarks = p.Remarks,
                         //   VoucherCodes = dbContext.PrizeRedeemCodes.Where(prc => prc.PrizeId == p.PrizeId).Select(c => c.RedeemCode).ToList()
                        }).FirstOrDefault();
            }
        }

        public string UpdatePrize(PrizeStoreViewModel prize)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Prize existingPrize = dbContext.Prizes.Where(p => p.PrizeId == prize.PrizeId).FirstOrDefault();
                string oldPrizeThumbnail = null;
                if (existingPrize != null)
                {
                    existingPrize.Title = prize.Title;
                    existingPrize.Description = prize.Description;
                    existingPrize.StartDate = prize.StartDate;
                    existingPrize.EndDate = prize.EndDate;
                    existingPrize.Points = prize.Points; 
                    existingPrize.Remarks = prize.Remarks;
                    if(!string.IsNullOrEmpty(prize.ThumbnailImage))
                    {
                        oldPrizeThumbnail = existingPrize.ThumbnailImage;
                        existingPrize.ThumbnailImage = prize.ThumbnailImage;
                    }
                    dbContext.SaveChanges();
                }
                return oldPrizeThumbnail;
            }
        }

        //public string GetOldThumbnailForPrize(long prizeId)
        //{
        //    using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
        //    {
        //        Prize existingPrize = dbContext.Prizes.Where(p => p.PrizeId == prizeId).FirstOrDefault();
        //        string oldPrizeThumbnail = string.Empty;
        //        if (existingPrize != null)
        //        {
        //            oldPrizeThumbnail = existingPrize.ThumbnailImage;
        //        }
        //        return oldPrizeThumbnail;
        //    }
        //}

        public bool UpdatePrizeStatus(PrizeStatusViewModel prize)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Prize existingPrize = dbContext.Prizes.Where(p => p.PrizeId == prize.PrizeId).FirstOrDefault();
                existingPrize.IsActive = prize.IsActive;
                dbContext.SaveChanges();
                return existingPrize.IsActive;
            }
        }

        public AvailPrizeResponseViewModel AvailPrize(long prizeId, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        Prize existingPrize = dbContext.Prizes.Where(p => p.PrizeId == prizeId).FirstOrDefault();
                        if (existingPrize != null)
                        {
                            EarnedPoint earnedPoint = dbContext.EarnedPoints.Where(ep => ep.UserId == userId).FirstOrDefault();
                            if (earnedPoint != null)
                            {
                                if (existingPrize.Points > earnedPoint.Points)
                                {
                                    transaction.Commit();
                                    return new AvailPrizeResponseViewModel { StatusType = (int)Constants.AvailPrizeStatus.InsufficientBalance };
                                }
                                else
                                {
                                    var redeemCode = dbContext.PrizeRedeemCodes.Where(pr => pr.PrizeId == prizeId && pr.IsValid).FirstOrDefault();
                                    if (redeemCode != null)
                                    {
                                        dbContext.AvailPrizes.Add(new AvailPrize
                                        {
                                            UserId = userId,
                                            PrizeRedeemCodeId = redeemCode.PrizeRedeemCodeId
                                        });
                                        redeemCode.IsValid = false;
                                        dbContext.SaveChanges();

                                        dbContext.PointsTransactions.Add(new PointsTransaction
                                        {
                                            UserId = userId,
                                            Description = existingPrize.Title,
                                            Points = existingPrize.Points,
                                            CreatedDate = DateTime.UtcNow,
                                            Balance = earnedPoint.Points - existingPrize.Points,
                                            IsEarned = false,
                                            Type = Constants.PointsEarnedType.Redeemed.ToString()
                                        });
                                        dbContext.SaveChanges();
                                        earnedPoint.Points = earnedPoint.Points - existingPrize.Points;
                                        dbContext.SaveChanges();

                                        //check if all the RedeemCodes are used , mark the prize as inactive
                                        int validRedeemCodeCount = dbContext.PrizeRedeemCodes.Where(pr => pr.PrizeId == prizeId && pr.IsValid).Count();
                                        if (validRedeemCodeCount == 0)
                                        {
                                            existingPrize.IsActive = false;
                                            dbContext.SaveChanges();
                                        }
                                        transaction.Commit();
                                        AvailPrizeResponseViewModel availedPrize = new AvailPrizeResponseViewModel
                                        { StatusType = (int)Constants.AvailPrizeStatus.SuccessfullyAvailed,
                                          PrizeCategoryId = existingPrize.PrizeCategoryId,
                                          AvailedRedeemCode = redeemCode.RedeemCode,
                                          TimeOffDuration = existingPrize.TimeOffDuration,
                                          CashPrizeAmount = existingPrize.PrizeAmount,
                                          BrandName = existingPrize.BrandName,
                                          Remarks = existingPrize.Remarks,
                                          User = (from u in dbContext.Users
                                                  where u.UserId == userId
                                                  select new UserViewModel
                                                  {
                                                      FirstName = u.FirstName,
                                                      LastName = u.LastName,
                                                      EmailId = u.EmailId
                                                  }).FirstOrDefault()
                                        };
                                        return availedPrize;
                                    }
                                    else
                                    {
                                        transaction.Commit();
                                        return new AvailPrizeResponseViewModel { StatusType = (int)Constants.AvailPrizeStatus.NoCodeAvailable };
                                    }
                                }
                            }
                            else
                            {
                                transaction.Commit();
                                return new AvailPrizeResponseViewModel { StatusType = (int)Constants.AvailPrizeStatus.InsufficientBalance };
                            }
                        }
                        transaction.Commit();
                        return null;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }

            }
        }

        public bool DeletePrize(long prizeId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                Prize existingPrize = dbContext.Prizes.Where(p => p.PrizeId == prizeId).FirstOrDefault();
                if (existingPrize != null && (existingPrize.StartDate > DateTime.UtcNow || existingPrize.EndDate < DateTime.UtcNow || !existingPrize.IsActive))
                {
                    existingPrize.IsDeleted = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
        }

    }
}
