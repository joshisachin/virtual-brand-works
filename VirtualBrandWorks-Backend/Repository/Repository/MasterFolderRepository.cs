﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;
using System;
using System.Web;
using Repository.Helpers;
using VirtualBrandWork.Entity;

namespace Repository.Repository
{
    public class MasterFolderRepository : IMasterFolderRepository
    {
        public List<MasterFolderViewModel> GetAllFolders()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var list = (from m in context.MasterFolders
                            join c in context.Categories on m.CategoryId equals c.CategoryId
                            where !m.IsDeleted
                            select new MasterFolderViewModel()
                            {
                                MasterFolderId = m.MasterFolderId,
                                Name = m.Name,
                                ShortName = m.ShortName,
                                CategoryId = m.CategoryId,
                                isDynamicFolder = (bool)(m.isDynamicFolder == null ? false : m.isDynamicFolder),
                                SelectedCategoryNames = (from cf in context.CategoryFolder
                                                         join
              category in context.Categories on cf.CategoryId equals category.CategoryId
                                                         where cf.FolderId == m.MasterFolderId
                                                         select category.CategoryName).ToList(),
                                NumberOfContent = context.MasterContents.Where(c => !c.IsDeleted && c.MasterFolderId == m.MasterFolderId).ToList().Count()
                            }).ToList();
                list.ForEach(obj =>
              {
                  obj.CategoryName = string.Join(", ", obj.SelectedCategoryNames);
              });

                return list;
            }
        }
        public List<FolderViewModel> GetFilterFolders(CompanyContentFolderRequestViewModel request)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {

                return (from m in context.MasterFolders
                        join cf in context.CategoryFolder on m.MasterFolderId equals cf.FolderId
                        join mc in context.Categories on cf.CategoryId equals mc.CategoryId
                        join u in context.Users on request.UserId equals u.UserId
                        join sf in context.MasterFolderUserSaved on m.MasterFolderId equals sf.FolderId into sg
                        from selectFolder in sg.Where(s => s.UserId == request.UserId).DefaultIfEmpty()
                        where (!m.IsDeleted && cf.CategoryId == u.Industry && !mc.IsDeleted && (string.IsNullOrEmpty(request.SearchText) || m.Name.Contains(request.SearchText) || m.ShortName.Contains(request.SearchText)))
                        select new FolderViewModel()
                        {
                            FolderId = m.MasterFolderId,
                            Name = m.Name,
                            ShortName = m.ShortName.ToLower(),
                            IsDynamicFolder = false,
                            CategoryId = m.CategoryId,
                            IsSelectedFolder = (selectFolder != null && selectFolder.UserId == request.UserId) ? true : false,
                            NumberOfContent = context.MasterContents.Where(c => !c.IsDeleted && c.MasterFolderId == m.MasterFolderId && c.IsPublished).ToList().Count()
                        }).Distinct().OrderByDescending(o => o.FolderId)
                                            .Skip(request.PageSize * (request.PageIndex - 1))
                                                                     .Take(request.PageSize).ToList()
                                            .Union((from mf in context.MasterFolders
                                            join sf in context.MasterFolderUserSaved on mf.MasterFolderId equals sf.FolderId into sg
                                            from selectFolder in sg.DefaultIfEmpty()
                                            where mf.isDynamicFolder == true
                                            select new FolderViewModel()
                                            {
                                                FolderId = mf.MasterFolderId,
                                                Name = mf.Name,
                                                ShortName = mf.ShortName.ToLower(),
                                                IsDynamicFolder = true,
                                                CategoryId = 0,
                                                IsSelectedFolder = (context.MasterFolderUserSaved.Where(obj => obj.UserId == request.UserId && obj.FolderId == mf.MasterFolderId).Any()),
                                                NumberOfContent = context.MasterContents.Where(c => !c.IsDeleted && c.MasterFolderId == mf.MasterFolderId && c.IsPublished).ToList().Count()
                                            }).Distinct()).ToList();
            }
        }

        public Response GetFoldersByCategoyId(long categoryId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<MasterFolderViewModel> folderViewModel = (from m in context.MasterFolders
                                                               join
                               cf in context.CategoryFolder on m.MasterFolderId equals cf.FolderId
                                                               where !m.IsDeleted && (bool)!m.isDynamicFolder && cf.CategoryId == categoryId
                                                               select new MasterFolderViewModel
                                                               {
                                                                   MasterFolderId = m.MasterFolderId,
                                                                   Name = m.Name
                                                               }).ToList();
                return new Response() { Data = folderViewModel, Status = true };
            }
        }

        public Response Create(MasterFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                MasterFolder folder = Mapper.Map<MasterFolder>(folderViewModel);

                //needs to be updated for now using 0.
                folder.CreatedBy = 0;
                folder.ModifiedBy = 0;
                folder.CreatedDate = DateTime.UtcNow;
                folder = context.MasterFolders.Add(folder);
                context.SaveChanges();
                if (folderViewModel.SelectedCategoryIds != null)
                {
                    List<long> cIds = folderViewModel.SelectedCategoryIds.Split(',').ToList().ConvertAll(long.Parse);
                    cIds.ForEach(id =>
                    {
                        context.CategoryFolder.Add(new CategoryFolder() { CategoryId = id, FolderId = folder.MasterFolderId });
                    });
                }
                context.SaveChanges();
                return new Response() { Status = true };
            }
        }

        public void Update(MasterFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                MasterFolder masterFolder = context.MasterFolders.Where(c => c.MasterFolderId == folderViewModel.MasterFolderId).FirstOrDefault();

                context.CategoryFolder.RemoveRange(context.CategoryFolder.Where(obj => obj.FolderId == folderViewModel.MasterFolderId).ToList());

                List<long> cIds = folderViewModel.SelectedCategoryIds.Split(',').ToList().ConvertAll(long.Parse);
                cIds.ForEach(id =>
                {
                    context.CategoryFolder.Add(new CategoryFolder() { CategoryId = id, FolderId = folderViewModel.MasterFolderId });
                });

                if (masterFolder != null)
                {
                    masterFolder.CategoryId = folderViewModel.CategoryId;
                    masterFolder.Name = folderViewModel.Name;
                    masterFolder.ShortName = folderViewModel.ShortName;
                    //needs to be updated for now using 0.
                    masterFolder.CreatedBy = 0;
                    masterFolder.ModifiedBy = 0;
                    masterFolder.ModifiedDate = DateTime.UtcNow;
                    context.SaveChanges();
                }
            }
        }
        public MasterFolderViewModel GetFolder(long folderId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var folder = (from mf in context.MasterFolders
                              where !mf.IsDeleted && mf.MasterFolderId == folderId
                              select new MasterFolderViewModel()
                              {
                                  MasterFolderId = mf.MasterFolderId,
                                  Name = mf.Name,
                                  ShortName = mf.ShortName,
                                  CategoryId = mf.CategoryId
                              }).FirstOrDefault();

                if (folder != null)
                {
                    var ids = context.CategoryFolder.Where(obj => obj.FolderId == folder.MasterFolderId).Select(c => c.CategoryId).ToList();
                    folder.SelectedCategoryIds = string.Join(",", ids);
                }
                return folder;
            }
        }
        public Response Delete(long folderId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                MasterFolder folder = context.MasterFolders.Find(folderId);
                if (folder != null && !(context.MasterContents.Where(obj => obj.MasterFolderId == folderId).Any()))
                {
                    folder.IsDeleted = true;
                    var contents = context.MasterContents.Where(c => !c.IsDeleted && c.MasterFolderId == folderId).ToList();
                    contents.ForEach(c => c.IsDeleted = true ); // deleting content which are related to the folder
                    context.Entry(folder).State = EntityState.Modified;
                    context.SaveChanges();
                    return new Response() { Status = true };
                }
                else return new Response() { Status = false, Message = Constants.FOLDER_CANNOT_BE_DELETED };
            }
        }
        public bool IsFolderExists(MasterFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<long> categoryIds = folderViewModel.SelectedCategoryIds.Split(',').ToList().ConvertAll(long.Parse);

                if (folderViewModel.MasterFolderId > 0)
                {
                    return (from mf in context.MasterFolders
                            join ec in context.CategoryFolder on mf.MasterFolderId equals ec.FolderId
                            join id in categoryIds on ec.CategoryId equals id
                            where mf.Name.ToLower() == folderViewModel.Name.ToLower() && ec.FolderId != folderViewModel.MasterFolderId && !mf.IsDeleted
                            select mf).Any();
                }

                return (from mf in context.MasterFolders
                        join cf in context.CategoryFolder on mf.MasterFolderId equals cf.FolderId
                        join id in categoryIds on cf.CategoryId equals id
                        where mf.Name.ToLower() == folderViewModel.Name.ToLower() && !mf.IsDeleted
                        select mf).Any();

            }
        }

        public bool IsFolderShortNameExists(MasterFolderViewModel folderViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<long> categoryIds = folderViewModel.SelectedCategoryIds.Split(',').ToList().ConvertAll(long.Parse);
                if (folderViewModel.MasterFolderId > 0)
                {
                    return (from mf in context.MasterFolders
                            join ec in context.CategoryFolder on mf.MasterFolderId equals ec.FolderId
                            join id in categoryIds on ec.CategoryId equals id
                            where mf.ShortName.ToLower() == folderViewModel.ShortName.ToLower() && ec.FolderId != folderViewModel.MasterFolderId && !mf.IsDeleted
                            select mf).Any();
                }
                return (from mf in context.MasterFolders
                        join cf in context.CategoryFolder on mf.MasterFolderId equals cf.FolderId
                        join id in categoryIds on cf.CategoryId equals id
                        where mf.ShortName.ToLower() == folderViewModel.ShortName.ToLower() && !mf.IsDeleted
                        select mf).Any();
            }
        }
    }
}
