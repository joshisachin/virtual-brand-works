﻿using Repository.Helpers;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class TeamRepository : ITeamRepository
    {
        public List<TeamListViewModel> GetTeamList(TeamListViewModel request)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<TeamListViewModel> teams = null;
                List<Team> teamList = null;

                if (request.IsTaggedContent)
                {
                    // Get the list of team from UserTeam

                    teams = (from ut in context.UserTeams
                             join t in context.Teams
                             on ut.TeamId equals t.TeamId
                             where !t.isDeleted && ut.UserId == request.UserId
                             select new TeamListViewModel
                             {
                                 CompanyId = t.CompanyId,
                                 IsCompanyTeam = request.IsCompanyTeam,
                                 IsDeleted = t.isDeleted,
                                 Name = t.Name,
                                 RoleId = request.RoleId,
                                 TeamId = t.TeamId,
                                 TrainerId = t.TrainerId,
                                 UserId = request.UserId
                             }).ToList();
                    
                    return teams;
                }

                request.CompanyId = (long)context.Users.Where(u => !u.IsDeleted && u.UserId == request.UserId).First().CompanyId;// get companyid for current user
                request.RoleId = context.UserRoles.Where(ur => ur.UserId == request.UserId).First().RoleId; // get roleid from db

                if (request.RoleId == Convert.ToInt32(Constants.UserRoles.Admin))
                {
                    teamList = context.Teams.Where(t => !t.isDeleted && t.CompanyId == request.CompanyId
                   && ((request.IsCompanyTeam && t.TrainerId == request.UserId) || ((!request.IsCompanyTeam && request.TrainerId == 0 && t.TrainerId != request.UserId) || t.TrainerId == request.TrainerId))).ToList();
                }
                else if (request.RoleId == Convert.ToInt32(Constants.UserRoles.Trainer))
                {
                    teamList = context.Teams.Where(t => !t.isDeleted && t.CompanyId == request.CompanyId && t.TrainerId == request.UserId).ToList();
                }
                teams = AutoMapper.Mapper.Map<List<Team>, List<TeamListViewModel>>(teamList);

                return teams;
            }
        }

        public TeamDetailViewModel GetTeamDetail(TeamDetailViewModel team)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                bool isTeamExist = context.Teams.Any(t => !t.isDeleted && t.TeamId == team.TeamId);
                if (isTeamExist)
                {
                    team.CompanyId = context.Teams.Where(t => !t.isDeleted && t.TeamId == team.TeamId).First().CompanyId;
                    TeamDetailViewModel teamDetail = (from t in context.Teams
                                                      join user in context.Users on t.TrainerId equals user.UserId
                                                      //join c in context.Cities on t.CityId equals c.ID
                                                      join loc in context.Locations on t.CityId equals loc.LocationId
                                                      where !t.isDeleted && t.TeamId == team.TeamId && t.CompanyId == team.CompanyId
                                                      select new TeamDetailViewModel
                                                      {
                                                          TeamId = t.TeamId,
                                                          TeamName = t.Name,
                                                          TrainerFirstName = user.FirstName,
                                                          TrainerLastName = user.LastName,
                                                          Status = t.Status,
                                                          NumberOfUsers = context.UserTeams.Where(ut => ut.TeamId == t.TeamId).ToList().Count(),
                                                          LocationId = loc.LocationId,
                                                          LocationName = loc.LocationName,
                                                          AssociatedPlayList = (from a in context.PlayListAssignedToes
                                                                                join p in context.PlayLists on a.PlayListId equals p.PlayListId
                                                                                where a.TeamId == t.TeamId & p.IsLaunched
                                                                                select p.Name).ToList(),
                                                          Users = (from u in context.Users
                                                                   join ut in context.UserTeams on u.UserId equals ut.UserId
                                                                   join tm in context.Teams on ut.TeamId equals tm.TeamId
                                                                   //join city in context.Cities on u.CityId equals city.ID
                                                                   join location in context.Locations on u.LocationId equals location.LocationId
                                                                   where !u.IsDeleted && u.IsActive && tm.TeamId == team.TeamId
                                                                   select new UserDetailForTeamViewModel
                                                                   {
                                                                       UserId = u.UserId,
                                                                       FirstName = u.FirstName,
                                                                       LastName = u.LastName,
                                                                       ProfilePicture = u.ProfilePicture,
                                                                       Status = u.IsActive,
                                                                       Email = u.EmailId,
                                                                       ContactNo = u.PhoneNumber,
                                                                       Location = location.LocationName,
                                                                       HandleName = u.UserHandle
                                                                   }).ToList(),
                                                           Tags = (from t in context.TeamTags
                                                                             join ta in context.Tags on t.TagId equals ta.TagId
                                                                             where !ta.IsDeleted && t.TeamId == team.TeamId
                                                                             select new TagViewModel
                                                                             {
                                                                                 TagId = ta.TagId,
                                                                                 Name = ta.Name,
                                                                                 ShortName = ta.ShortName
                                                                             }).Distinct().ToList(),
                                                           HandleName = t.TeamHandle
                                                      }).FirstOrDefault();


                    teamDetail.Users.ForEach(u =>
                    {
                        var playlistIds = (from a in context.PlayListAssignedToes
                                           join p in context.PlayLists on a.PlayListId equals p.PlayListId
                                           join ut in context.PublishedUserTeams on a.PublishedTeamId equals ut.PublishedTeamId
                                           where a.TeamId == team.TeamId & p.IsLaunched && ut.UserId == u.UserId
                                           select p.PlayListId).ToList();

                        var quizResults = context.UserQuizResults.Where(uq => uq.UserId == u.UserId && playlistIds.Contains(uq.PlaylistId)).ToList();
                        if(quizResults != null && quizResults.Count() > 0)
                        {
                            u.Score = quizResults.Select(qr => qr.MarksObtained).Sum();
                        }
                        if(playlistIds.Count() > 0)
                        {
                            u.CourseCompleted = (int)(((double)(context.UserPlayListStatus.Where(ups => ups.UserId == u.UserId && playlistIds.Contains(ups.PlaylistId) && ups.IsComplete).Count()) / (double)playlistIds.Count()) * 100);
                        }                        
                    });
                    return teamDetail;
                }
                return null;
            }
        }


        public List<TrainerListViewModel> GetTrainers(long UserId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                int roleId = Convert.ToInt32(Constants.UserRoles.Trainer);
                long companyId = (long)context.Users.Where(u => u.UserId == UserId).First().CompanyId;
                return (from u in context.Users
                        join ur in context.UserRoles on u.UserId equals ur.UserId
                        where !u.IsDeleted && u.CompanyId == companyId
                        && ur.RoleId == roleId
                        select new TrainerListViewModel
                        {
                            TrainerId = u.UserId,
                            FirstName = u.FirstName,
                            LastName = u.LastName
                        }).ToList();
            }
        }

        public void CreateTeam(CreateTeamViewModel team)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        team.CompanyId = (long)context.Users.Where(u => u.UserId == team.UserId).First().CompanyId;
                        Team newTeam = new Team
                        {
                            Name = team.TeamName,
                            CompanyId = team.CompanyId,
                            TrainerId = team.UserId,
                            CreatedBy = team.UserId,
                            CreatedDate = DateTime.UtcNow,
                            Status = team.Status,
                            CityId = team.LocationId,
                            TeamHandle = team.HandleName
                        };
                        context.Teams.Add(newTeam);
                        context.SaveChanges();
                        foreach (var user in team.Users)
                        {
                            UserTeam userTeam = new UserTeam
                            {
                                UserId = user.UserId,
                                TeamId = newTeam.TeamId
                            };
                            context.UserTeams.Add(userTeam);
                        }
                        context.SaveChanges();

                        List<TeamTag> tags = new List<TeamTag>();
                        if (team.TagIds != null)
                        {
                            team.TagIds.ForEach(tagId =>
                            {
                                tags.Add(new TeamTag() { TeamId = newTeam.TeamId, TagId = tagId });
                            });
                        }
                        context.TeamTags.AddRange(tags);
                       
                        // Save handle value

                        Tag tagsHandle = new Tag
                        {
                            TeamId = newTeam.TeamId,
                            Name = team.HandleName,
                            CreatedDate =DateTime.Now,
                            ModifiedDate = DateTime.Now
                        };
                        context.Tags.Add(tagsHandle);

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
        }

        public List<UserListViewModel> GetUsers(UserListRequestViewModel request)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                request.CompanyId = (long)context.Users.Where(u => u.UserId == request.UserId).First().CompanyId;
                return SortCollection.OrderBy<UserListViewModel>((from u in context.Users
                                                                  join ut in context.UserTeams on u.UserId equals ut.UserId into d
                                                                  from data in d.Where(o => o.TeamId == request.TeamId).DefaultIfEmpty()
                                                                  where !u.IsDeleted && u.IsActive && u.UserId != request.UserId
                                                                  && u.CompanyId == request.CompanyId
                                                                  && (string.IsNullOrEmpty(request.Search)
                                                                              || u.FirstName.ToLower().Contains(request.Search.ToLower()))
                                                                  && (request.LocationId == u.LocationId)
                                                                  select new UserListViewModel
                                                                  {
                                                                      UserId = u.UserId,
                                                                      ProfilePicture = u.ProfilePicture,
                                                                      FirstName = u.FirstName,
                                                                      LastName = u.LastName,
                                                                      IsSelected = data == null ? false : true
                                                                  }), request.SortBy).ToList();

            }
        }


        public List<long> UpdateTeam(TeamDetailViewModel team)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        List<long> usersToBeNotified = new List<long>() ;

                        Team existingTeam = context.Teams.Where(t => !t.isDeleted && t.TeamId == team.TeamId).FirstOrDefault();
                        if (existingTeam != null)
                        {
                           usersToBeNotified = GetUsersForNotification(existingTeam.UserTeams, team);

                            existingTeam.Name = team.TeamName;
                            existingTeam.Status = team.Status;
                            existingTeam.CityId = team.LocationId;
                            context.UserTeams.RemoveRange(context.UserTeams.Where(t => t.TeamId == team.TeamId).ToList());
                            //removing tags mapping for team
                            context.TeamTags.RemoveRange(context.TeamTags.Where(t => t.TeamId == team.TeamId).ToList());
                            context.SaveChanges();
                            foreach (var user in team.Users)
                            {
                                UserTeam userTeam = new UserTeam
                                {
                                    UserId = user.UserId,
                                    TeamId = existingTeam.TeamId
                                };
                                context.UserTeams.Add(userTeam);
                            }
                            context.SaveChanges();

                            List<TeamTag> tags = new List<TeamTag>();
                            if (team.TagIds != null)
                            {
                                team.TagIds.ForEach(tagId =>
                                {
                                    tags.Add(new TeamTag() { TeamId = existingTeam.TeamId, TagId = tagId });
                                });
                            }
                            context.TeamTags.AddRange(tags);

                            //updating publishedTeams_name
                            var publishedTeams = context.PublishedTeams.Where(pt => pt.MainTeamId == team.TeamId).ToList();
                            publishedTeams.ForEach(p =>
                            {
                                p.Name = existingTeam.Name;
                            });
                            context.SaveChanges();
                            
                            transaction.Commit();
                        }

                        return usersToBeNotified;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        private List<long> GetUsersForNotification(ICollection<UserTeam> existingUsers, TeamDetailViewModel team)
        {
            List<long> existingUsersList = existingUsers.Where(m => m.TeamId == team.TeamId).Select(m => m.UserId).ToList();

            List<long> newUsersList = team.Users.Select(m => m.UserId).ToList();

            return newUsersList.Except(existingUsersList).ToList(); 
        }

        public void DeleteTeam(long teamId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Team existingTeam = context.Teams.Where(t => !t.isDeleted && t.TeamId == teamId).FirstOrDefault();
                if (existingTeam != null)
                {
                    existingTeam.isDeleted = true;
                    context.SaveChanges();
                }
            }
        }

        public bool ValidateTeamHandleName(string name)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Team existingTeam = context.Teams.Where(t => (t.TeamHandle.ToLower().Equals(name.ToLower())) && !t.isDeleted).FirstOrDefault();
                if (existingTeam != null)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
