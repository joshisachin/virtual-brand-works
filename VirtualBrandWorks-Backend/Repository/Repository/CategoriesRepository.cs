﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class CategoriesRepository : ICategoriesRepository
    {
        public List<CategoryViewModel> GetAllCategories()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return (from c in context.Categories
                        where !c.IsDeleted && c.isDynamicCategory != true
                        orderby c.CategoryName
                        select new CategoryViewModel()
                        {
                            CategoryId = c.CategoryId,
                            CategoryName = c.CategoryName,
                            Description = c.Description
                        }).ToList();
            }
        }

        public void Create(CategoryViewModel categoryViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Category category = Mapper.Map<Category>(categoryViewModel);
                //needs to be updated with userId, for now using 0
                category.CreatedBy = 0;
                category.ModifiedBy = 0;
                category.CreatedDate = DateTime.UtcNow;
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public void Update(CategoryViewModel categoryViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Category category = context.Categories.Where(c => c.CategoryId == categoryViewModel.CategoryId).FirstOrDefault();
                if (category != null)
                {
                    category.CategoryName = categoryViewModel.CategoryName;
                    category.Description = categoryViewModel.Description;
                    context.SaveChanges();
                }
            }
        }
        public CategoryViewModel GetCategory(long categoryId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return (from c in context.Categories
                        where !c.IsDeleted && c.CategoryId == categoryId
                        select new CategoryViewModel()
                        {
                            CategoryId = c.CategoryId,
                            CategoryName = c.CategoryName,
                            Description = c.Description
                        }).FirstOrDefault();
            }
        }
        public Response Delete(long categoryId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Category category = context.Categories.Find(categoryId);
                category.IsDeleted = true;
                List<long> companyIds = context.Users.Where(u => !u.IsDeleted && u.Industry == categoryId).Select(obj => (long)obj.CompanyId).Distinct().ToList();
                // status of linked companies made false -> deactivated
                var result = context.Companies.Where(c => companyIds.Contains(c.CompanyId)).ToList().Select(c => { c.Status = false; return c; }).ToList();
                context.Entry(category).State = EntityState.Modified;
                context.SaveChanges();
                return new Response() { Status = true };

            }
        }

        public bool IsAssociatedCompany(long categoryId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return context.Users.Any(u => !u.IsDeleted && u.Industry == categoryId);
            }
        }

        public bool IsCategoryExists(CategoryViewModel categoryViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return context.Categories.Any(c => !c.IsDeleted && c.CategoryName.ToLower() == categoryViewModel.CategoryName.ToLower());
            }
        }
    }
}
