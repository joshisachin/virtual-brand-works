﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class TagRepository : ITagRepository
    {
        public List<TagViewModel> GetTags()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<TagViewModel> tagsList = new List<TagViewModel>();
                var tags = context.Tags.Where(t => !t.IsDeleted).ToList();
                tagsList = Mapper.Map<List<Tag>, List<TagViewModel>>(tags);
                return tagsList;
            }
        }

        public void Create(TagViewModel tag)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Tag newTag = new Tag();
                newTag = Mapper.Map<Tag>(tag);

                newTag.CreatedDate = DateTime.UtcNow;

                context.Tags.Add(newTag);
                context.SaveChanges();
            }
        }

        public bool IsTagExists(TagViewModel tag)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return context.Tags.Any(c => !c.IsDeleted && c.Name.ToLower() == tag.Name.ToLower() && c.ShortName.ToLower() == tag.ShortName.ToLower());
            }
        }

        public List<TagViewModel> GetTagSuggestion(string type, string suggestion, long userId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                List<TagViewModel> suggestions = new List<TagViewModel>();
                if (string.IsNullOrEmpty(suggestion))
                {
                    return suggestions;
                }
                if (type.ToLower().Equals(Constants.SUGGESTION_TYPE_USER)) // bind the users and team in the list
                {
                    if(suggestion.Substring(0,1) == "@")
                    {
                        //Get the Company of the users
                        suggestion = suggestion.Substring(1);
                        long companyId = (long)(from u in context.Users
                                                where u.UserId == userId
                                                select u.CompanyId).FirstOrDefault();

                        suggestions = (from t in context.Tags
                                       join u in context.Users on t.UserId equals u.UserId
                                       where t.UserId != null && u.CompanyId == companyId && u.UserHandle.StartsWith(suggestion)
                                       select new TagViewModel()
                                       {
                                           TagId = t.TagId,
                                           Name = u.UserHandle,
                                           ShortName = u.UserHandle,
                                           IsHandle = true
                                       }).Take(Constants.LimitDropDownSize).ToList();

                        suggestions.AddRange((from t in context.Tags
                                              join u in context.Teams on t.TeamId equals u.TeamId
                                              where t.TeamId != null && u.CompanyId == companyId && u.TeamHandle.StartsWith(suggestion)
                                              select new TagViewModel()
                                              {
                                                  TagId = t.TagId,
                                                  Name = u.TeamHandle,
                                                  ShortName = u.TeamHandle,
                                                  IsHandle = true
                                              }).Take(Constants.LimitDropDownSize).ToList());
                    }
                    
                }
                else if(type.ToLower().Equals(Constants.SUGGESTION_TYPE_TAGS))
                {
                    var user = context.Users.FirstOrDefault(x => x.UserId == userId && !x.IsDeleted);
                    if (user is null) throw new Exception("Invalid User");
                    List<Tag> tags = context.Tags.Where(t => !t.IsDeleted && t.Name.StartsWith(suggestion) && t.UserId == null && t.TeamId == null && t.IndustryId == user.Industry).Take(Constants.LimitDropDownSize).ToList();
                    suggestions = Mapper.Map<List<Tag>, List<TagViewModel>>(tags);
                    foreach (var item in suggestions)
                    {
                        item.IsHandle = false;
                    }
                }
                return suggestions;
            }
        }

        public List<TagViewModel> GetTagsWithIndustries()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return     (from t in context.Tags
                            join c in context.Categories on t.IndustryId equals c.CategoryId
                            where !t.IsDeleted && !c.IsDeleted
                            select new TagViewModel 
                            {
                                TagId = t.TagId,
                                Name = t.Name,
                                Industry = c.CategoryName
                            }).ToList();
                            
            }
        }
        public void EditTag(TagViewModel tagViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var tag = context.Tags.FirstOrDefault(x => x.TagId == tagViewModel.TagId);

                if(tag != null)
                {
                    tag.Name = tagViewModel.Name;
                    tag.IndustryId = tagViewModel.CategoryId;
                    tag.ModifiedDate = DateTime.UtcNow;

                    context.SaveChanges();
                }
            }
        }

        public TagViewModel GetTagById(long tagId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var tag = context.Tags.FirstOrDefault(x => x.TagId == tagId && !x.IsDeleted);
                var tagModel = Mapper.Map<Tag, TagViewModel>(tag);
                return tagModel;
            }
        }

        public void Delete(long tagId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var tag = context.Tags.FirstOrDefault(x => x.TagId == tagId);
                if(tag != null)
                {
                    tag.IsDeleted = true;
                    tag.ModifiedDate = DateTime.UtcNow;
                    context.SaveChanges();
                }
            }
        }
    }
}
