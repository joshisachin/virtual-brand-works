﻿using AutoMapper;
using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class SubscriptionRepository : ISubscriptionRepository
    {
        public IEnumerable<SubscriptionViewModel> GetSubscriptionList()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                IEnumerable<Subscription> subscriptionList = context.Subscriptions.Where(s => !s.IsDeleted).ToList();
                var subscriptionViewModelList = Mapper.Map<IEnumerable<Subscription>, IEnumerable<SubscriptionViewModel>>(subscriptionList);
                return subscriptionViewModelList;
            }
        }

        public List<SubscriptionViewModel> GetSubscriptionListForAdmin()
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return (from s in context.Subscriptions
                        join sp in context.SubscriptionPlanPrice on s.SubscriptionId equals sp.SubscriptionId
                        where !s.IsDeleted && s.Status
                        select new SubscriptionViewModel()
                        {
                            SubscriptionId = s.SubscriptionId,
                            Price = sp.Price,
                            PlanType = sp.PlanType,
                            PlanId = sp.PlanId,
                            PlanName = s.PlanName,//s.PlanName.Length > 25 ? s.PlanName.Substring(0,25)+"..." : s.PlanName,
                            NumberOfUsers = s.NumberOfUsers,
                            Status = s.Status,
                            SubscriptionPlanPriceId = sp.SubscriptionPlanPriceId
                        }).OrderBy(x => x.NumberOfUsers).ToList();
            }
        }

        /// <summary>
        ///Method to get Subscription details for a specific company 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public CompanySubscriptionViewModel GetSubscriptionDetailsForCompany(long companyId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return (from comp in context.Companies 
                        join c in context.CompanySubscription on comp.CompanyId equals c.CompanyId
                        join sp in context.SubscriptionPlanPrice on c.SubscriptionPlanPriceId equals sp.SubscriptionPlanPriceId
                        join s in context.Subscriptions on sp.SubscriptionId equals s.SubscriptionId
                        where c.CompanyId == companyId
                        select new CompanySubscriptionViewModel()
                        {
                            PlanId = sp.SubscriptionPlanPriceId,
                            SubscriptionName = s.PlanName,
                            PlanType = sp.PlanType,
                            UsersUpTo = s.NumberOfUsers,
                            ValidTill = c.ExpireDate,
                            ActiveFrom = c.IssueDate,
                            BilledAmount = sp.Price,
                            StripeSubscriptionId = comp.StripeSubscriptionId,
                            CompanySubscriptionId = c.CompanySubscriptionId
                        }).OrderByDescending(r => r.CompanySubscriptionId).FirstOrDefault();
            }
        }

        public List<SubscriptionPlanPriceViewModel> GetSubscriptionListForAdmin(string productId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return (from sp in context.SubscriptionPlanPrice
                        where sp.ProductId == productId
                        select new SubscriptionPlanPriceViewModel()
                        {
                            SubscriptionPlanPriceId = sp.SubscriptionPlanPriceId,
                            PlanId = sp.PlanId,
                            PlanType = sp.PlanType,
                            Price = sp.Price
                        }).ToList();
            }
        }

        public void Create(SubscriptionViewModel subscriptionViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Subscription subscription = Mapper.Map<Subscription>(subscriptionViewModel);
                if (subscription != null)
                {
                    Subscription sub = context.Subscriptions.Add(subscription);
                    context.SaveChanges();

                    List<SubscriptionPlanPrice> plans = new List<SubscriptionPlanPrice>();
                    subscriptionViewModel.SubscriptionPlanPrice.ForEach(obj =>
                    {
                        obj.SubscriptionId = sub.SubscriptionId;
                        plans.Add(Mapper.Map<SubscriptionPlanPrice>(obj));
                    });

                    context.SubscriptionPlanPrice.AddRange(plans);
                    context.SaveChanges();
                }
            }
        }

        public SubscriptionViewModel GetSubscriptionById(SubscriptionViewModel subscription)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                if (subscription.SubscriptionId > 0 && !string.IsNullOrEmpty(subscription.PlanId))
                {

                    return (from s in context.Subscriptions
                            join sp in context.SubscriptionPlanPrice on s.SubscriptionId equals sp.SubscriptionId
                            where !s.IsDeleted && s.SubscriptionId == subscription.SubscriptionId && sp.PlanId == subscription.PlanId
                            select new SubscriptionViewModel()
                            {
                                SubscriptionId = s.SubscriptionId,
                                Price = sp.Price,
                                PlanType = sp.PlanType,
                                PlanId = sp.PlanId,
                                PlanName = s.PlanName,
                                NumberOfUsers = s.NumberOfUsers,
                                Status = s.Status
                            }).FirstOrDefault();
                }
                else
                {
                    Subscription sub = context.Subscriptions.Where(obj => obj.SubscriptionId == subscription.SubscriptionId && !obj.IsDeleted).FirstOrDefault();
                    return Mapper.Map<SubscriptionViewModel>(sub);
                }
            }
        }
        public void Update(SubscriptionViewModel subscriptionViewModel)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Subscription subscription = Mapper.Map<Subscription>(subscriptionViewModel);
                if (subscription != null)
                {
                    subscriptionViewModel.SubscriptionPlanPrice.ForEach(price =>
                    {
                        SubscriptionPlanPrice plan = context.SubscriptionPlanPrice.Where(obj => obj.SubscriptionId == subscription.SubscriptionId
                        && obj.PlanType == price.PlanType).FirstOrDefault();
                        if (plan != null)
                        {
                            plan.PlanId = price.PlanId;
                            plan.Price = price.Price;
                        }
                    });

                    context.Entry(subscription).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        public void Delete(int subscriptionId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                Subscription subscription = context.Subscriptions.Where(s => !s.IsDeleted && s.SubscriptionId == subscriptionId).FirstOrDefault();
                if (subscription != null)
                {
                    // context.SubscriptionPlanPrice.RemoveRange(context.SubscriptionPlanPrice.Where(obj => obj.SubscriptionId == subscriptionId));

                    subscription.IsDeleted = true;
                    context.Entry(subscription).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        public string GetStripeSubscriptionId(long userId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                return (from u in context.Users
                        join c in context.Companies on u.CompanyId equals c.CompanyId
                        join cs in context.CompanySubscription on u.CompanyId equals cs.CompanyId
                        join sp in context.SubscriptionPlanPrice on cs.SubscriptionPlanPriceId equals sp.SubscriptionPlanPriceId
                        where u.UserId == userId /*&& cs.IsActive*/
                        select c.StripeSubscriptionId).FirstOrDefault();
            }
        }

        public List<SubscribedUserViewModel> GetSubscribedUsersForSubscription(long planId)
        {
            using (VirtualBrandWorksEntities context = new VirtualBrandWorksEntities())
            {
                var adminRole = (int)Constants.UserRoles.Admin;
                return (from sp in context.SubscriptionPlanPrice
                        join cs in context.CompanySubscription on sp.SubscriptionPlanPriceId equals cs.SubscriptionPlanPriceId
                        join u in context.Users on cs.CompanyId equals u.CompanyId
                        join ur in context.UserRoles on u.UserId equals ur.UserId
                        where !cs.IsObselete && sp.SubscriptionPlanPriceId == planId && ur.RoleId == adminRole
                        select new SubscribedUserViewModel
                        {
                            UserId = u.UserId,
                            UserName = u.FirstName,
                            EmailId = u.EmailId
                        }).GroupBy(x => x.UserId).Select(x => x.FirstOrDefault()).ToList();
            }
        }
    }
}
