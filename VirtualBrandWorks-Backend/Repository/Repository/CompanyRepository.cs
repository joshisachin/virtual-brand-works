﻿using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        public List<UserViewModel> GetUsersByCompanyId(long companyId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.Companies
                        join u in dbContext.Users on c.CompanyId equals u.CompanyId
                        join ur in dbContext.UserRoles on u.UserId equals ur.UserId
                        join r in dbContext.Roles on ur.RoleId equals r.RoleId
                        where (bool)!u.IsDeleted && c.CompanyId == companyId
                        select new UserViewModel()
                        {
                            UserId = u.UserId,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            EmailId = u.EmailId,
                            Company = c.Name,
                            CompanyId = companyId,
                            Role = r.Name,
                            CompanyStatus = c.Status,
                            Industry = dbContext.Categories.Any(cat => cat.CategoryId == u.Industry) ? dbContext.Categories.Where(cat => cat.CategoryId == u.Industry).FirstOrDefault().CategoryName : null
                        }).OrderBy(u => u.Role).ToList();
            }
        }

        public List<CompanyViewModel> GetAllCompany()
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from c in dbContext.Companies
                        where !c.IsDeleted
                        select new CompanyViewModel()
                        {
                            CompanyId = c.CompanyId,
                            Name = c.Name
                        }).OrderBy(o => o.Name).ToList();
            }
        }

        public CompanyViewModel ActivateOrDeactivateCompany(long companyId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var company = dbContext.Companies.Where(c =>c.CompanyId == companyId).FirstOrDefault();
                if(company != null)
                {
                    company.Status = !company.Status;
                    dbContext.SaveChanges();
                }
                CompanyViewModel companyInfo = (from c in dbContext.Companies
                                                join u in dbContext.Users on c.CompanyId equals u.CompanyId
                                                join ur in dbContext.UserRoles on u.UserId equals ur.UserId
                                                join r in dbContext.Roles on ur.RoleId equals r.RoleId
                                                where (bool)!u.IsDeleted && c.CompanyId == companyId && r.Name.Equals(Constants.UserRoles.Admin.ToString())
                                                select new CompanyViewModel()
                                                {
                                                    CompanyId = c.CompanyId,
                                                    Name = c.Name,
                                                    Status = c.Status,
                                                    AdminEmail = u.EmailId
                                                }).FirstOrDefault();
                return companyInfo;
            }
        }
    }
}
