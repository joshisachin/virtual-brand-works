﻿using Repository.Interfaces;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Repository.Repository
{
    public class MyPlaylistRepository : IMyPlaylistRepository
    {
        public List<PlaylistListViewModel> GetMyCreatedPlaylist(long userId, bool isLaunched)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var result = (from p in dbContext.PlayLists
                              where !p.IsDeleted && p.IsLaunched == isLaunched && p.CreatedBy == userId
                              select new PlaylistListViewModel
                              {
                                  PlaylistId = p.PlayListId,
                                  Name = p.Name,
                                  Description = p.Description,
                                  StartDate = p.StartDate,
                                  EndDate = p.EndDate,
                                  IsExpired = p.IsCompleted,//p.IsCompleted,
                                  NumberOfUsers = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && !pa.IsAssignedToTeam).ToList().Count(),
                                  NumberOfTeams = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && pa.IsAssignedToTeam).ToList().Count(),
                                  Status = p.IsLaunched,
                                  MessageCount = dbContext.Messages.Where(m => m.PlayListId == p.PlayListId && m.ReceiverId == userId && !m.IsMessageRead).Count(),
                                  Folders = ((from pf in dbContext.PlayListMasters
                                              join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                              where pf.IsFromMasterLibrary && pf.PlayListId == p.PlayListId
                                                      select new PlaylistFolderViewModel
                                                      {
                                                          FolderId = mf.MasterFolderId,
                                                          FolderName = mf.Name,
                                                          FolderShortName = mf.ShortName,
                                                          IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                          SortOrder = pf.SortOrder,
                                                          NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                                      }).Union(from pf in dbContext.PlayListMasters
                                                               join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                               where !pf.IsFromMasterLibrary && pf.PlayListId == p.PlayListId
                                                               select new PlaylistFolderViewModel()
                                                               {
                                                                   FolderId = pf.FolderId,
                                                                   FolderName = cf.Name,
                                                                   FolderShortName = cf.ShortName,
                                                                   IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                                   SortOrder = pf.SortOrder,
                                                                   NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                                               })).OrderBy(j => j.SortOrder).ToList()
                                      }).ToList();

                return result.OrderBy(r => r.IsExpired).ThenByDescending(rs => rs.EndDate).ToList();

            }
        }


        public List<PlaylistListViewModel> GetMyPlaylist(long userId, bool isCompleted)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var result = (from p in dbContext.PlayLists
                              join pa in dbContext.PlayListAssignedToes on p.PlayListId equals pa.PlayListId
                              join t in dbContext.PublishedUserTeams on userId equals t.UserId into d
                              from team in d.Where(s => s.PublishedTeamId == pa.PublishedTeamId).DefaultIfEmpty()
                              where !p.IsDeleted && p.IsLaunched && ((!pa.IsAssignedToTeam && pa.UserId == userId) || (pa.IsAssignedToTeam && pa.PublishedTeamId == team.PublishedTeamId))
                              select new PlaylistListViewModel
                              {
                                  PlaylistId = p.PlayListId,
                                  Name = p.Name,
                                  StartDate = p.StartDate,
                                  EndDate = p.EndDate,
                                  IsExpired = p.IsCompleted,//p.IsCompleted, //IsCompleted need to be renamed to IsExpired in db and updated using service/scheduler
                                  IsUserPass = dbContext.UserQuizResults.Any(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId) ? (((double)dbContext.UserQuizResults.Where(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId && uq.IsPassed).ToList().Count() / (double)dbContext.UserQuizResults.Where(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId).ToList().Count())*100) >= Constants.PassingPercentage : true,
                                  NumberOfUsers = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && !pa.IsAssignedToTeam).ToList().Count(),
                                  NumberOfTeams = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && pa.IsAssignedToTeam).ToList().Count(),
                                  Status = p.IsLaunched,
                                  MessageCount = dbContext.Messages.Where(m => m.PlayListId == p.PlayListId && m.ReceiverId == userId && !m.IsMessageRead).Count() ,
                                  Folders = ((from pf in dbContext.PlayListMasters
                                              join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                              where pf.IsFromMasterLibrary && pf.PlayListId == p.PlayListId
                                              select new PlaylistFolderViewModel
                                              {
                                                  FolderId = mf.MasterFolderId,
                                                  FolderName = mf.Name,
                                                  FolderShortName = mf.ShortName,
                                                  IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                  SortOrder = pf.SortOrder,
                                                  NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                              }).Union(from pf in dbContext.PlayListMasters
                                                       join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                       where !pf.IsFromMasterLibrary && pf.PlayListId == p.PlayListId
                                                       select new PlaylistFolderViewModel()
                                                       {
                                                           FolderId = pf.FolderId,
                                                           FolderName = cf.Name,
                                                           FolderShortName = cf.ShortName,
                                                           IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                           SortOrder = pf.SortOrder,
                                                           NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count()
                                                       })).OrderBy(j => j.SortOrder).ToList()
                              }).GroupBy(x => x.PlaylistId).Select(x => x.FirstOrDefault()).ToList();

                if (isCompleted)
                {
                    result = (from r in result
                              where (dbContext.UserPlayListStatus.Any(ups => ups.UserId == userId && ups.PlaylistId == r.PlaylistId && ups.IsComplete))
                              select r).ToList();
                }
                else
                {
                    result = (from r in result
                              where (!(dbContext.UserPlayListStatus.Any(ups => ups.UserId == userId && ups.PlaylistId == r.PlaylistId && ups.IsComplete)) || (dbContext.UserPlayListStatus.Any(ups => ups.UserId == userId && ups.PlaylistId == r.PlaylistId && !ups.IsComplete)))
                              select r).ToList();
                }
                //result.ForEach(r => { if (r.StartDate != null && r.EndDate != null) { r.StartDate = r.StartDate.Value.ToLocalTime(); r.EndDate = r.EndDate.Value.ToLocalTime(); } });

                return result.OrderBy(r => r.IsExpired).ThenByDescending(rs => rs.EndDate).ToList();

            }
        }

        public MyPlaylistDetailViewModel GetMyPlaylistDetail(long playlistId, long userId, bool isSelfCreated)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (isSelfCreated)
                {
                    var result = (from p in dbContext.PlayLists
                                      //join pa in dbContext.PlayListAssignedToes on p.PlayListId equals pa.PlayListId
                                  where p.IsLaunched /*&& pa.UserId == userId*/ && p.PlayListId == playlistId
                                  select new MyPlaylistDetailViewModel
                                  {
                                      PlaylistId = p.PlayListId,
                                      Name = p.Name,
                                      Description = p.Description,
                                      StartDate = (DateTime)p.StartDate,
                                      EndDate = (DateTime)p.EndDate,
                                      IsExpired = p.IsCompleted,//p.IsCompleted
                                      NumberOfUsers = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && !pa.IsAssignedToTeam).ToList().Count(),
                                      NumberOfTeams = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && pa.IsAssignedToTeam).ToList().Count(),
                                      Status = p.IsLaunched,
                                      IsCompleted = dbContext.UserPlayListStatus.Any(up => up.PlaylistId == playlistId && up.UserId == userId) == true ? dbContext.UserPlayListStatus.Where(up => up.PlaylistId == playlistId && up.UserId == userId).FirstOrDefault().IsComplete : false,
                                      IsUserPass = dbContext.UserQuizResults.Any(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId) ? (((double)dbContext.UserQuizResults.Where(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId && uq.IsPassed).ToList().Count() / (double)dbContext.UserQuizResults.Where(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId).ToList().Count()) * 100) >= Constants.PassingPercentage : true,
                                      TotalMarksObtained = (from uq in dbContext.UserQuizResults
                                                            where uq.PlaylistId == playlistId && uq.UserId == userId
                                                            select uq.MarksObtained).Sum(),
                                      TotalMarks = (from uq in dbContext.UserQuizResults
                                                    where uq.PlaylistId == playlistId && uq.UserId == userId
                                                    select uq.TotalMarks).Sum(),
                                      IsAssigned = false
                                  }).FirstOrDefault();
                    if (result != null)
                    {
                        result.Badges = (from upb in dbContext.UserPlaylistBadges
                                         where upb.PlaylistId == playlistId
                                         group upb by upb.BadgeId into gu
                                         select new BadgeInfoViewModel
                                         {
                                             BadgeId = (int)gu.Key,
                                             NumberOfAwardees = gu.Count()
                                         }).ToList();
                        result.Folders = ((from pf in dbContext.PlayListMasters
                                           join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                           where pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                                           select new PlaylistFolderViewModel
                                           {
                                               PlayListFolderId = pf.PlayListMasterId,
                                               FolderId = mf.MasterFolderId,
                                               FolderName = mf.Name,
                                               FolderShortName = mf.ShortName,
                                               IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                               SortOrder = pf.SortOrder,
                                               NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                               QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                            join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                            where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                            select pq.PublishedPlaylistQuizId).ToList().Count(),
                                           }).Union(from pf in dbContext.PlayListMasters
                                                    join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                    where !pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                                                    select new PlaylistFolderViewModel()
                                                    {
                                                        PlayListFolderId = pf.PlayListMasterId,
                                                        FolderId = pf.FolderId,
                                                        FolderName = cf.Name,
                                                        FolderShortName = cf.ShortName,
                                                        IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                        SortOrder = pf.SortOrder,
                                                        NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                                        QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                     join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                     where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                     select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                    })).OrderBy(j => j.SortOrder).ToList();

                        IEnumerable<long> userIds = (from pa in dbContext.PlayListAssignedToes
                                                     join ut in dbContext.PublishedUserTeams on pa.PublishedTeamId equals ut.PublishedTeamId
                                                     join u in dbContext.Users on ut.UserId equals u.UserId
                                                     where pa.IsAssignedToTeam && !u.IsDeleted && pa.PlayListId == playlistId
                                                     select u.UserId).ToList().Union((from pa in dbContext.PlayListAssignedToes
                                                                                      join u in dbContext.Users on pa.UserId equals u.UserId
                                                                                      where !pa.IsAssignedToTeam && !u.IsDeleted && pa.PlayListId == playlistId
                                                                                      select u.UserId).ToList()).Distinct();
                        if(userIds.Count() > 0)
                        {
                            result.Folders.ForEach(f =>
                            {
                                foreach (var id in userIds)
                                {
                                    if (f.QuizCount > 0)
                                    {
                                        f.QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                where pc.PlayListMasterId == f.PlayListFolderId && uq.PlaylistId == playlistId && uq.UserId == id && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                select uq.QuizId).ToList().Count();
                                        if (f.QuizCount == f.QuizCompletedCount)
                                        {
                                            f.NumberOfUserCompletedFolder = f.NumberOfUserCompletedFolder + 1;
                                        }
                                    }
                                    else
                                    {
                                        f.NumberOfUserCompletedFolder = f.NumberOfUserCompletedFolder + 1;
                                    }
                                }
                                f.PlaylistFolderCompletePercentage = (int)(((double)f.NumberOfUserCompletedFolder / (double)userIds.Count()) * 100);

                            });
                        }
                        
                    }
                    return result;
                }
                else
                {
                    var result = (from p in dbContext.PlayLists
                                      //join pa in dbContext.PlayListAssignedToes on p.PlayListId equals pa.PlayListId
                                  where p.IsLaunched /*&& pa.UserId == userId*/ && p.PlayListId == playlistId
                                  select new MyPlaylistDetailViewModel
                                  {
                                      PlaylistId = p.PlayListId,
                                      Name = p.Name,
                                      Description = p.Description,
                                      StartDate = (DateTime)p.StartDate,
                                      EndDate = (DateTime)p.EndDate,
                                      IsExpired = p.IsCompleted,//p.IsCompleted
                                      NumberOfUsers = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && !pa.IsAssignedToTeam).ToList().Count(),
                                      NumberOfTeams = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && pa.IsAssignedToTeam).ToList().Count(),
                                      Status = p.IsLaunched,
                                      IsCompleted = dbContext.UserPlayListStatus.Any(up => up.PlaylistId == playlistId && up.UserId == userId) == true ? dbContext.UserPlayListStatus.Where(up => up.PlaylistId == playlistId && up.UserId == userId).FirstOrDefault().IsComplete : false,
                                      IsUserPass = dbContext.UserQuizResults.Any(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId) ? (((double)dbContext.UserQuizResults.Where(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId && uq.IsPassed).ToList().Count() / (double)dbContext.UserQuizResults.Where(uq => uq.PlaylistId == p.PlayListId && uq.UserId == userId).ToList().Count()) * 100) >= Constants.PassingPercentage : true,
                                      TotalMarksObtained = (from uq in dbContext.UserQuizResults
                                                            where uq.PlaylistId == playlistId && uq.UserId == userId
                                                            select uq.MarksObtained).Sum(),
                                      TotalMarks = (from uq in dbContext.UserQuizResults
                                                    where uq.PlaylistId == playlistId && uq.UserId == userId
                                                    select uq.TotalMarks).Sum(),
                                      IsAssigned = true
                                  }).FirstOrDefault();
                    if (result != null)
                    {
                        result.Badges = (from upb in dbContext.UserPlaylistBadges
                                         where upb.PlaylistId == playlistId && upb.UserId == userId
                                         select new BadgeInfoViewModel
                                         {
                                             BadgeId = upb.BadgeId
                                         }).ToList();
                        result.Folders = ((from pf in dbContext.PlayListMasters
                                           join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                           where pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                                           select new PlaylistFolderViewModel
                                           {
                                               PlayListFolderId = pf.PlayListMasterId,
                                               FolderId = mf.MasterFolderId,
                                               FolderName = mf.Name,
                                               FolderShortName = mf.ShortName,
                                               IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                               SortOrder = pf.SortOrder,
                                               NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                               //completed calculation 
                                               QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                            join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                            where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                            select pq.PublishedPlaylistQuizId).ToList().Count(),
                                               QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                     join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                     where pc.PlayListMasterId == pf.PlayListMasterId && uq.PlaylistId == playlistId && uq.UserId == userId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                     select uq.QuizId).ToList().Count(),
                                               TotalMarks = (from pcm in dbContext.PlayListContentMasters
                                                             join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                             where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId
                                                             select uq.TotalMarks).Sum(),
                                               MarksObtained = (from pcm in dbContext.PlayListContentMasters
                                                                join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId
                                                                select uq.MarksObtained).Sum()
                                           }).Union(from pf in dbContext.PlayListMasters
                                                    join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                                    where !pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                                                    select new PlaylistFolderViewModel()
                                                    {
                                                        PlayListFolderId = pf.PlayListMasterId,
                                                        FolderId = pf.FolderId,
                                                        FolderName = cf.Name,
                                                        FolderShortName = cf.ShortName,
                                                        IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                                        SortOrder = pf.SortOrder,
                                                        NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                                        //completed calculation 
                                                        QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                     join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                     where pcm.PlayListMasterId == pf.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                     select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                        QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                              join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                              where pc.PlayListMasterId == pf.PlayListMasterId && uq.PlaylistId == playlistId && uq.UserId == userId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                              select uq.QuizId).ToList().Count(),
                                                        TotalMarks = (from pcm in dbContext.PlayListContentMasters
                                                                      join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                      where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId
                                                                      select uq.TotalMarks).Sum(),
                                                        MarksObtained = (from pcm in dbContext.PlayListContentMasters
                                                                         join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                         where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId
                                                                         select uq.MarksObtained).Sum()
                                                    })).OrderBy(j => j.SortOrder).ToList();
                    }
                    return result;
                }

            }
        }

        public ViewResultDetailAndFoldersViewModel ViewResultDetailAndFolders(long playlistId, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                ViewResultDetailAndFoldersViewModel result = new ViewResultDetailAndFoldersViewModel();
                User user = dbContext.Users.Where(u => !u.IsDeleted && u.UserId == userId).FirstOrDefault();
                if(user != null)
                {
                    result.FirstName = user.FirstName;
                    result.LastName = user.LastName;
                }

                //badges to be added later
               result.folders = ((from pf in dbContext.PlayListMasters
                                  join mf in dbContext.MasterFolders on pf.FolderId equals mf.MasterFolderId
                                  where pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                                  select new PlaylistFolderViewModel
                                  {
                                      PlayListFolderId = pf.PlayListMasterId,
                                      PlayListId = pf.PlayListId,
                                      FolderId = pf.FolderId,
                                      FolderName = mf.Name,
                                      FolderShortName = mf.ShortName,
                                      IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                      SortOrder = pf.SortOrder,
                                      NumberOfContent = dbContext.PlayListContentMasters.Where(pc => !pc.IsDeleted && pc.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                      MarksObtained = (from pcm in dbContext.PlayListContentMasters
                                                       join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                       where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId 
                                                       select uq.MarksObtained).Sum(),
                                      TotalMarks = (from pcm in dbContext.PlayListContentMasters
                                                    join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                    where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId 
                                                    select uq.TotalMarks).Sum()
                                  }).Union(from pf in dbContext.PlayListMasters
                                           join cf in dbContext.CompanyLibraryFolders on pf.FolderId equals cf.CompanyLibraryFolderId
                                           where !pf.IsFromMasterLibrary && pf.PlayListId == playlistId
                                           select new PlaylistFolderViewModel()
                                           {
                                               PlayListFolderId = pf.PlayListMasterId,
                                               PlayListId = pf.PlayListId,
                                               FolderId = pf.FolderId,
                                               FolderName = cf.Name,
                                               FolderShortName = cf.ShortName,
                                               IsMasterLibraryFolder = pf.IsFromMasterLibrary,
                                               SortOrder = pf.SortOrder,
                                               NumberOfContent = dbContext.PlayListContentMasters.Where(pc => !pc.IsDeleted && pc.PlayListMasterId == pf.PlayListMasterId).ToList().Count(),
                                               MarksObtained = (from pcm in dbContext.PlayListContentMasters
                                                                join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId 
                                                                select uq.MarksObtained).Sum(),
                                               TotalMarks = (from pcm in dbContext.PlayListContentMasters
                                                             join uq in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                             where uq.PlaylistId == playlistId && uq.UserId == userId && pcm.PlayListMasterId == pf.PlayListMasterId 
                                                             select uq.TotalMarks).Sum()
                                           })).OrderBy(p => p.SortOrder).ToList();
                return result;
            }
        }

        public List<PlaylistContentResponseViewModel> ViewResultContentList(long playListFolderId, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var playlistMaster = dbContext.PlayListMasters.Where(p => p.PlayListMasterId == playListFolderId).FirstOrDefault();
                if (playlistMaster != null)
                {
                    if (playlistMaster.IsFromMasterLibrary)
                    {
                        return (from mc in dbContext.MasterContents
                                join saveContent in dbContext.PlayListContentMasters on mc.MasterContentId equals saveContent.ContentId
                                join uq in dbContext.UserQuizResults on saveContent.PlayListContentMasterId equals uq.PlayListContentMasterId into d
                                from userQuizResult in d.Where(s => s.UserId == userId).DefaultIfEmpty()
                                where mc.IsPublished == true && !mc.IsDeleted && saveContent.ContentId == mc.MasterContentId && saveContent.PlayListMasterId == playListFolderId
                                select new PlaylistContentResponseViewModel()
                                {
                                    PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                    IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                    IsUserPassed = userQuizResult.IsPassed,
                                    MarksObtained = userQuizResult.MarksObtained,
                                    NumberOfQuizAttempts = userQuizResult.NumberOfAttempt,
                                    Content = new ContentViewModel
                                    {
                                        ContentId = mc.MasterContentId,
                                        ContentName = mc.Name,
                                        CreatedDate = mc.CreatedDate,
                                        ContentDescription = mc.Description,
                                        ContentURL = mc.ContentUrl,
                                        ViewCount = 0,
                                        QuizId = dbContext.PublishedPlaylistQuiz.Where(q => !q.IsDeleted && q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().PublishedPlaylistQuizId,
                                        MimeType = mc.MimeType,
                                        IsPublished = mc.IsPublished,
                                        IsAwsContent = true
                                    }

                                }).ToList();
                    }
                    else
                    {
                        return (from cc in dbContext.CompanyLibraryContents
                                join saveContent in dbContext.PlayListContentMasters on cc.CompanyLibraryContentId equals saveContent.ContentId
                                join uq in dbContext.UserQuizResults on saveContent.PlayListContentMasterId equals uq.PlayListContentMasterId into d
                                from userQuizResult in d.Where(s => s.UserId == userId).DefaultIfEmpty()
                                where cc.IsPublished == true && !cc.IsDeleted && saveContent.ContentId == cc.CompanyLibraryContentId && saveContent.PlayListMasterId == playListFolderId
                                select new PlaylistContentResponseViewModel()
                                {
                                    PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                    IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                    IsUserPassed = userQuizResult.IsPassed,
                                    MarksObtained = userQuizResult.MarksObtained,
                                    NumberOfQuizAttempts = userQuizResult.NumberOfAttempt,
                                    Content = new ContentViewModel
                                    {
                                        ContentId = cc.CompanyLibraryContentId,
                                        ContentName = cc.Name,
                                        CreatedDate = cc.CreatedDate,
                                        ContentDescription = cc.Description,
                                        ContentURL = cc.ContentUrl,
                                        ViewCount = 0,
                                        QuizId = dbContext.PublishedPlaylistQuiz.Where(q => !q.IsDeleted && q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().PublishedPlaylistQuizId,
                                        MimeType = cc.MimeType,
                                        IsPublished = cc.IsPublished,
                                        IsAwsContent = cc.IsAwsContent,
                                        IsYoutubeContent = cc.IsYoutubeContent,
                                        IsFacebookContent = cc.IsFacebookContent
                                    }

                                }).ToList();
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Note -> isMasterQuiz is true when the Folder is a master folder and isTurnedOffExistingQuiz is also false 
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="isMasterQuiz"></param>
        /// <returns></returns>
        public CompanyQuizViewModel GetQuizResult(long quizId, long playlistId, long userId, bool isMasterQuiz) 
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                CompanyQuizViewModel result = (from pq in dbContext.PublishedPlaylistQuiz
                                               join pques in dbContext.PublishedPlaylistQuestion on pq.PublishedPlaylistQuizId equals pques.PublishedPlaylistQuizId
                                               into d
                                               from data in d.DefaultIfEmpty()
                                               where pq.PublishedPlaylistQuizId == quizId
                                               select new CompanyQuizViewModel()
                                               {
                                                   QuizId = pq.PublishedPlaylistQuizId,
                                                   QuizName = pq.Name,
                                                   QuestionAnswers = (from q in dbContext.PublishedPlaylistQuestion
                                                                      where q.PublishedPlaylistQuizId == quizId
                                                                      select new QuestionAnswerViewModel()
                                                                      {
                                                                          Question = new QuestionViewModel() { QuestionId = q.PublishedPlaylistQuestionId, Question = q.Question },
                                                                          Answers = (from pans in dbContext.PublishedPlaylistAnswer
                                                                                     where pans.PublishedPlaylistQuestionId == q.PublishedPlaylistQuestionId
                                                                                     select new AnswerViewModel()
                                                                                     {
                                                                                         AnswerId = pans.PublishedPlaylistAnswerId,
                                                                                         Answer = pans.Options,
                                                                                         IsRightAnswer = pans.IsAnswer
                                                                                     }).ToList(),
                                                                          RightAnsId = (from pans in dbContext.PublishedPlaylistAnswer
                                                                                        where pans.PublishedPlaylistQuestionId == q.PublishedPlaylistQuestionId && pans.IsAnswer
                                                                                        select pans.PublishedPlaylistAnswerId).FirstOrDefault(),
                                                                          UserAnswerId = dbContext.UserAnsweredQuizs.Any(uaq => uaq.UserId == userId && uaq.PlayListId == playlistId && uaq.QuizId == quizId && uaq.QuestionId == q.PublishedPlaylistQuestionId) == false ? 0
                                                                          : (long)dbContext.UserAnsweredQuizs.Where(uaq => uaq.UserId == userId && uaq.PlayListId == playlistId && uaq.QuizId == quizId && uaq.QuestionId == q.PublishedPlaylistQuestionId).FirstOrDefault().AnswereId
                                                                      }).ToList()
                                               }).FirstOrDefault();

                if(result != null && result.QuestionAnswers.Count() > 0)
                {
                    result.TotalQuestion = result.QuestionAnswers.Count();
                    result.CorrectCount = result.QuestionAnswers.Where(qa => qa.UserAnswerId == qa.RightAnsId).Count();
                    result.IncorrectCount = result.QuestionAnswers.Where(qa => qa.UserAnswerId != qa.RightAnsId).Count();
                    result.MarksObtained = dbContext.UserQuizResults.Any(uqr => uqr.UserId == userId && uqr.PlaylistId == playlistId && uqr.QuizId == quizId) == false ? 0 : dbContext.UserQuizResults.Where(uqr => uqr.UserId == userId && uqr.PlaylistId == playlistId && uqr.QuizId == quizId).FirstOrDefault().MarksObtained;
                }
                return result;
            }
        }

        public MyPlaylistFolderDetailViewModel GetMyPlaylistFolderDetail(long playListFolderId, long playlistContentMasterId, long userId, string ipAddress)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var result = (from p in dbContext.PlayLists
                              join pa in dbContext.PlayListMasters on p.PlayListId equals pa.PlayListId
                              where p.IsLaunched && pa.PlayListMasterId == playListFolderId /*&& p.PlayListId == playlistId*/
                              select new MyPlaylistFolderDetailViewModel
                              {
                                  Folder = pa.IsFromMasterLibrary == true ? (from mf in dbContext.MasterFolders
                                                                             where mf.MasterFolderId == pa.FolderId
                                                                             select new PlaylistFolderViewModel
                                                                             {
                                                                                 FolderName = mf.Name,
                                                                                 FolderShortName = mf.ShortName.ToLower(),
                                                                                 NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pa.PlayListMasterId).ToList().Count(),
                                                                                 IsMasterLibraryFolder = pa.IsFromMasterLibrary,
                                                                                 //completed calculation 
                                                                                 QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                                              join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                                              where pcm.PlayListMasterId == pa.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                                              select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                                                 QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                                       join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                                       where pc.PlayListMasterId == pa.PlayListMasterId && uq.PlaylistId == pa.PlayListId && uq.UserId == userId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                                       select uq.QuizId).ToList().Count()
                                                                             }).FirstOrDefault() : (from cf in dbContext.CompanyLibraryFolders
                                                                                                    where cf.CompanyLibraryFolderId == pa.FolderId
                                                                                                    select new PlaylistFolderViewModel
                                                                                                    {
                                                                                                        FolderName = cf.Name,
                                                                                                        FolderShortName = cf.ShortName.ToLower(),
                                                                                                        NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pa.PlayListMasterId).ToList().Count(),
                                                                                                        IsMasterLibraryFolder = pa.IsFromMasterLibrary,
                                                                                                        //completed calculation 
                                                                                                        QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                                                                     join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                                                                     where pcm.PlayListMasterId == pa.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                                                                     select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                                                                        QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                                                              join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                                                              where pc.PlayListMasterId == pa.PlayListMasterId && uq.PlaylistId == pa.PlayListId && uq.UserId == userId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                                                              select uq.QuizId).ToList().Count()
                                                                                                    }).FirstOrDefault(),
                                  
                                  StartDate = p.StartDate,
                                  EndDate = p.EndDate,

                              }).FirstOrDefault();

                if (result != null)
                {
                    result.PlaylistContent = result.Folder.IsMasterLibraryFolder == true ? (from mc in dbContext.MasterContents
                                                                                            join saveContent in dbContext.PlayListContentMasters on mc.MasterContentId equals saveContent.ContentId
                                                                                            where mc.IsPublished == true && !mc.IsDeleted && saveContent.ContentId == mc.MasterContentId && saveContent.PlayListMasterId == playListFolderId && (playlistContentMasterId == 0 || saveContent.PlayListContentMasterId == playlistContentMasterId)
                                                                                            select new PlaylistContentResponseViewModel()
                                                                                            {
                                                                                                PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                                                                                IsUserPassed = dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? false : dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().IsPassed,
                                                                                                NumberOfQuizAttempts = dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().NumberOfAttempt,
                                                                                                IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                                                                                Content = new ContentViewModel
                                                                                                {
                                                                                                    ContentId = mc.MasterContentId,
                                                                                                    ContentName = mc.Name,
                                                                                                    CreatedDate = mc.CreatedDate,
                                                                                                    ContentDescription = mc.Description,
                                                                                                    ContentURL = mc.ContentUrl,
                                                                                                    ViewCount = mc.ViewCount,
                                                                                                    QuizId = dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().PublishedPlaylistQuizId,
                                                                                                    MimeType = mc.MimeType,
                                                                                                    IsPublished = mc.IsPublished,
                                                                                                    IsAwsContent = true
                                                                                                }
                                                                                            }).FirstOrDefault() : (from cc in dbContext.CompanyLibraryContents
                                                                                                                   join saveContent in dbContext.PlayListContentMasters on cc.CompanyLibraryContentId equals saveContent.ContentId
                                                                                                                   where cc.IsPublished == true && !cc.IsDeleted && saveContent.ContentId == cc.CompanyLibraryContentId && saveContent.PlayListMasterId == playListFolderId && (playlistContentMasterId == 0 || saveContent.PlayListContentMasterId == playlistContentMasterId)
                                                                                                                   select new PlaylistContentResponseViewModel()
                                                                                                                   {
                                                                                                                       PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                                                                                                       IsUserPassed = dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? false : dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().IsPassed,
                                                                                                                       NumberOfQuizAttempts = dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.UserQuizResults.Where(uq => uq.UserId == userId && uq.PlayListContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().NumberOfAttempt,
                                                                                                                       IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                                                                                                       Content = new ContentViewModel
                                                                                                                       {
                                                                                                                           ContentId = cc.CompanyLibraryContentId,
                                                                                                                           ContentName = cc.Name,
                                                                                                                           CreatedDate = cc.CreatedDate,
                                                                                                                           ContentDescription = cc.Description,
                                                                                                                           ContentURL = cc.ContentUrl,
                                                                                                                           ViewCount = cc.ViewCount,
                                                                                                                           QuizId = dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().PublishedPlaylistQuizId,
                                                                                                                           MimeType = cc.MimeType,
                                                                                                                           IsPublished = cc.IsPublished,
                                                                                                                           IsAwsContent = cc.IsAwsContent,
                                                                                                                           IsYoutubeContent = cc.IsYoutubeContent,
                                                                                                                           IsFacebookContent = cc.IsFacebookContent
                                                                                                                       }
                                                                                                                   }).FirstOrDefault();
                    //if (result.StartDate != null && result.EndDate != null)
                    //{
                    //    result.StartDate = result.StartDate.Value.ToLocalTime();
                    //    result.EndDate = result.EndDate.Value.ToLocalTime();
                    //}
                    if(result.PlaylistContent != null && !string.IsNullOrEmpty(ipAddress))
                    {
                        if (result.Folder.IsMasterLibraryFolder)
                        {
                            bool isViewAlreadyConsidered = dbContext.UserContentViews.Any(uc => uc.UserId == userId && uc.IPAddress == ipAddress && uc.ContentId == result.PlaylistContent.Content.ContentId && uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                int viewCountForSameUserWithDifferentIP = dbContext.UserContentViews.Where(uc => uc.UserId == userId && uc.ContentId == result.PlaylistContent.Content.ContentId && uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                                if (viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                                {
                                    dbContext.UserContentViews.Add(new UserContentViews()
                                    {
                                        UserId = userId,
                                        IPAddress = ipAddress,
                                        ContentId = result.PlaylistContent.Content.ContentId,
                                        IsMasterContent = true,
                                        CreatedDate = DateTime.UtcNow
                                    });

                                    MasterContent existingContent = dbContext.MasterContents.Where(mc => mc.MasterContentId == result.PlaylistContent.Content.ContentId).FirstOrDefault();
                                    if (existingContent != null)
                                    {
                                        existingContent.ViewCount = existingContent.ViewCount + 1;
                                    }
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            bool isViewAlreadyConsidered = dbContext.UserContentViews.Any(uc => uc.UserId == userId && uc.IPAddress == ipAddress && uc.ContentId == result.PlaylistContent.Content.ContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow));
                            if (!isViewAlreadyConsidered)
                            {
                                int viewCountForSameUserWithDifferentIP = dbContext.UserContentViews.Where(uc => uc.UserId == userId && uc.ContentId == result.PlaylistContent.Content.ContentId && !uc.IsMasterContent && EntityFunctions.TruncateTime(uc.CreatedDate) == EntityFunctions.TruncateTime(DateTime.UtcNow)).Count();
                                if (viewCountForSameUserWithDifferentIP < Constants.ViewCountForSameUserWithDifferentIP)
                                {
                                    dbContext.UserContentViews.Add(new UserContentViews()
                                    {
                                        UserId = userId,
                                        IPAddress = ipAddress,
                                        ContentId = result.PlaylistContent.Content.ContentId,
                                        IsMasterContent = false,
                                        CreatedDate = DateTime.UtcNow
                                    });

                                    CompanyLibraryContent existingContent = dbContext.CompanyLibraryContents.Where(cl => cl.CompanyLibraryContentId == result.PlaylistContent.Content.ContentId).FirstOrDefault();
                                    if (existingContent != null)
                                    {
                                        existingContent.ViewCount = existingContent.ViewCount + 1;                                        
                                    }
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                return result;

            }
        }

        /// <summary>
        /// method to get list of contents within the folder , other than the content being displayled
        /// </summary>
        /// <param name="playListFolderId"></param>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public List<PlaylistContentResponseViewModel> GetMyPlaylistFolderContentList(long playListFolderId, long contentId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var playlistMaster = dbContext.PlayListMasters.Where(p => p.PlayListMasterId == playListFolderId).FirstOrDefault();
                if (playlistMaster != null)
                {
                    if (playlistMaster.IsFromMasterLibrary)
                    {
                        return (from mc in dbContext.MasterContents
                                join saveContent in dbContext.PlayListContentMasters on mc.MasterContentId equals saveContent.ContentId
                                where mc.IsPublished == true && !mc.IsDeleted && saveContent.ContentId == mc.MasterContentId && saveContent.PlayListMasterId == playListFolderId && saveContent.PlayListContentMasterId != contentId
                                select new PlaylistContentResponseViewModel()
                                {
                                    PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                    IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                    Content = new ContentViewModel
                                    {
                                        ContentId = mc.MasterContentId,
                                        ContentName = mc.Name,
                                        CreatedDate = mc.CreatedDate,
                                        ContentDescription = mc.Description,
                                        ContentURL = mc.ContentUrl,
                                        ViewCount = mc.ViewCount,
                                        QuizId = dbContext.PublishedPlaylistQuiz.Where(q => !q.IsDeleted && q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().PublishedPlaylistQuizId,
                                        MimeType = mc.MimeType,
                                        IsPublished = mc.IsPublished,
                                        IsAwsContent = true
                                    }

                                }).ToList();
                    }
                    else
                    {
                        return (from cc in dbContext.CompanyLibraryContents
                                join saveContent in dbContext.PlayListContentMasters on cc.CompanyLibraryContentId equals saveContent.ContentId
                                where cc.IsPublished == true && !cc.IsDeleted && saveContent.ContentId == cc.CompanyLibraryContentId && saveContent.PlayListMasterId == playListFolderId && saveContent.PlayListContentMasterId != contentId
                                select new PlaylistContentResponseViewModel()
                                {
                                    PlaylistContentMasterId = saveContent.PlayListContentMasterId,
                                    IsTurnedOffExistingQuiz = saveContent.IsTurnedOffExistingQuiz,
                                    Content = new ContentViewModel
                                    {
                                        ContentId = cc.CompanyLibraryContentId,
                                        ContentName = cc.Name,
                                        CreatedDate = cc.CreatedDate,
                                        ContentDescription = cc.Description,
                                        ContentURL = cc.ContentUrl,
                                        ViewCount = cc.ViewCount,
                                        QuizId = dbContext.PublishedPlaylistQuiz.Where(q => !q.IsDeleted && q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault() == null ? 0 : dbContext.PublishedPlaylistQuiz.Where(q => q.PlaylistContentMasterId == saveContent.PlayListContentMasterId).FirstOrDefault().PublishedPlaylistQuizId,
                                        MimeType = cc.MimeType,
                                        IsPublished = cc.IsPublished,
                                        IsAwsContent = cc.IsAwsContent,
                                        IsYoutubeContent = cc.IsYoutubeContent,
                                        IsFacebookContent = cc.IsFacebookContent
                                    }
                                }).ToList();
                    }
                }
                return null;
            }
        }

        public List<PlaylistUsersViewModel> GetPlaylistUsers(long playListId, long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {

                var result = ((from p in dbContext.PlayLists 
                               join pa in dbContext.PlayListAssignedToes on p.PlayListId equals pa.PlayListId
                              join u in dbContext.Users on pa.UserId equals u.UserId
                              where pa.PlayListId == playListId && u.UserId != userId && !pa.IsAssignedToTeam
                              select new PlaylistUsersViewModel
                              {
                                  UserId = u.UserId,
                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  ProfilePicture = u.ProfilePicture,
                                  UnreadMessageCount = dbContext.Messages.Any(m => m.SenderId == u.UserId && m.ReceiverId == userId && m.PlayListId == playListId && !m.IsMessageRead) == true ? dbContext.Messages.Where(m => m.SenderId == u.UserId && m.ReceiverId == userId && m.PlayListId == playListId && !m.IsMessageRead).Count() : 0 ,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     LastMessageDate = (from m in dbContext.Messages
                                                      where m.PlayListId == playListId && ((m.SenderId == u.UserId && m.ReceiverId == userId) || (m.SenderId == userId && m.ReceiverId == u.UserId))
                                                      select new MessageDate
                                                      {
                                                          TimeOfLastMessage = m.Date
                                                      }).Select(md => md.TimeOfLastMessage).ToList().OrderByDescending(l => l.Value).FirstOrDefault(),
                                  IsPlaylistTrainer = p.CreatedBy == u.UserId
                              }).ToList().Union((from p in dbContext.PlayLists
                                                 join pa in dbContext.PlayListAssignedToes on p.PlayListId equals pa.PlayListId
                                                 join ut in dbContext.PublishedUserTeams on pa.PublishedTeamId equals ut.PublishedTeamId
                                                 join u in dbContext.Users on ut.UserId equals u.UserId
                                                 where pa.PlayListId == playListId && u.UserId != userId && pa.IsAssignedToTeam
                                                 select new PlaylistUsersViewModel
                                                 {
                                                     UserId = u.UserId,
                                                     FirstName = u.FirstName,
                                                     LastName = u.LastName,
                                                     ProfilePicture = u.ProfilePicture,
                                                     UnreadMessageCount = dbContext.Messages.Any(m => m.SenderId == u.UserId && m.ReceiverId == userId && m.PlayListId == playListId && !m.IsMessageRead) == true ? dbContext.Messages.Where(m => m.SenderId == u.UserId && m.PlayListId == playListId && m.ReceiverId == userId && !m.IsMessageRead).Count() : 0 ,
                                                     LastMessageDate = (from m in dbContext.Messages
                                                                        where m.PlayListId == playListId && ((m.SenderId == u.UserId && m.ReceiverId == userId) || (m.SenderId == userId && m.ReceiverId == u.UserId))
                                                                        select new MessageDate
                                                                        {
                                                                            TimeOfLastMessage = m.Date
                                                                        }).Select(md => md.TimeOfLastMessage).ToList().OrderByDescending(l => l.Value).FirstOrDefault(),
                                                     IsPlaylistTrainer = p.CreatedBy == u.UserId
                                                 }).ToList()).Union((from p in dbContext.PlayLists
                                                                     join u in dbContext.Users on p.CreatedBy equals u.UserId
                                                                     where p.PlayListId == playListId && u.UserId != userId
                                                                     select new PlaylistUsersViewModel
                                                                     {
                                                                         UserId = u.UserId,
                                                                         FirstName = u.FirstName,
                                                                         LastName = u.LastName,
                                                                         ProfilePicture = u.ProfilePicture,
                                                                         UnreadMessageCount = dbContext.Messages.Any(m => m.SenderId == u.UserId && m.ReceiverId == userId && m.PlayListId == playListId && !m.IsMessageRead) == true ? dbContext.Messages.Where(m => m.SenderId == u.UserId && m.PlayListId == playListId && m.ReceiverId == userId && !m.IsMessageRead).Count() : 0,
                                                                         LastMessageDate = (from m in dbContext.Messages
                                                                                            where m.PlayListId == playListId && ((m.SenderId == u.UserId && m.ReceiverId == userId) || (m.SenderId == userId && m.ReceiverId == u.UserId))
                                                                                            select new MessageDate
                                                                                            {
                                                                                                TimeOfLastMessage = m.Date
                                                                                            }).Select(md => md.TimeOfLastMessage).ToList().OrderByDescending(l => l.Value).FirstOrDefault(),
                                                                         IsPlaylistTrainer = true
                                                                     }).ToList())).GroupBy(x => x.UserId).Select(x => x.FirstOrDefault()).OrderByDescending(u => u.LastMessageDate).ToList();//.Distinct().OrderByDescending(u => u.LastMessageDate).ToList();

                return result;
            }
        }


        public PlaylistAssociatedUserViewModel AssociatedUsers(long playListId, long teamId, bool isTeam)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                if (isTeam)
                {
                    var result = (from t in dbContext.PublishedTeams
                                  where t.PublishedTeamId == teamId
                                  select new PlaylistAssociatedUserViewModel
                                  {
                                      TeamId = t.PublishedTeamId,
                                      TeamName = t.Name,
                                      TrainerFirstName = dbContext.Users.Where(user => user.UserId == t.TrainerId).FirstOrDefault().FirstName,
                                      TrainerLastName = dbContext.Users.Where(user => user.UserId == t.TrainerId).FirstOrDefault().LastName,
                                      PlaylistUsers = (from ut in dbContext.PublishedUserTeams
                                                       join u in dbContext.Users on ut.UserId equals u.UserId
                                                       where ut.PublishedTeamId == t.PublishedTeamId && !u.IsDeleted
                                                       select new AssociatedUserDetailViewModel
                                                       {
                                                           UserId = u.UserId,
                                                           FirstName = u.FirstName,
                                                           LastName = u.LastName,
                                                           ProfilePicture = u.ProfilePicture,
                                                           FolderResult = (from p in dbContext.PlayLists
                                                                           join pm in dbContext.PlayListMasters on p.PlayListId equals pm.PlayListId
                                                                           // join mf in dbContext.MasterFolders on pm.FolderId equals mf.MasterFolderId
                                                                           where p.PlayListId == playListId/* && pm.IsFromMasterLibrary*/
                                                                           select new PlaylistFolderResultViewModel
                                                                           {
                                                                               PlaylistFolderId = pm.PlayListMasterId,
                                                                               Name = pm.IsFromMasterLibrary ? dbContext.MasterFolders.Where(mf => mf.MasterFolderId == pm.FolderId).FirstOrDefault().Name : dbContext.CompanyLibraryFolders.Where(cf => cf.CompanyLibraryFolderId == pm.FolderId).FirstOrDefault().Name,
                                                                               ShortName = pm.IsFromMasterLibrary ? dbContext.MasterFolders.Where(mf => mf.MasterFolderId == pm.FolderId).FirstOrDefault().ShortName : dbContext.CompanyLibraryFolders.Where(cf => cf.CompanyLibraryFolderId == pm.FolderId).FirstOrDefault().ShortName,
                                                                               MarksObtained = (from pcm in dbContext.PlayListContentMasters
                                                                                                join uqr in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uqr.PlayListContentMasterId
                                                                                                where pcm.PlayListMasterId == pm.PlayListMasterId && uqr.PlaylistId == playListId && uqr.UserId == u.UserId
                                                                                                select uqr.MarksObtained).Sum(),
                                                                               NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pm.PlayListMasterId).ToList().Count(),
                                                                               //completed calculation 
                                                                               QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                                            join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                                            where pcm.PlayListMasterId == pm.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                                            select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                                               QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                                     join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                                     where pc.PlayListMasterId == pm.PlayListMasterId && uq.PlaylistId == playListId && uq.UserId == u.UserId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                                     select uq.QuizId).ToList().Count()
                                                                           }).ToList()

                                                       }).ToList(),
                                  }).FirstOrDefault();
                    return result;
                }
                else
                {
                    IEnumerable<long> userids = (from pa in dbContext.PlayListAssignedToes
                                   join ut in dbContext.PublishedUserTeams on pa.PublishedTeamId equals ut.PublishedTeamId
                                   join u in dbContext.Users on ut.UserId equals u.UserId
                                   where pa.IsAssignedToTeam && !u.IsDeleted && pa.PlayListId == playListId
                                   select u.UserId).ToList().Union((from pa in dbContext.PlayListAssignedToes
                                                                    join u in dbContext.Users on pa.UserId equals u.UserId
                                                                    where !pa.IsAssignedToTeam && !u.IsDeleted && pa.PlayListId == playListId
                                                                    select u.UserId).ToList()).Distinct();

                    var playlistUsers = (from u in dbContext.Users 
                                     where !u.IsDeleted && userids.Contains(u.UserId)
                                     select new AssociatedUserDetailViewModel
                                     {
                                         UserId = u.UserId,
                                         FirstName = u.FirstName,
                                         LastName = u.LastName,
                                         ProfilePicture = u.ProfilePicture,
                                         FolderResult = (from p in dbContext.PlayLists
                                                         join pm in dbContext.PlayListMasters on p.PlayListId equals pm.PlayListId
                                                         // join mf in dbContext.MasterFolders on pm.FolderId equals mf.MasterFolderId
                                                         where p.PlayListId == playListId/* && pm.IsFromMasterLibrary*/
                                                         select new PlaylistFolderResultViewModel
                                                         {
                                                             PlaylistFolderId = pm.PlayListMasterId,
                                                             Name = pm.IsFromMasterLibrary ? dbContext.MasterFolders.Where(mf => mf.MasterFolderId == pm.FolderId).FirstOrDefault().Name : dbContext.CompanyLibraryFolders.Where(cf => cf.CompanyLibraryFolderId == pm.FolderId).FirstOrDefault().Name,
                                                             ShortName = pm.IsFromMasterLibrary ? dbContext.MasterFolders.Where(mf => mf.MasterFolderId == pm.FolderId).FirstOrDefault().ShortName : dbContext.CompanyLibraryFolders.Where(cf => cf.CompanyLibraryFolderId == pm.FolderId).FirstOrDefault().ShortName,
                                                             MarksObtained = (from pcm in dbContext.PlayListContentMasters
                                                                              join uqr in dbContext.UserQuizResults on pcm.PlayListContentMasterId equals uqr.PlayListContentMasterId
                                                                              where pcm.PlayListMasterId == pm.PlayListMasterId && uqr.PlaylistId == playListId && uqr.UserId == u.UserId
                                                                              select uqr.MarksObtained).Sum(),
                                                             NumberOfContent = dbContext.PlayListContentMasters.Where(c => !c.IsDeleted && c.PlayListMasterId == pm.PlayListMasterId).ToList().Count(),
                                                             //completed calculation
                                                             QuizCount = (from pcm in dbContext.PlayListContentMasters
                                                                          join pq in dbContext.PublishedPlaylistQuiz on pcm.PlayListContentMasterId equals pq.PlaylistContentMasterId
                                                                          where pcm.PlayListMasterId == pm.PlayListMasterId && pq.PublishedPlaylistQuestions.Where(q => !q.IsDeleted).Count() > 0
                                                                          select pq.PublishedPlaylistQuizId).ToList().Count(),
                                                             QuizCompletedCount = (from pc in dbContext.PlayListContentMasters
                                                                                   join uq in dbContext.UserQuizResults on pc.PlayListContentMasterId equals uq.PlayListContentMasterId
                                                                                   where pc.PlayListMasterId == pm.PlayListMasterId && uq.PlaylistId == playListId && uq.UserId == u.UserId && (uq.IsPassed || uq.NumberOfAttempt == 3)
                                                                                   select uq.QuizId).ToList().Count()
                                                         }).ToList()

                                     }).ToList();

                    return new PlaylistAssociatedUserViewModel { PlaylistUsers = playlistUsers };
                }
            }
        }


        public List<AssociatedTeamListViewModel> GetPlaylistAssociatedTeams(long playListId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                return (from pa in dbContext.PlayListAssignedToes
                        join t in dbContext.PublishedTeams on pa.PublishedTeamId equals t.PublishedTeamId
                        where !t.IsDeleted && pa.IsAssignedToTeam && pa.PlayListId == playListId
                        select new AssociatedTeamListViewModel
                        {
                            TeamId = t.PublishedTeamId,
                            Name = t.Name
                        }).ToList();
            }
        }


        public List<PlaylistListViewModel> GetMyDashboardPlaylist(long userId)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                var result = (from p in dbContext.PlayLists
                              where p.IsLaunched && p.CreatedBy == userId
                              select new PlaylistListViewModel
                              {
                                  PlaylistId = p.PlayListId,
                                  Name = p.Name,
                                  StartDate = p.StartDate,
                                  EndDate = p.EndDate,
                                  NumberOfUsers = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && !pa.IsAssignedToTeam).ToList().Count(),
                                  NumberOfTeams = dbContext.PlayListAssignedToes.Where(pa => pa.PlayListId == p.PlayListId && pa.IsAssignedToTeam).ToList().Count(),
                                  Status = p.IsLaunched,
                                  IsExpired = p.IsCompleted,
                                  NumberOfUserCompletedPlaylist = (from ups in dbContext.UserPlayListStatus
                                                                  where ups.PlaylistId == p.PlayListId && ups.IsComplete
                                                                  select ups.PlaylistId).Count(),
                                  NumberOfStudents = ((from pa in dbContext.PlayListAssignedToes
                                                      join ut in dbContext.PublishedUserTeams on pa.PublishedTeamId equals ut.PublishedTeamId
                                                      where pa.IsAssignedToTeam && pa.PlayListId == p.PlayListId
                                                      select ut.UserId).Union((from pa in dbContext.PlayListAssignedToes
                                                                                     where !pa.IsAssignedToTeam && pa.PlayListId == p.PlayListId
                                                                                     select pa.UserId).Where(i => i.HasValue).Select(j => j.Value))).Distinct().Count(),

                                  AssignedTo = (from t in dbContext.PublishedTeams
                                                join pa in dbContext.PlayListAssignedToes on t.PublishedTeamId equals pa.PublishedTeamId
                                                where !t.IsDeleted && pa.IsAssignedToTeam && pa.PlayListId == p.PlayListId
                                                select t.Name).ToList().Union((from us in dbContext.Users
                                                                               join pa in dbContext.PlayListAssignedToes on us.UserId equals pa.UserId
                                                                               where !us.IsDeleted && !pa.IsAssignedToTeam && pa.PlayListId == p.PlayListId
                                                                               select us.FirstName).ToList()).ToList(),
            }).ToList();

                return result;

            }
        }

        /// <summary>
        /// method to delete playlist name and description
        /// </summary>
        /// <param name="playlist"></param>
        public void DeletePlaylist(PlaylistViewModel playlist)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                PlayList existingPlaylist = dbContext.PlayLists.Where(p => !p.IsDeleted && p.CreatedBy == playlist.UserId && p.PlayListId == playlist.PlayListId).FirstOrDefault();
                if (existingPlaylist != null)
                {
                    existingPlaylist.IsDeleted = true;
                    dbContext.SaveChanges();
                }
            }
        }

        public long CloneMyCreatedPlaylist(PlaylistViewModel playlist)
        {
            using (VirtualBrandWorksEntities dbContext = new VirtualBrandWorksEntities())
            {
                long newClonedPlaylistId = ClonePlaylist(dbContext, playlist.PlayListId, playlist.UserId);
                return newClonedPlaylistId;
            }
        }

        //private method

        //private long ClonePlaylist(VirtualBrandWorksEntities dbContext, long playListId, long userId)
        //{
        //    var query = "Exec ClonePlaylist @oldPLId, @userId";
        //    //var newId = new SqlParameter
        //    //{
        //    //    ParameterName = "newId",
        //    //    DbType = DbType.Int64,
        //    //    Direction = ParameterDirection.Output
        //    //};
        //    var parameters = new List<SqlParameter> {
        //                new SqlParameter("@oldPLId", playListId),
        //                new SqlParameter("@userId", userId)
        //            };

        //    var response = dbContext.Database.SqlQuery<object>(query, parameters.ToArray()).ToList();
        //    dbContext.SaveChanges();
        //    return 0; //response.FirstOrDefault();
        //}

        private long ClonePlaylist(VirtualBrandWorksEntities dbContext, long playListId, long userId)
        {

            using (SqlConnection conn = new SqlConnection(dbContext.Database.Connection.ConnectionString))
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }

                conn.Open();

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("ClonePlayList", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@oldPLId", playListId));
                cmd.Parameters.Add(new SqlParameter("@userId", userId));

                // execute the command
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    // iterate through results, printing each to console
                    while (rdr.Read())
                    {
                        return Convert.ToInt64(rdr["Id"]);
                    }
                }
            }
            return 0;
        }
    }
}
