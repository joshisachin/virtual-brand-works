﻿using Newtonsoft.Json;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork.Notification
{
    public abstract class NotificationSender
    {
        private Uri _fcmPushNotificationsURL = new Uri(AppUtil.FcmPushNotificationsURL);
        private string _fcmServerKey = AppUtil.FcmServerKey;
        private string _senderId = AppUtil.FcmSenderId;

        public abstract List<FcmResponse> SendPushNotification(List<FCMUserToken> objFCMTokens, string title, string body);
        internal string SendPushNotification(string token,string title, string body)
        {

            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

            tRequest.Method = "post";

            tRequest.ContentType = "application/json";

            string msg = "{\"notification\":{\"title\":\""+ title + "\",\"body\":\""+ body + "\"},\"to\":\""+ token + "\"}";

            Byte[] byteArray = Encoding.UTF8.GetBytes(msg);

            tRequest.Headers.Add(string.Format("Authorization: key={0}", _fcmServerKey));

            tRequest.Headers.Add(string.Format("Sender: id={0}", _senderId));

            tRequest.ContentLength = byteArray.Length;


            using (Stream dataStream = tRequest.GetRequestStream())
            {

                dataStream.Write(byteArray, 0, byteArray.Length);


                using (WebResponse tResponse = tRequest.GetResponse())
                {

                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {

                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {

                            String sResponseFromServer = tReader.ReadToEnd();

                            return sResponseFromServer;

                        }
                    }
                }
            }

            //string jsonMessage = JsonConvert.SerializeObject(message);
            //var request = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send");

            //request.Headers.TryAddWithoutValidation("Authorization", "key=" + _fcmServerKey);
            //request.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");

            //HttpResponseMessage result;
            //using (var client = new HttpClient())
            //{
            //    result = await client.Send(request);
            //    return await result.Content.ReadAsStringAsync();
            //}

        }
    }
}