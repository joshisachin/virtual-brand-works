﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Notification.Interfaces;

namespace VirtualBrandWork.Notification
{
    public abstract class EmailSender : IEmailService
    {
        public abstract void SendResetPasswordCode(string userName,string emailID, string activationCode, string emailFor);
        public abstract void SendWriteToUs(string sendFrom, string message);
        public abstract void SendActiveDeactiveNotification(string updateMessage,string AdminEmail, string companyName, bool flag);
        public abstract void SendUserInviteNotification(string email, string encodedEmail, string userName);
        public abstract void SendPayment(Dictionary<string, string> data, string email, string message, string subject, bool flag);
        public abstract void SendPrizeAvailedNotification(AvailPrizeResponseViewModel availedPrize);
        public abstract void SendUpdatePaymentInfoEmail(UserMyPlanNotiFicationViewModel userInfo);
        public abstract void SendSubscriptionUpdatedNotificationEmail(UserMyPlanNotiFicationViewModel userInfo);
        public abstract void SendSubscriptionScheduledToCancelEmail(UserMyPlanNotiFicationViewModel userInfo);
        public abstract void SendUpdatePaymentInfoFailureEmail(UserMyPlanNotiFicationViewModel userInfo);
        public abstract void SendSubscriptionDetailsUpdatedNotificationEmail(UserMyPlanNotiFicationViewModel userInfo, bool isMonthly);

        public void SendEmail(string emailID, string subject, string body)
        {
            var fromEmail = new MailAddress(AppUtil.EmailId, AppUtil.CompanyName);
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = AppUtil.EmailPassword;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        public void SendEmailToMultipleRecipient(List<string> emailID, string subject, string body)

        {
            var fromEmail = new MailAddress(AppUtil.EmailId, AppUtil.CompanyName);
            var fromEmailPassword = AppUtil.EmailPassword;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            //create the mail message
            MailMessage mail = new MailMessage();

            mail.From = fromEmail;
            emailID.ForEach(e =>
            {
                mail.To.Add(e);
            });

            //set the content
            mail.Subject = subject;
            mail.Body = body;
            mail.SubjectEncoding = System.Text.Encoding.Default;
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;

            smtp.Send(mail);
        }
    }
}