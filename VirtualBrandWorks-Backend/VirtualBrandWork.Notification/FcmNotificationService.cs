﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Notification.Interfaces;
using static VirtualBrandWork.Entity.Constants;
using VirtualBrandWork.Entity;
using System.Threading.Tasks;
using Repository.Model;

namespace VirtualBrandWork.Notification
{
    public class FcmNotificationService : NotificationSender, IFcmNotificationService
    {
        public override List<FcmResponse> SendPushNotification(List<FCMUserToken> objFCMTokens, string title, string body)
        {
            List<FcmResponse> objFCMResponse = new List<FcmResponse>();

            if (objFCMTokens.Count() > 0)
            {
                foreach (var tokenData in objFCMTokens)
                {
                    string message = GetNotificationMessage(tokenData.TokenId, title, body);
                    string response = SendPushNotification(tokenData.TokenId, title, body);
                    FcmResponse fcmResponse = JsonConvert.DeserializeObject<FcmResponse>(response);
                    fcmResponse.UserId = tokenData.UserId;
                    objFCMResponse.Add(fcmResponse);
                }
            }
            return objFCMResponse;
        }

        private string GetNotificationMessage(string token, string title, string body)
        {
            FcmMessage messageInformation = new FcmMessage()
            {
                Notification = new FcmNotification()
                {
                    Title = title,
                    Body = body
                },
                //Data = data,
                Tokens = new string[] { token }
            };
            return JsonConvert.SerializeObject(messageInformation);
        }
    }
}