﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Notification.Interfaces;

namespace VirtualBrandWork.Notification
{
    public class EmailService : EmailSender, IEmailService
    {
        public override void SendResetPasswordCode(string userName, string emailID, string activationCode, string emailFor = "ResetPassword")
        {
            string subject = "";
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))

            {

                body = reader.ReadToEnd();

            }
            StringBuilder content = new StringBuilder();
            if (emailFor == "ResetPassword")
            {
                subject = "Password Reset";
                content.AppendFormat("<p>We received a request to reset the password on your account.</p>");
                content.AppendFormat("<b>{0}</b>", activationCode);
                content.AppendFormat("<p>Enter this code to complete the reset.</p>");
                content.AppendFormat("<p>This OTP is valid for 10 minutes or 1 successful attempt whichever is earlier.</p>");
                content.AppendFormat("<p>Thanks for helping us keep your account secure.</p>");
            }
            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{Content}", content.ToString());
            SendEmail(emailID, subject, body);
        }
        public override void SendWriteToUs(string email, string message)
        {
            string subject = "Write To Us";
            string body = $"Email Id :{email}<br/>Message : {message}";
            string sendTo = ConfigurationManager.AppSettings["EmailId"];
            SendEmail(sendTo, subject, body);
        }
        public override void SendActiveDeactiveNotification(string updateMessage, string AdminEmail, string companyName, bool flag)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            string subject = string.Empty;
            if (flag)
            {
                subject = "Your Account is Active now";
            }
            else
            {
                subject = "Your account has been deactivated";
            }
            StringBuilder content = new StringBuilder();
            content.AppendFormat("<p>{0}</p>", updateMessage);
            if (!flag)
            {
                content.AppendFormat("<p>Please contact the Virtual Brandworks team for further details.</p>");
            }
            content.AppendFormat("<p>Thanks</p>");
            //content.AppendFormat("<p><b>Virtual Brandworks Team<b></p>");
            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", companyName);
            body = body.Replace("{Content}", content.ToString());
            string emailId = AdminEmail;
            SendEmail(emailId, subject, body);
        }
        public override void SendUserInviteNotification(string email, string encodedEmail, string userName)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            string subject = "Sign Up Now!";
            StringBuilder content = new StringBuilder();
            content.AppendFormat("<p>You have been invited by your company admin to register and start your journey of learning and sharing.</p>");
            content.AppendFormat("<p>Sign up now to start learning and creating your own content.</p>");
            //link for signupurl for specified email
            var link = AppUtil.SignUpUrl + encodedEmail;
            content.Append("<p>");
            content.AppendFormat("<a href=\"{0}\">" + link + "</a>", link);
            content.Append("</p>");
            content.AppendFormat("<p>Thanks</p>");
            string emailId = email;
            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Dear");
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{Content}", content.ToString());
            SendEmail(emailId, subject, body);
        }
        public override void SendPayment(Dictionary<string, string> data, string email, string message, string subject, bool flag)
        {
            string body = string.Empty; string userName = string.Empty;
            if (flag)
            {
                string SubscriptionPlan = string.Empty; decimal amount = 0; int billingCycle = 0; string firstName = string.Empty;
                string lastName = string.Empty;
                ////Get email template values
                foreach (KeyValuePair<string, string> metadata in data)
                {
                    if (metadata.Key.Equals("subscription", StringComparison.OrdinalIgnoreCase))
                    {
                        SubscriptionPlan = metadata.Value.ToString();
                    }
                    else if (metadata.Key.Equals("amount", StringComparison.OrdinalIgnoreCase))
                    {
                        amount = Convert.ToDecimal(metadata.Value.ToString());
                    }
                    else if (metadata.Key.Equals("billingCycle", StringComparison.OrdinalIgnoreCase))
                    {
                        billingCycle = Convert.ToInt32(metadata.Value.ToString());
                    }
                    else if (metadata.Key.Equals("FirstName", StringComparison.OrdinalIgnoreCase))
                    {
                        firstName = metadata.Value.ToString();
                    }
                    else if (metadata.Key.Equals("LastName", StringComparison.OrdinalIgnoreCase))
                    {
                        lastName = metadata.Value.ToString();
                    }
                }

                userName = $"{firstName} {lastName}";
                //using streamreader for reading my htmltemplate   
                using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/PaymentSuccessEmailTemplate.cshtml")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
                //body = body.Replace("{Greeting}", "Dear");
                body = body.Replace("{UserName}", userName);
                body = body.Replace("{SubscriptionPlan}", SubscriptionPlan);
                string date = DateTime.Now.ToString("dd-MMM-yyyy");
                DateTime dateTime = DateTime.ParseExact(date, "dd-MMM-yyyy", null);
                body = body.Replace("{StartDate}", date);
                body = body.Replace("{Currency}", "$");
                if (billingCycle != 2)
                {
                    amount = amount / 12;
                }
                body = body.Replace("{Amount}", amount.ToString());

                var planType = "-";
                if (billingCycle != 0)
                {
                    planType = billingCycle == 2 ? Constants.PlanType.Monthly.ToString() : "Annually";
                }
                body = body.Replace("{BillingCycle}", planType);
            }
            else
            {
                //using streamreader for reading my htmltemplate   
                using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
                {
                    body = reader.ReadToEnd();
                }
                StringBuilder content = new StringBuilder();
                content.AppendFormat("<p>" + message + "</p>");
                var homePageUrl = "/Home/Index";
                var link = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, homePageUrl);
                //getting "localhost" instead of Ip , the next line is used to replace the localhost 
                link = link.Replace("localhost", "44.232.180.17");
                content.Append("<p>Please visit ");
                content.AppendFormat("<a href=\"{0}\">" + link + "</a>", link);
                content.Append(" to sign up again.</p>");
                content.AppendFormat("<p>Thanks</p>");
                body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
                body = body.Replace("{Greeting}", "Hello");
                body = body.Replace("{UserName}", userName);
                body = body.Replace("{Content}", content.ToString());
            }
            string emailId = email;
            SendEmail(emailId, subject, body);
        }

        public override void SendPrizeAvailedNotification(AvailPrizeResponseViewModel availedPrize)
        {
            string subject = "";
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            StringBuilder content = new StringBuilder();
            switch (availedPrize.PrizeCategoryId)
            {
                case (int)Constants.PrizeCategory.TimeOffPrize:
                    subject = "You availed a Time Off Prize";
                    content.AppendFormat("<p>Congratulations you have successfully availed a {0} Time Off Prize</p>", availedPrize.TimeOffDuration);
                    content.AppendFormat("<p>Following is the redeem code :</p>");
                    content.AppendFormat("<b>{0}</b>", availedPrize.AvailedRedeemCode);
                    if (!string.IsNullOrEmpty(availedPrize.Remarks))
                    {
                        content.AppendFormat("<br/><br/><br/><p><b>Note</b> : {0}</p>", availedPrize.Remarks);
                    }
                    content.AppendFormat("<p>Thanks.</p>");
                    break;
                case (int)Constants.PrizeCategory.Voucher:
                    subject = "You availed a Voucher";
                    content.AppendFormat("<p>Congratulations you have successfully availed a {0} Voucher</p>", availedPrize.BrandName);
                    content.AppendFormat("<p>Following is the redeem code :</p>");
                    content.AppendFormat("<b>{0}</b>", availedPrize.AvailedRedeemCode);
                    if (!string.IsNullOrEmpty(availedPrize.Remarks))
                    {
                        content.AppendFormat("<br/><br/><br/><p><b>Note</b> : {0}</p>", availedPrize.Remarks);
                    }
                    content.AppendFormat("<p>Thanks.</p>");
                    break;
                case (int)Constants.PrizeCategory.GiftCard:
                    subject = "You availed a Giftcard Prize";
                    content.AppendFormat("<p>Congratulations you have successfully availed a {0} Giftcard</p>", availedPrize.BrandName);
                    content.AppendFormat("<p>Following is the redeem code :</p>");
                    content.AppendFormat("<b>{0}</b>", availedPrize.AvailedRedeemCode);
                    if (!string.IsNullOrEmpty(availedPrize.Remarks))
                    {
                        content.AppendFormat("<br/><br/><br/><p><b>Note</b> : {0}</p>", availedPrize.Remarks);
                    }
                    content.AppendFormat("<p>Thanks.</p>");
                    break;
                case (int)Constants.PrizeCategory.InHouseFoodCertification:
                    subject = "You availed a In House Food Certification Prize";
                    content.AppendFormat("<p>Congratulations you have successfully availed a In House Food Certification</p>");
                    content.AppendFormat("<p>Following is the redeem code :</p>");
                    content.AppendFormat("<b>{0}</b>", availedPrize.AvailedRedeemCode);
                    if (!string.IsNullOrEmpty(availedPrize.Remarks))
                    {
                        content.AppendFormat("<br/><br/><br/><p><b>Note</b> : {0}</p>", availedPrize.Remarks);
                    }
                    content.AppendFormat("<p>Thanks.</p>");
                    break;
                case (int)Constants.PrizeCategory.CashPrize:
                    subject = "You availed a Cash Prize";
                    content.AppendFormat("<p>Congratulations you have successfully availed a <b>${0}</b> Cash Prize</p>", availedPrize.CashPrizeAmount);
                    content.AppendFormat("<p>Following is the redeem code :</p>");
                    content.AppendFormat("<b>{0}</b>", availedPrize.AvailedRedeemCode);
                    if (!string.IsNullOrEmpty(availedPrize.Remarks))
                    {
                        content.AppendFormat("<br/><br/><br/><p><b>Note</b> : {0}</p>", availedPrize.Remarks);
                    }
                    content.AppendFormat("<p>Thanks.</p>");
                    break;
                default:
                    break;
            }

            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", availedPrize.User.FirstName);
            body = body.Replace("{Content}", content.ToString());
            SendEmail(availedPrize.User.EmailId, subject, body);
        }

        public override void SendUpdatePaymentInfoEmail(UserMyPlanNotiFicationViewModel userInfo)
        {
            string subject = "Payment Info Updated";
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            StringBuilder content = new StringBuilder();
            content.AppendFormat("<p>You have successfully updated your payment info</p>");
            content.AppendFormat("<br/><p>Old card : <b>XXXXXXXXXXXXX{0}</b></p>", userInfo.OldLast4Digit);
            content.AppendFormat("<br/><p>New card : <b>XXXXXXXXXXXXX{0}</b></p>", userInfo.NewLast4Digit);
            content.AppendFormat("<p>Thanks.</p>");

            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", userInfo.UserName);
            body = body.Replace("{Content}", content.ToString());
            SendEmail(userInfo.Email, subject, body);
        }

        //Change subscription plan notification email
        public override void SendSubscriptionUpdatedNotificationEmail(UserMyPlanNotiFicationViewModel userInfo)
        {
            string subject = "Subscription Plan Updated";
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            StringBuilder content = new StringBuilder();
            content.AppendFormat("<p>You have successfully updated your subscription plan!</p>");
            content.AppendFormat("<p>Plan changed From <b>{0}</b> To <b>{1}</b></p>", userInfo.OldPlanName, userInfo.PlanName);
            content.AppendFormat("<p>New plan allows access to {0} number of users.</p>", userInfo.NumberOfAllowedUsers);
            content.AppendFormat("<p>You have a <b>{0}</b> subscription and it costs <b>${1}</b></p>", userInfo.PlanType, userInfo.Price);
            content.AppendFormat("<p>Thanks.</p>");

            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", userInfo.UserName);
            body = body.Replace("{Content}", content.ToString());
            SendEmail(userInfo.Email, subject, body);
        }

        //subscription scheduled to cancel notification
        public override void SendSubscriptionScheduledToCancelEmail(UserMyPlanNotiFicationViewModel userInfo)
        {
            string subject = "Subscription scheduled to cancel";
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            StringBuilder content = new StringBuilder();
            content.AppendFormat("<p>Your subscription plan : <b>{0}</b> is scheduled to cancel on <b>{1}</b></p>", userInfo.PlanName, userInfo.CancelAt.ToString("dd/MM/yyyy"));
           // content.AppendFormat("<p>is scheduled to cancel on : <b>{0}</b></p>", userInfo.CancelAt.ToString("dd/MM/yyyy"));
            content.AppendFormat("<p>Thanks.</p>");

            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", userInfo.UserName);
            body = body.Replace("{Content}", content.ToString());
            SendEmail(userInfo.Email, subject, body);
        }

        //
        public override void SendUpdatePaymentInfoFailureEmail(UserMyPlanNotiFicationViewModel userInfo)
        {
            string subject = "Payment info could not be updated";
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            StringBuilder content = new StringBuilder();
            content.AppendFormat("<p>Your attempt to update payment info failed.</p>");
            content.AppendFormat("<p>Thanks.</p>");

            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", userInfo.UserName);
            body = body.Replace("{Content}", content.ToString());
            SendEmail(userInfo.Email, subject, body);
        }

        //subscription plan updated notification email
        public override void SendSubscriptionDetailsUpdatedNotificationEmail(UserMyPlanNotiFicationViewModel userInfo, bool isMonthly)
        {
            string subject = "Subscription details changed";
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailTemplate.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            StringBuilder content = new StringBuilder();
            content.AppendFormat("<p>Your subscription plan details has been updated!</p>");
            content.AppendFormat("<p>Following changes were applied :</p>");
            if (userInfo.IsPlanNameChanged)
            {
                content.AppendFormat("<p>Plan name changed from <b>{0}</b> to <b>{1}</b></p>", userInfo.OldPlanName, userInfo.PlanName);
            }
            if (userInfo.IsPlanDescriptionChanged)
            {
                content.AppendFormat("<p>Plan description changed from <b>{0}</b> to <b>{1}</b></p>", userInfo.OldPlanDescription, userInfo.PlanDescription);
            }
            if (userInfo.IsNumberOfUsersChanged)
            {
                content.AppendFormat("<p>Number of allowed users changed from <b>{0}</b> to <b>{1}</b></p>", userInfo.OldNumberOfAllowedUsers, userInfo.NumberOfAllowedUsers);
            }
            if (isMonthly && userInfo.IsMonthlyCostChanged)
            {
                content.AppendFormat("<p>Monthly cost changed from <b>{0}</b> to <b>{1}</b></p>", userInfo.OldMonthlyCost, userInfo.NewMonthlyCost);
            }
            if (!isMonthly && userInfo.IsAnnualCostChanged)
            {
                content.AppendFormat("<p>Yearly cost changed from <b>{0}</b> to <b>{1}</b></p>", userInfo.OldAnnualCost, userInfo.NewAnnualCost);
            }
            content.AppendFormat("<p>Thanks.</p>");

            body = body.Replace("{BaseUrl}", ConfigurationManager.AppSettings["BaseUrl"].ToString());
            body = body.Replace("{Greeting}", "Hi");
            body = body.Replace("{UserName}", "Subscriber");
            body = body.Replace("{Content}", content.ToString());
            if (isMonthly)
            {
                List<string> toEmailIds = new List<string>();
                userInfo.UsersForMonthlySubscription.ForEach(u => { toEmailIds.Add(u.EmailId); });
                if(toEmailIds.Count > 0)
                {
                    SendEmailToMultipleRecipient(toEmailIds, subject, body);
                }
            }
            else
            {
                List<string> toEmailIds = new List<string>();
                userInfo.UsersForYearlySubscription.ForEach(u => { toEmailIds.Add(u.EmailId); });
                if (toEmailIds.Count > 0)
                {
                    SendEmailToMultipleRecipient(toEmailIds, subject, body);
                }
            }

        }
    }
}