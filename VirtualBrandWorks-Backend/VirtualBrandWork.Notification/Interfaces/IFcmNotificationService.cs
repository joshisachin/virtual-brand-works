﻿using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork.Notification.Interfaces
{
   public interface IFcmNotificationService
    {
        List<FcmResponse> SendPushNotification(List<FCMUserToken> objFCMTokens, string title, string body);
    }
}
