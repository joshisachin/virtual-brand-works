﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace VirtualBrandWork.Notification.Interfaces
{
   public interface IEmailService
    {
        void SendResetPasswordCode(string userName, string emailID, string activationCode, string emailFor = "VerifyAccount");
        void SendWriteToUs(string sendFrom, string message);
        void SendActiveDeactiveNotification(string updateMessage, string AdminEmail, string companyName, bool flag);
        void SendUserInviteNotification(string email, string encodedEmail, string userName);
        void SendPayment(Dictionary<string, string> data, string email, string message, string subject, bool flag);
        void SendPrizeAvailedNotification(AvailPrizeResponseViewModel availedPrize);
        void SendUpdatePaymentInfoEmail(UserMyPlanNotiFicationViewModel userInfo);
        void SendSubscriptionUpdatedNotificationEmail(UserMyPlanNotiFicationViewModel userInfo);
        void SendSubscriptionScheduledToCancelEmail(UserMyPlanNotiFicationViewModel userInfo);
        void SendUpdatePaymentInfoFailureEmail(UserMyPlanNotiFicationViewModel userInfo);
        void SendSubscriptionDetailsUpdatedNotificationEmail(UserMyPlanNotiFicationViewModel userInfo, bool isMonthly);
    }
}
