﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class TagService : ITagService
    {
        private readonly ITagRepository _tagRepository;

        public TagService(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }
        public List<TagViewModel> GetTags()
        {
            return _tagRepository.GetTags();
        }

        public void Create(TagViewModel tag)
        {
            if(tag != null)
            {
                _tagRepository.Create(tag);
            }
        }

        public bool IsTagExists(TagViewModel tag)
        {
            return _tagRepository.IsTagExists(tag);
        }

        public List<TagViewModel> GetTagSuggestion(string type, string suggestion, long userId)
        {
            return _tagRepository.GetTagSuggestion(type, suggestion, userId);
        }

        public List<TagViewModel> GetTagsWithIndustries()
        {
            return _tagRepository.GetTagsWithIndustries();
        }

        public void EditTag(TagViewModel tagViewModel)
        {
             _tagRepository.EditTag(tagViewModel);
        }

        public TagViewModel GetTagById(long tagId)
        {
            return _tagRepository.GetTagById(tagId);
        }
        
        public void Delete(long tagId)
        {
            _tagRepository.Delete(tagId);
        }
    }
}
