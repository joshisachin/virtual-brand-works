﻿using Service.Interfaces;
using Repository.Interfaces;
using System;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class QuizService : IQuizService, IDisposable
    {
        private readonly IQuizRepository _quizRepository;
        public QuizService(IQuizRepository quizRepository)
        {
            _quizRepository = quizRepository;
        }
        public Response AddQuestion(QuestionAnswerViewModel questionAnswerViewModel)
        {
            return _quizRepository.AddQuestion(questionAnswerViewModel);
        }
        public Response SaveQuiz(QuizViewModel quizVirtualModel)
        {
            return _quizRepository.SaveQuiz(quizVirtualModel);
        }
        public QuizViewModel GetQuizDetails(long quizId)
        {
            return _quizRepository.GetQuizDetails(quizId);
        }
        public QuestionAnswerViewModel GetQuestionDetails(long questionId)
        {
            return _quizRepository.GetQuestionDetails(questionId);
        }
        public Response UpdateQuestion(QuestionAnswerViewModel questionAnswerViewModel)
        {
            return _quizRepository.UpdateQuestion(questionAnswerViewModel);
        }

        public Response DeleteQuiz(long quizId)
        {
            return _quizRepository.DeleteQuiz(quizId);
        }

        public Response DeleteQuestion(long questionId)
        {
            return _quizRepository.DeleteQuestion(questionId);
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

    }
}
