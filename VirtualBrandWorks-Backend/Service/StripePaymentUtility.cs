﻿using Stripe;
using Stripe.Checkout;
using System.Collections.Generic;
using System.Configuration;
using Service.Interfaces;
using VirtualBrandWork.Entity.ViewModels;
using System;

namespace Service
{
    public class StripePaymentUtility : IStripePaymentUtility
    {
        private readonly string _stripePublishKey = ConfigurationManager.AppSettings["StripePublishKey"];
        private readonly string _stripeSecretApiKey = ConfigurationManager.AppSettings["StripeSecretApiKey"];

        /// <summary>
        /// /* Get StripeSetupModel with Stripe publishable key and plan Id to initialize Stripe.js */
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public StripeSetupModel Setup(string planId)
        {
            StripeSetupModel setupModel = new StripeSetupModel() { PublishableKey = _stripePublishKey, PlanId = planId };
            return setupModel;
        }

        /// <summary>
        /// // Create a Checkout Session with the selected plan ID
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public StripeCheckoutSessionModel GetCreateCheckoutSession(string planId, string email, string subscription, string amount, string billingCycle, string firstName, string lastName, string successUrl, string cancelUrl)//, string tax)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string> {
                    "card",
                },
                CustomerEmail = email,
                Mode = "subscription",
                SuccessUrl = successUrl,
                CancelUrl = cancelUrl,
                LineItems = new List<SessionLineItemOptions>() { new SessionLineItemOptions() { Price = planId, Quantity = 1 } },//, TaxRates = new List<string> { tax } } },
                Metadata = new Dictionary<string, string>
                    {
                        {"Subscription", subscription },
                        {"Amount", amount},
                        {"BillingCycle", billingCycle },
                        {"FirstName", firstName },
                        {"LastName", lastName }
                    }

            };

            var service = new SessionService();
            Stripe.Checkout.Session session = service.Create(options);
            return new StripeCheckoutSessionModel() { SessionId = session.Id };
        }

        /// <summary>
        /// Create Product
        /// </summary>
        public Product CreateProduct(string name, string description)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var options = new ProductCreateOptions
            {
                Name = name,
                Description = description
            };

            var service = new ProductService();
            return service.Create(options);
        }

        /// <summary>
        /// Create Product
        /// </summary>
        public Product UpdateProduct(string name, string description, string productId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var options = new ProductUpdateOptions
            {
                Name = name,
                Description = description
            };

            var service = new ProductService();
            return service.Update(productId, options);
        }

        /// <summary>
        /// Create Plan
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <param name="interval"></param>
        /// <param name="productId"></param>
        public Plan CreatePlan(long amount, string currency, string interval, string productId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var options = new PlanCreateOptions
            {
                Amount = amount * 100,
                Currency = currency,
                Interval = interval,
                Product = productId
            };
            var service = new PlanService();
            return service.Create(options);
        }


        /// <summary>
        /// Delete Plan
        /// </summary>
        /// <param name="planId"></param>
        public Plan DeletePlan(string planId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new PlanService();
            return service.Delete(planId);
        }

        /// <summary>
        /// Delete Product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Product DeleteProduct(string productId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new ProductService();
            return service.Delete(productId);
        }

        public Session GetCheckoutSession(string sid)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new SessionService();
            return service.Get(sid);
        }

        /// <summary>
        /// Get list of payment methods
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public StripeList<PaymentMethod> PaymentMethods(string customerId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;

            var options = new PaymentMethodListOptions
            {
                Customer = customerId,
                Type = "card",
            };
            var service = new PaymentMethodService();
            StripeList<PaymentMethod> paymentMethods = service.List(
              options
            );
            return paymentMethods;
        }

        /// <summary>
        /// Detach payment method
        /// </summary>
        /// <param name="paymentMethod"></param>
        /// <returns></returns>
        public PaymentMethod DetachPaymethod(string paymentMethodId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new PaymentMethodService();
            return service.Detach(paymentMethodId);
        }

        /// <summary>
        /// Update stripe subscription based on new plan Id
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="planId"></param>
        /// <returns></returns>
        public Subscription UpdateSubscription(string subscriptionId, string planId, bool isTrial)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new Stripe.SubscriptionService();
            Subscription subscription = service.Get(subscriptionId);

            var items = new List<SubscriptionItemOptions> {
                new SubscriptionItemOptions
                {
                    Id = subscription.Items.Data[0].Id,
                    Price = planId,
                },
            };

            var options = new SubscriptionUpdateOptions
            {
                CancelAtPeriodEnd = false,
                Items = items,
               // TrialEnd = subscription.CurrentPeriodEnd,
               // ProrationBehavior = "",
                //BillingCycleAnchor = SubscriptionBillingCycleAnchor.Unchanged,
            };
            if (isTrial)
            {
                options.ProrationBehavior = "none";
            }
            return service.Update(subscriptionId, options);
        }

        /// <summary>
        /// cancel subscription based on subscription Id
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <returns></returns>
        public Subscription CancelSubscription(string subscriptionId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;

            var service = new Stripe.SubscriptionService();
            var options = new SubscriptionCancelOptions
            {
                InvoiceNow = false,
                Prorate = false,
            };
            return service.Cancel(subscriptionId, options);
        }

        /// <summary>
        /// Get card checkout session Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="successUrl"></param>
        /// <param name="cancelUrl"></param>
        /// <returns></returns>
        public string GetCreateCardCheckoutSession(string customerId, string subscriptionId, string successUrl, string cancelUrl)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string> {
                    "card",
                },
                Customer = customerId,
                SetupIntentData = new SessionSetupIntentDataOptions
                {
                    Metadata = new Dictionary<string, string>
                    {
                       { "customer_id", customerId },
                       { "subscription_id", subscriptionId },
                    }
                },
                Mode = "setup",
                SuccessUrl = successUrl,
                CancelUrl = cancelUrl,

            };

            var service = new SessionService();
            Stripe.Checkout.Session session = service.Create(options);
            return session.Id;
        }

        /// <summary>
        /// Update default payment method
        /// </summary>
        /// <param name="custonerId"></param>
        /// <param name="cardId"></param>
        public Customer SetDefaultPaymentMethod(string customerId, string paymentMethodId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new CustomerService();
            return service.Update(customerId, options: new CustomerUpdateOptions
            {
                InvoiceSettings = new CustomerInvoiceSettingsOptions
                {
                    DefaultPaymentMethod = paymentMethodId
                }
            });
        }

        /// <summary>
        /// Get default payment method Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public string GetDefaultPaymentMethodId(string customerId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new CustomerService();
            var customer = service.Get(customerId);
            return customer.InvoiceSettings.DefaultPaymentMethodId;
        }

        /// <summary>
        /// getSubscription
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="planId"></param>
        /// <returns></returns>
        public Subscription GetSubscription(string subscriptionId)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new Stripe.SubscriptionService();
            Subscription subscription = service.Get(subscriptionId);

            return subscription;
        }

        /// <summary>
        /// Update a payment method
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="planId"></param>
        /// <returns></returns>
        public PaymentMethod UpdatePaymentMethod(string paymentMethodId, MyPlanBillingAddressViewModel billingDetails)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var options = new PaymentMethodUpdateOptions
            {
                BillingDetails = new BillingDetailsOptions
                {
                    Address = new AddressOptions
                    {
                        City = billingDetails.City,
                        Country = billingDetails.Country,
                        Line1 = billingDetails.Line1,
                        Line2 = billingDetails.Line2,
                        PostalCode = billingDetails.PostalCode,
                        State = billingDetails.State
                    },
                    Name = billingDetails.Name
                }
            };
            var service = new PaymentMethodService();
            return service.Update(
              paymentMethodId,
              options
            );
        }

        /// <summary>
        /// get setupintent 
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public SetupIntent GetSetupIntentObject(string sid)
        {
            StripeConfiguration.ApiKey = _stripeSecretApiKey;
            var service = new SetupIntentService();
            return service.Get(sid);
        }

        /// <summary>
        /// cancel subscription at period end 
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public Subscription CancelSubscriptionAtPeriodEnd(string subscriptionId)
        {
            var service = new Stripe.SubscriptionService();
            var options = new SubscriptionUpdateOptions
            {
                CancelAtPeriodEnd = true,
            };
            return service.Update(subscriptionId, options);
        }
    }
}
