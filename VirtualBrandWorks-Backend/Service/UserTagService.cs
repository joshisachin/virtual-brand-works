﻿using System;
using Repository;
using Repository.Interfaces;
using Service.Interfaces;
using Repository.Model;
using System.Collections.Generic;

namespace Service
{
    public class UserTagService : IUserTagService
    {
        private readonly IUserTagRepository _userTagRepository;

        public UserTagService(IUserTagRepository userTagRepository)
        {
            _userTagRepository = userTagRepository;
        }

        public void Add(UserTag tag)
        {
            _userTagRepository.Add(tag);
        }

        public List<long?> GetUsersFromTags(List<long> tagIds, long userId)
        {
            return _userTagRepository.GetUsersFromTags(tagIds, userId);
        }

    }
}
