﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class CompanyContentService : ICompanyContentService
    {
        private readonly ICompanyContentRepository _companyContentRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;

        public CompanyContentService(ICompanyContentRepository companyContentRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _companyContentRepository = companyContentRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }
        public List<FolderViewModel> GetCompanyContentFolders(CompanyContentFolderRequestViewModel request)
        {
            return _companyContentRepository.GetCompanyContentFolders(request);
        }

        public ContentResponseViewModel CreateContent(CompanyContentViewModel companyContent)
        {
            return _companyContentRepository.CreateContent(companyContent);
        }

        public CompanyContentResponse GetContentConfirmation(ContentViewModel companyContent)
        {
            CompanyContentResponse content = _companyContentRepository.GetContentConfirmation(companyContent);
            if (content.IsAwsContent)
            {
                var contentUrls = _awsS3FileStorage.GetContentUrls(content.ContentUrl, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                content.ContentUrl = contentUrls.Item1;
                content.ThumbnailUrl = contentUrls.Item2;
                content.ContentURLWithExtension = contentUrls.Item3;
            }
            return content;
            // return _awsS3FileStorage.GetContentConfirmation(content);
        }

        public void PublishCompanyContent(ContentViewModel content)
        {
            _companyContentRepository.PublishCompanyContent(content);
        }

        public FolderContentDetailViewModel GetCompanyFolderContentDetail(ContentViewModel content, long userId)
        {
            FolderContentDetailViewModel folderContent = _companyContentRepository.GetCompanyFolderContentDetail(content, userId);
            if (folderContent.Content.IsAwsContent)
            {
                var contentUrls = _awsS3FileStorage.GetContentUrls(folderContent.Content.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                folderContent.Content.ContentURL = contentUrls.Item1;
                folderContent.Content.ThumbnailURL = contentUrls.Item2;
                folderContent.Content.ContentURLWithExtension = contentUrls.Item3;
            }
            return folderContent;
        }

        public QuizViewModel GetQuizDetails(long contenId)
        {
            return _companyContentRepository.GetQuizDetails(contenId);
        }

        public FolderDetailViewModel CreateFolder(CompanyFolderViewModel folderViewModel)
        {
            return _companyContentRepository.CreateFolder(folderViewModel);
        }

        public void UpdateFolder(CompanyFolderViewModel folderViewModel)
        {
            _companyContentRepository.UpdateFolder(folderViewModel);
        }

        public bool IsCompanyFolderExists(CompanyFolderViewModel folderViewModel)
        {
            return _companyContentRepository.IsCompanyFolderExists(folderViewModel);
        }

        public bool IsCompanyFolderShortNameExists(CompanyFolderViewModel folderViewModel)
        {
            return _companyContentRepository.IsCompanyFolderShortNameExists(folderViewModel);
        }

        public FolderContentViewModel GetCompanyFolderContents(FolderViewModel folder, long userId)
        {
            FolderContentViewModel masterFolderContentViewModel = _companyContentRepository.GetCompanyFolderContents(folder, userId);
            if (masterFolderContentViewModel != null && masterFolderContentViewModel.Contents != null)
            {
                masterFolderContentViewModel.Contents.ForEach(res =>
                {
                    if (res.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(res.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                        res.ContentURL = contentUrls.Item1;
                        res.ThumbnailURL = contentUrls.Item2;
                        res.ContentURLWithExtension = contentUrls.Item3;
                    }
                });
            }
            return masterFolderContentViewModel;
        }

        public List<FolderViewModel> GetCompanyContentSelectedFolders(long UserId)
        {
            return _companyContentRepository.GetCompanyContentSelectedFolders(UserId);
        }

        public void SaveSelectedCompanyContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId)
        {
            _companyContentRepository.SaveSelectedCompanyContentFolder(SavedFolders, UserId);
        }

        public void UpdateCompanyContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId)
        {
            _companyContentRepository.UpdateCompanyContentFolderOrders(SortOders, UserId);
        }

        public ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long folderId)
        {
            ContentPreviewViewModel contents = _companyContentRepository.GetFolderAndContentForPreviewPopUp(folderId);
            if (contents.ContentList != null)
            {
                foreach (var res in contents.ContentList)
                {
                    if (res.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(res.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                        res.ContentURL = contentUrls.Item1;
                        res.ThumbnailURL = contentUrls.Item2;
                        res.ContentURLWithExtension = contentUrls.Item3;
                    }
                }
            }
            return contents;
        }

        public List<FolderDetailViewModel> GetCompanyFolderList(long userId)
        {
            return _companyContentRepository.GetCompanyFolderList(userId);
        }

        public List<ContentViewModel> GetCompanyContentsByType(FolderContentRequestViewModel folder)
        {
            List<ContentViewModel> contents = _companyContentRepository.GetCompanyContentsByType(folder);
            if (contents != null && contents.Count > 0)
            {
                if (!contents.Any(c => c.IsFacebookContent || c.IsYoutubeContent))
                {
                    if (folder.ContentType == (int)Constants.ContentType.Video)
                    {
                        contents = contents.Where(c => c.ContentURL.ToLower().Contains(".mov") || c.ContentURL.ToLower().Contains(".mp4") || c.ContentURL.ToLower().Contains(".3gp")).ToList();
                    }
                    else if (folder.ContentType == (int)Constants.ContentType.Document)
                    {
                        contents = contents.Where(c => c.ContentURL.ToLower().Contains(".doc") || c.ContentURL.ToLower().Contains(".docx") || c.ContentURL.ToLower().Contains(".pdf") || c.ContentURL.ToLower().Contains(".txt")).ToList();
                    }
                    else
                    {
                        contents = contents.Where(c => c.ContentURL.ToLower().Contains(".jpeg") || c.ContentURL.ToLower().Contains(".jpg") || c.ContentURL.ToLower().Contains(".png")).ToList();
                    }
                }
                else
                {
                    //company dynamic folder only contains videos
                    if (folder.ContentType != (int)Constants.ContentType.Video)
                    {
                        contents = new List<ContentViewModel>();
                    }
                }
                contents.ForEach(res =>
                {
                    if (res.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(res.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                        res.ContentURL = contentUrls.Item1;
                        res.ThumbnailURL = contentUrls.Item2;
                        res.ContentURLWithExtension = contentUrls.Item3;
                    }
                });
            }
            return contents;
        }

        public List<ContentViewModel> GetUserCreatedContentList(long userId)
        {
            List<ContentViewModel> contents = _companyContentRepository.GetUserCreatedContentList(userId);
            if (contents != null)
            {
                contents.ForEach(res =>
                {
                    if (res.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(res.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                        res.ContentURL = contentUrls.Item1;
                        res.ThumbnailURL = contentUrls.Item2;
                        res.ContentURLWithExtension = contentUrls.Item3;
                    }
                });
            }
            return contents;
        }

        public bool DeleteCompanyContent(long contentId)
        {
            return _companyContentRepository.DeleteCompanyContent(contentId);
        }

        public bool BroadCastCompanyContent(ContentViewModel content)
        {
            return _companyContentRepository.BroadCastCompanyContent(content);
        }

        public void CompanyContentView(ContentViewModel requestContent, long userId)
        {
            _companyContentRepository.CompanyContentView(requestContent, userId);
        }

        public void UpdateCompanyContentVisibility(CompanyContentVisibilityViewModel content)
        {
            _companyContentRepository.UpdateCompanyContentVisibility(content);
        }
    }
}
