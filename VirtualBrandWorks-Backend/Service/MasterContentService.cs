﻿using Repository;
using Repository.Interfaces;
using Repository.Model;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class MasterContentService : IMasterContentService
    {
        private readonly IMasterContentRepository _masterContentRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;

        public MasterContentService(IMasterContentRepository masterContentRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _masterContentRepository = masterContentRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }

        public List<MasterContentViewModel> GetAllContents()
        {
            return _masterContentRepository.GetAllContents();
        }

        public MasterContentViewModel GetContent(long contentId)
        {
            return _masterContentRepository.GetContent(contentId);
        }
        public void Create(MasterContentViewModel content)
        {
            if(content != null)
            {
                _masterContentRepository.Create(content);
            }
        }
        public void Update(MasterContentViewModel content)
        {
            _masterContentRepository.Update(content);
        }
        public void Delete(long contentId)
        {
           _masterContentRepository.Delete(contentId);
        }
        public List<ContentViewModel> GetMasterContentsByFolderId(long masterFolderId)
        {
            return _masterContentRepository.GetMasterContentsByFolderId(masterFolderId);
        }
        public int PublishUnpublishContent(long id)
        {
            return _masterContentRepository.PublishUnpublishContent(id);
        }

        public ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long masterFolderId, long userId)
        {
            ContentPreviewViewModel contentsPreview = _masterContentRepository.GetFolderAndContentForPreviewPopUp(masterFolderId, userId);
            if (contentsPreview.ContentList != null)
            {
                contentsPreview.ContentList.ForEach(con =>
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(con.ContentURL, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                    con.ContentURL = contentUrls.Item1;
                    con.ThumbnailURL = contentUrls.Item2;
                    con.ContentURLWithExtension = contentUrls.Item3;
                });
            }
            return contentsPreview;
        }

        public FolderContentViewModel GetMasterFolderContents(FolderViewModel folder, long userId)
        {
            FolderContentViewModel masterFolderContentViewModel = _masterContentRepository.GetMasterFolderContents(folder, userId);
            if (masterFolderContentViewModel != null && masterFolderContentViewModel.Contents != null)
            {
                AWSS3FileStorage awsS3FileStorage = new AWSS3FileStorage();
                masterFolderContentViewModel.Contents.ForEach(res =>
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(res.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                    res.ContentURL = contentUrls.Item1;
                    res.ThumbnailURL = contentUrls.Item2;
                    res.ContentURLWithExtension = contentUrls.Item3;
                });
            }
            return masterFolderContentViewModel;
        }

        public List<FolderViewModel> GetMasterContentSelectedFolders(long UserId)
        {
            return _masterContentRepository.GetMasterContentSelectedFolders(UserId);
        }

        public void SaveSelectedMasterContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId)
        {
            _masterContentRepository.SaveSelectedMasterContentFolder(SavedFolders, UserId);
        }

        public void UpdateMasterContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId)
        {
            _masterContentRepository.UpdateMasterContentFolderOrders(SortOders, UserId);
        }

        public FolderContentDetailViewModel GetMasterFolderContentDetail(ContentViewModel requestContent, long userId)
        {
            FolderContentDetailViewModel folderContent =_masterContentRepository.GetMasterFolderContentDetail(requestContent, userId);
            var contentUrls = _awsS3FileStorage.GetContentUrls(folderContent.Content.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
            folderContent.Content.ContentURL = contentUrls.Item1;
            folderContent.Content.ThumbnailURL = contentUrls.Item2;
            folderContent.Content.ContentURLWithExtension = contentUrls.Item3;
            return folderContent;
        }

        public List<ContentViewModel> GetMasterContentsByType(FolderContentRequestViewModel folder)
        {
            List<ContentViewModel> contents = _masterContentRepository.GetMasterContentsByType(folder);
            if(folder.ContentType == (int)Constants.ContentType.Video)
            {
                contents = contents.Where(c => c.ContentURL.ToLower().Contains(".mov") || c.ContentURL.ToLower().Contains(".mp4") || c.ContentURL.ToLower().Contains(".3gp")).ToList();
            }
            else if(folder.ContentType == (int)Constants.ContentType.Document)
            {
                contents = contents.Where(c => c.ContentURL.ToLower().Contains(".doc") || c.ContentURL.ToLower().Contains(".docx") || c.ContentURL.ToLower().Contains(".pdf") || c.ContentURL.ToLower().Contains(".txt")).ToList();
            }
            else
            {
                contents = contents.Where(c => c.ContentURL.ToLower().Contains(".jpeg") || c.ContentURL.ToLower().Contains(".jpg") || c.ContentURL.ToLower().Contains(".png")).ToList();
            }

            if (contents != null)
            {
                contents.ForEach(res =>
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(res.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                    res.ContentURL = contentUrls.Item1;
                    res.ThumbnailURL = contentUrls.Item2;
                    res.ContentURLWithExtension = contentUrls.Item3;
                });
            }
            return contents;
        }
    }
}
