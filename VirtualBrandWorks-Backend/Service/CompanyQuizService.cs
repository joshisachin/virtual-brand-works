﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class CompanyQuizService : ICompanyQuizService
    {
        private readonly ICompanyQuizRepository _companyQuizRepository;
        public CompanyQuizService(ICompanyQuizRepository companyQuizRepository) {
            _companyQuizRepository = companyQuizRepository;
        }
        public void AddQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel, long userId)
        {
            _companyQuizRepository.AddQuestion(questionAnswerViewModel, userId);
        }

        public void DeleteQuestion(long questionId)
        {
            _companyQuizRepository.DeleteQuestion(questionId);
        }

        public void DeleteQuiz(long quizId)
        {
            _companyQuizRepository.DeleteQuiz(quizId);
        }

        public CompanyQuizViewModel GetQuizDetails(long quizId, bool isMasterFolder, bool isTurnedOffExistingQuiz)
        {
            return _companyQuizRepository.GetQuizDetails(quizId, isMasterFolder, isTurnedOffExistingQuiz);
        }

        public long SaveQuiz(CompanyQuizCreateViewModel companyQuiz)
        {
            return _companyQuizRepository.SaveQuiz(companyQuiz);
        }

        public void UpdateQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel, long userId)
        {
            _companyQuizRepository.UpdateQuestion(questionAnswerViewModel, userId);
        }

        public CompanyQuizViewModel GetQuizToPlay(long quizId, long userId, bool isMasterContent, bool isTurnedOffExistingQuiz)
        {
            return _companyQuizRepository.GetQuizToPlay(quizId, userId, isMasterContent, isTurnedOffExistingQuiz);
        }

        public PassFailViewModel SubmitQuiz(QuestionAnswerAttemptViewModel quizResponse)
        {
            return _companyQuizRepository.SubmitQuiz(quizResponse);
        }
    }
}
