﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        public CompanyService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public List<CompanyViewModel> GetAllCompany()
        {
            return _companyRepository.GetAllCompany();
        }

        public List<UserViewModel> GetUsersByCompanyId(long companyId)
        {
            if(companyId != 0)
            {
                return _companyRepository.GetUsersByCompanyId(companyId);
            }
            return null;
        }

        public CompanyViewModel ActivateOrDeactivateCompany(long companyId)
        {
            if(companyId != 0)
            {
               return _companyRepository.ActivateOrDeactivateCompany(companyId);
            }
            return null;
        }
    }
}
