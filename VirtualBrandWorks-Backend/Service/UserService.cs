﻿using Repository.Interfaces;
using Repository.Model;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Common;
using System.IO;
using VirtualBrandWork.Entity;
using Newtonsoft.Json;
using VirtualBrandWork.Notification.Interfaces;

namespace Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ISubscriptionRepository _subscriptionRepository;
        private readonly IStripePaymentUtility _stripePaymentUtility;
        private readonly IAWSS3FileStorage _awsS3FileStorage;

        public UserService(IUserRepository userTagRepository, ISubscriptionRepository subscriptionRepository, IStripePaymentUtility stripePaymentUtility, IAWSS3FileStorage awsS3FileStorage)
        {
            _userRepository = userTagRepository;
            _subscriptionRepository = subscriptionRepository;
            _stripePaymentUtility = stripePaymentUtility;
            _awsS3FileStorage = awsS3FileStorage;
        }
        public UserContext ValidateUser(string name, string password)
        {
            UserViewModel userObj = null;
            //check hash and then then return object
            userObj = ValidateUserWithEmail(name);

            if (userObj == null) // if the user have entered handle name
            {
                userObj = ValidateUserWithHandleName(name);
            }
            
            if (userObj != null)
            {
                var saltedHash = SaltedHash.GetHashPasswordWithSalt(password, userObj.Salt);
                if (saltedHash.Equals(userObj.PasswordHash))
                {
                    UserContext user = _userRepository.ValidateUser(name);
                    //string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                    if (!string.IsNullOrEmpty(user.ProfilePicture))
                    {
                        if (_awsS3FileStorage.fileExistsInS3(user.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                        {
                            user.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), user.ProfilePicture, true, string.Empty);
                        }
                    }
                    else
                    {
                         string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                        user.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                    }
                    if (!String.IsNullOrEmpty(user.CompanyLogo))
                    {
                        if (_awsS3FileStorage.fileExistsInS3(user.CompanyLogo, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.CompanyLogo.ToString()}"))
                        {
                            user.CompanyLogo = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.CompanyLogo.ToString(), user.CompanyLogo, true, string.Empty);
                        }
                    }
                    else
                    {
                        user.CompanyLogo = string.Empty;
                    }
                    return user;
                }
                else
                    return null;
            }
            else
                return null;

            //return _userRepository.ValidateUser(name, password);
        }
        public UserViewModel ValidateUserWithEmail(string emailId)
        {
            return _userRepository.ValidateUserWithEmail(emailId);
        }

        public UserContext ValidateExternalLogin(string email)
        {
            return _userRepository.ValidateExternalLogin(email);
        }
        public bool SetPasswordResetCode(UserViewModel user, string resetCode)
        {
            return _userRepository.SetPasswordResetCode(user, resetCode);
        }
        public int ResetPassword(ResetPasswordViewModel user)
        {
            //get salt from database to genereate new password hash
            var userExisting = ValidateUserWithEmail(user.Email);
            user.PasswordHash = SaltedHash.GetHashPasswordWithSalt(user.NewPassword.Trim(), userExisting.Salt.Trim());
            return _userRepository.ResetPassword(user);
        }
        public bool ChangePassword(ChangePasswordViewModel changePassword, int userId)
        {
            if (changePassword != null)
            {
                return _userRepository.ChangePassword(changePassword, userId);
            }
            else
            {
                return false;
            }
        }

        public bool ValidatePasswordResetCode(string resetCode)
        {
            if (resetCode != null)
            {
                return _userRepository.ValidatePasswordResetCode(resetCode);
            }
            return false;
        }



        public bool ValidateCompanyStatus(long userId)
        {
            if (userId != 0)
            {
                return _userRepository.ValidateCompanyStatus(userId);
            }
            return false;
        }
        public UserInviteViewModel GetUserInvite(string email)
        {
            if (!String.IsNullOrEmpty(email))
            {
                return _userRepository.GetUserInvite(email);
            }
            return null;
        }


        public void SignUpAsUser(SignUpViewModel signUpUser)
        {
            if (signUpUser != null)
            {
                //generate salt and hash and assign
                signUpUser.Salt = SaltedHash.GetSalt();
                signUpUser.PasswordHash = SaltedHash.GetHashPasswordWithSalt(signUpUser.Password.Trim(), signUpUser.Salt.Trim());
                _userRepository.SignUpAsUser(signUpUser);
            }
        }

        public List<ValidationSummaryViewModel> ValidateUserEmailAndCompanyName(SignUpViewModel signUpViewModel)
        {
            return _userRepository.ValidateUserEmailAndCompanyName(signUpViewModel);
        }

        public Response SaveAdminDetails(SignUpViewModel signUpViewModel)
        {
            signUpViewModel.Salt = SaltedHash.GetSalt();
            signUpViewModel.PasswordHash = SaltedHash.GetHashPasswordWithSalt(signUpViewModel.Password.Trim(), signUpViewModel.Salt.Trim());
            return _userRepository.SaveAdminDetails(signUpViewModel);
        }

        public Response DeleteSignUpDetails(string email)
        {
            return _userRepository.DeleteSignUpDetails(email);
        }

        public Response UpdateSignUpDetailsPaymentSuccess(string email, string customerId, string subscriptionId, bool isTrial)
        {
            return _userRepository.UpdateSignUpDetailsPaymentSuccess(email, customerId, subscriptionId, isTrial);
        }

        public UserMyPlanNotiFicationViewModel ChangeSubscriptionPlan(long userId, long planId, string stripeSubscriptionPlanId, string oldPlanName)
        {
            string stripeSubscriptionId = _subscriptionRepository.GetStripeSubscriptionId(userId);
            bool isTrial = false;
            if (oldPlanName.ToLower().Equals(Constants.TRIAL_PLAN.ToLower()))
                isTrial = true;
            ////Update subscription on stripe
            _stripePaymentUtility.UpdateSubscription(stripeSubscriptionId, stripeSubscriptionPlanId, isTrial);
            ///Update the database accordingly
            _userRepository.ChangeSubscriptionPlan(userId, planId);
            UserMyPlanNotiFicationViewModel info = _userRepository.GetUserNameAndEmail(userId);
            info.OldPlanName = oldPlanName;
            SubscriptionPriceDetailViewModel planDetails = _userRepository.GetSubscriptionDetailsFromPlanPriceId(planId);
            info.PlanName = planDetails.PlanName;
            info.PlanType = planDetails.PlanType == (int)Constants.PlanType.Yearly ? Constants.PlanType.Yearly.ToString() : Constants.PlanType.Monthly.ToString();
            info.Price = Convert.ToDouble(planDetails.Price);
            info.NumberOfAllowedUsers = _userRepository.LimitActiveUsersAsPerPlan(userId, planId);
            return info;
        }

        public UserMyPlanNotiFicationViewModel CancelSubscriptionPlan(long userId, long planId)
        {
            string stripeSubscriptionId = _subscriptionRepository.GetStripeSubscriptionId(userId);
            ////Cancel subscription on stripe that would cancelled after end of the period
            Stripe.Subscription subscription = _stripePaymentUtility.CancelSubscriptionAtPeriodEnd(stripeSubscriptionId);
            ///Update the database accordingly
            _userRepository.CancelSubscriptionPlan(userId, planId);
            UserMyPlanNotiFicationViewModel info = _userRepository.GetUserNameAndEmail(userId);
            info.PlanName = _userRepository.GetSubscriptionFromPlanPriceId(planId);
            info.CancelAt = subscription.CurrentPeriodEnd;
            return info;
           // _emailService.SendSubscriptionScheduledToCancelEmail(_userRepository.GetSubscriptionFromPlanPriceId(planId), subscription.CurrentPeriodEnd, _userRepository.GetUserNameAndEmail(userId));
        }

        public string GetCreateCardCheckoutSession(long userId, string successUrl, string cancelUrl)
        {
            string customerId = _userRepository.GetCustomerId(userId);
            string stripeSubscriptionId = _subscriptionRepository.GetStripeSubscriptionId(userId);
            ////get card session id for adding new card
            return _stripePaymentUtility.GetCreateCardCheckoutSession(customerId, stripeSubscriptionId, successUrl, cancelUrl);
        }

        public void SetDefaultPaymentMethod(long userId, string paymentMethodId)
        {
            string customerId = _userRepository.GetCustomerId(userId);
            _stripePaymentUtility.SetDefaultPaymentMethod(customerId, paymentMethodId);
        }

        public PaymentMethodViewModel PaymentMethods(long userId)
        {
            string customerId = _userRepository.GetCustomerId(userId);
            Stripe.StripeList<Stripe.PaymentMethod> response = _stripePaymentUtility.PaymentMethods(customerId);
            StripeCardViewModel cards = JsonConvert.DeserializeObject<StripeCardViewModel>(response.RawJObject.ToString());
            string defaultPaymentMethodId = _stripePaymentUtility.GetDefaultPaymentMethodId(customerId);
            PaymentMethodViewModel paymentMethod = null;
            if(cards != null)
            {
                if (cards.data.Count == 1)
                {
                    cards.data.ForEach(obj =>
                    {
                        paymentMethod = new PaymentMethodViewModel() { CardType = obj.card.brand, ExpMonth = obj.card.exp_month, ExpYear = obj.card.exp_year, Last4Digit = obj.card.last4, PaymentMethodId = obj.id, BillingDetails = obj.billing_details, IsDefault = obj.id.Equals(defaultPaymentMethodId) ? true : false };
                    });
                }
                else
                {
                    var defaultCard = cards.data.Find(c => c.id == defaultPaymentMethodId);
                    paymentMethod = paymentMethod = new PaymentMethodViewModel() { CardType = defaultCard.card.brand, ExpMonth = defaultCard.card.exp_month, ExpYear = defaultCard.card.exp_year, Last4Digit = defaultCard.card.last4, PaymentMethodId = defaultCard.id, BillingDetails = defaultCard.billing_details, IsDefault = defaultCard.id.Equals(defaultPaymentMethodId) ? true : false };
                }
            }
            
            return paymentMethod;
        }

        public bool UserLoginStatus(long userId, string statusCode)
        {
            return _userRepository.UserLoginStatus(userId, statusCode);
        }

        public bool RemoveLoginStatus(long userId, string statusCode)
        {
            return _userRepository.RemoveLoginStatus(userId, statusCode);
        }
        public void SaveUsersLoginStatus(string Email, string saltKey)
        {
            _userRepository.SaveUsersLoginStatus(Email, saltKey);
        }

        public UserContext GetUserContextFromUserId(long userId)
        {
            UserContext user = _userRepository.GetUserContextFromUserId(userId);
            return user;
        }

        public string GetEmailIdFromUserId(long userId)
        {
            return _userRepository.GetEmailIdFromUserId(userId);
        }

        public long GetCompanyIdFromUserId(long userId)
        {
            return _userRepository.GetCompanyIdFromUserId(userId);
        }

        public void UpdateBillingAddress(long userId, MyPlanBillingAddressViewModel billingDetails)
        {
            string customerId = _userRepository.GetCustomerId(userId);
            Stripe.StripeList<Stripe.PaymentMethod> response = _stripePaymentUtility.PaymentMethods(customerId);
            StripeCardViewModel cards = JsonConvert.DeserializeObject<StripeCardViewModel>(response.RawJObject.ToString());
            string paymentMethodId = null;
            string defaultPaymentMethodId = _stripePaymentUtility.GetDefaultPaymentMethodId(customerId);
            if (cards != null)
            {
                if (cards.data.Count == 1)
                {
                    paymentMethodId = cards.data.Find(c => c.id != null).id;
                }
                else
                {
                    paymentMethodId = cards.data.Find(c => c.id == defaultPaymentMethodId).id;
                }
                _stripePaymentUtility.UpdatePaymentMethod(paymentMethodId, billingDetails);
            }
        }

        public UserMyPlanNotiFicationViewModel UpdatePaymentMethodSuccess(long userId, string sessionId)
        {
            Stripe.Checkout.Session session = _stripePaymentUtility.GetCheckoutSession(sessionId);
            StripeSessionResponse stripeResponse = JsonConvert.DeserializeObject<StripeSessionResponse>(session.RawJObject.ToString());
            Stripe.SetupIntent setupIntent = _stripePaymentUtility.GetSetupIntentObject(stripeResponse.SetupIntent.ToString());
            string customerId = _userRepository.GetCustomerId(userId);
            //get all cards
            Stripe.StripeList<Stripe.PaymentMethod> response = _stripePaymentUtility.PaymentMethods(customerId);
            StripeCardViewModel cards = JsonConvert.DeserializeObject<StripeCardViewModel>(response.RawJObject.ToString());
            //get default paymentMethod
            string defaultPaymentMethodId = _stripePaymentUtility.GetDefaultPaymentMethodId(customerId);

            PaymentMethodViewModel oldPaymentMethod = null;
            if (!string.IsNullOrEmpty(defaultPaymentMethodId))
            {
                var defaultCard = cards.data.Find(c => c.id == defaultPaymentMethodId);
                oldPaymentMethod = new PaymentMethodViewModel() { CardType = defaultCard.card.brand, ExpMonth = defaultCard.card.exp_month, ExpYear = defaultCard.card.exp_year, Last4Digit = defaultCard.card.last4, PaymentMethodId = defaultCard.id, BillingDetails = defaultCard.billing_details, IsDefault = defaultCard.id.Equals(defaultPaymentMethodId) ? true : false };
            }
            else
            {
                cards.data.ForEach(obj =>
                {
                    oldPaymentMethod = new PaymentMethodViewModel() { CardType = obj.card.brand, ExpMonth = obj.card.exp_month, ExpYear = obj.card.exp_year, Last4Digit = obj.card.last4, PaymentMethodId = obj.id, BillingDetails = obj.billing_details, IsDefault = obj.id.Equals(defaultPaymentMethodId) ? true : false };
                });
            }
            var newCard = cards.data.Find(c => c.id == setupIntent.PaymentMethodId);
            
            PaymentMethodViewModel newCardDetails = new PaymentMethodViewModel() { CardType = newCard.card.brand, ExpMonth = newCard.card.exp_month, ExpYear = newCard.card.exp_year, Last4Digit = newCard.card.last4, PaymentMethodId = newCard.id, BillingDetails = newCard.billing_details, IsDefault = newCard.id.Equals(defaultPaymentMethodId) ? true : false };
            _stripePaymentUtility.SetDefaultPaymentMethod(customerId, setupIntent.PaymentMethodId);
            UserMyPlanNotiFicationViewModel info = _userRepository.GetUserNameAndEmail(userId);
            info.OldLast4Digit = oldPaymentMethod.Last4Digit;
            info.NewLast4Digit = newCardDetails.Last4Digit;
            return info;
            //_emailService.SendUpdatePaymentInfoEmail(oldPaymentMethod, newCardDetails, _userRepository.GetUserNameAndEmail(userId));
        }

        public UserMyPlanNotiFicationViewModel GetUserNameAndEmail(long userId)
        {
            return _userRepository.GetUserNameAndEmail(userId);
        }

        public bool ValidateSubscription(long userId)
        {
            try
            {
                string stripeSubscriptionId = _subscriptionRepository.GetStripeSubscriptionId(userId);
                Stripe.Subscription subscription = _stripePaymentUtility.GetSubscription(stripeSubscriptionId);
                // one day leeway provided
                if (subscription.Status.Equals("canceled") || subscription.CurrentPeriodEnd.AddDays(1) < DateTime.UtcNow)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return true;
            }
           
        }

        public UserViewModel ValidateUserWithHandleName(string handleName)
        {
            return _userRepository.ValidateUserWithHandleName(handleName);
        }

    }
}
