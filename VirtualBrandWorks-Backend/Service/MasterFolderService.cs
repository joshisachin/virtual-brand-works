﻿using Repository.Interfaces;
using Service.Interfaces;
using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class MasterFolderService : IMasterFolderService
    {
        private readonly IMasterFolderRepository _masterFolderRepository;
        public MasterFolderService(IMasterFolderRepository masterFolderRepository)
        {
            _masterFolderRepository = masterFolderRepository;
        }
        public bool IsFolderExists(MasterFolderViewModel folderViewModel)
        {
            return _masterFolderRepository.IsFolderExists(folderViewModel);
        }
        public bool IsFolderShortNameExists(MasterFolderViewModel folderViewModel)
        {
            return _masterFolderRepository.IsFolderShortNameExists(folderViewModel);
        }
        public List<MasterFolderViewModel> GetAllFolders()
        {
            return _masterFolderRepository.GetAllFolders();
        }
        List<FolderViewModel> IMasterFolderService.GetFilterFolders(CompanyContentFolderRequestViewModel request)
        {
            return _masterFolderRepository.GetFilterFolders(request);
        }
        public MasterFolderViewModel GetFolder(long folderId)
        {
            return _masterFolderRepository.GetFolder(folderId);
        }
        public Response Create(MasterFolderViewModel folderViewModel)
        {
           return _masterFolderRepository.Create(folderViewModel);
        }
        public void Update(MasterFolderViewModel FolderViewModel)
        {
            _masterFolderRepository.Update(FolderViewModel);
        }
        public Response Delete(long folderId)
        {
           return _masterFolderRepository.Delete(folderId);
        }
        public Response GetFoldersByCategoyId(long categoryId)
        {
            return _masterFolderRepository.GetFoldersByCategoyId(categoryId);
        }
  
    }
}
