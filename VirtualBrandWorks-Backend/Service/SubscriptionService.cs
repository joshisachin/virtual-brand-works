﻿using Repository.Interfaces;
using Service.Interfaces;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly ISubscriptionRepository _subscriptionRepository;
        private readonly IStripePaymentUtility _stripePaymentUtility;

        public SubscriptionService(ISubscriptionRepository subscriptionRepository, IStripePaymentUtility stripePaymentUtility)
        {
            _subscriptionRepository = subscriptionRepository;
            _stripePaymentUtility = stripePaymentUtility;
        }
        public IEnumerable<SubscriptionViewModel> GetAllSubscription()
        {
            var list = _subscriptionRepository.GetSubscriptionList();
            return list;
        }
        public SubscriptionViewModel GetSubscriptionById(SubscriptionViewModel subscription)
        {
            return _subscriptionRepository.GetSubscriptionById(subscription);
        }
        public void Create(SubscriptionViewModel subscription)
        {
            ///Create subscription in stripe 
            string monthlyPlanId = string.Empty, yearlyPlanId = string.Empty;
            Product product = _stripePaymentUtility.CreateProduct(subscription.PlanName, subscription.PlanDescription);
            if (product != null)
            {
                Plan plan = _stripePaymentUtility.CreatePlan(Convert.ToInt64(subscription.CostForMonthlyBilling), "usd", "month", product.Id);
                monthlyPlanId = plan.Id;

                if (subscription.CostForAnnualBilling > 0)
                {
                    plan = _stripePaymentUtility.CreatePlan(Convert.ToInt64(subscription.CostForAnnualBilling), "usd", "year", product.Id);
                    yearlyPlanId = plan.Id;
                }

                ///Set product and plan Id

                subscription.SubscriptionPlanPrice = new List<SubscriptionPlanPriceViewModel>()
                    {
                        new SubscriptionPlanPriceViewModel() { PlanId = monthlyPlanId, Price = subscription.CostForMonthlyBilling, PlanType = (int)Constants.PlanType.Monthly, ProductId = product.Id}
                    };

                if (!string.IsNullOrEmpty(yearlyPlanId))
                {
                    subscription.SubscriptionPlanPrice.Add(
                        new SubscriptionPlanPriceViewModel() { PlanId = yearlyPlanId, Price = subscription.CostForAnnualBilling, PlanType = (int)Constants.PlanType.Yearly, ProductId = product.Id });
                }

                subscription.ProductId = product.Id;
                _subscriptionRepository.Create(subscription);
            }
        }
        public UserMyPlanNotiFicationViewModel Update(SubscriptionViewModel subscription)
        {
            Product product = _stripePaymentUtility.UpdateProduct(subscription.PlanName, subscription.PlanDescription, subscription.ProductId);
            string monthlyPlanId = string.Empty, yearlyPlanId = string.Empty;
            Plan plan = new Plan();
            UserMyPlanNotiFicationViewModel info = new UserMyPlanNotiFicationViewModel();
            SubscriptionViewModel oldSubscriptionData = GetSubscriptionById(new SubscriptionViewModel { SubscriptionId = subscription.SubscriptionId, PlanType = 1 });
            if (!oldSubscriptionData.PlanName.Equals(subscription.PlanName))
            {
                info.IsPlanNameChanged = true;
                info.OldPlanName = oldSubscriptionData.PlanName;
                info.PlanName = subscription.PlanName;
            }
            if (!oldSubscriptionData.PlanDescription.Equals(subscription.PlanDescription))
            {
                info.IsPlanDescriptionChanged = true;
                info.OldPlanDescription = oldSubscriptionData.PlanDescription;
                info.PlanDescription = subscription.PlanDescription;
            }
            if (oldSubscriptionData.NumberOfUsers != subscription.NumberOfUsers)
            {
                info.IsNumberOfUsersChanged = true;
                info.OldNumberOfAllowedUsers = oldSubscriptionData.NumberOfUsers.Value;
                info.NumberOfAllowedUsers = subscription.NumberOfUsers.Value;
            }

            ///get old Plan, if price has changed then create new plan
            List<SubscriptionPlanPriceViewModel> plans = GetSubscriptionPlanPrice(subscription.ProductId);
            ///Verify monthly plan price
            var mPlan = plans.Where(p => p.PlanType == (int)Constants.PlanType.Monthly).FirstOrDefault();
            if (mPlan != null)
            {
                //Get users subscribed for monthly
                info.UsersForMonthlySubscription = _subscriptionRepository.GetSubscribedUsersForSubscription(mPlan.SubscriptionPlanPriceId);

                if (mPlan.Price != subscription.CostForMonthlyBilling)
                {
                    info.IsMonthlyCostChanged = true;
                    info.OldMonthlyCost = mPlan.Price.Value;
                    info.NewMonthlyCost = subscription.CostForMonthlyBilling.Value;

                    plan = _stripePaymentUtility.CreatePlan(Convert.ToInt64(subscription.CostForMonthlyBilling), "usd", "month", subscription.ProductId);
                    monthlyPlanId = plan.Id;

                    ///Delete old Plan from stripe
                    _stripePaymentUtility.DeletePlan(mPlan.PlanId);
                }
            }

            ///Verify yearly plan price
            var yPlan = plans.Where(p => p.PlanType == (int)Constants.PlanType.Yearly).FirstOrDefault();
            if (yPlan != null)
            {
                //Get users subscribed for yearly
                info.UsersForYearlySubscription = _subscriptionRepository.GetSubscribedUsersForSubscription(yPlan.SubscriptionPlanPriceId);

                if (yPlan.Price != subscription.CostForAnnualBilling && subscription.CostForAnnualBilling > 0)
                {
                    info.IsAnnualCostChanged = true;
                    info.OldAnnualCost = yPlan.Price.Value;
                    info.NewAnnualCost = subscription.CostForAnnualBilling.Value;

                    plan = _stripePaymentUtility.CreatePlan(Convert.ToInt64(subscription.CostForAnnualBilling), "usd", "year", subscription.ProductId);
                    yearlyPlanId = plan.Id;

                    ///Delete old Plan from stripe
                    _stripePaymentUtility.DeletePlan(yPlan.PlanId);
                }
            }

            ///Set product and plan Id if vlaue has changed
            subscription.SubscriptionPlanPrice = new List<SubscriptionPlanPriceViewModel>();
            if (!string.IsNullOrEmpty(monthlyPlanId))
            {
                subscription.SubscriptionPlanPrice.Add(
                    new SubscriptionPlanPriceViewModel() { PlanId = monthlyPlanId, Price = subscription.CostForMonthlyBilling, PlanType = (int)Constants.PlanType.Monthly, ProductId = subscription.ProductId });
            }

            if (!string.IsNullOrEmpty(yearlyPlanId))
            {
                subscription.SubscriptionPlanPrice.Add(
                    new SubscriptionPlanPriceViewModel() { PlanId = yearlyPlanId, Price = subscription.CostForAnnualBilling, PlanType = (int)Constants.PlanType.Yearly, ProductId = subscription.ProductId });
            }
            _subscriptionRepository.Update(subscription);
            return info;
        }
        public void Delete(SubscriptionViewModel subscription)
        {
            _subscriptionRepository.Delete(subscription.SubscriptionId);
            ///Delete old Product and Plan from stripe
           List<SubscriptionPlanPriceViewModel> plans = GetSubscriptionPlanPrice(subscription.ProductId);
            plans.ForEach(obj => { _stripePaymentUtility.DeletePlan(obj.PlanId); });
            _stripePaymentUtility.DeleteProduct(subscription.ProductId);
        }

        public List<SubscriptionViewModel> GetSubscriptionListForAdmin(bool noTrial)
        {
            List<SubscriptionViewModel> plans = _subscriptionRepository.GetSubscriptionListForAdmin();
            if (noTrial)
            {
                List<SubscriptionViewModel> plansWithoutTrial = plans.Where(p => !p.PlanName.ToLower().Equals(Constants.TRIAL_PLAN.ToLower())).ToList();
                return plansWithoutTrial;
            }
            return plans;
        }
        public CompanySubscriptionViewModel GetSubscriptionDetailsForCompany(long companyId)
        {
            if(companyId != 0)
            {
                CompanySubscriptionViewModel companySubscription = _subscriptionRepository.GetSubscriptionDetailsForCompany(companyId);
                if(companySubscription != null)
                {
                    //check if the subscription plan is a trial plan or not
                    if(!companySubscription.SubscriptionName.Equals(Constants.TRIAL_PLAN,StringComparison.OrdinalIgnoreCase))
                    {
                        companySubscription.BillingCycle = companySubscription.PlanType == (int)Constants.PlanType.Monthly ? Constants.PlanType.Monthly.ToString() : EnumHelper.ToDisplayName(Constants.PlanType.Yearly);

                    }
                    else
                    {
                        companySubscription.BillingCycle = Constants.TRIAL_PLAN_BILLING_CYCLE;
                    }
                    if(!string.IsNullOrEmpty(companySubscription.StripeSubscriptionId))
                    {
                        // get subscription
                        Subscription subscription = _stripePaymentUtility.GetSubscription(companySubscription.StripeSubscriptionId);
                        if (subscription != null)
                        {
                            companySubscription.ActiveFrom = subscription.CurrentPeriodStart;
                            companySubscription.ValidTill = subscription.CurrentPeriodEnd;
                            companySubscription.IsCancelled = subscription.CancelAtPeriodEnd;
                        }
                    }
                }
                return companySubscription;
            }
            return null;
        }

        public List<SubscriptionPlanPriceViewModel> GetSubscriptionPlanPrice(string productId) {
            return _subscriptionRepository.GetSubscriptionListForAdmin(productId);
        }


        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
