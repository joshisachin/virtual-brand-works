﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class BuildPlaylistService : IBuildPlaylistService
    {
        private readonly IBuildPlaylistRepository _buildPlaylistRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;
        public BuildPlaylistService(IBuildPlaylistRepository buildPlaylistRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _buildPlaylistRepository = buildPlaylistRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }
        public int SavePlaylistFolders(long playlistId, long userId)
        {
            return _buildPlaylistRepository.SavePlaylistFolders(playlistId, userId);
        }
        public List<PlaylistFolderViewModel> GetPlaylistFolders(long playlistId, long userId, bool isAllContentCount)
        {
            return _buildPlaylistRepository.GetPlaylistFolders(playlistId, userId, isAllContentCount);
        }
        public void UpdateSortOrderForPlaylistFolderUserSaved(List<PlaylistFolderUserSavedViewModel> folders)
        {
            _buildPlaylistRepository.UpdateSortOrderForPlaylistFolderUserSaved(folders);
        }
        public long CreatePlaylist(PlaylistViewModel playlist)
        {
            return _buildPlaylistRepository.CreatePlaylist(playlist);
        }

        public FolderContentViewModel GetPlayListContents(long playListFolderId, bool isPlaylistExist, long userId)
        {
            FolderContentViewModel folderContentViewModel = _buildPlaylistRepository.GetPlayListContents(playListFolderId, isPlaylistExist, userId);
            if (folderContentViewModel != null && folderContentViewModel.Contents != null)
            {
                folderContentViewModel.Contents.ForEach(pc => { if (pc.SortOrder == null || pc.SortOrder == 0) { pc.SortOrder = folderContentViewModel.Contents.Count(); } });
                folderContentViewModel.Contents = folderContentViewModel.Contents.OrderBy(o => o.SortOrder).ToList();
                int numberOfContents = folderContentViewModel.Contents.Count();
                folderContentViewModel.Contents.ForEach(con =>
                {
                    if (con.SortOrder == null || con.SortOrder == 0)
                    {
                        con.SortOrder = numberOfContents;
                    }
                    var contentUrls = _awsS3FileStorage.GetContentUrls(con.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                    con.ContentURL = contentUrls.Item1;
                    con.ThumbnailURL = contentUrls.Item2;
                    con.ContentURLWithExtension = contentUrls.Item3;
                });
            }
            return folderContentViewModel;
        }

        public void UpdatePlayListContents(PlaylistContentViewModel playlistContentViewModel, long userId)
        {
            _buildPlaylistRepository.UpdatePlayListContents(playlistContentViewModel, userId);
        }

        public List<FolderViewModel> GetPlaylistFooterData(long userId)
        {
            return _buildPlaylistRepository.GetPlaylistFooterData(userId);
        }

        public void SaveBuildPlaylistStatus(BuildPlaylistSavedStateViewModel state)
        {
            _buildPlaylistRepository.SaveBuildPlaylistStatus(state);
        }

        public BuildPlaylistSavedStateViewModel GetBuildPlaylistSavedState(long playlistId ,long userId)
        {
            return _buildPlaylistRepository.GetBuildPlaylistSavedState(playlistId, userId);
        }

        public List<long> AssignPlaylistTo(PlaylistAssignToViewModel playlistDetail, double offSet)
        {
            playlistDetail.StartDate = DateTimeHelper.ConvertDateTimeToUtc(playlistDetail.StartDate, offSet);
            playlistDetail.EndDate = DateTimeHelper.ConvertDateTimeToUtc(playlistDetail.EndDate, offSet);
           return _buildPlaylistRepository.AssignPlaylistTo(playlistDetail);
        }

        public PlaylistDetailViewModel GetPlaylistDetail(long playlistId)
        {
            return _buildPlaylistRepository.GetPlaylistDetail(playlistId);
        }

        public PlaylistAssignedUserOrTeam GetPlaylistAssignedUserAndTeam(long playlistId)
        {
            PlaylistAssignedUserOrTeam assignedUsersAndTeams =  _buildPlaylistRepository.GetPlaylistAssignedUserAndTeam(playlistId);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            assignedUsersAndTeams.Users.ForEach(u =>
            {
                if (u.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(u.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        u.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), u.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    // string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                    u.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            });
            return assignedUsersAndTeams;
        }

        public List<long> LaunchPlaylist(PlaylistUpdateViewModel playlist)
        {
            return _buildPlaylistRepository.LaunchPlaylist(playlist);
        }

        public PlaylistAssignedUserOrTeam GetUsersAndTeamsForAssignPlaylistToMore(long playlistId, int locationId, long userId)
        {
            PlaylistAssignedUserOrTeam assignedUsersandteams = _buildPlaylistRepository.GetUsersAndTeamsForAssignPlaylistToMore(playlistId, locationId, userId);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            assignedUsersandteams.Users.ForEach(u =>
            {
                if (u.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(u.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        u.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), u.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    u.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            });
            return assignedUsersandteams;
        }


        public PlaylistViewModel GetPlaylistForUpdate(long playlistId, long userId)
        {
            return _buildPlaylistRepository.GetPlaylistForUpdate(playlistId, userId);
        }

        public List<PlaylistContentResponseViewModel> GetPlayListContentsForAddQuestion(long playListFolderId, long userId)
        {
            List<PlaylistContentResponseViewModel>  playlistContents = _buildPlaylistRepository.GetPlayListContentsForAddQuestion(playListFolderId, userId);
            if (playlistContents != null)
            {
                playlistContents.ForEach(res =>
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(res.Content.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                    res.Content.ContentURL = contentUrls.Item1;
                    res.Content.ThumbnailURL = contentUrls.Item2;
                    res.Content.ContentURLWithExtension = contentUrls.Item3;
                });
            }
            return playlistContents;
        }

        public void UpdateSortOrderForPlaylistFolder(long playlistId, long playlistFolderId, int destinationSortOrder)
        {
            _buildPlaylistRepository.UpdateSortOrderForPlaylistFolder(playlistId, playlistFolderId, destinationSortOrder);
        }

        public void TurnOffExistingQuiz(long playlistContentMasterId, bool isTurnedOffExistingQuiz)
        {
            _buildPlaylistRepository.TurnOffExistingQuiz(playlistContentMasterId, isTurnedOffExistingQuiz);
        }

        public void TurnOffExistingQuizForAll(long playlistFolderId, bool isTurnedOffExistingQuiz)
        {
            _buildPlaylistRepository.TurnOffExistingQuizForAll(playlistFolderId, isTurnedOffExistingQuiz);
        }

        public void UpdatePlaylist(PlaylistNameAndDescriptionUpdateViewModel playlist)
        {
            _buildPlaylistRepository.UpdatePlaylist(playlist);
        }

    }
}
