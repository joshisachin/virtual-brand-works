﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class VBPlayService : IVBPlayService
    {
        private readonly IVBPlayRepository _vbPlayRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;
        public VBPlayService(IVBPlayRepository vbPlayRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _vbPlayRepository = vbPlayRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }
        public List<VBPlayViewModel> GetVBPlayContest(long userId, Constants.ContestType contestType, double offSet)
        {
            List<VBPlayViewModel> contest = _vbPlayRepository.GetVBPlayContest(userId, contestType);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            contest.ForEach(obj =>
            {
                if (obj.StartDate != null && obj.EndDate != null)
                {
                    obj.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(obj.StartDate, offSet);
                    obj.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(obj.EndDate, offSet);
                }
                if (obj.IsAwsContent)
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(obj.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                    obj.ContentUrl = contentUrls.Item1;
                    obj.ThumbnailUrl = contentUrls.Item2;
                }
                if (!string.IsNullOrEmpty(obj.ProfilePicture))
                {
                    if (_awsS3FileStorage.fileExistsInS3(obj.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        obj.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), obj.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    obj.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
                //check if there is a saved contestEntry in case of Joined Games
                if(contestType == Constants.ContestType.JoinedGames && obj.ChallengeEntry != null)
                {
                    if (obj.ChallengeEntry.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(obj.ChallengeEntry.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                        obj.ChallengeEntry.ContentUrl = contentUrls.Item1;
                        obj.ChallengeEntry.ThumbnailUrl = contentUrls.Item2;
                    }
                    if (obj.ChallengeEntry.ProfilePicture != null)
                    {
                        if (_awsS3FileStorage.fileExistsInS3(obj.ChallengeEntry.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                        {
                            obj.ChallengeEntry.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), obj.ChallengeEntry.ProfilePicture, true, string.Empty);
                        }
                    }
                    else
                    {
                        obj.ChallengeEntry.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                    }
                }
            });
            return contest;
        }

        public VBPlayViewModel CreateContest(VBPlayCreateViewModel vbPlayContest, long userId, double offSet)
        {
            vbPlayContest.StartDate = DateTimeHelper.ConvertDateTimeToUtc(vbPlayContest.StartDate, offSet);
            vbPlayContest.EndDate = DateTimeHelper.ConvertDateTimeToUtc(vbPlayContest.EndDate, offSet);
            VBPlayViewModel contest = _vbPlayRepository.CreateContest(vbPlayContest, userId);
            if (contest != null)
            {
                if(contest.StartDate != null && contest.EndDate != null)
                {
                    contest.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(contest.StartDate, offSet);
                    contest.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(contest.EndDate, offSet);
                }

                if (!string.IsNullOrEmpty(contest.ContentUrl) && contest.IsAwsContent)
                {
                    //creating thumbnail for contest
                    var isThumbCreated = _awsS3FileStorage.CreateThumbnailFromFile(contest.ContentUrl, contest.MimeType, vbPlayContest.Resolution);
                    // generating signedUrls
                    var contentUrls = _awsS3FileStorage.GetContentUrls(contest.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                    contest.ContentUrl = contentUrls.Item1;
                    contest.ThumbnailUrl = contentUrls.Item2;
                }
            }
            return contest;
        }

        public long JoinContest(VBPlayViewModel vbPlayViewModel, long userId)
        {
            long contestEnrtyId = _vbPlayRepository.JoinContest(vbPlayViewModel, userId);
            //creating thumbnail for contest
            var isThumbCreated = _awsS3FileStorage.CreateThumbnailFromFile(vbPlayViewModel.ContentUrl, vbPlayViewModel.MimeType);
            return contestEnrtyId;
        }

        public VBPlayViewModel GetContest(long contestId)
        {
            VBPlayViewModel vbPlayViewModel = _vbPlayRepository.GetContest(contestId);
            //ContentViewModel content = _awsS3FileStorage.GetContentSignedUrls(new ContentViewModel() { ContentURL = vbPlayViewModel.ContentUrl }, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.VBPlay, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.VBPlayThumbnail);
            //if (content != null)
            //{
            //    vbPlayViewModel.ContentUrl = content.ContentURL;
            //    vbPlayViewModel.ThumbnailUrl = content.ThumbnailURL;
            //}
            //vbPlayViewModel.StartDate = vbPlayViewModel.StartDate.ToLocalTime();
            //vbPlayViewModel.EndDate = vbPlayViewModel.EndDate.ToLocalTime();
            //var contentUrls = _awsS3FileStorage.GetContentUrls(vbPlayViewModel.ContentUrl, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.VBPlay, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.VBPlayThumbnail);
            //vbPlayViewModel.ContentUrl = contentUrls.Item1;
            //vbPlayViewModel.ThumbnailUrl = contentUrls.Item2;

            return vbPlayViewModel;
        }

        public ContestChallengeAndEntriesViewModel ViewContest(long userId, long contestId, string search, double offSet)
        {
            ContestChallengeAndEntriesViewModel contest = _vbPlayRepository.ViewContest(userId, contestId, search);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            if (contest.ContestChallenge != null){
                if (contest.ContestChallenge.StartDate != null && contest.ContestChallenge.EndDate != null)
                {
                    contest.ContestChallenge.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(contest.ContestChallenge.StartDate, offSet);
                    contest.ContestChallenge.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(contest.ContestChallenge.EndDate, offSet);
                }
                if (contest.ContestChallenge.IsAwsContent)
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(contest.ContestChallenge.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                    contest.ContestChallenge.ContentUrl = contentUrls.Item1;
                    contest.ContestChallenge.ThumbnailUrl = contentUrls.Item2;
                }

                if (!string.IsNullOrEmpty(contest.ContestChallenge.ProfilePicture))
                {
                    if (_awsS3FileStorage.fileExistsInS3(contest.ContestChallenge.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        contest.ContestChallenge.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), contest.ContestChallenge.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    contest.ContestChallenge.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            }
            if(contest.ContestEntries != null)
            {
                contest.ContestEntries.ForEach(obj =>
                {
                    if (obj.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(obj.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                        obj.ContentUrl = contentUrls.Item1;
                        obj.ThumbnailUrl = contentUrls.Item2;
                    }
                    if (!string.IsNullOrEmpty(obj.ProfilePicture))
                    {
                        if (_awsS3FileStorage.fileExistsInS3(obj.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                        {
                            obj.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), obj.ProfilePicture, true, string.Empty);
                        }
                    }
                    else
                    {
                        obj.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                    }
                });
            }

            return contest;
        }

        public VBPlayViewModel GetContestConfirmation(long contestId)
        {
            VBPlayViewModel contest = _vbPlayRepository.GetContestConfirmation(contestId);
            if(contest != null)
            {
                //contest.StartDate = contest.StartDate.ToLocalTime();
                //contest.EndDate = contest.EndDate.ToLocalTime();
                if(!string.IsNullOrEmpty(contest.ContentUrl) && contest.IsAwsContent)
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(contest.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                    contest.ContentUrl = contentUrls.Item1;
                    contest.ThumbnailUrl = contentUrls.Item2;
                }
            }
            return contest;
        }

        public ChallengeEntryViewModel GetContestEntryConfirmation(long contestEntryId)
        {
            ChallengeEntryViewModel contestEntry = _vbPlayRepository.GetContestEntryConfirmation(contestEntryId);
            if (contestEntry != null && !string.IsNullOrEmpty(contestEntry.ContentUrl) && contestEntry.IsAwsContent)
            {
                var contentUrls = _awsS3FileStorage.GetContentUrls(contestEntry.ContentUrl, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                contestEntry.ContentUrl = contentUrls.Item1;
                contestEntry.ThumbnailUrl = contentUrls.Item2;
            }
            return contestEntry;
        }

        public void BroadcastContest(ContestBroadcastRequestViewModel contest)
        {
            _vbPlayRepository.BroadcastContest(contest);
        }

        public List<BroadcastedContentViewModel> GetBroadcastedContent(string search, long userId)
        {
            List<BroadcastedContentViewModel> broadcastedContents = _vbPlayRepository.GetBroadcastedContent(search, userId);
            if(broadcastedContents != null && broadcastedContents.Count() > 0)
            {
                string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                broadcastedContents.ForEach(b =>
                {
                    if (b.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(b.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                        b.ContentUrl = contentUrls.Item1;
                        b.ThumbnailUrl = contentUrls.Item2;
                    }
                    if (!string.IsNullOrEmpty(b.ProfilePicture))
                    {
                        if (_awsS3FileStorage.fileExistsInS3(b.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                        {
                            b.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), b.ProfilePicture, true, string.Empty);
                        }
                    }
                    else
                    {
                        b.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                    }
                });
            }
            return broadcastedContents;
        }

        public long LikeContest(VBPlayContentCommonViewModel contestContent, long userId)
        {
           return _vbPlayRepository.LikeContest(contestContent, userId);
        }


        public void ContestContentViews(SharedContentViewModel contest)
        {
            _vbPlayRepository.ContestContentViews(contest);
        }

        public VBPlayViewModel SubmitContestEntry(ContestEntrySubmitViewModel contestEntry)
        {
            VBPlayViewModel contestSubmittedEntry = _vbPlayRepository.SubmitContestEntry(contestEntry);
            if (contestEntry != null && !string.IsNullOrEmpty(contestSubmittedEntry.ContentUrl) && contestSubmittedEntry.IsAwsContent)
            {
                var contentUrls = _awsS3FileStorage.GetContentUrls(contestSubmittedEntry.ContentUrl, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                contestSubmittedEntry.ContentUrl = contentUrls.Item1;
                contestSubmittedEntry.ThumbnailUrl = contentUrls.Item2;
            }
            return contestSubmittedEntry;
        }

        public ChallengeEntryViewModel GetContestEntry(long contestEntryId)
        {
            return _vbPlayRepository.GetContestEntry(contestEntryId);
            //if (contestEntry != null && !string.IsNullOrEmpty(contestEntry.ContentUrl) && contestEntry.IsAwsContent)
            //{
            //    var contentUrls = _awsS3FileStorage.GetContentUrls(contestEntry.ContentUrl, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
            //    contestEntry.ContentUrl = contentUrls.Item1;
            //    contestEntry.ThumbnailUrl = contentUrls.Item2;
            //}
            //return contestEntry;
        }
        
        public ChallengeEntryViewModel UpdateContestEntry(VBPlayCreateViewModel contestEntry)
        {
            string oldContestContentUrl = _vbPlayRepository.GetContestContentUrl(contestEntry);
            ChallengeEntryViewModel updatedEntry = _vbPlayRepository.UpdateContestEntry(contestEntry);
            if(updatedEntry.IsAwsContent && !string.IsNullOrEmpty(oldContestContentUrl) && !oldContestContentUrl.Equals(updatedEntry.ContentUrl))
            {
                //creating thumbnail for contest
                var isThumbCreated = _awsS3FileStorage.CreateThumbnailFromFile(updatedEntry.ContentUrl, updatedEntry.MimeType);
                _awsS3FileStorage.DeleteFile(oldContestContentUrl, Constants.S3Folders.Root);
            }
            return updatedEntry;
        }

        public BroadcastedContentViewModel GetSharedContent(long contentId, Constants.SharedContentType contentType, long userId)
        {
            BroadcastedContentViewModel sharedContent = _vbPlayRepository.GetSharedContent(contentId, contentType, userId);
            if (sharedContent != null)
            {
                string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";

                if (sharedContent.IsAwsContent)
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(sharedContent.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                    sharedContent.ContentUrl = contentUrls.Item1;
                    sharedContent.ThumbnailUrl = contentUrls.Item2;
                }
                if (sharedContent.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(sharedContent.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        sharedContent.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), sharedContent.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    sharedContent.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            }
            return sharedContent;
        }

        public EarnedPointsViewModel GetEarnedPoints(long userId)
        {
            return _vbPlayRepository.GetEarnedPoints(userId);
        }

        public bool DeleteContest(VBPlayViewModel contest)
        {
            return _vbPlayRepository.DeleteContest(contest);
        }

        public void ExtendContentDate(long contestId,long notificationId, string type)
        {
            _vbPlayRepository.ExtendContentDate(contestId, notificationId, type);
        }

        public void OptOutNotification(long notificationId)
        {
            _vbPlayRepository.OptOutNotification(notificationId);
        }

        public void UpdateVbPlayContentVisibility(ContestVisibilityViewModel contest)
        {
            _vbPlayRepository.UpdateVbPlayContentVisibility(contest);
        }

        public VBPlayViewModel SubmitContest(ContestEntrySubmitViewModel contest)
        {
            VBPlayViewModel contestSubmittedEntry = _vbPlayRepository.SubmitContest(contest);
            if (contest != null && !string.IsNullOrEmpty(contestSubmittedEntry.ContentUrl) && contestSubmittedEntry.IsAwsContent)
            {
                var contentUrls = _awsS3FileStorage.GetContentUrls(contestSubmittedEntry.ContentUrl, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                contestSubmittedEntry.ContentUrl = contentUrls.Item1;
                contestSubmittedEntry.ThumbnailUrl = contentUrls.Item2;
            }
            return contestSubmittedEntry;
        }

        public VBPlayViewModel UpdateContest(VBPlayUpdateViewModel contest)
        {
            string oldContestContentUrl = _vbPlayRepository.GetContestUrl(contest);
            VBPlayViewModel updatedEntry = _vbPlayRepository.UpdateContest(contest);
            if (updatedEntry.IsAwsContent && !string.IsNullOrEmpty(oldContestContentUrl) && !oldContestContentUrl.Equals(updatedEntry.ContentUrl))
            {
                //creating thumbnail for contest
                var isThumbCreated = _awsS3FileStorage.CreateThumbnailFromFile(updatedEntry.ContentUrl, updatedEntry.MimeType);
                _awsS3FileStorage.DeleteFile(oldContestContentUrl, Constants.S3Folders.Root);
            }
            return updatedEntry;
        }

        public List<ReportAbuseViewModel> GetAllReportAbuse(long userId, double offSet)
        {
            var reports = _vbPlayRepository.GetAllReportAbuse(userId);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            reports.ForEach(obj =>
            {
                if (obj.StartDate != null && obj.EndDate != null)
                {
                    obj.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(obj.StartDate, offSet);
                    obj.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(obj.EndDate, offSet);
                }
                if (obj.IsAwsContent)
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(obj.ContentUrl, _awsS3FileStorage.BucketName, Constants.S3Folders.thumbnails.ToString());
                    obj.ContentUrl = contentUrls.Item1;
                    obj.ThumbnailUrl = contentUrls.Item2;
                }
                if (!string.IsNullOrEmpty(obj.ProfilePicture))
                {
                    if (_awsS3FileStorage.fileExistsInS3(obj.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        obj.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), obj.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    obj.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            });
                return reports;
        }

        public void ReportAbuse(long contestId, long userId)
        {
            _vbPlayRepository.ReportAbuse(contestId, userId);
        }


        public void DeleteReportAbuse(long contestId)
        {
            _vbPlayRepository.DeleteReportAbuse(contestId);
        }

        public void SetNotificationExtenstionFlag(BroadcastedContentViewModel broadcastedContent, bool flag)
        {
            _vbPlayRepository.SetNotificationExtenstionFlag(broadcastedContent, flag);
        }
        public List<long> GetUserIdsOfAdminAndTrainerOfACompany(long userId)
        {
            return _vbPlayRepository.GetUserIdsOfAdminAndTrainerOfACompany(userId);
        }
        public bool IsContestAlreadyReported(long contestId, long userId)
        {
            return _vbPlayRepository.IsContestAlreadyReported(contestId, userId);
        }
    }
}
