﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class PlaylistService : IPlaylistService
    {
        private readonly IPlaylistRepository _playlistRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;

        public PlaylistService(IPlaylistRepository playlistRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _playlistRepository = playlistRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }

        public List<MessageViewModel> ReceiveMessage(MessageViewModel messageViewModel)
        {
            List<MessageViewModel> msgs = _playlistRepository.ReceiveMessage(messageViewModel);
            if (msgs != null)
            {
                msgs.ForEach(msgViewModel =>
                {
                    if (msgViewModel != null)
                    {
                        if(msgViewModel.SenderImage != null)
                        {
                            if (_awsS3FileStorage.fileExistsInS3(msgViewModel.SenderImage, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                            {
                                msgViewModel.SenderImage = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), msgViewModel.SenderImage, true, string.Empty);
                            }
                            else
                            {
                                string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                                msgViewModel.SenderImage = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                            }
                        }
                        else
                        {
                            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                            msgViewModel.SenderImage = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                        }

                        if (msgViewModel.ReceiverImage != null)
                        {
                            if (_awsS3FileStorage.fileExistsInS3(msgViewModel.ReceiverImage, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                            {
                                msgViewModel.ReceiverImage = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), msgViewModel.ReceiverImage, true, string.Empty);
                            }
                            else
                            {
                                string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                                msgViewModel.ReceiverImage = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                            }
                        }
                        else
                        {
                            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                            msgViewModel.ReceiverImage = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                        }
                                                
                        if (msgViewModel.Message != null && msgViewModel.MessageType.Equals((int)Constants.MessageType.Image) && _awsS3FileStorage.fileExistsInS3(msgViewModel.Message, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.MessageThumbnail.ToString()))
                        {
                            msgViewModel.Message = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.MessageThumbnail.ToString(), msgViewModel.Message, true, string.Empty);
                        }
                    }
                });
            }
            return msgs;
        }

        public MessageViewModel SendMessage(MessageViewModel messageViewModel)
        {
            if(!string.IsNullOrEmpty(messageViewModel.ImageMessage))
            {
                string fileName = Guid.NewGuid() + ".jpg";
                _awsS3FileStorage.UploadFile(fileName, messageViewModel.ImageMessage, Constants.S3Folders.MessageThumbnail);
                messageViewModel.ImageMessage = fileName;
            }
            MessageViewModel msgViewModel = _playlistRepository.SendMessage(messageViewModel);
            if (msgViewModel != null && !string.IsNullOrEmpty(msgViewModel.ImageMessage))
            {
                if (_awsS3FileStorage.fileExistsInS3(msgViewModel.ImageMessage, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.MessageThumbnail.ToString()))
                {
                    msgViewModel.ImageMessage = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.MessageThumbnail.ToString(), msgViewModel.ImageMessage, true, string.Empty);
                }
            }
            return msgViewModel;
        }

        public List<PlaylistTopPerformerViewModel> GetPlaylistTopPerofermers(long playListId, bool isTopTen)
        {
            List<PlaylistTopPerformerViewModel> topPerformers = _playlistRepository.GetPlaylistTopPerofermers(playListId, isTopTen);
            //if (isTopTen)
            //{
            //    topPerformers = topPerformers.Take(Constants.TopTen).ToList();
            //}
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            topPerformers.ForEach(u =>
            {
                if (u.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(u.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        u.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), u.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    u.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            });
            return topPerformers;
        }

        public List<PlaylistLeaderboardViewModel> GetPlaylistLeaderboard(long playListId, int? sortBy)
        {
            List<PlaylistLeaderboardViewModel> leaderboards = _playlistRepository.GetPlaylistLeaderboard(playListId);
            leaderboards.ForEach(u =>
            {
                u.Folders.ForEach(f =>
                {
                    f.QuizCompletedCount = (f.NumberOfContent - f.QuizCount) + f.QuizCompletedCount;
                    if (f.NumberOfContent != 0)
                    {
                        f.PlaylistFolderCompletePercentage = (int)Math.Round(((double)f.QuizCompletedCount / (double)f.NumberOfContent) * 100);
                    }
                    else
                    {
                        // hundred percent completed if numberofContent is 0
                        f.PlaylistFolderCompletePercentage = 100;
                    }
                });
                if (u.Folders.Count > 0)
                    u.PlaylistCompletePercentage = u.Folders.Sum(f => f.PlaylistFolderCompletePercentage) / u.Folders.Count();
                if(u.PlaylistCompletePercentage < (int)Constants.LeaderboardCompletionSegregation.TwentyFive)
                {
                    u.Color = Constants.RED_COLOR_CODE;
                }
                else if(u.PlaylistCompletePercentage >= (int)Constants.LeaderboardCompletionSegregation.TwentyFive && u.PlaylistCompletePercentage < (int)Constants.LeaderboardCompletionSegregation.SeventyFive)
                {
                    u.Color = Constants.YELLOW_COLOR_CODE;
                }
                else
                {
                    u.Color = Constants.GREEN_COLOR_CODE;
                }
            });

            if(leaderboards.Count > 0 && sortBy.HasValue && sortBy > 0)
            {
                switch (sortBy)
                {
                    case (int)Constants.LeaderboardFilter.Name:
                        leaderboards = leaderboards.OrderBy(l => l.FirstName).ToList();
                        break;
                    case (int)Constants.LeaderboardFilter.MaxCompletion:
                        leaderboards = leaderboards.OrderByDescending(l => l.PlaylistCompletePercentage).ToList();
                        break;
                    case (int)Constants.LeaderboardFilter.MinCompletion:
                        leaderboards = leaderboards.OrderBy(l => l.PlaylistCompletePercentage).ToList();
                        break;
                    default:
                        break;
                }
            }
            return leaderboards;
        }

        public List<PlaylistDropdownViewModel> GetAllPlaylist(long userId)
        {
            return _playlistRepository.GetAllPlaylist(userId);
        }

        public List<LeaderboardByTeamViewModel> GetPlaylistLeaderboardByTeam(long playListId, int? sortBy)
        {
            List<PlaylistLeaderboardViewModel> leaderboards = _playlistRepository.GetPlaylistLeaderboardByTeam(playListId);
            leaderboards.ForEach(u =>
            {
                u.Folders.ForEach(f =>
                {
                    f.QuizCompletedCount = (f.NumberOfContent - f.QuizCount) + f.QuizCompletedCount;
                    if (f.NumberOfContent != 0)
                    {
                        f.PlaylistFolderCompletePercentage = (int)Math.Round(((double)f.QuizCompletedCount / (double)f.NumberOfContent) * 100);
                    }
                    else
                    {
                        // hundred percent completed if numberofContent is 0
                        f.PlaylistFolderCompletePercentage = 100;
                    }
                });
                if (u.Folders.Count > 0)
                    u.PlaylistCompletePercentage = u.Folders.Sum(f => f.PlaylistFolderCompletePercentage) / u.Folders.Count();
            });

            List<LeaderboardByTeamViewModel> teams = leaderboards.GroupBy(x => x.TeamId).Select(x => new LeaderboardByTeamViewModel
            {
                TeamId = x.Key,
                Name = x.Select(t => t.TeamName).FirstOrDefault(),
                PlaylistCompletePercentage = x.Sum(t => t.PlaylistCompletePercentage) / x.Count()
            }).ToList();

            teams.ForEach(t =>
            {
                if (t.PlaylistCompletePercentage < (int)Constants.LeaderboardCompletionSegregation.TwentyFive)
                {
                    t.Color = Constants.RED_COLOR_CODE;
                }
                else if (t.PlaylistCompletePercentage >= (int)Constants.LeaderboardCompletionSegregation.TwentyFive && t.PlaylistCompletePercentage < (int)Constants.LeaderboardCompletionSegregation.SeventyFive)
                {
                    t.Color = Constants.YELLOW_COLOR_CODE;
                }
                else
                {
                    t.Color = Constants.GREEN_COLOR_CODE;
                }
            });

            if (teams.Count > 0 && sortBy.HasValue && sortBy > 0)
            {
                switch (sortBy)
                {
                    case (int)Constants.LeaderboardFilter.Name:
                        teams = teams.OrderBy(l => l.Name).ToList();
                        break;
                    case (int)Constants.LeaderboardFilter.MaxCompletion:
                        teams = teams.OrderByDescending(l => l.PlaylistCompletePercentage).ToList();
                        break;
                    case (int)Constants.LeaderboardFilter.MinCompletion:
                        teams = teams.OrderBy(l => l.PlaylistCompletePercentage).ToList();
                        break;
                    default:
                        break;
                }
            }

            return teams;
        }

    }
}
