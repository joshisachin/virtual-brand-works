﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class TeamService :ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;
        public TeamService(ITeamRepository teamRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _teamRepository = teamRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }

        public List<TeamListViewModel> GetTeamList(TeamListViewModel team)
        {
            return _teamRepository.GetTeamList(team);
        }

        public TeamDetailViewModel GetTeamDetail(TeamDetailViewModel team)
        {
            TeamDetailViewModel teamDetail =  _teamRepository.GetTeamDetail(team);
            if(teamDetail != null)
            {
                string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                teamDetail.Users.ForEach(u =>
                {
                    if (u.ProfilePicture != null)
                    {
                        if (_awsS3FileStorage.fileExistsInS3(u.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                        {
                            u.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), u.ProfilePicture, true, string.Empty);
                        }
                    }
                    else
                    {
                        u.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                    }
                });
            }
            return teamDetail;
        }

        public List<TrainerListViewModel> GetTrainers(long UserId)
        {
            return _teamRepository.GetTrainers(UserId);
        }

        public void CreateTeam(CreateTeamViewModel team)
        {
            _teamRepository.CreateTeam(team);
        }
        
        public List<UserListViewModel> GetUsers(UserListRequestViewModel request)
        {
            // string path = HttpContext.Current.Server.MapPath("~/ProfileImages"); //Path
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            List<UserListViewModel> users = _teamRepository.GetUsers(request) ?? new List<UserListViewModel>();
            users.ForEach(u =>
            {
                if (u.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(u.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        u.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), u.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    // string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                    u.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            });
            return users;
        }

        public List<long> UpdateTeam(TeamDetailViewModel team)
        {
           return _teamRepository.UpdateTeam(team);
        }

        public void DeleteTeam(long teamId)
        {
            _teamRepository.DeleteTeam(teamId);
        }

        public bool ValidateTeamHandleName(string name)
        {
            return _teamRepository.ValidateTeamHandleName(name);
        }
    }
}
