﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class MyPlaylistService : IMyPlaylistService
    {
        private readonly IMyPlaylistRepository _myPlaylistRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;

        public MyPlaylistService(IMyPlaylistRepository myPlaylistRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _myPlaylistRepository = myPlaylistRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }

        public List<PlaylistListViewModel> GetMyCreatedPlaylist(long userId, bool isLaunched, double offSet)
        {
            List<PlaylistListViewModel> playlists = _myPlaylistRepository.GetMyCreatedPlaylist(userId, isLaunched);
            playlists.ForEach(p =>
            {
                if (p.StartDate != null && p.EndDate != null)
                {
                    p.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.StartDate.Value, offSet);
                    p.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.EndDate.Value, offSet);
                }
            });
            return playlists;
        }

        public List<PlaylistListViewModel> GetMyPlaylist(long userId, bool isCompleted, double offSet)
        {
            List<PlaylistListViewModel> playlists = _myPlaylistRepository.GetMyPlaylist(userId, isCompleted);
            playlists.ForEach(p =>
            {
                if (p.StartDate != null && p.EndDate != null)
                {
                    p.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.StartDate.Value, offSet);
                    p.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.EndDate.Value, offSet);
                }
            });
            return playlists;
        }

        public MyPlaylistDetailViewModel GetMyPlaylistDetail(long playlistId, long userId, bool isSelfCreated, double offSet)
        {
            MyPlaylistDetailViewModel playlistDetail = _myPlaylistRepository.GetMyPlaylistDetail(playlistId, userId, isSelfCreated);
            if (playlistDetail != null)
            {
                if (playlistDetail.StartDate != null && playlistDetail.EndDate != null)
                {
                    playlistDetail.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(playlistDetail.StartDate, offSet);
                    playlistDetail.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(playlistDetail.EndDate, offSet);
                }
                if (!isSelfCreated)
                {
                    if (playlistDetail.TotalMarks != null && playlistDetail.TotalMarks != 0)
                    {
                        playlistDetail.TotalPercentage = (int)Math.Round(((double)playlistDetail.TotalMarksObtained / (double)playlistDetail.TotalMarks) * 100);
                        if(playlistDetail.TotalPercentage < 0)
                        {
                            playlistDetail.TotalPercentage = 0;
                        }
                    }
                    playlistDetail.Folders.ForEach(f =>
                    {
                        if (f.MarksObtained == null)
                        {
                            f.MarksObtained = 0;
                            if (f.TotalMarks == null)
                            {
                                f.TotalMarks = 0;
                            }
                        }

                        f.QuizCompletedCount = (f.NumberOfContent - f.QuizCount) + f.QuizCompletedCount;
                        if (f.NumberOfContent != 0)
                        {
                            f.PlaylistFolderCompletePercentage = (int)Math.Round(((double)f.QuizCompletedCount / (double)f.NumberOfContent) * 100);
                        }
                        else
                        {
                            f.PlaylistFolderCompletePercentage = 100;
                        }
                    });
                }
                if (playlistDetail.Folders.Count > 0)
                    playlistDetail.PlaylistCompletePercentage = playlistDetail.Folders.Sum(f => f.PlaylistFolderCompletePercentage) / playlistDetail.Folders.Count();
            }
            return playlistDetail;
        }

        public ViewResultDetailAndFoldersViewModel ViewResultDetailAndFolders(long playlistId, long userId)
        {
            ViewResultDetailAndFoldersViewModel viewResultDetail = _myPlaylistRepository.ViewResultDetailAndFolders(playlistId, userId);
            if(viewResultDetail != null)
            {
                viewResultDetail.TotalScore = (int)viewResultDetail.folders.Sum(f => f.MarksObtained);
                double playlistTotalMarks = (double)viewResultDetail.folders.Sum(f => f.TotalMarks);
                if(playlistTotalMarks != 0)
                {
                    viewResultDetail.TotalPercentage = (int)Math.Round(((double)viewResultDetail.TotalScore / playlistTotalMarks) * 100);
                }
            }
            return viewResultDetail;
        }

        public List<PlaylistContentResponseViewModel> ViewResultContentList(long playListFolderId, long userId)
        {
            List<PlaylistContentResponseViewModel> playlistContentsResult = _myPlaylistRepository.ViewResultContentList(playListFolderId, userId);
            if (playlistContentsResult != null)
            {
                playlistContentsResult.ForEach(res =>
                {
                    if (res.Content.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(res.Content.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                        res.Content.ContentURL = contentUrls.Item1;
                        res.Content.ThumbnailURL = contentUrls.Item2;
                        res.Content.ContentURLWithExtension = contentUrls.Item3;
                    }
                });
            }
            return playlistContentsResult;
        }

        public CompanyQuizViewModel GetQuizResult(long quizId, long playlistId, long userId, bool isMasterQuiz)
        {
            return _myPlaylistRepository.GetQuizResult(quizId, playlistId, userId, isMasterQuiz);
        }

        public MyPlaylistFolderDetailViewModel GetMyPlaylistFolderDetail(long playListFolderId, long playlistContentMasterId, long userId, string ipAddress, double offSet)
        {
            MyPlaylistFolderDetailViewModel playlistFolderDetail = _myPlaylistRepository.GetMyPlaylistFolderDetail(playListFolderId, playlistContentMasterId, userId, ipAddress);
            if (playlistFolderDetail != null && playlistFolderDetail.StartDate != null && playlistFolderDetail.EndDate != null)
            {
                playlistFolderDetail.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(playlistFolderDetail.StartDate.Value, offSet);
                playlistFolderDetail.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(playlistFolderDetail.EndDate.Value, offSet);
            }
            if (playlistFolderDetail != null && playlistFolderDetail.Folder != null)
            {
                playlistFolderDetail.Folder.QuizCompletedCount = (playlistFolderDetail.Folder.NumberOfContent - playlistFolderDetail.Folder.QuizCount) + playlistFolderDetail.Folder.QuizCompletedCount;
                if (playlistFolderDetail.Folder.NumberOfContent != 0)
                {
                    playlistFolderDetail.Folder.PlaylistFolderCompletePercentage = (int)Math.Round(((double)playlistFolderDetail.Folder.QuizCompletedCount / (double)playlistFolderDetail.Folder.NumberOfContent) * 100);
                }
                else
                {
                    playlistFolderDetail.Folder.PlaylistFolderCompletePercentage = 100;
                }
                if (playlistFolderDetail.PlaylistContent.Content.IsAwsContent)
                {
                    var contentUrls = _awsS3FileStorage.GetContentUrls(playlistFolderDetail.PlaylistContent.Content.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                    playlistFolderDetail.PlaylistContent.Content.ContentURL = contentUrls.Item1;
                    playlistFolderDetail.PlaylistContent.Content.ThumbnailURL = contentUrls.Item2;
                    playlistFolderDetail.PlaylistContent.Content.ContentURLWithExtension = contentUrls.Item3;
                }
            }
            return playlistFolderDetail;
        }

        public List<PlaylistContentResponseViewModel> GetMyPlaylistFolderContentList(long playListFolderId, long contentId)
        {
            List<PlaylistContentResponseViewModel> playlistFolderContents =  _myPlaylistRepository.GetMyPlaylistFolderContentList(playListFolderId, contentId);
            if (playlistFolderContents != null)
            {
                playlistFolderContents.ForEach(res =>
                {
                    if (res.Content.IsAwsContent)
                    {
                        var contentUrls = _awsS3FileStorage.GetContentUrls(res.Content.ContentURL, _awsS3FileStorage.BucketName, _awsS3FileStorage.BucketName + "/" + Constants.S3Folders.thumbnails);
                        res.Content.ContentURL = contentUrls.Item1;
                        res.Content.ThumbnailURL = contentUrls.Item2;
                        res.Content.ContentURLWithExtension = contentUrls.Item3;
                    }
                });
            }
            return playlistFolderContents;
        }

        public List<PlaylistUsersViewModel> GetPlaylistUsers(long playListId, long userId)
        {
            List<PlaylistUsersViewModel>  playlistUsers = _myPlaylistRepository.GetPlaylistUsers(playListId, userId);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            playlistUsers.ForEach(u =>
            {
                if (u.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(u.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        u.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), u.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    u.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            });
            return playlistUsers;
        }

        public PlaylistAssociatedUserViewModel AssociatedUsers(long playListId, long teamId, bool isTeam)
        {
            PlaylistAssociatedUserViewModel associatedUsers = _myPlaylistRepository.AssociatedUsers(playListId, teamId, isTeam);
            if(associatedUsers != null)
            {
                associatedUsers.PlaylistUsers.ForEach(pu =>
                {
                    //calculation of completed percentage and total score
                    pu.FolderResult.ForEach(f =>
                    {
                        f.QuizCompletedCount = (f.NumberOfContent - f.QuizCount) + f.QuizCompletedCount;
                        if (f.NumberOfContent != 0)
                        {
                            f.PlaylistFolderCompletePercentage = (int)Math.Round(((double)f.QuizCompletedCount / (double)f.NumberOfContent) * 100);
                        }
                        else
                        {
                            f.PlaylistFolderCompletePercentage = 100;
                        }
                    });
                    if (pu.FolderResult.Count > 0)
                        pu.CompletedPercentage = pu.FolderResult.Sum(f => f.PlaylistFolderCompletePercentage) / pu.FolderResult.Count();

                    pu.TotalScore = (int)pu.FolderResult.Sum(fr => fr.MarksObtained);

                    //returning just 5 folders to display
                    pu.FolderResult = pu.FolderResult.Take(5).ToList();

                    // user profile pic 
                    string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                    if (pu.ProfilePicture != null)
                    {
                        if (_awsS3FileStorage.fileExistsInS3(pu.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                        {
                            pu.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), pu.ProfilePicture, true, string.Empty);
                        }
                    }
                    else
                    {
                        pu.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                    }
                });
                associatedUsers.NumnberOfUsers = associatedUsers.PlaylistUsers.Count();
            }
            return associatedUsers;
        }

        public List<AssociatedTeamListViewModel> GetPlaylistAssociatedTeams(long playListId)
        {
            return _myPlaylistRepository.GetPlaylistAssociatedTeams(playListId);
        }

        public List<PlaylistListViewModel> GetMyDashboardPlaylist(long userId, double offSet)
        {
            List<PlaylistListViewModel> dashboardPlaylist = _myPlaylistRepository.GetMyDashboardPlaylist(userId);
            dashboardPlaylist.ForEach(dp =>
            {
                if (dp.StartDate != null && dp.EndDate != null)
                {
                    dp.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(dp.StartDate.Value, offSet);
                    dp.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(dp.EndDate.Value, offSet);
                }

                if (dp.NumberOfStudents > 0)
                {
                    dp.PlaylistCompletePercentage = (int)Math.Round(((double)dp.NumberOfUserCompletedPlaylist / (double)dp.NumberOfStudents) * 100);
                }
            });
            return dashboardPlaylist;
        }

        public void DeletePlaylist(PlaylistViewModel playlist)
        {
            _myPlaylistRepository.DeletePlaylist(playlist);
        }

        public long CloneMyCreatedPlaylist(PlaylistViewModel playlist)
        {
            return _myPlaylistRepository.CloneMyCreatedPlaylist(playlist);
        }
    }
}
