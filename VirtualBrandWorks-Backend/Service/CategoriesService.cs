﻿using Service.Interfaces;
using Repository.Interfaces;
using VirtualBrandWork.Entity.ViewModels;
using System.Collections.Generic;
using VirtualBrandWork.Common;

namespace Service
{
    public class CategoriesService : ICategoriesService
    {
        private readonly ICategoriesRepository _categoriesRepository;
        public CategoriesService(ICategoriesRepository categoriesRepository)
        {
            _categoriesRepository = categoriesRepository;
        }
        public bool IsCategoryExists(CategoryViewModel category)
        {
            return _categoriesRepository.IsCategoryExists(category);
        }
        public List<CategoryViewModel> GetAllCategories()
        {
            return _categoriesRepository.GetAllCategories();
        }
        public CategoryViewModel GetCategory(long categoryId)
        {
            return _categoriesRepository.GetCategory(categoryId);
        }
        public void Create(CategoryViewModel category)
        {
            _categoriesRepository.Create(category);
        }
        public void Update(CategoryViewModel category)
        {
            _categoriesRepository.Update(category);
        }
        public Response Delete(long categoryId)
        {
           return _categoriesRepository.Delete(categoryId);
        }

        public bool IsAssociatedCompany(long categoryId)
        {
            return _categoriesRepository.IsAssociatedCompany(categoryId);
        }

    }
}
