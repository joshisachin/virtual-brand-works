﻿using System;
using Service.Interfaces;
using VirtualBrandWork.Entity.ViewModels;
using Repository.Interfaces;
using static VirtualBrandWork.Entity.Constants;
using System.Collections.Generic;
using Repository.Model;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Notification;

namespace Service
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _notificationTypeRepository;
        public NotificationService(INotificationRepository notificationTypeRepository)
        {
            _notificationTypeRepository = notificationTypeRepository;
        }
        public NotificationTypeViewModel GetNotificationData(VirtualBrandWork.Entity.Constants.NotificationType notificationType)
        {
            return _notificationTypeRepository.GetNotificationData(notificationType);
        }

        public void SaveFcmToken(long userId, string token)
        {
            _notificationTypeRepository.SaveFcmToken(userId, token);
        }

        public void SaveNotificationData(long userId, long? notificationTypeId, bool isSuccess, string token, string messageId, string errorMessage, string multicastId, string body, string title)
        {
            _notificationTypeRepository.SaveNotificationData(userId, notificationTypeId, isSuccess, token, messageId, errorMessage, multicastId, body, title);
        }

        public List<FCMUserToken> GetFCMTokens(long[] userIds)
        {
            return _notificationTypeRepository.GetFcmTokens(userIds);
        }

        public List<Notifications> GetNotifications(long userId)
        {
            return _notificationTypeRepository.GetNotifications(userId);
        }

        public void ReadNotifications(long userId)
        {
            _notificationTypeRepository.ReadNotifications(userId);
        }

        public void SendNotifications(List<long> usersToBeNotified, string msgBody, string msgTitle)
        {
            foreach (var item in usersToBeNotified)
            {
                SaveNotificationData(item, (long)Constants.NotificationType.AdddedInTeam, true, null, null, null, null, msgBody, msgTitle);
            }
            List<FCMUserToken> objFCMTokens = GetFCMTokens(usersToBeNotified.ToArray());

            FcmNotificationService objNotificationService = new FcmNotificationService();

            objNotificationService.SendPushNotification(objFCMTokens, msgTitle, msgBody);
        }
    }
}
