﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IMyPlaylistService
    {
        List<PlaylistListViewModel> GetMyCreatedPlaylist(long userId, bool isLaunched, double offSet);
        List<PlaylistListViewModel> GetMyPlaylist(long userId, bool isCompleted, double offSet);
        MyPlaylistDetailViewModel GetMyPlaylistDetail(long playlistId, long userId, bool isSelfCreated, double offSet);
        ViewResultDetailAndFoldersViewModel ViewResultDetailAndFolders(long playlistId, long userId);
        List<PlaylistContentResponseViewModel> ViewResultContentList(long playListFolderId, long userId);
        CompanyQuizViewModel GetQuizResult(long quizId, long playlistId, long userId, bool isMasterQuiz);
        MyPlaylistFolderDetailViewModel GetMyPlaylistFolderDetail(long playListFolderId, long playlistContentMasterId, long userId, string ipAddress, double offSet);
        List<PlaylistContentResponseViewModel> GetMyPlaylistFolderContentList(long playListFolderId, long contentId);
        List<PlaylistUsersViewModel> GetPlaylistUsers(long playListId, long userId);
        PlaylistAssociatedUserViewModel AssociatedUsers(long playListId, long teamId, bool isTeam);
        List<AssociatedTeamListViewModel> GetPlaylistAssociatedTeams(long playListId);
        List<PlaylistListViewModel> GetMyDashboardPlaylist(long userId, double offSet);
        void DeletePlaylist(PlaylistViewModel playlist);
        long CloneMyCreatedPlaylist(PlaylistViewModel playlist);
    }
}
