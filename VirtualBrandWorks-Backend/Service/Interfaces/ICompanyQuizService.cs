﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface ICompanyQuizService
    {
        void AddQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel, long userId);
        long SaveQuiz(CompanyQuizCreateViewModel companyQuiz);
        CompanyQuizViewModel GetQuizDetails(long quizId, bool isMasterFolder, bool isTurnedOffExistingQuiz);
        void UpdateQuestion(List<QuestionAnswerViewModel> questionAnswerViewModel, long userId);
        void DeleteQuiz(long quizId);
        void DeleteQuestion(long questionId);
        CompanyQuizViewModel GetQuizToPlay(long quizId, long userId, bool isMasterContent, bool isTurnedOffExistingQuiz);
        PassFailViewModel SubmitQuiz(QuestionAnswerAttemptViewModel quizResponse);
    }
}
