﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface ICompanyService
    {
        List<UserViewModel> GetUsersByCompanyId(long companyId);
        List<CompanyViewModel> GetAllCompany();
        CompanyViewModel ActivateOrDeactivateCompany(long companyId);
    }
}
