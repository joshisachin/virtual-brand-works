﻿using System.Collections.Generic;
using System.Web.Mvc;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface ITagService
    {
        List<TagViewModel> GetTags();
        void Create(TagViewModel tag);
        bool IsTagExists(TagViewModel tag);
        List<TagViewModel> GetTagSuggestion(string type, string suggestion, long userId);
        List<TagViewModel> GetTagsWithIndustries();
        void EditTag(TagViewModel tagViewModel);
        TagViewModel GetTagById(long tagId);
        void Delete(long tagId);
    }
}
