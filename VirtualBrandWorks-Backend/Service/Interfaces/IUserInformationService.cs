﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IUserInformationService
    {
        List<CountryViewModel> GetCountries();
        List<StateViewModel> GetStates(int countryID);
        List<CityViewModel> GetCities(int stateID);
        List<CityViewModel> GetLocation(string location);
        UserProfileViewModel GetUserProfile(UserProfileViewModel user);
        List<UserReponseViewModel> GetCompanyUsers(UserRequestViewModel users);
        void AddProfileImage(UserRequestViewModel userRequestViewModel);
        void UpdateUserRole(UserRequestViewModel userRequestViewModel);
        bool UpdateUserStatus(UserRequestViewModel userRequestViewModel);
        bool CheckUserLimitForCompany(CompanyViewModel company);
        bool IsUserExist(string email);
        void AddUserInvite(UserInviteViewModel userInvite);
        UserInviteViewModel GetUserInvite(string email);
        bool UpdateNotificationSetting(UserRequestViewModel user);
        UserProfileViewModel UpdateUserProfile(UserProfileViewModel userProfileViewModel);
        CompanyInfoViewModel UpdateCompanyProfile(CompanyInfoViewModel companyInfo);
        UserProfileViewModel GetUserProfileStatus(UserProfileViewModel user);
        List<PlaylistListViewModel> GetUserAssociatedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist);
        List<PlaylistListViewModel> GetUserAssignedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist);
        List<AssociatedTeamListViewModel> GetUserAssociatedTeam(long userId, string search);
        List<AssociatedUsersListViewModel> GetUserAssociatedUsers(long userId, string search);
        Response CanUserRegister(string emailId);
        void UpdateCompanyLogo(CompanyLogoRequestViewModel company);
        CompanyInfoViewModel GetCompanyInfo(long userId);
        void DeleteCompanyLogo(CompanyLogoRequestViewModel company);
        void DeleteProfileImage(CompanyLogoRequestViewModel company);
        void SaveLocations(LocationsViewModel locationsViewModel, long userId);
        void DeleteLocation(int locationId);
        bool LocationExists(LocationsViewModel objLocationViewModel, long userId);
        List<LocationsViewModel> GetSignUpLocations(LocationsSearch location);
        List<LocationsViewModel> GetLocations(LocationsSearch locations, long userId);
        List<LocationsViewModel> GetLocationsByCompanyId(long companyId);
    }
}
