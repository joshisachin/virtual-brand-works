﻿using System;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IAWSS3FileStorage
    {
        string PublicKey { get; }
        string PrivateKey { get; }
        string BucketName { get; }

        string GeneratePreSignedURLForPopUp(string childFolder, string fileName, bool flag, string mimeType);
        bool CreateThumbnailFromFile(string contentURL, string mimeType, Resolution resolution=null);
        bool fileExistsInS3(string fileKey, string bucketName);
        string GetPolicyString(string fileId, string redirectUrl);
        void UploadFile(string fileName, string message, VirtualBrandWork.Entity.Constants.S3Folders s3Folder);
        void DeleteFile(string fileName, VirtualBrandWork.Entity.Constants.S3Folders s3Folder);
        Tuple<string, string, string> GetContentUrls(string contentUrl, string bucketName, string thumbnailBucketName);
    }
}
