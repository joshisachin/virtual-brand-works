﻿using Repository;
using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IMasterFolderService
    {
        bool IsFolderExists(MasterFolderViewModel folder);
        List<MasterFolderViewModel> GetAllFolders();
        List<FolderViewModel> GetFilterFolders(CompanyContentFolderRequestViewModel request);
        MasterFolderViewModel GetFolder(long folderId);
        Response Create(MasterFolderViewModel folder);
        void Update(MasterFolderViewModel folder);
        Response Delete(long folderId);
        Response GetFoldersByCategoyId(long categoryId);
        bool IsFolderShortNameExists(MasterFolderViewModel folderViewModel);
    }
}
