﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface ITeamService
    {
        List<TeamListViewModel> GetTeamList(TeamListViewModel team);
        TeamDetailViewModel GetTeamDetail(TeamDetailViewModel team);
        List<TrainerListViewModel> GetTrainers(long UserId);
        void CreateTeam(CreateTeamViewModel team);
        List<UserListViewModel> GetUsers(UserListRequestViewModel request);
        List<long> UpdateTeam(TeamDetailViewModel team);
        void DeleteTeam(long teamId);
        bool ValidateTeamHandleName(string name);
    }
}
