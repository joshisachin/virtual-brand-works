﻿using System.Collections.Generic;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IMasterContentService
    {
        List<MasterContentViewModel> GetAllContents();
        MasterContentViewModel GetContent(long contentId);
        void Create(MasterContentViewModel content);
        void Update(MasterContentViewModel content);
        void Delete(long contentId);
        List<ContentViewModel> GetMasterContentsByFolderId(long masterFolderId);
        ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long masterFolderId, long userId);
        int PublishUnpublishContent(long id);
        FolderContentViewModel GetMasterFolderContents(FolderViewModel folder, long userId);
        void SaveSelectedMasterContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId);
        List<FolderViewModel> GetMasterContentSelectedFolders(long UserId);
        void UpdateMasterContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId);
        FolderContentDetailViewModel GetMasterFolderContentDetail(ContentViewModel requestContent, long userId);
        List<ContentViewModel> GetMasterContentsByType(FolderContentRequestViewModel folder);
    }
}
