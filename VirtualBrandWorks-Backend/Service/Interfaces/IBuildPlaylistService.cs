﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IBuildPlaylistService
    {
        int SavePlaylistFolders(long playlistId, long userId);
        List<PlaylistFolderViewModel> GetPlaylistFolders(long playlistId, long userId, bool isAllContentCount);
        void UpdateSortOrderForPlaylistFolderUserSaved(List<PlaylistFolderUserSavedViewModel> folders);
        long CreatePlaylist(PlaylistViewModel playlist);
        FolderContentViewModel GetPlayListContents(long playListFolderId, bool isPlaylistExist, long userId);
        void UpdatePlayListContents(PlaylistContentViewModel playlistContentViewModel, long userId);
        List<FolderViewModel> GetPlaylistFooterData(long userId);
        void SaveBuildPlaylistStatus(BuildPlaylistSavedStateViewModel state);
        BuildPlaylistSavedStateViewModel GetBuildPlaylistSavedState(long playlistId, long userId);
        List<long> AssignPlaylistTo(PlaylistAssignToViewModel playlistDetail, double offSet);
        PlaylistDetailViewModel GetPlaylistDetail(long playlistId);
        PlaylistAssignedUserOrTeam GetPlaylistAssignedUserAndTeam(long playlistId);
        List<long> LaunchPlaylist(PlaylistUpdateViewModel playlist);
        PlaylistAssignedUserOrTeam GetUsersAndTeamsForAssignPlaylistToMore(long playlistId, int locationId, long userId);
        PlaylistViewModel GetPlaylistForUpdate(long playlistId, long userId);
        List<PlaylistContentResponseViewModel> GetPlayListContentsForAddQuestion(long playListFolderId, long userId);
        void UpdateSortOrderForPlaylistFolder(long playlistId, long playlistFolderId, int destinationSortOrder);
        void TurnOffExistingQuiz(long playlistContentMasterId, bool isTurnedOffExistingQuiz);
        void TurnOffExistingQuizForAll(long playlistFolderId, bool isTurnedOffExistingQuiz);

        void UpdatePlaylist(PlaylistNameAndDescriptionUpdateViewModel playlist);
    }
}
