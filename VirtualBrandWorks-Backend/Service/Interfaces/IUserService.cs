﻿using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Entity.ViewModels;
using VirtualBrandWork.Common;

namespace Service.Interfaces
{
    public interface IUserService
    {
        UserContext ValidateUser(string username, string password);
        UserViewModel ValidateUserWithEmail(string emailId);
        UserContext ValidateExternalLogin(string email);
        bool SetPasswordResetCode(UserViewModel user, string resetCode);
        int ResetPassword(ResetPasswordViewModel user);
        bool ChangePassword(ChangePasswordViewModel changePassword, int userId);
        bool ValidatePasswordResetCode(string resetCode);
        bool ValidateCompanyStatus(long userId);
        UserInviteViewModel GetUserInvite(string email);
        //Method for sign up of user/trainer for a company
        void SignUpAsUser(SignUpViewModel signUpUser);
        List<ValidationSummaryViewModel> ValidateUserEmailAndCompanyName(SignUpViewModel signUpViewModel);
        Response SaveAdminDetails(SignUpViewModel signUpViewModel);
        Response DeleteSignUpDetails(string email);
        Response UpdateSignUpDetailsPaymentSuccess(string email, string customerId, string subscriptionId,bool isTrial);
        UserMyPlanNotiFicationViewModel ChangeSubscriptionPlan(long userId, long planId, string stripeSubscriptionPlanId, string oldPlanName);
        UserMyPlanNotiFicationViewModel CancelSubscriptionPlan(long userId, long planId);
        string GetCreateCardCheckoutSession(long userId, string successUrl, string cancelUrl);
        void SetDefaultPaymentMethod(long userId, string paymentMethodId);
        PaymentMethodViewModel PaymentMethods(long userId);
        bool UserLoginStatus(long userId, string statusCode);
        bool RemoveLoginStatus(long userId, string statusCode);
        void SaveUsersLoginStatus(string email, string statusCode);
        UserContext GetUserContextFromUserId(long userId);
        string GetEmailIdFromUserId(long userId);
        long GetCompanyIdFromUserId(long userId);
        void UpdateBillingAddress(long userId, MyPlanBillingAddressViewModel billingDetails);
        UserMyPlanNotiFicationViewModel UpdatePaymentMethodSuccess(long userId, string sessionId);
        UserMyPlanNotiFicationViewModel GetUserNameAndEmail(long userId);
        bool ValidateSubscription(long userId);
        UserViewModel ValidateUserWithHandleName(string hanadleName);
    }
}
