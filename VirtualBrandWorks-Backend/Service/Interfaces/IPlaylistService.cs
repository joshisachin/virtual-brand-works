﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
   public interface IPlaylistService
    {
        MessageViewModel SendMessage(MessageViewModel messageViewModel);
        List<MessageViewModel> ReceiveMessage(MessageViewModel messageViewModel);
        List<PlaylistTopPerformerViewModel> GetPlaylistTopPerofermers(long playListId, bool isTopTen);
        List<PlaylistLeaderboardViewModel> GetPlaylistLeaderboard(long playListId, int? sortBy);
        List<PlaylistDropdownViewModel> GetAllPlaylist(long userId);
        List<LeaderboardByTeamViewModel> GetPlaylistLeaderboardByTeam(long playListId, int? sortBy);
    }
}
