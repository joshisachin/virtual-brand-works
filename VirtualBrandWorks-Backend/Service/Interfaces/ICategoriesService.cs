﻿using System.Collections.Generic;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface ICategoriesService
    {
        bool IsCategoryExists(CategoryViewModel categoryId);
        List<CategoryViewModel> GetAllCategories();
        CategoryViewModel GetCategory(long categoryId);
        void Create(CategoryViewModel category);
        void Update(CategoryViewModel category);
        Response Delete(long categoryId);
        bool IsAssociatedCompany(long categoryId);
    }
}
