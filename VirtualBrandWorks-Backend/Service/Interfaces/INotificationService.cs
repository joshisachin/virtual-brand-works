﻿using Repository.Model;
using System.Collections.Generic;
using VirtualBrandWork.Entity.ViewModels;
using static VirtualBrandWork.Entity.Constants;

namespace Service.Interfaces
{
    public interface INotificationService
    {
        NotificationTypeViewModel GetNotificationData(VirtualBrandWork.Entity.Constants.NotificationType notificationType);
        void SaveNotificationData(long userId, long? notificationTypeId, bool isSuccess, string token, string messageId, string errorMessage, string multicastId, string body, string title);
        void SaveFcmToken(long userId, string token);
        List<FCMUserToken> GetFCMTokens(long[] userIds);

        List<Notifications> GetNotifications(long userId);
        void ReadNotifications(long userId);

        void SendNotifications(List<long> usersToBeNotified, string msgBody, string msgTitle);

    }
}
