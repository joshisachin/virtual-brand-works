﻿using Stripe;
using Stripe.Checkout;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IStripePaymentUtility
    {
        StripeSetupModel Setup(string planId);
        StripeCheckoutSessionModel GetCreateCheckoutSession(string planId, string email, string subscription, string amount, string billingCycle, string firstName, string lastName, string successUrl, string cancelUrl);
        Product CreateProduct(string name, string description);

        Product UpdateProduct(string name, string description, string productId);
        Plan CreatePlan(long amount, string currency, string interval, string productId);
        Plan DeletePlan(string planId);
        Product DeleteProduct(string productId);
        Session GetCheckoutSession(string sid);
        Subscription UpdateSubscription(string subscriptionId, string planId, bool istrial);
        PaymentMethod DetachPaymethod(string paymentMethodId);
        StripeList<PaymentMethod> PaymentMethods(string customerId);
        Subscription CancelSubscription(string subscriptionId);
        string GetCreateCardCheckoutSession(string customerId,string subscriptionId, string successUrl, string cancelUrl);
        Customer SetDefaultPaymentMethod(string customerId, string paymentMethodId);
        string GetDefaultPaymentMethodId(string customerId);
        Subscription GetSubscription(string subscriptionId);
        PaymentMethod UpdatePaymentMethod(string paymentMethodId, MyPlanBillingAddressViewModel billingDetails);
        SetupIntent GetSetupIntentObject(string sid);
        Subscription CancelSubscriptionAtPeriodEnd(string subscriptionId);
    }
}
