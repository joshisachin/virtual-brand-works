﻿using VirtualBrandWork.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IQuizService
    {
        Response AddQuestion(QuestionAnswerViewModel questionAnswerViewModel);
        Response SaveQuiz(QuizViewModel quizVirtualModel);
        QuizViewModel GetQuizDetails(long quizId);
        QuestionAnswerViewModel GetQuestionDetails(long questionId);
        Response UpdateQuestion(QuestionAnswerViewModel questionAnswerViewModel);
        Response DeleteQuiz(long quizId);
        Response DeleteQuestion(long questionId);
    }
}
