﻿using Repository;
using Repository.Model;
using System.Collections.Generic;

namespace Service.Interfaces
{
    public interface IUserTagService
    {
        void Add(UserTag tag);
        List<long?> GetUsersFromTags(List<long> tagIds, long userId);
    }
}
