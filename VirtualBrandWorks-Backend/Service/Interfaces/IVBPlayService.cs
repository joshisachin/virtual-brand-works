﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IVBPlayService
    {
        List<VBPlayViewModel> GetVBPlayContest(long userId, Constants.ContestType contestType, double offSet);
        VBPlayViewModel CreateContest(VBPlayCreateViewModel vbPlayContest, long userId, double offSet);
        long JoinContest(VBPlayViewModel vbPlayViewModel, long userId);
        VBPlayViewModel GetContest(long contestId);
        ContestChallengeAndEntriesViewModel ViewContest(long userId, long contestId, string search, double offSet);
        VBPlayViewModel GetContestConfirmation(long contestId);
        ChallengeEntryViewModel GetContestEntryConfirmation(long contestEntryId);
        void BroadcastContest(ContestBroadcastRequestViewModel contest);
        List<BroadcastedContentViewModel> GetBroadcastedContent(string search, long userId);
        long LikeContest(VBPlayContentCommonViewModel contestContent, long userId);
        void ContestContentViews(SharedContentViewModel contest);
        VBPlayViewModel SubmitContestEntry(ContestEntrySubmitViewModel contestEntry);
        ChallengeEntryViewModel GetContestEntry(long contestEntryId);
        BroadcastedContentViewModel GetSharedContent(long contentId, Constants.SharedContentType contentType, long userId);
        ChallengeEntryViewModel UpdateContestEntry(VBPlayCreateViewModel contestEntry);
        EarnedPointsViewModel GetEarnedPoints(long userId);
        bool DeleteContest(VBPlayViewModel contest);
        void ExtendContentDate(long contestId, long notificationId, string type);
        void OptOutNotification(long notificationId);
        void UpdateVbPlayContentVisibility(ContestVisibilityViewModel contest);

        VBPlayViewModel SubmitContest(ContestEntrySubmitViewModel contestEntry);

        VBPlayViewModel UpdateContest(VBPlayUpdateViewModel vbPlayContest);

        List<ReportAbuseViewModel> GetAllReportAbuse(long userId, double offSet);
        void ReportAbuse(long contestId, long userId);
        void DeleteReportAbuse(long contestId);
        void SetNotificationExtenstionFlag(BroadcastedContentViewModel broadcastedContent, bool flag);
        List<long> GetUserIdsOfAdminAndTrainerOfACompany(long userId);
        bool IsContestAlreadyReported(long contestId, long userId);
    }
}
