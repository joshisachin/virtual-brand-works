﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface ICompanyContentService
    {
        List<FolderViewModel> GetCompanyContentFolders(CompanyContentFolderRequestViewModel request);
        ContentResponseViewModel CreateContent(CompanyContentViewModel companyContent);
        CompanyContentResponse GetContentConfirmation(ContentViewModel companyContent);
        FolderContentDetailViewModel GetCompanyFolderContentDetail(ContentViewModel content, long userId);
        QuizViewModel GetQuizDetails(long contentId);
        FolderDetailViewModel CreateFolder(CompanyFolderViewModel folderViewModel);
        void UpdateFolder(CompanyFolderViewModel folderViewModel);
        bool IsCompanyFolderExists(CompanyFolderViewModel folderViewModel);
        bool IsCompanyFolderShortNameExists(CompanyFolderViewModel folderViewModel);
        FolderContentViewModel GetCompanyFolderContents(FolderViewModel folder, long userId);
        void SaveSelectedCompanyContentFolder(UserSavedFoldersViewModel SavedFolders, long UserId);
        List<FolderViewModel> GetCompanyContentSelectedFolders(long userId);
        void UpdateCompanyContentFolderOrders(List<SortFoldersViewModel> SortOders, long UserId);
        ContentPreviewViewModel GetFolderAndContentForPreviewPopUp(long folderId);
        void PublishCompanyContent(ContentViewModel content);
        List<FolderDetailViewModel> GetCompanyFolderList(long userId);
        List<ContentViewModel> GetCompanyContentsByType(FolderContentRequestViewModel folder);
        List<ContentViewModel> GetUserCreatedContentList(long userId);
        bool DeleteCompanyContent(long contentId);
        bool BroadCastCompanyContent(ContentViewModel content);
        void CompanyContentView(ContentViewModel requestContent, long userId);
        void UpdateCompanyContentVisibility(CompanyContentVisibilityViewModel content);
    }
}
