﻿using System.Collections.Generic;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface ISubscriptionService
    {
        IEnumerable<SubscriptionViewModel> GetAllSubscription();
        SubscriptionViewModel GetSubscriptionById(SubscriptionViewModel subscription);
        void Create(SubscriptionViewModel subscription);
        UserMyPlanNotiFicationViewModel Update(SubscriptionViewModel subscription);
        void Delete(SubscriptionViewModel subscriptionId);
        List<SubscriptionViewModel> GetSubscriptionListForAdmin(bool noTrial);
        List<SubscriptionPlanPriceViewModel> GetSubscriptionPlanPrice(string productId);
        CompanySubscriptionViewModel GetSubscriptionDetailsForCompany(long companyId);
    }
}
