﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBrandWork.Entity.ViewModels;

namespace Service.Interfaces
{
    public interface IPrizeStoreService
    {
        PrizeCreateHelperViewModel GetPrizeCategories();
        void CreatePrize(PrizeStoreViewModel prize, long userId, double offSet);
        List<PrizeListViewModel> GetPrizeStore(long userId, double offSet, int? sortBy);
        List<PrizeListViewModel> GetMyCreatedPrize(long userId, double offSet, int? sortBy);
        PrizeStoreViewModel GetPrizeInfo(long prizeId);
        void UpdatePrize(PrizeStoreViewModel prize, double offSet);
        bool UpdatePrizeStatus(PrizeStatusViewModel prize);
        AvailPrizeResponseViewModel AvailPrize(long prizeId, long userId);
        bool DeletePrize(long prizeId);
    }
}
