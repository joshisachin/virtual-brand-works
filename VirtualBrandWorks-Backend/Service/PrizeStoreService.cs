﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.Common;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class PrizeStoreService :IPrizeStoreService
    {
        private readonly IPrizeStoreRepository _prizeStoreRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;

        public PrizeStoreService(IPrizeStoreRepository prizeStoreRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _prizeStoreRepository = prizeStoreRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }

        public PrizeCreateHelperViewModel GetPrizeCategories()
        {
            return _prizeStoreRepository.GetPrizeCategories();
        }
        public void CreatePrize(PrizeStoreViewModel prize, long userId, double offSet)
        {
            if(!string.IsNullOrEmpty(prize.ThumbnailImage))
            {
                string imageName = Guid.NewGuid() + ".jpg"; // create a unique name
                prize.ThumbnailImage = ImageHelper.GetBase64Stripped(prize.ThumbnailImage);
                _awsS3FileStorage.UploadFile(imageName, prize.ThumbnailImage, Constants.S3Folders.PrizeThumbnail);
                prize.ThumbnailImage = imageName;
            }
            if(prize.StartDate != null && prize.EndDate != null)
            {
                prize.StartDate = DateTimeHelper.ConvertDateTimeToUtc(prize.StartDate.Value, offSet);
                prize.EndDate = DateTimeHelper.ConvertDateTimeToUtc(prize.EndDate.Value, offSet);
            }
            _prizeStoreRepository.CreatePrize(prize, userId);
        }

        public List<PrizeListViewModel> GetPrizeStore(long userId, double offSet, int? sortBy)
        {
            List<PrizeListViewModel> prizes = _prizeStoreRepository.GetPrizeStore(userId);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";//for now using default propic
            prizes.ForEach(p =>
            {
                if (p.EndDate != null && p.StartDate != null)
                {
                    p.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.EndDate.Value, offSet);
                    p.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.StartDate.Value, offSet);
                }
                if (!string.IsNullOrEmpty(p.ThumbnailImage))
                {
                    if (_awsS3FileStorage.fileExistsInS3(p.ThumbnailImage, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.PrizeThumbnail.ToString()}"))
                    {
                        p.ThumbnailImage = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.PrizeThumbnail.ToString(), p.ThumbnailImage, true, string.Empty);
                    }
                }
                else
                {
                    p.ThumbnailImage = Path.Combine(path, Constants.DEFAULT_PRIZE_THUMBNAIL);
                }
            });
            if(prizes.Count > 0 && sortBy.HasValue && sortBy != 0)
            {
                prizes = FilterPrize(prizes, sortBy.Value);
            }
            return prizes;
        }
        public List<PrizeListViewModel> GetMyCreatedPrize(long userId, double offSet, int? sortBy)
        {
            List<PrizeListViewModel> myCreatedPrizes = _prizeStoreRepository.GetMyCreatedPrize(userId);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";//for now using default propic
            myCreatedPrizes.ForEach(p =>
            {
                if (p.EndDate != null && p.StartDate != null)
                {
                    p.EndDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.EndDate.Value, offSet);
                    p.StartDate = DateTimeHelper.ConvertUtcToLocalDateTime(p.StartDate.Value, offSet);
                }
                if (!string.IsNullOrEmpty(p.ThumbnailImage))
                {
                    if (_awsS3FileStorage.fileExistsInS3(p.ThumbnailImage, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.PrizeThumbnail.ToString()}"))
                    {
                        p.ThumbnailImage = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.PrizeThumbnail.ToString(), p.ThumbnailImage, true, string.Empty);
                    }
                }
                else
                {
                    p.ThumbnailImage = Path.Combine(path, Constants.DEFAULT_PRIZE_THUMBNAIL);
                }
            });
            if (myCreatedPrizes.Count > 0 && sortBy.HasValue && sortBy != 0)
            {
                myCreatedPrizes = FilterPrize(myCreatedPrizes, sortBy.Value);
            }
            return myCreatedPrizes;
        }

        public PrizeStoreViewModel GetPrizeInfo(long prizeId)
        {
            PrizeStoreViewModel prizeInfo = _prizeStoreRepository.GetPrizeInfo(prizeId);
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";//for now using default propic
            if (prizeInfo != null)
            {
                if (!string.IsNullOrEmpty(prizeInfo.ThumbnailImage))
                {
                    if (_awsS3FileStorage.fileExistsInS3(prizeInfo.ThumbnailImage, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.PrizeThumbnail.ToString()}"))
                    {
                        prizeInfo.ThumbnailImage = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.PrizeThumbnail.ToString(), prizeInfo.ThumbnailImage, true, string.Empty);
                    }
                }
                else
                {
                    prizeInfo.ThumbnailImage = Path.Combine(path, Constants.DEFAULT_PRIZE_THUMBNAIL);
                }
            }
            return prizeInfo;
        }

        public void UpdatePrize(PrizeStoreViewModel prize, double offSet)
        {
            //var oldPrizeThumbnail = string.Empty;
            if(prize != null)
            {
                if (!string.IsNullOrEmpty(prize.ThumbnailImage))
                {
                    //oldPrizeThumbnail = _prizeStoreRepository.GetOldThumbnailForPrize(prize.PrizeId);
                    string imageName = Guid.NewGuid() + ".jpg"; // create a unique name
                    prize.ThumbnailImage = ImageHelper.GetBase64Stripped(prize.ThumbnailImage);
                    _awsS3FileStorage.UploadFile(imageName, prize.ThumbnailImage, Constants.S3Folders.PrizeThumbnail);
                    prize.ThumbnailImage = imageName;
                }
                if (prize.StartDate != null && prize.EndDate != null)
                {
                    prize.StartDate = DateTimeHelper.ConvertDateTimeToUtc(prize.StartDate.Value, offSet);
                    prize.EndDate = DateTimeHelper.ConvertDateTimeToUtc(prize.EndDate.Value, offSet);
                }
                var oldPrizeThumbnail = _prizeStoreRepository.UpdatePrize(prize);
                if (!String.IsNullOrEmpty(oldPrizeThumbnail))
                {
                    _awsS3FileStorage.DeleteFile(oldPrizeThumbnail, Constants.S3Folders.PrizeThumbnail);
                }
            }
        }

        public bool UpdatePrizeStatus(PrizeStatusViewModel prize)
        {
            return _prizeStoreRepository.UpdatePrizeStatus(prize);
        }

        public AvailPrizeResponseViewModel AvailPrize(long prizeId, long userId)
        {
            return _prizeStoreRepository.AvailPrize(prizeId, userId);
        }

        public bool DeletePrize(long prizeId)
        {
            return _prizeStoreRepository.DeletePrize(prizeId);
        }

        //private method
        private List<PrizeListViewModel> FilterPrize(List<PrizeListViewModel> prizes, int sortBy)
        {
            switch (sortBy)
            {
                case (int)Constants.PrizeFilter.Name :
                    prizes = prizes.OrderBy(p => p.Title).ToList();
                    break;
                case (int)Constants.PrizeFilter.MaxPoints:
                    prizes = prizes.OrderByDescending(p => p.Points).ToList();
                    break;
                case (int)Constants.PrizeFilter.MinPoints:
                    prizes = prizes.OrderBy(p => p.Points).ToList();
                    break;
                case (int)Constants.PrizeFilter.EndDate:
                    prizes = prizes.OrderBy(p => p.EndDate).ToList();
                    break;
                default :
                    break;
            }
            return prizes;
        }
    }
}
