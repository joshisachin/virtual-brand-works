﻿using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class UserInformationService : IUserInformationService
    {
        private readonly IUserInformationRepository _userInformationRepository;
        private readonly IAWSS3FileStorage _awsS3FileStorage;

        public UserInformationService(IUserInformationRepository userInformationRepository, IAWSS3FileStorage awsS3FileStorage)
        {
            _userInformationRepository = userInformationRepository;
            _awsS3FileStorage = awsS3FileStorage;
        }

        public List<CountryViewModel> GetCountries()
        {
            return _userInformationRepository.GetCountries();
        }

        public List<StateViewModel> GetStates(int countryID)
        {
            return _userInformationRepository.GetStates(countryID);
        }

        public List<CityViewModel> GetCities(int stateID)
        {
            return _userInformationRepository.GetCities(stateID);
        }
        public List<CityViewModel> GetLocation(string location)
        {
            return _userInformationRepository.GetLocation(location);
        }

        public List<UserReponseViewModel> GetCompanyUsers(UserRequestViewModel request)
        {
            //string path = HttpContext.Current.Server.MapPath("~/ProfileImages"); //Path
            string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
            List<UserReponseViewModel> users = _userInformationRepository.GetCompanyUsers(request) ?? new List<UserReponseViewModel>();
            users.ForEach(u =>
            {
                if (u.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(u.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        u.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), u.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    // string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                    u.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
                u.AssignedPlayList = (_userInformationRepository.GetUserAssignedPlaylistList(new UserAssociatedPlaylistRequestViewModel { UserId = u.UserId , IsLaunched = true })).Count();
            });
            return users;
        }

        public UserProfileViewModel GetUserProfile(UserProfileViewModel user)
        {
            UserProfileViewModel userProfile = _userInformationRepository.GetUserProfile(user);
            if(userProfile != null)
            {
                userProfile.DateOfJoining = userProfile.DateOfJoining.ToLocalTime();
                if (userProfile.ProfilePicture != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(userProfile.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                    {
                        userProfile.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), userProfile.ProfilePicture, true, string.Empty);
                    }
                }
                else
                {
                    string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                    userProfile.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
                }
            }
            return userProfile;
        }

        public void AddProfileImage(UserRequestViewModel user)
        {
            // string path = HttpContext.Current.Server.MapPath("~/ProfileImages"); //Path
            //string path = $"{AppUtil.ProfileImagesFolder}";
            //if (!Directory.Exists(path))
            //    Directory.CreateDirectory(path); //Create directory if it doesn't exist

            string imageName = Guid.NewGuid() + ".jpg"; // create a unique name

            _awsS3FileStorage.UploadFile(imageName, user.ProfilePictue, Constants.S3Folders.ProfilePicture);
            user.ProfilePictue = imageName;
            string oldImage = _userInformationRepository.UpdateProfilePicture(user.UserId, imageName); // Get file name if exist
            //delete oldImageFromAws
            if(oldImage != null)
            _awsS3FileStorage.DeleteFile(oldImage, Constants.S3Folders.CompanyLogo);
        }

        public void UpdateUserRole(UserRequestViewModel user)
        {
            _userInformationRepository.UpdateUserRole(user);
        }
        public bool UpdateUserStatus(UserRequestViewModel user)
        {
            return _userInformationRepository.UpdateUserStatus(user);
        }

        public bool CheckUserLimitForCompany(CompanyViewModel company)
        {
            return _userInformationRepository.CheckUserLimitForCompany(company);
        }
        
        public bool IsUserExist(string email)
        {
            return _userInformationRepository.IsUserExist(email);
        }
        public void AddUserInvite(UserInviteViewModel userInvite)
        {
            _userInformationRepository.AddUserInvite(userInvite);
        }

        public UserInviteViewModel GetUserInvite(string email)
        {
            return _userInformationRepository.GetUserInvite(email);
        }

        public bool UpdateNotificationSetting(UserRequestViewModel user)
        {
            return _userInformationRepository.UpdateNotificationSetting(user);
        }

        public UserProfileViewModel UpdateUserProfile(UserProfileViewModel userProfileViewModel)
        {
            UserProfileViewModel userProfile = _userInformationRepository.UpdateUserProfile(userProfileViewModel);
            if (userProfile.ProfilePicture != null)
            {
                if (_awsS3FileStorage.fileExistsInS3(userProfile.ProfilePicture, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.ProfilePicture.ToString()}"))
                {
                    userProfile.ProfilePicture = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.ProfilePicture.ToString(), userProfile.ProfilePicture, true, string.Empty);
                }
            }
            else
            {
                string path = $"{AppUtil.BaseUrl}{AppUtil.ProfileImagesUrl}";
                userProfile.ProfilePicture = Path.Combine(path, Constants.DEFAULT_PROFILE_PICTURE);
            }
            return userProfile;
        }

        public CompanyInfoViewModel UpdateCompanyProfile(CompanyInfoViewModel companyInfo)
        {
            CompanyInfoViewModel company = _userInformationRepository.UpdateCompanyProfile(companyInfo);
            if (company != null && company.CompanyLogo != null)
            {
                if (_awsS3FileStorage.fileExistsInS3(company.CompanyLogo, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.CompanyLogo.ToString()}"))
                {
                    company.CompanyLogo = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.CompanyLogo.ToString(), company.CompanyLogo, true, string.Empty);
                }
            }
            return company;
        }

        public UserProfileViewModel GetUserProfileStatus(UserProfileViewModel user)
        {
            return _userInformationRepository.GetUserProfileStatus(user);
        }

        public List<PlaylistListViewModel> GetUserAssociatedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist)
        {
            return _userInformationRepository.GetUserAssociatedPlaylistList(associatedPlaylist);
        }

        public List<PlaylistListViewModel> GetUserAssignedPlaylistList(UserAssociatedPlaylistRequestViewModel associatedPlaylist)
        {
            return _userInformationRepository.GetUserAssignedPlaylistList(associatedPlaylist);
        }

        public List<AssociatedTeamListViewModel> GetUserAssociatedTeam(long userId, string search)
        {
            return _userInformationRepository.GetUserAssociatedTeam(userId, search);
        }

        public List<AssociatedUsersListViewModel> GetUserAssociatedUsers(long userId, string search)
        {
            return _userInformationRepository.GetUserAssociatedUsers(userId, search);
        }

        public Response CanUserRegister(string emailId)
        {
            return _userInformationRepository.CanUserRegister(emailId);
        }

        public void UpdateCompanyLogo(CompanyLogoRequestViewModel company)
        {
            string fileName = Guid.NewGuid() + ".jpg";
            _awsS3FileStorage.UploadFile(fileName, company.CompanyLogo, Constants.S3Folders.CompanyLogo);
            company.CompanyLogo = fileName;
            string oldImage =_userInformationRepository.UpdateCompanyLogo(company.UserId,company.CompanyLogo);
            //delete oldImageFromAws
            if (oldImage != null)
                _awsS3FileStorage.DeleteFile(oldImage, Constants.S3Folders.CompanyLogo);
        }

        public CompanyInfoViewModel GetCompanyInfo(long userId)
        {
            CompanyInfoViewModel companyInfo = _userInformationRepository.GetCompanyInfo(userId);
            if (companyInfo != null)
            {
                if(companyInfo.CompanyLogo != null)
                {
                    if (_awsS3FileStorage.fileExistsInS3(companyInfo.CompanyLogo, $"{_awsS3FileStorage.BucketName}/{Constants.S3Folders.CompanyLogo.ToString()}"))
                    {
                        companyInfo.CompanyLogo = _awsS3FileStorage.GeneratePreSignedURLForPopUp(Constants.S3Folders.CompanyLogo.ToString(), companyInfo.CompanyLogo, true, string.Empty);
                    }
                }
            }
            return companyInfo;
        }

        void IUserInformationService.DeleteCompanyLogo(CompanyLogoRequestViewModel company)
        {
            string oldImage = _userInformationRepository.DeleteCompanyLogo(company.UserId, company.CompanyLogo);
            //delete oldImageFromAws
            if (oldImage != null)
                _awsS3FileStorage.DeleteFile(oldImage, Constants.S3Folders.CompanyLogo);
        }

        void IUserInformationService.DeleteProfileImage(CompanyLogoRequestViewModel company)
        {
            string oldImage = _userInformationRepository.DeleteProfileImage(company.UserId, company.CompanyLogo);
            //delete oldImageFromAws
            if (oldImage != null)
                _awsS3FileStorage.DeleteFile(oldImage, Constants.S3Folders.CompanyLogo);
        }

        public void SaveLocations(LocationsViewModel locationsViewModel, long userId)
        {
            _userInformationRepository.SaveLocations(locationsViewModel, userId);
        }

        public void DeleteLocation(int locationsId)
        {
            _userInformationRepository.DeleteLocation(locationsId);
        }

        public bool LocationExists(LocationsViewModel objLocationViewModel, long userId)
        {
            return _userInformationRepository.LocationExists(objLocationViewModel, userId);
        }

        public List<LocationsViewModel> GetSignUpLocations(LocationsSearch location)
        {
            return _userInformationRepository.GetSignUpLocations(location);
        }   
        public List<LocationsViewModel> GetLocations(LocationsSearch location, long userId)
        {
            return _userInformationRepository.GetLocations(location, userId);
        } 
        public List<LocationsViewModel> GetLocationsByCompanyId(long companyId)
        {
            return _userInformationRepository.GetLocationsByCompanyId(companyId);
        }
    }
}
