﻿using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using VirtualBrandWork.Common;
using VirtualBrandWork.Entity;
using VirtualBrandWork.Entity.ViewModels;

namespace Service
{
    public class AWSS3FileStorage : IAWSS3FileStorage
    {
        private const string publicKeySetting = "S3AccessKey";
        private const string privateKeySetting = "S3SecreteKey";
        private const string bucketName = "Bucket";
        RegionEndpoint bucketRegion = RegionEndpoint.USWest2;

        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AWSS3FileStorage));

        public string PublicKey
        {
            get
            {
                return ConfigurationManager.AppSettings[publicKeySetting];
            }
        }

        public string PrivateKey
        {
            get
            {
                return ConfigurationManager.AppSettings[privateKeySetting];
            }
        }

        public string BucketName
        {
            get
            {
                return ConfigurationManager.AppSettings[bucketName];
            }
        }

        public string GeneratePreSignedURLForPopUp(string childFolder, string fileName, bool flag, string mimeType)
        {
            string urlString = string.Empty;
            var s3Client = new AmazonS3Client(ConfigurationManager.AppSettings[publicKeySetting], ConfigurationManager.AppSettings[privateKeySetting], bucketRegion);
            try
            {
                GetPreSignedUrlRequest urlRequest = new GetPreSignedUrlRequest
                {
                    BucketName = ConfigurationManager.AppSettings[bucketName],
                    Key = flag == true ? childFolder + "/" + fileName : fileName,
                    Expires = DateTime.Now.AddDays(Convert.ToInt32(Constants.UrlRequest_ExpiresIn)),                     
                };
                urlString = s3Client.GetPreSignedURL(urlRequest);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                logger.Error($"{amazonS3Exception.Message}");
            }
            catch (Exception ex)
            {
                logger.Error($"{ex}");
            }
            return urlString;
        }

        public bool CreateThumbnailFromFile(string contentURL, string mimeType, Resolution resolution = null)
        {
            try
            {
                var thumbnailbucket = "virtualbrandworks/thumbnails";

                var s3Client = new AmazonS3Client(ConfigurationManager.AppSettings[publicKeySetting], ConfigurationManager.AppSettings[privateKeySetting], bucketRegion);

                string URLString = GeneratePreSignedURLForPopUp(ConfigurationManager.AppSettings[bucketName], contentURL, false, mimeType);

                var keyThumb = Path.ChangeExtension(contentURL, ".jpg");
 
                Process ffmpegProces = new Process();
                ffmpegProces.StartInfo.FileName = ConfigurationManager.AppSettings["ffmpegLocation"] + "ffmpeg.exe";
                ffmpegProces.StartInfo.CreateNoWindow = true;
                ffmpegProces.StartInfo.UseShellExecute = false;
                string outputPath = ConfigurationManager.AppSettings["ffmpegLocation"] + keyThumb;
                string arg = null;
                if (resolution == null)
                {
                   arg = String.Format("-i \"{0}\" -vframes 1 -an -s 640x400 -ss 2 {1}", URLString, outputPath);
                }
                else
                {
                    arg = String.Format("-i \"{0}\" -vframes 1 -an -s {1}x{2} -ss 2 {3}", URLString, resolution.Width, resolution.Height, outputPath);
                }
                
                ffmpegProces.StartInfo.Arguments = arg;
                ffmpegProces.Start();
                ffmpegProces.WaitForExit();

                //if file is not generated then corrpted
                if (!File.Exists(outputPath))
                    return false;

                var fileTransferUtility = new TransferUtility(s3Client);

                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = thumbnailbucket,
                    FilePath = outputPath,
                    StorageClass = S3StorageClass.Standard,
                    PartSize = 6291456, // 6 MB.  
                    Key = keyThumb
                };

                fileTransferUtilityRequest.Metadata.Add("thumbnailforcontent", contentURL);
                fileTransferUtility.Upload(fileTransferUtilityRequest);
                fileTransferUtility.Dispose();

                //once move the thumbnail to server remove the content
                if ((File.Exists(outputPath)))
                    File.Delete(outputPath);

                return true;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    logger.Error($"{amazonS3Exception.Message}");
                }
                else
                {
                    logger.Error($"{amazonS3Exception.Message}");
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error($"{ex}");
                return false;
            }
        }
        public bool fileExistsInS3(string fileKey, string bucketName)
        {
            var s3Client = new AmazonS3Client(ConfigurationManager.AppSettings[publicKeySetting], ConfigurationManager.AppSettings[privateKeySetting], bucketRegion);
            try
            {
                S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(s3Client, bucketName, fileKey);
                if (s3FileInfo.Exists)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;
                logger.Error($"{ex}");
                //status wasn't not found, so throw the exception
                throw;
            }
        }

        public string GetPolicyString(string fileId, string redirectUrl)
        {
            var policy = new UploadPolicy(DateTime.Now.AddHours(10));

            policy.AddCondition(new List<string> { "eq", "$bucket", ConfigurationManager.AppSettings[bucketName] });
            policy.AddCondition(new List<string> { "eq", "$acl", "private" });
            policy.AddCondition(new List<string> { "content-length-range", "0", "100000000" });
            policy.AddCondition(new List<string> { "eq", "$key", fileId });
            //policy.AddCondition(new List<string> { "eq", "$ContentType", "" });
            //policy.AddCondition(new List<string> { "eq", "$Content-Disposition", "inline" });
            //policy.AddCondition(new List<string> { "eq", "$success_action_status", "201" });
            //policy.AddCondition(new List<string> { "eq", "$success_action_redirect", "201" });

            var ser = new DataContractJsonSerializer(typeof(UploadPolicy));
            var ms = new MemoryStream();
            ser.WriteObject(ms, policy);
            var json = Encoding.Default.GetString(ms.ToArray());
            return json;
        }

        public Tuple<string,string,string> GetContentUrls(string contentUrl, string bucketName, string thumbnailBucketName)
        {
            string thumbnailUrl = string.Empty;
            string contentURLWithExtension;
            string newContentUrl = contentUrl;
            if (fileExistsInS3(contentUrl, bucketName))
            {
                string extentionFound = Constants.EXTENSIONS.FirstOrDefault<string>(s => contentUrl.Contains(s));
                contentURLWithExtension = contentUrl;
                switch (extentionFound)
                {
                    case ".mp4":
                    case ".mov":
                    case ".3gp":
                    case ".MKV":
                    case ".MOV":
                    case ".mkv":
                    case ".webm":
                    case ".WEBM":
                        int index = contentUrl.LastIndexOf('.');
                        var key = index == -1 ? contentUrl : contentUrl.Substring(0, index);
                        var keyThumb = Path.ChangeExtension(key, ".jpg");
                        thumbnailUrl = GeneratePreSignedURLForPopUp(thumbnailBucketName, keyThumb, true, null);
                        break;
                    case ".pdf":
                        thumbnailUrl = $"{AppUtil.BaseUrl}{AppUtil.PdfThumbnail}";
                        break;
                    case ".doc":
                        thumbnailUrl = $"{AppUtil.BaseUrl}{AppUtil.DocThumbnail}";
                        break;
                    case ".txt":
                        thumbnailUrl = $"{AppUtil.BaseUrl}{AppUtil.TxtThumbnail}";
                        break;
                    default :
                        thumbnailUrl = GeneratePreSignedURLForPopUp(BucketName, contentUrl, false, null);
                        break;
                }
                newContentUrl = GeneratePreSignedURLForPopUp(BucketName, contentUrl, false, null);
            }
            else
            {
                contentURLWithExtension = "default";
            }
            return Tuple.Create(newContentUrl,thumbnailUrl,contentURLWithExtension);
        }

        public void UploadFile(string fileName, string message, VirtualBrandWork.Entity.Constants.S3Folders s3Folder)
        {
            try
            {
              //  byte[] byteArray = Encoding.ASCII.GetBytes(message);
                byte[] byteArray = Convert.FromBase64String(message);
                using (MemoryStream stream = new MemoryStream(byteArray))
                {
                    IAmazonS3 client;
                    using (client = new AmazonS3Client(PublicKey, PrivateKey, bucketRegion))
                    {
                        var request = new PutObjectRequest()
                        {
                            BucketName = BucketName + "/" + s3Folder.ToString(),
                            CannedACL = S3CannedACL.Private,
                            Key = fileName,
                            InputStream = stream
                        };

                        client.PutObject(request);
                    }
                }
            }
            catch (Exception ex)
            {

                logger.Error("Exception Occured while uploading to Amazon S3 : " + ex, ex);
            }
        }

        public void DeleteFile(string fileName, VirtualBrandWork.Entity.Constants.S3Folders s3Folder)
        {
            try
            {
                //  byte[] byteArray = Encoding.ASCII.GetBytes(message);
                //byte[] byteArray = Convert.FromBase64String(message);
                //using (MemoryStream stream = new MemoryStream(byteArray))
                //{
                    IAmazonS3 client;
                    using (client = new AmazonS3Client(PublicKey, PrivateKey, bucketRegion))
                    {
                        var request = new DeleteObjectRequest()
                        {
                            BucketName = s3Folder == Constants.S3Folders.Root ? BucketName : BucketName + "/" + s3Folder.ToString(),
                            Key = fileName,
                        };

                        client.DeleteObject(request);
                    }
                //}
            }
            catch (Exception ex)
            {

                logger.Error("Exception Occured while deleting from Amazon S3 : " + ex, ex);
            }
        }

    }
}
